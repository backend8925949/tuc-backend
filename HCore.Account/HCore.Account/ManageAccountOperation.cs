//==================================================================================
// FileName: ManageAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================



using HCore.Account.Framework;
using HCore.Account.Object;
using HCore.Helper;

namespace HCore.Account
{
    public class ManageAccountOperation
    {
        FrameworkAccountOperation _FrameworkAccountOperation;
        /// <summary>
        /// Description: Resets the password initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetPassword_Initialize(OAccountOperation.ResetPassword _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetPassword_Initialize(_Request);
        }
        /// <summary>
        /// Description: Resets the password confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetPassword_Confirm(OAccountOperation.ResetPassword_Confirm _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetPassword_Confirm(_Request);
        }

        /// <summary>
        /// Description: Resets the user name initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetUserName_Initialize(OAccountOperation.ResetUserName _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetUserName_Initialize(_Request);
        }
        /// <summary>
        /// Description: Resets the user name confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetUserName_Confirm(OAccountOperation.ResetUserName_Confirm _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetUserName_Confirm(_Request);
        }

        /// <summary>
        /// Description: Updates the password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePassword(OAccountOperation.UpdatePassword _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdatePassword(_Request);
        }
        /// <summary>
        /// Description: Updates the username.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUsername(OAccountOperation.UpdateUsername _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateUsername(_Request);
        }
    }
}
