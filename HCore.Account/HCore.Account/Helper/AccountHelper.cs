//==================================================================================
// FileName: AccountHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.Account.Helper
{
    internal static  class AccountHelper
    {
        internal static class SendGridEmailTemplate
        {
            public const string Acc_Password_Update = "d-34cf43c806da406b9b78007b4ca247d8";
            public const string Acc_LoginId_Update = "d-1bbd986ed73443119ca92914caf32e61";
            public const string Acc_ResetPassword_Initialize = "d-25172c025f234db08a250278d50cd5a0";
            public const string Acc_ResetPassword_Confirm = "d-1dc36572b3b143cc8643f05ab93a34e2";

            public const string Acc_ResetUserName_Initialize = "d-25172c025f234db08a250278d50cd5a0";
            public const string Acc_ResetUserName_Confirm = "d-1dc36572b3b143cc8643f05ab93a34e2";
        }
    }
}
