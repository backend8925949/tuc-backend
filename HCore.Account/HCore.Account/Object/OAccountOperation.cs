//==================================================================================
// FileName: OAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.Account.Object
{
    public class OAccountOperation
    {
        public class CreateAccount
        {
            public class Request
            {
                public long AccountTypeId { get; set; }
                public long? ReferenceId { get; set; }
                public string? Reference { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountTypeCode { get; set; }
                public string? AccountOperationTypeCode { get; set; }
                public string? RegistrationSourceCode { get; set; }
                public string? OwnerName { get; set; }

                public string? OwnerKey { get; set; }
                public string? SubOwnerKey { get; set; }
                public string? BankKey { get; set; }

                public string? UserName { get; set; }
                public string? Password { get; set; }
                public string? SecondaryPassword { get; set; }
                public string? AccessPin { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? SecondaryEmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? Description { get; set; }
                public string? WebsiteUrl { get; set; }

                public string? ReferralCode { get; set; }
                public string? ReferralUrl { get; set; }

                public string? RoleKey { get; set; }

                public string? AccountLevelCode { get; set; }
                public string? SubscriptionKey { get; set; }
                public string? ApplicationStatusCode { get; set; }

                public long? CountValue { get; set; }
                public double? AverageValue { get; set; }


                public string? CountryKey { get; set; }
                public string? RegionKey { get; set; }
                public string? RegionAreaKey { get; set; }
                public string? CityKey { get; set; }
                public string? CityAreaKey { get; set; }
                public string? Comment { get; set; }

                public sbyte EmailVerificationStatus { get; set; }
                public DateTime? EmailVerificationStatusDate { get; set; }

                public sbyte NumberVerificationStatus { get; set; }
                public DateTime? NumberVerificationStatusDate { get; set; }

                public OStorageContent IconContent { get; set; }
                public OStorageContent PosterContent { get; set; }

                public string? StatusCode { get; set; }
                public OAddress AddressInfo { get; set; }

                //public List<Owner> Owners { get; set; }
                public List<Configuration> Configurations { get; set; }

                public string? OldPassword { get; set; }
                public string? OldAccessPin { get; set; }

                public double Credit { get; set; }
                public double Debit { get; set; }
                public double AccountPercentage { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
              

            }
            public class AccountDetails
            {
            
            }
            public class Configuration
            {
                public string? TypeCode { get; set; }
                public string? SystemName { get; set; }
                public string? Value { get; set; }
                public string? HelperCode { get; set; }
                public string? CommonKey { get; set; }
                public string? ParameterKey { get; set; }
                public DateTime? StartTime { get; set; }
                public DateTime? EndTime { get; set; }
            }

            public class Details
            {
                internal long? AccountTypeId { get; set; }
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountTypeCode { get; set; }
                public string? AccountTypeName { get; set; }
                public string? AccountOperationTypeCode { get; set; }
                public string? AccountOperationTypeName { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }
                public string? OwnerIconUrl { get; set; }
                public string? BankKey { get; set; }
                public string? BankDisplayName { get; set; }
                public string? DisplayName { get; set; }
                public string? AccessPin { get; set; }
                public string? AccountCode { get; set; }
                public string? IconUrl { get; set; }
                public string? PosterUrl { get; set; }
                public string? ReferralCode { get; set; }
                public string? ReferralUrl { get; set; }
                public string? Description { get; set; }
                public string? RegistrationSourceCode { get; set; }
                public string? RegistrationSourceName { get; set; }

                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }

                public string? AppKey { get; set; }
                public string? AppName { get; set; }
                public string? AppVersionKey { get; set; }
                public string? AppVersionName { get; set; }
                public string? ApplicationStatusCode { get; set; }
                public string? ApplicationStatusName { get; set; }

                public DateTime? LastLoginDate { get; set; }

                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }

                public string? RequestKey { get; set; }

                public long? CountValue { get; set; }
                public double? AverageValue { get; set; }

                public DateTime? CreateDate { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public string? CreatedByIconUrl { get; set; }
                internal long? CreatedByAccountTypeId { get; set; }
                internal long? CreatedById { get; set; }
                public DateTime? ModifyDate { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? ModifyByIconUrl { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public string? UserName { get; set; }
                public string? Password { get; set; }
                public string? SecondaryPassword { get; set; }
                public string? SystemPassword { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? SecondaryEmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public DateTime? DateOfBirth { get; set; }

                //public string? Address { get; set; }
                //public double? Latitude { get; set; }
                //public double? Longitude { get; set; }
                //public string? CountryKey { get; set; }
                //public string? CountryName { get; set; }
                //public string? RegionKey { get; set; }
                //public string? RegionName { get; set; }
                //public string? RegionAreaKey { get; set; }
                //public string? RegionAreaName { get; set; }
                //public string? CityKey { get; set; }
                //public string? CityName { get; set; }
                //public string? CityAreaKey { get; set; }
                //public string? CityAreaName { get; set; }

                public string? WebsiteUrl { get; set; }

                public double? SubOwnerLatitude { get; set; }
                public double? SubOwnerLongitude { get; set; }
                public string? SubOwnerKey { get; set; }
                public string? SubOwnerDisplayName { get; set; }
                public string? SubOwnerAddress { get; set; }


                public sbyte? EmailVerificationStatus { get; set; }
                public DateTime? EmailVerificationStatusDate { get; set; }

                public sbyte? NumberVerificationStatus { get; set; }
                public DateTime? NumberVerificationStatusDate { get; set; }
                public long? SubAccounts { get; set; }
            }

        }
        public class ResetUserName
        {
            public string? EmailAddress { get; set; }
            public string? AccountTypeCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class ResetUserName_Confirm
        {
            public string? AccountTypeCode { get; set; }
            public long RequestId { get; set; }
            public string? RequestKey { get; set; }
            public string? ResetCode { get; set; }
            public string? NewUserName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class ResetPassword
        {
            public string? Username { get; set; }
            public string? AccountTypeCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class ResetPassword_Confirm
        {
            public string? AccountTypeCode { get; set; }
            public long RequestId { get; set; }
            public string? RequestKey { get; set; }
            public string? ResetCode { get; set; }
            public string? NewPassword { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class UpdatePassword
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? OldPassword { get; set; }
            public string? NewPassword { get; set; }
            public string? AccountTypeCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class UpdateUsername
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? Username { get; set; }
            public string? AccountTypeCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }


    

}
