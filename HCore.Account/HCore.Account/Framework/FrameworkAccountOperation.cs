//==================================================================================
// FileName: FrameworkAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Account.Helper;
using HCore.Account.Object;
using HCore.Account.Resource;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.Account.Framework
{
    internal class FrameworkAccountOperation
    {
        HCoreContext _HCoreContext;

        HCUAccountParameter _HCUAccountParameter;
        Random _Random;
        /// <summary>
        /// Description: Resets the password initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetPassword_Initialize(OAccountOperation.ResetPassword _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Username = _Request.Username.Trim();
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => (x.EmailAddress == _Request.Username || x.User.Username == _Request.Username) && x.AccountType.SystemName == _Request.AccountTypeCode )
                        .Select(x => new
                        {
                            AccountId = x.Id,
                            StatusId = x.StatusId,
                            Password = x.User.Password,
                            UserId = x.UserId,
                            DisplayName = x.DisplayName,
                            EmailAddress = x.EmailAddress,
                            AccountTypeId = x.AccountTypeId,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active || Details.StatusId == HelperStatus.Default.Inactive)
                        {
                            if (
                                Details.AccountTypeId == Helpers.UserAccountType.Merchant
                                || Details.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount
                                || Details.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                    || Details.AccountTypeId == Helpers.UserAccountType.Acquirer
                                    || Details.AccountTypeId == Helpers.UserAccountType.PgAccount
                                    || Details.AccountTypeId == Helpers.UserAccountType.PosAccount
                                    || Details.AccountTypeId == Helpers.UserAccountType.Partner
                                    || Details.AccountTypeId == Helpers.UserAccountType.Admin
                                    )
                            {
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = Details.AccountId;
                                _HCUAccountParameter.TypeId = Helpers.AccountActionType.ResetPasswordRequest;
                                _HCUAccountParameter.Name = "PasswordReset_Ini";
                                _HCUAccountParameter.SystemName = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountParameter.CountValue = 0;
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreatedById = Details.AccountId;
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                _HCoreContext.SaveChanges();
                                if (!string.IsNullOrEmpty(Details.EmailAddress))
                                {

                                    string ResetLink = "resetpassword/" + _HCUAccountParameter.Id + "/" + _HCUAccountParameter.Guid + "/" + _HCUAccountParameter.SystemName;
                                    if (Details.AccountTypeId == Helpers.UserAccountType.Merchant || Details.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Merchant + ResetLink;
                                    }
                                    if (Details.AccountTypeId == Helpers.UserAccountType.Acquirer || Details.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Acquirer + ResetLink;
                                    }
                                    if (Details.AccountTypeId == Helpers.UserAccountType.PgAccount)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Pssp + ResetLink;
                                    }
                                    if (Details.AccountTypeId == Helpers.UserAccountType.PosAccount)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Ptsp + ResetLink;
                                    }
                                    if (Details.AccountTypeId == Helpers.UserAccountType.Partner)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Partner + ResetLink;
                                    }
                                    if (Details.AccountTypeId == Helpers.UserAccountType.Admin)
                                    {
                                        ResetLink = _AppConfig.PanelUrl.Admin + ResetLink;
                                    }
                                    var _EmailParameters = new
                                    {
                                        UserDisplayName = Details.DisplayName,
                                        Code = _HCUAccountParameter.SystemName,
                                        ResetLink = ResetLink,
                                    };
                                    HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_ResetPassword_Initialize, Details.DisplayName, Details.EmailAddress, _EmailParameters, _Request.UserReference);
                                }
                                var _Parameters = new
                                {
                                    RequestId = _HCUAccountParameter.Id,
                                    RequestKey = _HCUAccountParameter.Guid,
                                    AccountTypeCode = _Request.AccountTypeCode,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Parameters, ResourceEn.HCAC010, ResourceEn.HCAC010M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC024, ResourceEn.HCAC024M);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC005, ResourceEn.HCAC005M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC0404, ResourceEn.HCAC0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ResetPassword_Initialize", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the password confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetPassword_Confirm(OAccountOperation.ResetPassword_Confirm _Request)
        {
            #region Manage Exception
            try
            {
                _Request.NewPassword = _Request.NewPassword.Trim();
                if (_Request.RequestId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC017, ResourceEn.HCAC017M);
                }
                if (string.IsNullOrEmpty(_Request.RequestKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC011, ResourceEn.HCAC011M);
                }
                if (string.IsNullOrEmpty(_Request.ResetCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC014, ResourceEn.HCAC014M);
                }
                if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC015, ResourceEn.HCAC015M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var RequestDetails = _HCoreContext.HCUAccountParameter
                        .Where(x => x.Guid == _Request.RequestKey && x.Id == _Request.RequestId && x.SystemName == _Request.ResetCode)
                        .FirstOrDefault();
                    if (RequestDetails != null)
                    {
                        if (RequestDetails.StatusId == HelperStatus.Default.Active)
                        {
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            var MinuteDifference = CurrentTime.Subtract((DateTime)RequestDetails.CreateDate).TotalMinutes;
                            if (MinuteDifference > 4)
                            {
                                RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC020, ResourceEn.HCAC020M);
                            }
                            if (RequestDetails.CountValue > 3)
                            {
                                RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC021, ResourceEn.HCAC021M);
                            }
                            if (RequestDetails.SystemName != _Request.ResetCode)
                            {
                                RequestDetails.CountValue += 1;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC013, ResourceEn.HCAC013M);
                            }
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == RequestDetails.AccountId).FirstOrDefault();
                            if (AccountDetails.StatusId == HelperStatus.Default.Active || AccountDetails.StatusId == HelperStatus.Default.Inactive)
                            {
                                var UserDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Id == AccountDetails.UserId).FirstOrDefault();
                                if (UserDetails != null)
                                {
                                    RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                    RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    UserDetails.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    UserDetails.SystemPassword = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    UserDetails.ModifyById = AccountDetails.Id;
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    #region Send Email
                                    if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                                    {
                                        var _EmailParameters = new
                                        {
                                            DisplayName = AccountDetails.DisplayName,
                                        };
                                        HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_ResetPassword_Confirm, AccountDetails.DisplayName, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                    }
                                    #endregion
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, ResourceEn.HCAC016, ResourceEn.HCAC016M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC023, ResourceEn.HCAC023M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC022, ResourceEn.HCAC022M);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC012, ResourceEn.HCAC012M);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC019, ResourceEn.HCAC019M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdatePassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the user name initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetUserName_Initialize(OAccountOperation.ResetUserName _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => (x.EmailAddress == _Request.EmailAddress) && x.AccountType.SystemName == _Request.AccountTypeCode)
                        .Select(x => new
                        {
                            AccountId = x.Id,
                            StatusId = x.StatusId,
                            Password = x.User.Password,
                            UserId = x.UserId,
                            DisplayName = x.DisplayName,
                            EmailAddress = x.EmailAddress,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active || Details.StatusId == HelperStatus.Default.Inactive)
                        {
                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.AccountId = Details.AccountId;
                            _HCUAccountParameter.TypeId = Helpers.AccountActionType.RestUsernameRequest;
                            _HCUAccountParameter.Name = "UserNameReset_Ini";
                            _HCUAccountParameter.SystemName = HCoreHelper.GenerateRandomNumber(6);
                            _HCUAccountParameter.CountValue = 0;
                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreatedById = Details.AccountId;
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            _HCoreContext.SaveChanges();
                            if (!string.IsNullOrEmpty(Details.EmailAddress))
                            {
                                var _EmailParameters = new
                                {
                                    UserDisplayName = Details.DisplayName,
                                    Code = _HCUAccountParameter.SystemName
                                };
                                HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_ResetUserName_Initialize, Details.DisplayName, Details.EmailAddress, _EmailParameters, _Request.UserReference);
                            }
                            var _Parameters = new
                            {
                                RequestId = _HCUAccountParameter.Id,
                                RequestKey = _HCUAccountParameter.Guid,
                                AccountTypeCode = _Request.AccountTypeCode,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Parameters, ResourceEn.HCAC010, ResourceEn.HCAC010M);
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC005, ResourceEn.HCAC005M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC0404, ResourceEn.HCAC0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ResetPassword_Initialize", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the user name confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetUserName_Confirm(OAccountOperation.ResetUserName_Confirm _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.RequestId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC017, ResourceEn.HCAC017M);
                }
                if (string.IsNullOrEmpty(_Request.RequestKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC011, ResourceEn.HCAC011M);
                }
                if (string.IsNullOrEmpty(_Request.ResetCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC014, ResourceEn.HCAC014M);
                }
                if (string.IsNullOrEmpty(_Request.NewUserName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC015, ResourceEn.HCAC015M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var RequestDetails = _HCoreContext.HCUAccountParameter
                        .Where(x => x.Guid == _Request.RequestKey && x.Id == _Request.RequestId && x.SystemName == _Request.ResetCode)
                        .FirstOrDefault();
                    if (RequestDetails != null)
                    {
                        if (RequestDetails.StatusId == HelperStatus.Default.Active)
                        {
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            var MinuteDifference = CurrentTime.Subtract((DateTime)RequestDetails.CreateDate).TotalMinutes;
                            if (MinuteDifference > 4)
                            {
                                RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC020, ResourceEn.HCAC020M);
                            }
                            if (RequestDetails.CountValue > 3)
                            {
                                RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC021, ResourceEn.HCAC021M);
                            }
                            if (RequestDetails.SystemName != _Request.ResetCode)
                            {
                                RequestDetails.CountValue += 1;
                                RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC013, ResourceEn.HCAC013M);
                            }
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == RequestDetails.AccountId).FirstOrDefault();
                            if (AccountDetails.StatusId == HelperStatus.Default.Active || AccountDetails.StatusId == HelperStatus.Default.Inactive)
                            {
                                var UserDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Id == AccountDetails.UserId).FirstOrDefault();
                                if (UserDetails != null)
                                {
                                    RequestDetails.StatusId = HelperStatus.Default.Blocked;
                                    RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.Password = _Request.NewUserName;
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    #region Send Email
                                    if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                                    {
                                        var _EmailParameters = new
                                        {
                                            DisplayName = AccountDetails.DisplayName,
                                        };
                                        HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_ResetUserName_Confirm, AccountDetails.DisplayName, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                    }
                                    #endregion
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, ResourceEn.HCAC016, ResourceEn.HCAC016M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC023, ResourceEn.HCAC023M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC022, ResourceEn.HCAC022M);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC012, ResourceEn.HCAC012M);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC019, ResourceEn.HCAC019M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdatePassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePassword(OAccountOperation.UpdatePassword _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCACACCREF, ResourceEn.HCACACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCACACCREFKEY, ResourceEn.HCACACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.OldPassword))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC007, ResourceEn.HCAC007M);
                }
                if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC008, ResourceEn.HCAC008M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountType.SystemName == _Request.AccountTypeCode)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                            Password = x.User.Password,
                            UserId = x.UserId,
                            DisplayName = x.DisplayName,
                            EmailAddress = x.EmailAddress,
                        })
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active || Details.StatusId == HelperStatus.Default.Inactive)
                        {
                            string OldPassword = HCoreEncrypt.DecryptHash(Details.Password);
                            if (OldPassword == _Request.OldPassword)
                            {
                                var UserDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                                if (UserDetails != null)
                                {
                                    UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                }
                                #region Send Email
                                if (!string.IsNullOrEmpty(Details.EmailAddress))
                                {
                                    var _EmailParameters = new
                                    {
                                        UserDisplayName = Details.DisplayName,
                                    };
                                    HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_Password_Update, Details.DisplayName, Details.EmailAddress, _EmailParameters, _Request.UserReference);
                                }
                                #endregion
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, ResourceEn.HCAC004, ResourceEn.HCAC004M);
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC006, ResourceEn.HCAC006M);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC005, ResourceEn.HCAC005M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC0404, ResourceEn.HCAC0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdatePassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the username.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUsername(OAccountOperation.UpdateUsername _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCACACCREF, ResourceEn.HCACACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCACACCREFKEY, ResourceEn.HCACACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.Username))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC009, ResourceEn.HCAC009M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountType.SystemName == _Request.AccountTypeCode)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active || Details.StatusId == HelperStatus.Default.Inactive)
                        {
                            var UserDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserDetails.Username = _Request.Username;
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            #region Send Email
                            if (!string.IsNullOrEmpty(Details.EmailAddress))
                            {
                                var _EmailParameters = new
                                {
                                    UserDisplayName = Details.DisplayName,
                                };
                                HCoreHelper.BroadCastEmail(AccountHelper.SendGridEmailTemplate.Acc_LoginId_Update, Details.DisplayName, Details.EmailAddress, _EmailParameters, _Request.UserReference);
                            }
                            #endregion
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, ResourceEn.HCAC004, ResourceEn.HCAC004M);

                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC005, ResourceEn.HCAC005M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceEn.HCAC0404, ResourceEn.HCAC0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUsername", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        
    }
}
