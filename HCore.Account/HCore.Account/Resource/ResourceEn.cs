//==================================================================================
// FileName: ResourceEn.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.Account.Resource
{
   internal class ResourceEn
    {
        internal const string HCAC0500 = "CA0500";
        internal const string HCAC0500M = "Internal server error occured. Please try after some time";

        internal const string HCAC0200 = "CA0200";
        internal const string HCAC0200M = "Details loaded";

        internal const string HCAC0404 = "CA0404";
        internal const string HCAC0404M = "Details not found. It might be removed or not available at the moment";

        internal const string HCAC0201 = "CA0201";
        internal const string HCAC0201M = "Details updated successfully";

        internal const string HCACREF = "HCACREF";
        internal const string HCACREFM = "Reference id missing";

        internal const string HCACREFKEY = "HCACREFKEY";
        internal const string HCACREFKEYM = "Reference key missing";

        internal const string HCACACCREF = "HCACACCREF";
        internal const string HCACACCREFM = "Account reference missing";

        internal const string HCACACCREFKEY = "HCACACCREFKEY";
        internal const string HCACACCREFKEYM = "Account reference key missing";

        internal const string HCACSTATUS = "HCACSTATUS";
        internal const string HCACSTATUSM = "Status code required";
        internal const string HCACINSTATUS = "HCACINSTATUS";
        internal const string HCACINSTATUSM = "Invalid status code";

        internal const string HCAC001 = "HCAC001";
        internal const string HCAC001M = "Old password required";
        internal const string HCAC002 = "HCAC002";
        internal const string HCAC002M = "New password required";
        internal const string HCAC003 = "HCAC003";
        internal const string HCAC003M = "New password required";
        internal const string HCAC004 = "HCAC004";
        internal const string HCAC004M = "Password updated successfully";
        internal const string HCAC005 = "HCAC005";
        internal const string HCAC005M = "Operation failed. Account is suspended or blocked";
        internal const string HCAC006 = "HCAC006";
        internal const string HCAC006M = "Invalid old password.";
        internal const string HCAC007 = "HCAC007";
        internal const string HCAC007M = "Old password required";
        internal const string HCAC008 = "HCAC008";
        internal const string HCAC008M = "New password required";
        internal const string HCAC009 = "HCAC009";
        internal const string HCAC009M = "Username required";
        internal const string HCAC010 = "HCAC010";
        internal const string HCAC010M = "We have sent you password reset link. Please click on the link or enter 6 digit code to reset your account password";
        internal const string HCAC011 = "HCAC011";
        internal const string HCAC011M = "Request key required";
        internal const string HCAC012 = "HCAC012";
        internal const string HCAC012M = "Invalid request. Please process reset again";
        internal const string HCAC013 = "HCAC013";
        internal const string HCAC013M = "Invalid reset code. Please enter valid reset code again";
        internal const string HCAC014 = "HCAC014";
        internal const string HCAC014M = "Reset code required";
        internal const string HCAC015 = "HCAC015";
        internal const string HCAC015M = "New password requred";
        internal const string HCAC016 = "HCAC016";
        internal const string HCAC016M = "Password reset successful";
        internal const string HCAC017 = "HCAC017";
        internal const string HCAC017M = "Request id missing";
        internal const string HCAC018 = "HCAC018";
        internal const string HCAC018M = "Invalid request. Please process reset password again";
        internal const string HCAC019 = "HCAC019";
        internal const string HCAC019M = "Request expired. Please process reset request again";
        internal const string HCAC020 = "HCAC020";
        internal const string HCAC020M = "Request expired. Please process reset request again";
        internal const string HCAC021 = "HCAC021";
        internal const string HCAC021M = "Request expired. Please process reset request again";
        internal const string HCAC022 = "HCAC022";
        internal const string HCAC022M = "Account is blocked or suspended. Please contact support to activate your account";
        internal const string HCAC023 = "HCAC023";
        internal const string HCAC023M = "Error with account. Please contact support";
        internal const string HCAC024 = "HCAC024";
        internal const string HCAC024M = "Reset password is not allowed for account type. Please contact account owner or support";
        internal const string HCAC025 = "HCAC025";
        internal const string HCAC025M = "xxxxxxxxxxxx";
        internal const string HCAC026 = "HCAC026";
        internal const string HCAC026M = "xxxxxxxxxxxx";
        internal const string HCAC027 = "HCAC027";
        internal const string HCAC027M = "xxxxxxxxxxxx";
        internal const string HCAC028 = "HCAC028";
        internal const string HCAC028M = "xxxxxxxxxxxx";
        internal const string HCAC029 = "HCAC029";
        internal const string HCAC029M = "xxxxxxxxxxxx";
        internal const string HCAC030 = "HCAC030";
        internal const string HCAC030M = "xxxxxxxxxxxx";
        internal const string HCAC031 = "HCAC031";
        internal const string HCAC031M = "xxxxxxxxxxxx";
        internal const string HCAC032 = "HCAC032";
        internal const string HCAC032M = "xxxxxxxxxxxx";
        internal const string HCAC033 = "HCAC033";
        internal const string HCAC033M = "xxxxxxxxxxxx";
        internal const string HCAC034 = "HCAC034";
        internal const string HCAC034M = "xxxxxxxxxxxx";
        internal const string HCAC035 = "HCAC035";
        internal const string HCAC035M = "xxxxxxxxxxxx";
        internal const string HCAC036 = "HCAC036";
        internal const string HCAC036M = "xxxxxxxxxxxx";
        internal const string HCAC037 = "HCAC037";
        internal const string HCAC037M = "xxxxxxxxxxxx";
        internal const string HCAC038 = "HCAC038";
        internal const string HCAC038M = "xxxxxxxxxxxx";
        internal const string HCAC039 = "HCAC039";
        internal const string HCAC039M = "xxxxxxxxxxxx";
        internal const string HCAC040 = "HCAC040";
        internal const string HCAC040M = "xxxxxxxxxxxx";
        internal const string HCAC041 = "HCAC041";
        internal const string HCAC041M = "xxxxxxxxxxxx";
        internal const string HCAC042 = "HCAC042";
        internal const string HCAC042M = "xxxxxxxxxxxx";
        internal const string HCAC043 = "HCAC043";
        internal const string HCAC043M = "xxxxxxxxxxxx";
        internal const string HCAC044 = "HCAC044";
        internal const string HCAC044M = "xxxxxxxxxxxx";
        internal const string HCAC045 = "HCAC045";
        internal const string HCAC045M = "xxxxxxxxxxxx";
        internal const string HCAC046 = "HCAC046";
        internal const string HCAC046M = "xxxxxxxxxxxx";
        internal const string HCAC047 = "HCAC047";
        internal const string HCAC047M = "xxxxxxxxxxxx";
        internal const string HCAC048 = "HCAC048";
        internal const string HCAC048M = "xxxxxxxxxxxx";
        internal const string HCAC049 = "HCAC049";
        internal const string HCAC049M = "xxxxxxxxxxxx";
        internal const string HCAC050 = "HCAC050";
        internal const string HCAC050M = "xxxxxxxxxxxx";
        internal const string HCAC051 = "HCAC051";
        internal const string HCAC051M = "xxxxxxxxxxxx";
        internal const string HCAC052 = "HCAC052";
        internal const string HCAC052M = "xxxxxxxxxxxx";
        internal const string HCAC053 = "HCAC053";
        internal const string HCAC053M = "xxxxxxxxxxxx";
        internal const string HCAC054 = "HCAC054";
        internal const string HCAC054M = "xxxxxxxxxxxx";
        internal const string HCAC055 = "HCAC055";
        internal const string HCAC055M = "xxxxxxxxxxxx";
        internal const string HCAC056 = "HCAC056";
        internal const string HCAC056M = "xxxxxxxxxxxx";
        internal const string HCAC057 = "HCAC057";
        internal const string HCAC057M = "xxxxxxxxxxxx";
        internal const string HCAC058 = "HCAC058";
        internal const string HCAC058M = "xxxxxxxxxxxx";
        internal const string HCAC059 = "HCAC059";
        internal const string HCAC059M = "xxxxxxxxxxxx";
        internal const string HCAC060 = "HCAC060";
        internal const string HCAC060M = "xxxxxxxxxxxx";
        internal const string HCAC061 = "HCAC061";
        internal const string HCAC061M = "xxxxxxxxxxxx";
        internal const string HCAC062 = "HCAC062";
        internal const string HCAC062M = "xxxxxxxxxxxx";
        internal const string HCAC063 = "HCAC063";
        internal const string HCAC063M = "xxxxxxxxxxxx";
        internal const string HCAC064 = "HCAC064";
        internal const string HCAC064M = "xxxxxxxxxxxx";
        internal const string HCAC065 = "HCAC065";
        internal const string HCAC065M = "xxxxxxxxxxxx";
        internal const string HCAC066 = "HCAC066";
        internal const string HCAC066M = "xxxxxxxxxxxx";
        internal const string HCAC067 = "HCAC067";
        internal const string HCAC067M = "xxxxxxxxxxxx";
        internal const string HCAC068 = "HCAC068";
        internal const string HCAC068M = "xxxxxxxxxxxx";
        internal const string HCAC069 = "HCAC069";
        internal const string HCAC069M = "xxxxxxxxxxxx";
        internal const string HCAC070 = "HCAC070";
        internal const string HCAC070M = "xxxxxxxxxxxx";
        internal const string HCAC071 = "HCAC071";
        internal const string HCAC071M = "xxxxxxxxxxxx";
        internal const string HCAC072 = "HCAC072";
        internal const string HCAC072M = "xxxxxxxxxxxx";
        internal const string HCAC073 = "HCAC073";
        internal const string HCAC073M = "xxxxxxxxxxxx";
        internal const string HCAC074 = "HCAC074";
        internal const string HCAC074M = "xxxxxxxxxxxx";
        internal const string HCAC075 = "HCAC075";
        internal const string HCAC075M = "xxxxxxxxxxxx";
        internal const string HCAC076 = "HCAC076";
        internal const string HCAC076M = "xxxxxxxxxxxx";
        internal const string HCAC077 = "HCAC077";
        internal const string HCAC077M = "xxxxxxxxxxxx";
        internal const string HCAC078 = "HCAC078";
        internal const string HCAC078M = "xxxxxxxxxxxx";
        internal const string HCAC079 = "HCAC079";
        internal const string HCAC079M = "xxxxxxxxxxxx";
        internal const string HCAC080 = "HCAC080";
        internal const string HCAC080M = "xxxxxxxxxxxx";
        internal const string HCAC081 = "HCAC081";
        internal const string HCAC081M = "xxxxxxxxxxxx";
        internal const string HCAC082 = "HCAC082";
        internal const string HCAC082M = "xxxxxxxxxxxx";
        internal const string HCAC083 = "HCAC083";
        internal const string HCAC083M = "xxxxxxxxxxxx";
        internal const string HCAC084 = "HCAC084";
        internal const string HCAC084M = "xxxxxxxxxxxx";
        internal const string HCAC085 = "HCAC085";
        internal const string HCAC085M = "xxxxxxxxxxxx";
        internal const string HCAC086 = "HCAC086";
        internal const string HCAC086M = "xxxxxxxxxxxx";
        internal const string HCAC087 = "HCAC087";
        internal const string HCAC087M = "xxxxxxxxxxxx";
        internal const string HCAC088 = "HCAC088";
        internal const string HCAC088M = "xxxxxxxxxxxx";
        internal const string HCAC089 = "HCAC089";
        internal const string HCAC089M = "xxxxxxxxxxxx";
        internal const string HCAC090 = "HCAC090";
        internal const string HCAC090M = "xxxxxxxxxxxx";
        internal const string HCAC091 = "HCAC091";
        internal const string HCAC091M = "xxxxxxxxxxxx";
        internal const string HCAC092 = "HCAC092";
        internal const string HCAC092M = "xxxxxxxxxxxx";
        internal const string HCAC093 = "HCAC093";
        internal const string HCAC093M = "xxxxxxxxxxxx";
        internal const string HCAC094 = "HCAC094";
        internal const string HCAC094M = "xxxxxxxxxxxx";
        internal const string HCAC095 = "HCAC095";
        internal const string HCAC095M = "xxxxxxxxxxxx";
        internal const string HCAC096 = "HCAC096";
        internal const string HCAC096M = "xxxxxxxxxxxx";
        internal const string HCAC097 = "HCAC097";
        internal const string HCAC097M = "xxxxxxxxxxxx";
        internal const string HCAC098 = "HCAC098";
        internal const string HCAC098M = "xxxxxxxxxxxx";
        internal const string HCAC099 = "HCAC099";
        internal const string HCAC099M = "xxxxxxxxxxxx";
        internal const string HCAC100 = "HCAC100";
        internal const string HCAC100M = "xxxxxxxxxxxx";
    }
}
