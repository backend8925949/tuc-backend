//==================================================================================
// FileName: HCoreAuth.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================


using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using HCore.Data;
using HCore.Data.Models;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using HCore.Data.Logging;
using static HCore.Helper.HCoreConstant.Helpers;
using Z.EntityFramework.Plus;
using HCore.Helper.DataStore;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Newtonsoft.Json.Serialization;

namespace HCore.Helper
{
    public static class HCoreAuth
    {
        //public static object OThankUGateway { get; private set; }

        public static OAuth.Response Auth_OuterRequest(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            OUserReference _OUserReference = new OUserReference();
            //_OUserReference.DbContext = ;
            try
            {
                #region Declare
                string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();// HttpContext.Connection.RemoteIpAddress.ToString();
                OAuth.Response _OAuth = new OAuth.Response();
                #endregion
                #region Send Response
                _OAuth.Status = StatusSuccess;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Success, _OUserReference, "HCAK003", Resource.ResourceEn.HCAK003);
                _OAuth.UserReference = _OUserReference;
                return _OAuth;
                #endregion
            }
            catch (Exception _Exception)
            {
                _OUserReference.Data = RequestBody;
                HCoreHelper.LogException("Auth_OuterRequest", _Exception, _OUserReference);
                #region Start Response
                OAuth.Response _OAuth = new OAuth.Response();
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCAK006", Resource.ResourceEn.HCAK006);
                return _OAuth;
                #endregion
            }
        }
        public static OAuth.Response Auth_Notify(HttpRequest Request, string? RequestBody, string? TerminalId, DateTime RequestTime, string? ApiName)
        {
            string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();// HttpContext.Connection.RemoteIpAddress.ToString();
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            OUserReference _OUserReference = new OUserReference();
            _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
            _OUserReference.RequestIpAddress = IpAddress;
            _OUserReference.RequestTime = RequestTime;
            _OUserReference.Request = RequestBody;
            _OUserReference.Data = RequestBody;
            _OAuth.UserReference = _OUserReference;
            _OUserReference.Task = ApiName;
            _OUserReference.ApiId = HCoreDataStore.SystemApis.Where(x => x.SystemName == ApiName).Select(x => x.ReferenceId).FirstOrDefault();
            try
            {
                var _Headers = Request.Headers;
                string ApiKey = null;
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var PosDetails = _HCoreContext.TUCTerminal
                                            .Where(x => x.IdentificationNumber == TerminalId
                                                   && x.Provider.AccountTypeId == UserAccountType.PosAccount
                                                   && x.StatusId == HelperStatus.Default.Active
                                                   && x.Provider.StatusId == HelperStatus.Default.Active)
                                               .Select(x => new
                                               {
                                                   TerminalId = x.Id,
                                                   TerminalKey = x.Guid,
                                                   TerminalDisplayName = x.DisplayName,
                                                   PosId = x.ProviderId
                                               }).FromCache().FirstOrDefault();
                    if (PosDetails != null)
                    {
                        OHCDataStore.App KeyDetails = HCoreDataStore.SystemApps.FirstOrDefault(x => x.UserAccountId == PosDetails.PosId);
                        if (KeyDetails == null)
                        {

                            KeyDetails = _HCoreContext.HCCoreAppVersion.Where(x =>
                                                                          x.App.AccountId == PosDetails.PosId
                                                                          && x.App.StatusId == HelperStatus.Default.Active
                                                                          && x.StatusId == HelperStatus.Default.Active)
                                                      .Select(x => new OHCDataStore.App
                                                      {
                                                          AppId = x.App.Id,
                                                          AppKey = x.App.AppKey,
                                                          AppVersionId = x.Id,
                                                          AppVersionKey = x.ApiKey,
                                                          IpAddress = x.App.IpAddress,
                                                          LogRequest = x.App.AllowLogging,
                                                          OsId = 1,
                                                          OsName = "na",
                                                          CountryIsd = x.App.Account.Country.Isd,
                                                          CountryId = x.App.Account.CountryId,
                                                          CountryTimeZoneName = x.App.Account.Country.TimeZoneName,
                                                          UserAccountId = x.App.AccountId,
                                                          UserAccountKey = x.App.Account.Guid,
                                                          UserAccountCode = x.App.Account.AccountCode,
                                                          UserAccountTypeId = x.App.Account.AccountTypeId,
                                                          UserAccountDisplayName = x.App.Account.DisplayName,
                                                          UserAccountOwnerId = x.App.Account.Owner.Id,
                                                          UserAccountOwnerKey = x.App.Account.Owner.Guid,
                                                          UserAccountOwnerCode = x.App.Account.Owner.AccountCode,
                                                          UserAccountOwnerAccountTypeId = x.App.Account.Owner.AccountTypeId,
                                                          UserAccountOwnerDisplayName = x.App.Account.Owner.DisplayName,
                                                          CountryMobileNumberLength = x.App.Account.Country.MobileNumberLength,
                                                      }).FirstOrDefault();
                            if (KeyDetails != null)
                            {
                                HCoreDataStore.SystemApps.Add(KeyDetails);
                            }
                        }
                        if (KeyDetails != null)
                        {
                            if (!string.IsNullOrEmpty(KeyDetails.IpAddress) && KeyDetails.IpAddress != IpAddress)
                            {
                                //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_Ip_Error-HCAK005", IpAddress, RequestBody, null, _OUserReference);
                                _OAuth.Status = StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK004", Resource.ResourceEn.HCAK004);
                                return _OAuth;
                            }
                            else
                            {
                                _OUserReference.AccountId = (long)KeyDetails.UserAccountId;
                                _OUserReference.AccountKey = KeyDetails.UserAccountKey;
                                _OUserReference.AccountTypeId = (long)KeyDetails.UserAccountTypeId;
                                _OUserReference.DisplayName = KeyDetails.UserAccountDisplayName;
                                _OUserReference.AccountCode = KeyDetails.UserAccountCode;
                                _OUserReference.CountryIsd = KeyDetails.CountryIsd;
                                _OUserReference.CountryId = KeyDetails.CountryId;
                                _OUserReference.CountryTimeZone = KeyDetails.CountryTimeZoneName;
                                _OUserReference.CountryMobileNumberLength = KeyDetails.CountryMobileNumberLength;
                                _OUserReference.SystemCountry = (int)KeyDetails.CountryId;
                                _OUserReference.AccountOwnerKey = KeyDetails.UserAccountOwnerKey;
                                _OUserReference.AccountOwnerId = KeyDetails.UserAccountOwnerId;
                                _OUserReference.AccountOwnerDisplayName = KeyDetails.UserAccountOwnerDisplayName;
                                _OUserReference.AppVersionId = KeyDetails.AppVersionId;
                                _OUserReference.LogRequest = KeyDetails.LogRequest;
                                _OUserReference.ResponseData = null;
                                _OUserReference.AppId = KeyDetails.AppId;
                                _OUserReference.OsId = (long)KeyDetails.OsId;
                                _OUserReference.Data = RequestBody;
                                #region Send Response
                                _OAuth.Status = StatusSuccess;
                                _OAuth.UserReference = _OUserReference;
                                return _OAuth;
                                #endregion
                            }
                        }
                        else
                        {
                            //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_ERROR_HCAK005", ApiKey, RequestBody, null, _OUserReference);
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK005", Resource.ResourceEn.HCAK005);
                            return _OAuth;
                        }
                    }
                    else
                    {
                        //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_ERROR_HCAK005", ApiKey, RequestBody, null, _OUserReference);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK005", Resource.ResourceEn.HCAK005);
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                _OUserReference.Data = RequestBody;
                HCoreHelper.LogException("Auth_Key", _Exception, _OUserReference);
                #region Start Response
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK006", Resource.ResourceEn.HCAK006);
                return _OAuth;
                #endregion
            }
        }

        public static OAuth.Response AuthorizeBulkRewardEP(string appVersionKey, string appKey)
        {
            var result = new OAuth.Response();
            try
            {
                var appKeyDetails = HCoreDataStore.SystemApps.Where(x =>
                                                                      x.AppVersionKey == appVersionKey &&
                                                                     x.AppKey == appKey)
                                                 .Select(x => new
                                                 {
                                                     OsId = x.OsId,
                                                     AppId = x.AppId,
                                                     AppVersionId = x.AppVersionId,
                                                     UserAccountKey = x.UserAccountId,
                                                     IpAddress = x.IpAddress,
                                                     LogRequests = x.LogRequest,
                                                     AppStatusId = x.AppStatusId,
                                                     AppVersionStatusId = x.AppVersionStatusId,
                                                 }).FirstOrDefault();

                if (appKeyDetails != null)
                {
                    var _OUserReference = new OUserReference();
                    _OUserReference.CountryId = 1;
                    _OUserReference.CountryIso = "NG";
                    _OUserReference.CountryMobileNumberLength = 10;
                    _OUserReference.CountryIsd = "234";
                    _OUserReference.AppVersionId = appKeyDetails.AppVersionId;
                    _OUserReference.AppId = (long)appKeyDetails.AppId;
                    if (appKeyDetails.OsId != null)
                    {
                        _OUserReference.OsId = 102;
                    }
                    else
                    {
                        _OUserReference.OsId = 1;
                    }
                    result.Status = StatusSuccess;
                    result.UserReference = _OUserReference;
                }
                else
                {
                    result.Status = StatusError;
                    result.UserResponse = HCoreHelper.SendResponse(result.UserReference, ResponseStatus.Error, null, "HCA110", "API keys are invalid");
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("AuthorizeBulkRewardEP", ex, null);
                result.Status = StatusError;
                result.UserResponse = HCoreHelper.SendResponse(result.UserReference, ResponseStatus.Error, null, "HCA110", "System error while validating API keys");
            }

            return result;
        }

        public static OAuth.Response Auth_Key(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName, string? ResponseType)
        {
            string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            OUserReference _OUserReference = new OUserReference();
            _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
            _OUserReference.RequestIpAddress = IpAddress;
            _OUserReference.RequestTime = RequestTime;
            _OUserReference.Request = RequestBody;
            _OUserReference.ResponseType = ResponseType;
            _OUserReference.ResponseData = null;
            //_OUserReference.Data = RequestBody;
            _OAuth.UserReference = _OUserReference;
            _OUserReference.Task = ApiName;
            _OUserReference.ApiId = HCoreDataStore.SystemApis.Where(x => x.SystemName == ApiName).Select(x => x.ReferenceId).FirstOrDefault();
            try
            {
                string Temp_Header_ApiKey = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                string ApiKey = null;
                if (string.IsNullOrEmpty(Temp_Header_ApiKey))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK001", Resource.ResourceEn.HCAK001);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                else
                {
                    try
                    {
                        Temp_Header_ApiKey = Temp_Header_ApiKey.Replace("Bearer", "").Trim();
                        ApiKey = HCoreEncrypt.DecodeText(Temp_Header_ApiKey);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Key_Request_ApiKeyError : " + Temp_Header_ApiKey, _Exception, _OUserReference);
                        #region Set response
                        _OAuth.UserReference = null;
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK002", Resource.ResourceEn.HCAK002);
                        return _OAuth;
                        #endregion
                    }
                    OHCDataStore.App? KeyDetails = HCoreDataStore.SystemApps.FirstOrDefault(x => x.AppVersionKey == ApiKey);
                    if (KeyDetails == null)
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            KeyDetails = _HCoreContext.HCCoreAppVersion.Where(x =>
                                                                          x.ApiKey == ApiKey
                                                                              && x.App.Account.StatusId == HelperStatus.Default.Active
                                                                          && x.App.StatusId == HelperStatus.Default.Active
                                                                          && x.StatusId == HelperStatus.Default.Active)
                                                      .Select(x => new OHCDataStore.App
                                                      {
                                                          AppId = x.App.Id,
                                                          AppKey = x.App.AppKey,
                                                          AppVersionId = x.Id,
                                                          AppVersionKey = x.ApiKey,
                                                          IpAddress = x.App.IpAddress,
                                                          LogRequest = x.App.AllowLogging,
                                                          OsId = 1,
                                                          OsName = "na",
                                                          CountryIsd = x.App.Account.Country.Isd,
                                                          CountryId = x.App.Account.CountryId,
                                                          CountryTimeZoneName = x.App.Account.Country.TimeZoneName,
                                                          CountryMobileNumberLength = x.App.Account.Country.MobileNumberLength,
                                                          UserAccountId = x.App.AccountId,
                                                          UserAccountKey = x.App.Account.Guid,
                                                          UserAccountCode = x.App.Account.AccountCode,
                                                          UserAccountTypeId = x.App.Account.AccountTypeId,
                                                          UserAccountDisplayName = x.App.Account.DisplayName,
                                                          UserAccountOwnerId = x.App.Account.Owner.Id,
                                                          UserAccountOwnerKey = x.App.Account.Owner.Guid,
                                                          UserAccountOwnerCode = x.App.Account.Owner.AccountCode,
                                                          UserAccountOwnerAccountTypeId = x.App.Account.Owner.AccountTypeId,
                                                          UserAccountOwnerDisplayName = x.App.Account.Owner.DisplayName,

                                                      }).FirstOrDefault();
                            if (KeyDetails != null)
                            {
                                HCoreDataStore.SystemApps.Add(KeyDetails);
                            }
                            _HCoreContext.Dispose();
                        }
                    }
                    if (KeyDetails != null)
                    {
                        //if (!string.IsNullOrEmpty(KeyDetails.IpAddress))
                        //{
                        //    if (KeyDetails.IpAddress.Contains(";"))
                        //    {
                        //        string[] ipList = KeyDetails.IpAddress.Split(";");
                        //        if (!ipList.Any(x => x == IpAddress))
                        //        {
                        //            _OAuth.Status = StatusError;
                        //            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK004", Resource.ResourceEn.HCAK004);
                        //            return _OAuth;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (KeyDetails.IpAddress != IpAddress)
                        //        {
                        //            _OAuth.Status = StatusError;
                        //            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK004", Resource.ResourceEn.HCAK004);
                        //            return _OAuth;
                        //        }
                        //    }
                        //}
                        _OUserReference.AccountId = (long)KeyDetails.UserAccountId;
                        _OUserReference.AccountKey = KeyDetails.UserAccountKey;
                        _OUserReference.AccountTypeId = (long)KeyDetails.UserAccountTypeId;
                        _OUserReference.DisplayName = KeyDetails.UserAccountDisplayName;
                        _OUserReference.AccountCode = KeyDetails.UserAccountCode;
                        _OUserReference.CountryIsd = KeyDetails.CountryIsd;
                        _OUserReference.CountryId = KeyDetails.CountryId;
                        _OUserReference.CountryTimeZone = KeyDetails.CountryTimeZoneName;
                        _OUserReference.CountryMobileNumberLength = KeyDetails.CountryMobileNumberLength;
                        _OUserReference.SystemCountry = (int)KeyDetails.CountryId;
                        _OUserReference.AccountOwnerKey = KeyDetails.UserAccountOwnerKey;
                        _OUserReference.AccountOwnerId = KeyDetails.UserAccountOwnerId;
                        _OUserReference.AccountOwnerDisplayName = KeyDetails.UserAccountOwnerDisplayName;
                        _OUserReference.AppVersionId = KeyDetails.AppVersionId;
                        _OUserReference.LogRequest = KeyDetails.LogRequest;
                        _OUserReference.ResponseData = null;
                        _OUserReference.AppId = KeyDetails.AppId;
                        if (KeyDetails.OsId != null)
                        {
                            _OUserReference.OsId = (long)KeyDetails.OsId;
                        }
                        else
                        {
                            _OUserReference.OsId = HCoreConstant.HelperType.Os;
                        }
                        #region Send Response
                        _OAuth.Status = StatusSuccess;
                        _OAuth.UserReference = _OUserReference;
                        return _OAuth;
                        #endregion
                        //if (!string.IsNullOrEmpty(KeyDetails.IpAddress) && KeyDetails.IpAddress != IpAddress)
                        //{
                        //    //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_Ip_Error-HCAK005", IpAddress, RequestBody, null, _OUserReference);
                        //    _OAuth.Status = StatusError;
                        //    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK004", Resource.ResourceEn.HCAK004);
                        //    return _OAuth;
                        //}
                        //else
                        //{
                        //}
                    }
                    else
                    {
                        //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_ERROR_HCAK005", ApiKey, RequestBody, null, _OUserReference);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK005", Resource.ResourceEn.HCAK005);
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_Key", _Exception, _OUserReference);
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK006", Resource.ResourceEn.HCAK006);
                return _OAuth;
            }
        }
        public static OAuth.Response Auth_Key_Web_MadDeals(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            OUserReference _OUserReference = new OUserReference();
            _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
            _OUserReference.RequestIpAddress = IpAddress;
            _OUserReference.RequestTime = RequestTime;
            _OUserReference.Request = RequestBody;
            _OUserReference.ResponseType = "json";
            _OUserReference.ResponseData = null;
            //_OUserReference.Data = RequestBody;
            _OAuth.UserReference = _OUserReference;
            _OUserReference.Task = ApiName;
            _OUserReference.ApiId = HCoreDataStore.SystemApis.Where(x => x.SystemName == ApiName).Select(x => x.ReferenceId).FirstOrDefault();
            try
            {
                string Temp_Header_ApiKey = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                string ApiKey = null;
                if (string.IsNullOrEmpty(Temp_Header_ApiKey))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK001", Resource.ResourceEn.HCAK001);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                else
                {
                    try
                    {
                        Temp_Header_ApiKey = Temp_Header_ApiKey.Replace("Bearer", "").Trim();
                        ApiKey = HCoreEncrypt.DecodeText(Temp_Header_ApiKey);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Key_Request_ApiKeyError : " + Temp_Header_ApiKey, _Exception, _OUserReference);
                        #region Set response
                        _OAuth.UserReference = null;
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK002", Resource.ResourceEn.HCAK002);
                        return _OAuth;
                        #endregion
                    }
                    OHCDataStore.App? KeyDetails = HCoreDataStore.SystemApps.FirstOrDefault(x => x.AppVersionKey == ApiKey);
                    if (KeyDetails == null)
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            KeyDetails = _HCoreContext.HCCoreAppVersion.Where(x =>
                                                                          x.ApiKey == ApiKey
                                                                          && x.App.Account.StatusId == HelperStatus.Default.Active
                                                                          && x.App.StatusId == HelperStatus.Default.Active
                                                                          && x.StatusId == HelperStatus.Default.Active)
                                                      .Select(x => new OHCDataStore.App
                                                      {
                                                          AppId = x.App.Id,
                                                          AppKey = x.App.AppKey,
                                                          AppVersionId = x.Id,
                                                          AppVersionKey = x.ApiKey,
                                                          IpAddress = x.App.IpAddress,
                                                          LogRequest = x.App.AllowLogging,
                                                          OsId = 1,
                                                          OsName = "na",
                                                          CountryIsd = x.App.Account.Country.Isd,
                                                          CountryId = x.App.Account.CountryId,
                                                          CountryTimeZoneName = x.App.Account.Country.TimeZoneName,
                                                          UserAccountId = x.App.AccountId,
                                                          UserAccountKey = x.App.Account.Guid,
                                                          UserAccountCode = x.App.Account.AccountCode,
                                                          UserAccountTypeId = x.App.Account.AccountTypeId,
                                                          UserAccountDisplayName = x.App.Account.DisplayName,
                                                          UserAccountOwnerId = x.App.Account.Owner.Id,
                                                          UserAccountOwnerKey = x.App.Account.Owner.Guid,
                                                          UserAccountOwnerCode = x.App.Account.Owner.AccountCode,
                                                          UserAccountOwnerAccountTypeId = x.App.Account.Owner.AccountTypeId,
                                                          UserAccountOwnerDisplayName = x.App.Account.Owner.DisplayName
                                                      }).FirstOrDefault();
                            if (KeyDetails != null)
                            {
                                HCoreDataStore.SystemApps.Add(KeyDetails);
                            }
                            _HCoreContext.Dispose();
                        }
                    }
                    if (KeyDetails != null)
                    {
                        if (!string.IsNullOrEmpty(KeyDetails.IpAddress) && KeyDetails.IpAddress != IpAddress)
                        {
                            //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_Ip_Error-HCAK005", IpAddress, RequestBody, null, _OUserReference);
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK004", Resource.ResourceEn.HCAK004);
                            return _OAuth;
                        }
                        else
                        {

                            string HAuthKey = Request.Headers.Where(x => x.Key == "ask").FirstOrDefault().Value;
                            if (!string.IsNullOrEmpty(HAuthKey))
                            {
                                string SessionKeyDetails = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(HAuthKey));
                                string[] Sd = SessionKeyDetails.Split("|");
                                long SessionId = Convert.ToInt64(Sd[0]);
                                string SessionKey = Sd[1];
                                using (HCoreContext _HCoreContext = new HCoreContext())
                                {
                                    var SessionDetails = _HCoreContext.HCUAccountSession
                                        .Where(x => x.Id == SessionId && x.Guid == SessionKey)
                                        .Select(x => new
                                        {
                                            StatusId = x.StatusId,
                                            AccountId = x.AccountId,
                                            AccountKey = x.Account.Guid,
                                            AccountTypeId = x.Account.AccountTypeId,
                                            AccountDisplayName = x.Account.DisplayName,
                                            AccountCode = x.Account.Guid,
                                            CountryId = x.Account.CountryId,
                                            CountryIsd = x.Account.Country.Isd,
                                            CountryTimeZoneName = x.Account.Country.TimeZoneName,
                                            AccountOwnerKey = x.Account.Owner.Guid,
                                            AccountOwnerId = x.Account.OwnerId,
                                            AccountOwnerDisplayName = x.Account.DisplayName,
                                        }).FirstOrDefault();
                                    if (SessionDetails != null)
                                    {
                                        if (SessionDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            _OUserReference.AccountId = (long)SessionDetails.AccountId;
                                            _OUserReference.AccountKey = SessionDetails.AccountKey;
                                            _OUserReference.AccountTypeId = (long)SessionDetails.AccountTypeId;
                                            _OUserReference.DisplayName = SessionDetails.AccountDisplayName;
                                            _OUserReference.AccountCode = SessionDetails.AccountCode;
                                            _OUserReference.CountryIsd = SessionDetails.CountryIsd;
                                            _OUserReference.CountryId = SessionDetails.CountryId;
                                            _OUserReference.CountryTimeZone = SessionDetails.CountryTimeZoneName;
                                            _OUserReference.SystemCountry = (int)SessionDetails.CountryId;
                                            _OUserReference.AccountOwnerKey = SessionDetails.AccountOwnerKey;
                                            _OUserReference.AccountOwnerId = SessionDetails.AccountOwnerId;
                                            _OUserReference.AccountOwnerDisplayName = SessionDetails.AccountOwnerDisplayName;
                                            _OUserReference.AppVersionId = KeyDetails.AppVersionId;
                                            _OUserReference.LogRequest = KeyDetails.LogRequest;
                                            _OUserReference.ResponseData = null;
                                            _OUserReference.AppId = KeyDetails.AppId;
                                            if (KeyDetails.OsId != null)
                                            {
                                                _OUserReference.OsId = (long)KeyDetails.OsId;
                                            }
                                            else
                                            {
                                                _OUserReference.OsId = HCoreConstant.HelperType.Os;
                                            }
                                            #region Send Response
                                            _OAuth.AccountId = (long)SessionDetails.AccountId;
                                            _OAuth.Status = StatusSuccess;
                                            _OAuth.UserReference = _OUserReference;
                                            return _OAuth;
                                            #endregion
                                        }
                                        else
                                        {
                                            _OAuth.Status = StatusError;
                                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", Resource.ResourceEn.HCA132);
                                            return _OAuth;
                                        }
                                    }
                                    else
                                    {
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA133", Resource.ResourceEn.HCA133);
                                        return _OAuth;
                                    }
                                }

                            }
                            else
                            {
                                _OUserReference.AccountId = (long)KeyDetails.UserAccountId;
                                _OUserReference.AccountKey = KeyDetails.UserAccountKey;
                                _OUserReference.AccountTypeId = (long)KeyDetails.UserAccountTypeId;
                                _OUserReference.DisplayName = KeyDetails.UserAccountDisplayName;
                                _OUserReference.AccountCode = KeyDetails.UserAccountCode;
                                _OUserReference.CountryIsd = KeyDetails.CountryIsd;
                                _OUserReference.CountryId = KeyDetails.CountryId;
                                _OUserReference.CountryTimeZone = KeyDetails.CountryTimeZoneName;
                                _OUserReference.SystemCountry = (int)KeyDetails.CountryId;
                                _OUserReference.AccountOwnerKey = KeyDetails.UserAccountOwnerKey;
                                _OUserReference.AccountOwnerId = KeyDetails.UserAccountOwnerId;
                                _OUserReference.AccountOwnerDisplayName = KeyDetails.UserAccountOwnerDisplayName;
                                _OUserReference.AppVersionId = KeyDetails.AppVersionId;
                                _OUserReference.LogRequest = KeyDetails.LogRequest;
                                _OUserReference.ResponseData = null;
                                _OUserReference.AppId = KeyDetails.AppId;
                                if (KeyDetails.OsId != null)
                                {
                                    _OUserReference.OsId = (long)KeyDetails.OsId;
                                }
                                else
                                {
                                    _OUserReference.OsId = HCoreConstant.HelperType.Os;
                                }
                                #region Send Response
                                _OAuth.Status = StatusSuccess;
                                _OAuth.UserReference = _OUserReference;
                                return _OAuth;
                                #endregion
                            }


                        }
                    }
                    else
                    {
                        //HCoreHelper.LogData(HCoreConstant.LogType.Log, "Auth_Key_ERROR_HCAK005", ApiKey, RequestBody, null, _OUserReference);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK005", Resource.ResourceEn.HCAK005);
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_Key", _Exception, _OUserReference);
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK006", Resource.ResourceEn.HCAK006);
                return _OAuth;
            }
        }

        public static OAuth.Response Auth_App(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();// HttpContext.Connection.RemoteIpAddress.ToString();
            OUserReference _OUserReference = new OUserReference();
            _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
            _OUserReference.RequestIpAddress = IpAddress;
            _OUserReference.RequestTime = RequestTime;
            _OUserReference.Request = RequestBody;
            //_OUserReference.DbContext = ;
            _OUserReference.Data = RequestBody;
            _OUserReference.Task = ApiName;
            _OUserReference.ApiId = HCoreDataStore.SystemApis.Where(x => x.SystemName == ApiName).Select(x => x.ReferenceId).FirstOrDefault();
            try
            {
                #region Load Headers
                var _Headers = Request.Headers;
                string Temp_Header_hctk = null;
                string Temp_Header_hcak = null;
                string Temp_Header_hcavk = null;
                string Temp_Header_hcudk = null;
                string Temp_Header_hcudlt = null;
                string Temp_Header_hcudln = null;
                string Header_Task = null;
                string Header_AppKey = null;
                string Header_AppVersionKey = null;
                string Header_DeviceKey = null;
                double Header_Latitude = 0;
                double Header_Longitude = 0;
                #endregion
                #region Filter Headers
                Temp_Header_hctk = _Headers.Where(x => x.Key == "hctk").FirstOrDefault().Value; //  Task  
                Temp_Header_hcak = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value; //  App Key  
                Temp_Header_hcavk = _Headers.Where(x => x.Key == "hcavk").FirstOrDefault().Value; //  App Version Key  
                if (string.IsNullOrEmpty(Temp_Header_hcavk))
                {
                    Temp_Header_hcavk = "MjQ0Nzgw"; //  App Version Key  
                }
                Temp_Header_hcudk = _Headers.Where(x => x.Key == "hcudk").FirstOrDefault().Value; // User Device Key
                Temp_Header_hcudlt = _Headers.Where(x => x.Key == "hcudlt").FirstOrDefault().Value; // User Device Latitude
                Temp_Header_hcudln = _Headers.Where(x => x.Key == "hcudln").FirstOrDefault().Value; // User Device Longitude
                #endregion
                #region Validate Headers
                #region Decrypt Latitude
                if (string.IsNullOrEmpty(Temp_Header_hcudlt))
                {
                    try
                    {
                        Header_Latitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_hcudlt));
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_Request_DeviceLatitude" + Temp_Header_hcudlt, _Exception, null);
                        #endregion
                        Header_Latitude = 0;
                    }
                }
                #endregion
                #region Decrypt Longitude
                if (string.IsNullOrEmpty(Temp_Header_hcudln))
                {
                    try
                    {
                        Header_Longitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_hcudln));
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_Request_DeviceLongitude" + Temp_Header_hcudln, _Exception, null);
                        #endregion
                        Header_Longitude = 0;
                    }
                }
                #endregion
                if (string.IsNullOrEmpty(Temp_Header_hcak))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA101", Resource.ResourceEn.HCA101);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                else if (string.IsNullOrEmpty(Temp_Header_hcavk))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA102", Resource.ResourceEn.HCA102);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                else if (string.IsNullOrEmpty(Temp_Header_hcudk))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA103", Resource.ResourceEn.HCA103);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                else if (string.IsNullOrEmpty(Temp_Header_hctk))
                {
                    #region Set response
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA104", Resource.ResourceEn.HCA104);
                    #endregion
                    #region Send Response
                    return _OAuth;
                    #endregion
                }
                #endregion
                else
                {
                    #region Decrypt Task
                    try
                    {
                        Header_Task = HCoreEncrypt.DecodeText(Temp_Header_hctk);
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_Request_Task" + Temp_Header_hctk, _Exception, null);
                        #endregion
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA105", Resource.ResourceEn.HCA105);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    #endregion
                    #region Decrypt App Key
                    try
                    {
                        Header_AppKey = HCoreEncrypt.DecodeText(Temp_Header_hcak);
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_App_AppKey " + Temp_Header_hcak, _Exception, null);
                        #endregion
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA106", Resource.ResourceEn.HCA106);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    #endregion
                    #region Decrypt App Version Key
                    try
                    {
                        Header_AppVersionKey = HCoreEncrypt.DecodeText(Temp_Header_hcavk);
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_App_AppVersionKey " + Temp_Header_hcavk, _Exception, null);
                        #endregion
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA107", Resource.ResourceEn.HCA107);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    #endregion
                    #region Decrypt Device Key
                    try
                    {
                        Header_DeviceKey = HCoreEncrypt.DecodeText(Temp_Header_hcudk);
                    }
                    catch (Exception _Exception)
                    {
                        #region Log Bug
                        HCoreHelper.LogException("Auth_Request_DeviceKey" + Temp_Header_hcudk, _Exception, null);
                        #endregion
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA108", Resource.ResourceEn.HCA108);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    #endregion
                    #region Validate Api Key
                    var AppKeyDetails = HCoreDataStore.SystemApps.Where(x =>
                                                                      x.AppVersionKey == Header_AppVersionKey &&
                                                                     x.AppKey == Header_AppKey)
                                                 .Select(x => new
                                                 {
                                                     OsId = x.OsId,
                                                     AppId = x.AppId,
                                                     AppVersionId = x.AppVersionId,
                                                     UserAccountKey = x.UserAccountId,
                                                     IpAddress = x.IpAddress,
                                                     LogRequests = x.LogRequest,
                                                     AppStatusId = x.AppStatusId,
                                                     AppVersionStatusId = x.AppVersionStatusId,
                                                 }).FirstOrDefault();
                    if (Header_AppVersionKey == "244780")
                    {
                        AppKeyDetails = HCoreDataStore.SystemApps
                                                 .Select(x => new
                                                 {
                                                     OsId = x.OsId,
                                                     AppId = x.AppId,
                                                     AppVersionId = x.AppVersionId,
                                                     UserAccountKey = x.UserAccountId,
                                                     IpAddress = x.IpAddress,
                                                     LogRequests = x.LogRequest,
                                                     AppStatusId = x.AppStatusId,
                                                     AppVersionStatusId = x.AppVersionStatusId,
                                                 }).FirstOrDefault();
                    }
                    if (AppKeyDetails != null)
                    {
                        //if (AppKeyDetails.AppStatusId != HelperStatus.Default.Active)
                        //{
                        //    #region Send Response
                        //    _OAuth.Status = StatusError;
                        //    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA111", "Please update your app to latest version.");
                        //    #endregion
                        //    #region Send Response
                        //    return _OAuth;
                        //    #endregion
                        //}
                        //if (AppKeyDetails.AppVersionStatusId != HelperStatus.Default.Active)
                        //{
                        //    #region Send Response
                        //    _OAuth.Status = StatusError;
                        //    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA112", "Please update your app to latest version.");
                        //    #endregion
                        //    #region Send Response
                        //    return _OAuth;
                        //    #endregion
                        //}
                        _OUserReference.CountryId = 1;
                        _OUserReference.CountryIso = "NG";
                        _OUserReference.CountryMobileNumberLength = 10;
                        _OUserReference.CountryIsd = "234";
                        _OUserReference.AppVersionId = AppKeyDetails.AppVersionId;
                        _OUserReference.AppId = (long)AppKeyDetails.AppId;
                        if (AppKeyDetails.OsId != null)
                        {
                            _OUserReference.OsId = 102;
                        }
                        else
                        {

                            _OUserReference.OsId = 1;
                        }
                        _OUserReference.DeviceSerialNumber = Header_DeviceKey;
                        _OUserReference.LogRequest = AppKeyDetails.LogRequests;
                        #region Validate Api
                        #region Send Response
                        _OAuth.Status = StatusSuccess;
                        _OAuth.UserReference = _OUserReference;
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                        #endregion
                    }
                    else
                    {
                        //HCoreHelper.LogData(HCoreConstant.LogType.Log, "HA110", "APPS : " + HCoreDataStore.SystemApps.Count, Header_AppVersionKey + "|" + Header_AppKey, RequestBody);
                        #region Send Response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA110", "System not available at the moment. Please try after some time");
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    //}
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("xxx", _Exception, null);
                #endregion
                #region Start Response
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HC500", Resource.ResourceEn.HC500);
                return _OAuth;
                #endregion
            }
        }
        public static OAuth.Response Auth_Request(HttpRequest Request, OAuth.Response _Auth_App_Response)
        {
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            _OAuth.UserReference = _Auth_App_Response.UserReference;
            try
            {
                #region Validate App
                if (_Auth_App_Response.Status == StatusSuccess)
                {
                    #region Load Headers
                    var _Headers = Request.Headers;
                    string Temp_Header_hcupk = null;
                    string Temp_Header_hcudlt = null;
                    string Temp_Header_hcudln = null;
                    string Temp_Header_hcuata = null;
                    string Temp_Header_hcuatp = null;
                    string Header_AccessKey = null;
                    string Header_PublicKey = null;
                    double Header_Latitude = 0;
                    double Header_Longitude = 0;
                    string Header_AccessPin = null;
                    string Header_Password = null;

                    #region  Access Key Parameters
                    long UserId = 0;
                    long UserAccountId = 0;
                    long UserDeviceId = 0;
                    long UserSessionId = 0;
                    #endregion

                    #endregion
                    #region Filter Headers
                    Header_AccessKey = _Headers.Where(x => x.Key == "hcuak").FirstOrDefault().Value; // User Access Key
                    Temp_Header_hcupk = _Headers.Where(x => x.Key == "hcupk").FirstOrDefault().Value; // User Public Key
                    Temp_Header_hcudlt = _Headers.Where(x => x.Key == "hcudlt").FirstOrDefault().Value; // User Device Latitude
                    Temp_Header_hcudln = _Headers.Where(x => x.Key == "hcudln").FirstOrDefault().Value; // User Device Longitude
                    Temp_Header_hcuata = _Headers.Where(x => x.Key == "hcuata").FirstOrDefault().Value; // User Access Pin 
                    Temp_Header_hcuatp = _Headers.Where(x => x.Key == "hcuatp").FirstOrDefault().Value; // User Passowrd
                    #endregion
                    #region Validate Headers
                    if (string.IsNullOrEmpty(Header_AccessKey))
                    {
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA111", Resource.ResourceEn.HCA111);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    else if (string.IsNullOrEmpty(Temp_Header_hcupk))
                    {
                        #region Set response
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA112", Resource.ResourceEn.HCA112);
                        #endregion
                        #region Send Response
                        return _OAuth;
                        #endregion
                    }
                    #endregion
                    else
                    {
                        #region Decrypt Public Key
                        try
                        {
                            Header_PublicKey = HCoreEncrypt.DecodeText(Temp_Header_hcupk);
                        }
                        catch (Exception _Exception)
                        {
                            #region Log Bug
                            HCoreHelper.LogException("Auth_Request_PublicKey" + Temp_Header_hcupk, _Exception, null);
                            #endregion
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA113", Resource.ResourceEn.HCA113);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        #endregion
                        #region Decrypt Latitude
                        if (!string.IsNullOrEmpty(Temp_Header_hcudlt))
                        {
                            try
                            {
                                Header_Latitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_hcudlt));
                            }
                            catch (Exception _Exception)
                            {
                                #region Log Bug
                                HCoreHelper.LogException("Auth_Request_DeviceLatitude" + Temp_Header_hcudlt, _Exception, null);
                                #endregion
                                Header_Latitude = 0;
                            }
                        }
                        #endregion
                        #region Decrypt Longitude
                        if (!string.IsNullOrEmpty(Temp_Header_hcudln))
                        {
                            try
                            {
                                Header_Longitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_hcudln));
                            }
                            catch (Exception _Exception)
                            {
                                #region Log Bug
                                HCoreHelper.LogException("Auth_Request_DeviceLongitude" + Temp_Header_hcudln, _Exception, null);
                                #endregion
                                Header_Latitude = 0;

                            }
                        }
                        #endregion
                        #region Decode Access Pin
                        if (!string.IsNullOrEmpty(Temp_Header_hcuata))
                        {
                            #region Decrypt Device Key
                            try
                            {
                                Header_AccessPin = HCoreEncrypt.DecodeText(Temp_Header_hcuata);
                            }
                            catch (Exception _Exception)
                            {
                                #region Log Bug
                                HCoreHelper.LogException("Auth_Request_AccessPin" + Temp_Header_hcuata, _Exception, null);
                                #endregion
                                #region Set response
                                _OAuth.Status = StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA114", Resource.ResourceEn.HCA114);
                                #endregion
                                #region Send Response
                                return _OAuth;
                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                        #region Decode Password
                        if (!string.IsNullOrEmpty(Temp_Header_hcuatp))
                        {
                            #region Decrypt Device Key
                            try
                            {
                                Header_Password = HCoreEncrypt.DecodeText(Temp_Header_hcuatp);
                            }
                            catch (Exception _Exception)
                            {
                                #region Log Bug
                                HCoreHelper.LogException("Auth_Request_Password" + Temp_Header_hcuatp, _Exception, null);
                                #endregion
                                #region Set response
                                _OAuth.Status = StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA115", Resource.ResourceEn.HCA115);
                                #endregion
                                #region Send Response
                                return _OAuth;
                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                        #region Decrypt Access Key
                        try
                        {
                            string User_AccessKey = HCoreEncrypt.DecryptHash(Header_AccessKey);
                            string[] Auth_Header_User_AccessKey = User_AccessKey.Split('|');
                            if (!string.IsNullOrEmpty(Auth_Header_User_AccessKey[4]))
                            {
                                UserId = Convert.ToInt64(Auth_Header_User_AccessKey[4]);
                            }
                            if (!string.IsNullOrEmpty(Auth_Header_User_AccessKey[5]))
                            {
                                UserAccountId = Convert.ToInt64(Auth_Header_User_AccessKey[5]);
                            }
                            if (!string.IsNullOrEmpty(Auth_Header_User_AccessKey[6]))
                            {
                                UserDeviceId = Convert.ToInt64(Auth_Header_User_AccessKey[6]);
                            }
                            if (!string.IsNullOrEmpty(Auth_Header_User_AccessKey[7]))
                            {
                                UserSessionId = Convert.ToInt64(Auth_Header_User_AccessKey[7]);
                            }
                        }
                        catch (Exception _Exception)
                        {
                            #region Log Bug
                            HCoreHelper.LogException("Auth_Request_AccessKey" + Header_AccessKey, _Exception, null);
                            #endregion
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA116", Resource.ResourceEn.HCA116);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        #endregion
                        if (UserId == 0)
                        {
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA117", Resource.ResourceEn.HCA117);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        else if (UserAccountId == 0)
                        {
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA118", Resource.ResourceEn.HCA118);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        else if (UserSessionId == 0)
                        {
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA120", Resource.ResourceEn.HCA120);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        else
                        {
                            #region Auth Req
                            using (HCoreContext _HCoreContext = new HCoreContext())
                            {
                                #region Get User Session Details
                                var _UserSessionDetails = (from n in _HCoreContext.HCUAccountSession
                                                           where n.Id == UserSessionId && n.AccountId == UserAccountId && n.StatusId == HelperStatus.Default.Active && n.LogoutDate == null && n.Account.StatusId == HelperStatus.Default.Active
                                                           select new
                                                           {
                                                               ReferenceId = n.Id,
                                                               ReferenceKey = n.Guid,
                                                               //ActivityDate = n.LastActivityDate,
                                                               UserAccountId = n.AccountId,
                                                               UserDeviceId = n.DeviceId,
                                                               UserDeviceSerialNumber = n.Device.SerialNumber,
                                                               UserDeviceKey = n.Device.Guid,
                                                               LoginDate = n.LoginDate,
                                                               UserDeviceStatus = n.Device.StatusId,
                                                               AccountTypeId = n.Account.AccountTypeId,
                                                           }).FirstOrDefault();
                                if (_UserSessionDetails != null)
                                {
                                    #region Validate Public Key
                                    if (_UserSessionDetails.ReferenceKey == Header_PublicKey || Header_PublicKey == "244780")
                                    {
                                        #region Validate Device Serial Number
                                        if (_UserSessionDetails.UserDeviceSerialNumber == _Auth_App_Response.UserReference.DeviceSerialNumber)
                                        {
                                            #region Validate User Id And User Id
                                            if (_UserSessionDetails.UserDeviceStatus == HelperStatus.Default.Active)
                                            {
                                                if (_UserSessionDetails.AccountTypeId == UserAccountType.Admin)
                                                {
                                                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime().AddHours(-24);
                                                    if (_UserSessionDetails.LoginDate < CurrentTime)
                                                    {
                                                        #region Set response
                                                        _OAuth.Status = StatusError;
                                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA124", Resource.ResourceEn.HCA124);
                                                        #endregion
                                                        #region Send Response
                                                        return _OAuth;
                                                        #endregion
                                                    }
                                                }
                                                #region User details 
                                                var _UserDetails = (from n in _HCoreContext.HCUAccount
                                                                    where n.UserId == UserId && n.Id == _UserSessionDetails.UserAccountId && n.Id == UserAccountId
                                                                    select new
                                                                    {
                                                                        UserAccountId = n.Id,
                                                                        UserAccountKey = n.Guid,
                                                                        AccountType = n.AccountType.Id,
                                                                        AccountTypeName = n.AccountType.Name,
                                                                        AccountTypeSystemName = n.AccountType.SystemName,
                                                                        OwnerId = n.OwnerId,
                                                                        OwnerKey = n.Owner.Guid,
                                                                        OwnerDisplayName = n.Owner.DisplayName,
                                                                        DisplayName = n.DisplayName,
                                                                        UserAccountCode = n.AccountCode,
                                                                        //AccessPin = n.AccessPin,
                                                                        Status = n.StatusId,
                                                                        UserName = n.User.Username,
                                                                        //Password = n.User.Password,
                                                                        Name = n.Name,
                                                                        EmailAddress = n.EmailAddress,
                                                                        ContactNumber = n.ContactNumber,
                                                                        CountryId = n.CountryId,
                                                                        CreateDate = n.CreateDate,
                                                                        UserStatus = n.StatusId,
                                                                    }).FromCache().FirstOrDefault();
                                                if (_UserDetails != null)
                                                {
                                                    if (_UserDetails.UserStatus == HelperStatus.Default.Active && _UserDetails.Status == HelperStatus.Default.Active)
                                                    {

                                                        #region Get role details
                                                        int AccessCount = 1;
                                                        #region Validate Access
                                                        if (AccessCount > 0)
                                                        {
                                                            if (!string.IsNullOrEmpty(Header_Password))
                                                            {
                                                                string Password = _HCoreContext.HCUAccount.Where(x => x.Id == _UserDetails.UserAccountId).Select(x => x.User.Password).FirstOrDefault();
                                                                string TPassword = HCoreEncrypt.DecryptHash(Password);
                                                                if (Header_Password != TPassword)
                                                                {
                                                                    _OAuth.Status = StatusError;
                                                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA128", Resource.ResourceEn.HCA128);
                                                                    return _OAuth;
                                                                }
                                                            }
                                                            if (!string.IsNullOrEmpty(Header_AccessPin))
                                                            {

                                                                string AccessPin = _HCoreContext.HCUAccount.Where(x => x.Id == _UserDetails.UserAccountId).Select(x => x.AccessPin).FirstOrDefault();
                                                                if (string.IsNullOrEmpty(AccessPin))
                                                                {
                                                                    #region Set response
                                                                    _OAuth.Status = StatusError;
                                                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA129", Resource.ResourceEn.HCA129);
                                                                    #endregion
                                                                    #region Send Response
                                                                    return _OAuth;
                                                                    #endregion
                                                                }
                                                                string TAccessPin = HCoreEncrypt.DecryptHash(AccessPin);
                                                                if (Header_AccessPin != TAccessPin)
                                                                {
                                                                    #region Set response
                                                                    _OAuth.Status = StatusError;
                                                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA130", Resource.ResourceEn.HCA130);
                                                                    #endregion
                                                                    #region Send Response
                                                                    return _OAuth;
                                                                    #endregion
                                                                }
                                                            }
                                                            #region Save Api Usage
                                                            #endregion
                                                            #region User Reference
                                                            #region Account Type
                                                            _OAuth.UserReference.AccountTypeId = _UserDetails.AccountType;
                                                            _OAuth.UserReference.AccountTypeName = _UserDetails.AccountTypeName;
                                                            _OAuth.UserReference.AccountTypeSystemName = _UserDetails.AccountTypeSystemName;
                                                            #endregion
                                                            #region Account
                                                            _OAuth.UserReference.AccountId = _UserDetails.UserAccountId;
                                                            _OAuth.UserReference.AccountKey = _UserDetails.UserAccountKey;
                                                            _OAuth.UserReference.AccountCode = _UserDetails.UserAccountCode;
                                                            #endregion
                                                            #region User Details
                                                            _OAuth.UserReference.UserName = _UserDetails.UserName;
                                                            _OAuth.UserReference.DisplayName = _UserDetails.DisplayName;
                                                            _OAuth.UserReference.Name = _UserDetails.Name;
                                                            _OAuth.UserReference.EmailAddress = _UserDetails.EmailAddress;
                                                            _OAuth.UserReference.ContactNumber = _UserDetails.ContactNumber;
                                                            _OAuth.UserReference.AccountCreateDate = _UserDetails.CreateDate;
                                                            #endregion
                                                            #region Account Session
                                                            _OAuth.UserReference.AccountSessionId = UserSessionId;
                                                            #endregion
                                                            #region User Account Device
                                                            _OAuth.UserReference.DeviceId = UserDeviceId;
                                                            _OAuth.UserReference.DeviceKey = _UserSessionDetails.UserDeviceKey;
                                                            _OAuth.UserReference.DeviceSerialNumber = _UserSessionDetails.UserDeviceSerialNumber;
                                                            #endregion
                                                            #region Set Owner
                                                            if (_UserDetails.OwnerId != null)
                                                            {
                                                                if (_UserDetails.OwnerId != 0)
                                                                {
                                                                    _OAuth.UserReference.AccountOwnerId = (long)_UserDetails.OwnerId;
                                                                    _OAuth.UserReference.AccountOwnerKey = _UserDetails.OwnerKey;
                                                                    _OAuth.UserReference.AccountOwnerDisplayName = _UserDetails.OwnerDisplayName;
                                                                }
                                                            }
                                                            #endregion
                                                            #region User Country
                                                            var CountryDetails = HCoreDataStore.SystemCountries.Where(x => x.ReferenceId == _UserDetails.CountryId).FirstOrDefault();
                                                            if (CountryDetails != null)
                                                            {
                                                                _OAuth.UserReference.SystemCountry = CountryDetails.ReferenceId;
                                                                _OAuth.UserReference.CountryId = CountryDetails.ReferenceId;
                                                                _OAuth.UserReference.CountryKey = CountryDetails.ReferenceKey;
                                                                _OAuth.UserReference.CountryName = CountryDetails.Name;
                                                                _OAuth.UserReference.CountryIsd = CountryDetails.Isd;
                                                                _OAuth.UserReference.CountryIso = CountryDetails.Iso;
                                                                _OAuth.UserReference.CountryTimeZone = CountryDetails.TimeZone;
                                                                _OAuth.UserReference.CountryMobileNumberLength = CountryDetails.MobileNumberLength;
                                                            }
                                                            #endregion
                                                            #region Request Location
                                                            _OAuth.UserReference.RequestLatitude = Header_Latitude;
                                                            _OAuth.UserReference.RequestLongitude = Header_Longitude;
                                                            #endregion
                                                            _OAuth.UserReference.RequestKey = _Auth_App_Response.UserReference.RequestKey;
                                                            _OAuth.UserReference.RequestIpAddress = _Auth_App_Response.UserReference.RequestIpAddress;
                                                            _OAuth.UserReference.AppVersionId = _Auth_App_Response.UserReference.AppVersionId;
                                                            _OAuth.UserReference.RequestTime = _Auth_App_Response.UserReference.RequestTime;
                                                            _OAuth.UserReference.Request = _Auth_App_Response.UserReference.Request;
                                                            _OAuth.UserReference.AppId = _Auth_App_Response.UserReference.AppId;
                                                            _OAuth.UserReference.OsId = _Auth_App_Response.UserReference.OsId;
                                                            _OAuth.UserReference.Data = _Auth_App_Response.UserReference.Data;
                                                            #endregion
                                                            #region Build Response
                                                            _OAuth.Status = StatusSuccess;
                                                            #endregion
                                                            #region Send Response
                                                            return _OAuth;
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region Set response
                                                            _OAuth.Status = StatusError;
                                                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA131", Resource.ResourceEn.HCA131);
                                                            #endregion
                                                            #region Send Response
                                                            return _OAuth;
                                                            #endregion
                                                        }
                                                        #endregion
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        #region Set response
                                                        _OAuth.Status = StatusError;
                                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA127", Resource.ResourceEn.HCA127);
                                                        #endregion
                                                        #region Send Response
                                                        return _OAuth;
                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    #region Set response
                                                    _OAuth.Status = StatusError;
                                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA126", Resource.ResourceEn.HCA126);
                                                    #endregion
                                                    #region Send Response
                                                    return _OAuth;
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Set response
                                                _OAuth.Status = StatusError;
                                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA125", Resource.ResourceEn.HCA125);
                                                #endregion
                                                #region Send Response
                                                return _OAuth;
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {

                                            #region Set response
                                            _OAuth.Status = StatusError;
                                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA123", Resource.ResourceEn.HCA123);
                                            #endregion
                                            #region Send Response
                                            return _OAuth;
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Set response
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA122", Resource.ResourceEn.HCA122);
                                        #endregion
                                        #region Send Response
                                        return _OAuth;
                                        #endregion
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Set response
                                    _OAuth.Status = StatusError;
                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA121", Resource.ResourceEn.HCA121);
                                    #endregion
                                    #region Send Response
                                    return _OAuth;
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    return _Auth_App_Response;
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("A", _Exception, null);
                #endregion
                #region Start Response
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_Auth_App_Response.UserReference, ResponseStatus.Error, null, "HCAR058", Resource.ResourceEn.HC500);
                return _OAuth;
                #endregion
            }
        }
        public static OAuth.Response Auth_Request_Dev(HttpRequest Request, OAuth.Response _Auth_App_Response, string? AccountType)
        {
            OAuth.Response _OAuth = new OAuth.Response();
            _OAuth.Status = StatusError;
            _OAuth.UserReference = _Auth_App_Response.UserReference;
            try
            {
                #region Validate App
                if (_Auth_App_Response.Status == StatusSuccess)
                {
                    #region Auth Req
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        #region  Access Key Parameters
                        long UserId = 0;
                        long UserAccountId = 0;
                        #endregion
                        if (AccountType == "merchant")
                        {
                            UserId = 3;
                            UserAccountId = 3;
                            var UserDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant)
                             .Where(x => x.HCUAccountSessionAccount.Any()).Select(x => new
                             {
                                 UserId = x.UserId,
                                 UserAccountId = x.Id,
                             }).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserId = UserDetails.UserId;
                                UserAccountId = UserDetails.UserAccountId;
                            }
                        }
                        if (AccountType == "pssp")
                        {
                            UserId = 8;
                            UserAccountId = 8;
                            var UserDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                             .Where(x => x.HCUAccountSessionAccount.Any()).Select(x => new
                             {
                                 UserId = x.UserId,
                                 UserAccountId = x.Id,
                             }).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserId = UserDetails.UserId;
                                UserAccountId = UserDetails.UserAccountId;
                            }
                        }
                        if (AccountType == "ptsp")
                        {
                            UserId = 9;
                            UserAccountId = 9;
                            var UserDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                               .Where(x => x.HCUAccountSessionAccount.Any()).Select(x => new
                               {
                                   UserId = x.UserId,
                                   UserAccountId = x.Id,
                               }).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserId = UserDetails.UserId;
                                UserAccountId = UserDetails.UserAccountId;
                            }
                        }
                        if (AccountType == "acquirer")
                        {
                            UserId = 4213;
                            UserAccountId = 7742;
                            var UserDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                .Where(x => x.HCUAccountSessionAccount.Any()).Select(x => new
                                {
                                    UserId = x.UserId,
                                    UserAccountId = x.Id,
                                }).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserId = UserDetails.UserId;
                                UserAccountId = UserDetails.UserAccountId;
                            }
                        }
                        if (AccountType == "admin")
                        {
                            UserId = 1;
                            UserAccountId = 15;
                            var UserDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Admin)
                                .Where(x => x.HCUAccountSessionAccount.Any()).Select(x => new
                                {
                                    UserId = x.UserId,
                                    UserAccountId = x.Id,
                                }).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                UserId = UserDetails.UserId;
                                UserAccountId = UserDetails.UserAccountId;
                            }
                        }
                        #region Get User Session Details
                        var _UserSessionDetails = (from n in _HCoreContext.HCUAccountSession
                                                   where n.AccountId == UserAccountId
                                                   orderby n.Id descending
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       ReferenceKey = n.Guid,
                                                       //ActivityDate = n.ActivityDate,
                                                       UserAccountId = n.AccountId,
                                                       UserDeviceId = n.DeviceId,
                                                       UserDeviceSerialNumber = n.Device.SerialNumber,
                                                       UserDeviceKey = n.Device.Guid,
                                                       LoginDate = n.LoginDate,
                                                       UserDeviceStatus = n.Device.StatusId,
                                                       DeviceId = n.DeviceId,
                                                   }).FirstOrDefault();
                        if (_UserSessionDetails != null)
                        {
                            #region User details 
                            var _UserDetails = (from n in _HCoreContext.HCUAccount
                                                where n.UserId == UserId && n.Id == _UserSessionDetails.UserAccountId && n.Id == UserAccountId
                                                select new
                                                {
                                                    UserAccountId = n.Id,
                                                    UserAccountKey = n.Guid,
                                                    AccountType = n.AccountType.Id,
                                                    AccountTypeName = n.AccountType.Name,
                                                    AccountTypeSystemName = n.AccountType.SystemName,
                                                    OwnerId = n.OwnerId,
                                                    OwnerKey = n.Owner.Guid,
                                                    OwnerDisplayName = n.Owner.DisplayName,
                                                    DisplayName = n.DisplayName,
                                                    UserAccountCode = n.AccountCode,
                                                    AccessPin = n.AccessPin,
                                                    Status = n.StatusId,
                                                    UserName = n.User.Username,
                                                    Password = n.User.Password,
                                                    Name = n.Name,
                                                    EmailAddress = n.EmailAddress,
                                                    ContactNumber = n.ContactNumber,
                                                    CountryId = n.CountryId,
                                                    CreateDate = n.CreateDate,
                                                    UserStatus = n.StatusId,
                                                    TimeZone = n.Country.TimeZoneName,
                                                }).FromCache().FirstOrDefault();
                            if (_UserDetails != null)
                            {
                                if (_UserDetails.UserStatus == HelperStatus.Default.Active && _UserDetails.Status == HelperStatus.Default.Active)
                                {
                                    #region Get role details
                                    int AccessCount = 1;
                                    #region Validate Access
                                    if (AccessCount > 0)
                                    {
                                        #region User Reference
                                        #region Account Type
                                        _OAuth.UserReference.AccountTypeId = _UserDetails.AccountType;
                                        _OAuth.UserReference.AccountTypeName = _UserDetails.AccountTypeName;
                                        _OAuth.UserReference.AccountTypeSystemName = _UserDetails.AccountTypeSystemName;
                                        #endregion
                                        #region Account
                                        _OAuth.UserReference.AccountId = _UserDetails.UserAccountId;
                                        _OAuth.UserReference.AccountKey = _UserDetails.UserAccountKey;
                                        _OAuth.UserReference.AccountCode = _UserDetails.UserAccountCode;
                                        #endregion
                                        #region User Details
                                        _OAuth.UserReference.UserName = _UserDetails.UserName;
                                        _OAuth.UserReference.DisplayName = _UserDetails.DisplayName;
                                        _OAuth.UserReference.Name = _UserDetails.Name;
                                        _OAuth.UserReference.EmailAddress = _UserDetails.EmailAddress;
                                        _OAuth.UserReference.ContactNumber = _UserDetails.ContactNumber;
                                        _OAuth.UserReference.AccountCreateDate = _UserDetails.CreateDate;
                                        #endregion
                                        #region Account Session
                                        _OAuth.UserReference.AccountSessionId = _UserSessionDetails.ReferenceId;
                                        #endregion
                                        #region User Account Device
                                        if (_UserSessionDetails.DeviceId != null)
                                        {
                                            _OAuth.UserReference.DeviceId = (long)_UserSessionDetails.DeviceId;
                                        }
                                        else
                                        {
                                            _OAuth.UserReference.DeviceId = 0;
                                        }
                                        _OAuth.UserReference.DeviceKey = _UserSessionDetails.UserDeviceKey;
                                        _OAuth.UserReference.DeviceSerialNumber = _UserSessionDetails.UserDeviceSerialNumber;
                                        #endregion
                                        #region Set Owner
                                        if (_UserDetails.OwnerId != null)
                                        {
                                            if (_UserDetails.OwnerId != 0)
                                            {
                                                _OAuth.UserReference.AccountOwnerId = (long)_UserDetails.OwnerId;
                                                _OAuth.UserReference.AccountOwnerKey = _UserDetails.OwnerKey;
                                                _OAuth.UserReference.AccountOwnerDisplayName = _UserDetails.OwnerDisplayName;
                                            }
                                        }
                                        #endregion
                                        #region User Country
                                        var CountryDetails = HCoreDataStore.SystemCountries.Where(x => x.ReferenceId == _UserDetails.CountryId).FirstOrDefault();
                                        if (CountryDetails != null)
                                        {
                                            _OAuth.UserReference.CountryId = CountryDetails.ReferenceId;
                                            _OAuth.UserReference.SystemCountry = CountryDetails.ReferenceId;
                                            _OAuth.UserReference.CountryKey = CountryDetails.ReferenceKey;
                                            _OAuth.UserReference.CountryName = CountryDetails.Name;
                                            _OAuth.UserReference.CountryIsd = CountryDetails.Isd;
                                            _OAuth.UserReference.CountryIso = CountryDetails.Iso;
                                            _OAuth.UserReference.CountryTimeZone = CountryDetails.TimeZone;
                                        }
                                        #endregion
                                        #region Request Location
                                        _OAuth.UserReference.RequestLatitude = 0;
                                        _OAuth.UserReference.RequestLongitude = 0;
                                        #endregion
                                        _OAuth.UserReference.RequestKey = _Auth_App_Response.UserReference.RequestKey;
                                        _OAuth.UserReference.RequestIpAddress = _Auth_App_Response.UserReference.RequestIpAddress;
                                        _OAuth.UserReference.AppVersionId = _Auth_App_Response.UserReference.AppVersionId;
                                        _OAuth.UserReference.RequestTime = _Auth_App_Response.UserReference.RequestTime;
                                        _OAuth.UserReference.Request = _Auth_App_Response.UserReference.Request;
                                        _OAuth.UserReference.AppId = _Auth_App_Response.UserReference.AppId;
                                        _OAuth.UserReference.OsId = _Auth_App_Response.UserReference.OsId;
                                        _OAuth.UserReference.Data = _Auth_App_Response.UserReference.Data;
                                        #endregion
                                        #region Build Response
                                        _OAuth.Status = StatusSuccess;
                                        #endregion
                                        #region Send Response
                                        return _OAuth;
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Set response
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA131", Resource.ResourceEn.HCA131);
                                        #endregion
                                        #region Send Response
                                        return _OAuth;
                                        #endregion
                                    }
                                    #endregion
                                    #endregion
                                }
                                else
                                {
                                    #region Set response
                                    _OAuth.Status = StatusError;
                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA127", Resource.ResourceEn.HCA127);
                                    #endregion
                                    #region Send Response
                                    return _OAuth;
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Set response
                                _OAuth.Status = StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA126", Resource.ResourceEn.HCA126);
                                #endregion
                                #region Send Response
                                return _OAuth;
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Set response
                            _OAuth.Status = StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA121", Resource.ResourceEn.HCA121);
                            #endregion
                            #region Send Response
                            return _OAuth;
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    return _Auth_App_Response;
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("A", _Exception, null);
                #endregion
                #region Start Response
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(_Auth_App_Response.UserReference, ResponseStatus.Error, null, "HCAR058", Resource.ResourceEn.HC500);
                return _OAuth;
                #endregion
            }
        }
        public static object Auth_ResponseJ(OResponse _OResponse, OUserReference _UserReference)
        {
            try
            {
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.Message;
                    //_OResponse.Message = _OResponse.Message + " : " + _OResponse.ResponseCode;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore,
                           ContractResolver = new CamelCasePropertyNamesContractResolver()
                       });
                #endregion
                #region Build Response
                if (_UserReference != null && _UserReference.ResponseType == "json")
                {
                    JObject _JObject = JObject.Parse(JsonString);
                    return _JObject;
                }
                else
                {
                    OAuth.Request _OAuthResponse = new OAuth.Request();
                    _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                    return _OAuthResponse;
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_Response", _Exception);
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.ResponseCode + ": " + _OResponse.Message;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion

                #region Build Response
                OAuth.Request _OAuthResponse = new OAuth.Request();
                _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                #endregion
                #region Send Response
                return _OAuthResponse;
                #endregion
            }
        }
        public static object Auth_Response(OResponse _OResponse, OUserReference _UserReference)
        {
            try
            {
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.Message;
                    //_OResponse.Message = _OResponse.Message + " : " + _OResponse.ResponseCode;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion
                #region Build Response
                if (_UserReference != null && _UserReference.ResponseType == "json")
                {
                    JObject _JObject = JObject.Parse(JsonString);
                    return _JObject;
                }
                else
                {
                    OAuth.Request _OAuthResponse = new OAuth.Request();
                    _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                    return _OAuthResponse;
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_Response", _Exception);
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.ResponseCode + ": " + _OResponse.Message;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion

                #region Build Response
                OAuth.Request _OAuthResponse = new OAuth.Request();
                _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                #endregion
                #region Send Response
                return _OAuthResponse;
                #endregion
            }
        }


        public static object Auth_ResponseClear(OResponse _OResponse, OUserReference _UserReference)
        {
            try
            {
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.Message;
                    //_OResponse.Message = _OResponse.Message + " : " + _OResponse.ResponseCode;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion
                #region Build Response

                JObject _JObject = JObject.Parse(JsonString);
                return _JObject;

                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_ResponseClear", _Exception);
                #region Manage Operations
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.ResponseCode + ": " + _OResponse.Message;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }
                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion

                #region Build Response
                OAuth.Request _OAuthResponse = new OAuth.Request();
                _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                #endregion
                #region Send Response
                return _OAuthResponse;
                #endregion
            }
        }


        public static OAuth.Request Auth_Response(OResponse _OResponse)
        {
            try
            {
                #region Manage Operations
                //_OResponse.Task = null;
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.Message + " : " + _OResponse.ResponseCode;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }

                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion

                #region Build Response
                OAuth.Request _OAuthResponse = new OAuth.Request();
                _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                #endregion
                #region Send Response
                return _OAuthResponse;
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Auth_Response", _Exception);
                #region Manage Operations
                //_OResponse.Task = null;
                if (_OResponse.Status == StatusError)
                {
                    _OResponse.Message = _OResponse.ResponseCode + ": " + _OResponse.Message;
                }
                else
                {
                    _OResponse.ResponseCode = "HC200";
                }

                #endregion
                #region Clean Response And Convert To Json
                string JsonString = JsonConvert.SerializeObject(_OResponse,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           NullValueHandling = NullValueHandling.Ignore
                       });
                #endregion

                #region Build Response
                OAuth.Request _OAuthResponse = new OAuth.Request();
                _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
                #endregion
                #region Send Response
                return _OAuthResponse;
                #endregion
            }

        }
        public static object Auth_ResponseDefaultObject(OResponse _OResponse)
        {

            //_OResponse.Task = null;
            if (_OResponse.Status == StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            //OResponse _NResponse = JsonConvert.DeserializeObject<OResponse>(JsonString);
            return JObject.Parse(JsonString);
        }
        public static object Auth_ResponseDefaultObjectRsa(OResponse _OResponse)
        {
            //_OResponse.Task = null;
            if (_OResponse.Status == StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            //OResponse _NResponse = JsonConvert.DeserializeObject<OResponse>(JsonString);
            if (HostEnvironment == HostEnvironmentType.Live)
            {
                string PublicKey = "NTEyITxSU0FLZXlWYWx1ZT48TW9kdWx1cz5zZHhmOFo2TE92cVBxMGNIcmMyQWpoQ1JuSkFnQ2kyVDd6dyt4ckZ0OERCNVYxZ3VMRDFmcWhBTkQrb1g2bnZRQzA5czFqWXFIbXF1MExZbTR5VHU4UT09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg==";
                ResponseV1 _ResponseV1 = new ResponseV1();
                _ResponseV1.data = HCoreEncryptRsa.Encrypt(JsonString, PublicKey);
                return _ResponseV1;
            }
            else
            {
                string PublicKey = "NDA5NiE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+a0h5R2EvNVhpVXhhQjIyZWc3aUFmTDVMaEN4RW5kUHpKNHMwek5tN1VEUDdmNWY1SWdGcXFxcFVtVjJ2SlVNUGJKcFBWaFhRcUtzY2dlaFU2c3lmOFpqclRIbVhCUS8zdVl4b1F5ODNQaExqRW45TWRVdGsyN3BjdStuUXE3MlhzcjNscWk0dkR4ekViNkdHTmpEWFlvYzBQNHZXdVM5bHNQbCt5TXA1RXVYNWNkd3Nqa3hmalBKUUtON05tTWs2OXlKNHp3M3R4bFNmaG1DZXRObmgxS01XVFQySGsxREN6R1d5USs0SDdYWDhVQ241Q2xnTTdHODFRUDlDaHlpOHg3UmxDY2o4QVg4d1UyUDBycGovN1JOcmhGUGpLU3kreUhET3YrOXh6NEhiNFVmakhvdnJhdlpIU25WNU0rblpjLzBEYXVqbmJONHQxZjJIUnRCbjlPa3pLK2xXdno3WjY5MW5PMit2ZDJnaFRrQm1jRUplR3RHQUlkZFhjZkh2TjlNQmhWMUxmMzlBY3QrNGdUN0l6UWNDL0tndVorM3Z2Y1pLWFBxc2tNeXU3dmNTUi9WamU2d0xBMWp5UkZsc3MraWcxM25TUDhCZTY5aHI3SXpXYmd2YTZBN3NPOG1tN2ZZcFdEMkQxUHVralJqRG9pMFk1SklseExUWkFseXJobU1EdzRxTEViWVBUeWpzOG16aHZiWjVCR1hvb3JNbFhJcW9VaHRTbWFmb1E3RzNIa2RpOExFZ2xNNHlZZnhzWGJxMkluWWNnaUs1cExaOE1UM1ZMd2ZtY0dGUHZxbUNla2M5ZHJIaEFkaUJtNHc1c0xOL0NBcHpRbE9adFRwNWJQWUJmTWxkOFhuVjFjTSsydmxPS3JWbFRHbTZuZy9FZ0Rzb1MrS1dNQjA9PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg==";
                ResponseV1 _ResponseV1 = new ResponseV1();
                _ResponseV1.data = HCoreEncryptRsa.Encrypt(JsonString, PublicKey);
                return _ResponseV1;
            }

        }
        public static string Auth_ResponseDefault(OResponse _OResponse)
        {

            //_OResponse.Task = null;
            if (_OResponse.Status == StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            //OResponse _NResponse = JsonConvert.DeserializeObject<OResponse>(JsonString);
            return JsonString;
        }
        public static string Auth_ResponseDefaultRsa(OResponse _OResponse)
        {

            //_OResponse.Task = null;
            if (_OResponse.Status == StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            //OResponse _NResponse = JsonConvert.DeserializeObject<OResponse>(JsonString);
            if (HostEnvironment == HostEnvironmentType.Live)
            {
                string PublicKey = "NTEyITxSU0FLZXlWYWx1ZT48TW9kdWx1cz5zZHhmOFo2TE92cVBxMGNIcmMyQWpoQ1JuSkFnQ2kyVDd6dyt4ckZ0OERCNVYxZ3VMRDFmcWhBTkQrb1g2bnZRQzA5czFqWXFIbXF1MExZbTR5VHU4UT09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg==";
                ResponseV1 _ResponseV1 = new ResponseV1();
                _ResponseV1.data = HCoreEncryptRsa.Encrypt(JsonString, PublicKey);
                return JsonString;
            }
            else
            {
                string PublicKey = "NDA5NiE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+a0h5R2EvNVhpVXhhQjIyZWc3aUFmTDVMaEN4RW5kUHpKNHMwek5tN1VEUDdmNWY1SWdGcXFxcFVtVjJ2SlVNUGJKcFBWaFhRcUtzY2dlaFU2c3lmOFpqclRIbVhCUS8zdVl4b1F5ODNQaExqRW45TWRVdGsyN3BjdStuUXE3MlhzcjNscWk0dkR4ekViNkdHTmpEWFlvYzBQNHZXdVM5bHNQbCt5TXA1RXVYNWNkd3Nqa3hmalBKUUtON05tTWs2OXlKNHp3M3R4bFNmaG1DZXRObmgxS01XVFQySGsxREN6R1d5USs0SDdYWDhVQ241Q2xnTTdHODFRUDlDaHlpOHg3UmxDY2o4QVg4d1UyUDBycGovN1JOcmhGUGpLU3kreUhET3YrOXh6NEhiNFVmakhvdnJhdlpIU25WNU0rblpjLzBEYXVqbmJONHQxZjJIUnRCbjlPa3pLK2xXdno3WjY5MW5PMit2ZDJnaFRrQm1jRUplR3RHQUlkZFhjZkh2TjlNQmhWMUxmMzlBY3QrNGdUN0l6UWNDL0tndVorM3Z2Y1pLWFBxc2tNeXU3dmNTUi9WamU2d0xBMWp5UkZsc3MraWcxM25TUDhCZTY5aHI3SXpXYmd2YTZBN3NPOG1tN2ZZcFdEMkQxUHVralJqRG9pMFk1SklseExUWkFseXJobU1EdzRxTEViWVBUeWpzOG16aHZiWjVCR1hvb3JNbFhJcW9VaHRTbWFmb1E3RzNIa2RpOExFZ2xNNHlZZnhzWGJxMkluWWNnaUs1cExaOE1UM1ZMd2ZtY0dGUHZxbUNla2M5ZHJIaEFkaUJtNHc1c0xOL0NBcHpRbE9adFRwNWJQWUJmTWxkOFhuVjFjTSsydmxPS3JWbFRHbTZuZy9FZ0Rzb1MrS1dNQjA9PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg==";
                ResponseV1 _ResponseV1 = new ResponseV1();
                _ResponseV1.data = HCoreEncryptRsa.Encrypt(JsonString, PublicKey);
                return JsonString;
            }

        }
        public static OAuth.Request Auth_Response(OResponse _OResponse, OAuth.Response _OAuth)
        {
            #region Manage Operations
            //_OResponse.Task = null;
            if (_OResponse != null && _OResponse.Status == StatusError)
            {
                _OResponse.Message = _OResponse.Message + " : " + _OResponse.ResponseCode;
            }
            #endregion
            #region Clean Response And Convert To Json
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            #endregion
            #region Build Response
            OAuth.Request _OAuthResponse = new OAuth.Request();
            _OAuthResponse.zx = HCoreEncrypt.EncodeText(JsonString);
            if (_OAuth != null)
            {
                if (_OAuth.UserReference != null)
                {
                }
            }
            #endregion
            #region Send Response
            return _OAuthResponse;
            #endregion
        }
    }
}
