//==================================================================================
// FileName: ODataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Helper.DataStore
{
    internal class OHCDataStore
    {
        internal class App
        {
            public long? LogRequest { get; set; }

            public long AppId { get; set; }
            public long AppVersionId { get; set; }


            public long AppStatusId { get; set; }
            public long AppVersionStatusId { get; set; }

            public string? AppKey { get; set; }
            public string? AppVersionKey { get; set; }

            public long? OsId { get; set; }
            public string? OsName { get; set; }

            public int? CountryId { get; set; }
            public string? CountryIsd { get; set; }
            public string? CountryIso { get; set; }
            public string? CountryTimeZoneName { get; set; }
            public int? CountryMobileNumberLength { get; set; }

            public long? UserAccountId { get; set; }
            public long? UserAccountTypeId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountCode { get; set; }
            public string? UserAccountDisplayName { get; set; }


            public long? UserAccountOwnerId { get; set; }
            public string? UserAccountOwnerKey { get; set; }
            public string? UserAccountOwnerCode { get; set; }
            public long? UserAccountOwnerAccountTypeId { get; set; }
            public string? UserAccountOwnerDisplayName { get; set; }

            public string? IpAddress { get; set; }
        }
        internal class Api
        {
            public long ReferenceId { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public long? StatusId { get; set; }
        }
        internal class Country
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Iso { get; set; }
            public string? Isd { get; set; }
            public string? TimeZone { get; set; }
            public int? MobileNumberLength { get; set; }
        }

        internal class Resource
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? Description { get; set; }
            public long? StatusId { get; set; }
        }

    }
}
