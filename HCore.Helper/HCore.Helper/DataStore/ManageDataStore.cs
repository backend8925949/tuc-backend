//==================================================================================
// FileName: ManageDataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Helper.DataStore
{
    internal static class HCoreDataStore
    {
        internal static List<OHCDataStore.App> SystemApps = new List<OHCDataStore.App>();
        internal static List<OHCDataStore.Api> SystemApis = new List<OHCDataStore.Api>();
        internal static List<OHCDataStore.Resource> SystemResources = new List<OHCDataStore.Resource>();
        internal static List<OHCDataStore.Country> SystemCountries = new List<OHCDataStore.Country>();
    }

    internal static class DataStoreManager
    {
        internal static void LoadDataStore()
        {
            try
            {
                HCoreDataStore.SystemApps.Clear();
                HCoreDataStore.SystemApis.Clear();
                HCoreDataStore.SystemResources.Clear();
                HCoreDataStore.SystemCountries.Clear();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    #region SystemApps
                    HCoreDataStore.SystemApps = (from x in _HCoreContext.HCCoreAppVersion
                                                 where x.StatusId == HelperStatus.Default.Active
                                                  && x.App.StatusId == HelperStatus.Default.Active
                                                 select new OHCDataStore.App
                                                 {
                                                     AppId = x.App.Id,
                                                     AppKey = x.App.AppKey,
                                                     AppVersionId = x.Id,
                                                     AppVersionKey = x.ApiKey,
                                                     IpAddress = x.App.IpAddress,
                                                     AppStatusId = x.App.StatusId,
                                                     AppVersionStatusId = x.StatusId,
                                                     LogRequest = x.App.AllowLogging,
                                                     OsId = 1,
                                                     OsName = "n/a",
                                                     CountryIsd = x.App.Account.Country.Isd,
                                                     CountryId = x.App.Account.CountryId,
                                                     CountryTimeZoneName = x.App.Account.Country.TimeZoneName,
                                                     CountryMobileNumberLength = x.App.Account.Country.MobileNumberLength,
                                                     UserAccountId = x.App.AccountId,
                                                     UserAccountKey = x.App.Account.Guid,
                                                     UserAccountCode = x.App.Account.AccountCode,
                                                     UserAccountTypeId = x.App.Account.AccountTypeId,
                                                     UserAccountDisplayName = x.App.Account.DisplayName,
                                                     UserAccountOwnerId = x.App.Account.Owner.Id,
                                                     UserAccountOwnerKey = x.App.Account.Owner.Guid,
                                                     UserAccountOwnerCode = x.App.Account.Owner.AccountCode,
                                                     UserAccountOwnerAccountTypeId = x.App.Account.Owner.AccountTypeId,
                                                     UserAccountOwnerDisplayName = x.App.Account.Owner.DisplayName
                                                 }).ToList();
                    #endregion
                    #region System Apis
                    HCoreDataStore.SystemApis = (from x in _HCoreContext.HCCoreCommon
                                                 where x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                  && x.TypeId == HCoreConstant.HelperType.Api
                                                 select new OHCDataStore.Api
                                                 {
                                                     ReferenceId = x.Id,
                                                     Name = x.Name,
                                                     SystemName = x.SystemName,
                                                 }).ToList();
                    #endregion
                    #region SystemResources
                    HCoreDataStore.SystemResources = (from n in _HCoreContext.HCCoreCommon
                                                      where n.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                      && n.TypeId == HCoreConstant.HelperType.ResponseCodes
                                                      select new OHCDataStore.Resource
                                                      {
                                                          ReferenceId = n.Id,
                                                          ReferenceKey = n.Guid,
                                                          SystemName = n.SystemName,
                                                          Value = n.Value
                                                      }).ToList();
                    #endregion
                    #region Countries
                    HCoreDataStore.SystemCountries = (from n in _HCoreContext.HCCoreCountry
                                                      where n.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                      select new OHCDataStore.Country
                                                      {
                                                          ReferenceId = n.Id,
                                                          ReferenceKey = n.Guid,
                                                          Name = n.Name,
                                                          SystemName = n.SystemName,
                                                          Iso = n.Iso,
                                                          Isd = n.Isd,
                                                          TimeZone = n.TimeZoneName,
                                                          MobileNumberLength = n.MobileNumberLength,
                                                      }).ToList();
                    #endregion
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("LoadDataStore", _Exception);
            }
        }
        internal static void RefreshApps()
        {
            try
            {
                #region System Apis
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    HCoreDataStore.SystemApps.Clear();
                    HCoreDataStore.SystemApps = (from x in _HCoreContext.HCCoreAppVersion
                                                 where x.StatusId == HelperStatus.Default.Active
                                                  && x.App.StatusId == HelperStatus.Default.Active
                                                 select new OHCDataStore.App
                                                 {
                                                     AppId = x.App.Id,
                                                     AppKey = x.App.AppKey,
                                                     AppVersionId = x.Id,
                                                     AppVersionKey = x.ApiKey,
                                                     IpAddress = x.App.IpAddress,
                                                     AppStatusId = x.App.StatusId,
                                                     AppVersionStatusId = x.StatusId,
                                                     LogRequest = x.App.AllowLogging,
                                                     OsId = 1,
                                                     OsName = "n/a",
                                                     CountryIsd = x.App.Account.Country.Isd,
                                                     CountryId = x.App.Account.CountryId,
                                                     CountryTimeZoneName = x.App.Account.Country.TimeZoneName,
                                                     CountryMobileNumberLength = x.App.Account.Country.MobileNumberLength,
                                                     UserAccountId = x.App.AccountId,
                                                     UserAccountKey = x.App.Account.Guid,
                                                     UserAccountCode = x.App.Account.AccountCode,
                                                     UserAccountTypeId = x.App.Account.AccountTypeId,
                                                     UserAccountDisplayName = x.App.Account.DisplayName,
                                                     UserAccountOwnerId = x.App.Account.Owner.Id,
                                                     UserAccountOwnerKey = x.App.Account.Owner.Guid,
                                                     UserAccountOwnerCode = x.App.Account.Owner.AccountCode,
                                                     UserAccountOwnerAccountTypeId = x.App.Account.Owner.AccountTypeId,
                                                     UserAccountOwnerDisplayName = x.App.Account.Owner.DisplayName
                                                 }).ToList();
                    _HCoreContext.Dispose();
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RefreshApps", _Exception);
            }

        }
        internal static async void RefreshApi()
        {
            try
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    #region System Apis
                    List<OHCDataStore.Api> Apis = await (from x in _HCoreContext.HCCoreCommon
                                                         where x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                          && x.TypeId == HCoreConstant.HelperType.Api
                                                         select new OHCDataStore.Api
                                                         {
                                                             ReferenceId = x.Id,
                                                             Name = x.Name,
                                                             SystemName = x.SystemName,
                                                         }).ToListAsync();
                    HCoreDataStore.SystemApis.Clear();
                    HCoreDataStore.SystemApis.AddRange(Apis);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RefreshApi", _Exception);
            }

        }
        internal static void RefreshResources()
        {
            #region System Apis
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                HCoreDataStore.SystemResources.Clear();
                HCoreDataStore.SystemResources = (from n in _HCoreContext.HCCoreCommon
                                                  where n.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                  && n.TypeId == HCoreConstant.HelperType.ResponseCodes
                                                  select new OHCDataStore.Resource
                                                  {
                                                      ReferenceId = n.Id,
                                                      ReferenceKey = n.Guid,
                                                      SystemName = n.SystemName,
                                                      Value = n.Value
                                                  }).ToList();
                _HCoreContext.Dispose();
            }
            #endregion
        }
        internal static void RefreshCountries()
        {
            #region System Apis
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                HCoreDataStore.SystemCountries.Clear();
                HCoreDataStore.SystemCountries = (from n in _HCoreContext.HCCoreCountry
                                                  where n.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                  select new OHCDataStore.Country
                                                  {
                                                      ReferenceId = n.Id,
                                                      ReferenceKey = n.Guid,
                                                      Name = n.Name,
                                                      SystemName = n.SystemName,
                                                      Iso = n.Iso,
                                                      Isd = n.Isd,
                                                  }).ToList();
                _HCoreContext.Dispose();
            }
            #endregion
        }
    }
}
