//==================================================================================
// FileName: HCoreEncrypt.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Security.Cryptography;
using System.Text;

namespace HCore.Helper
{
    public static class HCoreEncrypt
    {
        public static string GetSha256FromString(string strData)
        {
            var message = Encoding.ASCII.GetBytes(strData);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";

            var hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }
        public static void main()
        {
            string text = DecryptHash("XDSOaBbV2rJFel4Pv1KG3LzBwnVRtMiXBhUr19iFPrI=");
            System.Console.WriteLine(text);
        }
        #region Private
        private static readonly string PASSWORD = "4047E98985014A32BFDE0862C8D480B945AAB975C85D4997B6DB8AC61F244646";
        private static byte[] _rawSecretKey =
          {
            0x12, 0x00, 0x22, 0x55, 0x33, 0x78, 0x25, 0x11,
            0x33, 0x45, 0x00, 0x00, 0x34, 0x00, 0x23, 0x28
        };
        private static byte[] encodeDigest(string passPhrase)
        {
            MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = Encoding.UTF8.GetBytes(passPhrase);
            return x.ComputeHash(data);
        }
        private static string GetMd5Hash(MD5 md5Hash, string? input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        private static bool VerifyMd5Hash(MD5 md5Hash, string? input, string? hash)
        {
            string hashOfInput = GetMd5Hash(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static string Operation_CreateHash(string Content)
        {
            string hash;
            using (MD5 md5Hash = MD5.Create())
            {
                hash = GetMd5Hash(md5Hash, Content);
                if (VerifyMd5Hash(md5Hash, Content, hash))
                {

                }
                else
                {
                    hash = null;
                }
            }
            return hash;
        }

        #endregion
        /// <summary>
        /// Encrypts text to MD5 hash 
        /// </summary>
        /// <returns>Returns encrypted string</returns>
        /// <param name="Content">Pass string value to encrypt</param>
        public static string EncryptHash(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                byte[] passwordKey = encodeDigest(PASSWORD);
                RijndaelManaged _RijndaelManaged = new RijndaelManaged();
                _RijndaelManaged.IV = _rawSecretKey;
                _RijndaelManaged.Key = passwordKey;
                ICryptoTransform _Encryptor = _RijndaelManaged.CreateEncryptor(passwordKey, _rawSecretKey);
                byte[] plainText = Encoding.UTF8.GetBytes(Content);
                byte[] cipherText = _Encryptor.TransformFinalBlock(plainText, 0, plainText.Length);
                return Convert.ToBase64String(cipherText);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Decrypts encrypted content created by EncryptHash function.
        /// </summary>
        /// <returns>Returns decypted content string</returns>
        /// <param name="Content">Pass encrypted string to decrypt</param>
        public static string DecryptHash(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                byte[] passwordKey = encodeDigest(PASSWORD);
                RijndaelManaged _RijndaelManaged = new RijndaelManaged();
                _RijndaelManaged.IV = _rawSecretKey;
                _RijndaelManaged.Key = passwordKey;
                ICryptoTransform _Decryptor = _RijndaelManaged.CreateDecryptor(passwordKey, _rawSecretKey);
                byte[] cipherText = Convert.FromBase64String(Content);
                byte[] decryptedText = _Decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
                return Encoding.UTF8.GetString(decryptedText);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Converts string to base64 with utf8 encoding
        /// </summary>
        /// <returns>Returns base64 converted string</returns>
        /// <param name="Content">Pass string content to encode</param>
        public static string EncodeText(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Content);
                return System.Convert.ToBase64String(plainTextBytes);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///  Converts base64 to normal string with utf8 decoding
        /// </summary>
        /// <returns>Returns decoded text</returns>
        /// <param name="Content">Pass base64 utf8 encoded string</param>
        public static string DecodeText(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                var base64EncodedBytes = System.Convert.FromBase64String(Content);
                var decText = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            else
            {
                return null;
            }
        }

        public static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
