//==================================================================================
// FileName: HCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System.Text.RegularExpressions;
using Akka.Actor;
using Amazon.S3;
using Amazon.S3.Model;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using Z.EntityFramework.Plus;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using HCore.Helper.DataStore;
using HCore.Helper.ProcessHandler;
using System.Net;
using System.Reflection;
using ClosedXML.Excel;
using SelectPdf;
using System.Text;
using System.Globalization;
using HCore.Data.Logging.Models;

namespace HCore.Helper
{
    public static class HCoreHelper
    {
        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var random = new Random(); var range = to - from;
            var randTimeSpan = new TimeSpan((long)(random.NextDouble() * range.Ticks));
            return from + randTimeSpan;
        }
        public static double GetPercentage(double Amount, double Percentage, int Round, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (Amount * Percentage) / 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = (double)Math.Round(Value, Round);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }
        public static string GenerateTransactionReference(long? Number1 = null, long? Number2 = null, string Prefix = null)
        {
            DateTime GMTTime = DateTime.UtcNow;
            var Reference = "";

            Reference = Reference + "" + GMTTime.ToString("ms") +
                        GMTTime.ToString("ss") +
                        GMTTime.ToString("mm") +
                        GMTTime.ToString("HH") +
                        GMTTime.ToString("dd") +
                        GMTTime.ToString("MM") +
                        GMTTime.ToString("yy");
            if (Number1 != null)
            {
                Reference += GenerateFixedString(Number1);
            }

            if (Number2 != null)
            {
                Reference += GenerateFixedString(Number2);
            }

            Reference += GenerateRandomString(6);
            if (!string.IsNullOrEmpty(Prefix))
            {
                Reference = Prefix + Reference;
            }

            return Reference;
        }
        private static readonly Random _Random = new();
        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[_Random.Next(s.Length)]).ToArray());
        }
        public static string GenerateFixedString(long? Number, int? Length = 6)
        {
            if (Number > 0)
            {
                return string.Format("{0:00000000}", Number);
            }

            return "00000000";
        }

        public static string GenerateAccountCode(int length = 10, string? Prefix = null, string? Sufix = null)
        {
            string Code = GenerateAccountCodeX(length, Prefix, Sufix);
            if (Code.Length != length)
            {
                Code = GenerateAccountCodeX(length, Prefix, Sufix);
            }
            if (Code.Length != length)
            {
                Code = GenerateAccountCodeX(length, Prefix, Sufix);
            }
            return Code;
        }
        private static string GenerateAccountCodeX(int length = 10, string? Prefix = null, string? Sufix = null)
        {
            if (!string.IsNullOrEmpty(Prefix))
            {
                length = length - Prefix.Length;
            }
            if (!string.IsNullOrEmpty(Sufix))
            {
                length = length - Sufix.Length;
            }
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random _Random = new Random(BitConverter.ToInt32(seed, 0));
            int _RandomNumber = 0;
            String _GeneratedRandomNumber = "";
            for (int i = 0; i < length; i++)
            {
                _RandomNumber = _Random.Next(48, 58);
                _GeneratedRandomNumber = _GeneratedRandomNumber + (char)_RandomNumber;
            }
            if (!string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Sufix))
            {
                return Prefix + "" + _GeneratedRandomNumber + "" + Sufix;
            }
            else if (!string.IsNullOrEmpty(Prefix))
            {
                return Prefix + "" + _GeneratedRandomNumber;
            }
            else if (!string.IsNullOrEmpty(Sufix))
            {
                return _GeneratedRandomNumber + "" + Sufix;
            }
            else
            {
                return _GeneratedRandomNumber;
            }
        }

        public static double RoundNumber(double Value, int Round, bool AllowNegative = false)
        {
            #region Code Block
            return (double)Math.Round(Value, Round);
            #endregion
        }
        public static double GetPercentage(double Amount, double Percentage, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (Amount * Percentage) / 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = (double)Math.Round(Value, 2);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }
        public static double GetAmountPercentage(double SmallAmount, double BigAmount, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (SmallAmount / BigAmount) * 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = Math.Round(Value, 3);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }

        public static OMobileNumberStatus ValidateMobileNumber(string MobileNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(MobileNumber))
                {
                    string VMobileNumber = MobileNumber.Trim();
                    VMobileNumber = VMobileNumber.Replace("+", "");
                    VMobileNumber = "+" + VMobileNumber.Replace(" ", "");
                    var _PhoneNumberUtilLib = PhoneNumbers.PhoneNumberUtil.GetInstance();
                    var _NumberDetails = _PhoneNumberUtilLib.Parse(VMobileNumber, null);
                    var IsNumberValid = _PhoneNumberUtilLib.IsValidNumber(_NumberDetails);
                    if (IsNumberValid)
                    {
                        var _Response = new OMobileNumberStatus
                        {
                            IsNumberValid = IsNumberValid,
                            MobileNumber = MobileNumber,
                            Isd = _NumberDetails.CountryCode.ToString()
                        };
                        return _Response;
                    }
                    else
                    {
                        //For random mobile testing purposes
                        //var _Response = new OMobileNumberStatus
                        //{
                        //    IsNumberValid = IsNumberValid,
                        //    MobileNumber = MobileNumber
                        //};

                        //For random mobile testing purposes
                        var _Response = new OMobileNumberStatus
                        {
                            IsNumberValid = false,
                            MobileNumber = MobileNumber,
                            //Isd = "234"
                        };
                        return _Response;
                    }

                }
                else
                {
                    var _Response = new OMobileNumberStatus
                    {
                        IsNumberValid = false,
                        MobileNumber = MobileNumber
                    };
                    return _Response;
                }
            }
            catch (Exception ex)
            {
                var _Response = new OMobileNumberStatus
                {
                    IsNumberValid = false,
                    MobileNumber = MobileNumber
                };
                return _Response;
            }
        }
        public static string FormatMobileNumber(string CountryIsd, string MobileNumber)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                CountryIsd = CountryIsd.Trim();
                CountryIsd = CountryIsd.Replace("+", "");
                CountryIsd = CountryIsd.Replace(" ", "");
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                string ExPrefix = CountryIsd + "0";
                if (VMobileNumber.Length == 10)
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else if (VMobileNumber.Length > 10)
                {
                    int tail_length = 10;
                    VMobileNumber = VMobileNumber.Substring(VMobileNumber.Length - tail_length);
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                //if (VMobileNumber.StartsWith(ExPrefix))
                //{
                //    VMobileNumber = VMobileNumber.Replace(ExPrefix, "");
                //}
                //if (VMobileNumber.StartsWith("0"))
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber.Substring(1, (VMobileNumber.Length - 1));
                //}
                //else if (VMobileNumber.StartsWith(CountryIsd))
                //{
                //    VMobileNumber = "" + VMobileNumber;
                //}
                //else
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber;
                //}
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }


        public static string CleanMobileNumber(string MobileNumber, int Length)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                //if (VMobileNumber.StartsWith(ExPrefix))
                //{
                //    VMobileNumber = VMobileNumber.Replace(ExPrefix, "");
                //}
                //if (VMobileNumber.StartsWith("0"))
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber.Substring(1, (VMobileNumber.Length - 1));
                //}
                //else if (VMobileNumber.StartsWith(CountryIsd))
                //{
                //    VMobileNumber = "" + VMobileNumber;
                //}
                //else
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber;
                //}
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }
        public static string FormatMobileNumber(string CountryIsd, string MobileNumber, int Length)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                string ExPrefix = CountryIsd + "0";
                if (VMobileNumber.Length == Length)
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else if (VMobileNumber.Length > Length)
                {
                    int tail_length = Length;
                    VMobileNumber = VMobileNumber.Substring(VMobileNumber.Length - tail_length);
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                //if (VMobileNumber.StartsWith(ExPrefix))
                //{
                //    VMobileNumber = VMobileNumber.Replace(ExPrefix, "");
                //}
                //if (VMobileNumber.StartsWith("0"))
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber.Substring(1, (VMobileNumber.Length - 1));
                //}
                //else if (VMobileNumber.StartsWith(CountryIsd))
                //{
                //    VMobileNumber = "" + VMobileNumber;
                //}
                //else
                //{
                //    VMobileNumber = CountryIsd + VMobileNumber;
                //}
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }

        public static string GenerateGuid()
        {
            #region Code Block
            Guid _Guid = Guid.NewGuid();
            string TGuid = _Guid.ToString("N");
            //string TNGuid = _Guid.ToString("N");
            return TGuid;
            #endregion
        }

        public static bool ValidateEmailAddress(this string email)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        public static string Substring(string Content, int End)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                if (Content.Length <= End)
                {
                    return Content;
                }
                else
                {
                    return Content.Substring(0, End);
                }
            }
            else
            {
                return Content;
            }
        }
        public static string GenerateSystemName(string Value)
        {
            if (!string.IsNullOrEmpty(Value))
            {
                #region Code Block
                return Value.Replace(" ", String.Empty).ToLower();
                #endregion
            }
            else
            {

                return null;
            }

        }
        public static string CleanString(string Value)
        {
            return Regex.Replace(Value, @"\s", "");
        }
        public static string GenerateRandomNumber()
        {
            DateTime _DateTime = DateTime.Now;
            string Year = Convert.ToString(_DateTime.Year);
            string Month = Convert.ToString(_DateTime.Month);
            string Day = Convert.ToString(_DateTime.Day);
            string Minute = Convert.ToString(_DateTime.Minute);
            string Second = Convert.ToString(_DateTime.Second);
            string Millisecond = Convert.ToString(_DateTime.Millisecond);
            Random _Random = new Random();
            string RandomNo = Convert.ToString(_Random.Next(00, 99));
            return Year + Month + Day + Minute + Second + Millisecond + RandomNo;
        }
        public static string GenerateDateString()
        {
            DateTime GMTTime = DateTime.UtcNow;
            //return GMTTime.ToString("yy") +
            //       GMTTime.ToString("MM") +
            //       GMTTime.ToString("dd") +
            //       GMTTime.ToString("HH") +
            //       GMTTime.ToString("mm") +
            //       GMTTime.ToString("ss") +
            //       GMTTime.ToString("ms");

            return GMTTime.ToString("ms") +
                  GMTTime.ToString("ss") +
                  GMTTime.ToString("mm") +
                  GMTTime.ToString("HH") +
                  GMTTime.ToString("dd") +
                  GMTTime.ToString("MM") +
                  GMTTime.ToString("yy");
        }
        public static string GetAccountIdString(long? Number)
        {
            return string.Format("{0:00000}", Number);
        }
        public static DateTime GetGMTDate()
        {
            DateTime GMTTime = DateTime.UtcNow.Date;
            return GMTTime;
        }
        public static DateTime GetGMTDateTime()
        {
            DateTime GMTTime = DateTime.UtcNow;
            return GMTTime;
        }
        public static DateTime GetNigeriaToGMT(DateTime UTCDateTime)
        {
            return UTCDateTime.AddHours(-1);
        }
        public static DateTime GetNigeriaTime()
        {
            return DateTime.UtcNow.AddHours(-1);
        }
        public static DateTime GetGMTToNigeria()
        {
            return DateTime.UtcNow.AddHours(1);
        }
        public static DateTime GetDateTime(DateTime? Date, string TimeZoneId)
        {
            if (Date != null)
            {
                return TimeZoneInfo.ConvertTimeFromUtc((DateTime)Date, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
            else
            {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
        }
        public static DateTime ConvertTimeToUtc(DateTime? Date, string TimeZoneId)
        {
            if (Date != null)
            {
                return TimeZoneInfo.ConvertTimeToUtc((DateTime)Date, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
            else
            {
                return TimeZoneInfo.ConvertTimeToUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
        }
        public static DateTime GetGMTToNigeria(DateTime UTCDateTime)
        {
            return UTCDateTime.AddHours(1);
        }
        public static DateTime ConvertFromUTC(DateTime? Date, string TimeZoneId)
        {
            if (Date != null)
            {
                return TimeZoneInfo.ConvertTimeFromUtc((DateTime)Date, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
            else
            {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
        }
        public static DateTime ConvertToUTC(DateTime? Date, string TimeZoneId)
        {
            if (Date != null)
            {
                return TimeZoneInfo.ConvertTimeToUtc((DateTime)Date, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
            else
            {
                return TimeZoneInfo.ConvertTimeToUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId));
            }
        }
        public static string GenerateRandomNumber(int length)
        {
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random _Random = new Random(BitConverter.ToInt32(seed, 0));
            int _RandomNumber = 0;
            String _GeneratedRandomNumber = "";
            for (int i = 0; i < length; i++)
            {
                _RandomNumber = _Random.Next(48, 58);
                _GeneratedRandomNumber = _GeneratedRandomNumber + (char)_RandomNumber;
            }
            return _GeneratedRandomNumber;
        }
        public static OList.Request GetSearchCondition(OList.Request _Request, string SearchColumnName, string SearchColumnValue, string SearchConditions)
        {
            string SearchExpression = "";
            if (!string.IsNullOrEmpty(SearchColumnName))
            {
                SearchExpression += string.Format(SearchColumnName + " " + SearchConditions + " {0} ", SearchColumnValue);

                _Request.SearchCondition += !string.IsNullOrEmpty(_Request.SearchCondition) ? " OR " : "";
                _Request.SearchCondition += SearchExpression;

                return _Request;
            }
            else
            {
                return _Request;
            }
        }
        public static string GetSearchCondition(string SearchColumnName, string SearchColumnValue, string SearchConditions)
        {
            if (!string.IsNullOrEmpty(SearchColumnName))
            {
                return string.Format(SearchColumnName + " " + SearchConditions + " {0} ", SearchColumnValue);
            }
            else
            {
                return null;
            }
        }
        public static OList.Request GetSortCondition(OList.Request _Request, string FieldName, string SortOrder)
        {
            #region Sort Order
            string SortCondition = "";
            if (!string.IsNullOrEmpty(FieldName))
            {
                if (!string.IsNullOrEmpty(SortOrder))
                {
                    SortCondition = FieldName + " " + SortOrder;
                }
            }
            _Request.SortExpression = SortCondition;
            return _Request;
            #endregion
        }
        public static OList.Response GetListResponse(int TotalRecords, object Data, int Offset, int Limit)
        {
            OList.Response _OListResponse = new OList.Response();
            _OListResponse.TotalRecords = TotalRecords;
            _OListResponse.Data = Data;
            _OListResponse.Offset = Offset;
            _OListResponse.Limit = Limit;
            return _OListResponse;
        }
        public static bool WriteFile(string FileName, string Content)
        {
            try
            {
                string ItemUrl = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"files/" + FileName);
                if (System.IO.File.Exists(ItemUrl))
                {
                    System.IO.File.WriteAllText(ItemUrl, Content);
                }
                else
                {
                    System.IO.File.Create(ItemUrl).Close();
                    System.IO.File.WriteAllText(ItemUrl, Content);
                }
                return true;
            }
            catch (Exception _Exception)
            {

                LogException("CORE-WriteFile", _Exception);
                return false;
            }
        }
        public static string ReadFile(string FileName)
        {
            try
            {
                string ItemUrl = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"files/" + FileName);
                if (File.Exists(ItemUrl))
                {
                    return System.IO.File.ReadAllText(ItemUrl);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {

                LogException("CORE-ReadFile", _Exception);
                return null;
            }
        }
        public static void UpdateSmsNotification(string Content)
        {

            var system = ActorSystem.Create("ActorSmsBroadcastUpdateHandler");
            var greeter = system.ActorOf<ActorSmsBroadcastUpdateHandler>("ActorSmsBroadcastUpdateHandler");
            greeter.Tell(Content);
        }
        public static OResponse SendResponse(OUserReference? UserReference, ResponseStatus Status, object? Result, string ResponseCode, string Message)
        {

            string StatusText = Status == ResponseStatus.Success ? "Success" : "Error";
            OResponse _OResponse = new OResponse();
            _OResponse.Message = Message;
            _OResponse.Status = StatusText;
            _OResponse.Result = Result;
            _OResponse.ResponseCode = ResponseCode;
            switch (HostEnvironment)
            {
                case HostEnvironmentType.BackgroudProcessor:
                    _OResponse.Mode = "bot";
                    break;
                case HostEnvironmentType.Test:
                    _OResponse.Mode = "test";
                    break;
                case HostEnvironmentType.Tech:
                    _OResponse.Mode = "tech";
                    break;
                case HostEnvironmentType.Dev:
                    _OResponse.Mode = "dev";
                    break;
                case HostEnvironmentType.Local:
                    _OResponse.Mode = "local";
                    break;
                case HostEnvironmentType.Play:
                    _OResponse.Mode = "play";
                    break;
                default:
                    break;
            }
            if (UserReference != null && !string.IsNullOrEmpty(UserReference.RequestKey))
            {
                try
                {
                    UserReference.ResponseData = _OResponse;
                    UserReference.ResponseStatus = StatusText;
                    UserReference.ResponseTime = HCoreHelper.GetGMTDateTime();
                    var system = ActorSystem.Create("ActorCoreUsageHandlerUSAGE");
                    var greeter = system.ActorOf<ActorCoreUsageHandler>("ActorCoreUsageHandlerUSAGE");
                    greeter.Tell(UserReference);
                }
                catch (Exception _Exception)
                {
                    //LogException("AKKA-SendResponse-CUSTOM-MSG-FAILED", _Exception);
                }
            }
            return _OResponse;
        }
        public static OResponse SendResponseTerminal(OUserReference? UserReference, ResponseStatus Status, object Result, string ResponseCode, string Message)
        {

            string StatusText = Status == ResponseStatus.Success ? "Success" : "Error";
            OResponse _OResponse = new OResponse();
            _OResponse.Message = Message;
            _OResponse.Status = StatusText;
            _OResponse.Result = Result;
            _OResponse.ResponseCode = ResponseCode;
            switch (HostEnvironment)
            {
                case HostEnvironmentType.BackgroudProcessor:
                    _OResponse.Mode = "bot";
                    break;
                case HostEnvironmentType.Test:
                    _OResponse.Mode = "test";
                    break;
                case HostEnvironmentType.Tech:
                    _OResponse.Mode = "tech";
                    break;
                case HostEnvironmentType.Dev:
                    _OResponse.Mode = "dev";
                    break;
                case HostEnvironmentType.Local:
                    _OResponse.Mode = "local";
                    break;
                case HostEnvironmentType.Play:
                    _OResponse.Mode = "play";
                    break;
                default:
                    break;
            }
            if (UserReference != null && !string.IsNullOrEmpty(UserReference.RequestKey))
            {
                try
                {
                    UserReference.ResponseData = _OResponse;
                    UserReference.ResponseStatus = StatusText;
                    UserReference.ResponseTime = HCoreHelper.GetGMTDateTime();
                    var system = ActorSystem.Create("ActorCoreTerminalUsageHandler");
                    var greeter = system.ActorOf<ActorCoreTerminalUsageHandler>("ActorCoreTerminalUsageHandler");
                    greeter.Tell(UserReference);
                }
                catch (Exception _Exception)
                {
                    //LogException("AKKA-SendResponse-CUSTOM-MSG-FAILED", _Exception);
                }
            }
            return _OResponse;
        }

        // Start async
        public static async Task<OResponse> SendResponseAsync(OUserReference? UserReference, ResponseStatus Status, object Result, string ResponseCode, string Message)
        {
            string StatusText = Status == ResponseStatus.Success ? "Success" : "Error";
            OResponse _OResponse = new OResponse();
            _OResponse.Message = Message;
            _OResponse.Status = StatusText;
            _OResponse.Result = Result;
            _OResponse.ResponseCode = ResponseCode;
            switch (HostEnvironment)
            {
                case HostEnvironmentType.BackgroudProcessor:
                    _OResponse.Mode = "bot";
                    break;
                case HostEnvironmentType.Test:
                    _OResponse.Mode = "test";
                    break;
                case HostEnvironmentType.Tech:
                    _OResponse.Mode = "tech";
                    break;
                case HostEnvironmentType.Dev:
                    _OResponse.Mode = "dev";
                    break;
                case HostEnvironmentType.Local:
                    _OResponse.Mode = "local";
                    break;
                case HostEnvironmentType.Play:
                    _OResponse.Mode = "play";
                    break;
                default:
                    break;
            }
            if (UserReference != null && !string.IsNullOrEmpty(UserReference.RequestKey))
            {
                try
                {
                    UserReference.ResponseData = _OResponse;
                    UserReference.ResponseStatus = StatusText;
                    UserReference.ResponseTime = HCoreHelper.GetGMTDateTime();
                    var system = ActorSystem.Create("ActorCoreUsageHandlerUSAGE");
                    var greeter = system.ActorOf<ActorCoreUsageHandler>("ActorCoreUsageHandlerUSAGE");
                    greeter.Tell(UserReference);
                    await Task.Delay(0);
                }
                catch (Exception _Exception)
                {
                    //LogException("AKKA-SendResponse-CUSTOM-MSG-FAILED", _Exception);
                }
            }
            return _OResponse;
        }
        // Finsh Asyn

        public static OResponse SendResponse(OUserReference? UserReference, ResponseStatus Status, object? Result, string ResponseCode)
        {
            string Task = null;
            if (UserReference != null)
            {
                Task = UserReference.Task;
            }
            string StatusText = Status == ResponseStatus.Success ? "Success" : "Error";
            OResponse _OResponse = new OResponse();
            string ResourceDetails = HCoreDataStore.SystemResources.Where(x => x.SystemName == ResponseCode).Select(x => x.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(ResourceDetails))
            {
                //_OResponse.Task = Task;
                _OResponse.Message = ResourceDetails;
                _OResponse.Status = StatusText;
                _OResponse.Result = Result;
                _OResponse.ResponseCode = ResponseCode;
            }
            else
            {
                //_OResponse.Task = Task;
                _OResponse.Message = "";
                _OResponse.Status = StatusText;
                _OResponse.Result = Result;
                _OResponse.ResponseCode = ResponseCode;
            }
            switch (HostEnvironment)
            {
                case HostEnvironmentType.BackgroudProcessor:
                    _OResponse.Mode = "bot";
                    break;
                case HostEnvironmentType.Test:
                    _OResponse.Mode = "test";
                    break;
                case HostEnvironmentType.Tech:
                    _OResponse.Mode = "tech";
                    break;
                case HostEnvironmentType.Dev:
                    _OResponse.Mode = "dev";
                    break;
                case HostEnvironmentType.Live:
                    _OResponse.Mode = "live";
                    break;
                case HostEnvironmentType.Local:
                    _OResponse.Mode = "local";
                    break;
                case HostEnvironmentType.Play:
                    _OResponse.Mode = "play";
                    break;
                default:
                    _OResponse.Mode = "UFO";
                    break;
            }
            if (UserReference != null)
            {
                if (!string.IsNullOrEmpty(UserReference.RequestKey))
                {
                    try
                    {
                        UserReference.ResponseStatus = StatusText;
                        UserReference.ResponseData = _OResponse;
                        UserReference.ResponseTime = HCoreHelper.GetGMTDateTime();
                        var _ActorCoreUsageHandlerLogger = ActorSystem.Create("ActorCoreUsageHandlerLogger");
                        var _ActorCoreUsageHandlerLoggerGreet = _ActorCoreUsageHandlerLogger.ActorOf<ActorCoreUsageHandler>("ActorCoreUsageHandlerLogger");
                        _ActorCoreUsageHandlerLoggerGreet.Tell(UserReference);
                    }
                    catch (Exception _Exception)
                    {
                        //LogException("AKKA-SendResponse-CUSTOM-MSG", _Exception);
                    }
                }
            }
            return _OResponse;
        }

        public static long CreateDownload(string? Title, IEnumerable<object> Data, OUserReference UserReference)
        {
            try
            {

                using (var _XLWorkbook = new XLWorkbook())
                {
                    var _WorkSheet = _XLWorkbook.Worksheets.Add(Title);
                    PropertyInfo[] properties = Data.First().GetType().GetProperties();
                    List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                    for (int i = 0; i < headerNames.Count; i++)
                    {
                        _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                    }
                    _WorkSheet.Cell(2, 1).InsertData(Data);
                    MemoryStream _MemoryStream = new MemoryStream();
                    _XLWorkbook.SaveAs(_MemoryStream);
                    long? _StorageId = HCoreHelper.SaveStorage(Title, "xlsx", _MemoryStream, UserReference);
                    if (_StorageId != null && _StorageId != 0)
                    {

                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            HCUAccountParameter _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                            _HCUAccountParameter.Name = Title;
                            _HCUAccountParameter.IconStorageId = _StorageId;
                            _HCUAccountParameter.CreateDate = UserReference.RequestTime;
                            _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreatedById = UserReference.AccountId;
                            _HCUAccountParameter.ModifyById = UserReference.AccountId;
                            _HCUAccountParameter.AccountId = UserReference.AccountId;
                            _HCUAccountParameter.StatusId = HelperStatus.Download.Completed;
                            _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            _HCoreContext.SaveChanges();
                            return _HCUAccountParameter.Id;
                        }
                    }
                    else
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            HCUAccountParameter _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                            _HCUAccountParameter.Name = Title;
                            _HCUAccountParameter.CreateDate = UserReference.RequestTime;
                            _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreatedById = UserReference.AccountId;
                            _HCUAccountParameter.ModifyById = UserReference.AccountId;
                            _HCUAccountParameter.AccountId = UserReference.AccountId;
                            _HCUAccountParameter.StatusId = HelperStatus.Download.Failed;
                            _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            _HCoreContext.SaveChanges();
                            return _HCUAccountParameter.Id;
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                LogException("CreateDownload", _Exception);
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    HCUAccountParameter _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = Title;
                    _HCUAccountParameter.CreateDate = UserReference.RequestTime;
                    _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = UserReference.AccountId;
                    _HCUAccountParameter.ModifyById = UserReference.AccountId;
                    _HCUAccountParameter.AccountId = UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Download.Failed;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    return _HCUAccountParameter.Id;
                }
            }
            //using (HCoreContext _HCoreContext = new HCoreContext())
            //{
            //    HCUAccountParameter _HCUAccountParameter = new HCUAccountParameter();
            //    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
            //    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
            //    _HCUAccountParameter.Name = Title;
            //    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
            //    _HCUAccountParameter.CreatedById = UserReference.AccountId;
            //    _HCUAccountParameter.AccountId = UserReference.AccountId;
            //    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
            //    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
            //    _HCoreContext.SaveChanges();
            //    return _HCUAccountParameter.Id;
            //}
        }
        public static void UpdateDownloadStatus(long ReferenceId, long StorageId, OUserReference UserReference)
        {
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == ReferenceId).FirstOrDefault();
                if (TItem != null)
                {
                    TItem.IconStorageId = StorageId;
                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                    TItem.ModifyById = UserReference.AccountId;
                    TItem.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
        }

        public static OResponse LogException(string Title, Exception _Exception, OUserReference UserReference)
        {
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.Data = _Exception.ToString();
                _HCLCoreLog.HostName = HostName;
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
                return SendResponse(UserReference, ResponseStatus.Error, null, "HCER500", Resource.ResourceEn.HC500);
            }
        }
        public static OResponse LogException(string Title, Exception _Exception, OUserReference UserReference, string ResponseCode, string ResponseMessage)
        {
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())

            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.Data = _Exception.ToString();
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.CreateDate = HCoreHelper.GetGMTDateTime();
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            return SendResponse(UserReference, ResponseStatus.Error, null, ResponseCode, ResponseMessage);
        }
        public static async Task SaveLogAsync(string Name, string Comment)
        {
            try
            {
                using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
                {
                    HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                    _HCLCoreLog.Title = Name;
                    _HCLCoreLog.HostName = HostName;
                    _HCLCoreLog.Message = Comment;
                    _HCLCoreLog.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                    await _HCoreContextLogging.SaveChangesAsync();
                    _HCoreContextLogging.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveLogAsync", _Exception);
            }
        }
        public static void SaveLog(string Name, string Comment)
        {
            try
            {
                using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
                {
                    HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                    _HCLCoreLog.Title = Name;
                    _HCLCoreLog.HostName = HostName;
                    _HCLCoreLog.Message = Comment;
                    _HCLCoreLog.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                    _HCoreContextLogging.SaveChanges();
                    _HCoreContextLogging.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveLogAsync", _Exception);
            }

        }
        public static OResponse LogException(string Title, Exception _Exception, OUserReference UserReference, object Result, string ResponseCode, string ResponseMessage)
        {
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.HostName = HostName;

                _HCLCoreLog.Data = _Exception.ToString();
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            return SendResponse(UserReference, ResponseStatus.Error, Result, ResponseCode, ResponseMessage);
        }
        public static void LogException(string Title, Exception _Exception)
        {
            #region Code Block
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.Data = _Exception.ToString();
                _HCLCoreLog.CreateDate = HCoreHelper.GetGMTDateTime();
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            #endregion
        }
        public static OResponse LogException(LogLevel Level, string Title, Exception _Exception, OUserReference UserReference)
        {
            #region Code Block

            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.Data = _Exception.ToString();
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
            }
            #endregion
            #region Send Response
            return SendResponse(UserReference, ResponseStatus.Error, _Exception.ToString(), "HC500", Resource.ResourceEn.HC500);
            #endregion
        }
        public static async Task<OResponse> LogExceptionAsync(LogLevel Level, string Title, Exception _Exception, OUserReference UserReference)
        {
            #region Code Block

            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.Data = _Exception.ToString();
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                await _HCoreContextLogging.SaveChangesAsync();
            }
            #endregion
            #region Send Response
            return SendResponse(UserReference, ResponseStatus.Error, _Exception.ToString(), "HC500", Resource.ResourceEn.HC500);
            #endregion
        }
        public static async void LogExceptionAsync(string Title, Exception _Exception, OUserReference UserReference)
        {
            #region Code Block

            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.Message = _Exception.Message;
                _HCLCoreLog.Data = _Exception.ToString();
                if (UserReference != null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                await _HCoreContextLogging.SaveChangesAsync();
            }
            #endregion
        }
        public static void LogData(LogType LogType, string Title, string Message, string Data)
        {
            #region Code Block
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.Message = Message;
                _HCLCoreLog.Data = Data;
                _HCLCoreLog.HostName = HostName;
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            #endregion
        }
        public static async void LogData(LogType LogType, string Title, string Message, string Data, OUserReference? UserReference)
        {
            #region Code Block

            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                HCLCoreLog _HCLCoreLog = new HCLCoreLog();
                _HCLCoreLog.Title = Title;
                _HCLCoreLog.Message = Message;
                _HCLCoreLog.Data = Data;
                _HCLCoreLog.HostName = HostName;
                _HCLCoreLog.CreateDate = HCoreHelper.GetGMTDateTime();
                if (UserReference is not null)
                {
                    _HCLCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.HCLCoreLog.Add(_HCLCoreLog);
                await _HCoreContextLogging.SaveChangesAsync();
            }
            #endregion
        }
        public static void LogActivity(HCoreContext _HCoreContext, OUserReference _UserReference)
        {
            if (_HCoreContext.ChangeTracker.HasChanges())
            {
                OUserActivityLog _UserActivity = new OUserActivityLog();
                List<OChangeLog> _ChangeLogList = new List<OChangeLog>();
                OChangeLog _ChangeLog = new OChangeLog();
                var _DataChangeLog = _HCoreContext.ChangeTracker
                                                .Entries()
                                                .Where(x => x.State != EntityState.Unchanged)
                                                .Select(x => new
                                                {
                                                    State = x.State,
                                                    Original = x.OriginalValues.Properties.ToDictionary(pn => pn, pn => x.OriginalValues[pn]),
                                                    Current = x.CurrentValues.Properties.ToDictionary(pn => pn, pn => x.CurrentValues[pn]),
                                                    Entity = x.Entity.GetType().Name,
                                                })
                                                .ToList();
                if (_DataChangeLog.Count > 0)
                {
                    foreach (var _DataChanges in _DataChangeLog)
                    {

                        _UserActivity.EntityName = _DataChanges.Entity;
                        if (_DataChanges.State == EntityState.Added)
                        {
                        }
                        else if (_DataChanges.State == EntityState.Modified)
                        {
                            var ReferenceField = _DataChanges.Original.Where(x => x.Key.Name == "Guid").Select(x => x.Value).FirstOrDefault();
                            if (ReferenceField != null)
                            {
                                _UserActivity.ReferenceKey = ReferenceField.ToString();
                            }

                            foreach (var item in _DataChanges.Original)
                            {
                                string Field = item.Key.Name;
                                if (Field != "Guid" &&
                                    Field != "CreateDate" &&
                                    Field != "CreatedBy" &&
                                    Field != "ModifyDate" &&
                                    Field != "ModifyBy" &&
                                    Field != "CreatedById" &&
                                    Field != "ModifyById"
                                   )
                                {

                                    object OldValue = item.Value;
                                    object NewValue = _DataChanges.Current[item.Key];


                                    if (OldValue != NewValue && (OldValue == null || !OldValue.Equals(NewValue)))
                                    {

                                        if (OldValue == null && NewValue != null)
                                        {
                                            _ChangeLog.ActivityType = ActivityType.Add;
                                        }
                                        if (OldValue != null && NewValue != null)
                                        {
                                            _ChangeLog.ActivityType = ActivityType.Update;
                                        }
                                        if (OldValue != null && NewValue == null)
                                        {
                                            _ChangeLog.ActivityType = ActivityType.Delete;
                                        }
                                        _ChangeLog.FieldName = Field;

                                        if (OldValue != null)
                                        {
                                            _ChangeLog.OldValue = OldValue.ToString();
                                        }
                                        if (NewValue != null)
                                        {
                                            _ChangeLog.NewValue = NewValue.ToString();
                                        }
                                        _ChangeLogList.Add(_ChangeLog);
                                    }
                                }

                            }
                        }
                        else if (_DataChanges.State == EntityState.Deleted)
                        {

                        }
                        if (_ChangeLogList.Count() > 0)
                        {
                            _UserActivity.ChangeLog = _ChangeLogList;
                            _UserActivity.UserReference = _UserReference;
                            if (_UserActivity != null)
                            {
                                if (_UserActivity.ChangeLog != null)
                                {
                                    if (_UserActivity.ChangeLog.Count > 0)
                                    {
                                        foreach (var Activity in _UserActivity.ChangeLog)
                                        {
                                            #region Code Block
                                            HCUAccountActivity _HCUAccountActivity = new HCUAccountActivity();
                                            _HCUAccountActivity.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountActivity.AccountId = _UserReference.AccountId;
                                            _HCUAccountActivity.DeviceId = _UserReference.DeviceId;
                                            _HCUAccountActivity.ActivityTypeId = (int)Activity.ActivityType;
                                            _HCUAccountActivity.SubReferenceKey = _UserActivity.ParentReferenceKey;
                                            _HCUAccountActivity.ReferenceKey = _UserActivity.ReferenceKey;
                                            _HCUAccountActivity.EntityName = _UserActivity.EntityName;
                                            _HCUAccountActivity.FieldName = Activity.FieldName;
                                            _HCUAccountActivity.OldValue = Activity.OldValue;
                                            _HCUAccountActivity.NewValue = Activity.NewValue;
                                            _HCUAccountActivity.Description = _UserReference.Data;
                                            _HCUAccountActivity.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountActivity.StatusId = 2;
                                            _HCoreContext.HCUAccountActivity.Add(_HCUAccountActivity);
                                            #endregion
                                        }

                                    }
                                }
                            }
                        }
                    }

                }

            }
        }
        public static int? GetSystemHelperId(string ReferenceCode, long TypeId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceCode))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    int? Value = _HCoreContext.HCCore.Where(x => x.ParentId == TypeId && (x.SystemName == ReferenceCode)).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static int? GetSystemHelperId(string ReferenceCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceCode))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    int? Value = _HCoreContext.HCCore.Where(x => x.SystemName == ReferenceCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static int GetStatusId(string StatusCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(StatusCode))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    int Value = _HCoreContext.HCCore.Where(x => x.SystemName == StatusCode).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }
        public static int GetHelperId(string StatusCode)
        {
            if (!string.IsNullOrEmpty(StatusCode))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    int Value = _HCoreContext.HCCore.Where(x => x.SystemName == StatusCode).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }
        public static long GetCoreCommonId(string ReferenceKey)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long Value = _HCoreContext.HCCoreCommon.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }
        public static long GetCoreCommonId(string ReferenceKey, long TypeId)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long Value = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == TypeId && (x.Guid == ReferenceKey || x.SystemName == ReferenceKey)).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }
        public static int? GetCountryId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    int? Value = _HCoreContext.HCCoreCountry.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        private static List<OCoreCountry> _Countries = new List<OCoreCountry>();
        public static OCoreCountry GetCountry(int? ReferenceId)
        {
            if (ReferenceId != null)
            {
                if (_Countries.Where(x => x.ReferenceId == ReferenceId).Any())
                {
                    var Country = _Countries.Where(x => x.ReferenceId == ReferenceId).FirstOrDefault();
                    Country.GmtTime = HCoreHelper.GetGMTDateTime();
                    Country.LocalTime = HCoreHelper.GetDateTime(Country.GmtTime, Country.TimeZoneName);
                    return Country;
                }
                else
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        var Details = _HCoreContext.HCCoreCountry.Where(x => x.Id == ReferenceId)
                            .Select(x => new OCoreCountry
                            {
                                ReferenceId = x.Id,
                                Name = x.Guid,
                                Isd = x.Isd,
                                MobileNumberLength = x.MobileNumberLength,
                                TimeZoneName = x.TimeZoneName,
                            })
                            .FirstOrDefault();
                        _Countries.Add(Details);
                        Details.GmtTime = HCoreHelper.GetGMTDateTime();
                        Details.LocalTime = HCoreHelper.GetDateTime(Details.GmtTime, Details.TimeZoneName);
                        return Details;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetCoreCommonId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCCoreCommon.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static long? GetUserAccountId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCUAccount.Where(x => x.Guid == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static long GetUserAccountId(long ReferenceId, string ReferenceKey, long AccountTypeId, OUserReference UserReference)
        {
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                return _HCoreContext.HCUAccount.Where(x => x.Id == ReferenceId && x.Guid == ReferenceKey && x.AccountTypeId == AccountTypeId).Select(x => x.Id).FirstOrDefault();
            }
        }

        public static long? GetUserAccountId(long ReferenceId, string ReferenceKey, string AccountTypeCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long Value = _HCoreContext.HCUAccount.Where(x => x.Id == ReferenceId && x.Guid == ReferenceKey && x.AccountType.SystemName == AccountTypeCode).Select(x => x.Id).FirstOrDefault();
                    if (Value > 0)
                    {
                        return Value;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static long? GetUserDeviceId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCUAccountDevice.Where(x => x.Guid == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static long? GetUserId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCUAccountAuth.Where(x => x.Guid == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static OAddressResponse GetAddressComponent(OAddress _Address, OUserReference UserReference)
        {
            if (_Address != null)
            {
                //if (string.IsNullOrEmpty(_Address.CountryName))
                //{
                //    _Address.CountryName = "Nigeria";
                //}
                OAddressResponse _AddressResponse = new OAddressResponse();
                int CountryId = 0;
                long StateId = 0;
                long CityId = 0;
                long CityAreaId = 0;
                if (_Address.CountryId > 0)
                {
                    CountryId = (int)_Address.CountryId;
                }
                if (_Address.StateId > 0)
                {
                    StateId = (long)_Address.StateId;
                }
                if (_Address.CityId > 0)
                {
                    CityId = (long)_Address.CityId;
                }
                if (_Address.CityAreaId > 0)
                {
                    CityAreaId = (long)_Address.CityAreaId;
                }
                if (CountryId < 1 && !string.IsNullOrEmpty(_Address.CountryName))
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        string SystemName = GenerateSystemName(_Address.CountryName);
                        CountryId = _HCoreContext.HCCoreCountry.Where(x => x.SystemName == SystemName).Select(x => x.Id).FirstOrDefault();
                        if (CountryId == 0)
                        {
                            HCCoreCountry _HCCoreCountry = new HCCoreCountry
                            {
                                Guid = GenerateGuid(),
                                Name = _Address.CountryName,
                                SystemName = SystemName,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                StatusId = HelperStatus.Default.Active
                            };
                            if (UserReference != null && UserReference.AccountId > 0)
                            {
                                _HCCoreCountry.CreatedById = UserReference.AccountId;
                            }
                            else
                            {
                                _HCCoreCountry.CreatedById = 1;
                            }
                            _HCoreContext.HCCoreCountry.Add(_HCCoreCountry);
                            _HCoreContext.SaveChanges();
                            CountryId = _HCCoreCountry.Id;
                            _HCoreContext.Dispose();
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                        }
                    }
                }
                if (StateId < 1 && !string.IsNullOrEmpty(_Address.StateName))
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        string SystemName = GenerateSystemName(_Address.StateName);
                        StateId = _HCoreContext.HCCoreCountryState.Where(x => x.CountryId == CountryId && x.SystemName == SystemName).Select(x => x.Id).FirstOrDefault();
                        if (StateId == 0)
                        {
                            HCCoreCountryState _HCCoreCountryState = new HCCoreCountryState
                            {
                                Guid = GenerateGuid(),
                                CountryId = CountryId,
                                Name = _Address.StateName,
                                SystemName = SystemName,
                                Latitude = 0,
                                Longitude = 0,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                StatusId = HelperStatus.Default.Active
                            };
                            if (UserReference != null && UserReference.AccountId > 0)
                            {
                                _HCCoreCountryState.CreatedById = UserReference.AccountId;
                            }
                            else
                            {
                                _HCCoreCountryState.CreatedById = 1;
                            }
                            _HCoreContext.HCCoreCountryState.Add(_HCCoreCountryState);
                            _HCoreContext.SaveChanges();
                            StateId = _HCCoreCountryState.Id;
                            _HCoreContext.Dispose();
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                        }
                    }
                }
                if (StateId > 1 && CityId < 1 && !string.IsNullOrEmpty(_Address.CityName))
                {
                    if (!string.IsNullOrEmpty(_Address.CityName))
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            string SystemName = GenerateSystemName(_Address.CityName);
                            CityId = _HCoreContext.HCCoreCountryStateCity.Where(x => x.StateId == StateId && x.SystemName == SystemName).Select(x => x.Id).FirstOrDefault();
                            if (CityId == 0)
                            {
                                HCCoreCountryStateCity _HCCoreCountryStateCity = new HCCoreCountryStateCity
                                {
                                    Guid = GenerateGuid(),
                                    StateId = StateId,
                                    Name = _Address.CityName,
                                    SystemName = SystemName,
                                    Latitude = 0,
                                    Longitude = 0,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = HelperStatus.Default.Active
                                };
                                if (UserReference != null && UserReference.AccountId > 0)
                                {
                                    _HCCoreCountryStateCity.CreatedById = UserReference.AccountId;
                                }
                                else
                                {
                                    _HCCoreCountryStateCity.CreatedById = 1;
                                }
                                _HCoreContext.HCCoreCountryStateCity.Add(_HCCoreCountryStateCity);
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                                CityId = _HCCoreCountryStateCity.Id;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                }
                if (CityId > 1 && CityAreaId < 1 && !string.IsNullOrEmpty(_Address.CityAreaName))
                {
                    if (!string.IsNullOrEmpty(_Address.CityAreaName))
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            string SystemName = GenerateSystemName(_Address.CityAreaName);
                            CityAreaId = _HCoreContext.HCCoreCountryStateCityArea.Where(x => x.CityId == CityId && x.SystemName == SystemName).Select(x => x.Id).FirstOrDefault();
                            if (CityAreaId == 0)
                            {
                                HCCoreCountryStateCityArea _HCCoreCountryStateCityArea = new HCCoreCountryStateCityArea
                                {
                                    Guid = GenerateGuid(),
                                    CityId = CityId,
                                    Name = _Address.CityAreaName,
                                    SystemName = SystemName,
                                    Latitude = 0,
                                    Longitude = 0,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = HelperStatus.Default.Active
                                };
                                if (UserReference != null && UserReference.AccountId > 0)
                                {
                                    _HCCoreCountryStateCityArea.CreatedById = UserReference.AccountId;
                                }
                                else
                                {
                                    _HCCoreCountryStateCityArea.CreatedById = 1;
                                }
                                _HCoreContext.HCCoreCountryStateCityArea.Add(_HCCoreCountryStateCityArea);
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                                CityAreaId = _HCCoreCountryStateCityArea.Id;
                            }
                            _HCoreContext.Dispose();
                        }
                    }
                }

                _AddressResponse.Address = _Address.Address;
                if (_Address.Latitude != null)
                {
                    _AddressResponse.Latitude = (double)_Address.Latitude;
                }
                if (_Address.Longitude != null)
                {
                    _AddressResponse.Longitude = (double)_Address.Longitude;
                }
                _AddressResponse.CityAreaId = CityAreaId;
                _AddressResponse.CityAreaName = _Address.CityAreaName;
                _AddressResponse.CityId = CityId;
                _AddressResponse.CityName = _Address.CityName;
                _AddressResponse.StateId = StateId;
                _AddressResponse.StateName = _Address.StateName;
                _AddressResponse.CountryId = CountryId;
                _AddressResponse.CountryName = _Address.CountryName;
                return _AddressResponse;
            }
            else
            {
                return null;
            }
        }
        public static long? GetCoreParameterId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCCoreParameter.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static long GetCoreParameterId(string ReferenceKey)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long Value = _HCoreContext.HCCoreParameter.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }
        public static long GetRoleId(string ReferenceKey)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long Value = _HCoreContext.HCCoreRole.Where(x => x.Guid == ReferenceKey || x.SystemName == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    _HCoreContext.Dispose();
                    return Value;
                }
            }
            else
            {
                return 0;
            }
        }

        public static long? GetUserParameterId(string ReferenceKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceKey))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    long? Value = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static string GetConfigurationValueByUserAccount(string ConfigurationCode, long AccountId, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).DeferredFirstOrDefault().FromCache();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (AccountId != 0)
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                where n.CommonId == _Configuration.ReferenceId && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).DeferredFirstOrDefault().FromCache();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfigurationValue", _Exception, UserReference);
                #endregion
                return "0";
            }
            #endregion
        }
        public static string GetConfigurationValueByUserAccount(string ConfigurationCode, long AccountId, long ParentId, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).DeferredFirstOrDefault().FromCache();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (AccountId != 0)
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                where n.CommonId == _Configuration.ReferenceId && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).DeferredFirstOrDefault().FromCache();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                            else
                            {
                                string ParentConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                   where n.CommonId == _Configuration.ReferenceId && n.AccountId == ParentId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                   select n.Value).DeferredFirstOrDefault().FromCache();
                                if (!string.IsNullOrEmpty(ParentConfigurationValue))
                                {
                                    ConfigValue = ParentConfigurationValue;
                                }
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfigurationValue", _Exception, UserReference);
                #endregion
                return "0";
            }
            #endregion
        }
        public static string GetConfigurationValueByUserAccountActive(string ConfigurationCode, long AccountId, long ParentId, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).FirstOrDefault();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).FirstOrDefault();

                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (AccountId != 0)
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                where n.CommonId == _Configuration.ReferenceId && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                            else
                            {
                                string ParentConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                   where n.CommonId == _Configuration.ReferenceId && n.AccountId == ParentId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                   select n.Value).FirstOrDefault();
                                if (!string.IsNullOrEmpty(ParentConfigurationValue))
                                {
                                    ConfigValue = ParentConfigurationValue;
                                }
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfigurationValue", _Exception, UserReference);
                #endregion
                return "0";
            }
            #endregion
        }
        public static string GetAccountConfiguration(string ConfigurationCode, long AccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    string ConfigValue = "0";
                    if (AccountId != 0)
                    {
                        string UserConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                         where n.Common.SystemName == ConfigurationCode && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                         select n.Value).FirstOrDefault();
                        if (!string.IsNullOrEmpty(UserConfigurationValue))
                        {
                            ConfigValue = UserConfigurationValue;
                        }
                    }
                    _HCoreContext.Dispose();
                    #region Send Response
                    if (!string.IsNullOrEmpty(ConfigValue))
                    {
                        return ConfigValue;
                    }
                    else
                    {
                        return "0";
                    }
                    #endregion


                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfiguration1", _Exception);
                #endregion
                return "0";
            }
            #endregion
        }

        public static string GetConfiguration(string ConfigurationCode, long? AccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).FirstOrDefault();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (AccountId != 0)
                        {
                            string UserConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                             where n.CommonId == _Configuration.ReferenceId && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                             select n.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(UserConfigurationValue))
                            {
                                ConfigValue = UserConfigurationValue;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfiguration1", _Exception);
                #endregion
                return "0";
            }
            #endregion
        }
        public static string GetConfiguration(string ConfigurationCode, string UserAccountKey)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).DeferredFirstOrDefault().FromCache();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (!string.IsNullOrEmpty(UserAccountKey))
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                where n.CommonId == _Configuration.ReferenceId && n.Account.Guid == UserAccountKey && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).DeferredFirstOrDefault().FromCache();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfiguration1", _Exception);
                #endregion
                return "0";
            }
            #endregion
        }
        public static OConfiguration GetConfigurationDetails(string ConfigurationCode, long AccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    OConfiguration _OConfiguration = (from n in _HCoreContext.HCCoreCommon
                                                      where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                                      select new OConfiguration
                                                      {
                                                          Name = n.Name,
                                                          ReferenceId = n.Id,
                                                          Value = n.Value,
                                                          TypeCode = n.Helper.SystemName,
                                                          TypeName = n.Helper.Name,
                                                      }).DeferredFirstOrDefault().FromCache();
                    if (_OConfiguration != null)
                    {
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _OConfiguration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            _OConfiguration.Value = ConfigurationValue;
                        }
                        if (AccountId != 0)
                        {
                            var CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                             where n.CommonId == _OConfiguration.ReferenceId && n.AccountId == AccountId && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                             select new
                                                             {

                                                                 Value = n.Value,
                                                                 HelperCode = n.Helper.SystemName,
                                                                 HelperName = n.Helper.Name,
                                                             }).DeferredFirstOrDefault().FromCache();
                            if (CountryConfigurationValue != null)
                            {
                                _OConfiguration.Name = _OConfiguration.Name;
                                _OConfiguration.Value = CountryConfigurationValue.Value;
                                _OConfiguration.TypeCode = CountryConfigurationValue.HelperCode;
                                _OConfiguration.TypeName = CountryConfigurationValue.HelperName;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        return _OConfiguration;
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return null;
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfiguration1", _Exception);
                #endregion
                return null;
            }
            #endregion
        }
        public static string GetConfiguration(string ConfigurationCode)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == ConfigurationCode && n.StatusId == HCoreConstant.HelperStatus.Default.Active && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).DeferredFirstOrDefault().FromCache();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).DeferredFirstOrDefault().FromCache();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                LogException("GetConfiguration2", _Exception);
                #endregion
                return "0";
            }
            #endregion
        }

        public static bool DeleteStorage(long? StorageId, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                #region Delete old Image Operations
                if (StorageId != null && StorageId != 0)
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        var FileDetails = _HCoreContext.HCCoreStorage.Where(x => x.Id == StorageId).FirstOrDefault();
                        if (FileDetails != null)
                        {
                            #region Get Settings 
                            if (_AppConfig != null)
                            {
                                if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                                {
                                    bool IsFileDeleted = false;
                                    #region Save Image To S3
                                    //GoogleCredential _GoogleCredential = null;
                                    //using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                    //    FileAccess.Read, FileShare.Read))
                                    //{
                                    //    _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                                    //}
                                    //var _StorageClient = StorageClient.Create(_GoogleCredential);
                                    //var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                    //{
                                    //    Bucket = _AppConfig.StorageLocation,
                                    //    Name = FileDetails.Path,
                                    //};
                                    //try
                                    //{
                                    //    _StorageClient.DeleteObject(_UploadObject);
                                    //    IsFileDeleted = true;
                                    //}
                                    //catch (Exception _Exception)
                                    //{
                                    //    IsFileDeleted = false;
                                    //    LogException("GCloud-DELETE", _Exception);
                                    //}

                                    #region Save Image To S3
                                    using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                    {
                                        try
                                        {
                                            DeleteObjectRequest _DeleteObjectRequest = new DeleteObjectRequest
                                            {
                                                BucketName = _AppConfig.StorageLocation,
                                                Key = FileDetails.Path,
                                            };
                                            DeleteObjectResponse _DeleteObjectResponse = _IAmazonS3.DeleteObjectAsync(_DeleteObjectRequest).Result;
                                            IsFileDeleted = true;
                                        }
                                        catch (AmazonS3Exception _AmazonS3Exception)
                                        {
                                            IsFileDeleted = false;
                                            #region  Log Exception
                                            HCoreHelper.LogException("DeleteStorageByReferenceId-AWSSave", _AmazonS3Exception, UserReference);
                                            #endregion
                                        }
                                    }
                                    #endregion
                                    //using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                    //{
                                    //    try
                                    //    {
                                    //        DeleteObjectRequest _DeleteObjectRequest = new DeleteObjectRequest
                                    //        {
                                    //            BucketName = _AppConfig.StorageLocation,
                                    //            Key = FileDetails.Path,
                                    //        };
                                    //        DeleteObjectResponse _DeleteObjectResponse = _IAmazonS3.DeleteObjectAsync(_DeleteObjectRequest).Result;
                                    //        IsFileDeleted = true;
                                    //    }
                                    //    catch (AmazonS3Exception _AmazonS3Exception)
                                    //    {
                                    //        IsFileDeleted = false;
                                    //        #region  Log Exception
                                    //        HCoreHelper.LogException("DeleteStorageByReferenceId-AWSSave", _AmazonS3Exception, UserReference);
                                    //        #endregion
                                    //    }
                                    //}
                                    #endregion
                                    if (IsFileDeleted == true)
                                    {
                                        _HCoreContext.HCCoreStorage.Remove(FileDetails);
                                        LogActivity(_HCoreContext, UserReference);
                                        _HCoreContext.SaveChanges();
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }

                            }
                            else
                            {
                                return false;
                            }
                            #endregion


                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                LogException("DeleteStorage", _Exception, UserReference);
                return false;
            }
            #endregion
        }
        public static bool DeleteStorage(string StoragePath, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                if (!string.IsNullOrEmpty(StoragePath))
                {

                    #region Get Settings 
                    if (_AppConfig != null)
                    {
                        if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                        {
                            GoogleCredential _GoogleCredential = null;
                            using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                FileAccess.Read, FileShare.Read))
                            {
                                _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                            }
                            var _StorageClient = StorageClient.Create(_GoogleCredential);
                            var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                            {
                                Bucket = _AppConfig.StorageLocation,
                                Name = StoragePath,
                            };
                            try
                            {
                                _StorageClient.DeleteObject(_UploadObject);
                                return true;
                            }
                            catch (Exception _Exception)
                            {
                                LogException("GCloud-DELETE-PATH", _Exception);
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    #endregion
                }
                else
                {
                    return false;
                }
            }
            catch (Exception _Exception)
            {
                LogException("DeleteStorage", _Exception, UserReference);
                return false;
            }
            #endregion
        }

        public static long? SaveStorage(string Name, string Extension, string Content, long? DeleteStorageId, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Delete old Image Operations
                if (DeleteStorageId != null && DeleteStorageId != 0)
                {
                    try
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            var FileDetails = _HCoreContext.HCCoreStorage.Where(x => x.Id == DeleteStorageId).FirstOrDefault();
                            if (FileDetails != null)
                            {
                                #region Get Settings 
                                if (_AppConfig != null)
                                {
                                    if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                                    {
                                        bool IsFileDeleted = false;
                                        if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                        {
                                            #region Save Image To S3
                                            using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                            {
                                                try
                                                {
                                                    DeleteObjectRequest _DeleteObjectRequest = new DeleteObjectRequest
                                                    {
                                                        BucketName = _AppConfig.StorageLocation,
                                                        Key = FileDetails.Path,
                                                    };
                                                    DeleteObjectResponse _DeleteObjectResponse = _IAmazonS3.DeleteObjectAsync(_DeleteObjectRequest).Result;
                                                    IsFileDeleted = true;
                                                }
                                                catch (AmazonS3Exception _AmazonS3Exception)
                                                {
                                                    IsFileDeleted = false;
                                                    #region  Log Exception
                                                    HCoreHelper.LogException("DeleteStorageByReferenceId-AWSSave", _AmazonS3Exception, UserReference);
                                                    #endregion
                                                }
                                            }
                                            #endregion
                                        }
                                        else if (_AppConfig.StorageSourceCode == "storagesource.gcloud")
                                        {
                                            GoogleCredential _GoogleCredential = null;
                                            using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                                FileAccess.Read, FileShare.Read))
                                            {
                                                _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                                            }
                                            var _StorageClient = StorageClient.Create(_GoogleCredential);
                                            var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                            {
                                                Bucket = _AppConfig.StorageLocation,
                                                Name = FileDetails.Path,
                                            };
                                            try
                                            {
                                                _StorageClient.DeleteObject(_UploadObject);
                                                IsFileDeleted = true;
                                            }
                                            catch (Exception _Exception)
                                            {
                                                IsFileDeleted = false;
                                                LogException("GCloud-DELETE", _Exception);
                                            }

                                        }
                                        else
                                        {
                                            IsFileDeleted = false;
                                        }
                                        if (IsFileDeleted == true)
                                        {
                                            _HCoreContext.HCCoreStorage.Remove(FileDetails);
                                            HCoreHelper.LogActivity(_HCoreContext, UserReference);
                                            _HCoreContext.SaveChanges();
                                            _HCoreContext.Dispose();
                                        }
                                    }

                                }
                                #endregion
                            }
                            else
                            {

                            }
                        }
                    }
                    catch (Exception _Exception)
                    {
                        LogException("DeleteStorage-CoreHelper", _Exception);
                    }
                }

                #endregion

                if (string.IsNullOrEmpty(Content))
                {
                    return null;
                }
                else
                {
                    #region Perform Operations
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        #region Type Details
                        //long? TypeId = HCoreHelper.GetSystemHelperId(HCoreHelpers.StorageType.Image, HelperType.StorageType, UserReference);
                        //if (TypeId == null)
                        //{
                        //    return null;
                        //}
                        #endregion
                        #region Get Settings 
                        if (_AppConfig != null)
                        {

                            if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                            {
                                bool IsFileSaved = false;
                                string FileServerName = HCoreHelper.GenerateGuid() + "." + Extension;
                                string FileDirectory = HCoreHelper.GetGMTDate().Year + "/" + HCoreHelper.GetGMTDate().Month + "/";
                                if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.BackgroudProcessor)
                                {
                                    FileDirectory = "l/" + FileDirectory;
                                }
                                else
                                {
                                    FileDirectory = "o/" + FileDirectory;
                                }
                                string FilePath = FileDirectory + FileServerName;
                                #region Aws
                                if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                {
                                    #region Save Image To S3 
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(Content);
                                    using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                    {
                                        using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                        {
                                            try
                                            {
                                                PutObjectRequest _PutObjectRequest = new PutObjectRequest
                                                {
                                                    BucketName = _AppConfig.StorageLocation,
                                                    Key = FilePath,
                                                    InputStream = _MemoryStream,
                                                    CannedACL = S3CannedACL.PublicRead,
                                                    StorageClass = S3StorageClass.Standard,
                                                };
                                                _PutObjectRequest.Headers.ContentLength = Base64FileContentBytes.Length;
                                                PutObjectResponse response = _IAmazonS3.PutObjectAsync(_PutObjectRequest).Result;
                                                IsFileSaved = true;
                                            }
                                            catch (AmazonS3Exception _AmazonS3Exception)
                                            {
                                                IsFileSaved = false;
                                                #region  Log Exception
                                                HCoreHelper.LogException("SaveStorage-AWSSave", _AmazonS3Exception, UserReference);
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                                else if (_AppConfig.StorageSourceCode == "storagesource.gcloud")                                 {
                                    GoogleCredential _GoogleCredential = null;
                                    using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                        FileAccess.Read, FileShare.Read))
                                    {
                                        _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                                    }
                                    var _StorageClient = StorageClient.Create(_GoogleCredential);
                                    var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                    {
                                        Bucket = _AppConfig.StorageLocation,
                                        Name = FilePath,
                                    };
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(Content);
                                    using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                    {
                                        try
                                        {
                                            _StorageClient.UploadObject(_UploadObject, _MemoryStream, null, null);
                                            IsFileSaved = true;
                                        }
                                        catch (Exception _Exception)
                                        {
                                            IsFileSaved = false;
                                            LogException("GCloud-Save", _Exception);
                                        }
                                    }                                  }
                                #region Error
                                else
                                {
                                    IsFileSaved = false;
                                    return null;
                                }
                                #endregion
                                if (IsFileSaved == true)
                                {
                                    HCCoreStorage _HCCoreStorage = new HCCoreStorage();
                                    _HCCoreStorage.Guid = HCoreHelper.GenerateGuid();
                                    //if (UserReference.UserId != 0)
                                    //{
                                    //    _HCCoreStorage.UserId = UserReference.UserId;
                                    //}
                                    if (UserReference.AccountId != 0)
                                    {
                                        _HCCoreStorage.AccountId = UserReference.AccountId;
                                    }
                                    _HCCoreStorage.StorageTypeId = 1;
                                    _HCCoreStorage.SourceId = _AppConfig.StorageSourceId;
                                    if (string.IsNullOrEmpty(Name))
                                    {
                                        _HCCoreStorage.Name = HCoreHelper.GenerateGuid();
                                    }
                                    else
                                    {
                                        _HCCoreStorage.Name = Name;
                                    }
                                    _HCCoreStorage.Path = FilePath;
                                    _HCCoreStorage.Extension = Extension;
                                    //_HCCoreStorage.HelperId = HelperId;
                                    _HCCoreStorage.Size = 0;
                                    //_HCCoreStorage.Reference = Reference;
                                    _HCCoreStorage.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreStorage.StatusId = 2;
                                    _HCoreContext.HCCoreStorage.Add(_HCCoreStorage);
                                    //HCoreHelper.LogActivity(_HCoreContext, UserReference);
                                    _HCoreContext.SaveChanges();
                                    return _HCCoreStorage.Id;
                                }
                                else
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                return null;
                            }

                        }
                        else
                        {
                            return null;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveStorage", _Exception, UserReference);
                return null;
                #endregion
            }
            #endregion
        }
        public static string SaveStorage(string Name, string Extension, string Content, OUserReference UserReference)
        {
            try
            {
                if (string.IsNullOrEmpty(Content))
                {
                    return null;
                }
                else
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        if (_AppConfig != null)
                        {
                            if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                            {
                                bool IsFileSaved = false;
                                string FileServerName = HCoreHelper.GenerateGuid() + "." + Extension;
                                string FileDirectory = HCoreHelper.GetGMTDate().Year + "/" + HCoreHelper.GetGMTDate().Month + "/";
                                if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.BackgroudProcessor)
                                {
                                    FileDirectory = "l/" + FileDirectory;
                                }
                                else
                                {
                                    FileDirectory = "o/" + FileDirectory;
                                }
                                string FilePath = FileDirectory + FileServerName;
                                if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                {
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(Content);
                                    using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                    {
                                        using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                        {
                                            try
                                            {
                                                PutObjectRequest _PutObjectRequest = new PutObjectRequest
                                                {
                                                    BucketName = _AppConfig.StorageLocation,
                                                    Key = FilePath,
                                                    InputStream = _MemoryStream,
                                                    CannedACL = S3CannedACL.PublicRead,
                                                    StorageClass = S3StorageClass.Standard,
                                                };
                                                _PutObjectRequest.Headers.ContentLength = Base64FileContentBytes.Length;
                                                PutObjectResponse response = _IAmazonS3.PutObjectAsync(_PutObjectRequest).Result;
                                                IsFileSaved = true;
                                            }
                                            catch (AmazonS3Exception _AmazonS3Exception)
                                            {
                                                IsFileSaved = false;
                                                HCoreHelper.LogException("SaveStorage-AWSSave", _AmazonS3Exception, UserReference);
                                            }
                                        }
                                    }
                                }
                                else if (_AppConfig.StorageSourceCode == "storagesource.gcloud")                                 {
                                    GoogleCredential _GoogleCredential = null;
                                    using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                        FileAccess.Read, FileShare.Read))
                                    {
                                        _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                                    }
                                    var _StorageClient = StorageClient.Create(_GoogleCredential);
                                    var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                    {
                                        Bucket = _AppConfig.StorageLocation,
                                        Name = FilePath,
                                    };
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(Content);
                                    using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                    {
                                        try
                                        {
                                            _StorageClient.UploadObject(_UploadObject, _MemoryStream, null, null);
                                            IsFileSaved = true;
                                        }
                                        catch (Exception _Exception)
                                        {
                                            IsFileSaved = false;
                                            LogException("GCloud-Save", _Exception);
                                        }
                                    }                                 }
                                else
                                {
                                    IsFileSaved = false;
                                    return null;
                                }
                                if (IsFileSaved == true)
                                {
                                    return FilePath;
                                }
                                else
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveStorage", _Exception, UserReference);
                return null;
            }
        }
        public static long? SaveStorage(string Name, string Extension, MemoryStream _MemoryStream, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                if (_MemoryStream == null)
                {
                    return null;
                }
                else
                {
                    #region Perform Operations
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        #region Type Details
                        #endregion
                        #region Get Settings 
                        if (_AppConfig != null)
                        {

                            if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                            {
                                bool IsFileSaved = false;
                                string FileServerName = HCoreHelper.GenerateGuid() + "." + Extension;
                                string FileDirectory = HCoreHelper.GetGMTDate().Year + "/" + HCoreHelper.GetGMTDate().Month + "/";
                                if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.BackgroudProcessor)
                                {
                                    FileDirectory = "l/" + FileDirectory;
                                }
                                else
                                {
                                    FileDirectory = "o/" + FileDirectory;
                                }
                                string FilePath = FileDirectory + FileServerName;
                                #region Aws
                                if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                {
                                    using (AmazonS3Client _IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.EUWest2))
                                    {
                                        try
                                        {
                                            PutObjectRequest _PutObjectRequest = new PutObjectRequest
                                            {
                                                BucketName = _AppConfig.StorageLocation,
                                                Key = FilePath,
                                                InputStream = _MemoryStream,
                                                CannedACL = S3CannedACL.PublicRead,
                                                StorageClass = S3StorageClass.Standard,
                                            };
                                            _PutObjectRequest.Headers.ContentLength = _MemoryStream.Length;
                                            PutObjectResponse response = _IAmazonS3.PutObjectAsync(_PutObjectRequest).Result;
                                            IsFileSaved = true;
                                        }
                                        catch (AmazonS3Exception _AmazonS3Exception)
                                        {
                                            IsFileSaved = false;
                                            HCoreHelper.LogException("SaveStorage-AWSSave", _AmazonS3Exception, UserReference);
                                        }
                                    }

                                }
                                #endregion
                                else if (_AppConfig.StorageSourceCode == "storagesource.gcloud")                                 {
                                    GoogleCredential _GoogleCredential = null;
                                    using (var jsonStream = new FileStream("skey.json", FileMode.Open,
                                        FileAccess.Read, FileShare.Read))
                                    {
                                        _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                                    }
                                    var _StorageClient = StorageClient.Create(_GoogleCredential);
                                    var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                    {
                                        Bucket = _AppConfig.StorageLocation,
                                        Name = FilePath,
                                    };
                                    try
                                    {
                                        _StorageClient.UploadObject(_UploadObject, _MemoryStream, null, null);
                                        IsFileSaved = true;
                                    }
                                    catch (Exception _Exception)
                                    {
                                        IsFileSaved = false;
                                        LogException("GCloud-Save", _Exception);
                                    }                                 }
                                #region Error
                                else
                                {
                                    IsFileSaved = false;
                                    return null;
                                }
                                #endregion
                                if (IsFileSaved == true)
                                {
                                    HCCoreStorage _HCCoreStorage = new HCCoreStorage();
                                    _HCCoreStorage.Guid = HCoreHelper.GenerateGuid();
                                    //if (UserReference.UserId != 0)
                                    //{
                                    //    _HCCoreStorage.UserId = UserReference.UserId;
                                    //}
                                    if (UserReference.AccountId != 0)
                                    {
                                        _HCCoreStorage.AccountId = UserReference.AccountId;
                                    }
                                    _HCCoreStorage.StorageTypeId = 1;
                                    _HCCoreStorage.SourceId = _AppConfig.StorageSourceId;
                                    if (string.IsNullOrEmpty(Name))
                                    {
                                        _HCCoreStorage.Name = HCoreHelper.GenerateGuid();
                                    }
                                    else
                                    {
                                        _HCCoreStorage.Name = Name;
                                    }
                                    _HCCoreStorage.Path = FilePath;
                                    _HCCoreStorage.Extension = Extension;
                                    //_HCCoreStorage.HelperId = HelperId;
                                    _HCCoreStorage.Size = 0;
                                    //_HCCoreStorage.Reference = Reference;
                                    _HCCoreStorage.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreStorage.StatusId = 2;
                                    _HCoreContext.HCCoreStorage.Add(_HCCoreStorage);
                                    //HCoreHelper.LogActivity(_HCoreContext, UserReference);
                                    _HCoreContext.SaveChanges();
                                    return _HCCoreStorage.Id;
                                }
                                else
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                return null;
                            }

                        }
                        else
                        {
                            return null;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveStorage", _Exception, UserReference);
                return null;
                #endregion
            }
            #endregion
        }
        public static string FormatSms(string Sms)
        {
            return Sms.Replace("&", "%26").Replace("#", "%23").Replace("$", "%24").Replace("!", "%21");
        }


        public static void SendSMS(SmsType SmsType, string CountryIsd, string MobileNumber, string Message, long AccountId, string SystemReference, long? TransactionId = null, string SenderId = null, string ApiKey = null, string BaseUrl = null)
        {
            //if (HostEnvironment == HostEnvironmentType.Live ||
            //    HostEnvironment == HostEnvironmentType.BackgroudProcessor ||
            //    HostEnvironment == HostEnvironmentType.Test ||
            //    HostEnvironment == HostEnvironmentType.Tech ||
            //    HostEnvironment == HostEnvironmentType.Dev)
            //{
            if (HostEnvironment == HostEnvironmentType.Live ||
               HostEnvironment == HostEnvironmentType.BackgroudProcessor)
            {
                OHCoreSmsBroadCaster _OHCoreSmsBroadCaster = new OHCoreSmsBroadCaster();
                _OHCoreSmsBroadCaster.SmsType = SmsType;
                _OHCoreSmsBroadCaster.CountryIsd = CountryIsd;
                _OHCoreSmsBroadCaster.MobileNumber = MobileNumber;
                _OHCoreSmsBroadCaster.Message = Message;
                _OHCoreSmsBroadCaster.SenderId = SenderId;
                _OHCoreSmsBroadCaster.ApiKey = ApiKey;
                _OHCoreSmsBroadCaster.BaseUrl = BaseUrl;
                _OHCoreSmsBroadCaster.AccountId = AccountId;
                _OHCoreSmsBroadCaster.TransactionId = TransactionId;
                _OHCoreSmsBroadCaster.SystemReference = SystemReference;
                var _ActorSystem = ActorSystem.Create("ActorSmsBroadcastHandler");
                var _ActorNotifier = _ActorSystem.ActorOf<ActorSmsBroadcastHandler>("ActorSmsBroadcastHandler");
                _ActorNotifier.Tell(_OHCoreSmsBroadCaster);
            }
        }

        public static void BroadCastEmail(string TemplateId, string ToName, string ToAddress, object Parameters, OUserReference UserReference, string? SendGridKey = null, string? AttachmentBase64String = null, string? AttachmentName = null)
        {
            if (HostEnvironment == HostEnvironmentType.Live ||
                HostEnvironment == HostEnvironmentType.BackgroudProcessor ||
                HostEnvironment == HostEnvironmentType.Test ||
                HostEnvironment == HostEnvironmentType.Tech ||
                HostEnvironment == HostEnvironmentType.Dev
                )
            {
                OHCoreEmailBroadCaster _OHCoreEmailBroadCaster = new OHCoreEmailBroadCaster();
                _OHCoreEmailBroadCaster.TemplateId = TemplateId;
                _OHCoreEmailBroadCaster.ToName = ToName;
                _OHCoreEmailBroadCaster.ToAddress = ToAddress;
                _OHCoreEmailBroadCaster.Parameters = Parameters;
                _OHCoreEmailBroadCaster.attachmentBase64 = AttachmentBase64String;
                _OHCoreEmailBroadCaster.AttachmentName = AttachmentName;
                var _ActorSystem = ActorSystem.Create("ActorEmailBroadcastHandler");
                var _ActorNotifier = _ActorSystem.ActorOf<ActorEmailBroadcastHandler>("ActorEmailBroadcastHandler");
                _ActorNotifier.Tell(_OHCoreEmailBroadCaster);
            }
        }
        public static void BroadCastEmail(string TemplateId, Dictionary<string, string> ToAddress, object Parameters, string SendGridKey = null)
        {
            if (HostEnvironment == HostEnvironmentType.Live ||
                HostEnvironment == HostEnvironmentType.BackgroudProcessor ||
                HostEnvironment == HostEnvironmentType.Test ||
                HostEnvironment == HostEnvironmentType.Tech ||
                HostEnvironment == HostEnvironmentType.Dev
                )
            {
                //if (HostEnvironment == HostEnvironmentType.Live ||
                //   HostEnvironment == HostEnvironmentType.BackgroudProcessor
                //   )
                //{
                OHCoreEmailBroadCaster _OHCoreEmailBroadCaster = new OHCoreEmailBroadCaster();
                _OHCoreEmailBroadCaster.TemplateId = TemplateId;
                _OHCoreEmailBroadCaster.ToAddressList = ToAddress;
                _OHCoreEmailBroadCaster.Parameters = Parameters;
                var _ActorSystem = ActorSystem.Create("ActorEmailBroadcastHandler");
                var _ActorNotifier = _ActorSystem.ActorOf<ActorEmailBroadcastHandler>("ActorEmailBroadcastHandler");
                _ActorNotifier.Tell(_OHCoreEmailBroadCaster);
            }
        }
        public static void BroadCastEmail(string TemplateId, string ToName, string ToAddress, EmailBoradCasterParameter _Parameters, OUserReference UserReference, string SendGridKey = null)
        {
            if (string.IsNullOrEmpty(_Parameters.ReceiverDisplayName))
            {
                _Parameters.ReceiverDisplayName = ToName;
                _Parameters.ReceiverEmailAddress = ToAddress;
            }
            _Parameters.CompanyCopyRights = _AppConfig.Email_CompanyCopyRights;
            _Parameters.CompanyAddress = _AppConfig.Email_CompanyAddress;
            OHCoreEmailBroadCaster _OHCoreEmailBroadCaster = new OHCoreEmailBroadCaster();
            _OHCoreEmailBroadCaster.TemplateId = TemplateId;
            _OHCoreEmailBroadCaster.ToName = ToName;
            _OHCoreEmailBroadCaster.ToAddress = ToAddress;
            _OHCoreEmailBroadCaster.Parameters = _Parameters;
            var _ActorSystem = ActorSystem.Create("ActorEmailBroadcastHandler");
            var _ActorNotifier = _ActorSystem.ActorOf<ActorEmailBroadcastHandler>("ActorEmailBroadcastHandler");
            _ActorNotifier.Tell(_OHCoreEmailBroadCaster);
        }
        public static void BroadCastAdminEmail(string TemplateId, string ToName, string ToAddress, object Parameters, OUserReference UserReference)
        {

            OHCoreEmailBroadCaster _OHCoreEmailBroadCaster = new OHCoreEmailBroadCaster();
            _OHCoreEmailBroadCaster.TemplateId = TemplateId;
            _OHCoreEmailBroadCaster.ToName = ToName;
            _OHCoreEmailBroadCaster.ToAddress = ToAddress;
            _OHCoreEmailBroadCaster.Parameters = Parameters;
            var _ActorSystem = ActorSystem.Create("ActorEmailBroadcastHandler");
            var _ActorNotifier = _ActorSystem.ActorOf<ActorEmailBroadcastHandler>("ActorEmailBroadcastHandler");
            _ActorNotifier.Tell(_OHCoreEmailBroadCaster);
        }


        internal const string PrivateKey = "AAAAg0yl-Ms:APA91bH6REvl6sn5v35WaKpMOvMpiTWiwUL4v0ojPhf57BWlodX_IVZ0XIEoShq-KF9TYl-jpTWJ3TpaelvdnoS0oLhqUy4l-XiShNxF_OdbW8koPwWURq-vivDJTClloAd2IDGM6UCE";
        public static void SendPushToDevice(string DeviceKey, string Task, string Subject, string Message, string NavigateTo, long ReferenceId, string ReferenceKey, string ConfirmText, bool ForceNavigation = false, object Data = null, string ImageUrl = null)
        {
            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKey));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                ONotification _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;
                _ONotification.icon = "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png";
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    _ONotification.image = ImageUrl;
                }

                ONotificationDevicePushBody _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceId = ReferenceId;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;
                _ONotificationDevicePushBody.ForceNavigation = ForceNavigation;
                _ONotificationDevicePushBody.Data = Data;

                ONotificationDevicePush _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.to = DeviceKey;
                _ONotificationDevicePush.priority = "high";
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();


                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion
        }
        internal const string PrivateKeyMerchant = "AAAAMLaRixY:APA91bGTf-sG0SU6u8D8XSbZXqfZbQIySRBQThEzedBRQaopMPZYbKJEbtx-N8zD2qp_pSqzEBHiZ6zJxkc_wy-PE91pBsFsLMfn3gAOwZXGvi_hOAWlD_pMp_vWKNHF7Z_axGPlUQRR";

        public static object Default { get; set; }

        public static void SendPushToDeviceMerchant(string DeviceKey, string Task, string Subject, string Message, string NavigateTo, long ReferenceId, string ReferenceKey, string ConfirmText, string ImageUrl = null)
        {
            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKeyMerchant));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                ONotification _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;
                _ONotification.icon = "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png";
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    _ONotification.image = ImageUrl;
                }
                ONotificationDevicePushBody _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceId = ReferenceId;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;

                ONotificationDevicePush _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.to = DeviceKey;
                _ONotificationDevicePush.priority = "high";
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();
                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion
        }
        public static void SendPushToTopic(string TopicName, string Task, string Subject, string Message, string NavigateTo, long ReferenceId, string ReferenceKey, string ConfirmText, string? ImageUrl = null, string? MobileNumber = null, OUserReference? UserReference = null)
        {
            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKey));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                ONotificationDevicePushBody _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceId = ReferenceId;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;

                ONotification _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;
                _ONotification.icon = "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png";
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    _ONotification.image = ImageUrl;
                }
                ONotificationDevicePush _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.to = "/topics/" + TopicName;
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();

            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();
                    using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
                    {
                        HCLPushNotification _HCLPushNotification = new HCLPushNotification();
                        _HCLPushNotification.Guid = GenerateGuid();
                        _HCLPushNotification.TypeId = 1;
                        _HCLPushNotification.DestinationUrl = TopicName;
                        _HCLPushNotification.MobileNumber = MobileNumber;
                        _HCLPushNotification.Task = Task;
                        _HCLPushNotification.Title = Subject;
                        _HCLPushNotification.Message = Message;
                        _HCLPushNotification.Content = RequestString;
                        _HCLPushNotification.ResponseContent = ResponseString;
                        _HCLPushNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCLPushNotification.StatusId = 2;
                        if (UserReference != null)
                        {
                            _HCLPushNotification.CreatedById = UserReference.AccountId;
                        }
                        _HCoreContextLogging.Add(_HCLPushNotification);
                        _HCoreContextLogging.SaveChanges();
                        _HCoreContextLogging.Dispose();
                    }
                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion

        }
        public static void SendPushToTopicMerchant(string TopicName, string Task, string Subject, string Message, string NavigateTo, long ReferenceId, string ReferenceKey, string ConfirmText, string ImageUrl = null)
        {
            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKeyMerchant));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                ONotificationDevicePushBody _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceId = ReferenceId;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;

                ONotification _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;
                _ONotification.icon = "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png";
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    _ONotification.image = ImageUrl;
                }
                ONotificationDevicePush _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.to = "/topics/" + TopicName;
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();
                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion

        }

        public static void HtmlToPdf(string Path, string fileName, string HtmlString, string BaseUrl = "")
        {
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            PdfDocument doc = converter.ConvertHtmlString(HtmlString, BaseUrl);
            doc.Save(Path + fileName);
            doc.Close();
        }

        public static string HtmlToPdfBase64(string HtmlString, string BaseUrl = "")
        {
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            PdfDocument doc = converter.ConvertHtmlString(HtmlString, BaseUrl);
            MemoryStream pdfStream = new MemoryStream();
            doc.Save(pdfStream);
            pdfStream.Position = 0;
            string base64 = Convert.ToBase64String(pdfStream.ToArray());
            doc.Close();
            return base64;
        }


        /// <summary>
        /// This functions will genrate the slug for a string
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static string GenerateSlug(string Data)
        {
            if (!string.IsNullOrEmpty(Data))
            {
                string Slug = Data.RemoveAccents().ToLower();

                Slug = Regex.Replace(Slug, @"[^A-Za-z0-9%\s-]", "");

                Slug = Regex.Replace(Slug, @"\s+", " ").Trim();

                Slug = Regex.Replace(Slug, @"\s", "-");

                return Slug;
            }
            else
            {
                return Data;
            }
        }

        private static string RemoveAccents(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            char[] chars = text
                .Where(c => CharUnicodeInfo.GetUnicodeCategory(c)
                != UnicodeCategory.NonSpacingMark).ToArray();

            return new string(chars).Normalize(NormalizationForm.FormC);
        }

    }
    public class ResponseDto<T>
    {
        public int StatusCode { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public static ResponseDto<T> Fail(string errorMessage, int statusCode = (int)HttpStatusCode.NotFound)
        {
            return new ResponseDto<T> { Status = false, Message = errorMessage, StatusCode = statusCode };
        }
        public static ResponseDto<T> Success(string successMessage, T data, int statusCode = (int)HttpStatusCode.OK)
        {
            return new ResponseDto<T> { Status = true, Message = successMessage, Data = data, StatusCode = statusCode };
        }
    }
}
