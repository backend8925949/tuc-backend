//==================================================================================
// FileName: HCoreSystem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Helper
{
    public static class HCoreSystem
    {
        public static void RefreshSystem()
        {
            HCore.Helper.DataStore.DataStoreManager.LoadDataStore();
        }
        public static void DataStore_InitializeSystem()
        {
          HCore.Helper.DataStore.DataStoreManager.LoadDataStore();
        }
        public static void DataStore_Reset()
        {
             HCore.Helper.DataStore.DataStoreManager.LoadDataStore();
        }

         public static void DataStore_RefreshApi()
        {
             HCore.Helper.DataStore.DataStoreManager.RefreshApi();
        }
          public static void DataStore_RefreshApps()
        {
             HCore.Helper.DataStore.DataStoreManager.RefreshApps();
        }
         public static void DataStore_RefreshResources()
        {
             HCore.Helper.DataStore.DataStoreManager.RefreshResources();
        }
         public static void DataStore_RefreshCountries()
        {
             HCore.Helper.DataStore.DataStoreManager.RefreshCountries();
        }
    }
}
