//==================================================================================
// FileName: HCoreConstant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace HCore.Helper
{
    public class EmailBoradCasterParameter
    {
        public string? ReceiverEmailAddress;
        public string? ReceiverDisplayName;
        public string? MerchantDisplayName;
        public string? StoreDisplayName;
        public string? Message;
        public string? StoreAddress;
        public string? InvoiceAmount;
        public string? RewardAmount;
        public string? RedeemAmount;
        public string? ClaimAmount;
        public string? TransactionDate;
        public string? ReferenceId;
        public string? ReferenceKey;
        public string? UserDisplayname;
        public string? UserMobileNumber;
        public string? UserLink;
        public string? AccessToken;
        public string? CompanyCopyRights;
        public string? CompanyAddress;

        public string? CashierCode { get; set; }
        public string? CashierName { get; set; }

    }
    public static class HCoreConstant
    {
        public static class MailerliteKey
        {
            public const string API_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGY0ZWFmMjRhYmIzZTk1OTFkYjczZDMxYTgyY2VjODUxMTZhZTU0MmU0ZmQ4YmY5MjViOTIyNDQyZjNhMWYzMTFiNmE2YzMxYzMzOGQ5YzkiLCJpYXQiOjE2Njk4ODUxOTkuMDIyMjYyLCJuYmYiOjE2Njk4ODUxOTkuMDIyMjY1LCJleHAiOjE3MDE0MjExOTkuMDE0MTA4LCJzdWIiOiIzNzk1MSIsInNjb3BlcyI6W119.MKETftCF3pxpSYvNSjCwlO6D-T7otrsllsh7ZTWQuaKmle2A4pKRl2JjmEWCNyxY1RtfAqNEYOF4l-JvYikECw";
        }
        public static class SystemAccounts
        {
            public const int ThankUCashSystemId = 1;
            public const int SmashLabId = 971;
            public const int ThankUCashMerchant = 3;
            public const int TUCVas = 5;
            public const long BNPLSystemId = 10832;
            public const string BNPLSystemKey = "tucbnplcontroller";
        }
        public static class NotificationTemplates
        {
            public const string Admin_Transaction_HeavyAmount = "d-d1d37a9b67404d5a8409bf9596b3a9e1";
            public const string Admin_MessageOnly = "d-d82f8aeeda4e443b884ed6c895d58c1f";
            public const string AccountCredentails = "d-846b0e29e98e4ce0a485ec305ce419b1";
            public const string MerchantWelcomeEmail = "d-8a7bc7cd93d84cd7ac25684a3e851aea";
            public const string Merchant_Transaction_HeavyAmount = "d-dafd36e6e6714047ab7c8e334e432bd1";
            public const string MerchantOnboardingEmailVerification = "d-2b3dd388280c4b63aa2689acd067e89f";
            public const string MerchantOnboardingEmailVerificationCode = "d-ed1c7e2d78884473a704c7e9470f71fb";
            public const string CheckCreditLimit = "d-9361fd551c544df28632d16f6942403c";
            public const string CreateLoanRequest = "d-6a66b3ec430743c88589a61ffc2feefb";
            public const string ApprovedLoanRequest = "d-cb583cff496740db81296450cc663d98";
            public const string DisbursedLoan = "d-a19921c8b2fd48ac9c192c7f01997a76";
            public const string LoanRedeemed = "d-e384d257a5e44bfb87b4ac335c4f9fd4";
            public const string MerchantSettlement = "d-cfab42f2c0f541aca671b88b241d2a49";
            public const string TUCMerchantSettlement = "d-a509d17bf9d748ef8d148c22b441e885";
            public const string OverDuePayments = "d-1a22187133984266a67368334d7adb4e";
            public const string UsernameUpdateVerificationCode = "d-61bba08868dc4b6d8a328145c7031199";
        }
        public static Globals _AppConfig = new Globals();
        public static string HostName = "";
        public static HostEnvironmentType HostEnvironment = HostEnvironmentType.Local;
        public const string StatusSuccess = "Success";
        public const string StatusError = "Error";
        public enum TimeZoneId
        {
            WestCentralAfricaStandardTime = 1,
        }

        public enum HostEnvironmentType
        {
            Local = 1,
            Live = 2,
            Test = 3,
            Play = 4,
            BackgroudProcessor = 5,
            Management = 6,
            Tech = 7,
            Dev = 8
        }
        public enum ResponseStatus
        {
            Error = 2,
            Success = 1,
        }
        public enum LogType
        {
            Exception = 89,
            Log = 90,
            Alert = 91,
            Security = 92,
        }
        public enum SmsType
        {
            Transaction = 1,
            Promotion = 2,
        }
        public enum LogLevel
        {
            High = 89,
            Moderate = 90,
            Low = 91,
        }
        public enum ActivityType
        {
            Add = 93,
            Update = 94,
            Delete = 95,
            View = 96,
        }
        public static class UserDetailsChangeLog
        {
            public const int ChanagePassword = 186;
            public const int ChangeAccessPin = 187;
            public const int ChangeUserName = 188;
            public const int ChangeContactNumber = 189;
            public const int ChangeEmailAddress = 190;
            public const int ForgotPassword = 191;
            public const int ForgotAccessPin = 192;
            public const int ForgotUserName = 193;
        }
        public static class HelperType
        {
            public const int DataType = 41;
            public const int LogType = 42;
            public const int ActivityType = 43;
            public const int Os = 44;
            public const int Gender = 45;
            public const int ResponseCodes = 46;
            public const int AccountType = 47;
            public const int AccountOperationType = 48;
            public const int RegistrationSource = 49;
            public const int SystemUserVerification = 50;
            public const int StorageSource = 51;
            public const int StorageType = 52;
            public const int NotificationType = 53;
            public const int NotificationAccount = 54;
            public const int NotificationGateway = 55;
            public const int NotificationTemplate = 56;
            public const int NotificationParameter = 203;
            //public const int App = 57;
            //public const int AppVersion = 58;
            public const int Api = 59;

            public const int FeatureCategory = 685;
            public const int Feature = 60;
            public const int FeatureOption = 61;
            public const int FeatureApi = 62;
            public const int Role = 63;
            public const int RoleFeatures = 64;
            public const int RoleAccess = 65;
            public const int Configuration = 66;
            public const int ConfigurationValue = 67;
            public const int ConfigurationAccountType = 68;
            public const int BankAccountType = 69;
            public const int InvoiceType = 70;
            public const int DeviceSpecifications = 71;
            //public const int Country = 72;
            //public const int CountryRegion = 73;
            //public const int CountrySubregion = 74;
            //public const int CountryCity = 75;
            //public const int CountryCityArea = 76;
            public const int TransactionMode = 77;
            public const int TransactionType = 78;
            public const int TransactionSource = 79;
            public const int Downloads = 408;

            public const int BinCardBrand = 265;
            //public const int BinCardSubBrand = 266;
            public const int BinCardType = 267;
            public const int BinCardBank = 268;


            public const int StatusDefault = 271;
            public const int StatusOrder = 272;
            public const int StatusCardOrder = 273;
            public const int StatusDevice = 274;
            public const int StatusCard = 275;
            public const int StatusTransaction = 276;
            public const int StatusRewardInvoice = 277;
            public const int StatusInvoice = 278;
            public const int PaymentMode = 598;


            public const int AccountFlag = 399;
            public const int AccountLevel = 302;
            public const int MerchantCategory = 205;
            public const int Department = 644;

            public const int SliderImage = 197;
            public static class ProductUsageType
            {
                public const int FullValue = 447;
                public const string FullValueS = "caproductusagetype.fullvalue";
                public const int PartialValue = 448;
                public const string PartialValueS = "caproductusagetype.partialvalue";
            }

            public const int ThankUCashLoyalty = 793;
            public const int ThankUCashDeals = 795;
            public static class ThankUCashDeal
            {
                public const int ProductDeals = 797;
                public const int ServiceDeals = 798;
                public const int Both = 799;
            }
            public static class ThankUCashLoyalties
            {
                public const int OpenLoyalty = 794;
                public const int ClosedLoyalty = 796;
            }
        }
        public static class Helpers
        {
            public class DataType
            {
                public const int Text = 85;
                public const string TextS = "datatype.text";
                public const int Number = 86;
                public const string NumberS = "datatype.number";
                public const int Boolean = 87;
                public const string BooleanS = "datatype.boolean";
                public const int Date = 88;
                public const string DateS = "datatype.date";
                public const int Decimal = 585;
                public const string DecimalS = "datatype.decimal";
                public const int Image = 586;
                public const string ImageS = "datatype.image";
            }
            public class Gender
            {
                public const int Male = 103;
                public const int Female = 104;
                public const int Other = 105;
            }

            public class Platform
            {
                public const int Web = 754;
                public const int Mobile = 755;
                public const int Other = 756;
            }

            public class DealCodeUsageType
            {
                public const int HoursAfterPurchase = 625;
                public const string HoursAfterPurchaseS = "hour";
                public const int ExpiresOnDate = 626;
                public const string ExpiresOnDateS = "date";
                public const int DaysAfterPurchase = 761;
                public const string DaysAfterPurchaseS = "daysafterpurchase";
                public const int DealEndDate = 762;
                public const string DealEndDateS = "dealenddate";
            }

            public class DealType
            {
                public const int ProductDeal = 802;
                public const int ServiceDeal = 803;
            }

            public class DeliveryType
            {
                public const int InStorePickUp = 810;
                public const int Delivery = 811;
                public const int InStoreAndDelivery = 812;
            }

            public class SettelmentType
            {
                public const int DaysAfterRedeem = 782;
                public const string DaysAfterRedeemS = "daysafterredeem";
            }

            public class TransactionTypeValue
            {
                public const string Reward = "reward";
                public const string Redeem = "redeem";
                public const string Payments = "payments";
            }

            public static class TransactionSource
            {
                // APP Side
                //public const int App = 167;
                public const int TUC = 167;
                public const int TUCBlack = 694;
                public const int ThankUCashPlus = 244;
                public const int GiftPoints = 306;
                public const int GiftCards = 456;
                //public const int Card = 168;

                public const int Merchant = 166;
                public const int Store = 169;
                public const int Cashier = 170;
                public const int Settlement = 171;
                public const int Payments = 172;
                public const int Campaign = 244;
                public const int Ninja = 592;
                public const int Deals = 627;
                public const int CashOut = 657;
                public const int TUCBnpl = 749;
                public const int Acquirer = 757;


                public const int TUCVas = 645;
                public const int TUCSuperCash = 646;
                public const int TUCRefund = 660;

                public const int PromotionCampaign = 733;
                public const int TUCPromotions = 769;

                public const string AppS = "transaction.source.app";
                public const string CardS = "transaction.source.card";
                public const string SettlementS = "transaction.source.settlement";
                public const string PaymentsS = "transaction.source.payments";
                public const string GiftCardsS = "transaction.source.giftcards";
                public const string GiftPointS = "transaction.source.giftpoints";
            }
            public class TransactionTypeCategory
            {
                public const int Reward = 207;
                public const int Redeem = 208;
                public const int Settlement = 209;
                public const int PendingSettlement = 210;
                public const int PointTransfer = 211;
                public const int Payments = 212;
            }
            public static class TransactionType
            {
                public const int CashPayment = 147;
                public const int CardReward = 148;
                public const int OnlineGiftPoints = 149;
                public const int TUCWalletTopup = 567;

                public const int MerchantCredit = 150;
                public const int StoreCredit = 151;
                public const int CashierCredit = 152;
                public const int TUCardReward = 153;
                public const int Change = 154;
                public const int CashReward = 155;
                public const int PendingSettlement = 157;
                public const int PointSettlement = 158;
                public const int Atm = 159;
                public const int BankSettlement = 160;
                public const int Bank = 161;
                public const int CreditCardPayment = 162;
                public const int OnlinePayment = 163;
                public const int InoviceRewards = 165;
                public const int ReferralBonus = 202;
                public const int RewardCommission = 204;
                public const int QRCode = 243;
                public const int ThankUCashPlusCredit = 245;
                public const int TransactionBonus = 251;
                public const int PointTransferReward = 687;
                public const int QRCodeReward = 243;
                public const int Gift = 397;
                public const int BonusReward = 285;
                public const int GiftCard = 457;
                public const string GiftCardS = "transaction.type.giftcard";
                public const int RewardExpired = 641;
                public const int ExpiredRewardCredit = 643;
                public const int BillPayment = 568;
                public const int TucSuperCash = 670;
                public const int TUCPayReward = 684;
                public const int AcquirerCredit = 753;


                public static class TUCPromotions
                {
                    public const int PromotionalCredit = 770;

                }

                public static class TUCBnpl
                {
                    public const int LoanCredit = 750;
                    public const int LoanRedeem = 751;

                }

                public static class Loyalty
                {
                    public static class TUCReward
                    {

                    }
                    public static class TUCRedeem
                    {
                        public const int AppRedeem = 156;
                        public const int PosRedeem = 164;
                        public const int TUCPay = 658;
                        public const int PointTransferRedeem = 688;
                        public const int WebRedeem = 696;
                        public const int GiftPointRedeem = 398;
                        public const int GiftCardRedeem = 458;
                        public const int RedeemReward = 282;
                        //public const string GiftCardRedeemS = "transaction.type.giftcardredeem";
                        //public const int BillPaymentRedeem = 479;
                        //public const int LccTopupRedeem = 480;
                        //public const int DealPurchaseRedeem = 628;
                        //public const int RewardExpired = 642;
                        //public const int RewardExpired = 647;
                    }
                }

                public static class TUCCashOut
                {
                    public const int CashOut = 655;
                    public const int CashOutRefund = 656;
                    //public const int CashOut = 646;
                    //public const int CashOutRefund = 657;
                }
                public static class TUCRefund
                {
                    public const int Refund = 659;
                }
                public static class Vas
                {
                    public const int LccTopup = 647;
                    public const int AirtimePurchase = 648;
                    public const int TVPayment = 649;
                    public const int ElectricityPayment = 650;
                }
                public static class VasRefund
                {
                    public const int LccTopup = 651;
                    public const int AirtimePurchase = 652;
                    public const int TVPayment = 653;
                    public const int ElectricityPayment = 654;
                }

                public static class Ninja
                {
                    public const int DeliveryAmount = 593;
                    public const int DeliveryCommission = 594;
                    public const int BankTransfer = 595;
                }

                public static class Deal
                {
                    public const int DealPurchaseRedeem = 628;
                    public const int DealPurchaseRefund = 629;
                    public const int DealPurchase = 630;
                }

                public static class PromotionCampaign
                {
                    public const int WalletCredit = 734;
                }

            }
            public static class UserAccountType
            {
                public const int Controller = 106;
                public const int Admin = 107;
                public const int Merchant = 108;
                public const int Appuser = 109;
                public const int Carduser = 110;
                public const int MerchantStore = 111;
                public const int MerchantCashier = 112;
                public const int PgAccount = 113;
                public const int PosAccount = 114;
                public const int Terminal = 115;
                //public const int TerminalAccount = 115;
                public const int Acquirer = 116;
                public const int AcquirerSubAccount = 117;
                public const int Agent = 221;
                public const int MerchantSubAccount = 270;
                public const int RelationshipManager = 283;
                public const int Branch = 551;
                public const int Manager = 552;
                public const int Partner = 597;
                public const int TUCOperations = 641;
                public const int LoanProvider = 771;
                public const int TimeZone = 788;


                public const string AppUserS = "appuser";
            }
            public static class TransactionMode
            {
                public const int Credit = 145;
                public const int Debit = 146;
            }
            public static class Loyalty
            {
                public const int Reward = 605;
                public const int Redeem = 606;
                public const int RewardClaim = 607;
            }
            public static class RegistrationSource
            {
                public const int System = 118;
                public const string SystemS = "regsource.system";

                public const int Facebook = 119;
                public const string FacebookS = "regsource.facebook";

                public const int Twitter = 120;
                public const string TwitterS = "regsource.twitter";

                public const int Gmail = 121;
                public const string GmailS = "regsource.gmail";

                public const int App = 671;
                public const string AppS = "regsource.app";

                public const int Website = 672;
                public const string WebsiteS = "regsource.website";

                public const int MerchantImport = 673;
                public const string MerchantImportS = "regsource.merchantimport";

                public const int PartnerImport = 674;
                public const string PartnerImportS = "regsource.partnerimport";

                public const int AcquirerImport = 675;
                public const string AcquirerImportS = "regsource.acquirerimport";

                public const int TUCWebsite = 763;
                public const string TUCWebsiteS = "regsource.tucwebsite";


                public const int OpenApi = 764;
                public const string OpenApiS = "regsource.openapi";

                public const int Api = 765;
                public const string ApiS = "regsource.api";

                public const int DealsWebsite = 767;
                public const string DealsWebsiteS = "regsource.website";

                //public const int Website = 766;
                //public const string WebsiteS = "regsource.website";

                public const int MerchantApp = 768;
                public const string MerchantAppS = "regsource.merchantapp";

                public const int Ussd = 783;
                public const string UssdS = "regsource.ussd";

                public const int Merchant = 784;
                public const string MerchantS = "regsource.merchant";

                public const int MerchantPOS = 785;
                public const string MerchantPOSS = "regsource.merchantpos";

                public const int AndroidApp = 786;
                public const string AndroidAppS = "regsource.androidapp";

                public const int IosApp = 787;
                public const string IosAppS = "regsource.iosapp";
                //regsource.openapi
            }
            public static class UserApplicationStatus
            {
                public const int AppInstalled = 195;
                public const int ValidationPending = 199;
                public const int ApprovalPending = 200;
                public const int Approved = 201;
            }
            public static class TransactionPaymentMode
            {
                public const int TUCWallet = 599;
                public const int Card = 600;
                public const int Cash = 601;
                public const int BankTransfer = 602;
                public const int Ussd = 603;
            }
            public static class StorageType
            {
                public const string Image = "storagetype.image";
            }
            public static class InvoiceType
            {
                public const int Payment = 144;
                public const int RewardInvoice = 263;
                public const int RewardCommissionInvoice = 264;
                public const int InvoiceItem = 184;
            }
            public static class AccountOperationType
            {
                public const int Online = 173;
                public const int Offline = 174;
                public const int OnlineAndOffline = 175;
                public const string OnlineAndOfflineS = "accountoperationtype.onlineandoffline";
            }
            public static class LogType
            {
                public const int LogType_Exception = 89;
                public const int LogType_Info = 90;
                public const int LogType_Alert = 91;
                public const int LogType_Security = 92;
            }
            public static class NotificationType
            {
                public const int Email = 133;
                public const int Sms = 134;
                public const int DevicePush = 135;
                public const int InSystem = 136;
            }
            public static class NotificationGateway
            {
                public const int SMTP = 137;
                public const int MailGun = 138;
                public const int Firbase = 139;
                public const int InSystem = 140;
            }
            public static class Product
            {
                public const int GiftCard = 440;
                public const string GiftCardS = "caproducttype.giftcard";
                public const int QuickGiftCard = 459;
                public const string QuickGiftCardS = "caproducttype.quickgiftcard";
            }
            public static class AccountActionType
            {
                public const int RestUsernameRequest = 430;
                public const int ResetPasswordRequest = 431;
                public const int ResetPinRequest = 432;
            }

            public static class PaymentMethod
            {
                public const int TucWallet = 599;
                public const int Card = 600;
                public const int Cash = 601;
                public const int Bank = 602;
                public const int Ussd = 603;
                public const int Paystack = 692;
                public const int QuickTeller = 693;
                public const string UssdS = "ussd";
                public const string TucWalletS = "tucpay";
                public const string CardS = "card";
                public const string CashS = "cash";
                public const string BankS = "banktransfer";
                public const string PayStackS = "paystack";
                public const string QuickTellerS = "quickteller";
                public const string ReferralBonus = "transaction.type.referralbonus";
            }

            public static class ActivityStatusType
            {
                public const int Unused = 662;
                public const int Active = 663;
                public const int Idle = 664;
                public const int Dead = 665;
            }
            public class DealRedeemSource
            {
                public const int Pos = 690;
                public const int App = 691;
            }
        }
        public class HelperStatus
        {
            public static class Default
            {
                public const int Inactive = 1;
                public const int Active = 2;
                public const string ActiveS = "default.active";
                public const int Blocked = 3;
                public const int Suspended = 4;
            }
            public static class Transaction
            {
                public const int Initialized = 631;
                public const int Processing = 27;
                public const int Success = 28;
                public const int Pending = 29;
                public const int Failed = 30;
                public const int Cancelled = 31;
                public const int Error = 32;
                public const int Rejected = 676;
                public const int OnHold = 695;
            }

            public static class Card
            {
                public const int NotAssigned = 22;
                public const int Assigned = 23;
                public const int Blocked = 24;
                public const int Lost = 25;
                public const int Damaged = 26;
            }

            public static class Campaign
            {
                public const int Creating = 293;
                public const int UnderReview = 294;
                public const int Approved = 295;
                public const int Rejected = 296;
                public const int Published = 297;
                public const int Paused = 298;
                public const int LowBalance = 299;
                public const int Expired = 300;
                public const int Archived = 301;
            }


            public static class Product
            {
                public const int Inactive = 450;
                public const int Active = 451;
                public const int OnHold = 452;
                public const int Cancelled = 453;
                public const int Expired = 454;
                public const int Archived = 455;
            }

            public static class ProdutCode
            {
                public const int Unused = 461;
                public const int Blocked = 462;
                public const int Used = 463;
                public const int Expired = 464;
            }

            public static class HCProduct
            {
                public const int Inactive = 547;
                public const int Active = 548;
                public const int OutOfStock = 549;
                public const int NotAvailable = 550;
            }
            public static class OrderStatus
            {
                public const int New = 486;
                public const int PendingConfirmation = 487;
                public const int Confirmed = 488;
                public const int Preparing = 489;
                public const int Ready = 490;
                public const int ReadyToPickup = 491;
                public const int OutForDelivery = 492;
                public const int DeliveryFailed = 493;
                public const int CancelledByUser = 494;
                public const int CancelledBySeller = 495;
                public const int CancelledBySystem = 511;
                public const int Delivered = 512;
                public const int PickUped = 804;
                public const int ProcessedAtFacility = 805;
                public const int DepartedToFacility = 806;
                public const int ArrivedAtFacility = 807;
                public const int DepartedFromFacility = 808;
            }
            public class BillPaymentStatus
            {
                public const int Initialized = 557;
                public const int Processing = 558;
                public const int Success = 559;
                public const int Pending = 782;
                public const int Failed = 560;
                public const int Refunded = 561;
                public const int Cancelled = 591;
            }
            public class CashoutStatus
            {
                public const int Initialized = 678;
                public const int Processing = 679;
                public const int Success = 680;
                public const int Failed = 681;
                public const int Rejected = 682;
            }

            public class Deals
            {
                public const int Draft = 613;
                public const string DraftS = "deal.draft";
                public const int ApprovalPending = 614;
                public const string ApprovalPendingS = "deal.approvalpending";
                public const int Approved = 615;
                public const string ApprovedS = "deal.approved";
                public const int Published = 616;
                public const string PublishedS = "deal.published";
                public const int Paused = 617;
                public const string PausedS = "deal.paused";
                public const int Expired = 618;
                public const string ExpiredS = "deal.expired";
                public const int Rejected = 752;
                public const string RejectedS = "deal.rejected";
                public const int Upcoming = 774;
                public const string UpcomingS = "deal.upcoming";
            }
            public class DealCodes
            {
                public const int Unused = 620;
                public const int Used = 621;
                public const int Expired = 622;
                public const int Blocked = 623;
            }


            public class Download
            {
                public const int Processing = 667;
                public const int Completed = 668;
                public const int Failed = 669;
            }


            public class Loan
            {
                public const int Requested = 736;
                public const int PreApproved = 737;
                public const int Approved = 738;
                public const int Rejected = 739;
                public const int Active = 740;
                public const int Closed = 741;
                public const int Cancelled = 742;
                public const int Submitted = 772;
                public const int Default = 773;
            }

            public class RedeemFrom
            {
                public const int Loyalty = 775;
                public const string LoyaltyM = "Loyalty";
                public const int Deals = 776;
                public const string DealsM = "Deals";
                public const int BNPL = 777;
                public const string BNPLM = "BNPL";
            }
        }

        public class DeliveryPartners
        {
            public static string Dellyman = "65aabbd3efb842adbb84f08c8ef484ce";
            public static string GoShiip = "fc045aef6f5345e6a72bbd065c7fbfb3";
        }
    }
}
