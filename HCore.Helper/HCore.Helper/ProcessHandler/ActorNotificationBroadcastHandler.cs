//==================================================================================
// FileName: ActorNotificationBroadcastHandler.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Akka.Actor;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using Newtonsoft.Json;
using RestSharp;
using SendGrid;
using SendGrid.Helpers.Mail;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using static HCore.Helper.HCoreConstant;

namespace HCore.Helper.ProcessHandler
{
    internal class ActorSmsBroadcastHandler : ReceiveActor
    {
        public class OInfobipResponse
        {
            public List<SmsMessage> messages { get; set; }
        }
        public class SmsMessage
        {
            public string? to { get; set; }
            public string? messageId { get; set; }
            public SmsMessageStatus status { get; set; }

        }
        public class SmsMessageStatus
        {
            public long groupId { get; set; }
            public string? groupName { get; set; }
            public string? id { get; set; }
            public string? name { get; set; }
            public string? description { get; set; }
        }

        public class OSmsRequest
        {
            public string? from { get; set; }
            public string? to { get; set; }
            public string? text { get; set; }
        }

        public class OTermii
        {
            public class Request
            {
                public string? api_key { get; set; }
                public string? to { get; set; }
                public string? from { get; set; }
                public string? sms { get; set; }
                public string? type { get; set; }
                public string? channel { get; set; }
            }
            public class Response
            {
                public string? code { get; set; }
                public string? message_id { get; set; }
                public string? message { get; set; }
                public double balance { get; set; }
                public string? user { get; set; }
            }
        }
        public class CellulantAuthResponse
        {
            public string? token_type { get; set; }
            public int expires_in { get; set; }
            public string? access_token { get; set; }
            public string? refresh_token { get; set; }
            public DateTime? expiary_date { get; set; } = null;
        }
        public class CellulantSmsResponse
        {
            public string? transactionID { get; set; }
            public int statusCode { get; set; }
            public string? message { get; set; }
            public string? details { get; set; }
        }
        HCLSmsNotification _HCLSmsNotification;
        HCoreContextLogging _HCoreContextLogging;
        OTermii.Request _OTermiiRequest;
        public static CellulantAuthResponse _AuthResponse = new CellulantAuthResponse();
        public void Authenticate()
        {
            var client = new RestClient("https://engagementapi-msg.cellulant.co.ke/pilot/cas/authenticate");
            var request = new RestRequest();
            request.Method = Method.Post;
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\"username\":\"CONNECTED_KEN_API_USER\", \"password\":\"W`)KD=#yy#(=,<69::6bFNlX]sNhDI$~\"}", RestSharp.ParameterType.RequestBody);
            var response = client.ExecutePost(request);
            var data = JsonConvert.DeserializeObject<CellulantAuthResponse>(response.Content!)!;
            if (data != null)
            {
                _AuthResponse = data;
                _AuthResponse.expiary_date = HCoreHelper.GetGMTDateTime().AddMinutes((data.expires_in - 1));
                //SendSms("254733400047", "TUC TEST");
            }
        }
        public CellulantSmsResponse? SendSms(string MobileNumber, string Message)
        {
            if (_AuthResponse == null || _AuthResponse.expiary_date == null || _AuthResponse.expiary_date > HCoreHelper.GetGMTDateTime())
            {
                Authenticate();
            }
            if (_AuthResponse != null && !string.IsNullOrEmpty(_AuthResponse.access_token))
            {
                var _Request = new
                {
                    notificationType = "TransactionalAlerts",
                    message = Message,
                    referenceID = HCoreHelper.GenerateGuid(),
                    clientCode = "CONNECTED_KEN",
                    destination = new
                    {
                        phone = new[] { MobileNumber },
                    },
                    channels = new[] { "SMS" },
                    transactionID = "CL" + HCoreHelper.GenerateRandomNumber(8),
                    clientID = 55644,
                };

                var client = new RestClient("https://engagementapi-msg.cellulant.co.ke/pilot/api/engagement");
                var request = new RestRequest();
                request.Method = Method.Post;
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "Bearer " + _AuthResponse.access_token);
                request.AddParameter("application/json", JsonConvert.SerializeObject(_Request), RestSharp.ParameterType.RequestBody);
                var response = client.ExecutePost(request);
                var data = JsonConvert.DeserializeObject<CellulantSmsResponse>(response.Content!)!;
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public ActorSmsBroadcastHandler()
        {
            Receive<OHCoreSmsBroadCaster>(_BroadcastRequest =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(_BroadcastRequest.Message))
                    {
                        _BroadcastRequest.Message = HCoreHelper.FormatSms(_BroadcastRequest.Message);
                        if (!string.IsNullOrEmpty(_BroadcastRequest.MobileNumber))
                        {
                            #region Send SMS
                            if (_BroadcastRequest.SmsType == SmsType.Transaction)
                            {
                                if (string.IsNullOrEmpty(_BroadcastRequest.SenderId) || _BroadcastRequest.SenderId == "ThankUCash")
                                {
                                    if (_BroadcastRequest.CountryIsd == "234")
                                    {
                                        //string FinalMobileNumber = HCoreHelper.FormatMobileNumber(_BroadcastRequest.CountryIsd, _BroadcastRequest.MobileNumber);
                                        string Url = "http://193.105.74.59/api/sendsms/plain?user=ThankUCashCAs&password=ThankUCash@TUC321&GSM=" + _BroadcastRequest.MobileNumber + "&sender=ThankUCash&SMSText=" + _BroadcastRequest.Message;

                                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                                        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                                        using (HttpWebResponse _HttpWebResponse = (HttpWebResponse)request.GetResponse())
                                        using (Stream _Stream = _HttpWebResponse.GetResponseStream())
                                        using (StreamReader _StreamReader = new StreamReader(_Stream))
                                        {
                                            string FResponse = _StreamReader.ReadToEnd();
                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _HCLSmsNotification = new HCLSmsNotification();
                                                _HCLSmsNotification.Guid = HCoreHelper.GenerateGuid();
                                                if (!string.IsNullOrEmpty(_BroadcastRequest.SystemReference))
                                                {
                                                    _HCLSmsNotification.SystemReference = _BroadcastRequest.SystemReference;
                                                }
                                                if (_BroadcastRequest.AccountId != null && _BroadcastRequest.AccountId > 0)
                                                {
                                                    _HCLSmsNotification.AccountId = _BroadcastRequest.AccountId;
                                                }
                                                if (!string.IsNullOrEmpty(_BroadcastRequest.SenderId))
                                                {
                                                    _HCLSmsNotification.Source = _BroadcastRequest.SenderId;
                                                }
                                                else
                                                {
                                                    _HCLSmsNotification.Source = "ThankUCash";
                                                }
                                                _HCLSmsNotification.MobileNumber = _BroadcastRequest.MobileNumber;
                                                _HCLSmsNotification.Message = _BroadcastRequest.Message;
                                                _HCLSmsNotification.SendDate = HCoreHelper.GetGMTDateTime();
                                                _HCLSmsNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                                                if (FResponse == "-1")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Send_Error";
                                                }
                                                else if (FResponse == "-2")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Not_EnoughCredits";
                                                }
                                                else if (FResponse == "-3")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Network_NotCovered";
                                                }
                                                else if (FResponse == "-5")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Invalid_User_Or_Pass";
                                                }
                                                else if (FResponse == "-6")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Missing_Destination_Address";
                                                }
                                                else if (FResponse == "-7")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Missing_SMSText";
                                                }
                                                else if (FResponse == "-8")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Missing_SenderName";
                                                }
                                                else if (FResponse == "-9")
                                                {
                                                    _HCLSmsNotification.StatusCode = "DestAdd_InvalidFormat";
                                                }
                                                else if (FResponse == "-10")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Missing_Username";
                                                }
                                                else if (FResponse == "-13")
                                                {
                                                    _HCLSmsNotification.StatusCode = "Invalid_Destination_Address";
                                                }
                                                else
                                                {
                                                    _HCLSmsNotification.ReferenceNumber = FResponse;
                                                    _HCLSmsNotification.StatusCode = "Success";
                                                }
                                                _HCLSmsNotification.DeliveryResponse = FResponse;
                                                //_HCLSmsNotification.Data = response.Content;
                                                //if (!string.IsNullOrEmpty(response.Content))
                                                //{
                                                //    OInfobipResponse _Request = JsonConvert.DeserializeObject<OInfobipResponse>(response.Content);
                                                //    if (_Request.messages != null && _Request.messages.Count > 0)
                                                //    {
                                                //        var SmsItem = _Request.messages[0];
                                                //        _HCLSmsNotification.ReferenceNumber = SmsItem.messageId;
                                                //        if (SmsItem.status != null)
                                                //        {
                                                //            _HCLSmsNotification.ReferenceGroupId = SmsItem.status.groupId;
                                                //            _HCLSmsNotification.ReferenceGroupName = SmsItem.status.groupName;
                                                //            _HCLSmsNotification.StatusCode = SmsItem.status.name;
                                                //            _HCLSmsNotification.ReferenceMessage = SmsItem.status.description;
                                                //        }
                                                //    }
                                                //}
                                                _HCoreContextLogging.HCLSmsNotification.Add(_HCLSmsNotification);
                                                _HCoreContextLogging.SaveChanges();
                                                _HCoreContextLogging.Dispose();
                                            }
                                        }
                                    }
                                    else if (_BroadcastRequest.CountryIsd == "254")
                                    {

                                        var Response = SendSms(_BroadcastRequest.MobileNumber, _BroadcastRequest.Message);
                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _HCLSmsNotification = new HCLSmsNotification();
                                            _HCLSmsNotification.Guid = HCoreHelper.GenerateGuid();
                                            if (!string.IsNullOrEmpty(_BroadcastRequest.SystemReference))
                                            {
                                                _HCLSmsNotification.SystemReference = _BroadcastRequest.SystemReference;
                                            }
                                            if (_BroadcastRequest.AccountId != null && _BroadcastRequest.AccountId > 0)
                                            {
                                                _HCLSmsNotification.AccountId = _BroadcastRequest.AccountId;
                                            }
                                            if (!string.IsNullOrEmpty(_BroadcastRequest.SenderId))
                                            {
                                                _HCLSmsNotification.Source = _BroadcastRequest.SenderId;
                                            }
                                            else
                                            {
                                                _HCLSmsNotification.Source = "ThankUCash";
                                            }
                                            _HCLSmsNotification.MobileNumber = _BroadcastRequest.MobileNumber;
                                            _HCLSmsNotification.Message = _BroadcastRequest.Message;
                                            _HCLSmsNotification.SendDate = HCoreHelper.GetGMTDateTime();
                                            _HCLSmsNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                                            if (Response != null)
                                            {
                                                _HCLSmsNotification.ReferenceNumber = Response.transactionID;
                                                _HCLSmsNotification.StatusCode = "Success";
                                                _HCLSmsNotification.DeliveryResponse = JsonConvert.SerializeObject(Response);
                                            }
                                            else
                                            {
                                                _HCLSmsNotification.StatusCode = "Failed";
                                            }

                                            _HCoreContextLogging.HCLSmsNotification.Add(_HCLSmsNotification);
                                            _HCoreContextLogging.SaveChanges();
                                            _HCoreContextLogging.Dispose();
                                        }

                                    }

                                    else
                                    {
                                        string FinalMobileNumber = "+" + _BroadcastRequest.MobileNumber;
                                        string accountSid = "AC27a7ecf24433b5b8a62f86f3bb9b45fd";
                                        string authToken = "13fff96b863a333b77519d4cb9b1e706";
                                        //TwilioClient.Init(accountSid, authToken,);
                                        TwilioClient.Init(accountSid, authToken);
                                        //TwilioClient.Init("harshal@thankucash.com", "Universal1!123456789", "AC27a7ecf24433b5b8a62f86f3bb9b45fd");
                                        var message = MessageResource.Create(
                                                                  body: _BroadcastRequest.Message,
                                                                  from: new Twilio.Types.PhoneNumber("+18507894414"),
                                                                  to: new Twilio.Types.PhoneNumber(FinalMobileNumber)
                                                              );
                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _HCLSmsNotification = new HCLSmsNotification();
                                            _HCLSmsNotification.Guid = HCoreHelper.GenerateGuid();
                                            if (!string.IsNullOrEmpty(_BroadcastRequest.SystemReference))
                                            {
                                                _HCLSmsNotification.SystemReference = _BroadcastRequest.SystemReference;
                                            }
                                            if (_BroadcastRequest.AccountId != null && _BroadcastRequest.AccountId > 0)
                                            {
                                                _HCLSmsNotification.AccountId = _BroadcastRequest.AccountId;
                                            }
                                            if (!string.IsNullOrEmpty(_BroadcastRequest.SenderId))
                                            {
                                                _HCLSmsNotification.Source = _BroadcastRequest.SenderId;
                                            }
                                            else
                                            {
                                                _HCLSmsNotification.Source = "ThankUCash-Twilio";
                                            }
                                            _HCLSmsNotification.MobileNumber = FinalMobileNumber;
                                            _HCLSmsNotification.Message = _BroadcastRequest.Message;
                                            _HCLSmsNotification.SendDate = HCoreHelper.GetGMTDateTime();
                                            _HCLSmsNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCLSmsNotification.StatusCode = message.Status.ToString();
                                            _HCLSmsNotification.DeliveryResponse = JsonConvert.SerializeObject(message);
                                            _HCoreContextLogging.HCLSmsNotification.Add(_HCLSmsNotification);
                                            _HCoreContextLogging.SaveChanges();
                                            _HCoreContextLogging.Dispose();
                                        }
                                    }
                                }
                                else
                                {
                                    if (_BroadcastRequest.CountryIsd == "234")
                                    {
                                        string FinalMobileNumber = HCoreHelper.FormatMobileNumber(_BroadcastRequest.CountryIsd, _BroadcastRequest.MobileNumber, 10);
                                        string Url = "https://3jwl1.api.infobip.com/sms/2/text/single";
                                        if (!string.IsNullOrEmpty(_BroadcastRequest.BaseUrl))
                                        {
                                            Url = _BroadcastRequest.BaseUrl + "/sms/2/text/single";
                                        }
                                        string notifyUrl = "https://bot.thankucash.com/api/v3/con/sys/notifyps";
                                        if (HostEnvironment == HostEnvironmentType.Test)
                                        {
                                            notifyUrl = "https://testwebconnect.thankucash.com/api/v3/con/sys/notifyps";
                                        }
                                        var client = new RestSharp.RestClient(Url);
                                        var request = new RestSharp.RestRequest();
                                        request.Method = Method.Post;
                                        request.AddHeader("accept", "application/json");
                                        request.AddHeader("content-type", "application/json");
                                        if (!string.IsNullOrEmpty(_BroadcastRequest.ApiKey))
                                        {
                                            request.AddHeader("authorization", "App " + _BroadcastRequest.ApiKey);
                                        }
                                        else
                                        {
                                            request.AddHeader("authorization", "Basic VGhhbmtVQ2FzaDpTcGVlZGRlbW9AMTIz");
                                        }

                                        if (!string.IsNullOrEmpty(_BroadcastRequest.SenderId))
                                        {
                                            request.AddParameter("application/json", "{\"from\":\"" + _BroadcastRequest.SenderId + "\", \"to\":\"" + FinalMobileNumber + "\",\"text\":\"" + _BroadcastRequest.Message + "\",\"notifyUrl\":\"" + notifyUrl + "\"}", RestSharp.ParameterType.RequestBody);
                                        }
                                        else
                                        {
                                            request.AddParameter("application/json", "{\"from\":\"ThankUCash\", \"to\":\"" + FinalMobileNumber + "\",\"text\":\"" + _BroadcastRequest.Message + "\",\"notifyUrl\":\"" + notifyUrl + "\"}", RestSharp.ParameterType.RequestBody);
                                        }
                                        var response = client.Execute(request);
                                        //46b534aee55667c64a388465235a0044-621b0eb3-5a52-4522-b0f0-51edabb83b80
                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _HCLSmsNotification = new HCLSmsNotification();
                                            _HCLSmsNotification.Guid = HCoreHelper.GenerateGuid();
                                            if (!string.IsNullOrEmpty(_BroadcastRequest.SenderId))
                                            {
                                                _HCLSmsNotification.Source = _BroadcastRequest.SenderId;
                                            }
                                            else
                                            {
                                                _HCLSmsNotification.Source = "ThankUCash";
                                            }
                                            _HCLSmsNotification.MobileNumber = FinalMobileNumber;
                                            _HCLSmsNotification.Message = _BroadcastRequest.Message;
                                            _HCLSmsNotification.SendDate = HCoreHelper.GetGMTDateTime();
                                            _HCLSmsNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCLSmsNotification.StatusCode = response.StatusCode.ToString();
                                            _HCLSmsNotification.Data = response.Content;
                                            if (!string.IsNullOrEmpty(response.Content))
                                            {
                                                OInfobipResponse _Request = JsonConvert.DeserializeObject<OInfobipResponse>(response.Content);
                                                if (_Request.messages != null && _Request.messages.Count > 0)
                                                {
                                                    var SmsItem = _Request.messages[0];
                                                    _HCLSmsNotification.ReferenceNumber = SmsItem.messageId;
                                                    if (SmsItem.status != null)
                                                    {
                                                        _HCLSmsNotification.ReferenceGroupId = SmsItem.status.groupId;
                                                        _HCLSmsNotification.ReferenceGroupName = SmsItem.status.groupName;
                                                        _HCLSmsNotification.StatusCode = SmsItem.status.name;
                                                        _HCLSmsNotification.ReferenceMessage = SmsItem.status.description;
                                                    }
                                                }
                                            }
                                            _HCoreContextLogging.HCLSmsNotification.Add(_HCLSmsNotification);
                                            _HCoreContextLogging.SaveChanges();
                                            _HCoreContextLogging.Dispose();
                                        }
                                    }
                                }
                            }
                            else if (_BroadcastRequest.SmsType == SmsType.Promotion)
                            {
                                if (_BroadcastRequest.CountryIsd == "234")
                                {
                                    string FinalMobileNumber = HCoreHelper.FormatMobileNumber(_BroadcastRequest.CountryIsd, _BroadcastRequest.MobileNumber, 10);
                                    #region Send SMS Messagae
                                    string UserName = "ThankU";
                                    string Password = "qo6tPZO7";
                                    var smsapiurl = "http://ngn.rmlconnect.net/bulksms/bulksms?username=" + UserName
                                    + "&password=" + Password
                                    + "&type=0"
                                    + "&dlr=1"
                                    + "&destination=" + FinalMobileNumber
                                    + "&source=Thank U"
                                    + "&message=" + _BroadcastRequest.Message;

                                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                                    System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                                    string responseString = _StreamReader.ReadToEnd();
                                    if (responseString.StartsWith("OK", StringComparison.Ordinal))
                                    {
                                        _StreamReader.Close();
                                        _HttpWebResponse.Close();
                                    }
                                    else
                                    {
                                        _StreamReader.Close();
                                        _HttpWebResponse.Close();
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
                            {
                                HCLCoreNotification _HCLCoreNotification = new HCLCoreNotification
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    TypeId = Helpers.NotificationType.Sms,
                                    ToName = _BroadcastRequest.MobileNumber,
                                    Message = _BroadcastRequest.Message,
                                    SendDate = HCoreHelper.GetGMTDateTime(),
                                    Attempts = 1,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = 2
                                };
                                _HCoreContextLogging.HCLCoreNotification.Add(_HCLCoreNotification);
                                _HCoreContextLogging.SaveChanges();
                            }
                        }
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("ActorSmsBroadcastHandler", _Exception);
                }
            });
        }
    }
    internal class ActorSmsBroadcastUpdateHandler : ReceiveActor
    {
        public class SmsResponse
        {
            public string? bulkId { get; set; }
            public string? messageId { get; set; }
            public string? to { get; set; }
            public DateTime? sentAt { get; set; }
            public DateTime? doneAt { get; set; }
            public int smsCount { get; set; }
            public OPrice price { get; set; }
            public OStatus status { get; set; }
            public OError error { get; set; }
        }
        public class OPrice
        {
            public double pricePerMessage { get; set; }
            public string? currency { get; set; }
        }
        public class OStatus
        {
            public long groupId { get; set; }
            public string? groupName { get; set; }
            public long id { get; set; }
            public string? name { get; set; }
            public string? description { get; set; }
        }
        public class OError
        {
            public long groupId { get; set; }
            public string? groupName { get; set; }
            public long id { get; set; }
            public string? name { get; set; }
            public string? description { get; set; }
            public bool permanent { get; set; }
        }
        HCoreContextLogging _HCoreContextLogging;
        public ActorSmsBroadcastUpdateHandler()
        {
            Receive<string>(_RequestContent =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(_RequestContent))
                    {
                        SmsResponse _Request = JsonConvert.DeserializeObject<SmsResponse>(HCoreEncrypt.DecodeText(_RequestContent));
                        if (_Request != null && _Request.messageId != null)
                        {
                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                var SmsNotificationLog = _HCoreContextLogging.HCLSmsNotification.Where(x => x.ReferenceNumber == _Request.messageId && x.MobileNumber == _Request.to).FirstOrDefault();
                                if (SmsNotificationLog != null)
                                {
                                    if (_Request.sentAt != null)
                                    {
                                        SmsNotificationLog.SendDate = _Request.sentAt;
                                    }
                                    if (_Request.doneAt != null)
                                    {
                                        SmsNotificationLog.DeliveryDate = _Request.doneAt;
                                    }
                                    if (_Request.price != null)
                                    {
                                        SmsNotificationLog.Amount = _Request.price.pricePerMessage;
                                    }
                                    if (_Request.status != null)
                                    {
                                        SmsNotificationLog.ReferenceGroupId = _Request.status.groupId;
                                        SmsNotificationLog.ReferenceGroupName = _Request.status.groupName;
                                        SmsNotificationLog.ReferenceMessage = _Request.status.description;
                                    }
                                    if (_Request.error != null)
                                    {
                                        if (_Request.status.groupId > 0)
                                        {
                                            SmsNotificationLog.ReferenceGroupId = _Request.status.groupId;
                                            SmsNotificationLog.ReferenceGroupName = _Request.status.groupName;
                                            SmsNotificationLog.ReferenceMessage = _Request.status.description;
                                        }
                                    }
                                    _HCoreContextLogging.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContextLogging.Dispose();
                                }
                            }
                        }

                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogData(LogType.Exception, "", _Exception.ToString(), _RequestContent, null);
                    HCoreHelper.LogException("ActorSmsBroadcastUpdateHandler", _Exception);
                }
            });
        }
    }
    internal class ActorEmailBroadcastHandler : ReceiveActor
    {
        public ActorEmailBroadcastHandler()
        {
            Receive<OHCoreEmailBroadCaster>(async _BroadcastRequest =>
            {
                if (!string.IsNullOrEmpty(_BroadcastRequest.SendGridKey))
                {
                    if (!string.IsNullOrEmpty(_BroadcastRequest.ToAddress))
                    {
                        var _SendGridClient = new SendGridClient(_BroadcastRequest.SendGridKey);
                        List<EmailAddress> _Email = new List<EmailAddress>();
                        if (string.IsNullOrEmpty(_BroadcastRequest.ToName))
                        {
                            _Email.Add(new EmailAddress
                            {
                                Email = _BroadcastRequest.ToAddress,
                                Name = _AppConfig.SendGridDefaultReceiverName,
                            });
                        }
                        else
                        {
                            _Email.Add(new EmailAddress
                            {
                                Email = _BroadcastRequest.ToAddress,
                                Name = _BroadcastRequest.ToName,
                            });
                        }
                        var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                        var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                        if (!string.IsNullOrWhiteSpace(_BroadcastRequest.attachmentBase64))
                        {
                            _BroacastMessage.AddAttachment(_BroadcastRequest.AttachmentName, _BroadcastRequest.attachmentBase64);
                        }
                        var _BroadCastResponse = await _SendGridClient.SendEmailAsync(_BroacastMessage);
                    }
                    else
                    {
                        if (_BroadcastRequest.ToAddressList != null && _BroadcastRequest.ToAddressList.Count > 0)
                        {
                            var _SendGridClient = new SendGridClient(_BroadcastRequest.SendGridKey);
                            List<EmailAddress> _Email = new List<EmailAddress>();
                            foreach (var ToItem in _BroadcastRequest.ToAddressList)
                            {
                                _Email.Add(new EmailAddress
                                {
                                    Email = ToItem.Value,
                                    Name = ToItem.Key,
                                });
                            }
                            var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                            var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                            if (!string.IsNullOrWhiteSpace(_BroadcastRequest.attachmentBase64))
                            {
                                _BroacastMessage.AddAttachment(_BroadcastRequest.AttachmentName, _BroadcastRequest.attachmentBase64);
                            }
                            var _BroadCastResponse = _SendGridClient.SendEmailAsync(_BroacastMessage).GetAwaiter();
                        }
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(_BroadcastRequest.ToAddress))
                    {
                        if (!string.IsNullOrEmpty(_AppConfig.SendGridKey))
                        {
                            var _SendGridClient = new SendGridClient(_AppConfig.SendGridKey);
                            List<EmailAddress> _Email = new List<EmailAddress>();
                            if (string.IsNullOrEmpty(_BroadcastRequest.ToName))
                            {
                                _Email.Add(new EmailAddress
                                {
                                    Email = _BroadcastRequest.ToAddress,
                                    Name = _AppConfig.SendGridDefaultReceiverName,
                                });
                            }
                            else
                            {
                                _Email.Add(new EmailAddress
                                {
                                    Email = _BroadcastRequest.ToAddress,
                                    Name = _BroadcastRequest.ToName,
                                });
                            }
                            var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                            var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                            if (!string.IsNullOrWhiteSpace(_BroadcastRequest.attachmentBase64))
                            {
                                _BroacastMessage.AddAttachment(_BroadcastRequest.AttachmentName, _BroadcastRequest.attachmentBase64);
                            }
                            var _BroadCastResponse = await _SendGridClient.SendEmailAsync(_BroacastMessage);
                        }
                    }
                    else
                    {
                        if (_BroadcastRequest.ToAddressList != null && _BroadcastRequest.ToAddressList.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(_AppConfig.SendGridKey))
                            {
                                var _SendGridClient = new SendGridClient(_AppConfig.SendGridKey);
                                List<EmailAddress> _Email = new List<EmailAddress>();
                                foreach (var ToItem in _BroadcastRequest.ToAddressList)
                                {
                                    _Email.Add(new EmailAddress
                                    {
                                        Email = ToItem.Value,
                                        Name = ToItem.Key,
                                    });
                                }
                                var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                                var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                                if (!string.IsNullOrWhiteSpace(_BroadcastRequest.attachmentBase64))
                                {
                                    _BroacastMessage.AddAttachment(_BroadcastRequest.AttachmentName, _BroadcastRequest.attachmentBase64);
                                }
                                var _BroadCastResponse = _SendGridClient.SendEmailAsync(_BroacastMessage).GetAwaiter();
                            }
                        }
                    }

                }
            });
        }
    }
    internal class ActorDevicePushHandler : ReceiveActor
    {
        public ActorDevicePushHandler()
        {
            Receive<OHCoreEmailBroadCaster>(async _BroadcastRequest =>
            {
                if (!string.IsNullOrEmpty(_BroadcastRequest.ToAddress))
                {
                    if (!string.IsNullOrEmpty(_AppConfig.SendGridKey))
                    {
                        var _SendGridClient = new SendGridClient(_AppConfig.SendGridKey);
                        List<EmailAddress> _Email = new List<EmailAddress>();
                        if (string.IsNullOrEmpty(_BroadcastRequest.ToName))
                        {
                            _Email.Add(new EmailAddress
                            {
                                Email = _BroadcastRequest.ToAddress,
                                Name = _AppConfig.SendGridDefaultReceiverName,
                            });
                        }
                        else
                        {
                            _Email.Add(new EmailAddress
                            {
                                Email = _BroadcastRequest.ToAddress,
                                Name = _BroadcastRequest.ToName,
                            });
                        }
                        var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                        var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                        var _BroadCastResponse = await _SendGridClient.SendEmailAsync(_BroacastMessage);
                    }
                }
                else
                {
                    if (_BroadcastRequest.ToAddressList != null && _BroadcastRequest.ToAddressList.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(_AppConfig.SendGridKey))
                        {
                            var _SendGridClient = new SendGridClient(_AppConfig.SendGridKey);
                            List<EmailAddress> _Email = new List<EmailAddress>();
                            foreach (var ToItem in _BroadcastRequest.ToAddressList)
                            {
                                _Email.Add(new EmailAddress
                                {
                                    Email = ToItem.Value,
                                    Name = ToItem.Key,
                                });
                            }
                            var _FromAddress = new EmailAddress(_AppConfig.SendGridSenderEmail, _AppConfig.SendGridSenderName);
                            var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, _BroadcastRequest.TemplateId, _BroadcastRequest.Parameters);
                            var _BroadCastResponse = _SendGridClient.SendEmailAsync(_BroacastMessage).GetAwaiter();
                        }
                    }
                }
            });
        }
    }
}
