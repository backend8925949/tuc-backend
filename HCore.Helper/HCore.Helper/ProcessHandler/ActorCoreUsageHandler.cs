//==================================================================================
// FileName: ActorCoreUsageHandler.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using HCore.Helper.DataStore;
using HCore.Data.Logging.Models;
using Z.EntityFramework.Plus;
using Microsoft.EntityFrameworkCore;

namespace HCore.Helper.ProcessHandler
{
    internal class ActorCoreUsageHandler : ReceiveActor
    {
        HCoreContext _HCoreContext;
        //HCoreContextLogging _HCoreContextLogging;
        public ActorCoreUsageHandler()
        {
            Receive<OUserReference>(async UserReference =>
            {
                try
                {
                    //string ResponseBodyItem = JsonConvert.SerializeObject(UserReference.ResponseData,
                    //                           new JsonSerializerSettings()
                    //                           {
                    //                               NullValueHandling = NullValueHandling.Ignore
                    //                           });
                    //UserReference.ResponseData = ResponseBodyItem;
                    if (UserReference.ApiId == 0 && UserReference.ResponseStatus == "Success")
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            HCCoreCommon _CoreCommon = new HCCoreCommon();
                            _CoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _CoreCommon.TypeId = HelperType.Api;
                            _CoreCommon.Name = UserReference.Task;
                            _CoreCommon.SystemName = UserReference.Task;
                            _CoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _CoreCommon.StatusId = HelperStatus.Default.Active;
                            if (UserReference.AccountId != 0)
                            {
                                _CoreCommon.CreatedById = UserReference.AccountId;
                            }
                            _HCoreContext.HCCoreCommon.Add(_CoreCommon);
                            await _HCoreContext.SaveChangesAsync();
                            UserReference.ApiId = _CoreCommon.Id;
                            HCoreSystem.DataStore_RefreshApi();
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (UserReference.AccountSessionId != 0)
                        {
                            var SessionDetails = await _HCoreContext.HCUAccountSession.Where(x => x.Id == UserReference.AccountSessionId).FirstOrDefaultAsync();
                            if (SessionDetails != null)
                            {
                                SessionDetails.LastActivityDate = UserReference.RequestTime;
                            }
                        }
                        #region Log Api Count
                        if (UserReference.ApiId != 0)
                        {
                            var ApiDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.ApiId).FirstOrDefaultAsync();
                            if (ApiDetails != null)
                            {
                                if (ApiDetails.Count == null)
                                {
                                    ApiDetails.Count = 1;
                                }
                                else
                                {
                                    ApiDetails.Count = ApiDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        #region Log App Count
                        if (UserReference.AppId != 0)
                        {
                            var AppDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppId).FirstOrDefaultAsync();
                            if (AppDetails != null)
                            {
                                if (AppDetails.Count == null)
                                {
                                    AppDetails.Count = 1;
                                }
                                else
                                {
                                    AppDetails.Count = AppDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        #region Log App Version Count
                        if (UserReference.AppVersionId != 0)
                        {
                            var AppVersionDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppVersionId).FirstOrDefaultAsync();
                            if (AppVersionDetails != null)
                            {
                                if (AppVersionDetails.Count == null)
                                {
                                    AppVersionDetails.Count = 1;
                                }
                                else
                                {
                                    AppVersionDetails.Count = AppVersionDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                    }
                    var _ActorCoreUsageHandlerPart = ActorSystem.Create("ActorCoreUsageHandlerPart");
                    var _ActorCoreUsageHandlerPartGreet = _ActorCoreUsageHandlerPart.ActorOf<ActorCoreUsageHandlerPart>("ActorCoreUsageHandlerPart");
                    _ActorCoreUsageHandlerPartGreet.Tell(UserReference);

                }
                catch (Exception _Exception)
                {
                    //HCoreHelper.LogException("ActorCoreUsageHandler", _Exception);
                }
            });
        }
    }
    internal class ActorCoreUsageHandlerPart : ReceiveActor
    {
        HCoreContextLogging _HCoreContextLogging;
        public ActorCoreUsageHandlerPart()
        {
            Receive<OUserReference>(async UserReference =>
            {
                try
                {
                    if (UserReference.LogRequest == 1)
                    {
                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            string ResponseBody = JsonConvert.SerializeObject(UserReference.ResponseData,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore,
                               ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                           });

                            #region Add Usage Log
                            HCLCoreUsage _HCLCoreUsage = new HCLCoreUsage();
                            _HCLCoreUsage.Guid = UserReference.RequestKey;
                            if (UserReference.AppVersionId != 0)
                            {
                                _HCLCoreUsage.AppVersionId = UserReference.AppVersionId;
                            }
                            if (UserReference.ApiId != 0)
                            {
                                _HCLCoreUsage.ApiId = UserReference.ApiId;
                            }
                            if (UserReference.FeatureId != 0)
                            {
                                _HCLCoreUsage.FeatureId = UserReference.FeatureId;
                            }
                            if (UserReference.AccountId != 0)
                            {
                                _HCLCoreUsage.AccountId = UserReference.AccountId;
                            }
                            if (UserReference.AccountSessionId != 0)
                            {
                                _HCLCoreUsage.SessionId = UserReference.AccountSessionId;
                            }
                            if (!string.IsNullOrEmpty(UserReference.RequestIpAddress))
                            {
                                _HCLCoreUsage.IpAddress = UserReference.RequestIpAddress;
                            }
                            if (UserReference.RequestLatitude != 0)
                            {
                                _HCLCoreUsage.Latitude = UserReference.RequestLatitude;
                            }
                            if (UserReference.RequestLongitude != 0)
                            {
                                _HCLCoreUsage.Longitude = UserReference.RequestLongitude;
                            }
                            if (!string.IsNullOrEmpty(UserReference.Request))
                            {
                                _HCLCoreUsage.Request = UserReference.Request;
                            }
                            if (!string.IsNullOrEmpty(ResponseBody))
                            {
                                _HCLCoreUsage.Response = ResponseBody;
                            }
                            _HCLCoreUsage.RequestTime = UserReference.RequestTime;
                            _HCLCoreUsage.ResponseTime = UserReference.ResponseTime;
                            _HCLCoreUsage.TimeDifference = (_HCLCoreUsage.ResponseTime - UserReference.RequestTime).Value.Milliseconds;
                            if (UserReference.ResponseStatus == "Success")
                            {
                                _HCLCoreUsage.StatusId = HelperStatus.Default.Active;
                            }
                            else
                            {
                                _HCLCoreUsage.StatusId = HelperStatus.Default.Blocked;
                            }
                            _HCoreContextLogging.HCLCoreUsage.Add(_HCLCoreUsage);
                            await _HCoreContextLogging.SaveChangesAsync();
                            await _HCoreContextLogging.DisposeAsync();
                            #endregion
                        }
                    }

                    //if (UserReference.ApiId == 0 && UserReference.ResponseStatus == "Success")
                    //{
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        HCCoreCommon _CoreCommon = new HCCoreCommon();
                    //        _CoreCommon.Guid = HCoreHelper.GenerateGuid();
                    //        _CoreCommon.TypeId = HCoreConstant.HelperType.Api;
                    //        _CoreCommon.Name = UserReference.Task;
                    //        _CoreCommon.SystemName = UserReference.Task;
                    //        _CoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        _CoreCommon.StatusId = HCoreConstant.HelperStatus.Default.Active;
                    //        if (UserReference.AccountId != 0)
                    //        {
                    //            _CoreCommon.CreatedById = UserReference.AccountId;
                    //        }
                    //        _HCoreContext.HCCoreCommon.Add(_CoreCommon);
                    //        _HCoreContext.SaveChanges();
                    //        _HCoreContext.Dispose();
                    //        UserReference.ApiId = _CoreCommon.Id;
                    //        HCoreSystem.DataStore_RefreshApi();
                    //    }
                    //}


                    //using ( _HCoreContext = new HCoreContext())
                    //{
                    //    if (UserReference.AccountSessionId != 0)
                    //    {
                    //        var SessionDetails = _HCoreContext.HCUAccountSession.Where(x => x.Id == UserReference.AccountSessionId).FirstOrDefault();
                    //        if (SessionDetails != null)
                    //        {
                    //            SessionDetails.LastActivityDate = UserReference.RequestTime;
                    //        }
                    //    }
                    //    #region Log Api Count
                    //    if (UserReference.ApiId != 0)
                    //    {
                    //        var ApiDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.ApiId).FirstOrDefault();
                    //        if (ApiDetails != null)
                    //        {
                    //            if (ApiDetails.Count == null)
                    //            {
                    //                ApiDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                ApiDetails.Count = ApiDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    #region Log App Count
                    //    if (UserReference.AppId != 0)
                    //    {
                    //        var AppDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppId).FirstOrDefault();
                    //        if (AppDetails != null)
                    //        {
                    //            if (AppDetails.Count == null)
                    //            {
                    //                AppDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                AppDetails.Count = AppDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    #region Log App Version Count
                    //    if (UserReference.AppVersionId != 0)
                    //    {
                    //        var AppVersionDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppVersionId).FirstOrDefault();
                    //        if (AppVersionDetails != null)
                    //        {
                    //            if (AppVersionDetails.Count == null)
                    //            {
                    //                AppVersionDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                AppVersionDetails.Count = AppVersionDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    _HCoreContext.SaveChanges();
                    //    _HCoreContext.Dispose();
                    //}

                    //if (UserReference.LogRequest == 1)
                    //{
                    //    using (_HCoreContextLogging = new HCoreContextLogging())
                    //    {
                    //        string ResponseBody = JsonConvert.SerializeObject(UserReference.ResponseData,
                    //       new JsonSerializerSettings()
                    //       {
                    //           NullValueHandling = NullValueHandling.Ignore,
                    //           ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    //       });

                    //        #region Add Usage Log
                    //        HCLCoreUsage _HCLCoreUsage = new HCLCoreUsage();
                    //        _HCLCoreUsage.Guid = UserReference.RequestKey;
                    //        if (UserReference.AppVersionId != 0)
                    //        {
                    //            _HCLCoreUsage.AppVersionId = UserReference.AppVersionId;
                    //        }
                    //        if (UserReference.ApiId != 0)
                    //        {
                    //            _HCLCoreUsage.ApiId = UserReference.ApiId;
                    //        }
                    //        if (UserReference.FeatureId != 0)
                    //        {
                    //            _HCLCoreUsage.FeatureId = UserReference.FeatureId;
                    //        }
                    //        if (UserReference.AccountId != 0)
                    //        {
                    //            _HCLCoreUsage.UserAccountId = UserReference.AccountId;
                    //        }
                    //        if (UserReference.AccountSessionId != 0)
                    //        {
                    //            _HCLCoreUsage.SessionId = UserReference.AccountSessionId;
                    //        }
                    //        if (!string.IsNullOrEmpty(UserReference.RequestIpAddress))
                    //        {
                    //            _HCLCoreUsage.IpAddress = UserReference.RequestIpAddress;
                    //        }
                    //        if (UserReference.RequestLatitude != 0)
                    //        {
                    //            _HCLCoreUsage.Latitude = UserReference.RequestLatitude;
                    //        }
                    //        if (UserReference.RequestLongitude != 0)
                    //        {
                    //            _HCLCoreUsage.Longitude = UserReference.RequestLongitude;
                    //        }
                    //        if (!string.IsNullOrEmpty(UserReference.Request))
                    //        {
                    //            _HCLCoreUsage.Request = UserReference.Request;
                    //        }
                    //        if (!string.IsNullOrEmpty(ResponseBody))
                    //        {
                    //            _HCLCoreUsage.Response = ResponseBody;
                    //        }
                    //        _HCLCoreUsage.RequestTime = UserReference.RequestTime;
                    //        _HCLCoreUsage.ResponseTime = UserReference.ResponseTime;
                    //        _HCLCoreUsage.TimeDifference = (_HCLCoreUsage.ResponseTime - UserReference.RequestTime).Value.Milliseconds;
                    //        if (UserReference.ResponseStatus == "Success")
                    //        {
                    //            _HCLCoreUsage.StatusId = HelperStatus.Default.Active;
                    //        }
                    //        else
                    //        {
                    //            _HCLCoreUsage.StatusId = HelperStatus.Default.Blocked;
                    //        }
                    //        _HCoreContextLogging.HCLCoreUsage.Add(_HCLCoreUsage);
                    //        _HCoreContextLogging.SaveChanges();
                    //        _HCoreContextLogging.Dispose();
                    //        #endregion
                    //    }
                    //}
                }
                catch (Exception _Exception)
                {
                    //HCoreHelper.LogException("ActorCoreUsageHandler", _Exception);
                }
            });
        }
    }

    internal class ActorCoreTerminalUsageHandler : ReceiveActor
    {
        HCoreContext _HCoreContext;
        //HCoreContextLogging _HCoreContextLogging;
        public ActorCoreTerminalUsageHandler()
        {
            Receive<OUserReference>(async UserReference =>
            {
                try
                {
                    //string ResponseBodyItem = JsonConvert.SerializeObject(UserReference.ResponseData,
                    //                           new JsonSerializerSettings()
                    //                           {
                    //                               NullValueHandling = NullValueHandling.Ignore
                    //                           });
                    //UserReference.ResponseData = ResponseBodyItem;
                    if (UserReference.ApiId == 0 && UserReference.ResponseStatus == "Success")
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            HCCoreCommon _CoreCommon = new HCCoreCommon();
                            _CoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _CoreCommon.TypeId = HelperType.Api;
                            _CoreCommon.Name = UserReference.Task;
                            _CoreCommon.SystemName = UserReference.Task;
                            _CoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _CoreCommon.StatusId = HelperStatus.Default.Active;
                            if (UserReference.AccountId != 0)
                            {
                                _CoreCommon.CreatedById = UserReference.AccountId;
                            }
                            _HCoreContext.HCCoreCommon.Add(_CoreCommon);
                            await _HCoreContext.SaveChangesAsync();
                            UserReference.ApiId = _CoreCommon.Id;
                            HCoreSystem.DataStore_RefreshApi();
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (UserReference.AccountSessionId != 0)
                        {
                            var SessionDetails = await _HCoreContext.HCUAccountSession.Where(x => x.Id == UserReference.AccountSessionId).FirstOrDefaultAsync();
                            if (SessionDetails != null)
                            {
                                SessionDetails.LastActivityDate = UserReference.RequestTime;
                            }
                        }
                        #region Log Api Count
                        if (UserReference.ApiId != 0)
                        {
                            var ApiDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.ApiId).FirstOrDefaultAsync();
                            if (ApiDetails != null)
                            {
                                if (ApiDetails.Count == null)
                                {
                                    ApiDetails.Count = 1;
                                }
                                else
                                {
                                    ApiDetails.Count = ApiDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        #region Log App Count
                        if (UserReference.AppId != 0)
                        {
                            var AppDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppId).FirstOrDefaultAsync();
                            if (AppDetails != null)
                            {
                                if (AppDetails.Count == null)
                                {
                                    AppDetails.Count = 1;
                                }
                                else
                                {
                                    AppDetails.Count = AppDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        #region Log App Version Count
                        if (UserReference.AppVersionId != 0)
                        {
                            var AppVersionDetails = await _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppVersionId).FirstOrDefaultAsync();
                            if (AppVersionDetails != null)
                            {
                                if (AppVersionDetails.Count == null)
                                {
                                    AppVersionDetails.Count = 1;
                                }
                                else
                                {
                                    AppVersionDetails.Count = AppVersionDetails.Count + 1;
                                }
                            }
                        }
                        #endregion
                        await _HCoreContext.SaveChangesAsync();
                    }
                    var _ActorCoreUsageHandlerPart = ActorSystem.Create("ActorCoreTerminalUsageHandlerPart");
                    var _ActorCoreUsageHandlerPartGreet = _ActorCoreUsageHandlerPart.ActorOf<ActorCoreTerminalUsageHandlerPart>("ActorCoreTerminalUsageHandlerPart");
                    _ActorCoreUsageHandlerPartGreet.Tell(UserReference);

                }
                catch (Exception _Exception)
                {
                    //HCoreHelper.LogException("ActorCoreUsageHandler", _Exception);
                }
            });
        }
    }
    internal class ActorCoreTerminalUsageHandlerPart : ReceiveActor
    {
        HCoreContextLogging _HCoreContextLogging;
        public ActorCoreTerminalUsageHandlerPart()
        {
            Receive<OUserReference>(async UserReference =>
            {
                try
                {
                    if (UserReference.LogRequest == 1)
                    {
                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            string ResponseBody = JsonConvert.SerializeObject(UserReference.ResponseData,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore,
                               ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                           });

                            #region Add Usage Log
                            hcl_coreusage_terminal _hcl_coreusage_terminal = new hcl_coreusage_terminal();
                            _hcl_coreusage_terminal.guid = UserReference.RequestKey;
                            _hcl_coreusage_terminal.terminal_id = UserReference.RequestKey;
                            if (UserReference.AppVersionId != 0)
                            {
                                _hcl_coreusage_terminal.appversion_id = UserReference.AppVersionId;
                            }
                            if (UserReference.ApiId != 0)
                            {
                                _hcl_coreusage_terminal.api_id = UserReference.ApiId;
                            }
                            if (!string.IsNullOrEmpty(UserReference.RequestIpAddress))
                            {
                                _hcl_coreusage_terminal.ip_address = UserReference.RequestIpAddress;
                            }
                            if (!string.IsNullOrEmpty(UserReference.Request))
                            {
                                _hcl_coreusage_terminal.request = UserReference.Request;
                            }
                            if (!string.IsNullOrEmpty(ResponseBody))
                            {
                                _hcl_coreusage_terminal.response = ResponseBody;
                            }
                            _hcl_coreusage_terminal.request_time = UserReference.RequestTime;
                            _hcl_coreusage_terminal.response_time = UserReference.ResponseTime;
                            if (UserReference.ResponseStatus == "Success")
                            {
                                _hcl_coreusage_terminal.status_id = HelperStatus.Default.Active;
                            }
                            else
                            {
                                _hcl_coreusage_terminal.status_id = HelperStatus.Default.Blocked;
                            }
                            await _HCoreContextLogging.hcl_coreusage_terminal.AddAsync(_hcl_coreusage_terminal);
                            await _HCoreContextLogging.SaveChangesAsync();
                            await _HCoreContextLogging.DisposeAsync();
                            #endregion
                        }
                    }

                    //if (UserReference.ApiId == 0 && UserReference.ResponseStatus == "Success")
                    //{
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        HCCoreCommon _CoreCommon = new HCCoreCommon();
                    //        _CoreCommon.Guid = HCoreHelper.GenerateGuid();
                    //        _CoreCommon.TypeId = HCoreConstant.HelperType.Api;
                    //        _CoreCommon.Name = UserReference.Task;
                    //        _CoreCommon.SystemName = UserReference.Task;
                    //        _CoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        _CoreCommon.StatusId = HCoreConstant.HelperStatus.Default.Active;
                    //        if (UserReference.AccountId != 0)
                    //        {
                    //            _CoreCommon.CreatedById = UserReference.AccountId;
                    //        }
                    //        _HCoreContext.HCCoreCommon.Add(_CoreCommon);
                    //        _HCoreContext.SaveChanges();
                    //        _HCoreContext.Dispose();
                    //        UserReference.ApiId = _CoreCommon.Id;
                    //        HCoreSystem.DataStore_RefreshApi();
                    //    }
                    //}


                    //using ( _HCoreContext = new HCoreContext())
                    //{
                    //    if (UserReference.AccountSessionId != 0)
                    //    {
                    //        var SessionDetails = _HCoreContext.HCUAccountSession.Where(x => x.Id == UserReference.AccountSessionId).FirstOrDefault();
                    //        if (SessionDetails != null)
                    //        {
                    //            SessionDetails.LastActivityDate = UserReference.RequestTime;
                    //        }
                    //    }
                    //    #region Log Api Count
                    //    if (UserReference.ApiId != 0)
                    //    {
                    //        var ApiDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.ApiId).FirstOrDefault();
                    //        if (ApiDetails != null)
                    //        {
                    //            if (ApiDetails.Count == null)
                    //            {
                    //                ApiDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                ApiDetails.Count = ApiDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    #region Log App Count
                    //    if (UserReference.AppId != 0)
                    //    {
                    //        var AppDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppId).FirstOrDefault();
                    //        if (AppDetails != null)
                    //        {
                    //            if (AppDetails.Count == null)
                    //            {
                    //                AppDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                AppDetails.Count = AppDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    #region Log App Version Count
                    //    if (UserReference.AppVersionId != 0)
                    //    {
                    //        var AppVersionDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == UserReference.AppVersionId).FirstOrDefault();
                    //        if (AppVersionDetails != null)
                    //        {
                    //            if (AppVersionDetails.Count == null)
                    //            {
                    //                AppVersionDetails.Count = 1;
                    //            }
                    //            else
                    //            {
                    //                AppVersionDetails.Count = AppVersionDetails.Count + 1;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //    _HCoreContext.SaveChanges();
                    //    _HCoreContext.Dispose();
                    //}

                    //if (UserReference.LogRequest == 1)
                    //{
                    //    using (_HCoreContextLogging = new HCoreContextLogging())
                    //    {
                    //        string ResponseBody = JsonConvert.SerializeObject(UserReference.ResponseData,
                    //       new JsonSerializerSettings()
                    //       {
                    //           NullValueHandling = NullValueHandling.Ignore,
                    //           ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    //       });

                    //        #region Add Usage Log
                    //        HCLCoreUsage _HCLCoreUsage = new HCLCoreUsage();
                    //        _HCLCoreUsage.Guid = UserReference.RequestKey;
                    //        if (UserReference.AppVersionId != 0)
                    //        {
                    //            _HCLCoreUsage.AppVersionId = UserReference.AppVersionId;
                    //        }
                    //        if (UserReference.ApiId != 0)
                    //        {
                    //            _HCLCoreUsage.ApiId = UserReference.ApiId;
                    //        }
                    //        if (UserReference.FeatureId != 0)
                    //        {
                    //            _HCLCoreUsage.FeatureId = UserReference.FeatureId;
                    //        }
                    //        if (UserReference.AccountId != 0)
                    //        {
                    //            _HCLCoreUsage.UserAccountId = UserReference.AccountId;
                    //        }
                    //        if (UserReference.AccountSessionId != 0)
                    //        {
                    //            _HCLCoreUsage.SessionId = UserReference.AccountSessionId;
                    //        }
                    //        if (!string.IsNullOrEmpty(UserReference.RequestIpAddress))
                    //        {
                    //            _HCLCoreUsage.IpAddress = UserReference.RequestIpAddress;
                    //        }
                    //        if (UserReference.RequestLatitude != 0)
                    //        {
                    //            _HCLCoreUsage.Latitude = UserReference.RequestLatitude;
                    //        }
                    //        if (UserReference.RequestLongitude != 0)
                    //        {
                    //            _HCLCoreUsage.Longitude = UserReference.RequestLongitude;
                    //        }
                    //        if (!string.IsNullOrEmpty(UserReference.Request))
                    //        {
                    //            _HCLCoreUsage.Request = UserReference.Request;
                    //        }
                    //        if (!string.IsNullOrEmpty(ResponseBody))
                    //        {
                    //            _HCLCoreUsage.Response = ResponseBody;
                    //        }
                    //        _HCLCoreUsage.RequestTime = UserReference.RequestTime;
                    //        _HCLCoreUsage.ResponseTime = UserReference.ResponseTime;
                    //        _HCLCoreUsage.TimeDifference = (_HCLCoreUsage.ResponseTime - UserReference.RequestTime).Value.Milliseconds;
                    //        if (UserReference.ResponseStatus == "Success")
                    //        {
                    //            _HCLCoreUsage.StatusId = HelperStatus.Default.Active;
                    //        }
                    //        else
                    //        {
                    //            _HCLCoreUsage.StatusId = HelperStatus.Default.Blocked;
                    //        }
                    //        _HCoreContextLogging.HCLCoreUsage.Add(_HCLCoreUsage);
                    //        _HCoreContextLogging.SaveChanges();
                    //        _HCoreContextLogging.Dispose();
                    //        #endregion
                    //    }
                    //}
                }
                catch (Exception _Exception)
                {
                    //HCoreHelper.LogException("ActorCoreUsageHandler", _Exception);
                }
            });
        }
    }

}
