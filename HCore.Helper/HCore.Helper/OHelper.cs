//==================================================================================
// FileName: OHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;

namespace HCore.Helper
{
    public class OResponse
    {
        //public string? Task { get; set; } 
        public string? Status { get; set; }
        public string? Message { get; set; }
        public object Result { get; set; }
        public string? ResponseCode { get; set; }
        public string? Action { get; set; }
        public string? Mode { get; set; }
    }

    public class OResponseData
    {
        //public string? Task { get; set; } 
        public string? Status { get; set; }
        public string? Message { get; set; }
        public string? Result { get; set; }
        public object Data { get; set; }
        public string? ResponseCode { get; set; }
        public string? Action { get; set; }
        public string? Mode { get; set; }
    }

    public class OReferralAmountResponse
    {

        public string? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public decimal? Amount { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? DateSet { get; set; }
    }

    public class RequestV1
    {
        public string? data { get; set; }
    }
    public class ResponseV1
    {
        public string? data { get; set; }
    }

    public class OAddress
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }

        public string? Address { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }

        public string? Name { get; set; }
        public string? ContactNumber { get; set; }
        public string? EmailAddress { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public long? CityId { get; set; }
        public string? CityCode { get; set; }
        public string? CityName { get; set; }

        public long? CityAreaId { get; set; }
        public string? CityAreaCode { get; set; }
        public string? CityAreaName { get; set; }

        public long? StateId { get; set; }
        public string? StateCode { get; set; }
        public string? StateName { get; set; }

        public int? CountryId { get; set; }
        public string? CountryCode { get; set; }
        public string? CountryIsd { get; set; }
        public string? CountryIso { get; set; }
        public string? CountryName { get; set; }

        public string? MapAddress { get; set; }

        public string? Reference { get; set; }
    }



    public class OAddressResponse
    {
        public string? Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string? CityAreaName { get; set; }

        public long CityAreaId { get; set; }
        public string? CityName { get; set; }

        public long CityId { get; set; }
        public string? StateName { get; set; }

        public long StateId { get; set; }
        public string? CountryName { get; set; }

        public int CountryId { get; set; }
        public string? MapAddress { get; set; }
    }


    //public class OAddressDetails
    //{
    //    public string? Address { get; set; }
    //    public double? Latitude { get; set; }
    //    public double? Longitude { get; set; }
    //    public string? CityAreaName { get; set; }
    //    public long? CityAreaId { get; set; }
    //    public string? CityName { get; set; }
    //    public long? CityId { get; set; }
    //    public string? StateName { get; set; }
    //    public long? StateId { get; set; }
    //    public string? CountryName { get; set; }
    //    public int? CountryId { get; set; }
    //    public string? MapAddress { get; set; }
    //}

    public class OUserReference
    {

        public bool IsRsa { get; set; }
        public string? ResponseType { get; set; }
        public string? Data { get; set; }
        internal long StatusId { get; set; }
        public string? Task { get; set; }
        #region Account Type
        public long AccountTypeId { get; set; }
        public string? AccountTypeName { get; set; }
        public string? AccountTypeSystemName { get; set; }
        #endregion

        #region Account
        public long AccountId { get; set; }
        public string? AccountKey { get; set; }
        public string? AccountCode { get; set; }
        public DateTime AccountCreateDate { get; set; }
        #endregion

        #region Account Session
        public long AccountSessionId { get; set; }
        #endregion

        #region User Details
        public string? UserName { get; set; }
        public string? DisplayName { get; set; }
        public string? Name { get; set; }
        public string? EmailAddress { get; set; }
        public string? ContactNumber { get; set; }
        #endregion

        #region Account Owner
        public long? AccountOwnerId { get; set; }
        public string? AccountOwnerKey { get; set; }
        public string? AccountOwnerDisplayName { get; set; }
        #endregion

        #region User Account Device
        public long DeviceId { get; set; }
        public string? DeviceKey { get; set; }
        public string? DeviceSerialNumber { get; set; }
        #endregion

        #region  Country
        public int? CountryId { get; set; }
        public string? CountryKey { get; set; }
        public string? CountryName { get; set; }
        public string? CountryIsd { get; set; }
        public string? CountryIso { get; set; }
        public string? CountryTimeZone { get; set; }
        public int? CountryMobileNumberLength { get; set; }
        #endregion

        #region Request Location
        public double RequestLatitude { get; set; }
        public double RequestLongitude { get; set; }
        public string? RequestIpAddress { get; set; }
        #endregion
        #region Api | App  | App Versions | Os
        public DateTime RequestTime { get; set; }
        public DateTime ResponseTime { get; set; }
        public string? RequestKey { get; set; }
        public long? LogRequest { get; set; }
        public long AppId { get; set; }
        public long AppVersionId { get; set; }
        public long OsId { get; set; }
        public long FeatureId { get; set; }
        public long ApiId { get; set; }
        public string? Request { get; set; }
        public object ResponseData { get; set; }
        public string? ResponseStatus { get; set; }
        #endregion

        public int? PlanId { get; set; }
        public int? DebitMandateId { get; set; }
        public int SystemCountry { get; set; } = 1;
    }
    public class OList
    {
        public class Request
        {

            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }

            public long AuthAccountId { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public bool RefreshCount { get; set; } = true;
            public int Offset { get; set; }
            public int Limit { get; set; }
            public int TotalRecords { get; set; }
            public string? SearchParameter { get; set; }
            public string? SearchCondition { get; set; }
            public string? SortExpression { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SubReferenceKey { get; set; }
            public long ReferenceId { get; set; }
            public long SubReferenceId { get; set; }
            public string? Type { get; set; }
            public string? CategoryIds { get; set; }

            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public bool IsDownload { get; set; }
            public bool IsTucPlus { get; set; }

            public int ListType { get; set; }
            //public long Status { get; set; }
            public int Radius { get; set; }

            //public long TypeId { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
            public long? StoreReferenceId { get; set; }
            public long? CustomerReferenceId { get; set; }

            public int TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }


            public string? AccountTypeCode { get; set; }
            public long RedeemFrom { get; set; }
            public string MobileNumber { get; set; }
            public string AccessPin { get; set; }
            public string CountryIsd { get; set; }
            public long ProgramId { get; set; }

            public string Slug { get; set; }
        }

        public class Response
        {
            public int Offset { get; set; }
            public int? Limit { get; set; }
            public int TotalRecords { get; set; }
            public object Data { get; set; }
            public object SubData { get; set; }
            public double? Amount { get; set; }
            public double? TotalAmount { get; set; }
            public double? TotalCharge { get; set; }
            public double? TotalComission { get; set; }
            public double? Total { get; set; }
            public double? PurchaseAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? ThankUAmount { get; set; }
            public double? MerchantAmount { get; set; }
            public long? Inactive { get; set; }
            public long? Active { get; set; }
            public long? Suspended { get; set; }
            public long? Blocked { get; set; }

        }

        public class ORequest
        {
            public long AuthAccountId { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? Gender { get; set; }
            public string? ReferralCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public OUserReference? UserReference { get; set; }
            public OStorageContent ImageContent { get; set; }
        }

        public class CancelDealRequest
        {
            public string? TotalDealCount { get; set; }
            public string? DealKey { get; set; }
            public double CancelCount { get; set; }
            public double DealPrice { get; set; }
            public long? DealId { get; set; }
            public string? PaymentReference { get; set; }

            public string? TransactionReference { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class FRequest
        {
            public string? SourceCode { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public double Amount { get; set; }
            public string? PaymentReference { get; set; }
            public string? TransactionReference { get; set; }
            public string? Comment { get; set; }
            public string? PaymentType { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class FResponse
        {
            public string? ProductCode { get; set; }
            public double Credit { get; set; }
            public double Debit { get; set; }
            public double Balance { get; set; }
        }
    }

    public class HostConfig
    {
        public string[] Host { get; set; }
        public string Type { get; set; }
    }

    public class Globals
    {
        public string? AppUserPrefix { get; set; }
        public string? Name { get; set; }
        public string? WebsiteUrl { get; set; }
        public string? DomainName { get; set; }
        public string? CopyRightsText { get; set; }
        public string? CompanyNotificationEmailAddress { get; set; }

        public string? Default_Icon { get; set; }
        public string? Default_Store_Icon { get; set; }
        public string? Default_Poster { get; set; }

        public int DefaultRecordsLimit { get; set; }
        public int StorageSourceId { get; set; }
        public string? StorageSourceCode { get; set; }

        public string? StorageUrl { get; set; }
        public string? StorageLocation { get; set; }
        public string? StorageAccessKey { get; set; }
        public string? StoragePrivateKey { get; set; }


        public string? DateFormatNumeric { get; set; }
        public string? DateFormatText { get; set; }
        public string? DateTimeFormatNumeric { get; set; }
        public string? DateTimeFormatText { get; set; }


        public string? SendGridKey { get; set; }
        public string? SendGridDefaultReceiverName { get; set; }
        public string? SendGridSenderEmail { get; set; }
        public string? SendGridSenderName { get; set; }

        public int SystemEntryRoundDouble { get; set; }
        public int SystemExitRoundDouble { get; set; }
        public int SystemRoundPercentage { get; set; }
        public int SystemMultiplyByDigit { get; set; }
        public int AppUserBalanceValidityDays { get; set; }


        public string? Email_CompanyCopyRights { get; set; }
        public string? Email_CompanyAddress { get; set; }


        public string? PaystackPrivateKey { get; set; }
        public string? PaystackPublicKey { get; set; }

        public string? PaystackGhanaPrivateKey { get; set; }
        public string? PaystackGhanaPublicKey { get; set; }
        public string? LccKey { get; set; }
        public string? CoralPayKey { get; set; }

        public string MailerliteToken { get; set; }

        public string? VasCoralPayUrl { get; set; }
        public string? VasCoralPayUserName { get; set; }
        public string? VasCoralPayPassword { get; set; }

        public string? Shipmonk_Url { get; set; }
        public string? Shipmonk_PublicKey { get; set; }
        public string? Shipmonk_SecretKey { get; set; }

        //public long WakanowId { get; set; }
        public string? EvolveUrl { get; set; }
        public string? EvolveApiKey { get; set; }
        public long EvolveProductId { get; set; }

        public string? SrverCon { get; set; }

        public string? CellulantUrl { get; set; }
        public string? CellulantCustomUrl { get; set; }

        public OPanelUrl PanelUrl { get; set; }

        public string? DellymenUrl { get; set; }
        public string? DellymenApiKey { get; set; }

        public string? PickUpRequestedTime { get; set; }
        public string? DeliveryRequestedTime { get; set; }

        public string ExpresspayUrl { get; set; }
        public string ExpresspayAuthToken { get; set; }
        public string ExpresspayApiKey { get; set; }
        public string DellymanWebhookSecret { get; set; }

        public string? RabbitMQHostName { get; set; }
        public string? RabbitMQUsername { get; set; }
        public string? RabbitMQPassword { get; set; }
        public object? ConnectionObject { get; set; }

        public string? SQSQueueUrl { get; set; }
        public string? SQSMessageGroupId { get; set; }
        public string? SQSMessageDupId { get; set; }
        public string? SQSAccessKey { get; set; }
        public string? SQSSecretKey { get; set; }

        public string? FlutterwaveUrl { get; set; }
        public string? FlutterwaveApiKey { get; set; }

        public string? ShiipUrl { get; set; }
        public string? ShiipUserId { get; set; }
        public string? ShiipUserKey { get; set; }
        public int? ShiipUserAccountId { get; set; }
    }
    public class OPanelUrl
    {
        public string? Merchant { get; set; }
        public string? Admin { get; set; }
        public string? Ptsp { get; set; }
        public string? Pssp { get; set; }
        public string? Acquirer { get; set; }
        public string? Partner { get; set; }
    }
    public class OStorageContent
    {

        public string? UserKey { get; set; }
        public string? UserAccountKey { get; set; }
        public string? TypeCode { get; set; }
        public string? HelperCode { get; set; }
        public string? Name { get; set; }
        public string? Extension { get; set; }
        public string? Content { get; set; }
        public string? Reference { get; set; }
        public string? Tags { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserActivityLog
    {
        public List<OActivity> Activities { get; set; }
        public string? EntityName { get; set; }
        public string? ParentReferenceKey { get; set; }
        public string? ReferenceKey { get; set; }
        public List<OChangeLog> ChangeLog { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OActivity
    {
        public ActivityType ActivityType { get; set; }
        public string? ParentReferenceKey { get; set; }
        public string? ReferenceKey { get; set; }
        public string? OldValue { get; set; }
        public string? NewValue { get; set; }
        public string? Description { get; set; }
        public string? ActivityCode { get; set; }
    }
    public class OChangeLog
    {
        public ActivityType ActivityType { get; set; }
        public string? FieldName { get; set; }
        public string? OldValue { get; set; }
        public string? NewValue { get; set; }
    }
    public class OConfiguration
    {
        public long ReferenceId { get; set; }
        public string? Name { get; set; }
        public string? Value { get; set; }
        public string? TypeName { get; set; }
        public string? TypeCode { get; set; }
    }
    public class OAuth
    {
        public class Response
        {
            public long AccountId { get; set; }
            public string? Status { get; set; }
            public OResponse UserResponse { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Request
        {
            public string? fx { get; set; }
            public string? vx { get; set; }
            public string? zx { get; set; }
        }
    }
    internal class OHCoreSmsBroadCaster
    {
        public SmsType SmsType { get; set; }
        public string? SenderId { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }
        public string? Subject { get; set; }
        public string? Message { get; set; }
        public string? CountryIsd { get; set; }
        public string? ApiKey { get; set; }
        public string? BaseUrl { get; set; }
        public string? SystemReference { get; set; }
        public long? AccountId { get; set; }
        public long? TransactionId { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    internal class OHCoreEmailBroadCaster
    {
        public string? SendGridKey { get; set; }
        public string? TemplateId { get; set; }
        public string? ToName { get; set; }
        public string? ToAddress { get; set; }
        public object Parameters { get; set; }
        public string? attachmentBase64 { get; set; }
        public string? AttachmentName { get; set; }
        public Dictionary<string, string?> ToAddressList { get; set; }
    }

    public class OReference
    {
        public long AuthAccountId { get; set; }
        public long TypeId { get; set; }
        public string? PaymentSource { get; set; }
        public string? ReferenceCode { get; set; }
        public long AccountId { get; set; }
        public string? AccountKey { get; set; }
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public long AccountTypeCode { get; set; }
        public long BranchId { get; set; }
        public string? BranchKey { get; set; }
        public string? Comment { get; set; }
        public string? StatusCode { get; set; }
        public long RedeemFrom { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public OUserReference? UserReference { get; set; }
    }


    public class ONotification
    {
        public string? icon { get; set; }
        public string? title { get; set; }
        public string? body { get; set; }
        public string? image { get; set; }
    }
    public class ONotificationDevicePushBody
    {

        public string? Task { get; set; }
        public string? Subject { get; set; }
        public string? Message { get; set; }
        public string? NavigateTo { get; set; }
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? ConfirmText { get; set; }
        public object Data { get; set; }
        public bool ForceNavigation { get; set; } = false;
    }
    public class ONotificationDevicePush
    {
        public ONotification notification { get; set; }
        public string? to { get; set; }
        public string? priority { get; set; }
        public object data { get; set; }
    }
    public class FCMResponse
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMResult> results { get; set; }
    }

    public class FCMResult
    {
        public string? message_id { get; set; }
    }

    public class Category
    {
        public int ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? Name { get; set; }
        public string? IconUrl { get; set; }
    }
    public class ContactPerson
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }
    }

    public class OMobileNumberStatus
    {
        public bool IsNumberValid { get; set; }
        public string? MobileNumber { get; set; }
        public string? Isd { get; set; }
    }

    public class OCoreCountry
    {
        public int? ReferenceId { get; set; }
        public string? Name { get; set; }
        public string? Isd { get; set; }
        public int MobileNumberLength { get; set; }
        public string? TimeZoneName { get; set; }
        public DateTime GmtTime { get; set; }
        public DateTime LocalTime { get; set; }
    }
    public class DeleteCustomerInfo
    {
        public class Request
        {
            public string MobileNumber { get; set; }
            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }

        }
        public class Response
        {
            public int StatusCode { get; set; }
            public string ResponseMessage { get; set; }
        }
    }

    public class CustomerAccountDetails
    {
        public string BankCode { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public long UserId { get; set; }
    }
}
