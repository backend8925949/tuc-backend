//==================================================================================
// FileName: ManageSettlement.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Object;
using HCore.ThankU.Framework;
using HCore.ThankU.Object;
using HCore.Helper;

namespace HCore.ThankU
{
    public class ManageSettlement
    {
        FrameworkSettlement _FrameworkSettlement;
        //public void CreateRedeemSettlementInvoices()
        //{
        //    _FrameworkSettlement = new FrameworkSettlement();
        //    _FrameworkSettlement.CreateRedeemSettlementInvoices();
        //}

        /// <summary>
        /// Description: Creates the reward settlement invoices.
        /// </summary>
        public void CreateRewardSettlementInvoices()
        {
            _FrameworkSettlement = new FrameworkSettlement();
            _FrameworkSettlement.CreateRewardSettlementInvoices();
        }
        /// <summary>
        /// Description: Processes the transaction bins.
        /// </summary>
        public void ProcessTransactionBins()
        {
            _FrameworkSettlement = new FrameworkSettlement();
            _FrameworkSettlement.ProcessTransactionBins();
        }

        //public OResponse CreatePaymentSettlement(OUserSettlement.Request _Request)
        //{
        //    _FrameworkSettlement = new FrameworkSettlement();
        //    return _FrameworkSettlement.CreatePaymentSettlement(_Request);
        //}
        /// <summary>
        /// Description: Updates the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePaymentInvoice(OUserSettlement.Request _Request)
        {
            _FrameworkSettlement = new FrameworkSettlement();
            return _FrameworkSettlement.UpdatePaymentInvoice(_Request);
        }

        /// <summary>
        /// Description: Gets the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPaymentInvoice(OUserSettlement.Request _Request)
        {
            _FrameworkSettlement = new FrameworkSettlement();
            return _FrameworkSettlement.GetPaymentInvoice(_Request);
        }
        //public void CreatePaymentSettlement()
        //{
        //    _FrameworkSettlement = new FrameworkSettlement();
        //    _FrameworkSettlement.CreatePaymentSettlement();
        //}
        /// <summary>
        /// Description: Gets the payment invoices.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPaymentInvoices(OList.Request _Request)
        {
            _FrameworkSettlement = new FrameworkSettlement();
            return _FrameworkSettlement.GetPaymentInvoices(_Request);
        }

        /// <summary>
        /// Description: Gets the settlements overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSettlementsOverview(OUserSettlement.Request _Request)
        {
            _FrameworkSettlement = new FrameworkSettlement();
            return _FrameworkSettlement.GetSettlementsOverview(_Request);
        }

    }
}
