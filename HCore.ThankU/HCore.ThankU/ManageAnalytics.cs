//==================================================================================
// FileName: ManageAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Object;
using HCore.ThankU.Framework;
using HCore.ThankU.Object;
using HCore.Helper;


namespace HCore.ThankU
{
   public class ManageAnalytics
    {
        FrameworkAnalytics _FrameworkAnalytics;
        FrameworkAnalyticsChart _FrameworkAnalyticsChart;
        /// <summary>
        /// Gets the user account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetUserAccountOverview(_Request);
        }
        /// <summary>
        /// Gets the user analytics.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAnalytics(OChart.Request _Request)
        {
            _FrameworkAnalyticsChart = new FrameworkAnalyticsChart();
            return _FrameworkAnalyticsChart.GetUserAnalytics(_Request);
        }
    }
}
