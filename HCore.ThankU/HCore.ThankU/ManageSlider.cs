//==================================================================================
// FileName: ManageSlider.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Object;
using HCore.ThankU.Framework;
using HCore.Helper;

namespace HCore.ThankU
{
    public class ManageSlider
    {
        FrameworkSlider _FrameworkSlider;
        /// <summary>
        /// Description: Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSliderImages(OList.Request _Request)
        {
            _FrameworkSlider = new FrameworkSlider();
            return _FrameworkSlider.GetSliderImages(_Request);
        }


    }
}
