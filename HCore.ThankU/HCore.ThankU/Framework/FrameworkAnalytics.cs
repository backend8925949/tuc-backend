//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to Analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Object;
using HCore.Helper;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankU.Framework
{
    internal class FrameworkAnalytics
    {
        #region Declare
        #endregion
        #region Entity
        HCoreContext _HCoreContext;
        #endregion
        #region Objects
        OAnalytics.Response _AnaResponse;
        OAnalytics.Rewards _AnaRewards;
        OAnalytics.Visitors _AnaVisitors;
        OAnalytics.Invoices _AnaInvoices;
        OAnalytics.RewardBalance _AnaRewardBalance;
        OAnalytics.OUsersCount _AnaUsersCount;
        OAnalytics.Distribution _AnaAmountDistribution;
        OAnalytics.RegSource _AnaRegSource;
        OAnalytics.Ratings _AnaRatings;
        #endregion
        /// <summary>
        /// Description: Gets the user account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountOverview(OAnalytics.Request _Request)
        {
            #region Declare
            _AnaResponse = new OAnalytics.Response();
            _AnaUsersCount = new OAnalytics.OUsersCount();
            _AnaRewards = new OAnalytics.Rewards();
            _AnaVisitors = new OAnalytics.Visitors();
            _AnaInvoices = new OAnalytics.Invoices();
            _AnaRewardBalance = new OAnalytics.RewardBalance();
            #endregion
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region System Helpers
                    List<OAnalytics.OTransactionType> TransactionTypes = _HCoreContext.HCCore
                        .Where(x => x.ParentId == HelperType.TransactionType && x.StatusId == StatusActive)
                                                  .Select(x => new OAnalytics.OTransactionType
                                                  {
                                                      ReferenceId = x.Id,
                                                      Name = x.Name,
                                                      Value = x.Value
                                                  }).ToList();
                    #endregion
                    foreach (var TransactionType in TransactionTypes)
                    {
                        TransactionType.Name = TransactionType.Name.Replace("Rewards", "").Replace("Reward", "").Replace("Redeems", "").Replace("Redeem", "");
                    }
                    DateTime StartTime = DateTime.UtcNow.Date.AddDays(-1);
                    DateTime EndTime = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1);
                    if (_Request.StartTime != null)
                    {
                        StartTime = (DateTime)_Request.StartTime;
                    }
                    if (_Request.EndTime != null)
                    {
                        EndTime = (DateTime)_Request.EndTime;
                    }
                    OAnalytics.Account UserAccount = _HCoreContext.HCUAccount
                                                   .Where(x => x.Guid == _Request.UserAccountKey)
                        .Select(x => new OAnalytics.Account
                        {
                            UserId = x.UserId,
                            OwnerId = x.Owner.Id,
                            OwnerParentId = x.Owner.Owner.Id,
                            UserAccountId = x.Id,
                            CreatedById = x.CreatedById,
                            CreateDate = x.CreateDate,
                            AccountTypeId = x.AccountTypeId,
                            AccountOperationTypeId = x.AccountOperationTypeId,
                        }).FirstOrDefault();
                    if (UserAccount != null)
                    {

                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            _AnaResponse = GetMerchantOverview(_Request, UserAccount, TransactionTypes, StartTime, EndTime);
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                        {
                            _AnaResponse = GetOwnerOverview(_Request, UserAccount, TransactionTypes, StartTime, EndTime);
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                        {
                            _AnaResponse = GetCreatedByOverview(_Request, UserAccount, TransactionTypes, StartTime, EndTime);
                        }
                        if (UserAccount.AccountTypeId == UserAccountType.Acquirer || UserAccount.AccountTypeId == UserAccountType.AcquirerSubAccount)
                        {
                            _AnaResponse = GetUserOverview(_Request, UserAccount, TransactionTypes, StartTime, EndTime);
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            _AnaResponse = GetUserOverview(_Request, UserAccount, TransactionTypes, StartTime, EndTime);
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant
                               || UserAccount.AccountTypeId == UserAccountType.PosAccount
                               || UserAccount.AccountTypeId == UserAccountType.PgAccount
                               || UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            {

                                _AnaInvoices.PaidInvoices = _HCoreContext.HCUAccountInvoice.Where(x => x.AccountId == UserAccount.UserAccountId && x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();
                                _AnaInvoices.UnPaidInvoices = _HCoreContext.HCUAccountInvoice.Where(x => x.AccountId == UserAccount.UserAccountId && x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();
                                _AnaInvoices.PaidInvoiceAmount = _HCoreContext.HCUAccountInvoice.Where(x => x.AccountId == UserAccount.UserAccountId && x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => x.TotalAmount) ?? 0;
                                _AnaInvoices.UnpaidInvoiceAmount = _HCoreContext.HCUAccountInvoice.Where(x => x.AccountId == UserAccount.UserAccountId && x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => x.TotalAmount) ?? 0;
                                double TotalSettlementAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(x => x.AccountId == UserAccount.UserAccountId && x.SourceId == TransactionSource.Settlement &&
                                                                     x.StatusId == StatusHelperId.Transaction_Success)
                                                              .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.AccountId == UserAccount.UserAccountId && x.SourceId == TransactionSource.Settlement &&
                                                                        x.ModeId == TransactionMode.Debit &&
                                                                        x.StatusId == StatusHelperId.Transaction_Success)
                                                                 .Sum(x => (double?)x.TotalAmount) ?? 0;
                                _AnaInvoices.UnSettledAmount = (TotalSettlementAmount - TotalDebit);
                                _AnaInvoices.SettledAmount = TotalSettlementAmount;
                                _AnaResponse.Invoices = _AnaInvoices;
                            }

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnaResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Rewards
                        _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Reward
                               && x.ModeId == TransactionMode.Debit
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Account.AccountTypeId == UserAccountType.MerchantCashier
                                   || x.Account.AccountTypeId == UserAccountType.Merchant
                                  ))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _AnaRewards.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Reward
                               && x.ModeId == TransactionMode.Debit
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Account.AccountTypeId == UserAccountType.MerchantCashier
                                   || x.Account.AccountTypeId == UserAccountType.Merchant))
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Reward
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && x.ModeId == TransactionMode.Debit
                               && (x.Account.AccountTypeId == UserAccountType.MerchantCashier
                                   || x.Account.AccountTypeId == UserAccountType.Merchant))
                                .Count();

                        _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Redeem
                               && x.ModeId == TransactionMode.Debit
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Redeem
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                               && x.ModeId == TransactionMode.Debit
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Count();

                        _AnaRewards.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.Type.Value == TransactionTypeValue.Redeem
                              && x.ModeId == TransactionMode.Debit
                              && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                              && (x.Account.AccountTypeId == UserAccountType.Carduser
                                  || x.Account.AccountTypeId == UserAccountType.Appuser))
                               .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Type.Value == TransactionTypeValue.Redeem
                                   || x.Type.Value == TransactionTypeValue.Reward
                                   || x.TypeId == TransactionType.Change
                                  )
                               && (x.Account.AccountTypeId == UserAccountType.Carduser
                                   || x.Account.AccountTypeId == UserAccountType.Appuser
                                  ))
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Payments
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && x.ModeId == TransactionMode.Debit
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                        _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Payments
                               && x.ModeId == TransactionMode.Debit
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Count();

                        _AnaAmountDistribution = new OAnalytics.Distribution();
                        _AnaAmountDistribution.User = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Type.Value == TransactionTypeValue.Reward
                               && x.ModeId == TransactionMode.Credit
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                        _AnaAmountDistribution.ThankU = _HCoreContext.HCUAccountTransaction
                        .Where(x =>
                            x.AccountId == 3
                            && x.Type.Value == TransactionTypeValue.Reward
                            && x.SourceId == TransactionSource.Settlement
                            && x.ModeId == TransactionMode.Credit
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                        _AnaAmountDistribution.POS = _HCoreContext.HCUAccountTransaction
                        .Where(x =>
                            x.Account.AccountTypeId == UserAccountType.PosAccount
                            && x.Type.Value == TransactionTypeValue.Reward
                            && x.SourceId == TransactionSource.Settlement
                            && x.ModeId == TransactionMode.Credit
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                        _AnaAmountDistribution.PG = _HCoreContext.HCUAccountTransaction
                        .Where(x =>
                            x.Account.AccountTypeId == UserAccountType.PgAccount
                            && x.Type.Value == TransactionTypeValue.Reward
                            && x.SourceId == TransactionSource.Settlement
                            && x.ModeId == TransactionMode.Credit
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                        _AnaAmountDistribution.MerchantAcquirer = _HCoreContext.HCUAccountTransaction
                        .Where(x =>
                               x.AccountId == x.Parent.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                            && x.SourceId == TransactionSource.Settlement
                            && x.ModeId == TransactionMode.Credit
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                        //_AnaAmountDistribution.UserAcquirer = _HCoreContext.HCUAccountTransaction
                        //.Where(x =>
                        //   x.UserAccountId != x.Parent.OwnerId
                        //&& x.Type.Value == TransactionTypeValue.Reward
                        //&& x.SourceId == TransactionSource.Settlement
                        //&& x.ModeId == TransactionMode.Credit
                        //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                        //.Sum(x => (double?)x.TotalAmount) ?? 0;


                        _AnaAmountDistribution.UserAcquirer = (from n in _HCoreContext.HCUAccount
                                                               where n.OwnerId != null && n.OwnerId != 3
                                                               select _HCoreContext.HCUAccountTransaction
                        .Where(x =>
                               x.AccountId == n.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                            && x.SourceId == TransactionSource.Settlement
                            && x.ModeId == TransactionMode.Credit
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime)
                            .Sum(x => (double?)x.TotalAmount) ?? 0).FirstOrDefault();

                        _AnaRewards.RewardDistribution = _AnaAmountDistribution;
                        _AnaResponse.Rewards = _AnaRewards;
                        #endregion
                        _AnaInvoices.PaidInvoices = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();
                        _AnaInvoices.UnPaidInvoices = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();
                        _AnaInvoices.PaidInvoiceAmount = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => x.TotalAmount);
                        _AnaInvoices.UnpaidInvoiceAmount = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => x.TotalAmount);
                        double TotalSettlementAmount = _HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                                x.StatusId == StatusHelperId.Transaction_Success)
                                                         .Sum(x => (double?)x.TotalAmount) ?? 0;

                        double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                                x.ModeId == TransactionMode.Debit &&
                                                                x.StatusId == StatusHelperId.Transaction_Success)
                                                         .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _AnaInvoices.UnSettledAmount = (TotalSettlementAmount - TotalDebit);
                        _AnaInvoices.SettledAmount = TotalSettlementAmount;
                        _AnaResponse.Invoices = _AnaInvoices;


                        _AnaUsersCount = new OAnalytics.OUsersCount();
                        _AnaUsersCount.Merchant = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.Store = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.Cashier = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.PSSP = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.PgAccount && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.PSTP = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.PosAccount && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.Terminal = _HCoreContext.TUCTerminal.Where(x => x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();
                        _AnaUsersCount.Acquirer = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.CreateDate > StartTime
                      && x.CreateDate < EndTime).Select(x => x.Id).Count();

                        _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
               .Where(x => x.AccountTypeId == UserAccountType.Appuser
                      && x.CreateDate > StartTime
                      && x.CreateDate < EndTime
                     )
               .Count();

                        _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser;

                        _AnaResponse.UsersCount = _AnaUsersCount;

                        #region Registration Source
                        _AnaRegSource = new OAnalytics.RegSource();
                        _AnaRegSource.App = _HCoreContext.HCUAccount.Where(x =>
                                                                              x.AccountTypeId == UserAccountType.Appuser
                                                                              && x.CreateDate > StartTime
                                                                              && x.CreateDate < EndTime
                                                                              && x.CreatedById == null
                                                                             ).Count();
                        _AnaRegSource.Acquirer = _HCoreContext.HCUAccount.Where(x =>
                                                                              x.AccountTypeId == UserAccountType.Appuser
                                                                                   && x.CreateDate > StartTime
                                                                              && x.CreateDate < EndTime
                                                                                   && x.CreatedBy.AccountTypeId == UserAccountType.Acquirer
                                                                             ).Count();
                        _AnaRegSource.Cashier = _HCoreContext.HCUAccount.Where(x =>
                                                                             x.AccountTypeId == UserAccountType.Appuser
                                                                                  && x.CreateDate > StartTime
                                                                              && x.CreateDate < EndTime
                                                                                  && x.CreatedBy.AccountTypeId == UserAccountType.MerchantCashier
                                                                            ).Count();
                        //_AnaRegSource.Terminals = _HCoreContext.HCUAccount.Where(x =>
                        //                                                    x.AccountTypeId == UserAccountType.Appuser
                        //                                                            && x.CreateDate > StartTime
                        //                                                      && x.CreateDate < EndTime
                        //                                                            && x.CreatedBy.AccountTypeId == UserAccountType.TerminalAccount
                        //                                                   ).Count();
                        _AnaRegSource.PG = _HCoreContext.HCUAccount.Where(x =>
                                                                          x.AccountTypeId == UserAccountType.Appuser
                                                                             && x.CreateDate > StartTime
                                                                              && x.CreateDate < EndTime
                                                                             && x.CreatedBy.AccountTypeId == UserAccountType.PgAccount
                                                                         ).Count();
                        _AnaResponse.RegSource = _AnaRegSource;
                        #endregion

                        #region Visitors
                        _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Select(x => x.AccountId).Count();

                        _AnaVisitors.Single = _HCoreContext.HCUAccount
                            .Where(x =>
                                   (x.AccountTypeId == UserAccountType.Appuser) &&
                                   x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                               && a.TransactionDate > StartTime && a.TransactionDate < EndTime).Count() == 1)
                                .Select(x => x.Id).Count();

                        _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                            .Where(x =>
                                   (x.AccountTypeId == UserAccountType.Appuser) &&
                                   x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                               && a.TransactionDate > StartTime && a.TransactionDate < EndTime).Count() > 1)
                                .Select(x => x.Id).Count();

                        _AnaVisitors.Cashier = _HCoreContext.HCUAccountTransaction
                                           .Where(x =>
                                                   x.CreatedBy.AccountTypeId == UserAccountType.MerchantCashier
                                                  && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                  && (x.Account.AccountTypeId == UserAccountType.Appuser))
                           .Select(x => x.AccountId).Count();

                        _AnaVisitors.PG = _HCoreContext.HCUAccountTransaction
                                           .Where(x =>
                                                  x.CreatedBy.AccountTypeId == UserAccountType.PgAccount
                                                  && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                  && (x.Account.AccountTypeId == UserAccountType.Appuser))
                           .Select(x => x.AccountId).Count();
                        //_AnaVisitors.Terminal = _HCoreContext.HCUAccountTransaction
                        //                 .Where(x =>
                        //                        x.CreatedBy.AccountTypeId == UserAccountType.TerminalAccount
                        //                        && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                        //                        && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                        //                        && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        // .Select(x => x.AccountId).Count();
                        _AnaResponse.Vistitors = _AnaVisitors;
                        #endregion

                        #region Reward Overview
                        _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                           .Select(x => new OAnalytics.OUserTransactionType
                           {
                               Id = x.ReferenceId,
                               Name = x.Name
                           }).ToList();
                        foreach (var RewardType in _AnaResponse.RewardOverview)
                        {
                            RewardType.Amount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.TypeId == RewardType.Id
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && x.ModeId == TransactionMode.Credit
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;
                        }
                        #endregion
                        #region Redeem Overview
                        _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                           .Select(x => new OAnalytics.OUserTransactionType
                           {
                               Id = x.ReferenceId,
                               Name = x.Name
                           }).ToList();
                        foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                        {
                            RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.TypeId == RedeemOverview.Id
                                && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                && x.ModeId == TransactionMode.Debit
                                && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;
                        }
                        #endregion
                        #region Payments Overview
                        _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                           .Select(x => new OAnalytics.OUserTransactionType
                           {
                               Id = x.ReferenceId,
                               Name = x.Name
                           }).ToList();
                        foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                        {
                            PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                && x.TypeId == PaymentOverview.Id
                                && x.ModeId == TransactionMode.Debit
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                .Sum(x => (double?)x.TotalAmount) ?? 0;
                        }
                        #endregion
                        #region Top Users

                        _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                                    where
                                                     (n.AccountTypeId == UserAccountType.Appuser)
                                                    && (_HCoreContext.HCUAccountTransaction.
                                                    Where(x => x.AccountId == n.Id
                                                          && x.TransactionDate > StartTime
                                                          && x.TransactionDate < EndTime
                                                          && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                          )
                                                    .Count() > 0)
                                                    orderby _HCoreContext.HCUAccountTransaction.
                                                    Where(x => x.AccountId == n.Id
                                                          && x.TransactionDate > StartTime
                                                          && x.TransactionDate < EndTime
                                                          && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                          )
                                                    .Count() descending
                                                    select new OAnalytics.OUsers
                                                    {
                                                        ReferenceKey = n.Guid,
                                                        DisplayName = n.DisplayName,
                                                        ContactNumber = n.ContactNumber,
                                                        CreateDate = n.CreateDate,
                                                        Count = _HCoreContext.HCUAccountTransaction.
                                                                             Where(x => x.AccountId == n.Id
                                                          && x.TransactionDate > StartTime
                                                          && x.TransactionDate < EndTime
                                                          && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                          )
                                                    .Count()
                                                    }
                                                  ).Skip(0).Take(5).ToList();

                        _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                                   where
                                                    (n.AccountTypeId == UserAccountType.Appuser)
                                                   && (_HCoreContext.HCUAccountTransaction.
                                                                             Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Reward
                                                        )
                                                   .Sum(x => x.TotalAmount) > 0)
                                                   orderby _HCoreContext.HCUAccountTransaction.
                                                                             Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Reward
                                                         )
                                                   .Sum(x => x.TotalAmount) descending
                                                   select new OAnalytics.OUsers
                                                   {
                                                       ReferenceKey = n.Guid,
                                                       DisplayName = n.DisplayName,
                                                       ContactNumber = n.ContactNumber,
                                                       CreateDate = n.CreateDate,
                                                       Amount = _HCoreContext.HCUAccountTransaction.
                                                                             Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Reward
                                                         )
                                                   .Sum(x => x.TotalAmount)
                                                   }
                                                  ).Skip(0).Take(5).ToList();

                        _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                                   where
                                                    (n.AccountTypeId == UserAccountType.Appuser)
                                                   && (_HCoreContext.HCUAccountTransaction.
                                                   Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Redeem
                                                        )
                                                   .Sum(x => x.TotalAmount) > 0)
                                                   orderby _HCoreContext.HCUAccountTransaction.
                                                   Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Redeem
                                                         )
                                                   .Sum(x => x.TotalAmount) descending
                                                   select new OAnalytics.OUsers
                                                   {
                                                       ReferenceKey = n.Guid,
                                                       DisplayName = n.DisplayName,
                                                       ContactNumber = n.ContactNumber,
                                                       CreateDate = n.CreateDate,
                                                       Amount = _HCoreContext.HCUAccountTransaction.
                                                                             Where(x => x.AccountId == n.Id
                                                         && x.TransactionDate > StartTime
                                                         && x.TransactionDate < EndTime
                                                         && x.Type.Value == TransactionTypeValue.Redeem
                                                         )
                                                   .Sum(x => x.TotalAmount)
                                                   }
                                                 ).Skip(0).Take(5).ToList();
                        #endregion

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnaResponse, "HC0001");
                        #endregion
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="UserAccount">The user account.</param>
        /// <param name="TransactionTypes">The transaction types.</param>
        /// <param name="StartTime">The start time.</param>
        /// <param name="EndTime">The end time.</param>
        /// <returns>OAnalytics.Response.</returns>
        private OAnalytics.Response GetMerchantOverview(OAnalytics.Request _Request, OAnalytics.Account UserAccount, List<OAnalytics.OTransactionType> TransactionTypes, DateTime StartTime, DateTime EndTime)
        {
            #region Declare

            _AnaResponse = new OAnalytics.Response();
            _AnaUsersCount = new OAnalytics.OUsersCount();
            _AnaRewards = new OAnalytics.Rewards();
            _AnaVisitors = new OAnalytics.Visitors();
            _AnaInvoices = new OAnalytics.Invoices();
            _AnaRatings = new OAnalytics.Ratings();
            _AnaRewardBalance = new OAnalytics.RewardBalance();
            #endregion
            if (!string.IsNullOrEmpty(_Request.OwnerKey))
            {
                var OwnerDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.Guid == _Request.OwnerKey)
                                                .Select(x => new
                                                {
                                                    OwnerId = x.Id,
                                                    AccountTypeId = x.AccountTypeId
                                                }).FirstOrDefault();
                if (OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                {

                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedById == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Visitors
                    _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.ParentId == UserAccount.UserAccountId
                                               && x.CreatedById == OwnerDetails.OwnerId
                                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Select(x => x.AccountId).Count();

                    _AnaVisitors.Single = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                           && a.CreatedById == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                               a.ParentId == UserAccount.UserAccountId).Count() == 1)
                            .Select(x => x.Id).Count();

                    _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                           && a.CreatedById == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                               a.ParentId == UserAccount.UserAccountId).Count() > 1)
                            .Select(x => x.Id).Count();
                    _AnaResponse.Vistitors = _AnaVisitors;
                    #endregion

                    #region Users Count

                    _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                                 && (x.CreatedById == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Carduser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                                 && (x.CreatedById == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;

                    _AnaResponse.UsersCount = _AnaUsersCount;
                    #endregion


                    _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                      .Where(x =>
                             x.ParentId != UserAccount.UserAccountId
                             && x.AccountId == UserAccount.UserAccountId
                             && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                             && x.CreatedById == OwnerDetails.OwnerId

                              && x.SourceId == TransactionSource.Settlement
                             && x.ModeId == TransactionMode.Credit)
                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.TypeId == RewardType.Id
                                   && x.CreatedById == OwnerDetails.OwnerId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TypeId == RedeemOverview.Id
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.CreatedById == OwnerDetails.OwnerId
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.CreatedById == OwnerDetails.OwnerId
                            && x.TypeId == PaymentOverview.Id
                            && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion

                    #region Top Users

                    _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                                where
                                                 (n.AccountTypeId == UserAccountType.Appuser)
                                                && (_HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.CreatedById == OwnerDetails.OwnerId

                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count() > 0)
                                                orderby _HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.CreatedById == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count() descending
                                                select new OAnalytics.OUsers
                                                {
                                                    ReferenceKey = n.Guid,
                                                    UserKey = n.Guid,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    CreateDate = n.CreateDate,
                                                    Count = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count()
                                                }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.TransactionDate > StartTime
                                                     && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedById == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                             ).Skip(0).Take(5).ToList();
                    #endregion
                }
                else if (OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                {
                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Visitors
                    _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.ParentId == UserAccount.UserAccountId
                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Select(x => x.AccountId).Count();

                    _AnaVisitors.Single = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                           && a.CreatedById == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                               a.ParentId == UserAccount.UserAccountId).Count() == 1)
                            .Select(x => x.Id).Count();

                    _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                           && a.CreatedById == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                               a.ParentId == UserAccount.UserAccountId).Count() > 1)
                            .Select(x => x.Id).Count();
                    _AnaResponse.Vistitors = _AnaVisitors;
                    #endregion

                    #region Users Count

                    _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                               && (x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Carduser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                               && (x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;

                    _AnaResponse.UsersCount = _AnaUsersCount;
                    #endregion


                    _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                      .Where(x =>
                             x.ParentId != UserAccount.UserAccountId
                             && x.AccountId == UserAccount.UserAccountId
                             && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                             && x.CreatedBy.OwnerId == OwnerDetails.OwnerId

                              && x.SourceId == TransactionSource.Settlement
                             && x.ModeId == TransactionMode.Credit)
                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.TypeId == RewardType.Id
                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TypeId == RedeemOverview.Id
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                            && x.TypeId == PaymentOverview.Id
                            && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion

                    #region Top Users

                    _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                                where
                                                 (n.AccountTypeId == UserAccountType.Appuser)
                                                && (_HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.CreatedBy.OwnerId == OwnerDetails.OwnerId

                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count() > 0)
                                                orderby _HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count() descending
                                                select new OAnalytics.OUsers
                                                {
                                                    ReferenceKey = n.Guid,
                                                    DisplayName = n.DisplayName,
                                                    UserKey = n.Guid,
                                                    ContactNumber = n.ContactNumber,
                                                    CreateDate = n.CreateDate,
                                                    Count = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.ParentId == UserAccount.UserAccountId)
                                                .Count()
                                                }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId

                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   UserKey = n.Guid,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.TransactionDate > StartTime
                                                     && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   UserKey = n.Guid,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.ParentId == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                             ).Skip(0).Take(5).ToList();
                    #endregion
                }
                else if (OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                {
                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.Type.Value == TransactionTypeValue.Payments
                           && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Visitors
                    _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.ParentId == UserAccount.UserAccountId
                                               && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Select(x => x.AccountId).Count();

                    _AnaVisitors.Single = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(m =>
                                                       m.AccountId == OwnerDetails.OwnerId
                                                       && m.GroupKey == a.GroupKey
                                                       && m.ModeId == TransactionMode.Credit
                                                       && m.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                               a.ParentId == UserAccount.UserAccountId

                                                                          ).Count() == 1)
                            .Select(x => x.Id).Count();

                    _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                             && (_HCoreContext.HCUAccountTransaction
                                                .Where(m =>
                                                       m.AccountId == OwnerDetails.OwnerId
                                                       && m.GroupKey == a.GroupKey
                                                       && m.ModeId == TransactionMode.Credit
                                                       && m.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                               a.ParentId == UserAccount.UserAccountId).Count() > 1)
                            .Select(x => x.Id).Count();
                    _AnaResponse.Vistitors = _AnaVisitors;
                    #endregion

                    #region Users Count

                    _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                               && (x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Carduser
                               && x.CreateDate > StartTime
                               && x.CreateDate < EndTime
                               && x.OwnerId == OwnerDetails.OwnerId
                               && (x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.OwnerId == UserAccount.UserAccountId
                                   || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                        .Count();
                    _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;

                    _AnaResponse.UsersCount = _AnaUsersCount;
                    #endregion


                    _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                      .Where(x =>
                             x.ParentId == UserAccount.UserAccountId
                             && x.AccountId == OwnerDetails.OwnerId
                             && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                             && x.CreatedBy.OwnerId == OwnerDetails.OwnerId

                              && x.SourceId == TransactionSource.Settlement
                             && x.ModeId == TransactionMode.Credit)
                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                           && x.TypeId == RewardType.Id
                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TypeId == RedeemOverview.Id
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId
                            && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                            && x.TypeId == PaymentOverview.Id
                            && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion

                }
            }
            else
            {
                #region Reward balance
                _AnaRewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.Account.Id == UserAccount.UserAccountId &&
                           x.ModeId == TransactionMode.Credit &&
                           x.TypeId == TransactionType.MerchantCredit &&
                           x.SourceId == TransactionSource.Merchant
                                 ).Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.ParentId == UserAccount.UserAccountId &&
                                  x.ModeId == TransactionMode.Debit &&
                                  x.SourceId == TransactionSource.Cashier
                                 ).Sum(x => (double?)x.TotalAmount) ?? 0;
                _AnaRewardBalance.Balance = _AnaRewardBalance.Credit - _AnaRewardBalance.Debit;
                _AnaResponse.RewardBalance = _AnaRewardBalance;
                #endregion
                #region Rewards
                _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Reward
                       && x.ModeId == TransactionMode.Credit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Reward
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                       && x.ModeId == TransactionMode.Credit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Redeem
                       && x.ModeId == TransactionMode.Debit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Redeem
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                       && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Payments
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Payments
                       && x.ModeId == TransactionMode.Debit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaResponse.Rewards = _AnaRewards;
                #endregion
                #region Visitors
                _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.ParentId == UserAccount.UserAccountId
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                           && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Select(x => x.AccountId).Count();

                _AnaVisitors.Single = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                       && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                           a.ParentId == UserAccount.UserAccountId).Count() == 1)
                        .Select(x => x.Id).Count();

                _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                       && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                           a.ParentId == UserAccount.UserAccountId).Count() > 1)
                        .Select(x => x.Id).Count();
                _AnaResponse.Vistitors = _AnaVisitors;
                #endregion

                #region Users Count
                _AnaUsersCount.Cashier = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.MerchantCashier).Select(x => x.Id).Count();
                _AnaUsersCount.Store = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId).Select(x => x.Id).Count();
                _AnaUsersCount.PSSP = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PgAccount).Select(x => x.Id).Count();
                _AnaUsersCount.PSTP = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).Count();
                _AnaUsersCount.Terminal = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == UserAccount.UserAccountId).Select(x => x.Id).Count();
                _AnaUsersCount.AcquirerKey = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.UserAccountId).Select(x => x.Owner.Guid).FirstOrDefault();
                _AnaUsersCount.AcquirerDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.UserAccountId).Select(x => x.Owner.DisplayName).FirstOrDefault();

                _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                           && x.CreateDate > StartTime
                           && x.CreateDate < EndTime
                           && (x.OwnerId == UserAccount.UserAccountId
                               || x.CreatedById == UserAccount.UserAccountId
                               || x.Owner.OwnerId == UserAccount.UserAccountId
                               || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                    .Count();
                _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Carduser
                           && x.CreateDate > StartTime
                           && x.CreateDate < EndTime
                           && (x.OwnerId == UserAccount.UserAccountId ||
                               x.CreatedById == UserAccount.UserAccountId ||
                               x.Owner.OwnerId == UserAccount.UserAccountId ||
                               x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                    .Count();
                _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;

                _AnaResponse.UsersCount = _AnaUsersCount;
                #endregion


                _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                  .Where(x =>
                         x.ParentId != UserAccount.UserAccountId
                         && x.AccountId == UserAccount.UserAccountId
                         && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                          && x.SourceId == TransactionSource.Settlement
                         && x.ModeId == TransactionMode.Credit)
                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                #region Reward Overview
                _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var RewardType in _AnaResponse.RewardOverview)
                {
                    RewardType.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.ParentId == UserAccount.UserAccountId
                       && x.TypeId == RewardType.Id
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Credit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
                #region Redeem Overview
                _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                {
                    RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.TypeId == RedeemOverview.Id
                        && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                        && x.ModeId == TransactionMode.Debit
                        && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
                #region Payments Overview
                _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                {
                    PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.ParentId == UserAccount.UserAccountId
                        && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                        && x.TypeId == PaymentOverview.Id
                        && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion

                #region Top Users

                _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                            where
                                             (n.AccountTypeId == UserAccountType.Appuser)
                                            && (_HCoreContext.HCUAccountTransaction.
                                            Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.ParentId == UserAccount.UserAccountId)
                                            .Count() > 0)
                                            orderby _HCoreContext.HCUAccountTransaction.
                                            Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.ParentId == UserAccount.UserAccountId)
                                            .Count() descending
                                            select new OAnalytics.OUsers
                                            {
                                                ReferenceKey = n.Guid,
                                                DisplayName = n.DisplayName,
                                                UserKey = n.Guid,
                                                ContactNumber = n.ContactNumber,
                                                CreateDate = n.CreateDate,
                                                Count = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.ParentId == UserAccount.UserAccountId)
                                            .Count()
                                            }
                                          ).Skip(0).Take(5).ToList();

                _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                           where
                                            (n.AccountTypeId == UserAccountType.Appuser)
                                           && (_HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) > 0)
                                           orderby _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) descending
                                           select new OAnalytics.OUsers
                                           {
                                               ReferenceKey = n.Guid,
                                               UserKey = n.Guid,
                                               DisplayName = n.DisplayName,
                                               ContactNumber = n.ContactNumber,
                                               CreateDate = n.CreateDate,
                                               Amount = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount)
                                           }
                                          ).Skip(0).Take(5).ToList();

                _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                           where
                                            (n.AccountTypeId == UserAccountType.Appuser)
                                           && (_HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) > 0)
                                           orderby _HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) descending
                                           select new OAnalytics.OUsers
                                           {
                                               ReferenceKey = n.Guid,
                                               UserKey = n.Guid,
                                               DisplayName = n.DisplayName,
                                               ContactNumber = n.ContactNumber,
                                               CreateDate = n.CreateDate,
                                               Amount = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.ParentId == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount)
                                           }
                                         ).Skip(0).Take(5).ToList();
                #endregion


                #region Ratings
                //_AnaRatings.Average = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId).Select(x => x.AverageValue).Sum() ?? 0;
                _AnaRatings.Total = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206).Select(x => x.Id).Count();
                _AnaRatings.R1 = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "1").Select(x => x.Id).Count();
                _AnaRatings.R2 = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "2").Select(x => x.Id).Count();
                _AnaRatings.R3 = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "3").Select(x => x.Id).Count();
                _AnaRatings.R4 = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "4").Select(x => x.Id).Count();
                _AnaRatings.R5 = _HCoreContext.HCUAccountParameter.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "5").Select(x => x.Id).Count();
                _AnaResponse.Ratings = _AnaRatings;
                #endregion
            }


            return _AnaResponse;
        }
        /// <summary>
        /// Description: Gets the owner overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="UserAccount">The user account.</param>
        /// <param name="TransactionTypes">The transaction types.</param>
        /// <param name="StartTime">The start time.</param>
        /// <param name="EndTime">The end time.</param>
        /// <returns>OAnalytics.Response.</returns>
        private OAnalytics.Response GetOwnerOverview(OAnalytics.Request _Request, OAnalytics.Account UserAccount, List<OAnalytics.OTransactionType> TransactionTypes, DateTime StartTime, DateTime EndTime)
        {
            #region Declare

            _AnaResponse = new OAnalytics.Response();
            _AnaUsersCount = new OAnalytics.OUsersCount();
            _AnaRewards = new OAnalytics.Rewards();
            _AnaVisitors = new OAnalytics.Visitors();
            _AnaInvoices = new OAnalytics.Invoices();
            _AnaRewardBalance = new OAnalytics.RewardBalance();
            #endregion
            if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
            {
                #region Reward balance
                _AnaRewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.Account.Id == UserAccount.UserAccountId &&
                           x.ModeId == TransactionMode.Credit &&
                           x.TypeId == TransactionType.StoreCredit &&
                           x.SourceId == TransactionSource.Store
                                 ).Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId &&
                                  x.ModeId == TransactionMode.Debit &&
                                  x.SourceId == TransactionSource.Cashier
                                 ).Sum(x => (double?)x.TotalAmount) ?? 0;
                _AnaRewardBalance.Balance = _AnaRewardBalance.Credit - _AnaRewardBalance.Debit;
                _AnaResponse.RewardBalance = _AnaRewardBalance;
                #endregion
            }
            else
            {
                _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
             .Where(x =>
                    x.ParentId != UserAccount.UserAccountId
                    && x.AccountId == UserAccount.UserAccountId
                    && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                     && x.SourceId == TransactionSource.Settlement
                    && x.ModeId == TransactionMode.Credit)
                     .Sum(x => (double?)x.TotalAmount) ?? 0;
            }
            #region User Counts
            _AnaUsersCount.Cashier = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.MerchantCashier).Select(x => x.Id).Count();
            _AnaUsersCount.Merchant = _HCoreContext.HCUAccountOwner.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.Account.AccountTypeId == UserAccountType.Merchant && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Id).Count();
            _AnaUsersCount.Terminal = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == UserAccount.UserAccountId && x.StatusId == StatusActive).Select(x => x.Id).Count();

            _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
               .Where(x => x.AccountTypeId == UserAccountType.Appuser
                      && x.CreateDate > StartTime
                      && x.CreateDate < EndTime
                      && (x.OwnerId == UserAccount.UserAccountId
                          || x.CreatedById == UserAccount.UserAccountId
                          || x.Owner.OwnerId == UserAccount.UserAccountId
                          || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                .Count();
            _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                .Where(x => x.AccountTypeId == UserAccountType.Carduser
                       && x.CreateDate > StartTime
                       && x.CreateDate < EndTime
                       && (x.OwnerId == UserAccount.UserAccountId ||
                           x.CreatedById == UserAccount.UserAccountId ||
                           x.Owner.OwnerId == UserAccount.UserAccountId ||
                           x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                .Count();
            _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
            _AnaResponse.UsersCount = _AnaUsersCount;
            #endregion

            #region Rewards
            _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
            .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Reward
                   && x.ModeId == TransactionMode.Credit
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;

            _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Reward
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && x.ModeId == TransactionMode.Credit
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Count();

            _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Redeem
                   && x.ModeId == TransactionMode.Debit
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;
            _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Redeem
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                   && x.ModeId == TransactionMode.Debit
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Count();

            _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;

            _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Payments
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && x.ModeId == TransactionMode.Debit
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;

            _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.Type.Value == TransactionTypeValue.Payments
                   && x.ModeId == TransactionMode.Debit
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Count();

            _AnaResponse.Rewards = _AnaRewards;
            #endregion
            #region Visitors
            _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                .Select(x => x.AccountId).Count();

            _AnaVisitors.Single = _HCoreContext.HCUAccount
                .Where(x =>
                       (x.AccountTypeId == UserAccountType.Appuser) &&
                       x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                   && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                   a.CreatedBy.OwnerId == UserAccount.UserAccountId).Count() == 1)
                    .Select(x => x.Id).Count();

            _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                .Where(x =>
                       (x.AccountTypeId == UserAccountType.Appuser) &&
                       x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                   && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                                                                   a.CreatedBy.OwnerId == UserAccount.UserAccountId).Count() > 1)
                    .Select(x => x.Id).Count();
            _AnaResponse.Vistitors = _AnaVisitors;
            #endregion


            #region Reward Overview
            _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
               .Select(x => new OAnalytics.OUserTransactionType
               {
                   Id = x.ReferenceId,
                   Name = x.Name
               }).ToList();
            foreach (var RewardType in _AnaResponse.RewardOverview)
            {
                RewardType.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                   && x.TypeId == RewardType.Id
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && x.ModeId == TransactionMode.Credit
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;
            }
            #endregion
            #region Redeem Overview
            _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
               .Select(x => new OAnalytics.OUserTransactionType
               {
                   Id = x.ReferenceId,
                   Name = x.Name
               }).ToList();
            foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
            {
                RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.TypeId == RedeemOverview.Id
                    && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                    && x.ModeId == TransactionMode.Debit
                    && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;
            }
            #endregion
            #region Payments Overview
            _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
               .Select(x => new OAnalytics.OUserTransactionType
               {
                   Id = x.ReferenceId,
                   Name = x.Name
               }).ToList();
            foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
            {
                PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                    && x.TypeId == PaymentOverview.Id
                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                   && x.ModeId == TransactionMode.Debit
                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Sum(x => (double?)x.TotalAmount) ?? 0;
            }
            #endregion
            _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                        where
                                         (n.AccountTypeId == UserAccountType.Appuser)
                                        && (_HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == n.Id
                                              && x.TransactionDate > StartTime
                                              && x.TransactionDate < EndTime
                                              && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                              && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                        .Count() > 0)
                                        orderby _HCoreContext.HCUAccountTransaction.
                                        Where(x => x.AccountId == n.Id
                                              && x.TransactionDate > StartTime
                                              && x.TransactionDate < EndTime
                                              && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                              && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                        .Count() descending
                                        select new OAnalytics.OUsers
                                        {
                                            ReferenceKey = n.Guid,
                                            UserKey = n.Guid,
                                            DisplayName = n.DisplayName,
                                            ContactNumber = n.ContactNumber,
                                            CreateDate = n.CreateDate,
                                            Count = _HCoreContext.HCUAccountTransaction.
                                                                 Where(x => x.AccountId == n.Id
                                              && x.TransactionDate > StartTime
                                              && x.TransactionDate < EndTime
                                              && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                              && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                        .Count()
                                        }
                                    ).Skip(0).Take(5).ToList();

            _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                       where
                                        (n.AccountTypeId == UserAccountType.Appuser)
                                       && (_HCoreContext.HCUAccountTransaction.
                                       Where(x => x.AccountId == n.Id
                                             && x.TransactionDate > StartTime
                                             && x.TransactionDate < EndTime
                                             && x.Type.Value == TransactionTypeValue.Reward
                                             && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                       .Sum(x => x.TotalAmount) > 0)
                                       orderby _HCoreContext.HCUAccountTransaction.
                                       Where(x => x.AccountId == n.Id
                                             && x.TransactionDate > StartTime
                                             && x.TransactionDate < EndTime
                                             && x.Type.Value == TransactionTypeValue.Reward
                                             && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                       .Sum(x => x.TotalAmount) descending
                                       select new OAnalytics.OUsers
                                       {
                                           ReferenceKey = n.Guid,
                                           UserKey = n.Guid,
                                           DisplayName = n.DisplayName,
                                           ContactNumber = n.ContactNumber,
                                           CreateDate = n.CreateDate,
                                           Amount = _HCoreContext.HCUAccountTransaction.
                                                                 Where(x => x.AccountId == n.Id
                                             && x.TransactionDate > StartTime
                                             && x.TransactionDate < EndTime
                                             && x.Type.Value == TransactionTypeValue.Reward
                                             && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                       .Sum(x => x.TotalAmount)
                                       }
                                      ).Skip(0).Take(5).ToList();

            _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                       where
                                        (n.AccountTypeId == UserAccountType.Appuser)
                                       && (_HCoreContext.HCUAccountTransaction.
                                       Where(x => x.AccountId == n.Id
                                             && x.TransactionDate > StartTime
                                             && x.TransactionDate < EndTime
                                             && x.Type.Value == TransactionTypeValue.Redeem
                                             && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                       .Sum(x => x.TotalAmount) > 0)
                                       orderby _HCoreContext.HCUAccountTransaction.
                                      Where(x => x.AccountId == n.Id
                                            && x.TransactionDate > StartTime
                                            && x.TransactionDate < EndTime
                                            && x.Type.Value == TransactionTypeValue.Redeem
                                            && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                      .Sum(x => x.TotalAmount) descending
                                       select new OAnalytics.OUsers
                                       {
                                           ReferenceKey = n.Guid,
                                           DisplayName = n.DisplayName,
                                           UserKey = n.Guid,
                                           ContactNumber = n.ContactNumber,
                                           CreateDate = n.CreateDate,
                                           Amount = _HCoreContext.HCUAccountTransaction.
                                                                 Where(x => x.AccountId == n.Id
                                             && x.TransactionDate > StartTime
                                             && x.TransactionDate < EndTime
                                             && x.Type.Value == TransactionTypeValue.Redeem
                                             && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                       .Sum(x => x.TotalAmount)
                                       }
                                     ).Skip(0).Take(5).ToList();

            #region Ratings
            _AnaRatings = new OAnalytics.Ratings();
            _AnaRatings.Average = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.UserAccountId).Select(x => x.AverageValue).FirstOrDefault();
            _AnaRatings.Total = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206).Select(x => x.Id).Count();
            _AnaRatings.R1 = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "1").Select(x => x.Id).Count();
            _AnaRatings.R2 = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "2").Select(x => x.Id).Count();
            _AnaRatings.R3 = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "3").Select(x => x.Id).Count();
            _AnaRatings.R4 = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "4").Select(x => x.Id).Count();
            _AnaRatings.R5 = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == UserAccount.UserAccountId && x.TypeId == 206 && x.Value == "5").Select(x => x.Id).Count();
            _AnaResponse.Ratings = _AnaRatings;
            #endregion
            return _AnaResponse;
        }
        /// <summary>
        /// Description: Gets the created by overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="UserAccount">The user account.</param>
        /// <param name="TransactionTypes">The transaction types.</param>
        /// <param name="StartTime">The start time.</param>
        /// <param name="EndTime">The end time.</param>
        /// <returns>OAnalytics.Response.</returns>
        private OAnalytics.Response GetCreatedByOverview(OAnalytics.Request _Request, OAnalytics.Account UserAccount, List<OAnalytics.OTransactionType> TransactionTypes, DateTime StartTime, DateTime EndTime)
        {
            #region Declare

            _AnaResponse = new OAnalytics.Response();
            _AnaUsersCount = new OAnalytics.OUsersCount();
            _AnaRewards = new OAnalytics.Rewards();
            _AnaVisitors = new OAnalytics.Visitors();
            _AnaInvoices = new OAnalytics.Invoices();
            _AnaRewardBalance = new OAnalytics.RewardBalance();
            #endregion
            if (!string.IsNullOrEmpty(_Request.OwnerKey))
            {
                var OwnerDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.Guid == _Request.OwnerKey)
                                                .Select(x => new
                                                {
                                                    OwnerId = x.Id,
                                                    AccountTypeId = x.AccountTypeId
                                                }).FirstOrDefault();
                _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                            .Where(x =>
                                   x.ParentId == OwnerDetails.OwnerId
                                   && x.AccountId == UserAccount.UserAccountId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.SourceId == TransactionSource.Settlement
                                   && x.ModeId == TransactionMode.Credit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                if (OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                {
                    #region User Counts
                    _AnaUsersCount.Merchant = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).Count();

                    _AnaUsersCount.MerchantKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.MerchantDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                    _AnaUsersCount.PSSPKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.PSSPDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                    _AnaUsersCount.AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.AcquirerDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();
                    if (OwnerDetails != null)
                    {
                        _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                  .Where(x => x.AccountTypeId == UserAccountType.Appuser
                         && x.CreateDate > StartTime
                         && x.CreateDate < EndTime
                         && x.OwnerId == OwnerDetails.OwnerId
                          && (x.CreatedById == UserAccount.UserAccountId
                             || x.Owner.OwnerId == UserAccount.UserAccountId
                             || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                      .Count();
                        _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Carduser
                                   && x.CreateDate > StartTime
                                   && x.CreateDate < EndTime
                                   && x.OwnerId == UserAccount.UserAccountId
                                  && (x.CreatedById == UserAccount.UserAccountId ||
                                       x.Owner.OwnerId == UserAccount.UserAccountId ||
                                       x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                            .Count();
                        _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
                        _AnaResponse.UsersCount = _AnaUsersCount;
                    }
                    else
                    {
                        _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                  .Where(x => x.AccountTypeId == UserAccountType.Appuser
                         && x.CreateDate > StartTime
                         && x.CreateDate < EndTime
                         && (x.OwnerId == UserAccount.UserAccountId
                             || x.CreatedById == UserAccount.UserAccountId
                             || x.Owner.OwnerId == UserAccount.UserAccountId
                             || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                      .Count();
                        _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Carduser
                                   && x.CreateDate > StartTime
                                   && x.CreateDate < EndTime
                                   && (x.OwnerId == UserAccount.UserAccountId ||
                                       x.CreatedById == UserAccount.UserAccountId ||
                                       x.Owner.OwnerId == UserAccount.UserAccountId ||
                                       x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                            .Count();
                        _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
                        _AnaResponse.UsersCount = _AnaUsersCount;
                    }
                    #endregion
                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId

                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))

                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId

                               && x.Type.Value == TransactionTypeValue.Reward
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Redeem

                               && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Debit
                            && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                            && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Payments

                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit

                                && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0) && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Payments

                               && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Visitors
                    _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedById == UserAccount.UserAccountId

                                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                                && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Select(x => x.AccountId).Count();

                    _AnaVisitors.Single = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                            (_HCoreContext.HCUAccountTransaction
                                                .Where(m =>
                                                       m.AccountId == OwnerDetails.OwnerId
                                                       && m.GroupKey == a.GroupKey
                                                       && m.ModeId == TransactionMode.Credit
                                                       && m.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0) &&
                                                                           a.CreatedById == UserAccount.UserAccountId).Count() == 1)
                            .Select(x => x.Id).Count();

                    _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                            (_HCoreContext.HCUAccountTransaction
                                                .Where(m =>
                                                       m.AccountId == OwnerDetails.OwnerId
                                                       && m.GroupKey == a.GroupKey
                                                       && m.ModeId == TransactionMode.Credit
                                                       && m.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0) &&
                                                                           a.CreatedById == UserAccount.UserAccountId).Count() > 1)
                            .Select(x => x.Id).Count();
                    _AnaResponse.Vistitors = _AnaVisitors;
                    #endregion
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                           && x.TypeId == RewardType.Id
                                    && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.TypeId == RedeemOverview.Id
                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.TypeId == PaymentOverview.Id
                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Top Users
                    _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                                where
                                                 (n.AccountTypeId == UserAccountType.Appuser)
                                                && (_HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id

                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count() > 0)
                                                orderby _HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count() descending
                                                select new OAnalytics.OUsers
                                                {
                                                    ReferenceKey = n.Guid,
                                                    UserKey = n.Guid,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    CreateDate = n.CreateDate,
                                                    Count = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count()
                                                }
                                            ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                     && x.TransactionDate > StartTime
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                             ).Skip(0).Take(5).ToList();
                    #endregion

                }
                else
                {
                    #region User Counts
                    _AnaUsersCount.Merchant = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).Count();

                    _AnaUsersCount.MerchantKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.MerchantDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                    _AnaUsersCount.PSSPKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.PSSPDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                    _AnaUsersCount.AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                    _AnaUsersCount.AcquirerDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();
                    if (OwnerDetails != null)
                    {
                        _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                  .Where(x => x.AccountTypeId == UserAccountType.Appuser
                         && x.CreateDate > StartTime
                         && x.CreateDate < EndTime
                         && x.OwnerId == OwnerDetails.OwnerId
                          && (x.CreatedById == UserAccount.UserAccountId
                             || x.Owner.OwnerId == UserAccount.UserAccountId
                             || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                      .Count();
                        _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Carduser
                                   && x.CreateDate > StartTime
                                   && x.CreateDate < EndTime
                                   && x.OwnerId == UserAccount.UserAccountId
                                  && (x.CreatedById == UserAccount.UserAccountId ||
                                       x.Owner.OwnerId == UserAccount.UserAccountId ||
                                       x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                            .Count();
                        _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
                        _AnaResponse.UsersCount = _AnaUsersCount;
                    }
                    else
                    {
                        _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                  .Where(x => x.AccountTypeId == UserAccountType.Appuser
                         && x.CreateDate > StartTime
                         && x.CreateDate < EndTime
                         && (x.OwnerId == UserAccount.UserAccountId
                             || x.CreatedById == UserAccount.UserAccountId
                             || x.Owner.OwnerId == UserAccount.UserAccountId
                             || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                      .Count();
                        _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Carduser
                                   && x.CreateDate > StartTime
                                   && x.CreateDate < EndTime
                                   && (x.OwnerId == UserAccount.UserAccountId ||
                                       x.CreatedById == UserAccount.UserAccountId ||
                                       x.Owner.OwnerId == UserAccount.UserAccountId ||
                                       x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                            .Count();
                        _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
                        _AnaResponse.UsersCount = _AnaUsersCount;
                    }
                    #endregion
                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                           && x.ParentId == OwnerDetails.OwnerId
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                               && x.ParentId == OwnerDetails.OwnerId
                               && x.Type.Value == TransactionTypeValue.Reward
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Redeem
                               && x.ParentId == OwnerDetails.OwnerId
                               && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Redeem
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && x.ParentId == OwnerDetails.OwnerId
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && x.ParentId == OwnerDetails.OwnerId
                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Payments
                               && x.ParentId == OwnerDetails.OwnerId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.Type.Value == TransactionTypeValue.Payments
                               && x.ParentId == OwnerDetails.OwnerId
                               && x.ModeId == TransactionMode.Debit
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Count();

                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Visitors
                    _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                                               && x.ParentId == OwnerDetails.OwnerId
                                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Select(x => x.AccountId).Count();

                    _AnaVisitors.Single = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                           && a.ParentId == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                           a.CreatedById == UserAccount.UserAccountId).Count() == 1)
                            .Select(x => x.Id).Count();

                    _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                        .Where(x =>
                               (x.AccountTypeId == UserAccountType.Appuser) &&
                               x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                           && a.ParentId == OwnerDetails.OwnerId
                                                                           && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                           a.CreatedById == UserAccount.UserAccountId).Count() > 1)
                            .Select(x => x.Id).Count();
                    _AnaResponse.Vistitors = _AnaVisitors;
                    #endregion
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                           && x.TypeId == RewardType.Id
                                   && x.ParentId == OwnerDetails.OwnerId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.TypeId == RedeemOverview.Id
                                   && x.ParentId == OwnerDetails.OwnerId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                            && x.TypeId == PaymentOverview.Id
                                   && x.ParentId == OwnerDetails.OwnerId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Top Users
                    _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                                where
                                                 (n.AccountTypeId == UserAccountType.Appuser)
                                                && (_HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id
                                                      && x.ParentId == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count() > 0)
                                                orderby _HCoreContext.HCUAccountTransaction.
                                                Where(x => x.AccountId == n.Id

                                                      && x.ParentId == OwnerDetails.OwnerId
                                                      && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count() descending
                                                select new OAnalytics.OUsers
                                                {
                                                    ReferenceKey = n.Guid,
                                                    UserKey = n.Guid,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    CreateDate = n.CreateDate,
                                                    Count = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate > StartTime
                                                      && x.TransactionDate < EndTime
                                                      && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                      && x.CreatedById == UserAccount.UserAccountId)
                                                .Count()
                                                }
                                            ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Reward
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                              ).Skip(0).Take(5).ToList();

                    _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                               where
                                                (n.AccountTypeId == UserAccountType.Appuser)
                                               && (_HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) > 0)
                                               orderby _HCoreContext.HCUAccountTransaction.
                                               Where(x => x.AccountId == n.Id
                                                     && x.ParentId == OwnerDetails.OwnerId
                                                     && x.TransactionDate > StartTime
                                                     && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount) descending
                                               select new OAnalytics.OUsers
                                               {
                                                   ReferenceKey = n.Guid,
                                                   UserKey = n.Guid,
                                                   DisplayName = n.DisplayName,
                                                   ContactNumber = n.ContactNumber,
                                                   CreateDate = n.CreateDate,
                                                   Amount = _HCoreContext.HCUAccountTransaction.
                                                                         Where(x => x.AccountId == n.Id
                                                     && x.TransactionDate > StartTime
                                                                               && x.ParentId == OwnerDetails.OwnerId
                                                                               && x.TransactionDate < EndTime
                                                     && x.Type.Value == TransactionTypeValue.Redeem
                                                     && x.CreatedById == UserAccount.UserAccountId)
                                               .Sum(x => x.TotalAmount)
                                               }
                                             ).Skip(0).Take(5).ToList();
                    #endregion
                }

            }
            else
            {
                _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                             .Where(x =>
                                    x.ParentId != UserAccount.UserAccountId
                                    && x.AccountId == UserAccount.UserAccountId
                                    && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                     && x.SourceId == TransactionSource.Settlement
                                    && x.ModeId == TransactionMode.Credit)
                                     .Sum(x => (double?)x.TotalAmount) ?? 0;

                #region User Counts
                _AnaUsersCount.Merchant = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).Count();

                _AnaUsersCount.MerchantKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                _AnaUsersCount.MerchantDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                _AnaUsersCount.PSSPKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                _AnaUsersCount.PSSPDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();

                _AnaUsersCount.AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.Guid).FirstOrDefault();
                _AnaUsersCount.AcquirerDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer && x.StatusId == StatusActive).Select(x => x.Owner.DisplayName).FirstOrDefault();
                _AnaUsersCount.MobileUser = _HCoreContext.HCUAccount
                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                       && x.CreateDate > StartTime
                       && x.CreateDate < EndTime
                       && (x.OwnerId == UserAccount.UserAccountId
                           || x.CreatedById == UserAccount.UserAccountId
                           || x.Owner.OwnerId == UserAccount.UserAccountId
                           || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                    .Count();
                _AnaUsersCount.CardUser = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Carduser
                           && x.CreateDate > StartTime
                           && x.CreateDate < EndTime
                           && (x.OwnerId == UserAccount.UserAccountId ||
                               x.CreatedById == UserAccount.UserAccountId ||
                               x.Owner.OwnerId == UserAccount.UserAccountId ||
                               x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                    .Count();
                _AnaUsersCount.AppUser = _AnaUsersCount.MobileUser + _AnaUsersCount.CardUser;
                _AnaResponse.UsersCount = _AnaUsersCount;
                #endregion
                #region Rewards
                _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Reward
                       && x.ModeId == TransactionMode.Credit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Reward
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                       && x.ModeId == TransactionMode.Credit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Redeem
                       && x.ModeId == TransactionMode.Debit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Redeem
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime

                       && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Payments
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.Type.Value == TransactionTypeValue.Payments
                       && x.ModeId == TransactionMode.Debit
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Count();

                _AnaResponse.Rewards = _AnaRewards;
                #endregion
                #region Visitors
                _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedById == UserAccount.UserAccountId
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                           && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Select(x => x.AccountId).Count();

                _AnaVisitors.Single = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                       && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                       a.CreatedById == UserAccount.UserAccountId).Count() == 1)
                        .Select(x => x.Id).Count();

                _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.Account.UserId == x.UserId
                                                                       && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&
                                                                       a.CreatedById == UserAccount.UserAccountId).Count() > 1)
                        .Select(x => x.Id).Count();
                _AnaResponse.Vistitors = _AnaVisitors;
                #endregion
                #region Reward Overview
                _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var RewardType in _AnaResponse.RewardOverview)
                {
                    RewardType.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                       && x.TypeId == RewardType.Id
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Credit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
                #region Redeem Overview
                _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                {
                    RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.TypeId == RedeemOverview.Id
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                        && x.ModeId == TransactionMode.Debit
                        && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
                #region Payments Overview
                _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                {
                    PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                        && x.TypeId == PaymentOverview.Id
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Debit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
                #region Top Users
                _AnaResponse.TopVisitors = (from n in _HCoreContext.HCUAccount
                                            where
                                             (n.AccountTypeId == UserAccountType.Appuser)
                                            && (_HCoreContext.HCUAccountTransaction.
                                            Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.CreatedById == UserAccount.UserAccountId)
                                            .Count() > 0)
                                            orderby _HCoreContext.HCUAccountTransaction.
                                            Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.CreatedById == UserAccount.UserAccountId)
                                            .Count() descending
                                            select new OAnalytics.OUsers
                                            {
                                                ReferenceKey = n.Guid,
                                                DisplayName = n.DisplayName,
                                                ContactNumber = n.ContactNumber,
                                                CreateDate = n.CreateDate,
                                                Count = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                  && x.TransactionDate > StartTime
                                                  && x.TransactionDate < EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                                                  && x.CreatedById == UserAccount.UserAccountId)
                                            .Count()
                                            }
                                        ).Skip(0).Take(5).ToList();

                _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                           where
                                            (n.AccountTypeId == UserAccountType.Appuser)
                                           && (_HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) > 0)
                                           orderby _HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) descending
                                           select new OAnalytics.OUsers
                                           {
                                               ReferenceKey = n.Guid,
                                               DisplayName = n.DisplayName,
                                               UserKey = n.Guid,
                                               ContactNumber = n.ContactNumber,
                                               CreateDate = n.CreateDate,
                                               Amount = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount)
                                           }
                                          ).Skip(0).Take(5).ToList();

                _AnaResponse.TopRedeems = (from n in _HCoreContext.HCUAccount
                                           where
                                            (n.AccountTypeId == UserAccountType.Appuser)
                                           && (_HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) > 0)
                                           orderby _HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount) descending
                                           select new OAnalytics.OUsers
                                           {
                                               ReferenceKey = n.Guid,
                                               DisplayName = n.DisplayName,
                                               UserKey = n.Guid,
                                               ContactNumber = n.ContactNumber,
                                               CreateDate = n.CreateDate,
                                               Amount = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Redeem
                                                 && x.CreatedById == UserAccount.UserAccountId)
                                           .Sum(x => x.TotalAmount)
                                           }
                                         ).Skip(0).Take(5).ToList();
                #endregion 
            }

            return _AnaResponse;
        }
        /// <summary>
        /// Description: Gets the user overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="UserAccount">The user account.</param>
        /// <param name="TransactionTypes">The transaction types.</param>
        /// <param name="StartTime">The start time.</param>
        /// <param name="EndTime">The end time.</param>
        /// <returns>OAnalytics.Response.</returns>
        private OAnalytics.Response GetUserOverview(OAnalytics.Request _Request, OAnalytics.Account UserAccount, List<OAnalytics.OTransactionType> TransactionTypes, DateTime StartTime, DateTime EndTime)
        {
            #region Declare

            _AnaResponse = new OAnalytics.Response();
            _AnaUsersCount = new OAnalytics.OUsersCount();
            _AnaRewards = new OAnalytics.Rewards();
            _AnaVisitors = new OAnalytics.Visitors();
            _AnaInvoices = new OAnalytics.Invoices();
            _AnaRewardBalance = new OAnalytics.RewardBalance();
            #endregion

            if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
            {
                _AnaInvoices.UserCommission = _HCoreContext.HCUAccountTransaction
                .Where(x =>
                       x.ParentId != UserAccount.UserAccountId
                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.AccountId == UserAccount.UserAccountId
                       && x.SourceId == TransactionSource.Settlement
                       && x.ModeId == TransactionMode.Credit)
                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaUsersCount.AppUser = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId && x.StatusId == StatusActive && x.AccountTypeId == UserAccountType.Appuser && x.StatusId == StatusActive).Select(x => x.Guid).Count();
                _AnaUsersCount.ReferredMerchant = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId && x.StatusId == StatusActive && x.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.Guid).Count();

                List<long> MerchantId = new List<long>();
                var Terminals = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccount.UserAccountId && x.Account.AccountTypeId == UserAccountType.Terminal && x.StatusId == StatusActive).Select(x => x.AccountId).ToList();
                foreach (var item in Terminals)
                {
                    long MId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == item && x.Owner.AccountTypeId == UserAccountType.Merchant && x.StatusId == StatusActive).Select(x => x.OwnerId).FirstOrDefault();
                    MerchantId.Add(MId);
                    _AnaRewards.InvoiceAmount = _AnaRewards.InvoiceAmount + _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.CreatedById == item
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                }
                var Merchants = MerchantId.Distinct();
                _AnaUsersCount.Merchant = MerchantId.Distinct().Count();
                foreach (var Merchant in Merchants)
                {
                    _AnaUsersCount.Store = _AnaUsersCount.Store + _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.OwnerId == Merchant && x.StatusId == StatusActive).Count();
                }
                //_AnaUsersCount.Merchant = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId && x.StatusId == StatusActive && x.AccountTypeId == UserAccountType.Merchant).Select(x => x.Guid).Count();
                _AnaUsersCount.Terminal = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccount.UserAccountId && x.Account.AccountTypeId == UserAccountType.Terminal && x.StatusId == StatusActive).Select(x => x.Guid).Count();
                _AnaResponse.UsersCount = _AnaUsersCount;
                #region Rewards




                //_AnaRewards.Reward = (from x in _HCoreContext.HCUAccountTransaction
                //where x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime
                //&& x.TransactionDate < EndTime
                //&& x.SourceId == TransactionSource.TUC
                //&& (x.Account.AccountTypeId == UserAccountType.Appuser
                //    || x.Account.AccountTypeId == UserAccountType.Carduser)
                //&& x.ModeId == TransactionMode.Credit
                //&& _HCoreContext.HCUAccountTransaction
                //    .Where(a =>
                //           a.AccountId == UserAccount.UserAccountId
                //           && a.GroupKey == x.GroupKey
                //           && a.ModeId == TransactionMode.Credit
                //           && a.SourceId == TransactionSource.Settlement
                //          ).Count() > 0
                //select (double?)x.ParentTransaction.TotalAmount).Sum() ?? 0;

                //_AnaRewards.RewardCount = (from x in _HCoreContext.HCUAccountTransaction
                //where x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime
                //&& x.TransactionDate < EndTime
                //&& x.SourceId == TransactionSource.TUC
                //&& (x.Account.AccountTypeId == UserAccountType.Appuser
                //    || x.Account.AccountTypeId == UserAccountType.Carduser)
                //&& x.ModeId == TransactionMode.Credit
                //&& _HCoreContext.HCUAccountTransaction
                //    .Where(a =>
                //           a.AccountId == UserAccount.UserAccountId
                //           && a.GroupKey == x.GroupKey
                //           && a.ModeId == TransactionMode.Credit
                //           && a.SourceId == TransactionSource.Settlement
                //          ).Count() > 0
                //select x.Id).Count();


                //_AnaRewards.InvoiceAmount = (from x in _HCoreContext.HCUAccountTransaction
                //where x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime
                //&& x.TransactionDate < EndTime
                //&& x.SourceId == TransactionSource.TUC
                //&& (x.Account.AccountTypeId == UserAccountType.Appuser
                //    || x.Account.AccountTypeId == UserAccountType.Carduser)
                //&& x.ModeId == TransactionMode.Credit
                //&& _HCoreContext.HCUAccountTransaction
                //    .Where(a =>
                //           a.AccountId == UserAccount.UserAccountId
                //           && a.GroupKey == x.GroupKey
                //           && a.ModeId == TransactionMode.Credit
                //           && a.SourceId == TransactionSource.Settlement
                //          ).Count() > 0
                //select (double?)x.PurchaseAmount).Sum() ?? 0;


                //_AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                //.Where(x =>
                //    ((x.Parent.OwnerId == UserAccount.UserAccountId
                //                                                       && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                      )
                //                                                   || (
                //                                                  x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                  ))
                // && x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //&& x.ModeId == TransactionMode.Credit
                //   && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //.Sum(x => (double?)x.TotalAmount) ?? 0;

                //                _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                //.Where(x =>
                //((x.Parent.OwnerId == UserAccount.UserAccountId
                //&& x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //)
                //|| (
                //x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //))
                //&& x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //&& x.ModeId == TransactionMode.Credit
                //&& (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //.Count();


                //_AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                //   .Where(x =>
                //          ((x.Parent.OwnerId == UserAccount.UserAccountId
                //                                                             && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                            )
                //                                                         || (
                //                                                        x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                        ))
                //       && x.Type.Value == TransactionTypeValue.Redeem
                //      && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //      && x.ModeId == TransactionMode.Debit
                //          && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //       .Sum(x => (double?)x.TotalAmount) ?? 0;

                //_AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                //.Where(x =>
                //    ((x.Parent.OwnerId == UserAccount.UserAccountId
                //                                                       && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                      )
                //                                                   || (
                //                                                  x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                  ))
                // && x.Type.Value == TransactionTypeValue.Redeem
                //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //&& x.ModeId == TransactionMode.Debit
                //   && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //.Count();


                //_AnaRewards.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                //.Where(x =>
                //    ((x.Parent.OwnerId == UserAccount.UserAccountId
                //                                                       && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                      )
                //                                                   || (
                //                                                  x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                  ))
                // && x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //&& x.ModeId == TransactionMode.Credit
                //   && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //.Sum(x => (double?)x.PurchaseAmount) ?? 0;


                //_AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                //.Where(x =>
                //    ((x.Parent.OwnerId == UserAccount.UserAccountId
                //                                                       && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                      )
                //                                                   || (
                //                                                  x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                //                                                  ))
                //    && (x.Type.Value == TransactionTypeValue.Reward || x.Type.Value == TransactionTypeValue.Redeem)
                //&& x.TransactionDate > StartTime && x.TransactionDate < EndTime
                //&& x.ModeId == TransactionMode.Credit
                //   && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                //.Sum(x => (double?)x.PurchaseAmount) ?? 0;

                _AnaResponse.Rewards = _AnaRewards;


                #endregion                 //_AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                           //                            where
                                           //                             ( n.AccountTypeId == UserAccountType.Appuser)
                                           //                            && (_HCoreContext.HCUAccountTransaction.
                                           //                            Where(x => x.AccountId == n.Id
                                           //                                  && x.TransactionDate > StartTime
                                           //                                  && x.TransactionDate < EndTime
                                           //                                  && x.Type.Value == TransactionTypeValue.Reward
                                           //                                  && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                           //                                 && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                                )
                                           //                             || (
                                           //                            x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                            )))
                                           //                            .Sum(x => x.TotalAmount) > 0)
                                           //                            orderby _HCoreContext.HCUAccountTransaction.
                                           //                            Where(x => x.AccountId == n.Id
                                           //                                  && x.TransactionDate > StartTime
                                           //                                  && x.TransactionDate < EndTime
                                           //                                  && x.Type.Value == TransactionTypeValue.Reward
                                           //                                  && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                           //                                 && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                                )
                                           //                             || (
                                           //                            x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                            )))
                                           //                            .Sum(x => x.TotalAmount) descending
                                           //                            select new OAnalytics.OUsers
                                           //                            {
                                           //                                ReferenceKey = n.Guid,
                                           //                                DisplayName = n.DisplayName,
                                           //                                UserKey = n.Guid,
                                           //                                ContactNumber = n.ContactNumber,
                                           //                                CreateDate = n.CreateDate,
                                           //                                Amount = _HCoreContext.HCUAccountTransaction.
                                           //                                                      Where(x => x.AccountId == n.Id
                                           //                                  && x.TransactionDate > StartTime
                                           //                                  && x.TransactionDate < EndTime
                                           //                                  && x.Type.Value == TransactionTypeValue.Reward
                                           //                                                            && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                           //                                 && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                                )
                                           //                             || (
                                           //                            x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.TerminalAccount).Select(a => a.AccountId).FirstOrDefault())
                                           //                            )))
                                           //                            .Sum(x => x.TotalAmount)
                                           //                            }
                                           //).Skip(0).Take(5).ToList();
                #region Visitors
                //_AnaVisitors.Total = (from x in _HCoreContext.HCUAccountTransaction
                //where x.Type.Value == TransactionTypeValue.Reward
                //&& x.TransactionDate > StartTime
                //&& x.TransactionDate < EndTime
                //&& x.SourceId == TransactionSource.App
                //&& (x.Account.AccountTypeId == UserAccountType.Appuser
                //    || x.Account.AccountTypeId == UserAccountType.Carduser)
                //&& x.ModeId == TransactionMode.Credit
                //&& _HCoreContext.HCUAccountTransaction
                //    .Where(a =>
                //           a.AccountId == UserAccount.UserAccountId
                //           && a.GroupKey == x.GroupKey
                //           && a.ModeId == TransactionMode.Credit
                //           && a.SourceId == TransactionSource.Settlement
                //          ).Count() > 0
                //select x.Id).Count();
                _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(x => x.OwnerId == UserAccount.UserAccountId).Count() > 0
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                _AnaVisitors.Single = _HCoreContext.HCUAccount
                    .Where(x => (x.AccountTypeId == UserAccountType.Appuser) && (x.HCUAccountTransactionAccount
                                                       .Where(m =>
                                                            m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccount.UserAccountId).Count() > 0
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count() == 1)).Count();

                _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                    .Where(x => (x.AccountTypeId == UserAccountType.Appuser) && (x.HCUAccountTransactionAccount
                                                       .Where(m =>
                                                            m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccount.UserAccountId).Count() > 0
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count() > 1)).Count();

                _AnaResponse.Vistitors = _AnaVisitors;
                #endregion
                #region Reward Overview
                #endregion
            }
            else if (UserAccount.AccountTypeId == UserAccountType.AcquirerSubAccount)
            {
                _AnaUsersCount.Merchant = _HCoreContext.HCUAccount.Where(x => x.CreatedById == UserAccount.UserAccountId && x.StatusId == StatusActive).Select(x => x.Guid).Count();
                _AnaUsersCount.Terminal = _HCoreContext.HCUAccountOwner.Where(x => x.CreatedById == UserAccount.UserAccountId && x.Account.AccountTypeId == UserAccountType.Terminal && x.StatusId == StatusActive).Select(x => x.Guid).Count();
                _AnaResponse.UsersCount = _AnaUsersCount;


                #region Rewards
                _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                   .Where(x =>
                          ((x.Parent.OwnerId == UserAccount.UserAccountId
                                                                             && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                            )
                                                                         || (
                                                                        x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                        ))
                       && x.Type.Value == TransactionTypeValue.Reward
                      && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                      && x.ModeId == TransactionMode.Credit
                          && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                       .Sum(x => (double?)x.TotalAmount) ?? 0;

                _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                   .Where(x =>
                          ((x.Parent.OwnerId == UserAccount.UserAccountId
                            && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                            )
                                                                         || (
                               x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                        ))
                       && x.Type.Value == TransactionTypeValue.Reward
                      && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                      && x.ModeId == TransactionMode.Credit
                          && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                       .Count();





                _AnaRewards.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                   .Where(x =>
                          ((x.Parent.OwnerId == UserAccount.UserAccountId
                            && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                            )
                                                                         || (
                               x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                        ))
                       && x.Type.Value == TransactionTypeValue.Reward
                      && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                      && x.ModeId == TransactionMode.Credit
                          && (x.Account.AccountTypeId == UserAccountType.Appuser || x.Account.AccountTypeId == UserAccountType.Carduser))
                       .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                _AnaResponse.Rewards = _AnaRewards;
                #endregion
                _AnaResponse.TopRewards = (from n in _HCoreContext.HCUAccount
                                           where
                                            (n.AccountTypeId == UserAccountType.Appuser)
                                           && (_HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                                      && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                               )
                                            || (
                                                         x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                           )))
                                           .Sum(x => x.TotalAmount) > 0)
                                           orderby _HCoreContext.HCUAccountTransaction.
                                           Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                 && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                                      && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                               )
                                            || (
                                                         x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                           )))
                                           .Sum(x => x.TotalAmount) descending
                                           select new OAnalytics.OUsers
                                           {
                                               ReferenceKey = n.Guid,
                                               DisplayName = n.DisplayName,
                                               UserKey = n.Guid,
                                               ContactNumber = n.ContactNumber,
                                               CreateDate = n.CreateDate,
                                               Amount = _HCoreContext.HCUAccountTransaction.
                                                                     Where(x => x.AccountId == n.Id
                                                 && x.TransactionDate > StartTime
                                                 && x.TransactionDate < EndTime
                                                 && x.Type.Value == TransactionTypeValue.Reward
                                                                           && ((x.Parent.OwnerId == UserAccount.UserAccountId
                                                                                && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                               )
                                            || (
                                                                                   x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                           )))
                                           .Sum(x => x.TotalAmount)
                                           }
                                          ).Skip(0).Take(5).ToList();
                #region Visitors
                _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                    .Where(x => ((x.Parent.OwnerId == UserAccount.UserAccountId
                                  && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.OwnerId == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                            )
                                                                         || (
                                     x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                        ))
                          && x.Type.Value == TransactionTypeValue.Reward
                      && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                      && (x.Account.AccountTypeId == UserAccountType.Appuser))
                    .Count();


                _AnaVisitors.Single = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id
                                                                       && a.Type.Value == TransactionTypeValue.Reward
                                                                       && a.TransactionDate > StartTime && a.TransactionDate < EndTime &&

                                                                          ((a.Parent.OwnerId == UserAccount.UserAccountId
                                                                            && a.CreatedById != (_HCoreContext.HCUAccountOwner.Where(m => m.Account.CreatedById == UserAccount.UserAccountId && m.Account.AccountTypeId == UserAccountType.Terminal).Select(m => m.AccountId).FirstOrDefault())
                                                                            )
                                                                           || (a.CreatedById == (_HCoreContext.HCUAccountOwner.Where(m => m.Account.CreatedById == UserAccount.UserAccountId && m.Account.AccountTypeId == UserAccountType.Terminal).Select(m => m.AccountId).FirstOrDefault()))
                                                                        )
                                                                      ).Count() == 1)
                        .Select(x => x.Id).Count();

                _AnaVisitors.Multiple = _HCoreContext.HCUAccount
                    .Where(x =>
                           (x.AccountTypeId == UserAccountType.Appuser) &&
                           x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id && a.TransactionDate > StartTime && a.TransactionDate < EndTime
                                                                       && (a.Type.Value == TransactionTypeValue.Reward)
                                                                       &&

                                                                          ((a.Parent.OwnerId == UserAccount.UserAccountId
                                                                            && a.CreatedById != (_HCoreContext.HCUAccountOwner.Where(m => m.Account.CreatedById == UserAccount.UserAccountId && m.Account.AccountTypeId == UserAccountType.Terminal).Select(m => m.AccountId).FirstOrDefault())
                                                                            )
                                                                           || (a.CreatedById == (_HCoreContext.HCUAccountOwner.Where(m => m.Account.CreatedById == UserAccount.UserAccountId && m.Account.AccountTypeId == UserAccountType.Terminal).Select(m => m.AccountId).FirstOrDefault()))
                                                                        )
                                                                      ).Count() > 1)
                        .Select(x => x.Id).Count();
                _AnaResponse.Vistitors = _AnaVisitors;
                #endregion
                #region Reward Overview
                _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                   .Select(x => new OAnalytics.OUserTransactionType
                   {
                       Id = x.ReferenceId,
                       Name = x.Name
                   }).ToList();
                foreach (var RewardType in _AnaResponse.RewardOverview)
                {
                    RewardType.Amount = _HCoreContext.HCUAccountTransaction
                        .Where(x => ((x.Parent.OwnerId == UserAccount.UserAccountId
                                      && x.CreatedById != (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                            )
                                                                         || (
                                         x.CreatedById == (_HCoreContext.HCUAccountOwner.Where(a => a.Account.CreatedById == UserAccount.UserAccountId && a.Account.AccountTypeId == UserAccountType.Terminal).Select(a => a.AccountId).FirstOrDefault())
                                                                        ))
                       && x.TypeId == RewardType.Id
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                       && x.ModeId == TransactionMode.Credit
                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                        .Sum(x => (double?)x.TotalAmount) ?? 0;
                }
                #endregion
            }
            else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
            {
                _AnaUsersCount.Cards = _HCoreContext.HCUAccount.Where(x => x.UserId == UserAccount.UserId
                                                                        && x.AccountTypeId == UserAccountType.Carduser)
                    .Select(x => new OAnalytics.UserCards
                    {
                        ReferenceKey = x.Guid,
                        CardNumber = x.AccountCode,
                        SerialNumber = x.ReferralCode,
                        CardType = x.Card.CardType.Name,
                        CreateDate = x.CreateDate,
                        CreatedByKey = x.CreatedBy.Guid,
                        CreatedByDisplayName = x.CreatedBy.DisplayName,
                        CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                        CreatedByAccountType = x.CreatedBy.AccountType.SystemName,
                        CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,
                        CreatedByOwnerAccountType = x.CreatedBy.AccountType.SystemName,
                    }).ToList();
                var UDetaildInfo = _HCoreContext.HCUAccount
                                       .Where(x => x.Guid == _Request.UserAccountKey)
            .Select(x => new
            {
                CreatedByKey = x.CreatedBy.Guid,
                CreatedByDisplayName = x.CreatedBy.DisplayName,
                CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                CreatedByAccountType = x.CreatedBy.AccountType.SystemName,
                CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,
                CreatedByOwnerAccountType = x.CreatedBy.AccountType.SystemName,

            }).FirstOrDefault();
                if (UDetaildInfo != null)
                {
                    _AnaUsersCount.CreatedByKey = UDetaildInfo.CreatedByKey;
                    _AnaUsersCount.CreatedByDisplayName = UDetaildInfo.CreatedByDisplayName;
                    _AnaUsersCount.CreatedByAccountType = UDetaildInfo.CreatedByAccountType;
                    _AnaUsersCount.CreatedByOwnerKey = UDetaildInfo.CreatedByOwnerKey;
                    _AnaUsersCount.CreatedByOwnerDisplayName = UDetaildInfo.CreatedByOwnerDisplayName;
                    _AnaUsersCount.CreatedByOwnerAccountType = UDetaildInfo.CreatedByOwnerAccountType;
                }
                _AnaUsersCount.Cashier = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccount.UserAccountId && x.AccountTypeId == UserAccountType.MerchantCashier).Select(x => x.Id).Count();
                _AnaUsersCount.Store = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccount.UserAccountId).Select(x => x.Id).Count();
                _AnaUsersCount.PSSP = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).Count();
                _AnaUsersCount.PSTP = _HCoreContext.HCUAccountOwner.Where(x => x.Account.OwnerId == UserAccount.UserAccountId && x.Owner.AccountTypeId == UserAccountType.PosAccount).Select(x => x.Id).Count();
                _AnaUsersCount.Terminal = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == UserAccount.UserAccountId).Select(x => x.Id).Count();
                _AnaUsersCount.AcquirerKey = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.UserAccountId).Select(x => x.Owner.Guid).FirstOrDefault();
                _AnaUsersCount.AcquirerDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.UserAccountId).Select(x => x.Owner.DisplayName).FirstOrDefault();
                _AnaUsersCount.AppUser = _HCoreContext.HCUAccount
                    .Where(x => (x.AccountTypeId == UserAccountType.Appuser) && (x.CreatedById == UserAccount.UserAccountId || x.Owner.OwnerId == UserAccount.UserAccountId || x.Owner.Owner.OwnerId == UserAccount.UserAccountId)).Select(x => x.Id).Count();
                _AnaResponse.UsersCount = _AnaUsersCount;

                if (!string.IsNullOrEmpty(_Request.OwnerKey))
                {
                    var OwnerDetails = _HCoreContext.HCUAccount
                                                    .Where(x => x.Guid == _Request.OwnerKey)
                                                    .Select(x => new
                                                    {
                                                        OwnerId = x.Id,
                                                        AccountTypeId = x.AccountTypeId
                                                    }).FirstOrDefault();
                    if (OwnerDetails != null)
                    {
                        if (OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                        {
                            #region Rewards
                            _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                   && x.ModeId == TransactionMode.Credit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                       && x.ModeId == TransactionMode.Credit)
                                    .Count();
                            _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward))
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaResponse.Rewards = _AnaRewards;
                            #endregion
                            #region Reward Overview
                            _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RewardType in _AnaResponse.RewardOverview)
                            {
                                RewardType.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TypeId == RewardType.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Credit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Redeem Overview
                            _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                            {
                                RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == RedeemOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.ModeId == TransactionMode.Debit
                                    && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Payments Overview
                            _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                            {
                                PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.ParentId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == PaymentOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Debit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Visits
                            _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.UserId == UserAccount.UserId
                                       && x.ParentId == OwnerDetails.OwnerId)
                                .Select(x => x.AccountId).Count();
                            _AnaResponse.Vistitors = _AnaVisitors;
                            #endregion
                        }
                        else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantStore
                                 || OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                        {

                            #region Rewards
                            _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                   && x.ModeId == TransactionMode.Credit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                       && x.ModeId == TransactionMode.Credit)
                                    .Count();
                            _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward))
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaResponse.Rewards = _AnaRewards;
                            #endregion
                            #region Reward Overview
                            _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RewardType in _AnaResponse.RewardOverview)
                            {
                                RewardType.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TypeId == RewardType.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Credit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Redeem Overview
                            _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                            {
                                RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == RedeemOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.ModeId == TransactionMode.Debit
                                    && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Payments Overview
                            _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                            {
                                PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == PaymentOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Debit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Visits
                            _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.UserId == UserAccount.UserId
                                       && x.CreatedBy.OwnerId == OwnerDetails.OwnerId)
                                .Select(x => x.AccountId).Count();
                            _AnaResponse.Vistitors = _AnaVisitors;
                            #endregion
                        }
                        else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantCashier
                                 || OwnerDetails.AccountTypeId == UserAccountType.Terminal
                                 || OwnerDetails.AccountTypeId == UserAccountType.PgAccount
                                )
                        {
                            #region Rewards
                            _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                   && x.ModeId == TransactionMode.Credit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Reward
                                       && x.ModeId == TransactionMode.Credit)
                                    .Count();
                            _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Redeem
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward))
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                       && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.Type.Value == TransactionTypeValue.Payments
                                       && x.ModeId == TransactionMode.Debit)
                                    .Count();
                            _AnaResponse.Rewards = _AnaRewards;
                            #endregion
                            #region Reward Overview
                            _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RewardType in _AnaResponse.RewardOverview)
                            {
                                RewardType.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                   && x.TypeId == RewardType.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Credit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Redeem Overview
                            _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                            {
                                RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == RedeemOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                    && x.ModeId == TransactionMode.Debit
                                    && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Payments Overview
                            _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                               .Select(x => new OAnalytics.OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                            {
                                PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedById == OwnerDetails.OwnerId
                                   && x.Account.UserId == UserAccount.UserId
                                    && x.TypeId == PaymentOverview.Id
                                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                                   && x.ModeId == TransactionMode.Debit
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;
                            }
                            #endregion
                            #region Visits
                            _AnaVisitors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.UserId == UserAccount.UserId
                                       && x.CreatedById == OwnerDetails.OwnerId)
                                .Select(x => x.AccountId).Count();
                            _AnaResponse.Vistitors = _AnaVisitors;
                            #endregion
                        }
                    }

                }
                else
                {


                    #region Rewards
                    _AnaRewards.Reward = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.Account.UserId == UserAccount.UserId
                           && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Reward
                           && x.ModeId == TransactionMode.Credit)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RewardCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Reward
                               && x.ModeId == TransactionMode.Credit)
                            .Count();
                    _AnaRewards.Redeem = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Redeem
                               && x.ModeId == TransactionMode.Debit)
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _AnaRewards.RedeemCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Redeem
                               && x.ModeId == TransactionMode.Debit)
                            .Count();
                    _AnaRewards.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                               && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    _AnaRewards.Payments = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Payments
                               && x.ModeId == TransactionMode.Debit)
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    _AnaRewards.PaymentsCount = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Account.UserId == UserAccount.UserId
                               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.Type.Value == TransactionTypeValue.Payments
                               && x.ModeId == TransactionMode.Debit)
                            .Count();
                    _AnaResponse.Rewards = _AnaRewards;
                    #endregion
                    #region Reward Overview
                    _AnaResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RewardType in _AnaResponse.RewardOverview)
                    {
                        RewardType.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.UserId == UserAccount.UserId
                           && x.TypeId == RewardType.Id
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Credit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Redeem Overview
                    _AnaResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var RedeemOverview in _AnaResponse.RedeemOverview)
                    {
                        RedeemOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.UserId == UserAccount.UserId
                            && x.TypeId == RedeemOverview.Id
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                            && x.ModeId == TransactionMode.Debit
                            && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                    #region Payments Overview
                    _AnaResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                       .Select(x => new OAnalytics.OUserTransactionType
                       {
                           Id = x.ReferenceId,
                           Name = x.Name
                       }).ToList();
                    foreach (var PaymentOverview in _AnaResponse.PaymentsOverview)
                    {
                        PaymentOverview.Amount = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.UserId == UserAccount.UserId
                            && x.TypeId == PaymentOverview.Id
                                   && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                           && x.ModeId == TransactionMode.Debit
                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    #endregion
                }

            }

            return _AnaResponse;
        }
    }
}
