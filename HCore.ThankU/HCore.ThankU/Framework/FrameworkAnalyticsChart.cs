//==================================================================================
// FileName: FrameworkAnalyticsChart.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to analytics chart
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankU.Framework
{
    internal class FrameworkAnalyticsChart
    {
        #region Declare

        #endregion
        #region References
        //ManageLog _ManageLog;
        #endregion
        #region Entity
        HCoreContext _HCoreContext;
        #endregion
        #region Objects



        OChart.Request _OChartRequest;
        List<OChart.RequestDateRange> _OChartRequestRange;
        List<OChart.RequestDateRange> _TChartRequestRange;
        List<OChart.RequestData> _OChartRequestRangeValue;
        #endregion
        /// <summary>
        /// Description: Gets the user analytics.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAnalytics(OChart.Request _Request)
        {

            #region Intialize Object

            _OChartRequest = new OChart.Request();
            _OChartRequestRange = new List<OChart.RequestDateRange>();
            #endregion
            #region Build Date Range
            _TChartRequestRange = new List<OChart.RequestDateRange>();
            int Days = (_Request.EndTime - _Request.StartTime).Days;
            if (Days > 30)
            {
                _Request.StartTime = _Request.EndTime.AddDays(-30);
            }
            Days = (_Request.EndTime - _Request.StartTime).Days;
            if (Days == 0 || Days == 1)
            {
                for (int i = 0; i < 23; i++)
                {
                    DateTime TStartTime = _Request.StartTime.Date;
                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                    _Request.StartTime = TEndTime.AddSeconds(1);
                    _TChartRequestRange.Add(new OChart.RequestDateRange()
                    {
                        StartTime = TStartTime,
                        EndTime = TEndTime,
                    });
                }
            }
            else
            {
                for (int i = 0; i < Days; i++)
                {
                    DateTime TStartTime = _Request.StartTime;
                    DateTime TEndTime = _Request.StartTime.AddDays(1).AddSeconds(-1);
                    _Request.StartTime = TEndTime.AddSeconds(1);
                    _TChartRequestRange.Add(new OChart.RequestDateRange()
                    {
                        StartTime = TStartTime,
                        EndTime = TEndTime,
                    });
                }
            }


            _Request.DateRange = _TChartRequestRange;
            #endregion
            using (_HCoreContext = new HCoreContext())
            {
                #region System Helpers
                List<OAnalytics.OTransactionType> TransactionTypes = _HCoreContext.HCCore
                                       .Where(x => x.ParentId == HelperType.TransactionType && x.StatusId == StatusActive)
                    .Select(x => new OAnalytics.OTransactionType
                    {
                        ReferenceId = x.Id,
                        Name = x.Name,
                        Value = x.Value,
                    }).ToList();
                foreach (var TransactionType in TransactionTypes)
                {
                    TransactionType.Name = TransactionType.Name.Replace("Rewards", "").Replace("Reward", "").Replace("Redeems", "").Replace("Redeem", "");
                }
                #endregion
                var UserAccount = _HCoreContext.HCUAccount
                                                   .Where(x => x.Guid == _Request.UserAccountKey)
                                                   .Select(x => new
                                                   {
                                                       UserAccountId = x.Id,
                                                       CreateDate = x.CreateDate,
                                                       AccountTypeId = x.AccountTypeId,
                                                       UserId = x.UserId
                                                   }).FirstOrDefault();
                if (UserAccount != null)
                {

                    var OwnerDetails = _HCoreContext.HCUAccount
                                                    .Where(x => x.Guid == _Request.SubReferenceKey)
                                                    .Select(x => new
                                                    {
                                                        OwnerId = x.Id,
                                                        AccountTypeId = x.AccountTypeId
                                                    }).FirstOrDefault();

                    if (_Request.Type == ListType.Rewards)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                 .Select(x => new
                                 {
                                     ReferenceId = x.ReferenceId,
                                     Name = x.Name
                                 }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                            {

                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && x.CreatedById == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId

                                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                       && x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Credit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                            {
                                if (UserAccount.AccountTypeId == UserAccountType.PgAccount && OwnerDetails != null)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                                                   && x.ParentId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x =>
                                                                        (_HCoreContext.HCUAccountTransaction
                                             .Where(a =>
                                                    a.AccountId == UserAccount.UserAccountId
                                                    && a.GroupKey == x.GroupKey
                                                    && a.ModeId == TransactionMode.Credit
                                                    && a.SourceId == TransactionSource.Settlement
                                                   ).FirstOrDefault() != null)
                                       && x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Credit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.AcquirerSubAccount)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x =>
                                                                       (_HCoreContext.HCUAccountTransaction
                                             .Where(a =>
                                                    a.Account.OwnerId == UserAccount.UserAccountId
                                                    && a.GroupKey == x.GroupKey
                                                    && a.ModeId == TransactionMode.Credit
                                                    && a.SourceId == TransactionSource.Settlement
                                                   ).FirstOrDefault() != null)
                                       && x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Credit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }

                            else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                            {
                                if (OwnerDetails != null)
                                {
                                    if (OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                                       && x.ParentId == OwnerDetails.OwnerId
                                               && x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Credit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantStore
                                || OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                                       && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                               && x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Credit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantCashier
                                 || OwnerDetails.AccountTypeId == UserAccountType.Terminal
                                 || OwnerDetails.AccountTypeId == UserAccountType.PgAccount
                                )
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                                       && x.CreatedById == OwnerDetails.OwnerId
                                               && x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Credit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Credit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Credit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.Redeems)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && x.CreatedById == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                                   && x.ParentId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                            {
                                if (UserAccount.AccountTypeId == UserAccountType.PgAccount && OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ParentId == OwnerDetails.OwnerId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }

                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(x =>
                                                                            x.Parent.OwnerId == OwnerDetails.OwnerId
                                           && x.ParentId == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(x =>
                                                                            ((x.Parent.OwnerId == UserAccount.UserAccountId
                                                                             && x.CreatedById != (_HCoreContext.TUCTerminal.Where(a => a.ProviderId == UserAccount.UserAccountId).Select(a => a.Id).FirstOrDefault())
                                                                            )
                                                                         || (
                                                                        x.CreatedById == (_HCoreContext.TUCTerminal.Where(a => a.ProviderId == UserAccount.UserAccountId).Select(a => a.Id).FirstOrDefault())
                                                                        ))
                                                                            && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }

                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                            {
                                if (OwnerDetails != null)
                                {
                                    if (OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                                       && x.ParentId == OwnerDetails.OwnerId
                                               && x.TypeId == HelperType.ReferenceId
                                                       && x.ModeId == TransactionMode.Debit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantStore
                                || OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                                       && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                               && x.TypeId == HelperType.ReferenceId
                                                       && x.ModeId == TransactionMode.Debit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantCashier
                                 || OwnerDetails.AccountTypeId == UserAccountType.Terminal
                                 || OwnerDetails.AccountTypeId == UserAccountType.PgAccount
                                )
                                    {
                                        foreach (var HelperType in HelperTypes)
                                        {
                                            double Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.UserId == UserAccount.UserId
                                               && x.TypeId == HelperType.ReferenceId
                                                       && x.CreatedById == OwnerDetails.OwnerId
                                                       && x.ModeId == TransactionMode.Debit
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            _OChartRequestRangeValue.Add(new OChart.RequestData
                                            {
                                                Name = HelperType.Name,
                                                Value = Amount.ToString(),
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }

                            }
                            else
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Debit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.Payments)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                               .Select(x => new
                               {
                                   ReferenceId = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                                   && x.CreatedById == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });

                                    }

                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.ParentId == UserAccount.UserAccountId
                                                 && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                         && x.TypeId == HelperType.ReferenceId
                                         && x.ModeId == TransactionMode.Debit
                                         && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                         && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.ParentId == UserAccount.UserAccountId
                                                 && x.Parent.OwnerId == OwnerDetails.OwnerId
                                         && x.TypeId == HelperType.ReferenceId
                                         && x.ModeId == TransactionMode.Debit
                                         && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                         && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == UserAccount.UserAccountId
                                           && x.TypeId == HelperType.ReferenceId
                                           && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });

                                    }

                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                       && x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Debit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedById == UserAccount.UserAccountId
                                       && x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Debit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;


                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Parent.OwnerId == UserAccount.UserAccountId
                                       && x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Debit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;


                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.ParentId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null
                                    && (OwnerDetails.AccountTypeId == UserAccountType.MerchantStore
                                        || OwnerDetails.AccountTypeId == UserAccountType.PosAccount))
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else if (OwnerDetails != null
                                    && (OwnerDetails.AccountTypeId == UserAccountType.MerchantCashier
                                        || OwnerDetails.AccountTypeId == UserAccountType.PgAccount
                                        || OwnerDetails.AccountTypeId == UserAccountType.PosAccount))
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.CreatedById == OwnerDetails.OwnerId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    foreach (var HelperType in HelperTypes)
                                    {
                                        double Amount = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                           && x.TypeId == HelperType.ReferenceId
                                                   && x.ModeId == TransactionMode.Debit
                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                           && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;


                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = HelperType.Name,
                                            Value = Amount.ToString(),
                                        });
                                    }
                                }
                            }
                            else
                            {
                                foreach (var HelperType in HelperTypes)
                                {
                                    double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.TypeId == HelperType.ReferenceId
                                               && x.ModeId == TransactionMode.Debit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = HelperType.Name,
                                        Value = Amount.ToString(),
                                    });
                                }
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.AppUsers)
                    {
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant
                                || UserAccount.AccountTypeId == UserAccountType.MerchantStore
                                || UserAccount.AccountTypeId == UserAccountType.Acquirer
                                || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    long MobileUser = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && x.CreateDate > _RangeItem.StartTime
                                                           && x.CreateDate < _RangeItem.EndTime
                                                         && x.OwnerId == OwnerDetails.OwnerId
                                                           && x.CreatedById == UserAccount.UserAccountId)
                                                    .Count();
                                    long CardUser = 0;
                                    long Value = MobileUser + CardUser;


                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Users",
                                        Value = Value.ToString(),
                                    });
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                                {
                                    long MobileUser = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && x.CreateDate > _RangeItem.StartTime
                                                           && x.CreateDate < _RangeItem.EndTime
                                                         && x.CreatedById == OwnerDetails.OwnerId
                                                           && x.OwnerId == UserAccount.UserAccountId)
                                                    .Count();
                                    long CardUser = 0;
                                    long Value = MobileUser + CardUser;


                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Users",
                                        Value = Value.ToString(),
                                    });
                                }
                                else
                                {
                                    long MobileUser = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && x.CreateDate > _RangeItem.StartTime
                                                           && x.CreateDate < _RangeItem.EndTime
                                                    && (x.OwnerId == UserAccount.UserAccountId
                                                        || x.CreatedById == UserAccount.UserAccountId
                                                        || x.Owner.OwnerId == UserAccount.UserAccountId
                                                        || x.Owner.Owner.OwnerId == UserAccount.UserAccountId))
                                                    .Count();
                                    long CardUser = 0;
                                    long Value = MobileUser + CardUser;
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Users",
                                        Value = Value.ToString(),
                                    });
                                }

                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                long MobileUser = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && x.CreateDate > _RangeItem.StartTime
                                                           && x.CreateDate < _RangeItem.EndTime
                                                           && x.CreatedBy.OwnerId == UserAccount.UserAccountId)
                                                    .Count();
                                long CardUser = 0;
                                long Value = MobileUser + CardUser;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Users",
                                    Value = Value.ToString(),
                                });
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                            {

                                if (UserAccount.AccountTypeId == UserAccountType.PgAccount && OwnerDetails != null)
                                {
                                    long MobileUser = _HCoreContext.HCUAccount
                                                                                       .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                                              && x.CreateDate > _RangeItem.StartTime
                                                                                              && x.CreateDate < _RangeItem.EndTime
                                                                                              && x.OwnerId == OwnerDetails.OwnerId
                                                                                              && x.CreatedById == UserAccount.UserAccountId)
                                                                                       .Count();
                                    long CardUser = 0;
                                    long Value = MobileUser + CardUser;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Users",
                                        Value = Value.ToString(),
                                    });
                                }
                                else
                                {
                                    long MobileUser = _HCoreContext.HCUAccount
                                                                                       .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                                              && x.CreateDate > _RangeItem.StartTime
                                                                                              && x.CreateDate < _RangeItem.EndTime
                                                                                              && x.CreatedById == UserAccount.UserAccountId)
                                                                                       .Count();
                                    long CardUser = 0;
                                    long Value = MobileUser + CardUser;

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Users",
                                        Value = Value.ToString(),
                                    });
                                }
                            }
                            //else if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            //{
                            //}
                            //else if (UserAccount.AccountTypeId == UserAccountType.Appuser || UserAccount.AccountTypeId == UserAccountType.Carduser)
                            //{
                            //}
                            else
                            {
                                long MobileUser = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && x.CreateDate > _RangeItem.StartTime
                                                           && x.CreateDate < _RangeItem.EndTime)
                                                    .Count();
                                long CardUser = 0;
                                long Value = MobileUser + CardUser;
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Users",
                                    Value = Value.ToString(),
                                });
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.Visitors)
                    {
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.ParentId == UserAccount.UserAccountId
                                                       && x.CreatedById == OwnerDetails.OwnerId
                                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.ParentId == UserAccount.UserAccountId
                                                                             && x.CreatedById == OwnerDetails.OwnerId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                             && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                             && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        //Name = "Store Visits",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.ParentId == UserAccount.UserAccountId
                                                       && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.ParentId == UserAccount.UserAccountId
                                                                             && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                             && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                             && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                                else if (OwnerDetails != null && OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.ParentId == UserAccount.UserAccountId
                                                       && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.ParentId == UserAccount.UserAccountId
                                                                             && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.AccountId == OwnerDetails.OwnerId
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                             && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                             && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                                else
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.ParentId == UserAccount.UserAccountId
                                                       && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.ParentId == UserAccount.UserAccountId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                             && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                             && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        //Name = "Store Visits",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore || UserAccount.AccountTypeId == UserAccountType.PosAccount)
                            {
                                long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                   && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                  .Where(x => x.CreatedBy.OwnerId == UserAccount.UserAccountId
                                                                         && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                           && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                           && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Visitors",
                                    Value = TotalVisitors.ToString(),
                                });

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Transactions",
                                    //Name = "Store Visits",
                                    Value = TotalVisits.ToString(),
                                });
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.PgAccount || UserAccount.AccountTypeId == UserAccountType.MerchantCashier || UserAccount.AccountTypeId == UserAccountType.Terminal)
                            {
                                if (UserAccount.AccountTypeId == UserAccountType.PgAccount && OwnerDetails != null)
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                           .Where(x => x.CreatedById == UserAccount.UserAccountId
                                                  && x.ParentId == OwnerDetails.OwnerId
                                                  && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                  && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                  && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.CreatedById == UserAccount.UserAccountId
                                                                             && x.ParentId == OwnerDetails.OwnerId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                               && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        //Name = "Store Visits",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                                else
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == UserAccount.UserAccountId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                   && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.CreatedById == UserAccount.UserAccountId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                               && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visitors",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        //Name = "Store Visits",
                                        Value = TotalVisits.ToString(),
                                    });
                                }

                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Acquirer)
                            {
                                long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(x => x.OwnerId == UserAccount.UserAccountId).Count() > 0
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _RangeItem.StartTime
                                                           && m.TransactionDate < _RangeItem.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();
                                //long TotalVisits = _HCoreContext.HCUAccountTransaction
                                //                   .Where(x =>
                                //           (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                //         && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                //         && (x.Account.AccountTypeId == UserAccountType.Appuser)
                                //                         && _HCoreContext.HCUAccountTransaction
                                //.Where(a =>
                                // a.AccountId == UserAccount.UserAccountId
                                // && a.GroupKey == x.GroupKey
                                // && a.ModeId == TransactionMode.Credit
                                // && a.SourceId == TransactionSource.Settlement
                                //).Count() > 0
                                //).Select(x => x.Id).Count();
                                //long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                //                     .Where(x =>
                                //                              (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                //                            && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                //                            && (x.Account.AccountTypeId == UserAccountType.Appuser)
                                //                           && _HCoreContext.HCUAccountTransaction
                                //.Where(a =>
                                // a.AccountId == UserAccount.UserAccountId
                                // && a.GroupKey == x.GroupKey
                                // && a.ModeId == TransactionMode.Credit
                                // && a.SourceId == TransactionSource.Settlement
                                //).Count() > 0
                                //).Select(x => x.Account.UserId).Distinct().Count();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Visitors",
                                    Value = TotalVisits.ToString(),
                                });
                                //_OChartRequestRangeValue.Add(new OChart.RequestData
                                //{
                                //    Name = "Transactions",
                                //    //Name = "Store Visits",
                                //    Value = TotalVisits.ToString(),
                                //});
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.AcquirerSubAccount)
                            {
                                long TotalVisits = _HCoreContext.HCUAccountTransaction
                                                                .Where(x =>

                                                        (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                      && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                      && (x.Account.AccountTypeId == UserAccountType.Appuser)
                                                                      && _HCoreContext.HCUAccountTransaction
                                             .Where(a =>
                                                    a.Account.OwnerId == UserAccount.UserAccountId
                                                    && a.GroupKey == x.GroupKey
                                                    && a.ModeId == TransactionMode.Credit
                                                    && a.SourceId == TransactionSource.Settlement
                                                   ).Count() > 0
                                                                      ).Select(x => x.Id).Count();
                                long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                  .Where(x =>

                                                                           (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                         && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                         && (x.Account.AccountTypeId == UserAccountType.Appuser)
                                                                        && _HCoreContext.HCUAccountTransaction
                                             .Where(a =>
                                                    a.Account.OwnerId == UserAccount.UserAccountId
                                                    && a.GroupKey == x.GroupKey
                                                    && a.ModeId == TransactionMode.Credit
                                                    && a.SourceId == TransactionSource.Settlement
                                                   ).Count() > 0
                                                                        ).Select(x => x.Account.UserId).Distinct().Count();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Visitors",
                                    Value = TotalVisitors.ToString(),
                                });
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Transactions",
                                    Value = TotalVisits.ToString(),
                                });
                            }
                            else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                            {
                                if (OwnerDetails != null)
                                {
                                    if (OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                                    {
                                        long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.ParentId == OwnerDetails.OwnerId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                                        long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                          .Where(x => x.Account.UserId == UserAccount.UserId
                                                                                 && x.ParentId == OwnerDetails.OwnerId
                                                                                 && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.AccountId).Distinct().Count();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Visits",
                                            Value = TotalVisitors.ToString(),
                                        });

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Transactions",
                                            //Name = "Store Visits",
                                            Value = TotalVisits.ToString(),
                                        });
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantStore
                                        || OwnerDetails.AccountTypeId == UserAccountType.PosAccount)
                                    {

                                        long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                                        long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                          .Where(x => x.Account.UserId == UserAccount.UserId
                                                                                 && x.CreatedBy.OwnerId == OwnerDetails.OwnerId
                                                                                 && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.AccountId).Distinct().Count();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Visits",
                                            Value = TotalVisitors.ToString(),
                                        });

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Transactions",
                                            Value = TotalVisits.ToString(),
                                        });
                                    }
                                    else if (OwnerDetails.AccountTypeId == UserAccountType.MerchantCashier
                                             || OwnerDetails.AccountTypeId == UserAccountType.Terminal
                                             || OwnerDetails.AccountTypeId == UserAccountType.PgAccount)
                                    {

                                        long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && x.CreatedById == OwnerDetails.OwnerId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                                        long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                          .Where(x => x.Account.UserId == UserAccount.UserId
                                                                                 && x.CreatedById == OwnerDetails.OwnerId
                                                                                 && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.AccountId).Distinct().Count();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Visits",
                                            Value = TotalVisitors.ToString(),
                                        });

                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = "Transactions",
                                            Value = TotalVisits.ToString(),
                                        });
                                    }
                                }
                                else
                                {
                                    long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.UserId == UserAccount.UserId
                                                   && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                                    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                      .Where(x => x.Account.UserId == UserAccount.UserId
                                                                             && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime).Select(x => x.AccountId).Distinct().Count();
                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Visits",
                                        Value = TotalVisitors.ToString(),
                                    });

                                    _OChartRequestRangeValue.Add(new OChart.RequestData
                                    {
                                        Name = "Transactions",
                                        Value = TotalVisits.ToString(),
                                    });
                                }
                            }
                            else
                            {
                                long TotalVisits = _HCoreContext.HCUAccountTransaction
                                           .Where(x => x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                  && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                  && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                                long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                                  .Where(x => x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                         && (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                           && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Visitors",
                                    Value = TotalVisitors.ToString(),
                                });

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = "Transactions",
                                    Value = TotalVisits.ToString(),
                                });
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    if (_Request.Type == ListType.Rewards)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                 .Select(x => new
                                 {
                                     ReferenceId = x.ReferenceId,
                                     Name = x.Name
                                 }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            foreach (var HelperType in HelperTypes)
                            {
                                double Amount = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.TypeId == HelperType.ReferenceId
                                       && x.ModeId == TransactionMode.Credit
                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                       && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Amount.ToString(),
                                });
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }

                    }
                    else if (_Request.Type == ListType.Redeems)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            foreach (var HelperType in HelperTypes)
                            {
                                double Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.TypeId == HelperType.ReferenceId
                                   && x.ModeId == TransactionMode.Debit
                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Amount.ToString(),
                                });
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.Payments)
                    {
                        var HelperTypes = TransactionTypes.Where(x => x.Value == TransactionSource.PaymentsS)
                               .Select(x => new
                               {
                                   ReferenceId = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            foreach (var HelperType in HelperTypes)
                            {
                                double Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.TypeId == HelperType.ReferenceId
                                   && x.ModeId == TransactionMode.Debit
                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                   && (x.Account.AccountTypeId == UserAccountType.Appuser))
                                    .Sum(x => (double?)x.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Amount.ToString(),
                                });
                            }
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.AppUsers)
                    {
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            long MobileUser = _HCoreContext.HCUAccount
                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                           && x.CreateDate > _RangeItem.StartTime
                                           && x.CreateDate < _RangeItem.EndTime)
                                    .Count();
                            long CardUser = 0;
                            long Value = MobileUser + CardUser;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Users",
                                Value = Value.ToString(),
                            });
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else if (_Request.Type == ListType.Visitors)
                    {
                        foreach (var _RangeItem in _Request.DateRange)
                        {
                            _OChartRequestRangeValue = new List<OChart.RequestData>();
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                            .Where(x => (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                   && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                   && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                              .Where(x => (x.Type.Value == TransactionTypeValue.Redeem || x.Type.Value == TransactionTypeValue.Reward)
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                                     && (x.Account.AccountTypeId == UserAccountType.Appuser)).Select(x => x.Account.UserId).Distinct().Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Visitors",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",
                                //Name = "Store Visits",
                                Value = TotalVisits.ToString(),
                            });
                            #region Add Values To Range
                            _OChartRequestRange.Add(new OChart.RequestDateRange
                            {
                                EndTime = _RangeItem.EndTime,
                                StartTime = _RangeItem.StartTime,
                                Label = _RangeItem.Label,
                                Data = _OChartRequestRangeValue
                            });
                            #endregion
                        }
                    }
                    else
                    {

                    }
                }
                #region Build Response
                _OChartRequest.DateRange = _OChartRequestRange;
                _OChartRequest.StartTime = _Request.StartTime;
                _OChartRequest.EndTime = _Request.EndTime;
                _OChartRequest.Type = _Request.Type;
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                #endregion
            }
        }

    }
}
