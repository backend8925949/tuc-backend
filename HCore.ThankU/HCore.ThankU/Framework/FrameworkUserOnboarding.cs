//==================================================================================
// FileName: FrameworkUserOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user onboarding
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.ThankU.Framework
{
    public class FrameworkUserOnboarding
    {
        HCoreContext _HCoreContext;
        HCUAccountParameter _HCUAccountParameter;

        /// <summary>
        /// Description: Saves the account configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveAccountConfiguration(OAccountConfiguration _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.UserAccountKey)
                        .Select(x => new
                        {
                            Id = x.Id,
                            StatusId = x.StatusId,
                        }).FirstOrDefault();

                    var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                    x.SystemName == _Request.ConfigurationKey
                    && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                    && x.TypeId == HCoreConstant.HelperType.Configuration
                    ).Select(x => new
                    {
                        SystemName = x.SystemName,
                        HelperId = x.HelperId,
                        Value = x.Value,
                        ConfigurationId = x.Id,
                    }).FirstOrDefault();

                    if (ConfigurationDetails != null)
                    {
                        if (UserAccountDetails.StatusId != HCoreConstant.HelperStatus.Default.Inactive)
                        {
                            var UserConfigurations = _HCoreContext.HCUAccountParameter
                                                    .Where(x =>
                                                    x.TypeId == HCoreConstant.HelperType.ConfigurationValue
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.AccountId == UserAccountDetails.Id
                                                    && x.CommonId == ConfigurationDetails.ConfigurationId
                                                    ).ToList();
                            if (UserConfigurations.Count > 0)
                            {
                                foreach (var UserConfiguration in UserConfigurations)
                                {
                                    UserConfiguration.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                                    UserConfiguration.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserConfiguration.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                if (ConfigurationDetails.SystemName == "rewardpercentage")
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        Account.AccountPercentage = Convert.ToDouble(_Request.Value);
                                    }
                                }
                                int HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.HelperCode).Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                _HCUAccountParameter.Value = _Request.Value;
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (HelperId != 0)
                                {
                                    _HCUAccountParameter.HelperId = HelperId;
                                }
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.StatusId = StatusActive;
                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC001", "Configuration Updated Successfully");
                                #endregion
                            }
                        }
                        else
                        {
                            var UserConfiguration = _HCoreContext.HCUAccountParameter.Where(x =>
                         x.TypeId == HCoreConstant.HelperType.ConfigurationValue
                         && x.AccountId == UserAccountDetails.Id
                         && x.CommonId == ConfigurationDetails.ConfigurationId
                        ).FirstOrDefault();
                            if (UserConfiguration != null)
                            {
                                if (ConfigurationDetails.SystemName == "rewardpercentage")
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        Account.AccountPercentage = Convert.ToDouble(_Request.Value);
                                    }
                                }
                                int HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.HelperCode).Select(x => x.Id).FirstOrDefault();
                                UserConfiguration.CommonId = ConfigurationDetails.ConfigurationId;
                                UserConfiguration.Value = _Request.Value;
                                UserConfiguration.ModifyDate = HCoreHelper.GetGMTDateTime();
                                if (HelperId != 0)
                                {
                                    UserConfiguration.HelperId = HelperId;
                                }
                                UserConfiguration.ModifyById = _Request.UserReference.AccountId;
                                UserConfiguration.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC001", "Configuration Updated Successfully");
                                #endregion
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (ConfigurationDetails.SystemName == "rewardpercentage")
                                    {
                                        var Account = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).FirstOrDefault();
                                        if (Account != null)
                                        {
                                            Account.AccountPercentage = Convert.ToDouble(_Request.Value);
                                        }
                                    }
                                    int HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.HelperCode).Select(x => x.Id).FirstOrDefault();
                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                    _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                    _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                    _HCUAccountParameter.Value = _Request.Value;
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    if (HelperId != 0)
                                    {
                                        _HCUAccountParameter.HelperId = HelperId;
                                    }
                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountParameter.StatusId = StatusActive;
                                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    _HCoreContext.SaveChanges();
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC001", "Configuration Updated Successfully");
                                    #endregion
                                }
                            }

                            //    var UserConfigurations = _HCoreContext.HCUAccountParameter.Where(x =>
                            // x.TypeId == HCoreConstant.HelperType.ConfigurationValue
                            // && x.StatusId == HelperStatus.Default.Active
                            // && x.UserAccountId == UserAccountId
                            // && x.CommonId == ConfigurationDetails.ConfigurationId
                            //).ToList();
                            //if (UserConfigurations.Count > 0)
                            //{
                            //    foreach (var UserConfiguration in UserConfigurations)
                            //    {
                            //        UserConfiguration.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                            //        UserConfiguration.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        UserConfiguration.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //    }
                            //}

                        }

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC002", "Details not found");
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantImportList", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000", "Internal server error occured. Please try after some time");
                #endregion
            }
            #endregion
        }

    }
}
