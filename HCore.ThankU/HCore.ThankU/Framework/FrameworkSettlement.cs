//==================================================================================
// FileName: FrameworkSettlement.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to settlement
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using Akka.Actor;
//using HCore.ThankU.Core;
using HCore.Operations.Object;
using HCore.Operations;

namespace HCore.ThankU.Framework
{
    public class FrameworkSettlement
    {

        OCoreTransaction.Request _CoreTransactionRequest;
        //OThankUGateway.Response _GatewayResponse;
        //OUserInfo _OUserInfo;
        //OAppProfile.Request _AppProfileRequest;
        //OThankUGateway.Request _GatewayRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;

        #region References
        ManageUserTransaction _ManageUserTransaction;
        ManageConfiguration _ManageConfiguration;
        #endregion
        #region Entities
        HCoreContext _HCoreContext;
        HCUAccountInvoice _HCUserInvoice;
        #endregion
        #region Objects
        //OSaveTransactions _OSaveTransactions;
        OSaveTransaction _OSaveTransaction;
        OUserSettlement.Overview _OOverview;
        OUserSettlement.OverviewSettlement _OOverviewSettlements;
        OUserSettlement.OverviewInvoices _OOverviewInvoices;
        OUserSettlement.OverviewSettlementAccountType _OOverviewSettlementAccountType;
        //List<OTransactionItem> _TransactionItems;
        #endregion
        //internal void CreateRedeemSettlementInvoices()
        //{
        //    #region TransactionPostProcess
        //    var _Actor = ActorSystem.Create("ActorProductBatchCodeProcess");
        //    var _ActorNotify = _Actor.ActorOf<ProcessRedeemSettlementInvoices>("ActorProductBatchCodeProcess");
        //    _ActorNotify.Tell("callssytem");
        //    #endregion
        //}
        /// <summary>
        /// Description: Creates the reward settlement invoices.
        /// </summary>
        internal void CreateRewardSettlementInvoices()
        {
            //#region TransactionPostProcess
            //var _ActorRewardInvoice = ActorSystem.Create("ProcessRewardSettlementInvoices");
            //var _ActorRewardInvoiceNotify = _ActorRewardInvoice.ActorOf<ProcessRewardSettlementInvoices>("ProcessRewardSettlementInvoices");
            //_ActorRewardInvoiceNotify.Tell("callssytem");
            //#endregion
        }
        /// <summary>
        /// Description: Processes the transaction bins.
        /// </summary>
        internal void ProcessTransactionBins()
        {
            //#region TransactionPostProcess
            //var _ActorRewardInvoice = ActorSystem.Create("ProcessTransactionBins");
            //var _ActorRewardInvoiceNotify = _ActorRewardInvoice.ActorOf<ProcessTransactionBins>("ProcessTransactionBins");
            //_ActorRewardInvoiceNotify.Tell("callssytem");
            //#endregion
        }



        // HCUAccountInvoiceItem _HCUAccountInvoiceItem;
        //List<HCUAccountInvoiceItem> _HCUAccountInvoiceItems;

        //internal void ProcessFullRewardSettlementInvoices()
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        #region Declare
        //        _ManageConfiguration = new ManageConfiguration();
        //        #endregion
        //        #region Code Block
        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var UserAccounts = _HCoreContext.HCUAccount.Where(x =>
        //                x.Id != 3
        //                && x.Id != 971
        //                && x.Id != 49751
        //                && x.AccountTypeId == UserAccountType.Merchant)
        //            .Select(x => new
        //            {
        //                UserAccountId = x.Id,
        //                Name = x.Name,
        //                EmailAddress = x.EmailAddress,
        //                ContactNumber = x.ContactNumber,
        //                Address = x.Address,
        //                CreateDate = x.CreateDate,
        //            }).ToList();
        //            foreach (var AccountDetails in UserAccounts)
        //            {
        //                DateTime TransactionStartDate = HCoreHelper.GetNigeriaToGMT(AccountDetails.CreateDate.Date).AddSeconds(-1);
        //                DateTime TransactionEndDate = TransactionStartDate.AddDays(1);

        //                DateTime InvoiceDate = AccountDetails.CreateDate.Date;
        //                DateTime InvoiceStartDate = InvoiceDate;
        //                DateTime InvoiceEndDate = InvoiceDate.AddDays(1).AddSeconds(-1);
        //                int DaysDifference = (HCoreHelper.GetGMTDateTime() - AccountDetails.CreateDate).Days + 10;
        //                for (int i = 0; i < DaysDifference; i++)
        //                {
        //                    int TDaysDifference = (HCoreHelper.GetGMTDateTime() - InvoiceDate).Days;
        //                    if (TDaysDifference > 0)
        //                    {
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            // Reward Deduction Type
        //                            string IsThankUCashEnable = HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId);
        //                            OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", AccountDetails.UserAccountId);
        //                            OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", AccountDetails.UserAccountId);
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
        //                                {
        //                                    // Commission Amount Prepaid And Reward Amount Postpaid

        //                                    // Charge invoice
        //                                    int InvoiceStatus = StatusHelperId.Invoice_Pending;
        //                                    List<long> TransactonList;
        //                                    long TotalChargeTransactions = 0;
        //                                    double TotalChargeAmount = 0;
        //                                    if (IsThankUCashEnable == "1")
        //                                    {
        //                                        TransactonList = _HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                              && x.TransactionDate > TransactionStartDate
        //                                                              && x.TransactionDate < TransactionEndDate
        //                                                              && x.SourceId == TransactionSource.Merchant
        //                                                              && x.ModeId == TransactionMode.Debit
        //                                                              //&& x.InvoiceId == null
        //                                                              && !x.HCUAccountInvoiceItem.Any()
        //                                                              && x.StatusId == StatusHelperId.Transaction_Success).Select(x => x.Id).ToList();

        //                                        TotalChargeTransactions = _HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                               && x.TransactionDate > TransactionStartDate
        //                                                               && x.TransactionDate < TransactionEndDate
        //                                                               && x.SourceId == TransactionSource.Merchant
        //                                                               && x.ModeId == TransactionMode.Debit
        //                                                               //&& x.InvoiceId == null
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                               && x.StatusId == StatusHelperId.Transaction_Success).Count();

        //                                        TotalChargeAmount = _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                       && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                                       && x.SourceId == TransactionSource.Merchant
        //                                                                       && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                       && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Sum(x => (double?)x.ComissionAmount) ?? 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        TransactonList = _HCoreContext.HCUAccountTransaction
        //                                                      .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                             && x.TransactionDate > TransactionStartDate
        //                                                             && x.TransactionDate < TransactionEndDate
        //                                                             && x.SourceId == TransactionSource.Merchant
        //                                                             && x.ModeId == TransactionMode.Debit
        //                                                             //&& x.InvoiceId == null
        //                                                             && !x.HCUAccountInvoiceItem.Any()
        //                                                             && x.StatusId == StatusHelperId.Transaction_Success).Select(x => x.Id).ToList();

        //                                        TotalChargeTransactions = _HCoreContext.HCUAccountTransaction
        //                                                      .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                             && x.TransactionDate > TransactionStartDate
        //                                                             && x.TransactionDate < TransactionEndDate
        //                                                             && x.SourceId == TransactionSource.Merchant
        //                                                             && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                             && x.StatusId == StatusHelperId.Transaction_Success).Count();

        //                                        TotalChargeAmount = _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                       && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                                       && x.SourceId == TransactionSource.Merchant
        //                                                                       && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                       && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Sum(x => (double?)x.ComissionAmount) ?? 0;
        //                                    }
        //                                    if (TotalChargeTransactions > 0)
        //                                    {
        //                                        #region Create Invoice
        //                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                        _HCUserInvoice.TypeId = InvoiceType.RewardCommissionInvoice;
        //                                        _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                        _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                        _HCUserInvoice.Name = "Convenience Charges Invoice ";
        //                                        _HCUserInvoice.FromName = "Thank U Cash";
        //                                        _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                        _HCUserInvoice.FromContactNumber = "07002000000";
        //                                        _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                        _HCUserInvoice.ToName = AccountDetails.Name;
        //                                        _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                        _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                        _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                        _HCUserInvoice.TotalItem = TotalChargeTransactions;
        //                                        _HCUserInvoice.Amount = TotalChargeAmount;
        //                                        _HCUserInvoice.ChargePercentage = 0;
        //                                        _HCUserInvoice.Charge = 0;
        //                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                        _HCUserInvoice.DiscountAmount = 0;
        //                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                        _HCUserInvoice.ComissionAmount = 0;
        //                                        _HCUserInvoice.TotalAmount = TotalChargeAmount;
        //                                        _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                        _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                        _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                        _HCUserInvoice.CreatedById = 2;
        //                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                        _HCoreContext.SaveChanges();
        //                                        long InvoiceId = _HCUserInvoice.Id;
        //                                        #endregion
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var Trans = _HCoreContext.HCUAccountTransaction
        //                                                     .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.Merchant
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && !x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success
        //                                                           ).Select(x=>x.Id).ToList();
        //                                            if (Trans.Count > 0)
        //                                            {
        //                                                foreach (var TranItem in Trans)
        //                                                {
        //                                                    _HCUAccountInvoiceItem = new HCUAccountInvoiceItem();
        //                                                    _HCUAccountInvoiceItem.TransactionId = TranItem;
        //                                                    _HCUAccountInvoiceItem.InvoiceId = InvoiceId;
        //                                                    _HCoreContext.HCUAccountInvoiceItem.Add(_HCUAccountInvoiceItem);
        //                                                }
        //                                                _HCoreContext.SaveChanges();
        //                                            }


        //                                            //_HCoreContext.HCUAccountTransaction
        //                                            //         .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                            //                && x.TransactionDate > TransactionStartDate
        //                                            //                && x.TransactionDate < TransactionEndDate
        //                                            //                && x.SourceId == TransactionSource.Merchant
        //                                            //                && x.ModeId == TransactionMode.Debit
        //                                            //                   && ! x.HCUAccountInvoiceItem.Any()
        //                                            //                && x.StatusId == StatusHelperId.Transaction_Success
        //                                            //               )
        //                                            //             .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                        }
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long Total = _HCoreContext.HCUAccountTransaction
        //                                                                               .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                      && x.TransactionDate > TransactionStartDate
        //                                                                                      && x.TransactionDate < TransactionEndDate
        //                                                                                      && x.SourceId == TransactionSource.Merchant
        //                                                                                      && x.ModeId == TransactionMode.Debit
        //                                                                                      && x.InvoiceId == InvoiceId
        //                                                                                      && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                    .Count();
        //                                            if (Total > 0)
        //                                            {
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                _HCUserInvoice.ParentId = InvoiceId;
        //                                                _HCUserInvoice.Name = "Convenience Charges";
        //                                                _HCUserInvoice.TotalItem = Total;
        //                                                _HCUserInvoice.Amount = TotalChargeAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = TotalChargeAmount;
        //                                                _HCUserInvoice.UnitCost = 0;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCUserInvoice.StatusId = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceItemId = _HCUserInvoice.Id;
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                && x.TransactionDate > TransactionStartDate
        //                                                && x.TransactionDate < TransactionEndDate
        //                                                && x.SourceId == TransactionSource.Merchant
        //                                                && x.ModeId == TransactionMode.Debit
        //                                                && x.InvoiceId == InvoiceId
        //                                                && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        #region Add Settlement Transaction
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                InvoiceDetails.StatusId = InvoiceStatus;
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCoreContext.SaveChanges();

        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }

        //                                    // Reward Invoice
        //                                    if (IsThankUCashEnable == "1")
        //                                    {
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long RewardTransactions = _HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                              .Count();
        //                                            if (RewardTransactions > 0)
        //                                            {
        //                                                double RewardAmount = _HCoreContext.HCUAccountTransaction
        //                                                   .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                         && x.TransactionDate < TransactionEndDate
        //                                                          && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                          && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                          && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                   .Sum(x => (double?)x.Amount) ?? 0;
        //                                                if (RewardTransactions > 0)
        //                                                {
        //                                                    RewardAmount = Math.Round(RewardAmount, 3);
        //                                                    #region Create Invoice
        //                                                    _HCUserInvoice = new HCUAccountInvoice();
        //                                                    _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                    _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                                    _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                                    _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                                    _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                                    _HCUserInvoice.FromName = "Thank U Cash";
        //                                                    _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                                    _HCUserInvoice.FromContactNumber = "07002000000";
        //                                                    _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                                    _HCUserInvoice.ToName = AccountDetails.Name;
        //                                                    _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                                    _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                                    _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                                    _HCUserInvoice.TotalItem = RewardTransactions;
        //                                                    _HCUserInvoice.Amount = RewardAmount;
        //                                                    _HCUserInvoice.ChargePercentage = 0;
        //                                                    _HCUserInvoice.Charge = 0;
        //                                                    _HCUserInvoice.DiscountPercentage = 0;
        //                                                    _HCUserInvoice.DiscountAmount = 0;
        //                                                    _HCUserInvoice.ComissionPercentage = 0;
        //                                                    _HCUserInvoice.ComissionAmount = 0;
        //                                                    _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                    _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                                    _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                                    _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                                    _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                    _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                                    _HCUserInvoice.CreatedById = 2;
        //                                                    _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                    _HCoreContext.SaveChanges();
        //                                                    long InvoiceId = _HCUserInvoice.Id;
        //                                                    #endregion
        //                                                    #region Set Invoice Id
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x =>
        //                                                                         x.ParentId == AccountDetails.UserAccountId
        //                                                                        && x.TransactionDate > TransactionStartDate
        //                                                                        && x.TransactionDate < TransactionEndDate
        //                                                                        && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                                        && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                        && x.StatusId == StatusHelperId.Transaction_Success
        //                                                                       ).Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                                    }
        //                                                    #endregion
        //                                                    #region Set invoices items
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                           .Where(x => x.InvoiceId == InvoiceId)
        //                                                                                           .Count();
        //                                                        if (Total > 0)
        //                                                        {
        //                                                            _HCUserInvoice = new HCUAccountInvoice();
        //                                                            _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                            _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                            _HCUserInvoice.ParentId = InvoiceId;
        //                                                            _HCUserInvoice.Name = "Claimed Rewards";
        //                                                            _HCUserInvoice.TotalItem = Total;
        //                                                            _HCUserInvoice.Amount = RewardAmount;
        //                                                            _HCUserInvoice.ChargePercentage = 0;
        //                                                            _HCUserInvoice.Charge = 0;
        //                                                            _HCUserInvoice.DiscountPercentage = 0;
        //                                                            _HCUserInvoice.DiscountAmount = 0;
        //                                                            _HCUserInvoice.ComissionPercentage = 0;
        //                                                            _HCUserInvoice.ComissionAmount = 0;
        //                                                            _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                            _HCUserInvoice.UnitCost = 0;
        //                                                            _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                            _HCUserInvoice.CreatedById = 2;
        //                                                            _HCUserInvoice.StatusId = 2;
        //                                                            _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                            _HCoreContext.SaveChanges();
        //                                                            long InvoiceItemId = _HCUserInvoice.Id;
        //                                                            using (_HCoreContext = new HCoreContext())
        //                                                            {
        //                                                                _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.InvoiceId == InvoiceId)
        //                                                                .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    #endregion
        //                                                    #region Add Settlement Transaction
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                                        if (InvoiceDetails != null)
        //                                                        {
        //                                                            InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                            InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                            InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                            _HCoreContext.SaveChanges();
        //                                                        }
        //                                                    }
        //                                                    #endregion
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long RewardTransactions = _HCoreContext.HCUAccountTransaction
        //                                               .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                     && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                      && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                      && x.SourceId == TransactionSource.TUC
        //                                                      && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                      && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                            if (RewardTransactions > 0)
        //                                            {
        //                                                double RewardAmount = (_HCoreContext.HCUAccountTransaction
        //                                                  .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                         && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                         && x.SourceId == TransactionSource.TUC
        //                                                         && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                         && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                  .Sum(x => (double?)x.Amount) ?? 0);
        //                                                RewardAmount = Math.Round(RewardAmount, 3);

        //                                                #region Create Invoice
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                                _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                                _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                                _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                                _HCUserInvoice.FromName = "Thank U Cash";
        //                                                _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                                _HCUserInvoice.FromContactNumber = "07002000000";
        //                                                _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                                _HCUserInvoice.ToName = AccountDetails.Name;
        //                                                _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                                _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                                _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                                _HCUserInvoice.TotalItem = RewardTransactions;
        //                                                _HCUserInvoice.Amount = RewardAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                                _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                                _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceId = _HCUserInvoice.Id;
        //                                                #endregion
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                             .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                       && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                      && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                      && x.SourceId == TransactionSource.TUC
        //                                                      && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                      && x.StatusId == StatusHelperId.Transaction_Success
        //                                                                   )
        //                                                                 .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                                }
        //                                                #region Add Settlement Transaction
        //                                                #region Set invoices items
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                                                              && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                                                              && x.SourceId == TransactionSource.TUC
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Count();
        //                                                    if (Total > 0)
        //                                                    {
        //                                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                        _HCUserInvoice.ParentId = InvoiceId;
        //                                                        _HCUserInvoice.Name = "Claimed Rewards";
        //                                                        _HCUserInvoice.TotalItem = Total;
        //                                                        _HCUserInvoice.Amount = RewardAmount;
        //                                                        _HCUserInvoice.ChargePercentage = 0;
        //                                                        _HCUserInvoice.Charge = 0;
        //                                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                                        _HCUserInvoice.DiscountAmount = 0;
        //                                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                                        _HCUserInvoice.ComissionAmount = 0;
        //                                                        _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                        _HCUserInvoice.UnitCost = 0;
        //                                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                        _HCUserInvoice.CreatedById = 2;
        //                                                        _HCUserInvoice.StatusId = 2;
        //                                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                        _HCoreContext.SaveChanges();
        //                                                        long InvoiceItemId = _HCUserInvoice.Id;
        //                                                        using (_HCoreContext = new HCoreContext())
        //                                                        {
        //                                                            _HCoreContext.HCUAccountTransaction
        //                                                            .Where(x => x.InvoiceId == InvoiceId)
        //                                                        .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                        }
        //                                                    }
        //                                                }
        //                                                #endregion
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                                    if (InvoiceDetails != null)
        //                                                    {
        //                                                        InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                        InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                        _HCoreContext.SaveChanges();
        //                                                    }
        //                                                }
        //                                                #endregion
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    // All Amounts Postpaid or direct debit
        //                                    long MerchantPendingInvoiceTransactions = _HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                               && x.TransactionDate > TransactionStartDate
        //                                                               && x.TransactionDate < TransactionEndDate
        //                                                               && x.SourceId == TransactionSource.Merchant
        //                                                               && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                               && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                    if (MerchantPendingInvoiceTransactions > 0)
        //                                    {
        //                                        double MerchantPendingInvoiceAmount = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                           && x.TransactionDate > TransactionStartDate
        //                                                                           && x.TransactionDate < TransactionEndDate
        //                                                                           && x.SourceId == TransactionSource.Merchant
        //                                                                           && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                           && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
        //                                        MerchantPendingInvoiceAmount = Math.Round(MerchantPendingInvoiceAmount, 3);
        //                                        int InvoiceStatus = StatusHelperId.Invoice_Pending;

        //                                        #region Create Invoice
        //                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                        _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                        _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber(5);
        //                                        _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                        _HCUserInvoice.FromName = "Thank U Cash";
        //                                        _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                        _HCUserInvoice.FromContactNumber = "07002000000";
        //                                        _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                        _HCUserInvoice.ToName = AccountDetails.Name;
        //                                        _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                        _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                        _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                        _HCUserInvoice.TotalItem = MerchantPendingInvoiceTransactions;
        //                                        _HCUserInvoice.Amount = MerchantPendingInvoiceAmount;
        //                                        _HCUserInvoice.ChargePercentage = 0;
        //                                        _HCUserInvoice.Charge = 0;
        //                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                        _HCUserInvoice.DiscountAmount = 0;
        //                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                        _HCUserInvoice.ComissionAmount = 0;
        //                                        _HCUserInvoice.TotalAmount = MerchantPendingInvoiceAmount;
        //                                        _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                        _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                        _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                        _HCUserInvoice.CreatedById = 2;
        //                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                        _HCoreContext.SaveChanges();
        //                                        long InvoiceId = _HCUserInvoice.Id;
        //                                        #endregion
        //                                        #region Update Invoice Id To Transactions
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            _HCoreContext.HCUAccountTransaction
        //                                                     .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.Merchant
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success
        //                                                           )
        //                                                         .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                        }
        //                                        #endregion
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long Total = _HCoreContext.HCUAccountTransaction
        //                                                                               .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                        && x.TransactionDate > TransactionStartDate
        //                                                                                        && x.TransactionDate < TransactionEndDate
        //                                                                                      && x.SourceId == TransactionSource.Merchant
        //                                                                                      && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                                      && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                    .Count();
        //                                            if (Total > 0)
        //                                            {
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                _HCUserInvoice.ParentId = InvoiceId;
        //                                                _HCUserInvoice.Name = "User Rewards";
        //                                                _HCUserInvoice.TotalItem = Total;
        //                                                _HCUserInvoice.Amount = MerchantPendingInvoiceAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = MerchantPendingInvoiceAmount;
        //                                                _HCUserInvoice.UnitCost = 0;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCUserInvoice.StatusId = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceItemId = _HCUserInvoice.Id;
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                    && x.TransactionDate > TransactionStartDate
        //                                                    && x.TransactionDate < TransactionEndDate
        //                                                    && x.InvoiceId == InvoiceId
        //                                                    && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                    .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        #region Set Invoice Status
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                InvoiceDetails.StatusId = InvoiceStatus;
        //                                                _HCoreContext.SaveChanges();
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                }
        //                            }
        //                        }


        //                        using (_HCoreContext = new HCoreContext())
        //                        {


        //                            double UserSettlementTransactionAmount = (_HCoreContext.HCUAccountTransaction
        //                                                  .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                         && x.TransactionDate < TransactionEndDate
        //                                                         && x.SourceId == TransactionSource.Settlement
        //                                                         && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                         && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                  .Sum(x => (double?)x.TotalAmount) ?? 0);
        //                            if (UserSettlementTransactionAmount > 0)
        //                            {

        //                                long UserSettlementPendingTransactions = _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                        && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                if (UserSettlementPendingTransactions > 0)
        //                                {
        //                                    UserSettlementTransactionAmount = Math.Round(UserSettlementTransactionAmount, 3);
        //                                    #region Create Invoice
        //                                    _HCUserInvoice = new HCUAccountInvoice();
        //                                    _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                    _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.Payment;
        //                                    _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                    _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                    _HCUserInvoice.Name = "Redeem & Commission Invoice";
        //                                    _HCUserInvoice.FromName = "Thank U Cash";
        //                                    _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                    _HCUserInvoice.FromContactNumber = "07002000000";
        //                                    _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                    _HCUserInvoice.ToName = AccountDetails.Name;
        //                                    _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                    _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                    _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                    _HCUserInvoice.TotalItem = UserSettlementPendingTransactions;
        //                                    _HCUserInvoice.Amount = 0;
        //                                    _HCUserInvoice.ChargePercentage = 0;
        //                                    _HCUserInvoice.Charge = 0;
        //                                    _HCUserInvoice.DiscountPercentage = 0;
        //                                    _HCUserInvoice.DiscountAmount = 0;
        //                                    _HCUserInvoice.ComissionPercentage = 0;
        //                                    _HCUserInvoice.ComissionAmount = 0;
        //                                    _HCUserInvoice.TotalAmount = 0;
        //                                    _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                    _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                    _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                    _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                    _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                    _HCUserInvoice.CreatedById = 2;
        //                                    _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                    _HCoreContext.SaveChanges();
        //                                    long InvoiceId = _HCUserInvoice.Id;
        //                                    double SettlementAmount = 0;
        //                                    #endregion
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success
        //                                                       )
        //                                                     .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                    }

        //                                    #region Add Settlement Transaction
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        SettlementAmount = _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                        && x.InvoiceId == InvoiceId
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                              .Sum(x => (double?)x.TotalAmount) ?? 0;
        //                                        #region Add Merchant Settlement Debit transaction
        //                                        _OSaveTransaction = new OSaveTransaction();
        //                                        _OSaveTransaction.UserAccountId = AccountDetails.UserAccountId;
        //                                        _OSaveTransaction.ModeId = TransactionMode.Debit;
        //                                        _OSaveTransaction.TypeId = TransactionType.BankSettlement;
        //                                        _OSaveTransaction.SourceId = TransactionSource.Settlement;
        //                                        _OSaveTransaction.Amount = SettlementAmount;
        //                                        _OSaveTransaction.AmountPercentage = 100;
        //                                        _OSaveTransaction.Charge = 0;
        //                                        _OSaveTransaction.ChargePercentage = 0;
        //                                        _OSaveTransaction.ComissionAmount = 0;
        //                                        _OSaveTransaction.ComissionPercentage = 0;
        //                                        _OSaveTransaction.PayableAmount = SettlementAmount;
        //                                        _OSaveTransaction.TotalAmount = SettlementAmount;
        //                                        _OSaveTransaction.TotalAmountPercentage = 100;
        //                                        _OSaveTransaction.PurchaseAmount = SettlementAmount;
        //                                        _OSaveTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                        _OSaveTransaction.ParentId = AccountDetails.UserAccountId;
        //                                        _OSaveTransaction.ReferenceNumber = InvoiceId.ToString();
        //                                        _OSaveTransaction.Status = StatusHelperId.Transaction_Success;
        //                                        _ManageUserTransaction = new ManageUserTransaction();
        //                                        OTransactionReference TrDebitPgAccountResponse = _ManageUserTransaction.SaveTransaction(_OSaveTransaction);
        //                                        #endregion
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var SystemHelpers = _HCoreContext.HCCore
        //                                                                             .Where(x => x.Parent.SystemName == "hcore.transactiontype" && x.StatusId == StatusHelperId.Default_Active)
        //                                                                             .Select(x => new
        //                                                                             {
        //                                                                                 Id = x.Id,
        //                                                                                 Name = x.Name
        //                                                                             }).ToList();
        //                                            foreach (var SystemHelper in SystemHelpers)
        //                                            {
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                              && x.TransactionDate > TransactionStartDate
        //                                                                                              && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.SourceId == TransactionSource.Settlement
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                                                              && x.TypeId == SystemHelper.Id
        //                                                                                              && x.InvoiceId == InvoiceId
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Count();
        //                                                    if (Total > 0)
        //                                                    {
        //                                                        double TotalAmount = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                               && x.TransactionDate > TransactionStartDate
        //                                                                                                && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.SourceId == TransactionSource.Settlement
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                                                              && x.TypeId == SystemHelper.Id
        //                                                                                              && x.InvoiceId == InvoiceId
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Sum(x => x.TotalAmount);

        //                                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                        _HCUserInvoice.ParentId = InvoiceId;
        //                                                        _HCUserInvoice.Name = SystemHelper.Name;
        //                                                        _HCUserInvoice.TotalItem = Total;
        //                                                        _HCUserInvoice.Amount = Math.Round(TotalAmount, 2);
        //                                                        _HCUserInvoice.ChargePercentage = 0;
        //                                                        _HCUserInvoice.Charge = 0;
        //                                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                                        _HCUserInvoice.DiscountAmount = 0;
        //                                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                                        _HCUserInvoice.ComissionAmount = 0;
        //                                                        _HCUserInvoice.TotalAmount = _HCUserInvoice.Amount;
        //                                                        _HCUserInvoice.UnitCost = 0;
        //                                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                        _HCUserInvoice.CreatedById = 2;
        //                                                        _HCUserInvoice.StatusId = 2;
        //                                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                        _HCoreContext.SaveChanges();
        //                                                        long InvoiceItemId = _HCUserInvoice.Id;
        //                                                        using (_HCoreContext = new HCoreContext())
        //                                                        {
        //                                                            _HCoreContext.HCUAccountTransaction
        //                                                            .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                        && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                        && x.TypeId == SystemHelper.Id
        //                                                        && x.InvoiceId == InvoiceId
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            //StartDate = _HCoreContext.HCUAccountTransaction
        //                                            //.Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                            // && x.TransactionDate < SEndDate
        //                                            // && x.SourceId == TransactionSource.Settlement
        //                                            // && x.ModeId == TransactionMode.Credit
        //                                            // && x.InvoiceId == InvoiceId
        //                                            // && x.StatusId == StatusHelperId.Transaction_Success
        //                                            //).OrderBy(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                //InvoiceDetails.StartDate = StartDate;
        //                                                InvoiceDetails.Amount = SettlementAmount;
        //                                                InvoiceDetails.TotalAmount = SettlementAmount;
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                _HCoreContext.SaveChanges();
        //                                            }
        //                                        }
        //                                    }
        //                                    #endregion
        //                                }
        //                                else
        //                                {

        //                                }

        //                            }
        //                        }



        //                        TransactionStartDate = TransactionStartDate.AddDays(1);
        //                        TransactionEndDate = TransactionEndDate.AddDays(1);
        //                        InvoiceDate = InvoiceDate.AddDays(1);
        //                        InvoiceStartDate = InvoiceStartDate.AddDays(1);
        //                        InvoiceEndDate = InvoiceEndDate.AddDays(1);
        //                    }
        //                }
        //            }
        //        }
        //        #endregion
        //        #endregion
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException("CreatePaymentSettlementManual", _Exception);
        //        #endregion
        //    }
        //    #endregion
        //}
        //internal void ProcessRewardSettlementInvoices()
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        #region Declare
        //        _ManageConfiguration = new ManageConfiguration();
        //        #endregion
        //        #region Code Block
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var UserAccounts = _HCoreContext.HCUAccount.Where(x =>
        //                x.Id != 3
        //                && x.Id != 971
        //                && x.Id != 49751
        //                && x.AccountTypeId == UserAccountType.Merchant)
        //            .Select(x => new
        //            {
        //                UserAccountId = x.Id,
        //                Name = x.Name,
        //                EmailAddress = x.EmailAddress,
        //                ContactNumber = x.ContactNumber,
        //                Address = x.Address,
        //            }).ToList();
        //            _HCoreContext.Dispose();

        //            //DateTime TransactionStartDate = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddDays(-1).AddSeconds(-1);
        //            //DateTime TransactionEndDate = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate());
        //            //DateTime InvoiceDate = HCoreHelper.GetGMTDate().AddDays(-1);
        //            //DateTime InvoiceStartDate = HCoreHelper.GetGMTDate().AddDays(-1);
        //            //DateTime InvoiceEndDate = HCoreHelper.GetGMTDate().AddSeconds(-1);

        //            DateTime InvoiceDate = HCoreHelper.GetGMTDate().AddDays(-4);
        //            DateTime TransactionStartDate = HCoreHelper.GetNigeriaToGMT(InvoiceDate).AddSeconds(-1);
        //            DateTime TransactionEndDate = TransactionStartDate.AddDays(1).AddSeconds(1);
        //            DateTime InvoiceStartDate = InvoiceDate;
        //            DateTime InvoiceEndDate = InvoiceDate.AddDays(1).AddSeconds(-1);
        //            DateTime FinalDate = HCoreHelper.GetGMTDate();
        //            int DaysDifference = (FinalDate - InvoiceDate).Days;
        //            for (int i = 0; i < DaysDifference; i++)
        //            {
        //                if (InvoiceDate != FinalDate)
        //                {
        //                    foreach (var AccountDetails in UserAccounts)
        //                    {
        //                        // Reward Deduction Type
        //                        //string IsThankUCashEnable = HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId);
        //                        //OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", AccountDetails.UserAccountId);
        //                        //OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", AccountDetails.UserAccountId);
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            // Reward Deduction Type
        //                            string IsThankUCashEnable = HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId);
        //                            OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", AccountDetails.UserAccountId);
        //                            OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", AccountDetails.UserAccountId);
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
        //                                {
        //                                    // Commission Amount Prepaid And Reward Amount Postpaid
        //                                    // Charge invoice
        //                                    int InvoiceStatus = StatusHelperId.Invoice_Pending;
        //                                    long TotalChargeTransactions = 0;
        //                                    double TotalChargeAmount = 0;
        //                                    if (IsThankUCashEnable == "1")
        //                                    {
        //                                        TotalChargeTransactions = _HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                               && x.TransactionDate > TransactionStartDate
        //                                                               && x.TransactionDate < TransactionEndDate
        //                                                               && x.SourceId == TransactionSource.Merchant
        //                                                               && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                               && x.StatusId == StatusHelperId.Transaction_Success).Count();

        //                                        TotalChargeAmount = _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                       && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                                       && x.SourceId == TransactionSource.Merchant
        //                                                                       && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                       && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Sum(x => (double?)x.ComissionAmount) ?? 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        TotalChargeTransactions = _HCoreContext.HCUAccountTransaction
        //                                                      .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                             && x.TransactionDate > TransactionStartDate
        //                                                             && x.TransactionDate < TransactionEndDate
        //                                                             && x.SourceId == TransactionSource.Merchant
        //                                                             && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                             && x.StatusId == StatusHelperId.Transaction_Success).Count();

        //                                        TotalChargeAmount = _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                       && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                                       && x.SourceId == TransactionSource.Merchant
        //                                                                       && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                       && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Sum(x => (double?)x.ComissionAmount) ?? 0;
        //                                    }
        //                                    if (TotalChargeTransactions > 0)
        //                                    {
        //                                        #region Create Invoice
        //                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                        _HCUserInvoice.TypeId = InvoiceType.RewardCommissionInvoice;
        //                                        _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                        _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                        _HCUserInvoice.Name = "Convenience Charges Invoice ";
        //                                        _HCUserInvoice.FromName = "Thank U Cash";
        //                                        _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                        _HCUserInvoice.FromContactNumber = "07002000000";
        //                                        _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                        _HCUserInvoice.ToName = AccountDetails.Name;
        //                                        _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                        _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                        _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                        _HCUserInvoice.TotalItem = TotalChargeTransactions;
        //                                        _HCUserInvoice.Amount = TotalChargeAmount;
        //                                        _HCUserInvoice.ChargePercentage = 0;
        //                                        _HCUserInvoice.Charge = 0;
        //                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                        _HCUserInvoice.DiscountAmount = 0;
        //                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                        _HCUserInvoice.ComissionAmount = 0;
        //                                        _HCUserInvoice.TotalAmount = TotalChargeAmount;
        //                                        _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                        _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                        _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                        _HCUserInvoice.CreatedById = 2;
        //                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                        _HCoreContext.SaveChanges();
        //                                        long InvoiceId = _HCUserInvoice.Id;
        //                                        #endregion
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            _HCoreContext.HCUAccountTransaction
        //                                                     .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.Merchant
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success
        //                                                           )
        //                                                         .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                        }
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long Total = _HCoreContext.HCUAccountTransaction
        //                                                                               .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                      && x.TransactionDate > TransactionStartDate
        //                                                                                      && x.TransactionDate < TransactionEndDate
        //                                                                                      && x.SourceId == TransactionSource.Merchant
        //                                                                                      && x.ModeId == TransactionMode.Debit
        //                                                                                      && x.InvoiceId == InvoiceId
        //                                                                                      && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                    .Count();
        //                                            if (Total > 0)
        //                                            {
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                _HCUserInvoice.ParentId = InvoiceId;
        //                                                _HCUserInvoice.Name = "Convenience Charges";
        //                                                _HCUserInvoice.TotalItem = Total;
        //                                                _HCUserInvoice.Amount = TotalChargeAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = TotalChargeAmount;
        //                                                _HCUserInvoice.UnitCost = 0;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCUserInvoice.StatusId = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceItemId = _HCUserInvoice.Id;
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                && x.TransactionDate > TransactionStartDate
        //                                                && x.TransactionDate < TransactionEndDate
        //                                                && x.SourceId == TransactionSource.Merchant
        //                                                && x.ModeId == TransactionMode.Debit
        //                                                && x.InvoiceId == InvoiceId
        //                                                && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        #region Add Settlement Transaction
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                InvoiceDetails.StatusId = InvoiceStatus;
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCoreContext.SaveChanges();

        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }

        //                                    // Reward Invoice
        //                                    if (IsThankUCashEnable == "1")
        //                                    {
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long RewardTransactions = _HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                              .Count();
        //                                            if (RewardTransactions > 0)
        //                                            {
        //                                                double RewardAmount = _HCoreContext.HCUAccountTransaction
        //                                                   .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                         && x.TransactionDate < TransactionEndDate
        //                                                          && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                          && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                          && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                   .Sum(x => (double?)x.Amount) ?? 0;
        //                                                if (RewardTransactions > 0)
        //                                                {
        //                                                    RewardAmount = Math.Round(RewardAmount, 3);
        //                                                    #region Create Invoice
        //                                                    _HCUserInvoice = new HCUAccountInvoice();
        //                                                    _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                    _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                                    _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                                    _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                                    _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                                    _HCUserInvoice.FromName = "Thank U Cash";
        //                                                    _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                                    _HCUserInvoice.FromContactNumber = "07002000000";
        //                                                    _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                                    _HCUserInvoice.ToName = AccountDetails.Name;
        //                                                    _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                                    _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                                    _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                                    _HCUserInvoice.TotalItem = RewardTransactions;
        //                                                    _HCUserInvoice.Amount = RewardAmount;
        //                                                    _HCUserInvoice.ChargePercentage = 0;
        //                                                    _HCUserInvoice.Charge = 0;
        //                                                    _HCUserInvoice.DiscountPercentage = 0;
        //                                                    _HCUserInvoice.DiscountAmount = 0;
        //                                                    _HCUserInvoice.ComissionPercentage = 0;
        //                                                    _HCUserInvoice.ComissionAmount = 0;
        //                                                    _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                    _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                                    _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                                    _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                                    _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                    _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                                    _HCUserInvoice.CreatedById = 2;
        //                                                    _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                    _HCoreContext.SaveChanges();
        //                                                    long InvoiceId = _HCUserInvoice.Id;
        //                                                    #endregion
        //                                                    #region Set Invoice Id
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x =>
        //                                                                         x.ParentId == AccountDetails.UserAccountId
        //                                                                        && x.TransactionDate > TransactionStartDate
        //                                                                        && x.TransactionDate < TransactionEndDate
        //                                                                        && x.SourceId == TransactionSource.ThankUCashPlus
        //                                                                        && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                        && x.StatusId == StatusHelperId.Transaction_Success
        //                                                                       ).Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                                    }
        //                                                    #endregion
        //                                                    #region Set invoices items
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                           .Where(x => x.InvoiceId == InvoiceId)
        //                                                                                           .Count();
        //                                                        if (Total > 0)
        //                                                        {
        //                                                            _HCUserInvoice = new HCUAccountInvoice();
        //                                                            _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                            _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                            _HCUserInvoice.ParentId = InvoiceId;
        //                                                            _HCUserInvoice.Name = "Claimed Rewards";
        //                                                            _HCUserInvoice.TotalItem = Total;
        //                                                            _HCUserInvoice.Amount = RewardAmount;
        //                                                            _HCUserInvoice.ChargePercentage = 0;
        //                                                            _HCUserInvoice.Charge = 0;
        //                                                            _HCUserInvoice.DiscountPercentage = 0;
        //                                                            _HCUserInvoice.DiscountAmount = 0;
        //                                                            _HCUserInvoice.ComissionPercentage = 0;
        //                                                            _HCUserInvoice.ComissionAmount = 0;
        //                                                            _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                            _HCUserInvoice.UnitCost = 0;
        //                                                            _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                            _HCUserInvoice.CreatedById = 2;
        //                                                            _HCUserInvoice.StatusId = 2;
        //                                                            _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                            _HCoreContext.SaveChanges();
        //                                                            long InvoiceItemId = _HCUserInvoice.Id;
        //                                                            using (_HCoreContext = new HCoreContext())
        //                                                            {
        //                                                                _HCoreContext.HCUAccountTransaction
        //                                                                .Where(x => x.InvoiceId == InvoiceId)
        //                                                                .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    #endregion
        //                                                    #region Add Settlement Transaction
        //                                                    using (_HCoreContext = new HCoreContext())
        //                                                    {
        //                                                        var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                                        if (InvoiceDetails != null)
        //                                                        {
        //                                                            InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                            InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                            InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                            _HCoreContext.SaveChanges();
        //                                                        }
        //                                                    }
        //                                                    #endregion
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long RewardTransactions = _HCoreContext.HCUAccountTransaction
        //                                               .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                     && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                      && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                      && x.SourceId == TransactionSource.TUC
        //                                                      && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                      && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                            if (RewardTransactions > 0)
        //                                            {
        //                                                double RewardAmount = (_HCoreContext.HCUAccountTransaction
        //                                                  .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                                       && x.TransactionDate < TransactionEndDate
        //                                                         && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                         && x.SourceId == TransactionSource.TUC
        //                                                         && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                         && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                  .Sum(x => (double?)x.Amount) ?? 0);
        //                                                RewardAmount = Math.Round(RewardAmount, 3);

        //                                                #region Create Invoice
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                                _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                                _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                                _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                                _HCUserInvoice.FromName = "Thank U Cash";
        //                                                _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                                _HCUserInvoice.FromContactNumber = "07002000000";
        //                                                _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                                _HCUserInvoice.ToName = AccountDetails.Name;
        //                                                _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                                _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                                _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                                _HCUserInvoice.TotalItem = RewardTransactions;
        //                                                _HCUserInvoice.Amount = RewardAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                                _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                                _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceId = _HCUserInvoice.Id;
        //                                                #endregion
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                             .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                       && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                      && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                      && x.SourceId == TransactionSource.TUC
        //                                                      && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                      && x.StatusId == StatusHelperId.Transaction_Success
        //                                                                   )
        //                                                                 .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                                }
        //                                                #region Add Settlement Transaction
        //                                                #region Set invoices items
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.ParentId == AccountDetails.UserAccountId
        //                                                                                              && x.TransactionDate > TransactionStartDate
        //                                                                    && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.Type.SubParentId == TransactionTypeCategory.Reward
        //                                                                                              && x.SourceId == TransactionSource.TUC
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                                                              && x.InvoiceId == InvoiceId
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Count();
        //                                                    if (Total > 0)
        //                                                    {
        //                                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                        _HCUserInvoice.ParentId = InvoiceId;
        //                                                        _HCUserInvoice.Name = "Claimed Rewards";
        //                                                        _HCUserInvoice.TotalItem = Total;
        //                                                        _HCUserInvoice.Amount = RewardAmount;
        //                                                        _HCUserInvoice.ChargePercentage = 0;
        //                                                        _HCUserInvoice.Charge = 0;
        //                                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                                        _HCUserInvoice.DiscountAmount = 0;
        //                                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                                        _HCUserInvoice.ComissionAmount = 0;
        //                                                        _HCUserInvoice.TotalAmount = RewardAmount;
        //                                                        _HCUserInvoice.UnitCost = 0;
        //                                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                        _HCUserInvoice.CreatedById = 2;
        //                                                        _HCUserInvoice.StatusId = 2;
        //                                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                        _HCoreContext.SaveChanges();
        //                                                        long InvoiceItemId = _HCUserInvoice.Id;
        //                                                        using (_HCoreContext = new HCoreContext())
        //                                                        {
        //                                                            _HCoreContext.HCUAccountTransaction
        //                                                            .Where(x => x.InvoiceId == InvoiceId)
        //                                                        .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                        }
        //                                                    }
        //                                                }
        //                                                #endregion
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                                    if (InvoiceDetails != null)
        //                                                    {
        //                                                        InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                        InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                        _HCoreContext.SaveChanges();
        //                                                    }
        //                                                }
        //                                                #endregion
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    // All Amounts Postpaid or direct debit
        //                                    long MerchantPendingInvoiceTransactions = _HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                               && x.TransactionDate > TransactionStartDate
        //                                                               && x.TransactionDate < TransactionEndDate
        //                                                               && x.SourceId == TransactionSource.Merchant
        //                                                               && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                               && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                    if (MerchantPendingInvoiceTransactions > 0)
        //                                    {
        //                                        double MerchantPendingInvoiceAmount = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                           && x.TransactionDate > TransactionStartDate
        //                                                                           && x.TransactionDate < TransactionEndDate
        //                                                                           && x.SourceId == TransactionSource.Merchant
        //                                                                           && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                                           && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
        //                                        MerchantPendingInvoiceAmount = Math.Round(MerchantPendingInvoiceAmount, 3);
        //                                        int InvoiceStatus = StatusHelperId.Invoice_Pending;

        //                                        #region Create Invoice
        //                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.RewardInvoice;
        //                                        _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                        _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber(5);
        //                                        _HCUserInvoice.Name = "Claimed Rewards Invoice";
        //                                        _HCUserInvoice.FromName = "Thank U Cash";
        //                                        _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                        _HCUserInvoice.FromContactNumber = "07002000000";
        //                                        _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                        _HCUserInvoice.ToName = AccountDetails.Name;
        //                                        _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                        _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                        _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                        _HCUserInvoice.TotalItem = MerchantPendingInvoiceTransactions;
        //                                        _HCUserInvoice.Amount = MerchantPendingInvoiceAmount;
        //                                        _HCUserInvoice.ChargePercentage = 0;
        //                                        _HCUserInvoice.Charge = 0;
        //                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                        _HCUserInvoice.DiscountAmount = 0;
        //                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                        _HCUserInvoice.ComissionAmount = 0;
        //                                        _HCUserInvoice.TotalAmount = MerchantPendingInvoiceAmount;
        //                                        _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                        _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                        _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                        _HCUserInvoice.CreatedById = 2;
        //                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                        _HCoreContext.SaveChanges();
        //                                        long InvoiceId = _HCUserInvoice.Id;
        //                                        #endregion
        //                                        #region Update Invoice Id To Transactions
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            _HCoreContext.HCUAccountTransaction
        //                                                     .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                            && x.TransactionDate > TransactionStartDate
        //                                                            && x.TransactionDate < TransactionEndDate
        //                                                            && x.SourceId == TransactionSource.Merchant
        //                                                            && x.ModeId == TransactionMode.Debit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                            && x.StatusId == StatusHelperId.Transaction_Success
        //                                                           )
        //                                                         .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                        }
        //                                        #endregion
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            long Total = _HCoreContext.HCUAccountTransaction
        //                                                                               .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                        && x.TransactionDate > TransactionStartDate
        //                                                                                        && x.TransactionDate < TransactionEndDate
        //                                                                                      && x.SourceId == TransactionSource.Merchant
        //                                                                                      && x.ModeId == TransactionMode.Debit
        //                                                                                      && x.InvoiceId == InvoiceId
        //                                                                                      && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                    .Count();
        //                                            if (Total > 0)
        //                                            {
        //                                                _HCUserInvoice = new HCUAccountInvoice();
        //                                                _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                _HCUserInvoice.ParentId = InvoiceId;
        //                                                _HCUserInvoice.Name = "User Rewards";
        //                                                _HCUserInvoice.TotalItem = Total;
        //                                                _HCUserInvoice.Amount = MerchantPendingInvoiceAmount;
        //                                                _HCUserInvoice.ChargePercentage = 0;
        //                                                _HCUserInvoice.Charge = 0;
        //                                                _HCUserInvoice.DiscountPercentage = 0;
        //                                                _HCUserInvoice.DiscountAmount = 0;
        //                                                _HCUserInvoice.ComissionPercentage = 0;
        //                                                _HCUserInvoice.ComissionAmount = 0;
        //                                                _HCUserInvoice.TotalAmount = MerchantPendingInvoiceAmount;
        //                                                _HCUserInvoice.UnitCost = 0;
        //                                                _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                _HCUserInvoice.CreatedById = 2;
        //                                                _HCUserInvoice.StatusId = 2;
        //                                                _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                _HCoreContext.SaveChanges();
        //                                                long InvoiceItemId = _HCUserInvoice.Id;
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    _HCoreContext.HCUAccountTransaction
        //                                                    .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                    && x.TransactionDate > TransactionStartDate
        //                                                    && x.TransactionDate < TransactionEndDate
        //                                                    && x.InvoiceId == InvoiceId
        //                                                    && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                    .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        #region Set Invoice Status
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                InvoiceDetails.Name = InvoiceDetails.Name + " Invoice No : " + InvoiceDetails.Id.ToString();
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                InvoiceDetails.StatusId = InvoiceStatus;
        //                                                _HCoreContext.SaveChanges();
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        using (_HCoreContext = new HCoreContext())
        //                        {


        //                            double UserSettlementTransactionAmount = (_HCoreContext.HCUAccountTransaction
        //                                                  .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                         && x.TransactionDate < TransactionEndDate
        //                                                         && x.SourceId == TransactionSource.Settlement
        //                                                         && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                         && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                  .Sum(x => (double?)x.TotalAmount) ?? 0);
        //                            if (UserSettlementTransactionAmount > 0)
        //                            {

        //                                long UserSettlementPendingTransactions = _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                        && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success).Count();
        //                                if (UserSettlementPendingTransactions > 0)
        //                                {
        //                                    UserSettlementTransactionAmount = Math.Round(UserSettlementTransactionAmount, 3);
        //                                    #region Create Invoice
        //                                    _HCUserInvoice = new HCUAccountInvoice();
        //                                    _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                    _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.Payment;
        //                                    _HCUserInvoice.AccountId = AccountDetails.UserAccountId;
        //                                    _HCUserInvoice.InoviceNumber = HCoreHelper.GenerateRandomNumber();
        //                                    _HCUserInvoice.Name = "Redeem & Commission Invoice";
        //                                    _HCUserInvoice.FromName = "Thank U Cash";
        //                                    _HCUserInvoice.FromAddress = "No. 7 Ibiyinka Olorunbe Close, Victoria Island, Lagos";
        //                                    _HCUserInvoice.FromContactNumber = "07002000000";
        //                                    _HCUserInvoice.FromEmailAddress = "cards@thankucard.co";
        //                                    _HCUserInvoice.ToName = AccountDetails.Name;
        //                                    _HCUserInvoice.ToAddress = AccountDetails.Address;
        //                                    _HCUserInvoice.ToContactNumber = AccountDetails.ContactNumber;
        //                                    _HCUserInvoice.ToEmailAddress = AccountDetails.EmailAddress;
        //                                    _HCUserInvoice.TotalItem = UserSettlementPendingTransactions;
        //                                    _HCUserInvoice.Amount = 0;
        //                                    _HCUserInvoice.ChargePercentage = 0;
        //                                    _HCUserInvoice.Charge = 0;
        //                                    _HCUserInvoice.DiscountPercentage = 0;
        //                                    _HCUserInvoice.DiscountAmount = 0;
        //                                    _HCUserInvoice.ComissionPercentage = 0;
        //                                    _HCUserInvoice.ComissionAmount = 0;
        //                                    _HCUserInvoice.TotalAmount = 0;
        //                                    _HCUserInvoice.StartDate = InvoiceStartDate;
        //                                    _HCUserInvoice.EndDate = InvoiceEndDate;
        //                                    _HCUserInvoice.InoviceDate = InvoiceDate;
        //                                    _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                    _HCUserInvoice.StatusId = StatusHelperId.Invoice_Creating;
        //                                    _HCUserInvoice.CreatedById = 2;
        //                                    _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                    _HCoreContext.SaveChanges();
        //                                    long InvoiceId = _HCUserInvoice.Id;
        //                                    double SettlementAmount = 0;
        //                                    #endregion
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                               && ! x.HCUAccountInvoiceItem.Any()
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success
        //                                                       )
        //                                                     .Update(x => new HCUAccountTransaction() { InvoiceId = InvoiceId });
        //                                    }

        //                                    #region Add Settlement Transaction
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        SettlementAmount = _HCoreContext.HCUAccountTransaction
        //                                                 .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                         && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                        && x.InvoiceId == InvoiceId
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                              .Sum(x => (double?)x.TotalAmount) ?? 0;
        //                                        #region Add Merchant Settlement Debit transaction
        //                                        _OSaveTransaction = new OSaveTransaction();
        //                                        _OSaveTransaction.UserAccountId = AccountDetails.UserAccountId;
        //                                        _OSaveTransaction.ModeId = TransactionMode.Debit;
        //                                        _OSaveTransaction.TypeId = TransactionType.BankSettlement;
        //                                        _OSaveTransaction.SourceId = TransactionSource.Settlement;
        //                                        _OSaveTransaction.Amount = SettlementAmount;
        //                                        _OSaveTransaction.AmountPercentage = 100;
        //                                        _OSaveTransaction.Charge = 0;
        //                                        _OSaveTransaction.ChargePercentage = 0;
        //                                        _OSaveTransaction.ComissionAmount = 0;
        //                                        _OSaveTransaction.ComissionPercentage = 0;
        //                                        _OSaveTransaction.PayableAmount = SettlementAmount;
        //                                        _OSaveTransaction.TotalAmount = SettlementAmount;
        //                                        _OSaveTransaction.TotalAmountPercentage = 100;
        //                                        _OSaveTransaction.PurchaseAmount = SettlementAmount;
        //                                        _OSaveTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                        _OSaveTransaction.ParentId = AccountDetails.UserAccountId;
        //                                        _OSaveTransaction.ReferenceNumber = InvoiceId.ToString();
        //                                        _OSaveTransaction.Status = StatusHelperId.Transaction_Success;
        //                                        _ManageUserTransaction = new ManageUserTransaction();
        //                                        OTransactionReference TrDebitPgAccountResponse = _ManageUserTransaction.SaveTransaction(_OSaveTransaction);
        //                                        #endregion
        //                                        #region Set invoices items
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            var SystemHelpers = _HCoreContext.HCCore
        //                                                                             .Where(x => x.Parent.SystemName == "hcore.transactiontype" && x.StatusId == StatusHelperId.Default_Active)
        //                                                                             .Select(x => new
        //                                                                             {
        //                                                                                 Id = x.Id,
        //                                                                                 Name = x.Name
        //                                                                             }).ToList();
        //                                            foreach (var SystemHelper in SystemHelpers)
        //                                            {
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    long Total = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                              && x.TransactionDate > TransactionStartDate
        //                                                                                              && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.SourceId == TransactionSource.Settlement
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                                                              && x.TypeId == SystemHelper.Id
        //                                                                                              && x.InvoiceId == InvoiceId
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Count();
        //                                                    if (Total > 0)
        //                                                    {
        //                                                        double TotalAmount = _HCoreContext.HCUAccountTransaction
        //                                                                                       .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                                                               && x.TransactionDate > TransactionStartDate
        //                                                                                                && x.TransactionDate < TransactionEndDate
        //                                                                                              && x.SourceId == TransactionSource.Settlement
        //                                                                                              && x.ModeId == TransactionMode.Credit
        //                                                                                              && x.TypeId == SystemHelper.Id
        //                                                                                              && x.InvoiceId == InvoiceId
        //                                                                                              && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                                                            .Sum(x => x.TotalAmount);

        //                                                        _HCUserInvoice = new HCUAccountInvoice();
        //                                                        _HCUserInvoice.Guid = HCoreHelper.GenerateGuid();
        //                                                        _HCUserInvoice.TypeId = CoreHelpers.InvoiceType.InvoiceItem;
        //                                                        _HCUserInvoice.ParentId = InvoiceId;
        //                                                        _HCUserInvoice.Name = SystemHelper.Name;
        //                                                        _HCUserInvoice.TotalItem = Total;
        //                                                        _HCUserInvoice.Amount = Math.Round(TotalAmount, 2);
        //                                                        _HCUserInvoice.ChargePercentage = 0;
        //                                                        _HCUserInvoice.Charge = 0;
        //                                                        _HCUserInvoice.DiscountPercentage = 0;
        //                                                        _HCUserInvoice.DiscountAmount = 0;
        //                                                        _HCUserInvoice.ComissionPercentage = 0;
        //                                                        _HCUserInvoice.ComissionAmount = 0;
        //                                                        _HCUserInvoice.TotalAmount = _HCUserInvoice.Amount;
        //                                                        _HCUserInvoice.UnitCost = 0;
        //                                                        _HCUserInvoice.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                                        _HCUserInvoice.CreatedById = 2;
        //                                                        _HCUserInvoice.StatusId = 2;
        //                                                        _HCoreContext.HCUAccountInvoice.Add(_HCUserInvoice);
        //                                                        _HCoreContext.SaveChanges();
        //                                                        long InvoiceItemId = _HCUserInvoice.Id;
        //                                                        using (_HCoreContext = new HCoreContext())
        //                                                        {
        //                                                            _HCoreContext.HCUAccountTransaction
        //                                                            .Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                                        && x.TransactionDate > TransactionStartDate
        //                                                        && x.TransactionDate < TransactionEndDate
        //                                                        && x.SourceId == TransactionSource.Settlement
        //                                                        && x.ModeId == TransactionMode.Credit
        //                                                        && x.TypeId == SystemHelper.Id
        //                                                        && x.InvoiceId == InvoiceId
        //                                                        && x.StatusId == StatusHelperId.Transaction_Success)
        //                                                        .Update(x => new HCUAccountTransaction() { InvoiceItemId = InvoiceItemId });
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                        using (_HCoreContext = new HCoreContext())
        //                                        {
        //                                            //StartDate = _HCoreContext.HCUAccountTransaction
        //                                            //.Where(x => x.AccountId == AccountDetails.UserAccountId
        //                                            // && x.TransactionDate < SEndDate
        //                                            // && x.SourceId == TransactionSource.Settlement
        //                                            // && x.ModeId == TransactionMode.Credit
        //                                            // && x.InvoiceId == InvoiceId
        //                                            // && x.StatusId == StatusHelperId.Transaction_Success
        //                                            //).OrderBy(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

        //                                            var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceId).FirstOrDefault();
        //                                            if (InvoiceDetails != null)
        //                                            {
        //                                                //InvoiceDetails.StartDate = StartDate;
        //                                                InvoiceDetails.Amount = SettlementAmount;
        //                                                InvoiceDetails.TotalAmount = SettlementAmount;
        //                                                InvoiceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                InvoiceDetails.StatusId = StatusHelperId.Invoice_Pending;
        //                                                _HCoreContext.SaveChanges();
        //                                            }
        //                                        }
        //                                    }
        //                                    #endregion
        //                                }
        //                                else
        //                                {

        //                                }

        //                            }
        //                        }


        //                    }

        //                }
        //                InvoiceDate = InvoiceDate.AddDays(1);
        //                TransactionStartDate = TransactionStartDate.AddDays(1);
        //                TransactionEndDate = TransactionEndDate.AddDays(1);
        //                InvoiceStartDate = InvoiceStartDate.AddDays(1);
        //                InvoiceEndDate = InvoiceEndDate.AddDays(1);

        //            }

        //        }
        //        #endregion
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException("CreatePaymentSettlementManual", _Exception);
        //        #endregion
        //    }
        //    #endregion
        //}




        /// <summary>
        /// Description: Gets the payment invoices.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPaymentInvoices(OList.Request _Request)
        {
            #region Declare
            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCUAccountInvoice
                                        select new OUserSettlement.List
                                        {
                                            ReferenceId = n.Id,
                                            ReferenceKey = n.Guid,
                                            TypeCode = n.Type.SystemName,
                                            Name = n.Name,
                                            UserKey = n.Account.Guid,
                                            UserDisplayName = n.Account.DisplayName,
                                            TotalAmount = n.TotalAmount,
                                            StartDate = n.StartDate,
                                            EndDate = n.EndDate,
                                            InoviceDate = n.InoviceDate,
                                            Status = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name,
                                            CreateDate = n.CreateDate,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserSettlement.List> Data = (from n in _HCoreContext.HCUAccountInvoice
                                                       select new OUserSettlement.List
                                                       {
                                                           ReferenceId = n.Id,
                                                           ReferenceKey = n.Guid,
                                                           TypeCode = n.Type.SystemName,
                                                           UserKey = n.Account.Guid,
                                                           Name = n.Name,
                                                           UserDisplayName = n.Account.DisplayName,
                                                           TotalAmount = n.TotalAmount,
                                                           StartDate = n.StartDate,
                                                           EndDate = n.EndDate,
                                                           InoviceDate = n.InoviceDate,
                                                           Status = n.StatusId,
                                                           StatusCode = n.Status.SystemName,
                                                           StatusName = n.Status.Name,
                                                           CreateDate = n.CreateDate,
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetPaymentInvoices", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePaymentInvoice(OUserSettlement.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                _ManageConfiguration = new ManageConfiguration();
                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.InvoiceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Comment))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Guid == _Request.InvoiceKey
                                                                              && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                              .Select(x => new
                                                                              {
                                                                                  InvoiceNumber = x.InoviceNumber,
                                                                                  InvoiceTypeId = x.TypeId,
                                                                                  InvoiceId = x.Id,
                                                                                  UserAccountId = x.AccountId,
                                                                                  UserAccountTypeId = x.Account.AccountTypeId,
                                                                                  TotalAmount = x.TotalAmount,
                                                                              })
                                                           .FirstOrDefault();
                        if (InvoiceDetails != null)
                        {

                            if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardInvoice)
                            {
                                if (InvoiceDetails.UserAccountTypeId == UserAccountType.Merchant)
                                {
                                    OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", (long)InvoiceDetails.UserAccountId);
                                    string IsThankUCashEnable = HCoreHelper.GetConfiguration("thankucashplus", InvoiceDetails.UserAccountId);
                                    OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", (long)InvoiceDetails.UserAccountId);

                                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay"
                                    || RewardDeductionType.TypeCode == "rewarddeductiontype.postpay"
                                    || RewardDeductionType.TypeCode == "rewarddeductiontype.directdebit")
                                    {

                                        // Credit Amount To Merchant Amount
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.InvoiceAmount = (double)InvoiceDetails.TotalAmount;
                                        _CoreTransactionRequest.ReferenceNumber = InvoiceDetails.InvoiceNumber;
                                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                        _CoreTransactionRequest.ParentId = (long)InvoiceDetails.UserAccountId;
                                        _CoreTransactionRequest.ReferenceAmount = (double)InvoiceDetails.TotalAmount;

                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = (long)InvoiceDetails.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.MerchantCredit,
                                            SourceId = TransactionSource.Merchant,

                                            Amount = (double)InvoiceDetails.TotalAmount,
                                            Comission = 0,
                                            TotalAmount = (double)InvoiceDetails.TotalAmount,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                             && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                           .FirstOrDefault();
                                                if (InvoiceInfo != null)
                                                {
                                                    InvoiceInfo.Comment = _Request.Comment;
                                                    InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                    InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                    InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    _HCoreContext.SaveChanges();
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                    #endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                            #endregion
                                        }

                                    }
                                    else if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
                                    {
                                        if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardInvoice)
                                        {
                                            var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                           && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                         .FirstOrDefault();
                                            if (InvoiceInfo != null)
                                            {
                                                InvoiceInfo.Comment = _Request.Comment;
                                                InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                _HCoreContext.SaveChanges();
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                #endregion
                                            }
                                        }
                                        else if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardCommissionInvoice)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.InvoiceAmount = (double)InvoiceDetails.TotalAmount;
                                            _CoreTransactionRequest.ReferenceNumber = InvoiceDetails.InvoiceNumber;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _CoreTransactionRequest.ReferenceAmount = (double)InvoiceDetails.TotalAmount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = (long)InvoiceDetails.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.MerchantCredit,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = (double)InvoiceDetails.TotalAmount,
                                                Comission = 0,
                                                TotalAmount = (double)InvoiceDetails.TotalAmount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                                 && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                               .FirstOrDefault();
                                                    if (InvoiceInfo != null)
                                                    {
                                                        InvoiceInfo.Comment = _Request.Comment;
                                                        InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                        InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                        InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                        _HCoreContext.SaveChanges();
                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                        #endregion
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                        #endregion
                                    }


                                }
                                else
                                {
                                    var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                            && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                          .FirstOrDefault();
                                    if (InvoiceInfo != null)
                                    {
                                        InvoiceInfo.Comment = _Request.Comment;
                                        InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                        InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                        InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                        #endregion
                                    }
                                }
                            }
                            else
                            {
                                var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                              && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                            .FirstOrDefault();
                                if (InvoiceInfo != null)
                                {
                                    InvoiceInfo.Comment = _Request.Comment;
                                    InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                    InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                    InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                    #endregion
                                }

                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdatePaymentInvoice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPaymentInvoice(OUserSettlement.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                _ManageConfiguration = new ManageConfiguration();
                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.InvoiceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001");
                    #endregion
                }
                else
                {
                    //double MinSettlementAmount = Convert.ToDouble(_ManageConfiguration.GetConfigurationValue("minimumsettlementamount", null, _Request.UserReference)) + 1;
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserSettlement.Details InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Guid == _Request.InvoiceKey)
                                                          .Select(x => new OUserSettlement.Details
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,
                                                              TypeKey = x.Type.SystemName,
                                                              TypeName = x.Type.Name,
                                                              InoviceNumber = x.InoviceNumber,
                                                              UserKey = x.Account.Guid,
                                                              UserDisplayName = x.Account.DisplayName,
                                                              FromName = x.FromName,
                                                              FromAddress = x.FromAddress,
                                                              FromContactNumber = x.FromContactNumber,
                                                              FromEmailAddress = x.FromEmailAddress,
                                                              FromFax = x.FromFax,
                                                              ToName = x.ToName,
                                                              ToAddress = x.ToAddress,
                                                              ToContactNumber = x.ToContactNumber,
                                                              ToEmailAddress = x.ToEmailAddress,
                                                              ToFax = x.ToFax,
                                                              TotalItem = x.TotalItem,
                                                              Amount = x.Amount,
                                                              ChargePercentage = x.ChargePercentage,
                                                              Charge = x.Charge,
                                                              DiscountPercentage = x.DiscountPercentage,
                                                              DiscountAmount = x.DiscountAmount,
                                                              ComissionPercentage = x.ComissionPercentage,
                                                              ComissionAmount = x.ComissionAmount,
                                                              InoviceDate = x.InoviceDate,
                                                              TotalAmount = x.TotalAmount,
                                                              StartDate = x.StartDate,
                                                              EndDate = x.EndDate,
                                                              CreateDate = x.CreateDate,
                                                              CreatedByKey = x.CreatedBy.Guid,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                              ModifyDate = x.ModifyDate,
                                                              ModifyByKey = x.ModifyBy.Guid,
                                                              ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                              Status = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                              Comment = x.Comment,
                                                          })
                                                           .FirstOrDefault();
                        if (InvoiceDetails != null)
                        {
                            InvoiceDetails.Items = _HCoreContext.HCUAccountInvoice.Where(x => x.ParentId == InvoiceDetails.ReferenceId)
                                .Select(x => new OUserSettlement.DetailsItem
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    InoviceId = x.Parent.Id,
                                    InvoiceKey = x.Parent.Guid,
                                    Name = x.Name,
                                    Description = x.Description,
                                    UnitCost = x.UnitCost,
                                    Quantity = x.TotalItem,
                                    Amount = x.Amount,
                                    CreateDate = x.CreateDate,
                                    CreatedByKey = x.CreatedBy.Guid,
                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                    ModifyDate = x.ModifyDate,
                                    ModifyByKey = x.ModifyBy.Guid,
                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                    Status = x.StatusId,
                                    StatusCode = x.Status.SystemName,
                                    StatusName = x.Status.Name
                                }).ToList();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, InvoiceDetails, "HCR007");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetPaymentInvoice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the settlements overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSettlementsOverview(OUserSettlement.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                _OOverview = new OUserSettlement.Overview();
                _OOverviewSettlements = new OUserSettlement.OverviewSettlement();
                _OOverviewInvoices = new OUserSettlement.OverviewInvoices();
                _OOverviewSettlementAccountType = new OUserSettlement.OverviewSettlementAccountType();
                #endregion
                #region Code Block
                using (_HCoreContext = new HCoreContext())
                {

                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        _OOverviewInvoices.Total = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.TotalCount = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();

                        _OOverviewInvoices.Completed = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.CompletedCount = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();

                        _OOverviewInvoices.Pending = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.PendingCount = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();
                        _OOverview.Invoices = _OOverviewInvoices;


                        _OOverviewSettlements.Total = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == TransactionSource.Settlement &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewSettlements.TotalCount = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == TransactionSource.Settlement &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                            .Count();
                        _OOverviewSettlements.Completed = _HCoreContext.HCUAccountTransaction
                                                       .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == TransactionSource.Settlement &&
                                                              x.ModeId == TransactionMode.Debit &&
                                                              x.StatusId == StatusHelperId.Transaction_Success)
                                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewSettlements.CompletedCount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == TransactionSource.Settlement &&
                                                                                x.ModeId == TransactionMode.Debit &&
                                                                                x.StatusId == StatusHelperId.Transaction_Success).Count();
                        _OOverviewSettlements.Pending = _OOverviewSettlements.Total - _OOverviewSettlements.Completed;
                        _OOverviewSettlements.PendingCount = 0;

                        _OOverview.Settlements = _OOverviewSettlements;
                        _OOverview.Invoices = _OOverviewInvoices;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverview, "HCR007");
                        #endregion
                    }
                    else
                    {
                        _OOverviewInvoices.Total = _HCoreContext.HCUAccountInvoice.Where(x => x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.TotalCount = _HCoreContext.HCUAccountInvoice.Where(x => x.TypeId == CoreHelpers.InvoiceType.Payment).Count();

                        _OOverviewInvoices.Completed = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.CompletedCount = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();

                        _OOverviewInvoices.Pending = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewInvoices.PendingCount = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending && x.TypeId == CoreHelpers.InvoiceType.Payment).Count();


                        _OOverviewSettlements.Total = _HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                              x.ModeId == TransactionMode.Credit &&
                                                                x.StatusId == StatusHelperId.Transaction_Success)
                                                         .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewSettlements.TotalCount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                              x.ModeId == TransactionMode.Credit &&
                                                                                  x.StatusId == StatusHelperId.Transaction_Success).Count();


                        _OOverviewSettlements.Completed = _HCoreContext.HCUAccountTransaction
                                                       .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                              x.ModeId == TransactionMode.Debit &&
                                                              x.StatusId == StatusHelperId.Transaction_Success)
                                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                        _OOverviewSettlements.CompletedCount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.SourceId == TransactionSource.Settlement &&
                                                                                x.ModeId == TransactionMode.Debit &&
                                                                                x.StatusId == StatusHelperId.Transaction_Success).Count();

                        _OOverviewSettlements.Pending = _OOverviewSettlements.Total - _OOverviewSettlements.Completed;
                        _OOverviewSettlements.PendingCount = _OOverviewSettlements.TotalCount - _OOverviewSettlements.CompletedCount;

                        _OOverview.Settlements = _OOverviewSettlements;
                        _OOverview.Invoices = _OOverviewInvoices;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverview, "HCR007");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetPaymentInvoice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion

        }
    }
}
