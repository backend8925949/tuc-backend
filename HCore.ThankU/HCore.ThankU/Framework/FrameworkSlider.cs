//==================================================================================
// FileName: FrameworkSlider.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to slider
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;

namespace HCore.ThankU.Framework
{
    public class FrameworkSlider
    {

        #region Entity
        HCoreContext _HCoreContext;
        #endregion
        /// <summary>
        /// Description: Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSliderImages(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = 0;
                    #endregion
                    #region Get Data
                    List<SliderImages> Data = (from n in _HCoreContext.HCCoreStorage
                                               where n.Helper.SystemName == "sliderimage"
                                               orderby n.CreateDate descending
                                               select new SliderImages
                                               {
                                                   PosterUrl = _AppConfig.StorageUrl + n.Path,
                                               })
                                              .Skip(0)
                                              .Take(8)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion

        }

    }
}
