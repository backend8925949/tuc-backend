//==================================================================================
// FileName: FrameworkApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to app
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
//using HCore.ThankU.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using GeoCoordinatePortable;

namespace HCore.ThankU.Framework
{
    public class FrameworkApp
    {
        /// <summary>
        /// Description: Converts to radians.
        /// </summary>
        /// <param name="degrees">The degrees.</param>
        /// <returns>System.Double.</returns>
        public double ToRadians(double degrees) => degrees * Math.PI / 180.0;
        /// <summary>
        /// Description: Distances the in miles.
        /// </summary>
        /// <param name="lon1d">The lon1d.</param>
        /// <param name="lat1d">The lat1d.</param>
        /// <param name="lon2d">The lon2d.</param>
        /// <param name="lat2d">The lat2d.</param>
        /// <returns>System.Double.</returns>
        public double DistanceInMiles(double lon1d, double lat1d, double? lon2d, double? lat2d)
        {
            var lon1 = ToRadians((double)lon1d);
            var lat1 = ToRadians((double)lat1d);
            var lon2 = ToRadians((double)lon2d);
            var lat2 = ToRadians((double)lat2d);
            var deltaLon = lon2 - lon1;
            var c = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(deltaLon));
            var earthRadius = 3958.76;
            var distInMiles = earthRadius * c;
            return distInMiles;
        }
        #region References
        ManageConfiguration _ManageConfiguration;
        OThankUCashApp.Response.DeviceConnect _AppResponse;
        #endregion
        #region Declare
        HCoreContext _HCoreContext;
        #endregion
        #region Code Block
        /// <summary>
        /// Description: Gets the user accounts.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccounts(OList.Request _Request)
        {
            #region Declare
            List<OUserAccountList> Data = null;
            int TotalRecords = 0;
            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Set Default Limit
                if (_Request.Limit == -1)
                {
                    _Request.Limit = TotalRecords;
                }
                else if (_Request.Limit == 0)
                {
                    _Request.Limit = DefaultLimit;

                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == ListType.Stores)
                    {
                        if (!string.IsNullOrEmpty(_Request.CategoryIds))
                        {
                            if (_Request.Latitude != 0 && _Request.Longitude != 0)
                            {
                                var coord = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                #region With Distance 
                                #region Total Records
                                TotalRecords = (from n in _HCoreContext.HCUAccount
                                                where
                                                n.AccountTypeId == UserAccountType.MerchantStore
                                                && n.Latitude != 0 && n.Longitude != 0
                                                && n.Owner.StatusId == StatusActive
                                                && n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                && (n.Owner.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                                select new OUserAccountList
                                                {
                                                    ReferenceKey = n.Guid,
                                                    Name = n.DisplayName,
                                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                                    OwnerKey = n.Owner.Guid,
                                                    OwnerDisplayName = n.Owner.DisplayName,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    Address = n.Address,
                                                    AddressLatitude = n.Latitude,
                                                    AddressLongitude = n.Longitude,
                                                    CountryKey = n.Country.Guid,
                                                    Status = n.StatusId,
                                                    CreateDate = n.CreateDate,
                                                    CountValue = n.CountValue,
                                                    AverageValue = n.AverageValue
                                                }).Where(_Request.SearchCondition)
                                           .Count();
                                #endregion
                                #region Get Data
                                Data = (from n in _HCoreContext.HCUAccount
                                        where
                                                  n.AccountTypeId == UserAccountType.MerchantStore
                                                && n.Latitude != 0 && n.Longitude != 0
                                                && n.Owner.StatusId == StatusActive
                                                && n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                && (n.Owner.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                        orderby new GeoCoordinate(n.Latitude, n.Longitude).GetDistanceTo(coord)
                                        select new OUserAccountList
                                        {
                                            ReferenceKey = n.Guid,
                                            Name = n.DisplayName,
                                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                            OwnerId = n.OwnerId,
                                            OwnerKey = n.Owner.Guid,
                                            OwnerDisplayName = n.Owner.DisplayName,
                                            DisplayName = n.DisplayName,
                                            ContactNumber = n.ContactNumber,
                                            IconUrl = n.Owner.IconStorage.Path,
                                            Address = n.Address,
                                            AddressLatitude = n.Latitude,
                                            AddressLongitude = n.Longitude,
                                            CountryKey = n.Country.Guid,
                                            Status = n.StatusId,
                                            CreateDate = n.CreateDate,
                                            CountValue = n.CountValue,
                                            AverageValue = n.AverageValue
                                        })

                                                          .Where(_Request.SearchCondition)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();
                                #endregion
                                _ManageConfiguration = new ManageConfiguration();
                                foreach (var item in Data)
                                {
                                    item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                        .Select(x => new OParameter
                                        {
                                            ReferenceKey = x.Category.Guid,
                                            Name = x.Category.Name,
                                        }).ToList();
                                    item.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.Account.Guid == item.ReferenceKey)
                                  .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();
                                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.OwnerId));
                                }
                                #endregion
                            }
                            else
                            {
                                #region Without Distance
                                #region Total Records
                                TotalRecords = (from n in _HCoreContext.HCUAccount
                                                where n.AccountTypeId == UserAccountType.MerchantStore &&
                                                n.Owner.StatusId == StatusActive &&
                                                n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                && (n.Owner.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                                select new OUserAccountList
                                                {
                                                    ReferenceKey = n.Guid,
                                                    Name = n.DisplayName,
                                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                                    OwnerKey = n.Owner.Guid,
                                                    OwnerDisplayName = n.Owner.DisplayName,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    Address = n.Address,
                                                    AddressLatitude = n.Latitude,
                                                    AddressLongitude = n.Longitude,
                                                    CountryKey = n.Country.Guid,
                                                    Status = n.StatusId,
                                                    CountValue = n.CountValue,
                                                    AverageValue = n.AverageValue
                                                }).Where(_Request.SearchCondition)
                                           .Count();
                                #endregion
                                #region Get Data
                                Data = (from n in _HCoreContext.HCUAccount
                                        where n.AccountTypeId == UserAccountType.MerchantStore &&
                                        n.Owner.StatusId == StatusActive &&
                                                n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                        && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                && (n.Owner.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                        select new OUserAccountList
                                        {
                                            ReferenceKey = n.Guid,
                                            Name = n.DisplayName,
                                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                            OwnerId = n.OwnerId,
                                            OwnerKey = n.Owner.Guid,
                                            OwnerDisplayName = n.Owner.DisplayName,
                                            DisplayName = n.DisplayName,
                                            ContactNumber = n.ContactNumber,
                                            IconUrl = n.Owner.IconStorage.Path,
                                            Address = n.Address,
                                            AddressLatitude = n.Latitude,
                                            AddressLongitude = n.Longitude,
                                            CountryKey = n.Country.Guid,
                                            Status = n.StatusId,

                                            CountValue = n.CountValue,
                                            AverageValue = n.AverageValue
                                        })
                                                          .Where(_Request.SearchCondition)
                                                          .OrderBy(_Request.SortExpression)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();
                                #endregion
                                _ManageConfiguration = new ManageConfiguration();
                                foreach (var item in Data)
                                {
                                    //item.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == item.OwnerKey &&
                                    //                                                      x.TypeId == HelperType.MerchantCategory)
                                    //    .Select(x => new OParameter
                                    //    {
                                    //        ReferenceKey = x.Common.Guid,
                                    //        Name = x.Common.Name,
                                    //    }).ToList();
                                    item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                       .Select(x => new OParameter
                                       {
                                           ReferenceKey = x.Category.Guid,
                                           Name = x.Category.Name,
                                       }).ToList();
                                    item.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.Account.Guid == item.ReferenceKey)
                                .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();
                                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", (long)item.OwnerId));
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (_Request.Latitude != 0 && _Request.Longitude != 0)
                            {
                                var coord = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                #region With Distance 
                                #region Total Records
                                TotalRecords = (from n in _HCoreContext.HCUAccount
                                                where
                                                n.AccountTypeId == UserAccountType.MerchantStore
                                                && n.Latitude != 0 && n.Longitude != 0
                                                && n.Owner.StatusId == StatusActive
                                                && n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                select new OUserAccountList
                                                {
                                                    ReferenceKey = n.Guid,
                                                    Name = n.DisplayName,
                                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                                    OwnerKey = n.Owner.Guid,
                                                    OwnerDisplayName = n.Owner.DisplayName,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    Address = n.Address,
                                                    AddressLatitude = n.Latitude,
                                                    AddressLongitude = n.Longitude,
                                                    CountryKey = n.Country.Guid,
                                                    Status = n.StatusId,
                                                    CreateDate = n.CreateDate,
                                                    CountValue = n.CountValue,
                                                    AverageValue = n.AverageValue
                                                }).Where(_Request.SearchCondition)
                                           .Count();
                                #endregion
                                #region Get Data
                                Data = (from n in _HCoreContext.HCUAccount
                                        where
                                                  n.AccountTypeId == UserAccountType.MerchantStore
                                                && n.Latitude != 0 && n.Longitude != 0
                                                && n.Owner.StatusId == StatusActive
                                                && n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                        orderby new GeoCoordinate(n.Latitude, n.Longitude).GetDistanceTo(coord)
                                        select new OUserAccountList
                                        {
                                            ReferenceKey = n.Guid,
                                            Name = n.DisplayName,
                                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                            OwnerId = n.OwnerId,
                                            OwnerKey = n.Owner.Guid,
                                            OwnerDisplayName = n.Owner.DisplayName,
                                            DisplayName = n.DisplayName,
                                            ContactNumber = n.ContactNumber,
                                            IconUrl = n.Owner.IconStorage.Path,
                                            Address = n.Address,
                                            AddressLatitude = n.Latitude,
                                            AddressLongitude = n.Longitude,
                                            CountryKey = n.Country.Guid,
                                            Status = n.StatusId,
                                            CreateDate = n.CreateDate,
                                            CountValue = n.CountValue,
                                            AverageValue = n.AverageValue
                                        })

                                                          .Where(_Request.SearchCondition)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();
                                #endregion
                                _ManageConfiguration = new ManageConfiguration();
                                foreach (var item in Data)
                                {
                                    //item.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == item.OwnerKey &&
                                    //                                                      x.TypeId == HelperType.MerchantCategory)
                                    //    .Select(x => new OParameter
                                    //    {
                                    //        ReferenceKey = x.Common.Guid,
                                    //        Name = x.Common.Name,
                                    //    }).ToList();
                                    item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                      .Select(x => new OParameter
                                      {
                                          ReferenceKey = x.Category.Guid,
                                          Name = x.Category.Name,
                                      }).ToList();
                                    item.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.Account.Guid == item.ReferenceKey)
                                  .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();
                                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.OwnerId));
                                }
                                #endregion
                            }
                            else
                            {
                                #region Without Distance
                                #region Total Records
                                TotalRecords = (from n in _HCoreContext.HCUAccount
                                                where n.AccountTypeId == UserAccountType.MerchantStore &&
                                                n.Owner.StatusId == StatusActive &&
                                                n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                                select new OUserAccountList
                                                {
                                                    ReferenceKey = n.Guid,
                                                    Name = n.DisplayName,
                                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                                    OwnerKey = n.Owner.Guid,
                                                    OwnerDisplayName = n.Owner.DisplayName,
                                                    DisplayName = n.DisplayName,
                                                    ContactNumber = n.ContactNumber,
                                                    Address = n.Address,
                                                    AddressLatitude = n.Latitude,
                                                    AddressLongitude = n.Longitude,
                                                    CountryKey = n.Country.Guid,
                                                    Status = n.StatusId,
                                                    CountValue = n.CountValue,
                                                    AverageValue = n.AverageValue
                                                }).Where(_Request.SearchCondition)
                                           .Count();
                                #endregion
                                #region Get Data
                                Data = (from n in _HCoreContext.HCUAccount
                                        where n.AccountTypeId == UserAccountType.MerchantStore &&
                                        n.Owner.StatusId == StatusActive &&
                                                n.StatusId == StatusActive
                                                && n.Owner.IconStorageId != null
                                        && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
                                        select new OUserAccountList
                                        {
                                            ReferenceKey = n.Guid,
                                            Name = n.DisplayName,
                                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                            OwnerId = n.OwnerId,
                                            OwnerKey = n.Owner.Guid,
                                            OwnerDisplayName = n.Owner.DisplayName,
                                            DisplayName = n.DisplayName,
                                            ContactNumber = n.ContactNumber,
                                            IconUrl = n.Owner.IconStorage.Path,
                                            Address = n.Address,
                                            AddressLatitude = n.Latitude,
                                            AddressLongitude = n.Longitude,
                                            CountryKey = n.Country.Guid,
                                            Status = n.StatusId,

                                            CountValue = n.CountValue,
                                            AverageValue = n.AverageValue
                                        })
                                                          .Where(_Request.SearchCondition)
                                                          .OrderBy(_Request.SortExpression)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();
                                #endregion
                                _ManageConfiguration = new ManageConfiguration();
                                foreach (var item in Data)
                                {
                                    //item.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == item.OwnerKey &&
                                    //                                                      x.TypeId == HelperType.MerchantCategory)
                                    //    .Select(x => new OParameter
                                    //    {
                                    //        ReferenceKey = x.Common.Guid,
                                    //        Name = x.Common.Name,
                                    //    }).ToList();
                                    item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                      .Select(x => new OParameter
                                      {
                                          ReferenceKey = x.Category.Guid,
                                          Name = x.Category.Name,
                                      }).ToList();
                                    item.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.Account.Guid == item.ReferenceKey)
                                .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();
                                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", (long)item.OwnerId));
                                }
                                #endregion
                            }

                        }
                    }
                    else if (_Request.Type == ListType.Merchants)
                    {
                        if (!string.IsNullOrEmpty(_Request.CategoryIds))
                        {
                            #region Total Records
                            TotalRecords = (from n in _HCoreContext.HCUAccount
                                            where n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
                                                && n.IconStorageId != null
                                    && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
                                                && (n.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                            select new OUserAccountList
                                            {
                                                ReferenceKey = n.Guid,
                                                Name = n.Name,

                                                DisplayName = n.DisplayName,
                                                ContactNumber = n.ContactNumber,
                                                EmailAddress = n.EmailAddress,

                                                IconUrl = n.IconStorage.Path,
                                                PosterUrl = n.PosterStorage.Path,

                                                Address = n.Address,
                                                AddressLatitude = n.Latitude,
                                                AddressLongitude = n.Longitude,

                                                CountryKey = n.Country.Guid,
                                                CountryName = n.Country.Name,

                                                Status = n.StatusId,
                                                StatusCode = n.Status.SystemName,

                                                CountValue = n.CountValue,
                                                AverageValue = n.AverageValue
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                            #endregion
                            #region Get Data
                            Data = (from n in _HCoreContext.HCUAccount
                                    where n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
                                                && n.IconStorageId != null
                                    && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
                                                && (n.HCUAccountParameterAccount.Where(x => x.Common.Guid == _Request.CategoryIds).Count() > 0)
                                    select new OUserAccountList
                                    {
                                        ReferenceKey = n.Guid,
                                        Name = n.Name,

                                        DisplayName = n.DisplayName,
                                        ContactNumber = n.ContactNumber,
                                        EmailAddress = n.EmailAddress,

                                        IconUrl = n.IconStorage.Path,
                                        PosterUrl = n.PosterStorage.Path,

                                        Address = n.Address,
                                        AddressLatitude = n.Latitude,
                                        AddressLongitude = n.Longitude,

                                        CountryKey = n.Country.Guid,
                                        CountryName = n.Country.Name,

                                        Status = n.StatusId,
                                        StatusCode = n.Status.SystemName,

                                        CountValue = n.CountValue,
                                        AverageValue = n.AverageValue
                                    })
                                                      .Where(_Request.SearchCondition)
                                                      .OrderBy(_Request.SortExpression)
                                                      .Skip(_Request.Offset)
                                                      .Take(_Request.Limit)
                                                      .ToList();
                            #endregion
                            _ManageConfiguration = new ManageConfiguration();
                            foreach (var item in Data)
                            {
                                //item.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == item.ReferenceKey &&
                                //                                                          x.TypeId == HelperType.MerchantCategory)
                                //        .Select(x => new OParameter
                                //        {
                                //            ReferenceKey = x.Common.Guid,
                                //            Name = x.Common.Name,
                                //        }).ToList();
                                item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                      .Select(x => new OParameter
                                      {
                                          ReferenceKey = x.Category.Guid,
                                          Name = x.Category.Name,
                                      }).ToList();
                                item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.ReferenceKey));
                            }
                        }
                        else
                        {
                            #region Total Records
                            TotalRecords = (from n in _HCoreContext.HCUAccount
                                            where n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
                                                && n.IconStorageId != null && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
                                            select new OUserAccountList
                                            {
                                                ReferenceKey = n.Guid,
                                                Name = n.Name,

                                                DisplayName = n.DisplayName,
                                                ContactNumber = n.ContactNumber,
                                                EmailAddress = n.EmailAddress,

                                                IconUrl = n.IconStorage.Path,
                                                PosterUrl = n.PosterStorage.Path,

                                                Address = n.Address,
                                                AddressLatitude = n.Latitude,
                                                AddressLongitude = n.Longitude,

                                                CountryKey = n.Country.Guid,
                                                CountryName = n.Country.Name,

                                                Status = n.StatusId,
                                                StatusCode = n.Status.SystemName,

                                                CountValue = n.CountValue,
                                                AverageValue = n.AverageValue
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                            #endregion
                            #region Get Data
                            Data = (from n in _HCoreContext.HCUAccount
                                    where n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
                                                && n.IconStorageId != null
                                    && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
                                    select new OUserAccountList
                                    {
                                        ReferenceKey = n.Guid,
                                        Name = n.Name,

                                        DisplayName = n.DisplayName,
                                        ContactNumber = n.ContactNumber,
                                        EmailAddress = n.EmailAddress,

                                        IconUrl = n.IconStorage.Path,
                                        PosterUrl = n.PosterStorage.Path,

                                        Address = n.Address,
                                        AddressLatitude = n.Latitude,
                                        AddressLongitude = n.Longitude,

                                        CountryKey = n.Country.Guid,
                                        CountryName = n.Country.Name,

                                        Status = n.StatusId,
                                        StatusCode = n.Status.SystemName,

                                        CountValue = n.CountValue,
                                        AverageValue = n.AverageValue
                                    })
                                                      .Where(_Request.SearchCondition)
                                                      .OrderBy(_Request.SortExpression)
                                                      .Skip(_Request.Offset)
                                                      .Take(_Request.Limit)
                                                      .ToList();
                            #endregion
                            _ManageConfiguration = new ManageConfiguration();
                            foreach (var item in Data)
                            {
                                //item.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == item.ReferenceKey &&
                                //                                                          x.TypeId == HelperType.MerchantCategory)
                                //        .Select(x => new OParameter
                                //        {
                                //            ReferenceKey = x.Common.Guid,
                                //            Name = x.Common.Name,
                                //        }).ToList();
                                item.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == item.OwnerKey)
                                      .Select(x => new OParameter
                                      {
                                          ReferenceKey = x.Category.Guid,
                                          Name = x.Category.Name,
                                      }).ToList();
                                item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.ReferenceKey));
                            }
                        }
                    }
                    if (Data != null)
                    {
                        #region Create  Response Object
                        foreach (var DataItem in Data)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            if (!string.IsNullOrEmpty(DataItem.PosterUrl))
                            {
                                DataItem.PosterUrl = _AppConfig.StorageUrl + DataItem.PosterUrl;
                            }
                        }

                        OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Create DataTable Response Object
                        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccounts", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccount(OThankUCashApp.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    if (_Request.Type == ListType.Stores)
                    {
                        OThankUCashApp.Response.Info _Details = (from n in _HCoreContext.HCUAccount
                                                                 where n.Guid == _Request.ReferenceKey
                                                                 && n.AccountTypeId == UserAccountType.MerchantStore
                                                                 select new OThankUCashApp.Response.Info
                                                                 {
                                                                     ReferenceId = n.Id,
                                                                     ReferenceKey = n.Guid,
                                                                     Name = n.Owner.DisplayName,
                                                                     OwnerId = n.OwnerId,
                                                                     OwnerKey = n.Owner.Guid,
                                                                     DisplayName = n.Owner.DisplayName,
                                                                     ContactNumber = n.ContactNumber,
                                                                     EmailAddress = n.EmailAddress,
                                                                     IconUrl = n.Owner.IconStorage.Path,
                                                                     Address = n.Address,
                                                                     AddressLatitude = n.Latitude,
                                                                     AddressLongitude = n.Longitude,
                                                                     Description = n.Owner.Description,
                                                                     CreateDate = n.CreateDate,
                                                                     CountValue = n.CountValue,
                                                                     AverageValue = n.AverageValue
                                                                 }).FirstOrDefault();
                        if (_Details != null)
                        {
                            //var Cat = _HCoreContext.HCUAccountParameter
                            //                       .Where(x => x.AccountId == _Details.OwnerId
                            //                              && x.TypeId == HelperType.MerchantCategory)
                            //        .Select(x => new OParameter
                            //        {
                            //            ReferenceKey = x.Common.Guid,
                            //            Name = x.Common.Name,
                            //        }).ToList();
                            var Cat = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _Details.OwnerId)
                                       .Select(x => new OParameter
                                       {
                                           ReferenceKey = x.Category.Guid,
                                           Name = x.Category.Name,
                                       }).ToList();
                            _Details.Categories = Cat;
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            _Details.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.AccountId == _Details.ReferenceId)
                            .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();

                            _ManageConfiguration = new ManageConfiguration();
                            _Details.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _Details.OwnerKey));
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                            #endregion
                        }

                    }
                    else if (_Request.Type == ListType.Merchants)
                    {
                        OThankUCashApp.Response.Info _Details = (from n in _HCoreContext.HCUAccount
                                                                 where n.Guid == _Request.ReferenceKey
                                                                 select new OThankUCashApp.Response.Info
                                                                 {
                                                                     ReferenceId = n.Id,
                                                                     ReferenceKey = n.Guid,
                                                                     Name = n.DisplayName,
                                                                     DisplayName = n.DisplayName,
                                                                     ContactNumber = n.ContactNumber,
                                                                     EmailAddress = n.EmailAddress,
                                                                     IconUrl = n.IconStorage.Path,
                                                                     Address = n.Address,
                                                                     AddressLatitude = n.Latitude,
                                                                     AddressLongitude = n.Longitude,
                                                                     Description = n.Description,
                                                                     CreateDate = n.CreateDate,
                                                                     CountValue = n.CountValue,
                                                                     AverageValue = n.AverageValue
                                                                 }).FirstOrDefault();
                        if (_Details != null)
                        {
                            //_Details.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.Account.Guid == _Details.ReferenceKey &&
                            //                                                         x.TypeId == HelperType.MerchantCategory)
                            //       .Select(x => new OParameter
                            //       {
                            //           ReferenceKey = x.Common.Guid,
                            //           Name = x.Common.Name,
                            //       }).ToList();
                            _Details.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.Account.Guid == _Details.ReferenceKey)
                                .Select(x => new OParameter
                                {
                                    ReferenceKey = x.Category.Guid,
                                    Name = x.Category.Name,
                                }).ToList();
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            _ManageConfiguration = new ManageConfiguration();
                            _Details.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _Details.ReferenceId));
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageList", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Connects the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConnectApp(OThankUCashApp.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Update Device Details
                    HCUAccountDevice DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.AccountId == _Request.UserReference.AccountId && x.SerialNumber == _Request.SerialNumber).FirstOrDefault();
                    if (DeviceDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.NotificationUrl))
                        {
                            DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                        }

                        DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                    }
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ConnectApp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Connects the device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConnectDevice(OThankUCashApp.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _AppResponse = new OThankUCashApp.Response.DeviceConnect();
                    #region CheckAccountDetails
                    string TempPrefix = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var UserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.User.Username == TempPrefix && x.AccountTypeId == UserAccountType.Appuser)
                                                          .Select(x => new
                                                          {
                                                              x.Id,
                                                              x.StatusId,
                                                          })
                                                          .FirstOrDefault();
                    if (UserAccountDetails != null)
                    {
                        _AppResponse.IsAccountExists = true;
                    }
                    else
                    {
                        _AppResponse.IsAccountExists = false;
                    }
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ConnectDevice", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion
    }
}
