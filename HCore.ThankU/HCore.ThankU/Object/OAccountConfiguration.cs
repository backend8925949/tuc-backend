//==================================================================================
// FileName: OAccountConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.ThankU.Object
{
    public class OAccountConfiguration
    {
        public string? UserAccountKey { get; set; }
        public string? ConfigurationKey { get; set; }
        public string? Value { get; set; }
        public string? SubValue { get; set; }
        public string? HelperCode { get; set; }

        public OUserReference? UserReference { get; set; }
    }
}
