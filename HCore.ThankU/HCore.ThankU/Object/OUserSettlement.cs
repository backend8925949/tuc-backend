//==================================================================================
// FileName: OUserSettlement.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Object;
using HCore.Helper;

namespace HCore.ThankU.Object
{
    public class OUserSettlement
    {
        public class Request
        {
            public string? InvoiceKey { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? Comment { get; set; }
            public string? UserAccountKey { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Overview
        {
            public OverviewSettlement Settlements { get; set; }
            public OverviewInvoices Invoices { get; set; }
        }

        public class OverviewSettlement
        {
            public double? Total { get; set; }
            public long? TotalCount { get; set; }
            public double? Completed { get; set; }
            public long? CompletedCount { get; set; }
            public double? Pending { get; set; }
            public long? PendingCount { get; set; }
            public List<OverviewSettlementAccountType> SettlementDistribution { get; set; }
        }
        public class OverviewSettlementAccountType
        {
            public string? Name { get; set; }
            public double? Total { get; set; }
            public long? TotalCount { get; set; }
            public double? Completed { get; set; }
            public long? CompletedCount { get; set; }
            public double? Pending { get; set; }
            public long? PendingCount { get; set; }
        }
        public class OverviewInvoices
        {
            public double? Total { get; set; }
            public long? TotalCount { get; set; }

            public double? Completed { get; set; }
            public long? CompletedCount { get; set; }

            public double? Pending { get; set; }
            public long? PendingCount { get; set; }
        }


        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? UserKey { get; set; }
            public string? UserDisplayName { get; set; }
            public string? Name { get; set; }
            public double? TotalAmount { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? InoviceDate { get; set; }
            public DateTime? CreateDate { get; set; }

            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }


        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeName { get; set; }
            public string? TypeKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserKey { get; set; }
            public string? UserDisplayName { get; set; }


            public string? FromName { get; set; }
            public string? FromAddress { get; set; }
            public string? FromContactNumber { get; set; }
            public string? FromEmailAddress { get; set; }
            public string? FromFax { get; set; }
            public string? ToName { get; set; }
            public string? ToAddress { get; set; }
            public string? ToContactNumber { get; set; }
            public string? ToEmailAddress { get; set; }
            public string? ToFax { get; set; }
            public long? TotalItem { get; set; }
            public double? Amount { get; set; }
            public double? ChargePercentage { get; set; }
            public double? Charge { get; set; }
            public double? DiscountPercentage { get; set; }
            public double? DiscountAmount { get; set; }
            public double? ComissionPercentage { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }
            public DateTime? InoviceDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? Comment { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }


            public List<DetailsItem> Items { get; set; }
        }
        public class DetailsItem
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? InoviceId { get; set; }
            public string? InvoiceKey { get; set; }

            public string? Name { get; set; }
            public string? Description { get; set; }


            public double? UnitCost { get; set; }
            public long? Quantity { get; set; }
            public double? Amount { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public long? Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
