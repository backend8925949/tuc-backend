//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Object;
using HCore.Helper;

namespace HCore.ThankU.Object
{
    public class SliderImages
    {
        public string? PosterUrl { get; set; }
    }

    public class OAnalytics
    {
        internal class Account
        {
            internal long UserId { get; set; }
            internal long? AccountOperationTypeId { get; set; }
            internal long? OwnerParentId { get; set; }
            internal long? OwnerId { get; set; }
            internal long UserAccountId { get; set; }
            internal long? CreatedById { get; set; }
            internal long AccountTypeId { get; set; }
            internal DateTime CreateDate { get; set; }
        }
        public class Request
        {
            public string? Type { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public string? UserKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? OwnerKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Response
        {
            public Rewards Rewards { get; set; }
            public Visitors Vistitors { get; set; }
            public RegSource RegSource { get; set; }
            public Ratings Ratings { get; set; }

            public Invoices Invoices { get; set; }
            public RewardBalance RewardBalance { get; set; }
            public OUsersCount UsersCount { get; set; }
            public List<OUserTransactionType> RewardOverview { get; set; }
            public List<OUserTransactionType> RedeemOverview { get; set; }
            public List<OUserTransactionType> PaymentsOverview { get; set; }

            public List<OUsers> TopRewards { get; set; }
            public List<OUsers> TopRedeems { get; set; }
            public List<OUsers> TopVisitors { get; set; }


        }

        public class Rewards
        {
            public Distribution RewardDistribution { get; set; }

            public double? Reward { get; set; }
            public double? RewardInvoiceAmount { get; set; }
            public long? RewardCount { get; set; }

            public double? Redeem { get; set; }
            public double? RedeemInvoiceAmount { get; set; }
            public long? RedeemCount { get; set; }

            public double? Change { get; set; }
            public double? ChangeInvoiceAmount { get; set; }
            public long? ChangeCount { get; set; }

            public double? InvoiceAmount { get; set; }

            public double? Payments { get; set; }
            public long? PaymentsCount { get; set; }
        }
        public class Invoices
        {
            public long? PaidInvoices { get; set; }
            public long? UnPaidInvoices { get; set; }

            public double? UnpaidInvoiceAmount { get; set; }
            public double? PaidInvoiceAmount { get; set; }


            public double? UnSettledAmount { get; set; }
            public double? SettledAmount { get; set; }

            public double? UserCommission { get; set; }
        }
        public class Ratings
        {
            public double? Average { get; set; }
            public long? Total { get; set; }
            public long? R1 { get; set; }
            public long? R2 { get; set; }
            public long? R3 { get; set; }
            public long? R4 { get; set; }
            public long? R5 { get; set; }
        }
        public class Visitors
        {
            public long? Total { get; set; }
            public long? Single { get; set; }
            public long? Multiple { get; set; }


            public long? Cashier { get; set; }
            public long? Terminal { get; set; }
            public long? PG { get; set; }
        }
        public class OUserTransactionType
        {
            internal long Id { get; set; }
            public string? Name { get; set; }
            public double? Amount { get; set; }
        }
        public class OUsersCount
        {
            public long ConnectedMerchants { get; set; }
            public long ReferredMerchant { get; set; }
            public long Merchant { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }

            public long Cashier { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierDisplayName { get; set; }

            public long Store { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreName { get; set; }


            public long PSSP { get; set; }
            public string? PSSPKey { get; set; }
            public string? PSSPDisplayName { get; set; }

            public long PSTP { get; set; }
            public string? PSTPKey { get; set; }
            public string? PSTPDisplayName { get; set; }

            public long Terminal { get; set; }
            public string? TerminalKey { get; set; }
            public string? TerminalDisplayName { get; set; }

            public long Acquirer { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }

            public long CardUser { get; set; }
            public long MobileUser { get; set; }
            public long AppUser { get; set; }
            public List<UserCards> Cards { get; set; }



            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountType { get; set; }
            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }
            public string? CreatedByOwnerAccountType { get; set; }
        }

        public class UserCards
        {
            public string? ReferenceKey { get; set; }
            public string? CardNumber { get; set; }
            public string? SerialNumber { get; set; }
            public string? CardType { get; set; }
            public DateTime CreateDate { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountType { get; set; }
            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }
            public string? CreatedByOwnerAccountType { get; set; }
        }

        public class RewardBalance
        {
            public double Credit { get; set; }
            public double Debit { get; set; }
            public double Balance { get; set; }
            public double? Comission { get; set; }
            public double? Charge { get; set; }
        }
        internal class OTransactionType
        {
            internal int ReferenceId { get; set; }
            public string? Name { get; set; }
            public string? Value { get; set; }
        }
        public class OUsers
        {
            public string? ReferenceKey { get; set; }
            public string? UserKey { get; set; }
            public string? DisplayName { get; set; }
            public string? ContactNumber { get; set; }
            public long? Count { get; set; }
            public double? Amount { get; set; }
            public DateTime CreateDate { get; set; }
        }

        public class Distribution
        {
            public double? User { get; set; }
            public double? ThankU { get; set; }
            public double? MerchantAcquirer { get; set; }
            public double? UserAcquirer { get; set; }
            public double? POS { get; set; }
            public double? PG { get; set; }
        }
        public class RegSource
        {
            public long? App { get; set; }
            public long? Acquirer { get; set; }
            public long? Cashier { get; set; }
            public long? Terminals { get; set; }
            public long? PG { get; set; }
        }
    }
}
