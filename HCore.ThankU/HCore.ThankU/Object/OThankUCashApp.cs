//==================================================================================
// FileName: OThankUCashApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Object;
using HCore.Helper;

namespace HCore.ThankU.Object
{
    public class OThankUCashApp
    {
        public class Request
        {
            public string? Type { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MobileNumber { get; set; }
            public string? SerialNumber { get; set; }
            public string? OsName { get; set; }
            public string? OsVersion { get; set; }
            public string? Brand { get; set; }
            public string? Model { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string? CarrierName { get; set; }
            public string? CountryCode { get; set; }
            public string? Mcc { get; set; }
            public string? Mnc { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? NotificationUrl { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public class DeviceConnect
            {
                public bool IsAccountExists { get; set; }
            }
            public class AppConnect
            {

            }
            public class Info
            {
                public string? Type { get; set; }
                public string? ReferenceKey { get; set; }
                internal long ReferenceId { get; set; }
                internal long? OwnerId { get; set; }

                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }
                public string? OwnerAddress { get; set; }

                public string? OwnerParentKey { get; set; }
                public string? OwnerParentDisplayName { get; set; }
                public string? OwnerParentAddress { get; set; }

                public string? Description { get; set; }


                public string? Name { get; set; }
                public string? DisplayName { get; set; }
                public string? EmailAddress { get; set; }
                public string? UserName { get; set; }
                public string? ContactNumber { get; set; }
                public string? Address { get; set; }

                public double? RewardPercentage { get; set; }
                public double? AddressLatitude { get; set; }
                public double? AddressLongitude { get; set; }
                public double? ReferralEarning { get; set; }

                public string? IconUrl { get; set; }
                public string? PosterUrl { get; set; }
                public List<string> PosterUrls { get; set; }

                public DateTime CreateDate { get; set; }
                public DateTime? ModifyDate { get; set; }

                public DateTime? LastVisit { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }


                public long? CountValue { get; set; }
                public double? AverageValue { get; set; }


                public int? TotalVisits { get; set; }
                public double? TotalPurchase { get; set; }
                public double? TotalPointReward { get; set; }
                public double? TotalPointRedeem { get; set; }

                public List<OParameter> Categories { get; set; }
            }

        }

    }
}
