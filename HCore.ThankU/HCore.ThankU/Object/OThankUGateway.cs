//==================================================================================
// FileName: OThankUGateway.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Object;
using HCore.Helper;

namespace HCore.ThankU.Object
{
    //internal class OGatewayInfo
    //{
    //    internal long TerminalId { get; set; }
    //    internal string TerminalKey { get; set; }
    //    internal string TerminalDisplayName { get; set; }

    //    internal long MerchantId { get; set; }
    //    //internal string MerchantKey { get; set; }
    //    internal string MerchantDisplayName { get; set; }


    //    internal long? AcquirerId { get; set; }
    //    internal long? AcquirerAccountTypeId { get; set; }

    //}
    //internal class OUserInfo
    //{
    //    internal long? OwnerId { get; set; }
    //    internal long? OwnerAccountTypeId { get; set; }
    //    internal string DisplayName { get; set; }
    //    internal string AccountNumber { get; set; }
    //    internal string MobileNumber { get; set; }
    //    internal string EmailAddress { get; set; }
    //    internal long UserAccountId { get; set; }
    //    internal long? AccountType { get; set; }
    //    //internal long UserMerchantId { get; set; }
    //    //internal string UserAccountKey { get; set; }
    //    internal string Pin { get; set; }
    //    internal long IssuerId { get; set; }
    //    internal int IssuerAccountTypeId { get; set; }


    //    internal long? CreatedById { get; set; }
    //    internal long? CreatedByAccountTypeId { get; set; }
    //}
    //public class OThankUGateway
    //{
    //    public class NotifyPayment
    //    {
    //        public class Request
    //        {
    //            public string? referenceId { get; set; }                 // Unique transaction id
    //            public string? clientId { get; set; }                    // GA Id
    //            public string? userId { get; set; }                      // Mobile Number
    //            public double amount { get; set; }                      // Invoice Amount
    //            public double fees { get; set; }                        // Fees
    //            public string? currency { get; set; }                    // Currency Name 'NGN'
    //            public string? createdAt { get; set; }                   // Transaction Date / Create Date
    //            public string? completedAt { get; set; }                 // Transaction Complete Date / Modify Date
    //            public string? channel { get; set; }                     // Payment Channel  - CARD | ACCOUNT | ACCOUNT_OTP | USSD
    //            public string? status { get; set; }                      // Transaction Status "SUCCESS"
    //            public string? statusMessage { get; set; }               // Status message
    //            public string? notificationStatus { get; set; }          // If notification have been sent to client
    //            public string? providerReference { get; set; }           // Reference number from payment provider
    //            public string? provider { get; set; }                    // Payment processing provider to use
    //            public string? bankCode { get; set; }                    // Payment account back code
    //            public string? bankAccount { get; set; }                 // Payment account bank account number
    //            public string? providerInitiated { get; set; }           // Notification status
    //            public string? platformSettled { get; set; }             // Notification status
    //            public string? providerResponse { get; set; }            // Notification status
    //            public string? hash { get; set; }                        // Notification status
    //            public string? paymentUrl { get; set; }                  // Notification status
    //            public string? callbackURL { get; set; }                 // Notification status
    //            public string? callbackMode { get; set; }                // Notification status
    //            public OPaymentInstruement paymentInstrument { get; set; }
    //            public OUserReference? UserReference { get; set; }
    //        }

    //        public class OPaymentInstruement
    //        {
    //            public string? reference { get; set; }               //  Payment instrument internal reference
    //            public string? userId { get; set; }                  //  User Mobile Number
    //            public string? paymentReference { get; set; }        //  Reference payment (combination of payment referenceand application reference )
    //            public string? active { get; set; }                  //  Status of the payment instrument , default to true
    //            public string? instrumentType { get; set; }          //  CARD , ACCOUNT
    //            public string? provider { get; set; }                //  Payment provider name e.g. posvas
    //            public string? expiresAt { get; set; }               //  Date payment would expiry
    //            public string? reusable { get; set; }                //  If payment instrument can be re used for subsequent transaction
    //            public string? createdAt { get; set; }               //  Time instrument was first used
    //            public string? attributes { get; set; }              //  Instrument specific attribute from provider
    //            public string? masked { get; set; }                  //  Masked Card Number
    //            public string? status { get; set; }                  //  Current status of the payment
    //        }
    //    }
    //    public class NotifySettlement
    //    {
    //        public string? eventd { get; set; }                 // Unique transaction id
    //        public NotifySettlementData data { get; set; }                 // Unique transaction id
    //        public OUserReference? UserReference { get; set; }
    //    }
    //    public class NotifySettlementData
    //    {
    //        public long id { get; set; }
    //        public string? domain { get; set; }
    //        public string? date { get; set; }
    //        public string? currency { get; set; }
    //        public double total_amount { get; set; }
    //        public double total_fees { get; set; }
    //        public double total_processed { get; set; }
    //        public double total_reward { get; set; }
    //        public string? created_at { get; set; }
    //        public object merchants { get; set; }
    //    }

    //    public class RequestTemp
    //    {
    //        public long AccountId { get; set; }
    //        public long AccountType { get; set; }
    //        public string? DisplayName { get; set; }
    //        public long UserId { get; set; }
    //        public string? Pin { get; set; }
    //        public long? CreatedById { get; set; }
    //        public long? CreatedByAccountTypeId { get; set; }
    //    }

    //    public class Request
    //    {

    //        public string? Name { get; set; }
    //        public string? EmailAddress { get; set; }
    //        public string? Gender { get; set; }

    //        public string? TransactionMode { get; set; }


    //        public string? MerchantId { get; set; }
    //        public string? TerminalId { get; set; }

    //        public string? MobileNumber { get; set; }
    //        public string? CardNumber { get; set; }
    //        public string? TagNumber { get; set; }
    //        public string? Pin { get; set; }
    //        public string? SixDigitPan { get; set; }

    //        public string? ReferenceNumber { get; set; }
    //        public long InvoiceAmount { get; set; }
    //        public long RewardAmount { get; set; }
    //        public long RedeemAmount { get; set; }
    //        public long ChangeAmount { get; set; }
    //        public string? TransactionReference { get; set; }

    //        public string? Comment { get; set; }

    //        public OUserReference? UserReference { get; set; }
    //    }

    //    public class Response
    //    {
    //        //public string? MerchantId { get; set; }
    //        //public string? MerchantDisplayName { get; set; }
    //        public DateTime? StartTime { get; set; }
    //        public DateTime? EndTime { get; set; }
    //        public DateTime? TransactionDate { get; set; }

    //        public double? RewardPercentage { get; set; }
    //        public long? RewardAmount { get; set; }
    //        public long? UserRewardAmount { get; set; }
    //        public long? AcquirerAmount { get; set; }
    //        public long? MerchantAmount { get; set; }
    //        public long? ChangeAmount { get; set; }
    //        public long? Balance { get; set; }
    //        public long? ThankUCashPlusBalance { get; set; }
    //        public long? RedeemAmount { get; set; }
    //        public long? Comission { get; set; }
    //        public long? CommissionAmount { get; set; }

    //        public string? MobileNumber { get; set; }
    //        public string? Name { get; set; }
    //        public string? EmailAddress { get; set; }
    //        public string? Gender { get; set; }



    //        public string? ReferenceNumber { get; set; }
    //        public long? InvoiceAmount { get; set; }
    //        public string? TransactionReference { get; set; }

    //        public long? BalanceValidity { get; set; }
    //    }
    //}
}
