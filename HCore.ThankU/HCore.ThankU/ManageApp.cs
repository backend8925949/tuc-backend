//==================================================================================
// FileName: ManageApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================


using HCore.Object;
using HCore.ThankU.Framework;
using HCore.ThankU.Object;
using HCore.Helper;

namespace HCore.ThankU
{
    public class ManageApp
    {
        FrameworkApp _FrameworkApp;
        /// <summary>
        /// Description: Gets the user accounts.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccounts(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetUserAccounts(_Request);
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccount(OThankUCashApp.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetUserAccount(_Request);
        }
        /// <summary>
        /// Description: Connects the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConnectApp(OThankUCashApp.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.ConnectApp(_Request);
        }
        /// <summary>
        /// Description: Connects the device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConnectDevice(OThankUCashApp.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.ConnectDevice(_Request);
        }

    }
}
