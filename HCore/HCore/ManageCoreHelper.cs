//==================================================================================
// FileName: ManageCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;
namespace HCore
{
    public class ManageCoreHelper
    {
        FrameworkCoreHelper _FrameworkCoreHelper;
        /// <summary>
        /// Description: Saves the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.SaveCoreHelper(_Request);
        }

        /// <summary>
        /// return static list of merchants 
        /// </summary>
        /// <returns></returns>
        public object getdealmerchants()
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.getdealmerchants();
        }
        /// <summary>
        /// Description: Saves the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.SaveCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Saves the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.SaveCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.UpdateCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Updates the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.UpdateCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Updates the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.UpdateCoreParameter(_Request);
        }

        /// <summary>
        /// Description: Deletes the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.DeleteCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Deletes the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.DeleteCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Deletes the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.DeleteCoreParameter(_Request);
        }

        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreParameter(_Request);
        }

        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreHelper(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreCommon(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreParameter(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the core usage details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreUsageDetails(OCoreUsage.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreUsageDetails(_Request);
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreUsage(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreUsage(_Request);
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreLog(OCoreLog _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreLog(_Request);
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreLog(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreLog(_Request);
        }
    }
}
