//==================================================================================
// FileName: FrameworkNotificationBroadcaster.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using HCore.Helper;
using HCore.Object;
using MailKit.Net.Smtp;
using MimeKit;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
//using RestSharp;
//using RestSharp.Authenticators;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkEmailBroadcaster
    {

        #region References
        //RestSharp.RestClient _RestClient;
        //RestSharp.RestRequest _RestRequest;
        #endregion
        #region Entity
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        HCLCoreNotification _HCLCoreNotification;
        #endregion
        #region Objects
        //ONotificationDevicePush _ONotificationDevicePush;
        //ONotificationDevicePushBody _ONotificationDevicePushBody;
        #endregion
        #region  Save Notification
        internal OResponse SaveNotification(ONotificationBroadcast _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.TemplateCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");
                }
                else if (string.IsNullOrEmpty(_Request.ToAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    // Notification Template Details
                    var NotificationTemplateDetails = (from n in _HCoreContext.HCCoreCommon
                                                       where n.SystemName == _Request.TemplateCode
                                                       select new
                                                       {
                                                           TemplateId = n.Id,
                                                           //CategoryId = n.CategoryId,
                                                           //NotificationTypeCode = n.NotificationType.SystemName,
                                                           //NotificationTypeId = n.NotificationTypeId,

                                                           Name = n.Name,

                                                           TSubject = n.Value,
                                                           TMessage = n.Description,

                                                           EmailParameter = n.Data,

                                                           CcAddress = n.SubValue,

                                                           //BccAddress = n.BccAddress,

                                                           StatusId = n.StatusId

                                                       }).FirstOrDefault();
                    if (NotificationTemplateDetails != null)
                    {
                        if (NotificationTemplateDetails.StatusId == 2)
                        {
                            //var NotificationGatewayAccountDetails = (from n in _HCoreContext.HCCoreCommon
                            //                                         where n.StatusId == 2 && n.TypeId == _Request.TypeId // NotificationTemplateDetails.NotificationTypeId
                            //                                         select new
                            //                                         {
                            //                                             AccountId = n.Id,
                            //                                             GatewayId = n.GatewayId,
                            //                                             PrivateKey = n.PrivateKey,
                            //                                             PublicKey = n.PublicKey,
                            //                                             ServerName = n.ServerName,
                            //                                             UserName = n.UserName,
                            //                                             Password = n.Password,
                            //                                             FromName = n.FromName,
                            //                                             FromAddress = n.FromAddress,
                            //                                             ReplyToName = n.ReplyToName,
                            //                                             ReplyToAddress = n.ReplyToAddress,
                            //                                             GatewayStatus = n.Gateway.Status,
                            //                                         }).FirstOrDefault();
                            //if (NotificationGatewayAccountDetails != null)
                            //{
                            //if (NotificationGatewayAccountDetails.GatewayStatus == 2)
                            //{
                            var DefaultParameters = _HCoreContext.HCCoreCommon
                                                                      .Where(x => x.StatusId == 2 && x.TypeId == HelperType.NotificationParameter)
                                                                      .Select(x => new
                                                                      {
                                                                          SystemName = x.SystemName,
                                                                          Value = x.Value
                                                                      }).ToList();
                            string Subject = NotificationTemplateDetails.TSubject;
                            string Message = NotificationTemplateDetails.TMessage;
                            string CcAddress = NotificationTemplateDetails.CcAddress;
                            //string BccAddress = NotificationTemplateDetails.BccAddress;
                            #region Add Cc Address
                            if (_Request.Cc != null)
                            {
                                if (_Request.Cc.Count > 0)
                                {
                                    foreach (var Cc in _Request.Cc)
                                    {
                                        if (string.IsNullOrEmpty(CcAddress))
                                        {
                                            CcAddress = Cc.EmailAddress;
                                        }
                                        else
                                        {

                                            CcAddress = CcAddress + "," + Cc.EmailAddress;
                                        }
                                    }
                                }
                            }

                            #endregion
                            #region Process Email Template
                            #region Process Subject
                            #region Replace User Values
                            if (_Request.Parameters != null)
                            {
                                if (_Request.Parameters.Count > 0)
                                {
                                    foreach (var CustomValuesItem in _Request.Parameters)
                                    {
                                        if (!string.IsNullOrEmpty(CustomValuesItem.Name))
                                        {
                                            if (CustomValuesItem.Value != null)
                                            {
                                                Subject = Subject.Replace(CustomValuesItem.Name, CustomValuesItem.Value.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region Replace System Values
                            if (DefaultParameters != null)
                            {
                                if (DefaultParameters.Count > 0)
                                {
                                    foreach (var DefaultEmailParameter in DefaultParameters)
                                    {
                                        if (!string.IsNullOrEmpty(DefaultEmailParameter.Value))
                                        {
                                            Subject = Subject.Replace(DefaultEmailParameter.SystemName, DefaultEmailParameter.Value);
                                        }
                                    }
                                }
                            }

                            #endregion
                            #endregion
                            #region Process Message
                            #region Replace User Values
                            if (_Request.Parameters != null)
                            {
                                if (_Request.Parameters.Count > 0)
                                {
                                    foreach (var CustomValuesItem in _Request.Parameters)
                                    {
                                        if (!string.IsNullOrEmpty(CustomValuesItem.Name))
                                        {
                                            if (CustomValuesItem.Value != null)
                                            {
                                                Message = Message.Replace(CustomValuesItem.Name, CustomValuesItem.Value.ToString());
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                            #region Replace System Values
                            if (DefaultParameters != null)
                            {
                                if (DefaultParameters.Count > 0)
                                {
                                    foreach (var DefaultEmailParameter in DefaultParameters)
                                    {
                                        if (!string.IsNullOrEmpty(DefaultEmailParameter.Value))
                                        {
                                            Message = Message.Replace(DefaultEmailParameter.SystemName, DefaultEmailParameter.Value);
                                        }
                                    }
                                }
                            }
                            #endregion
                            #endregion
                            #endregion
                            #region Save Log
                            string LogKey = HCoreHelper.GenerateGuid();
                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _HCLCoreNotification = new HCLCoreNotification();
                                _HCLCoreNotification.Guid = LogKey;
                                _HCLCoreNotification.TypeId = (long)_Request.TypeId;
                                //_HCLCoreNotification.GatewayId = NotificationGatewayAccountDetails.GatewayId;
                                //_HCLCoreNotification.CategoryId = NotificationTemplateDetails.CategoryId;
                                _HCLCoreNotification.TemplateId = NotificationTemplateDetails.TemplateId;
                                //_HCLCoreNotification.AccountId = NotificationGatewayAccountDetails.AccountId;


                                if (_Request.UserAccountId != 0)
                                {
                                    _HCLCoreNotification.ToUserAccountId = _Request.UserAccountId;
                                }
                                if (_Request.UserDeviceId != 0)
                                {
                                    _HCLCoreNotification.ToUserDeviceId = _Request.UserDeviceId;
                                }

                                if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                                {
                                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                                    if (UserAccountId != 0)
                                    {
                                        _HCLCoreNotification.ToUserAccountId = UserAccountId;
                                    }
                                }

                                if (!string.IsNullOrEmpty(_Request.UserDeviceKey))
                                {
                                    long UserDeviceId = _HCoreContext.HCUAccountDevice.Where(x => x.Guid == _Request.UserDeviceKey).Select(x => x.Id).FirstOrDefault();
                                    if (UserDeviceId != 0)
                                    {
                                        _HCLCoreNotification.ToUserDeviceId = UserDeviceId;
                                    }
                                }

                                _HCLCoreNotification.Priority = _Request.Priority;
                                _HCLCoreNotification.ToName = _Request.ToName;
                                _HCLCoreNotification.ToAddress = _Request.ToAddress;

                                //_HCLCoreNotification.FromName = NotificationGatewayAccountDetails.FromName;
                                //_HCLCoreNotification.FromAddress = NotificationGatewayAccountDetails.FromAddress;

                                _HCLCoreNotification.CcAddress = CcAddress;
                                //_HCLCoreNotification.BccAddress = BccAddress;

                                //_HCLCoreNotification.ReplyToName = NotificationGatewayAccountDetails.ReplyToName;
                                //_HCLCoreNotification.ReplyToAddress = NotificationGatewayAccountDetails.ReplyToAddress;
                                _HCLCoreNotification.Subject = Subject;
                                _HCLCoreNotification.Message = Message;
                                _HCLCoreNotification.SendDate = _Request.SendDate;
                                _HCLCoreNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCLCoreNotification.Attempts = 0;

                                _HCLCoreNotification.StatusId = 1;
                                //if (_Request.TypeId == CoreHelpers.NotificationTypeE.InSystem)
                                //{
                                //    _HCLCoreNotification.Attempts = 1;

                                //    _HCLCoreNotification.ActualSendDate = _HCLCoreNotification.SendDate;
                                //    _HCLCoreNotification.StatusId = 2;
                                //}
                                //else
                                //{
                                //    _HCLCoreNotification.Attempts = 0;

                                //    _HCLCoreNotification.StatusId = 1;
                                //}
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCLCoreNotification.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCoreContextLogging.HCLCoreNotification.Add(_HCLCoreNotification);
                                _HCoreContextLogging.SaveChanges();
                            }
                            #endregion

                            #region Save Template User Count
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TemplateDetails = _HCoreContext.HCCoreCommon
                                                                   .Where(x => x.Id == NotificationTemplateDetails.TemplateId).FirstOrDefault();
                                if (TemplateDetails != null)
                                {
                                    TemplateDetails.Count = TemplateDetails.Count + 1;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            #endregion

                            #region Send High Priority Email Immediately 
                            if (_Request.Priority == 1)
                            {
                                // BroadCastNotification(1, _Request.UserReference);
                            }
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HCNB100");
                            #endregion
                            //}
                            //else
                            //{

                            //    #region Send Response
                            //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");
                            //    #endregion
                            //}

                            //}
                            //else
                            //{
                            //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");
                            //}

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");

                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCNB100");
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveNotification", _Exception, _Request.UserReference);
            }
            #endregion
        }
        #endregion
        #region Process Notification
        internal void BroadCastNotification(int Priority, OUserReference _UserReference)
        {
            //#region Manage Exception
            //try
            //{
            //    using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //    {
            //        using (_HCoreContextLogging = new HCoreContextLogging())
            //        {
            //            DateTime NextDay = HCoreHelper.GetGMTDate().AddDays(3);
            //            var NotificationQueue = (from n in _HCoreContextLogging.HCLCoreNotification
            //                                     where n.StatusId == 1 && n.Priority == Priority && n.SendDate < NextDay
            //                                     orderby n.SendDate ascending
            //                                     select new
            //                                     {
            //                                         TypeCode = n.Type.SystemName,
            //                                         ReferenceId = n.Id,
            //                                         GatewayCode = n.Gateway.SystemName,
            //                                         ToName = n.ToName,
            //                                         ToAddress = n.ToAddress,
            //                                         FromName = n.FromName,
            //                                         FromAddress = n.FromAddress,
            //                                         ReplyToName = n.ReplyToName,
            //                                         ReplyToAddress = n.ReplyToAddress,
            //                                         CcAddress = n.CcAddress,
            //                                         BccAddress = n.BccAddress,
            //                                         Subject = n.Subject,
            //                                         Message = n.Message,
            //                                         SendDate = n.SendDate,

            //                                         UserName = n.Account.Value,
            //                                         Password = n.Account.SubValue,
            //                                         PrivateKey = n.Account.Data,
            //                                         PublicKey = n.Account.Description,
            //                                         ServerName = n.Account.Name,
            //                                         ServerPort = n.Account.Sequence,
            //                                         NotificationAttempts = n.Attempts,
            //                                     })
            //                                .Skip(0)
            //                                .Take(1000)
            //                                .ToList();
            //            foreach (var NotificationDetails in NotificationQueue)
            //            {
            //                if (NotificationDetails.NotificationAttempts < 6)
            //                {
            //                    if (NotificationDetails.TypeCode == "notificationtype.email")
            //                    {
            //                        #region SMTP
            //                        if (NotificationDetails.GatewayCode == "notificationgateway.smtp")
            //                        {
            //                            try
            //                            {
            //                                var _MimeMessage = new MimeMessage();
            //                                _MimeMessage.From.Add(new MailboxAddress(NotificationDetails.FromName, NotificationDetails.FromAddress));
            //                                _MimeMessage.To.Add(new MailboxAddress(NotificationDetails.ToName, NotificationDetails.ToAddress));
            //                                _MimeMessage.Subject = NotificationDetails.Subject;
            //                                string TMessage = "<html><head><title> " + NotificationDetails.Subject + " </title></head><body>" + NotificationDetails.Message + "</body> </html>";

            //                                _MimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            //                                {
            //                                    Text = TMessage
            //                                };

            //                                using (var _SmtpClient = new SmtpClient())
            //                                {
            //                                    _SmtpClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //                                    _SmtpClient.Connect(NotificationDetails.ServerName, (int)NotificationDetails.ServerPort, false);
            //                                    _SmtpClient.Authenticate(NotificationDetails.UserName, NotificationDetails.Password);
            //                                    _SmtpClient.Send(_MimeMessage);
            //                                    _SmtpClient.Disconnect(true);
            //                                }
            //                                using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                                {
            //                                    var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                                    if (EmailLog != null)
            //                                    {
            //                                        EmailLog.ActualSendDate = HCoreHelper.GetGMTDateTime();
            //                                        EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                        if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                        {
            //                                            EmailLog.ModifyById = _UserReference.AccountId;
            //                                        }
            //                                        EmailLog.StatusId = 2;
            //                                        _HCoreContext.SaveChanges();
            //                                    }
            //                                }
            //                            }
            //                            catch (Exception _Exception)
            //                            {
            //                                HCoreHelper.LogException("BroadCastNotification - SMTP ", _Exception, _UserReference);
            //                            }
            //                        }
            //                        #endregion
            //                        #region Mail Gun
            //                        else if (NotificationDetails.GatewayCode == "notificationgateway.mailgun")
            //                        {
            //                            try
            //                            {
            //                                _RestClient = new RestClient();
            //                                _RestClient.BaseUrl = new Uri("https://api.mailgun.net/v3");
            //                                _RestClient.Authenticator = new HttpBasicAuthenticator("api", NotificationDetails.PrivateKey);
            //                                _RestRequest = new RestRequest();
            //                                _RestRequest.AddParameter("domain", NotificationDetails.ServerName, ParameterType.UrlSegment);
            //                                _RestRequest.Resource = NotificationDetails.ServerName + "/messages";
            //                                _RestRequest.AddParameter("from", NotificationDetails.FromName + "<" + NotificationDetails.FromAddress + ">");
            //                                if (!string.IsNullOrEmpty(NotificationDetails.ToName))
            //                                {
            //                                    _RestRequest.AddParameter("to", NotificationDetails.ToName + "<" + NotificationDetails.ToAddress + ">");
            //                                }
            //                                else
            //                                {
            //                                    _RestRequest.AddParameter("to", NotificationDetails.ToAddress);
            //                                }
            //                                _RestRequest.AddParameter("subject", NotificationDetails.Subject);
            //                                string TMessage = "<html><head><title> " + NotificationDetails.Subject + " </title></head><body>" + NotificationDetails.Message + "</body> </html>";
            //                                _RestRequest.AddParameter("html", TMessage);
            //                                _RestRequest.Method = Method.POST;
            //                                IRestResponse _IRestResponse = _RestClient.Execute(_RestRequest);
            //                                string Response = _IRestResponse.Content.ToString();
            //                                MailGunResponse _MailGunResponse = JsonConvert.DeserializeObject<MailGunResponse>(Response);

            //                                if (_MailGunResponse != null)
            //                                {
            //                                    if (!string.IsNullOrEmpty(_MailGunResponse.id))
            //                                    {
            //                                        using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                                        {
            //                                            var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                                            if (EmailLog != null)
            //                                            {
            //                                                EmailLog.ActualSendDate = HCoreHelper.GetGMTDateTime();
            //                                                EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                                if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                                {
            //                                                    EmailLog.ModifyById = _UserReference.AccountId;
            //                                                }
            //                                                EmailLog.StatusId = 2;
            //                                                EmailLog.Attempts = EmailLog.Attempts + 1;
            //                                                EmailLog.ReferenceNumber = _MailGunResponse.id;
            //                                                EmailLog.Description = Response;
            //                                                _HCoreContext.SaveChanges();
            //                                            }
            //                                        }
            //                                    }
            //                                    else
            //                                    {
            //                                        using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                                        {
            //                                            var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                                            if (EmailLog != null)
            //                                            {
            //                                                EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                                if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                                {
            //                                                    EmailLog.ModifyById = _UserReference.AccountId;
            //                                                }
            //                                                EmailLog.Attempts = EmailLog.Attempts + 1;
            //                                                EmailLog.StatusId = 1;
            //                                                EmailLog.Description = Response;
            //                                                _HCoreContext.SaveChanges();
            //                                            }
            //                                        }
            //                                    }

            //                                }
            //                                else
            //                                {
            //                                    using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                                    {
            //                                        var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                                        if (EmailLog != null)
            //                                        {
            //                                            EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                            if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                            {
            //                                                EmailLog.ModifyById = _UserReference.AccountId;
            //                                            }
            //                                            EmailLog.Attempts = EmailLog.Attempts + 1;

            //                                            EmailLog.StatusId = 1;
            //                                            EmailLog.Description = Response;
            //                                            _HCoreContext.SaveChanges();
            //                                        }
            //                                    }
            //                                }

            //                            }
            //                            catch (Exception _Exception)
            //                            {
            //                                HCoreHelper.LogException("BroadCastNotification - Mail Gun ", _Exception, _UserReference);
            //                            }
            //                        }
            //                        else
            //                        {

            //                        }
            //                        #endregion
            //                    }
            //                    else if (NotificationDetails.TypeCode == "notificationtype.devicepush")
            //                    {
            //                        #region Send Push
            //                        sbyte PushStatus = 0;
            //                        string RequestString = "";
            //                        string ResponseString = "";
            //                        string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            //                        HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            //                        _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            //                        _HttpWebRequest.Method = "POST";
            //                        _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", NotificationDetails.PrivateKey));
            //                        using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            //                        {
            //                            _ONotificationDevicePushBody = new ONotificationDevicePushBody();
            //                            _ONotificationDevicePushBody.Task = NotificationDetails.Subject;
            //                            _ONotificationDevicePushBody.Subject = NotificationDetails.Subject;
            //                            _ONotificationDevicePushBody.Message = NotificationDetails.Message;
            //                            _ONotificationDevicePush = new ONotificationDevicePush();
            //                            _ONotificationDevicePush.to = NotificationDetails.ToAddress;
            //                            _ONotificationDevicePush.data = _ONotificationDevicePushBody;
            //                            RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
            //                                      new JsonSerializerSettings()
            //                                      {
            //                                          NullValueHandling = NullValueHandling.Ignore
            //                                      });
            //                            _StreamWriter.Write(RequestString);
            //                            _StreamWriter.Flush();
            //                            _StreamWriter.Close();
            //                        }
            //                        try
            //                        {
            //                            HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
            //                            using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
            //                            {
            //                                ResponseString = _StreamReader.ReadToEnd();
            //                                _StreamReader.Close();
            //                                PushStatus = 1;
            //                            }
            //                        }
            //                        catch (WebException _WebException)
            //                        {
            //                            if (_WebException.Response != null)
            //                            {
            //                                using (var errorResponse = (HttpWebResponse)_WebException.Response)
            //                                {
            //                                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
            //                                    {
            //                                        ResponseString = reader.ReadToEnd();
            //                                        PushStatus = 1;

            //                                    }
            //                                }
            //                            }
            //                        }
            //                        #endregion
            //                        #region Update Log
            //                        using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                        {
            //                            var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                            if (EmailLog != null)
            //                            {
            //                                if (PushStatus == 1)
            //                                {
            //                                    EmailLog.ActualSendDate = HCoreHelper.GetGMTDateTime();
            //                                    EmailLog.StatusId = 2;
            //                                }
            //                                EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                {
            //                                    EmailLog.ModifyById = _UserReference.AccountId;
            //                                }
            //                                EmailLog.Attempts = EmailLog.Attempts + 1;
            //                                EmailLog.Description = ResponseString;
            //                                _HCoreContext.SaveChanges();
            //                            }
            //                        }
            //                        #endregion
            //                    }
            //                    else if (NotificationDetails.TypeCode == "notificationtype.devicetopicpush")
            //                    {
            //                        #region Send Push
            //                        sbyte PushStatus = 0;
            //                        string RequestString = "";
            //                        string ResponseString = "";
            //                        string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            //                        HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            //                        _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            //                        _HttpWebRequest.Method = "POST";
            //                        _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", NotificationDetails.PrivateKey));
            //                        using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            //                        {
            //                            _ONotificationDevicePushBody = new ONotificationDevicePushBody();
            //                            _ONotificationDevicePushBody.Task = NotificationDetails.Subject;
            //                            _ONotificationDevicePushBody.Subject = NotificationDetails.Subject;
            //                            _ONotificationDevicePushBody.Message = NotificationDetails.Message;
            //                            _ONotificationDevicePush = new ONotificationDevicePush();
            //                            _ONotificationDevicePush.to = "/topics/" + NotificationDetails.ToAddress;
            //                            _ONotificationDevicePush.data = _ONotificationDevicePushBody;
            //                            RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
            //                                      new JsonSerializerSettings()
            //                                      {
            //                                          NullValueHandling = NullValueHandling.Ignore
            //                                      });
            //                            _StreamWriter.Write(RequestString);
            //                            _StreamWriter.Flush();
            //                            _StreamWriter.Close();
            //                        }
            //                        try
            //                        {
            //                            HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
            //                            using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
            //                            {
            //                                ResponseString = _StreamReader.ReadToEnd();
            //                                _StreamReader.Close();
            //                                PushStatus = 1;
            //                            }
            //                        }
            //                        catch (WebException _WebException)
            //                        {
            //                            if (_WebException.Response != null)
            //                            {
            //                                using (var errorResponse = (HttpWebResponse)_WebException.Response)
            //                                {
            //                                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
            //                                    {
            //                                        ResponseString = reader.ReadToEnd();
            //                                        PushStatus = 1;

            //                                    }
            //                                }
            //                            }
            //                        }
            //                        #endregion
            //                        #region Update Log
            //                        using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                        {
            //                            var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                            if (EmailLog != null)
            //                            {
            //                                if (PushStatus == 1)
            //                                {
            //                                    EmailLog.ActualSendDate = HCoreHelper.GetGMTDateTime();
            //                                    EmailLog.StatusId = 2;
            //                                }
            //                                EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                                if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                                {
            //                                    EmailLog.ModifyById = _UserReference.AccountId;
            //                                }
            //                                EmailLog.Attempts = EmailLog.Attempts + 1;
            //                                EmailLog.Description = ResponseString;
            //                                _HCoreContext.SaveChanges();
            //                            }
            //                        }
            //                        #endregion
            //                    }
            //                }
            //                else
            //                {
            //                    using (_HCoreContext = new HCoreContext(_UserReference.DbContext))
            //                    {
            //                        var EmailLog = _HCoreContext.HCLCoreNotification.Where(x => x.Id == NotificationDetails.ReferenceId).FirstOrDefault();
            //                        if (EmailLog != null)
            //                        {
            //                            EmailLog.ModifyDate = HCoreHelper.GetGMTDateTime();
            //                            if (_UserReference.AccountId != 0 && _UserReference.AccountTypeId == 1)
            //                            {
            //                                EmailLog.ModifyById = _UserReference.AccountId;
            //                            }
            //                            EmailLog.StatusId = 3;
            //                            _HCoreContext.SaveChanges();
            //                        }
            //                    }
            //                }

            //            }
            //        }
            //    }
            //}
            //catch (Exception _Exception)
            //{
            //    HCoreHelper.LogException("BroadCastNotification ", _Exception, _UserReference);
            //}
            //#endregion
        }
        #endregion

    }
}

