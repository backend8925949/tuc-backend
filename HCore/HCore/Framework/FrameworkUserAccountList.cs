//==================================================================================
// FileName: FrameworkUserAccountList.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Framework
{
    internal class FrameworkUserAccountList
    {
        //HCoreContext _HCoreContext;
        //internal OResponse GetUserAccounts(OList.Request _Request)
        //{
        //    #region Declare
        //    List<OUserAccountList> Data = null;
        //    int TotalRecords = 0;
        //    #endregion
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.Type == "owners")
        //        {
        //            return GetUserAccountOwners(_Request);
        //        }
        //        #region Add Default Request
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
        //            #endregion
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //            #endregion
        //        }
        //        #endregion
        //        #region Set Default Limit
        //        if (_Request.Limit == -1)
        //        {
        //            _Request.Limit = TotalRecords;
        //        }
        //        else if (_Request.Limit == 0)
        //        {
        //            _Request.Limit = DefaultLimit;

        //        }
        //        #endregion
        //        #region Operation
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.Type == ListType.MerchantGateways)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                where n.Account.Guid == _Request.ReferenceKey && n.Owner.AccountTypeId == UserAccountType.PgAccount && n.StatusId == StatusActive
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Owner.Guid,
        //                                    Name = n.Owner.Name,
        //                                    DisplayName = n.Owner.DisplayName,
        //                                    EmailAddress = n.Owner.EmailAddress,
        //                                    UserName = n.Owner.User.Username,
        //                                    ContactNumber = n.Owner.ContactNumber,
        //                                    AccountCode = n.Owner.AccountCode,
        //                                    AccountTypeCode = n.Account.AccountType.SystemName,
        //                                    Address = n.Owner.Address,

        //                                    CountryKey = n.Owner.Country.Guid,
        //                                    CountryName = n.Owner.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.AccountTypeId == UserAccountType.Merchant && n.Account.Guid == _Request.ReferenceKey && n.Owner.AccountTypeId == UserAccountType.PgAccount && n.StatusId == StatusActive
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Owner.Guid,
        //                            Name = n.Owner.Name,


        //                            DisplayName = n.Owner.DisplayName,
        //                            EmailAddress = n.Owner.EmailAddress,
        //                            UserName = n.Owner.User.Username,
        //                            ContactNumber = n.Owner.ContactNumber,
        //                            AccountCode = n.Owner.AccountCode,
        //                            AccountTypeCode = n.Account.AccountType.SystemName,
        //                            Address = n.Owner.Address,

        //                            CountryKey = n.Owner.Country.Guid,
        //                            CountryName = n.Owner.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,



        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.MerchantTerminals)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                where n.Owner.Guid == _Request.ReferenceKey && n.StatusId == StatusActive
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Account.Guid,
        //                                    Name = n.Account.Name,

        //                                    OwnerKey = n.Account.Owner.Guid,
        //                                    OwnerDisplayName = n.Account.Owner.DisplayName,
        //                                    OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                                    DisplayName = n.Account.DisplayName,
        //                                    EmailAddress = n.Account.EmailAddress,
        //                                    UserName = n.Owner.User.Username,
        //                                    ContactNumber = n.Account.ContactNumber,
        //                                    AccountCode = n.Account.AccountCode,
        //                                    AccountTypeCode = n.Account.AccountType.SystemName,

        //                                    Address = n.Account.Address,

        //                                    OwnerParentKey = n.Account.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Account.Owner.DisplayName,


        //                                    CountryKey = n.Account.Country.Guid,
        //                                    CountryName = n.Account.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.Owner.Guid == _Request.ReferenceKey && n.StatusId == StatusActive
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Account.Guid,
        //                            Name = n.Account.Name,

        //                            OwnerKey = n.Account.Owner.Guid,
        //                            OwnerDisplayName = n.Account.Owner.DisplayName,
        //                            OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                            DisplayName = n.Account.DisplayName,
        //                            EmailAddress = n.Account.EmailAddress,
        //                            UserName = n.Owner.User.Username,
        //                            ContactNumber = n.Account.ContactNumber,
        //                            AccountCode = n.Account.AccountCode,
        //                            AccountTypeCode = n.Account.AccountType.SystemName,

        //                            Address = n.Account.Address,

        //                            CountryKey = n.Account.Country.Guid,
        //                            CountryName = n.Account.Country.Name,

        //                            OwnerParentKey = n.Account.Owner.Guid,
        //                            OwnerParentDisplayName = n.Account.Owner.DisplayName,


        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.OwnerView)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                where n.StatusId == StatusActive
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Account.Guid,
        //                                    Name = n.Account.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,
        //                                    OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                                    DisplayName = n.Account.DisplayName,
        //                                    EmailAddress = n.Account.EmailAddress,
        //                                    UserName = n.Account.User.Username,
        //                                    ContactNumber = n.Account.ContactNumber,
        //                                    AccountCode = n.Account.AccountCode,
        //                                    AccountTypeCode = n.Account.AccountType.SystemName,

        //                                    Address = n.Account.Address,

        //                                    OwnerParentKey = n.Account.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Account.Owner.DisplayName,


        //                                    CountryKey = n.Account.Country.Guid,
        //                                    CountryName = n.Account.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByOwnerKey = n.CreatedBy.Owner.Guid
        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.StatusId == StatusActive
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Account.Guid,
        //                            Name = n.Account.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,
        //                            OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                            DisplayName = n.Account.DisplayName,
        //                            EmailAddress = n.Account.EmailAddress,
        //                            UserName = n.Account.User.Username,
        //                            ContactNumber = n.Account.ContactNumber,
        //                            AccountCode = n.Account.AccountCode,
        //                            AccountTypeCode = n.Account.AccountType.SystemName,

        //                            Address = n.Account.Address,

        //                            CountryKey = n.Account.Country.Guid,
        //                            CountryName = n.Account.Country.Name,

        //                            OwnerParentKey = n.Account.Owner.Guid,
        //                            OwnerParentDisplayName = n.Account.Owner.DisplayName,


        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByOwnerKey = n.CreatedBy.Owner.Guid
        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.TerminalView)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                where n.StatusId == StatusActive && n.AccountTypeId == UserAccountType.TerminalAccount && n.Owner.AccountTypeId == UserAccountType.PosAccount
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Account.Guid,
        //                                    Name = n.Account.Name,


        //                                    DisplayName = n.Account.DisplayName, // Terminal
        //                                    UserName = n.Account.User.Username,
        //                                    AccountTypeCode = n.Account.AccountType.SystemName,
        //                                    CountryKey = n.Account.Country.Guid,

        //                                    OwnerKey = n.Account.Owner.Guid, // Provider
        //                                    OwnerDisplayName = n.Account.Owner.DisplayName,

        //                                    OwnerParentKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                                    OwnerParentDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(),

        //                                    StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Owner.DisplayName).FirstOrDefault(),


        //                                    AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.Guid).FirstOrDefault(), // Acquirer
        //                                    AcquirerName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Acquirer

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    CreatedByKey = n.Account.CreatedBy.Guid,
        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                           .Where(m => m.CreatedById == n.UserAccountId
        //                                                            && m.TypeId == TransactionType.CardReward
        //                                                            && m.ModeId == Helpers.TransactionMode.Credit
        //                                                            && m.TransactionDate > _Request.StartDate
        //                                                            && m.TransactionDate < _Request.EndDate
        //                                                           && m.ModeId == TransactionMode.Credit
        //                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
        //                                                            && m.StatusId == HelperStatus.Transaction.Success
        //                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0,
        //                                    //LastLoginDate = _HCoreContext.HCUAccountTransaction
        //                                    //.Where(m => m.CreatedById == n.Id
        //                                    // && m.TypeId == TransactionType.CardReward
        //                                    // && m.ModeId == Helpers.TransactionMode.Credit
        //                                    // && m.TransactionDate > _Request.StartDate
        //                                    // && m.TransactionDate < _Request.EndDate
        //                                    //&& m.ModeId == TransactionMode.Credit
        //                                    //&& (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
        //                                    //&& m.StatusId == HelperStatus.Transaction.Success
        //                                    //).OrderByDescending(x=>x.TransactionDate).Select(x=>x.TransactionDate).FirstOrDefault(),
        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.StatusId == StatusActive && n.AccountTypeId == UserAccountType.TerminalAccount && n.Owner.AccountTypeId == UserAccountType.PosAccount
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Account.Guid,
        //                            Name = n.Account.Name,


        //                            DisplayName = n.Account.DisplayName, // Terminal
        //                            UserName = n.Account.User.Username,
        //                            AccountTypeCode = n.Account.AccountType.SystemName,
        //                            CountryKey = n.Account.Country.Guid,

        //                            OwnerKey = n.Account.Owner.Guid, // Provider
        //                            OwnerDisplayName = n.Account.Owner.DisplayName,

        //                            OwnerParentKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                            OwnerParentDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(),

        //                            StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Owner.DisplayName).FirstOrDefault(),

        //                            AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.Guid).FirstOrDefault(), // Acquirer
        //                            AcquirerName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Acquirer

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            CreatedByKey = n.Account.CreatedBy.Guid,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                            TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                           .Where(m => m.CreatedById == n.UserAccountId
        //                                                            && m.TypeId == TransactionType.CardReward
        //                                                            && m.ModeId == Helpers.TransactionMode.Credit
        //                                                            && m.TransactionDate > _Request.StartDate
        //                                                            && m.TransactionDate < _Request.EndDate
        //                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
        //                                                            && m.StatusId == HelperStatus.Transaction.Success
        //                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0,
        //                            //LastLoginDate = _HCoreContext.HCUAccountTransaction
        //                            //.Where(m => m.CreatedById == n.Id
        //                            // && m.TypeId == TransactionType.CardReward
        //                            // && m.ModeId == Helpers.TransactionMode.Credit
        //                            // && m.TransactionDate > _Request.StartDate
        //                            // && m.TransactionDate < _Request.EndDate
        //                            //&& m.ModeId == TransactionMode.Credit
        //                            //&& (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
        //                            //&& m.StatusId == HelperStatus.Transaction.Success
        //                            //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),

        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.AcquirerMerchantView)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                where n.StatusId == StatusActive && ((n.AccountTypeId == UserAccountType.TerminalAccount && n.Owner.AccountTypeId == UserAccountType.PosAccount)
        //                                                                   || (n.AccountTypeId == UserAccountType.Merchant && n.Owner.AccountTypeId == UserAccountType.Acquirer)
        //                                                                  )
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                                    Name = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Merchant
        //                                    DisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Merchant
        //                                    Address = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Address).FirstOrDefault(), // Merchant



        //                                    AccountTypeCode = n.Account.AccountType.SystemName,
        //                                    CountryKey = n.Account.Country.Guid,

        //                                    OwnerKey = n.Account.Owner.Guid, // Provider
        //                                    OwnerDisplayName = n.Account.Owner.DisplayName,

        //                                    OwnerParentKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                                    OwnerParentDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(),


        //                                    AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.Guid).FirstOrDefault(), // Acquirer
        //                                    AcquirerName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Acquirer

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                }).GroupBy(x => x.ReferenceKey)
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.StatusId == StatusActive && ((n.AccountTypeId == UserAccountType.TerminalAccount && n.Owner.AccountTypeId == UserAccountType.PosAccount)
        //                                                                   || (n.AccountTypeId == UserAccountType.Merchant && n.Owner.AccountTypeId == UserAccountType.Acquirer)
        //                                                                  )
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                            Name = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Merchant
        //                            DisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Merchant
        //                            Address = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Address).FirstOrDefault(), // Merchant



        //                            AccountTypeCode = n.Account.AccountType.SystemName,
        //                            CountryKey = n.Account.Country.Guid,

        //                            OwnerKey = n.Account.Owner.Guid, // Provider
        //                            OwnerDisplayName = n.Account.Owner.DisplayName,

        //                            OwnerParentKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.Guid).FirstOrDefault(), // Merchant
        //                            OwnerParentDisplayName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.Owner.DisplayName).FirstOrDefault(),


        //                            AcquirerKey = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.Guid).FirstOrDefault(), // Acquirer
        //                            AcquirerName = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == n.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Acquirer).Select(x => x.Owner.DisplayName).FirstOrDefault(), // Acquirer

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.OwnerViewUnique)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Account.Guid,
        //                                    Name = n.Account.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,
        //                                    OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                                    DisplayName = n.Account.DisplayName,
        //                                    EmailAddress = n.Account.EmailAddress,
        //                                    UserName = n.Account.User.Username,
        //                                    ContactNumber = n.Account.ContactNumber,
        //                                    AccountCode = n.Account.AccountCode,
        //                                    AccountTypeCode = n.Account.AccountType.SystemName,

        //                                    Address = n.Account.Address,

        //                                    OwnerParentKey = n.Account.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Account.Owner.DisplayName,



        //                                    CountryKey = n.Account.Country.Guid,
        //                                    CountryName = n.Account.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                })
        //                    .Distinct()
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccountOwner
        //                        where n.StatusId == StatusActive
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Account.Guid,
        //                            Name = n.Account.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,
        //                            OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                            DisplayName = n.Account.DisplayName,
        //                            EmailAddress = n.Account.EmailAddress,
        //                            UserName = n.Account.User.Username,
        //                            ContactNumber = n.Account.ContactNumber,
        //                            AccountCode = n.Account.AccountCode,
        //                            AccountTypeCode = n.Account.AccountType.SystemName,

        //                            Address = n.Account.Address,

        //                            CountryKey = n.Account.Country.Guid,
        //                            CountryName = n.Account.Country.Name,

        //                            OwnerParentKey = n.Account.Owner.Guid,
        //                            OwnerParentDisplayName = n.Account.Owner.DisplayName,

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                        })
        //                    .Distinct()
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == ListType.ReferralUsers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.Appuser && (n.CreatedBy.Guid == _Request.ReferenceKey || n.Owner.Guid == _Request.ReferenceKey)
        //                                select new OUserAccountList
        //                                {

        //                                    UserId = n.UserId,
        //                                    ReferenceKey = n.Guid,
        //                                    IconUrl = n.IconStorage.Path,

        //                                    Name = n.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,


        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    ReferralEarning = 0,
        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.Appuser && (n.CreatedBy.Guid == _Request.ReferenceKey || n.Owner.Guid == _Request.ReferenceKey)
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceId = n.Id,
        //                            UserId = n.UserId,
        //                            ReferenceKey = n.Guid,
        //                            IconUrl = n.IconStorage.Path,

        //                            Name = n.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                            ReferralEarning = 0,


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //                if (Data.Count > 0)
        //                {
        //                    foreach (var DataItem in Data)
        //                    {


        //                        DataItem.ReferralEarning = _HCoreContext.HCUAccountTransaction
        //                                          .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                                 && x.StatusId == StatusHelperId.Transaction_Success
        //                                                 && x.ModeId == TransactionMode.Credit
        //                                                 && x.TypeId == TransactionType.ReferralBonus
        //                                                 && x.SourceId == TransactionSource.TUC
        //                                                )
        //                            .Sum(x => (double?)x.TotalAmount) ?? 0;
        //                    }
        //                }
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_UserAccounts && _Request.Type == ListType.AcquirerMerchants)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.Merchant
        //                                && (
        //                                    (n.Owner.Guid == _Request.ReferenceKey) ||
        //                                    (_HCoreContext.HCUAccountOwner
        //                                     .Where(x => x.OwnerId == n.Id
        //                                            && x.AccountTypeId == UserAccountType.TerminalAccount
        //                                            && _HCoreContext.HCUAccountOwner
        //                                                .Where(b => b.Owner.Guid == _Request.ReferenceKey
        //                                                      && b.UserAccountId == x.UserAccountId).FirstOrDefault() != null).FirstOrDefault() != null)
        //                                   )
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    AccountTypeCode = n.AccountType.SystemName,
        //                                    AccountTypeName = n.AccountType.Name,

        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    Name = n.Name,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    Address = n.Address,
        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.Merchant
        //                        && (
        //                            (n.Owner.Guid == _Request.ReferenceKey) ||
        //                            (_HCoreContext.HCUAccountOwner
        //                             .Where(x => x.OwnerId == n.Id
        //                                    && x.AccountTypeId == UserAccountType.TerminalAccount
        //                                    && _HCoreContext.HCUAccountOwner
        //                                        .Where(b => b.Owner.Guid == _Request.ReferenceKey
        //                                              && b.UserAccountId == x.UserAccountId).FirstOrDefault() != null).FirstOrDefault() != null)
        //                           )
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            AccountTypeCode = n.AccountType.SystemName,
        //                            AccountTypeName = n.AccountType.Name,

        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,
        //                            Name = n.Name,
        //                            OwnerParentKey = n.Owner.Owner.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,
        //                            Address = n.Address,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_UserAccounts && _Request.Type == ListType.Settlements)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where ((n.HCUAccountTransactionAccount.Where(x => x.AccountId == n.Id &&
        //                                                 x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                 x.ModeId == TransactionMode.Credit &&
        //                                                 x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) - (n.HCUAccountTransactionAccount.Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                  x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)) > 0
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    Name = n.Name,
        //                                    AccountTypeCode = n.AccountType.SystemName,
        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                    Credit = _HCoreContext.HCUAccountTransaction
        //                                           .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                    Debit = _HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                              x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                    Balance = ((_HCoreContext.HCUAccountTransaction
        //                                           .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                      (_HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                              x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where ((n.HCUAccountTransactionAccount.Where(x => x.AccountId == n.Id &&
        //                                         x.StatusId == StatusHelperId.Transaction_Success &&
        //                                         x.ModeId == TransactionMode.Credit &&
        //                                         x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0) - (n.HCUAccountTransactionAccount.Where(x => x.AccountId == n.Id &&
        //                                          x.StatusId == StatusHelperId.Transaction_Success &&
        //                                          x.ModeId == TransactionMode.Debit &&
        //                                          x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0)) > 0
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            Name = n.Name,
        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,
        //                            AccountTypeCode = n.AccountType.SystemName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            Credit = _HCoreContext.HCUAccountTransaction
        //                                   .Where(x => x.AccountId == n.Id &&
        //                                          x.StatusId == StatusHelperId.Transaction_Success &&
        //                                          x.ModeId == TransactionMode.Credit &&
        //                                          x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                            Debit = _HCoreContext.HCUAccountTransaction
        //                                               .Where(x => x.AccountId == n.Id &&
        //                                          x.StatusId == StatusHelperId.Transaction_Success &&
        //                                          x.ModeId == TransactionMode.Debit &&
        //                                                      x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                            Balance = ((_HCoreContext.HCUAccountTransaction
        //                                   .Where(x => x.AccountId == n.Id &&
        //                                          x.StatusId == StatusHelperId.Transaction_Success &&
        //                                          x.ModeId == TransactionMode.Credit &&
        //                                          x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                              (_HCoreContext.HCUAccountTransaction
        //                                               .Where(x => x.AccountId == n.Id &&
        //                                          x.StatusId == StatusHelperId.Transaction_Success &&
        //                                          x.ModeId == TransactionMode.Debit &&
        //                                                      x.SourceId == TransactionSource.Settlement)
        //                                       .Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion

        //                foreach (var item in Data)
        //                {
        //                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.ReferenceId));
        //                }
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_UserAccounts && _Request.Type == ListType.Invoices)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.HCUserInvoiceUserAccount.Count() > 0
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    Name = n.Name,
        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                    PaidInvoices = n.HCUserInvoiceUserAccount.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Count(),
        //                                    UnpaidInvoices = n.HCUserInvoiceUserAccount.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Count(),
        //                                    TotalInvoices = n.HCUserInvoiceUserAccount.Count(),


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.HCUserInvoiceUserAccount.Count() > 0
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            Name = n.Name,
        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            PaidInvoices = n.HCUserInvoiceUserAccount.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Count(),
        //                            UnpaidInvoices = n.HCUserInvoiceUserAccount.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Count(),
        //                            TotalInvoices = n.HCUserInvoiceUserAccount.Count(),


        //                        }).Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion

        //                foreach (var item in Data)
        //                {
        //                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.ReferenceId));
        //                }
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_UserAccounts && _Request.Type == ListType.Merchants)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.IconStorageId != null && n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
        //                        && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    Name = n.Name,

        //                                    DisplayName = n.DisplayName,
        //                                    ContactNumber = n.ContactNumber,
        //                                    EmailAddress = n.EmailAddress,

        //                                    IconUrl = n.IconStorage.Path,
        //                                    PosterUrl = n.PosterStorage.Path,

        //                                    Address = n.Address,
        //                                    AddressLatitude = n.Latitude,
        //                                    AddressLongitude = n.Longitude,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.IconStorageId != null && n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
        //                        && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,
        //                            Name = n.Name,

        //                            DisplayName = n.DisplayName,
        //                            ContactNumber = n.ContactNumber,
        //                            EmailAddress = n.EmailAddress,

        //                            IconUrl = n.IconStorage.Path,
        //                            PosterUrl = n.PosterStorage.Path,

        //                            Address = n.Address,
        //                            AddressLatitude = n.Latitude,
        //                            AddressLongitude = n.Longitude,

        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_Stores && _Request.Type == ListType.CardUsers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.MerchantStore &&
        //                                n.Owner.StatusId == StatusActive &&
        //                                n.StatusId == StatusActive
        //                                && n.Owner.IconStorageId != null
        //                                && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
        //                                //&& (n.OwnerId != 3 && n.OwnerId != 971)
        //                                && (n.OwnerId != 3)
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    Name = n.DisplayName,
        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    OwnerKey = n.Owner.Guid,
        //                                    Description = n.Owner.Description,
        //                                    OwnerDisplayName = n.Owner.DisplayName,
        //                                    DisplayName = n.DisplayName,
        //                                    ContactNumber = n.ContactNumber,
        //                                    IconUrl = n.IconStorage.Path,
        //                                    Address = n.Address,
        //                                    AddressLatitude = n.Latitude,
        //                                    AddressLongitude = n.Longitude,
        //                                    CountryKey = n.Country.Guid,
        //                                    Status = n.StatusId,

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.MerchantStore &&
        //                        n.Owner.StatusId == StatusActive &&
        //                                n.StatusId == StatusActive
        //                                && n.Owner.IconStorageId != null

        //                        && (n.OwnerId != 3 && n.OwnerId != 971 && n.OwnerId != 10 && n.OwnerId != 6967)
        //                        //&& (n.OwnerId != 3 && n.OwnerId != 971)
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceId = n.Id,
        //                            ReferenceKey = n.Guid,
        //                            Name = n.DisplayName,
        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            OwnerId = n.OwnerId,
        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,
        //                            Description = n.Owner.Description,
        //                            DisplayName = n.DisplayName,
        //                            ContactNumber = n.ContactNumber,
        //                            IconUrl = n.Owner.IconStorage.Path,
        //                            Address = n.Address,
        //                            AddressLatitude = n.Latitude,
        //                            AddressLongitude = n.Longitude,
        //                            CountryKey = n.Country.Guid,
        //                            Status = n.StatusId,


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //                foreach (var item in Data)
        //                {
        //                    item.PosterUrls = _HCoreContext.HCCoreStorage.Where(x => x.AccountId == item.ReferenceId)
        //                        .Select(x => _AppConfig.StorageUrl + "" + x.Path).ToList();
        //                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", (long)item.OwnerId));
        //                }
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_Merchants && _Request.Type == ListType.CardUsers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.IconStorageId != null && n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
        //                        && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    Name = n.Name,

        //                                    DisplayName = n.DisplayName,
        //                                    ContactNumber = n.ContactNumber,
        //                                    EmailAddress = n.EmailAddress,

        //                                    IconUrl = n.IconStorage.Path,
        //                                    PosterUrl = n.PosterStorage.Path,

        //                                    Address = n.Address,
        //                                    AddressLatitude = n.Latitude,
        //                                    AddressLongitude = n.Longitude,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.IconStorageId != null && n.AccountTypeId == UserAccountType.Merchant && n.StatusId == StatusActive
        //                        && (n.Id != 3 && n.Id != 971 && n.Id != 10 && n.Id != 6967)
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,
        //                            Name = n.Name,

        //                            DisplayName = n.DisplayName,
        //                            ContactNumber = n.ContactNumber,
        //                            EmailAddress = n.EmailAddress,

        //                            IconUrl = n.IconStorage.Path,
        //                            PosterUrl = n.PosterStorage.Path,

        //                            Address = n.Address,
        //                            AddressLatitude = n.Latitude,
        //                            AddressLongitude = n.Longitude,

        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_Stores)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.MerchantStore
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    Name = n.Name,


        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    DisplayName = n.DisplayName,
        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    Address = n.Address,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                    //Balance = ((_HCoreContext.HCUAccountTransaction
        //                                    //.Where(x => x.AccountId == n.Id &&
        //                                    //       x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //       x.ModeId == TransactionMode.Credit &&
        //                                    //       x.SourceId == TransactionSource.Store)
        //                                    //    .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                    //            (_HCoreContext.HCUAccountTransaction
        //                                    //.Where(x => x.AccountId == n.Id &&
        //                                    //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //   x.ModeId == TransactionMode.Debit &&
        //                                    //   x.SourceId == TransactionSource.Store)
        //                                    //.Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.MerchantStore
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,
        //                            Name = n.Name,


        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            Address = n.Address,

        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                            //Balance = ((_HCoreContext.HCUAccountTransaction
        //                            //.Where(x => x.AccountId == n.Id &&
        //                            //       x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //       x.ModeId == TransactionMode.Credit &&
        //                            //       x.SourceId == TransactionSource.Store)
        //                            //    .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                            //            (_HCoreContext.HCUAccountTransaction
        //                            //.Where(x => x.AccountId == n.Id &&
        //                            //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //   x.ModeId == TransactionMode.Debit &&
        //                            //   x.SourceId == TransactionSource.Store)
        //                            //.Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_Merchants)
        //            {

        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.Merchant

        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    Name = n.Name,
        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    Description = n.Description,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                    //Balance = ((_HCoreContext.HCUAccountTransaction
        //                                    //.Where(x => x.AccountId == n.Id &&
        //                                    //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //   x.ModeId == TransactionMode.Credit &&
        //                                    //   x.SourceId == TransactionSource.Merchant)
        //                                    //.Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                    //       (_HCoreContext.HCUAccountTransaction
        //                                    //        .Where(x => x.CreatedBy.Owner.Owner.Id == n.Id &&
        //                                    //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //   x.ModeId == TransactionMode.Debit &&
        //                                    //   x.SourceId == TransactionSource.Cashier)
        //                                    //.Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                                    PendingSettlement = ((_HCoreContext.HCUAccountTransaction
        //                                           .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                      (_HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                              x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.Merchant
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceId = n.Id,
        //                            ReferenceKey = n.Guid,

        //                            Name = n.Name,
        //                            IconUrl = n.IconStorage.Path,
        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,


        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,
        //                            Description = n.Description,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                            //Balance = ((_HCoreContext.HCUAccountTransaction
        //                            //.Where(x => x.AccountId == n.Id &&
        //                            //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //   x.ModeId == TransactionMode.Credit &&
        //                            //   x.SourceId == TransactionSource.Merchant)
        //                            //.Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                            //       (_HCoreContext.HCUAccountTransaction
        //                            //        .Where(x => x.CreatedBy.Owner.Owner.Id == n.Id &&
        //                            //   x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //   x.ModeId == TransactionMode.Debit &&
        //                            //   x.SourceId == TransactionSource.Cashier)
        //                            //.Sum(x => (double?)x.TotalAmount) ?? 0)),

        //                            PendingSettlement = ((_HCoreContext.HCUAccountTransaction
        //                                           .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                      (_HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.AccountId == n.Id &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                              x.SourceId == TransactionSource.Settlement)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)),

        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion

        //                foreach (var item in Data)
        //                {
        //                    item.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", item.ReferenceId));
        //                }
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_Cashiers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.MerchantCashier
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    DisplayName = n.DisplayName,
        //                                    Name = n.Name,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    //Balance = ((_HCoreContext.HCUAccountTransaction
        //                                    //    .Where(x => x.AccountId == n.Id &&
        //                                    //           x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //           x.ModeId == TransactionMode.Credit &&
        //                                    //           x.SourceId == TransactionSource.Cashier)
        //                                    //.Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                    //             (_HCoreContext.HCUAccountTransaction
        //                                    //.Where(x => x.AccountId == n.Id &&
        //                                    //x.StatusId == StatusHelperId.Transaction_Success &&
        //                                    //x.ModeId == TransactionMode.Debit &&
        //                                    //x.SourceId == TransactionSource.Cashier)
        //                                    //.Sum(x => (double?)x.TotalAmount) ?? 0)),


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.MerchantCashier
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,
        //                            IconUrl = n.IconStorage.Path,

        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,
        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,
        //                            Name = n.Name,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,
        //                            //Balance = ((_HCoreContext.HCUAccountTransaction
        //                            //    .Where(x => x.AccountId == n.Id &&
        //                            //           x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //           x.ModeId == TransactionMode.Credit &&
        //                            //           x.SourceId == TransactionSource.Cashier)
        //                            //.Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                            //             (_HCoreContext.HCUAccountTransaction
        //                            //.Where(x => x.AccountId == n.Id &&
        //                            //x.StatusId == StatusHelperId.Transaction_Success &&
        //                            //x.ModeId == TransactionMode.Debit &&
        //                            //x.SourceId == TransactionSource.Cashier)
        //                            //.Sum(x => (double?)x.TotalAmount) ?? 0)),

        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (!string.IsNullOrEmpty(_Request.ReferenceKey)
        //                     && _Request.UserReference.Task == HCoreTask.Get_CardUsers
        //                     && _Request.Type == ListType.CardUserCashierVisit)
        //            {


        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where (n.AccountTypeId == UserAccountType.Appuser)
        //                                && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                && x.CreatedBy.Guid == _Request.ReferenceKey).Count() > 0
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    UserKey = n.Guid,
        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    ReferralCode = n.ReferralCode,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    CardKey = n.Card.Guid,

        //                                    DisplayName = n.DisplayName,
        //                                    Name = n.Name,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,


        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                               && x.CreatedBy.Guid == _Request.ReferenceKey
        //                                                                                               && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),


        //                                    //LastVisit = _HCoreContext.HCUAccountTransaction
        //                                    //            .Where(x => x.Account.UserId == n.UserId &&
        //                                    //                   x.CreatedBy.Guid == _Request.ReferenceKey
        //                                    //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")
        //                                    //&& x.StatusId == StatusHelperId.Transaction_Success
        //                                    //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                                    TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                                        x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                                    TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                    TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where (n.AccountTypeId == UserAccountType.Appuser)
        //                        && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                        && x.CreatedBy.Guid == _Request.ReferenceKey).Count() > 0
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            UserKey = n.Guid,

        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            CardKey = n.Card.Guid,

        //                            DisplayName = n.DisplayName,
        //                            Name = n.Name,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            ReferralCode = n.ReferralCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,


        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                       && x.CreatedBy.Guid == _Request.ReferenceKey
        //                                                                                                       && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),

        //                            //LastVisit = _HCoreContext.HCUAccountTransaction
        //                            //            .Where(x => x.Account.UserId == n.UserId &&
        //                            //                   x.CreatedBy.Guid == _Request.ReferenceKey
        //                            //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")
        //                            //&& x.StatusId == StatusHelperId.Transaction_Success
        //                            //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                            TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                                        x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                            TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                            TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,



        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (!string.IsNullOrEmpty(_Request.ReferenceKey)
        //                     && _Request.UserReference.Task == HCoreTask.Get_CardUsers
        //                     && _Request.Type == ListType.CardUserStoreVisit)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where (n.AccountTypeId == UserAccountType.Appuser)
        //                                && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                && x.CreatedBy.Owner.Guid == _Request.ReferenceKey).Count() > 0
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,
        //                                    UserKey = n.Guid,

        //                                    ReferralCode = n.ReferralCode,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    CardKey = n.Card.Guid,

        //                                    DisplayName = n.DisplayName,
        //                                    Name = n.Name,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,


        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                               && x.CreatedBy.Owner.Guid == _Request.ReferenceKey
        //                                                                                               && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),

        //                                    //LastVisit = _HCoreContext.HCUAccountTransaction
        //                                    //            .Where(x => x.Account.UserId == n.UserId &&
        //                                    //                   x.CreatedBy.Owner.Guid == _Request.ReferenceKey
        //                                    //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                    //&& x.StatusId == StatusHelperId.Transaction_Success
        //                                    //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                                    TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                                        x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                                    TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                    TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where (n.AccountTypeId == UserAccountType.Appuser)
        //                        && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                        && x.CreatedBy.Owner.Guid == _Request.ReferenceKey).Count() > 0
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,
        //                            UserKey = n.Guid,


        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            CardKey = n.Card.Guid,

        //                            DisplayName = n.DisplayName,
        //                            Name = n.Name,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            ReferralCode = n.ReferralCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,


        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                       && x.CreatedBy.Owner.Guid == _Request.ReferenceKey
        //                                                                                                       && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),

        //                            //LastVisit = _HCoreContext.HCUAccountTransaction
        //                            //            .Where(x => x.Account.UserId == n.UserId &&
        //                            //                   x.CreatedBy.Owner.Guid == _Request.ReferenceKey
        //                            //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                            //&& x.StatusId == StatusHelperId.Transaction_Success
        //                            //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                            TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                                        x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                            TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                            TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.CreatedBy.Owner.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,



        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (!string.IsNullOrEmpty(_Request.ReferenceKey)
        //                     && _Request.UserReference.Task == HCoreTask.Get_CardUsers
        //                     && _Request.Type == ListType.CardUserMerchantVisit)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where (n.AccountTypeId == UserAccountType.Appuser)
        //                                && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                         && x.Parent.Guid == _Request.ReferenceKey).Count() > 0
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    UserKey = n.Guid,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    ReferralCode = n.ReferralCode,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    CardKey = n.Card.Guid,

        //                                    DisplayName = n.DisplayName,
        //                                    Name = n.Name,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,


        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                               && x.Parent.Guid == _Request.ReferenceKey
        //                                                                                               && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),

        //                                    //LastVisit = _HCoreContext.HCUAccountTransaction
        //                                    //            .Where(x => x.Account.UserId == n.UserId &&
        //                                    // x.Parent.Guid == _Request.ReferenceKey
        //                                    //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                    //&& x.StatusId == StatusHelperId.Transaction_Success
        //                                    //).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                                    TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                                    TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                  x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                    TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where (n.AccountTypeId == UserAccountType.Appuser)
        //                        && _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                        && x.Parent.Guid == _Request.ReferenceKey).Count() > 0
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,
        //                            UserKey = n.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,


        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            CardKey = n.Card.Guid,

        //                            DisplayName = n.DisplayName,
        //                            Name = n.Name,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,

        //                            ReferralCode = n.ReferralCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,


        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == n.UserId
        //                                                                                                       && x.Parent.Guid == _Request.ReferenceKey
        //                                                                                                       && (x.Type.Value == "reward" || x.Type.Value == "redeem")

        //                                                                                              ).Count(),

        //                            //LastVisit = _HCoreContext.HCUAccountTransaction
        //                            //            .Where(x => x.Account.UserId == n.UserId &&
        //                            // x.Parent.Guid == _Request.ReferenceKey
        //                            //                   && (x.Type.Value == "reward" || x.Type.Value == "redeem")
        //                            //&& x.StatusId == StatusHelperId.Transaction_Success
        //                            //).OrderByDescending(x => x.CreateDate).Select(x => x.CreateDate).FirstOrDefault(),
        //                            TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                        (x.Type.Value == "reward" || x.Type.Value == "redeem") &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                            TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                  x.Account.UserId == n.UserId &&
        //                                                  x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "reward" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                            TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                    .Where(x =>
        //                                                                           x.Account.UserId == n.UserId &&
        //                                                                           x.Parent.Guid == _Request.ReferenceKey &&
        //                                                                           x.Type.Value == "redeem" &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit)
        //                                                    .Sum(x => (double?)x.TotalAmount) ?? 0,



        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (!string.IsNullOrEmpty(_Request.ReferenceKey)
        //                     && _Request.UserReference.Task == HCoreTask.Get_CardUsers
        //                     && _Request.Type == ListType.CardUserAllVisit)
        //            {
        //                var TTotalRecords = (from n in _HCoreContext.HCUAccountTransaction
        //                                     where n.Account.AccountTypeId == UserAccountType.Appuser && n.Account.Guid == _Request.ReferenceKey
        //                                     select new OStoreList
        //                                     {
        //                                         ReferenceKey = n.Guid,

        //                                         StoreKey = n.CreatedBy.Owner.Guid,
        //                                         StoreDisplayName = n.CreatedBy.Owner.DisplayName,
        //                                         AddressLatitude = n.CreatedBy.Owner.Latitude,
        //                                         AddressLongitude = n.CreatedBy.Owner.Longitude,

        //                                         MerchantKey = n.CreatedBy.Owner.Owner.Guid,
        //                                         MerchantDisplayName = n.CreatedBy.Owner.Owner.DisplayName,
        //                                         IconUrl = n.CreatedBy.Owner.Owner.IconStorage.Path,
        //                                         PosterUrl = n.CreatedBy.Owner.Owner.PosterStorage.Path,


        //                                         TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == n.UserAccountId && x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid).Count(),

        //                                         LastVisit = _HCoreContext.HCUAccountTransaction
        //                                                .Where(x => x.AccountId == n.UserAccountId &&
        //                                                       x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                       x.StatusId == StatusHelperId.Transaction_Success
        //                                                       ).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                                         TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                      .Where(x => x.AccountId == n.UserAccountId &&
        //                                                       x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                       x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                       x.ModeId == TransactionMode.Credit)
        //                                                         .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                                         TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                         .Where(x => x.AccountId == n.UserAccountId &&
        //                                                       x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                       x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                       x.ModeId == TransactionMode.Credit)
        //                                                         .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                         TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                         .Where(x => x.AccountId == n.UserAccountId &&
        //                                                       x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                       x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                       x.ModeId == TransactionMode.Debit)
        //                                                         .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                     }
        //                                    ).Where(_Request.SearchCondition)
        //                    .GroupBy(x => x.StoreKey)
        //                    .Select(g => g.First())
        //                    .Count();
        //                List<OStoreList> TData = (from n in _HCoreContext.HCUAccountTransaction
        //                                          where n.Account.AccountTypeId == UserAccountType.Appuser && n.Account.Guid == _Request.ReferenceKey
        //                                          select new OStoreList
        //                                          {
        //                                              ReferenceKey = n.Guid,

        //                                              StoreKey = n.CreatedBy.Owner.Guid,
        //                                              StoreDisplayName = n.CreatedBy.Owner.DisplayName,
        //                                              AddressLatitude = n.CreatedBy.Owner.Latitude,
        //                                              AddressLongitude = n.CreatedBy.Owner.Longitude,

        //                                              MerchantKey = n.CreatedBy.Owner.Owner.Guid,
        //                                              MerchantDisplayName = n.CreatedBy.Owner.Owner.DisplayName,
        //                                              IconUrl = n.CreatedBy.Owner.Owner.IconStorage.Path,
        //                                              PosterUrl = n.CreatedBy.Owner.Owner.PosterStorage.Path,


        //                                              TotalVisits = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == n.UserAccountId && x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid).Count(),

        //                                              LastVisit = _HCoreContext.HCUAccountTransaction
        //                                                     .Where(x => x.AccountId == n.UserAccountId &&
        //                                                            x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                            x.StatusId == StatusHelperId.Transaction_Success
        //                                                            ).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault(),
        //                                              TotalPurchase = _HCoreContext.HCUAccountTransaction
        //                                                                           .Where(x => x.AccountId == n.UserAccountId &&
        //                                                            x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                            x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                            x.ModeId == TransactionMode.Credit)
        //                                                              .Sum(x => (double?)x.PurchaseAmount) ?? 0,
        //                                              TotalPointReward = _HCoreContext.HCUAccountTransaction
        //                                                                              .Where(x => x.AccountId == n.UserAccountId &&
        //                                                            x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                            x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                            x.ModeId == TransactionMode.Credit)
        //                                                              .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                              TotalPointRedeem = _HCoreContext.HCUAccountTransaction
        //                                                                              .Where(x => x.AccountId == n.UserAccountId &&
        //                                                            x.CreatedBy.Owner.Guid == n.CreatedBy.Owner.Guid &&
        //                                                            x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                            x.ModeId == TransactionMode.Debit)
        //                                                              .Sum(x => (double?)x.TotalAmount) ?? 0,
        //                                          }
        //                                         )
        //                             .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                    .GroupBy(x => x.StoreKey)
        //                    .Select(g => g.First())
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                    .ToList();

        //                #region Create  Response Object
        //                foreach (var DataItem in TData)
        //                {

        //                    DataItem.Tag = null;

        //                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                    {
        //                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //                    }
        //                    if (!string.IsNullOrEmpty(DataItem.PosterUrl))
        //                    {
        //                        DataItem.PosterUrl = _AppConfig.StorageUrl + DataItem.PosterUrl;
        //                    }
        //                }
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(TTotalRecords, TData, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_CardUsers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.Appuser
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    CardKey = n.Card.Guid,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,


        //                                    DisplayName = n.DisplayName,
        //                                    Name = n.Name,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    ReferralCode = n.ReferralCode,

        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,

        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,
        //                                    UserId = n.UserId,
        //                                    Balance = ((_HCoreContext.HCUAccountTransaction
        //                                                .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                       (_HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)),

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.Appuser
        //                        select new OUserAccountList
        //                        {
        //                            UserId = n.UserId,
        //                            ReferenceKey = n.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,


        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            CardKey = n.Card.Guid,
        //                            ReferralCode = n.ReferralCode,

        //                            DisplayName = n.DisplayName,
        //                            Name = n.Name,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,

        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            Balance = ((_HCoreContext.HCUAccountTransaction
        //                                                .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                       (_HCoreContext.HCUAccountTransaction
        //                                                        .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                               .Sum(x => (double?)x.TotalAmount) ?? 0)),
        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_AppUsers)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                where n.AccountTypeId == UserAccountType.Appuser
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    UserId = n.UserId,

        //                                    Name = n.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,


        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,


        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreateDate = n.CreateDate,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    CreatedByKey = n.CreatedBy.Guid,



        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    AppName = n.AppVersion.Parent.Name,
        //                                    AppKey = n.AppVersion.Parent.Guid,


        //                                    //AndroidAppRequest = n.HCCoreUsage.Count(x => x.AppVersion.ParentId == 4),
        //                                    //IosAppRequest = n.HCCoreUsage.Count(x => x.AppVersion.ParentId == 5),

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,



        //                                    Balance = ((_HCoreContext.HCUAccountTransaction
        //                                                .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                                .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                      (_HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                              x.SourceId == TransactionSource.TUC)
        //                                                .Sum(x => (double?)x.TotalAmount) ?? 0)),

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        where n.AccountTypeId == UserAccountType.Appuser
        //                        select new OUserAccountList
        //                        {
        //                            UserId = n.UserId,
        //                            ReferenceKey = n.Guid,
        //                            IconUrl = n.IconStorage.Path,

        //                            Name = n.Name,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,


        //                            OwnerParentKey = n.Owner.Owner.Guid,
        //                            OwnerParentDisplayName = n.Owner.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreateDate = n.CreateDate,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                            AppName = n.AppVersion.Parent.Name,
        //                            AppKey = n.AppVersion.Parent.Guid,



        //                            //AndroidAppRequest = n.HCCoreUsage.Count(x => x.AppVersion.ParentId == 4),
        //                            //IosAppRequest = n.HCCoreUsage.Count(x => x.AppVersion.ParentId == 5),

        //                            Balance = ((_HCoreContext.HCUAccountTransaction
        //                                        .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Credit &&
        //                                               x.SourceId == TransactionSource.TUC)
        //                                                .Sum(x => (double?)x.TotalAmount) ?? 0) -
        //                                                      (_HCoreContext.HCUAccountTransaction
        //                                                       .Where(x => x.Account.UserId == n.UserId &&
        //                                                  x.StatusId == StatusHelperId.Transaction_Success &&
        //                                                  x.ModeId == TransactionMode.Debit &&
        //                                                  x.SourceId == TransactionSource.TUC)
        //                                                .Sum(x => (double?)x.TotalAmount) ?? 0)),
        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else if (_Request.UserReference.Task == HCoreTask.Get_UserAccounts)
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                select new OUserAccountList
        //                                {
        //                                    ReferenceKey = n.Guid,

        //                                    AccountTypeCode = n.AccountType.SystemName,
        //                                    AccountTypeName = n.AccountType.Name,

        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    AccountOperationTypeName = n.AccountOperationType.Name,

        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerDisplayName = n.Owner.DisplayName,

        //                                    Name = n.Name,

        //                                    OwnerParentKey = n.Owner.Owner.Guid,

        //                                    DisplayName = n.DisplayName,

        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    AccountCode = n.AccountCode,

        //                                    Address = n.Address,
        //                                    CountryKey = n.Country.Guid,
        //                                    CountryName = n.Country.Name,

        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreatedByName = n.CreatedBy.DisplayName,
        //                                    CreateDate = n.CreateDate,
        //                                    ModifyDate = n.ModifyDate,
        //                                    LastLoginDate = n.LastLoginDate,

        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    StatusName = n.Status.Name,

        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                Data = (from n in _HCoreContext.HCUAccount
        //                        select new OUserAccountList
        //                        {
        //                            ReferenceKey = n.Guid,

        //                            AccountTypeCode = n.AccountType.SystemName,
        //                            AccountTypeName = n.AccountType.Name,

        //                            AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                            AccountOperationTypeName = n.AccountOperationType.Name,
        //                            Name = n.Name,
        //                            OwnerParentKey = n.Owner.Owner.Guid,

        //                            OwnerKey = n.Owner.Guid,
        //                            OwnerDisplayName = n.Owner.DisplayName,

        //                            DisplayName = n.DisplayName,

        //                            EmailAddress = n.EmailAddress,
        //                            UserName = n.User.Username,
        //                            ContactNumber = n.ContactNumber,
        //                            AccountCode = n.AccountCode,
        //                            Address = n.Address,


        //                            CountryKey = n.Country.Guid,
        //                            CountryName = n.Country.Name,

        //                            CreatedByKey = n.CreatedBy.Guid,
        //                            CreatedByName = n.CreatedBy.DisplayName,
        //                            CreateDate = n.CreateDate,
        //                            ModifyDate = n.ModifyDate,
        //                            LastLoginDate = n.LastLoginDate,

        //                            Status = n.StatusId,
        //                            StatusCode = n.Status.SystemName,
        //                            StatusName = n.Status.Name,

        //                        })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Total Records
        //                TotalRecords = (from n in _HCoreContext.HCUAccount
        //                                select new OUserAccountListLite
        //                                {
        //                                    ReferenceKey = n.Guid,
        //                                    AccountTypeCode = n.AccountType.SystemName,
        //                                    AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                    OwnerDisplayName = n.Owner.DisplayName,
        //                                    OwnerKey = n.Owner.Guid,
        //                                    OwnerParentKey = n.Owner.Owner.Guid,
        //                                    DisplayName = n.DisplayName,
        //                                    EmailAddress = n.EmailAddress,
        //                                    UserName = n.User.Username,
        //                                    ContactNumber = n.ContactNumber,
        //                                    CountryKey = n.Country.Guid,
        //                                    AccountCode = n.AccountCode,
        //                                    Status = n.StatusId,
        //                                    StatusCode = n.Status.SystemName,
        //                                    CreatedByDisplayName = n.CreatedBy.DisplayName,
        //                                    CreatedByKey = n.CreatedBy.Guid,
        //                                    CreateDate = n.CreateDate,


        //                                })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //                #endregion
        //                #region Get Data
        //                List<OUserAccountListLite> SData = (from n in _HCoreContext.HCUAccount
        //                                                    select new OUserAccountListLite
        //                                                    {
        //                                                        ReferenceKey = n.Guid,
        //                                                        AccountTypeCode = n.AccountType.SystemName,
        //                                                        AccountOperationTypeCode = n.AccountOperationType.SystemName,
        //                                                        OwnerDisplayName = n.Owner.DisplayName,
        //                                                        OwnerKey = n.Owner.Guid,
        //                                                        OwnerParentKey = n.Owner.Owner.Guid,
        //                                                        DisplayName = n.DisplayName,
        //                                                        EmailAddress = n.EmailAddress,
        //                                                        UserName = n.User.Username,
        //                                                        ContactNumber = n.ContactNumber,
        //                                                        CountryKey = n.Country.Guid,
        //                                                        AccountCode = n.AccountCode,
        //                                                        Status = n.StatusId,

        //                                                        StatusCode = n.Status.SystemName,
        //                                                        CreatedByDisplayName = n.CreatedBy.DisplayName,
        //                                                        CreatedByKey = n.CreatedBy.Guid,
        //                                                        CreateDate = n.CreateDate,

        //                                                    })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #region Create  Response Object
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, SData, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //                #endregion
        //            }
        //            if (Data != null)
        //            {
        //                foreach (var DataItem in Data)
        //                {
        //                }
        //                #region Create  Response Object
        //                if (_Request.UserReference.Task == HCoreTask.Get_Stores && _Request.Type == ListType.CardUsers)
        //                {
        //                    foreach (var DataItem in Data)
        //                    {
        //                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                        {
        //                            DataItem.IconUrl = _AppConfig.StorageUrl.Replace("https://", "http://") + DataItem.IconUrl;
        //                        }
        //                        if (!string.IsNullOrEmpty(DataItem.PosterUrl))
        //                        {
        //                            DataItem.PosterUrl = _AppConfig.StorageUrl.Replace("https://", "http://") + DataItem.PosterUrl;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var DataItem in Data)
        //                    {
        //                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                        {
        //                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //                        }
        //                        else
        //                        {

        //                        }
        //                        if (!string.IsNullOrEmpty(DataItem.PosterUrl))
        //                        {
        //                            DataItem.PosterUrl = _AppConfig.StorageUrl + DataItem.PosterUrl;
        //                        }
        //                    }
        //                }

        //                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Create DataTable Response Object
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //                #endregion
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GetUserAccounts", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}
        //internal OResponse GetUserAccountOwners(OList.Request _Request)
        //{
        //    #region Declare

        //    List<OUserAccountList> Data = null;
        //    int TotalRecords = 0;
        //    #endregion
        //    #region Manage Exception
        //    try
        //    {
        //        #region Add Default Request
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
        //            #endregion
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //            #endregion
        //        }
        //        #endregion
        //        #region Set Default Limit
        //        if (_Request.Limit == -1)
        //        {
        //            _Request.Limit = TotalRecords;
        //        }
        //        else if (_Request.Limit == 0)
        //        {
        //            _Request.Limit = DefaultLimit;

        //        }
        //        #endregion
        //        #region Operation
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Total Records
        //            TotalRecords = (from n in _HCoreContext.HCUAccountOwner
        //                            select new OUserAccountList
        //                            {
        //                                ReferenceKey = n.Guid,
        //                                // Merchant || POS ACCOUNT
        //                                OwnerKey = n.Owner.Guid,
        //                                OwnerDisplayName = n.Owner.DisplayName,
        //                                OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                                // POS TERMINAL
        //                                AccountTypeCode = n.Account.AccountType.SystemName,
        //                                AccountTypeName = n.Account.AccountType.Name,
        //                                UserAccountKey = n.Account.Guid,
        //                                DisplayName = n.Account.DisplayName,
        //                                AccountCode = n.Account.AccountCode,

        //                                // POS
        //                                OwnerParentKey = n.Account.Owner.Guid,
        //                                OwnerParentDisplayName = n.Account.Owner.DisplayName,

        //                                StartDate = n.StartDate,
        //                                EndDate = n.EndDate,

        //                                CreatedByKey = n.Account.CreatedBy.Guid,
        //                                CreatedByName = n.Account.CreatedBy.DisplayName,

        //                                CreateDate = n.CreateDate,
        //                                ModifyDate = n.ModifyDate,

        //                                Status = n.StatusId,
        //                                StatusCode = n.Status.SystemName,
        //                                StatusName = n.Status.Name,
        //                            })
        //                               .Where(_Request.SearchCondition)
        //                       .Count();
        //            #endregion
        //            #region Get Data
        //            Data = (from n in _HCoreContext.HCUAccountOwner
        //                    select new OUserAccountList
        //                    {
        //                        ReferenceKey = n.Guid,
        //                        // Merchant || POS
        //                        OwnerKey = n.Owner.Guid,
        //                        OwnerDisplayName = n.Owner.DisplayName,
        //                        OwnerAccountTypeCode = n.Owner.AccountType.SystemName,

        //                        // TERMINAL
        //                        AccountTypeCode = n.Account.AccountType.SystemName,
        //                        AccountTypeName = n.Account.AccountType.Name,
        //                        DisplayName = n.Account.DisplayName,
        //                        UserAccountKey = n.Account.Guid,
        //                        AccountCode = n.Account.AccountCode,


        //                        // POS
        //                        OwnerParentKey = n.Account.Owner.Guid,
        //                        OwnerParentDisplayName = n.Account.Owner.DisplayName,


        //                        StartDate = n.StartDate,
        //                        EndDate = n.EndDate,
        //                        CreateDate = n.CreateDate,
        //                        ModifyDate = n.ModifyDate,

        //                        Status = n.StatusId,
        //                        StatusCode = n.Status.SystemName,
        //                        StatusName = n.Status.Name,
        //                    })
        //                                      .Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                                      .ToList();
        //            #endregion
        //            if (Data != null)
        //            {
        //                #region Create  Response Object
        //                foreach (var DataItem in Data)
        //                {
        //                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                    {
        //                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //                    }
        //                    if (!string.IsNullOrEmpty(DataItem.PosterUrl))
        //                    {
        //                        DataItem.PosterUrl = _AppConfig.StorageUrl + DataItem.PosterUrl;
        //                    }
        //                }
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Create DataTable Response Object
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //                #endregion
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GetUserAccountOwners", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}


    }
}
