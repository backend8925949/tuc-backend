//==================================================================================
// FileName: FrameworkUserDevice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user device
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    internal class FrameworkUserDevice
    {
        #region Entity
        HCoreContext _HCoreContext;
        HCUAccountDevice _HCUAccountDevice;
        #endregion
        #region Manage Operations
        #region Save
        /// <summary>
        /// Description: Saves the user device.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SerialNumber">The serial number.</param>
        /// <param name="NotificationUrl">The notification URL.</param>
        /// <param name="BrandName">Name of the brand.</param>
        /// <param name="ModelName">Name of the model.</param>
        /// <param name="Height">The height.</param>
        /// <param name="Width">The width.</param>
        /// <param name="_UserReference">The user reference.</param>
        /// <returns>System.String.</returns>
        internal string SaveUserDevice(long UserAccountId, string? SerialNumber, string? NotificationUrl, string? BrandName, string? ModelName, int Height, int Width, OUserReference _UserReference)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccountDevice ValidateDetails = (from n in _HCoreContext.HCUAccountDevice
                                                        where n.AccountId == UserAccountId && n.SerialNumber == SerialNumber
                                                        select n).FirstOrDefault();
                    if (ValidateDetails != null)
                    {
                        if (ValidateDetails.StatusId == StatusActive)
                        {
                            //ValidateDetails.AppId = _UserReference.AppId;
                            ValidateDetails.AppVersionId = _UserReference.AppVersionId;
                            ValidateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                            return ValidateDetails.Guid;

                        }
                        else
                        {
                            return null;

                        }
                    }
                    else
                    {
                        long BrandId = 0;
                        long ModelId = 0;
                        long ResolutionId = 0;
                        //_FrameworkDeviceSpecification = new FrameworkDeviceSpecification();
                        //#region Get Configuration
                        //if (!string.IsNullOrEmpty(_Request.Brand))
                        //{
                        //    BrandId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Brand, 0, _Request.Brand, 0, 0, _Request.UserReference);
                        //}
                        //if (_Request.Width != 0 && _Request.Height != 0)
                        //{
                        //    ResolutionId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Resolution, 0, _Request.Width.ToString() + " x " + _Request.Height.ToString(), _Request.Width, _Request.Height, _Request.UserReference);
                        //}
                        //if (!string.IsNullOrEmpty(_Request.Model) && BrandId != 0 && ResolutionId != 0)
                        //{
                        //    ModelId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Model, BrandId, _Request.Model, _Request.Width, _Request.Height, _Request.UserReference);
                        //}
                        //#endregion
                        #region Save Device
                        using (_HCoreContext = new HCoreContext())
                        {
                            string DeviceKey = HCoreHelper.GenerateGuid();
                            _HCUAccountDevice = new HCUAccountDevice();
                            _HCUAccountDevice.Guid = DeviceKey;
                            _HCUAccountDevice.AccountId = UserAccountId;
                            _HCUAccountDevice.SerialNumber = SerialNumber;
                            //_HCUAccountDevice.OsId = _UserReference.OsId;
                            //_HCUAccountDevice.AppId = _UserReference.AppId;
                            _HCUAccountDevice.AppVersionId = _UserReference.AppVersionId;
                            _HCUAccountDevice.NotificationUrl = NotificationUrl;
                            if (BrandId != 0)
                            {
                                //_HCUAccountDevice.BrandId = BrandId;
                            }
                            if (ResolutionId != 0)
                            {
                                _HCUAccountDevice.ResolutionId = ResolutionId;
                            }
                            if (ModelId != 0)
                            {
                                _HCUAccountDevice.ModelId = ModelId;
                            }
                            _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountDevice.StatusId = StatusActive;
                            _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                            _HCoreContext.SaveChanges();
                        }
                        return _HCUAccountDevice.Guid;
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserDevice", _Exception, _UserReference);
                #endregion
                return null;

            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveUserDevice(OUserDeviceSave _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                #region Code Block
                if (_Request.UserId == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD001");
                    #endregion
                }
                else if (_Request.UserAccountId == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD002");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD003");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountDevice ValidateDetails = (from n in _HCoreContext.HCUAccountDevice
                                                            where n.AccountId == _Request.UserAccountId && n.SerialNumber == _Request.SerialNumber
                                                            select n).FirstOrDefault();
                        if (ValidateDetails != null)
                        {
                            if (ValidateDetails.StatusId == StatusActive)
                            {
                                //ValidateDetails.AppId = _Request.UserReference.AppId;
                                ValidateDetails.AppVersionId = _Request.UserReference.AppVersionId;
                                ValidateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ValidateDetails.Id, "HCUAD004");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD005");
                                #endregion
                            }
                        }
                        else
                        {
                            long BrandId = 0;
                            long ModelId = 0;
                            long ResolutionId = 0;
                            //_FrameworkDeviceSpecification = new FrameworkDeviceSpecification();
                            //#region Get Configuration
                            //if (!string.IsNullOrEmpty(_Request.Brand))
                            //{
                            //    BrandId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Brand, 0, _Request.Brand, 0, 0, _Request.UserReference);
                            //}
                            //if (_Request.Width != 0 && _Request.Height != 0)
                            //{
                            //    ResolutionId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Resolution, 0, _Request.Width.ToString() + " x " + _Request.Height.ToString(), _Request.Width, _Request.Height, _Request.UserReference);
                            //}
                            //if (!string.IsNullOrEmpty(_Request.Model) && BrandId != 0 && ResolutionId != 0)
                            //{
                            //    ModelId = _FrameworkDeviceSpecification.AutoSaveDeviceSpecification(SystemHelperTypeCode.DeviceSpecification_Model, BrandId, _Request.Model, _Request.Width, _Request.Height, _Request.UserReference);
                            //}
                            //#endregion
                            #region Save Device
                            using (_HCoreContext = new HCoreContext())
                            {
                                string DeviceKey = HCoreHelper.GenerateGuid();
                                _HCUAccountDevice = new HCUAccountDevice();
                                _HCUAccountDevice.Guid = DeviceKey;
                                _HCUAccountDevice.AccountId = _Request.UserAccountId;
                                _HCUAccountDevice.SerialNumber = _Request.SerialNumber;
                                //_HCUAccountDevice.OsId = _Request.UserReference.OsId;
                                //_HCUAccountDevice.AppId = _Request.UserReference.AppId;
                                _HCUAccountDevice.AppVersionId = _Request.UserReference.AppVersionId;
                                _HCUAccountDevice.NotificationUrl = _Request.NotificationUrl;
                                if (BrandId != 0)
                                {
                                    //_HCUAccountDevice.BrandId = BrandId;
                                }
                                if (ResolutionId != 0)
                                {
                                    _HCUAccountDevice.ResolutionId = ResolutionId;
                                }
                                if (ModelId != 0)
                                {
                                    _HCUAccountDevice.ModelId = ModelId;
                                }
                                _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountDevice.StatusId = StatusActive;
                                _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                                _HCoreContext.SaveChanges();
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _HCUAccountDevice.Id, "HCUAD006");
                            #endregion
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveUserDevice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Update
        /// <summary>
        /// Description: Updates the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserDevice(OUserDevice _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {

                    HCUAccountDevice Details = (from n in _HCoreContext.HCUAccountDevice
                                                where n.Id == _Request.UserReference.DeviceId || n.Guid == _Request.ReferenceKey
                                                select n).FirstOrDefault();

                    if (Details != null)
                    {
                        _HCUAccountDevice.ModifyDate = HCoreHelper.GetGMTDateTime();
                        if (string.IsNullOrEmpty(_Request.NotificationUrl))
                        {
                            Details.NotificationUrl = _Request.NotificationUrl;
                        }
                        if (_Request.Status != 0)
                        {
                            _HCUAccountDevice.StatusId = _Request.Status;
                        }
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HCUAD007");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD008");
                        #endregion
                    }

                }
                #endregion

                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateUserDevice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Delete
        /// <summary>
        /// Description: Deletes the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteUserDevice(OUserDevice _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD009");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {

                        var DeviceDetails = (from n in _HCoreContext.HCUAccountDevice
                                             where n.Guid == _Request.ReferenceKey
                                             select new
                                             {
                                                 DeviceId = n.Id,
                                                 //Counts = n.HCUAccountTransaction.Count,
                                             }).FirstOrDefault();
                        if (DeviceDetails != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {


                                //var ApiUsageLogs = _HCoreContext.HCApiUsage.Where(x => x.DeviceId == DeviceDetails.DeviceId).ToList();
                                //foreach (var ApiUsageLog in ApiUsageLogs)
                                //{
                                //    ApiUsageLog.UserDeviceId = null;
                                //}

                                //var UserPasswords = _HCoreContext.HCUserPassword.Where(x => x.DeviceId == DeviceDetails.DeviceId).ToList();
                                //foreach (var UserPassword in UserPasswords)
                                //{
                                //    UserPassword.UserDeviceId = null;
                                //}

                                //var AccessPinLogs = _HCoreContext.HCUAccountAccessPin.Where(x => x.DeviceId == DeviceDetails.DeviceId).ToList();
                                //foreach (var AccessPinLog in AccessPinLogs)
                                //{
                                //    AccessPinLog.UserDeviceId = null;
                                //}

                                var UserAccountActivities = _HCoreContext.HCUAccountActivity.Where(x => x.DeviceId == DeviceDetails.DeviceId).ToList();
                                foreach (var UserAccountActivity in UserAccountActivities)
                                {
                                    UserAccountActivity.DeviceId = null;
                                }

                                var Sessions = _HCoreContext.HCUAccountSession.Where(x => x.DeviceId == DeviceDetails.DeviceId).ToList();
                                _HCoreContext.HCUAccountSession.RemoveRange(Sessions);
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                HCUAccountDevice _Details = _HCoreContext.HCUAccountDevice.Where(x => x.Id == DeviceDetails.DeviceId).FirstOrDefault();
                                if (_Details != null)
                                {
                                    _HCoreContext.HCUAccountDevice.Remove(_Details);
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCUAD010");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD011");
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD012");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteDevice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserDevice(OUserDevice _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD013");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        OUserDevice Data = (from n in _HCoreContext.HCUAccountDevice
                                            where n.Guid == _Request.ReferenceKey
                                            select new OUserDevice
                                            {
                                                ReferenceKey = n.Guid,


                                                UserAccountKey = n.Account.Guid,
                                                UserAccountDisplayName = n.Account.DisplayName,

                                                SerialNumber = n.SerialNumber,

                                                NotificationUrl = n.NotificationUrl,

                                                OsKey = n.OsVersion.Parent.Guid,
                                                OsName = n.OsVersion.Parent.Name,

                                                OsVersionKey = n.OsVersion.Guid,
                                                OsVersionName = n.OsVersion.Name,

                                                BrandKey = n.Model.Parent.Guid,
                                                BrandName = n.Model.Parent.Name,

                                                ModelKey = n.Model.Guid,
                                                ModelName = n.Model.Name,

                                                ResolutionKey = n.Resolution.Guid,
                                                ResolutionName = n.Resolution.Name,

                                                NetworkOperatorKey = n.NetworkOperator.Guid,
                                                NetworkOperatorName = n.NetworkOperator.Name,

                                                CreateDate = n.CreateDate,
                                                ModifyDate = n.ModifyDate,


                                                Status = n.StatusId,
                                                StatusCode = n.Status.SystemName,
                                                StatusName = n.Status.Name,
                                            }).FirstOrDefault();
                        if (Data != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC002");
                            #endregion
                        }
                        #endregion

                    }
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetDevice", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user device list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserDevices(OList.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCUAccountDevice
                                        select new OUserDevice
                                        {
                                            ReferenceKey = n.Guid,


                                            UserAccountKey = n.Account.Guid,
                                            UserAccountDisplayName = n.Account.DisplayName,

                                            SerialNumber = n.SerialNumber,

                                            OsKey = n.OsVersion.Parent.Guid,
                                            OsName = n.OsVersion.Parent.Name,

                                            OsVersionKey = n.OsVersion.Guid,
                                            OsVersionName = n.OsVersion.Name,

                                            BrandKey = n.Model.Parent.Guid,
                                            BrandName = n.Model.Parent.Name,

                                            ModelKey = n.Model.Guid,
                                            ModelName = n.Model.Name,

                                            ResolutionKey = n.Resolution.Guid,
                                            ResolutionName = n.Resolution.Name,

                                            NetworkOperatorKey = n.NetworkOperator.Guid,
                                            NetworkOperatorName = n.NetworkOperator.Name,


                                            CreateDate = n.CreateDate,
                                            ModifyDate = n.ModifyDate,


                                            Status = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OUserDevice> Data = (from n in _HCoreContext.HCUAccountDevice
                                              select new OUserDevice
                                              {
                                                  ReferenceKey = n.Guid,


                                                  UserAccountKey = n.Account.Guid,
                                                  UserAccountDisplayName = n.Account.DisplayName,

                                                  SerialNumber = n.SerialNumber,

                                                  NotificationUrl = n.NotificationUrl,

                                                  OsKey = n.OsVersion.Parent.Guid,
                                                  OsName = n.OsVersion.Parent.Name,

                                                  OsVersionKey = n.OsVersion.Guid,
                                                  OsVersionName = n.OsVersion.Name,


                                                  BrandKey = n.Model.Parent.Guid,
                                                  BrandName = n.Model.Parent.Name,

                                                  ModelKey = n.Model.Guid,
                                                  ModelName = n.Model.Name,

                                                  ResolutionKey = n.Resolution.Guid,
                                                  ResolutionName = n.Resolution.Name,

                                                  NetworkOperatorKey = n.NetworkOperator.Guid,
                                                  NetworkOperatorName = n.NetworkOperator.Name,

                                                  CreateDate = n.CreateDate,
                                                  ModifyDate = n.ModifyDate,


                                                  Status = n.StatusId,
                                                  StatusCode = n.Status.SystemName,
                                                  StatusName = n.Status.Name,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetDevices", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion
        #endregion
    }
}
