//==================================================================================
// FileName: FrameworkCountry.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkCountry
    {
        //#region Declare
        //HCCoreCommon _HCCoreCommon;
        //HCoreContext _HCoreContext;
        //#endregion
        //internal OResponse SaveCountry(OCountry.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

        //        if (StatusId == null)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC499_InvalidStatusCode);
        //            #endregion
        //        }
        //        if (_Request.Name == null)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC007_NameRequired);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                long CheckConfiguration = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == SystemName && x.TypeId == HelperType.Country).Select(x => x.Id).FirstOrDefault();
        //                if (CheckConfiguration == 0)
        //                {
        //                    _HCCoreCommon = new HCCoreCommon();
        //                    _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
        //                    _HCCoreCommon.TypeId = HelperType.Country;
        //                    _HCCoreCommon.Name = _Request.Name;
        //                    _HCCoreCommon.SystemName = SystemName;
        //                    _HCCoreCommon.Value = _Request.Iso;
        //                    _HCCoreCommon.SubValue = _Request.Isd;
        //                    _HCCoreCommon.Description = _Request.CurrencyName;
        //                    _HCCoreCommon.Data = _Request.CurrencyNotation;
        //                    _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
        //                    _HCCoreCommon.ModifyById = _Request.UserReference.AccountId;
        //                    _HCCoreCommon.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                    _HCCoreCommon.StatusId = (int)StatusId;
        //                    _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
        //                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                    _HCoreContext.SaveChanges();
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _HCCoreCommon.Guid, HCoreResponseCode.System.HC003_DetailsSaved);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC498_DetailsAlreadyPresent);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("SaveCountry", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse UpdateCountry(OCountry.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        if (_Request.Name == null)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC007_NameRequired);
        //            #endregion
        //        }
        //        #region Code Block

        //        int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
        //        if (StatusId == 0)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCC100);
        //        }
        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            HCCoreCommon ItemDetails = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (ItemDetails != null)
        //            {
        //                if (!string.IsNullOrEmpty(_Request.Name) && ItemDetails.Name != _Request.Name)
        //                {
        //                    ItemDetails.Name = _Request.Name;
        //                    ItemDetails.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                }
        //                if (!string.IsNullOrEmpty(_Request.Iso) && ItemDetails.Value != _Request.Iso)
        //                {
        //                    ItemDetails.Value = _Request.Iso;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.Isd) && ItemDetails.SubValue != _Request.Isd)
        //                {
        //                    ItemDetails.SubValue = _Request.Isd;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.CurrencyName) && ItemDetails.Data != _Request.CurrencyName)
        //                {
        //                    ItemDetails.Description = _Request.CurrencyName;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.CurrencyNotation) && ItemDetails.Description != _Request.CurrencyNotation)
        //                {
        //                    ItemDetails.Description = _Request.CurrencyNotation;
        //                }
        //                if (StatusId != null && ItemDetails.StatusId != StatusId)
        //                {
        //                    ItemDetails.StatusId = (int)StatusId;
        //                }
        //                ItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                ItemDetails.ModifyById = _Request.UserReference.AccountId;
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC004_DetailsUpdated);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("UpdateCountry", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse DeleteCountry(OCountry.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            HCCoreCommon DeleteItemDetails = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (DeleteItemDetails != null)
        //            {
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.HCCoreCommon.Remove(DeleteItemDetails);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC005_DetailsDeleted);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("DeleteCountry", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetCountryDetails(OCountry.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                OCountry.List Details = _HCoreContext.HCCoreCommon
        //                    .Where(a => a.Guid == _Request.ReferenceKey && a.TypeId == HelperType.Country)
        //                    .Select(x => new OCountry.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,

        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Iso = x.Value,
        //                        Isd = x.SubValue,
        //                        CurrencyName= x.Description,
        //                        CurrencyNotation = x.Data,

        //                        Regions = x.HCCoreParameterCommon.Count(),

        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    })
        //                    .FirstOrDefault();
        //                _HCoreContext.Dispose();


        //                if (Details != null)
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, HCoreResponseCode.System.HCC113);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetCountryetails", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetCountries(OList.Request _Request)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.RefreshCount)
        //            {
        //                #region Total Records
        //                _Request.TotalRecords = (from x in _HCoreContext.HCCoreCommon
        //                                         where x.TypeId == HelperType.Country
        //                                         select new OCountry.List
        //                                         {
        //                                             ReferenceId = x.Id,
        //                                             ReferenceKey = x.Guid,

        //                                             Name = x.Name,
        //                                             SystemName = x.SystemName,
        //                                             Iso = x.Value,
        //                                             Isd = x.SubValue,
        //                                             CurrencyName = x.Description,
        //                                             CurrencyNotation = x.Data,
        //                                             Regions = x.HCCoreParameterCommon.Count(),
        //                                             CreateDate = x.CreateDate,
        //                                             CreatedByKey = x.CreatedBy.Guid,
        //                                             CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                                             ModifyDate = x.ModifyDate,
        //                                             ModifyByKey = x.ModifyBy.Guid,
        //                                             ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                                             StatusId = x.StatusId,
        //                                             StatusCode = x.Status.SystemName,
        //                                             StatusName = x.Status.Name
        //                                         })
        //                                    .Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                            .Count();
        //                #endregion
        //            }
        //            List<OCountry.List> Data = (from x in _HCoreContext.HCCoreCommon
        //                                        where x.TypeId == HelperType.Country
        //                                        select new OCountry.List
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,

        //                                            Name = x.Name,
        //                                            SystemName = x.SystemName,
        //                                            Iso = x.Value,
        //                                            Isd = x.SubValue,
        //                                            CurrencyName = x.Description,
        //                                            CurrencyNotation = x.Data,
        //                                            Regions = x.HCCoreParameterCommon.Count(),
        //                                            CreateDate = x.CreateDate,
        //                                            CreatedByKey = x.CreatedBy.Guid,
        //                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                                            ModifyDate = x.ModifyDate,
        //                                            ModifyByKey = x.ModifyBy.Guid,
        //                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                                            StatusId = x.StatusId,
        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name
        //                                        })
        //                                      .Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                                      .ToList();

        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, HCoreResponseCode.System.HC001_ListLoaded);
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetCountries", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, HCoreResponseCode.System.HC500);
        //    }
        //}


    }
}
