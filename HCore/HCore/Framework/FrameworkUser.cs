//==================================================================================
// FileName: FrameworkUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    internal class FrameworkUser
    {
        HCoreContext _HCoreContext;
        HCUAccountParameter _HCUAccountParameter;
        /// <summary>
        /// Description: Saves the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                long? ParentId = HCoreHelper.GetUserParameterId(_Request.ParentKey, _Request.UserReference);
                long? SubParentId = HCoreHelper.GetUserParameterId(_Request.SubParentKey, _Request.UserReference);
                long? UserId = HCoreHelper.GetUserId(_Request.UserKey, _Request.UserReference);
                long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                long? UserDeviceId = HCoreHelper.GetUserDeviceId(_Request.UserDeviceKey, _Request.UserReference);
                long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                long? ParameterId = HCoreHelper.GetCoreParameterId(_Request.ParameterKey, _Request.UserReference);
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (UserAccountId == null)
                {
                    UserAccountId = _Request.UserReference.AccountId;
                }
                if (StatusId == null)
                {
                    StatusId = HelperStatus.Default.Active;
                }
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else if (TypeId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else if (UserAccountId == null && UserId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.TypeId = (int)TypeId;

                        _HCUAccountParameter.ParentId = ParentId;
                        _HCUAccountParameter.SubParentId = SubParentId;

                        //_HCUAccountParameter.UserId = UserId;
                        _HCUAccountParameter.AccountId = UserAccountId;
                        _HCUAccountParameter.DeviceId = UserDeviceId;
                        _HCUAccountParameter.CommonId = CommonId;

                        _HCUAccountParameter.ParameterId = ParameterId;
                        _HCUAccountParameter.HelperId = HelperId;
                        _HCUAccountParameter.Name = _Request.Name;

                        if (string.IsNullOrEmpty(_Request.SystemName))
                        {
                            _HCUAccountParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.SystemName);
                        }
                        else
                        {
                            _HCUAccountParameter.SystemName = _Request.SystemName;
                        }
                        _HCUAccountParameter.Value = _Request.Value;
                        _HCUAccountParameter.SubValue = _Request.SubValue;
                        _HCUAccountParameter.Description = _Request.Description;
                        _HCUAccountParameter.Data = _Request.Data;
                        if (_Request.StartTime != null)
                        {
                            _HCUAccountParameter.StartTime = _Request.StartTime;
                        }
                        if (_Request.EndTime != null)
                        {
                            _HCUAccountParameter.EndTime = _Request.EndTime;
                        }
                        if (_Request.CountValue != null)
                        {
                            _HCUAccountParameter.CountValue = _Request.CountValue;
                        }
                        if (_Request.AverageValue != null)
                        {
                            _HCUAccountParameter.AverageValue = _Request.AverageValue;
                        }
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountParameter.StatusId = (int)StatusId;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCUAccountParameter.Guid;
                        long RId = _HCUAccountParameter.Id;

                        if (_Request.TypeCode == "hcore.userreview")
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long? star5 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 5).Select(x => x.CountValue).Count();
                                long? star4 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 4).Select(x => x.CountValue).Count();
                                long? star3 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 3).Select(x => x.CountValue).Count();
                                long? star2 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 4).Select(x => x.CountValue).Count();
                                long? star1 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 1).Select(x => x.CountValue).Count();
                                double rating = (double?)(5 * star5 + 4 * star4 + 3 * star3 + 2 * star2 + 1 * star1) / (star1 + star2 + star3 + star4 + star5) ?? 0;

                                double? Average = Math.Round(rating, 1);

                                //double? Average = _HCoreContext.HCUAccountParameter
                                //.Where(c => c.AccountId == UserAccountId && c.CountValue != null).Select(x => (double?)x.CountValue).Average();
                                var Ratings = _HCoreContext.HCUAccountParameter
                                                           .Where(c => c.AccountId == UserAccountId && c.CountValue != null).Count();
                                var UserAccount = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId).FirstOrDefault();
                                if (UserAccount != null)
                                {
                                    UserAccount.AverageValue = Average;
                                    UserAccount.CountValue = Ratings;
                                    UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                }
                                var UserParameter = _HCoreContext.HCUAccountParameter.Where(x => x.Id == RId).FirstOrDefault();
                                if (UserParameter != null)
                                {
                                    UserParameter.AverageValue = Average;
                                    UserParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                }
                                _HCoreContext.SaveChanges();
                            }

                        }

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetUserParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetUserParameterId(_Request.SubParentKey, _Request.UserReference);
                    long? UserId = HCoreHelper.GetUserId(_Request.UserKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    long? UserDeviceId = HCoreHelper.GetUserDeviceId(_Request.UserDeviceKey, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? ParameterId = HCoreHelper.GetCoreParameterId(_Request.ParameterKey, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter Details = _HCoreContext.HCUAccountParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (TypeId != null)
                            {
                                Details.TypeId = (int)TypeId;
                            }
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }

                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }

                            if (UserDeviceId != null)
                            {
                                Details.DeviceId = UserDeviceId;
                            }

                            if (CommonId != null)
                            {
                                Details.CommonId = CommonId;
                            }
                            if (ParameterId != null)
                            {
                                Details.ParameterId = ParameterId;
                            }

                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue))
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                Details.Description = _Request.Description;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data))
                            {
                                Details.Data = _Request.Data;
                            }

                            if (_Request.CountValue != null)
                            {
                                Details.CountValue = _Request.CountValue;
                            }
                            if (_Request.AverageValue != null)
                            {
                                Details.AverageValue = _Request.AverageValue;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter Details = _HCoreContext.HCUAccountParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCUAccountParameter.Remove(Details);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserParameter.Details Details = _HCoreContext.HCUAccountParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                            .Select(x => new OUserParameter.Details
                            {
                                ReferenceKey = x.Guid,


                                ParentKey = x.Parent.Guid,
                                ParentName = x.Parent.Name,

                                SubParentKey = x.SubParent.Guid,
                                SubParentName = x.SubParent.Name,

                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,

                                //UserKey = x.User.Guid,
                                //UserName = x.User.Name,

                                UserAccountKey = x.Account.Guid,
                                UserAccountDisplayName = x.Account.DisplayName,

                                DeviceKey = x.Device.Guid,
                                DeviceSerialNumber = x.Device.SerialNumber,

                                CommonKey = x.Common.Guid,
                                CommonName = x.Common.Name,

                                ParameterKey = x.Parameter.Guid,
                                ParameterName = x.Parameter.Name,

                                HelperCode = x.Helper.SystemName,
                                HelperName = x.Helper.Name,

                                Name = x.Name,
                                SystemName = x.SystemName,
                                Value = x.Value,
                                SubValue = x.SubValue,
                                Description = x.Description,
                                Data = x.Data,

                                StartTime = x.StartTime,
                                EndTime = x.EndTime,

                                CountValue = x.CountValue,
                                AverageValue = x.AverageValue,

                                RequestKey = x.RequestKey,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .FirstOrDefault();
                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user parameter list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserParameter(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountParameter
                                        select new OUserParameter.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            ParentKey = x.Parent.Guid,
                                            ParentName = x.Parent.Name,

                                            SubParentKey = x.SubParent.Guid,
                                            SubParentName = x.SubParent.Name,


                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            //UserKey = x.User.Guid,
                                            //UserName = x.User.Name,

                                            UserAccountOwnerKey = x.Account.Owner.Guid,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            DeviceKey = x.Device.Guid,
                                            DeviceSerialNumber = x.Device.SerialNumber,

                                            CommonKey = x.Common.Guid,
                                            CommonName = x.Common.Name,
                                            CommonSystemName = x.Common.SystemName,

                                            CountValue = x.CountValue,
                                            AverageValue = x.AverageValue,
                                            ParameterKey = x.Parameter.Guid,
                                            ParameterName = x.Parameter.Name,

                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,

                                            Name = x.Name,
                                            SystemName = x.SystemName,
                                            Value = x.Value,
                                            SubValue = x.SubValue,

                                            StartTime = x.StartTime,
                                            EndTime = x.EndTime,
                                            RequestKey = x.RequestKey,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OUserParameter.Details> Data = (from x in _HCoreContext.HCUAccountParameter
                                                         select new OUserParameter.Details
                                                         {
                                                             ReferenceKey = x.Guid,

                                                             TypeCode = x.Type.SystemName,
                                                             TypeName = x.Type.Name,

                                                             //UserKey = x.User.Guid,
                                                             //UserName = x.User.Name,

                                                             UserAccountOwnerKey = x.Account.Owner.Guid,
                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                             UserAccountIconUrl = x.Account.IconStorage.Path,

                                                             DeviceKey = x.Device.Guid,
                                                             DeviceSerialNumber = x.Device.SerialNumber,

                                                             CommonKey = x.Common.Guid,
                                                             CommonName = x.Common.Name,
                                                             CommonSystemName = x.Common.SystemName,


                                                             ParameterKey = x.Parameter.Guid,
                                                             ParameterName = x.Parameter.Name,
                                                             CountValue = x.CountValue,
                                                             AverageValue = x.AverageValue,
                                                             HelperCode = x.Helper.SystemName,
                                                             HelperName = x.Helper.Name,

                                                             Name = x.Name,
                                                             SystemName = x.SystemName,
                                                             Value = x.Value,
                                                             SubValue = x.SubValue,

                                                             StartTime = x.StartTime,
                                                             EndTime = x.EndTime,

                                                             RequestKey = x.RequestKey,

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                             CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    Data.Add(new OUserParameter.Details
                    {
                        ReferenceKey = "fce7fe380be9462c8b0216f2296e9b1d",
                        TypeCode = "hcore.configurationvalue",
                        TypeName = "Configuration Value",
                        UserAccountKey = "thankumerchant",
                        UserAccountDisplayName = "ThankUCash Merchant ",
                        UserAccountIconUrl = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/l/2022/1/1835435929b3443daafaee48ae86e5ad",
                        CommonKey = "1b7369bf47ba46eca516a20333333418wsx",
                        CommonName = "Reward Deduction Type",
                        CommonSystemName = "rewarddeductiontype",
                        HelperCode = "rewarddeductiontype.prepay",
                        HelperName = "Pre Pay",
                        Value = "Post Pay",
                        RequestKey = "c427fd425e7b439195e55b7b6ac1c112",
                        CreatedByKey = "d11f86853c504270b89eec9ffb90a391",
                        CreatedByDisplayName = "TUC",
                        StatusId = 2,
                        StatusCode = "default.active",
                        StatusName = "Active",
                    });
                    #endregion
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.UserAccountIconUrl))
                        {
                            DataItem.UserAccountIconUrl = _AppConfig.StorageUrl + DataItem.UserAccountIconUrl;
                        }

                        if (!string.IsNullOrEmpty(DataItem.CreatedByIconUrl))
                        {
                            DataItem.CreatedByIconUrl = _AppConfig.StorageUrl + DataItem.CreatedByIconUrl;
                        }
                    }
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserParameter", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserActivity(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ActivityId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == ListType.Lite)
                    {
                        #region Total Records
                        int TotalRecords = (from n in _HCoreContext.HCUAccountActivity
                                            select new OUserActivity
                                            {
                                                ActivityId = n.Id,
                                                ActivityKey = n.Guid,

                                                PermissionKey = n.Feature.Guid,
                                                PermissionName = n.Feature.Name,

                                                ActivityTypeCode = n.ActivityType.SystemName,
                                                ActivityTypeName = n.ActivityType.Name,

                                                ReferenceKey = n.ReferenceKey,
                                                ParentReferenceKey = n.SubReferenceKey,

                                                OldValue = n.OldValue,
                                                NewValue = n.NewValue,
                                                EntityName = n.EntityName,
                                                FieldName = n.FieldName,

                                                Description = n.Description,


                                                CreateDate = n.CreateDate,
                                                Status = n.StatusId,
                                            })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                        #region Set Default Limit
                        if (_Request.Limit == -1)
                        {
                            _Request.Limit = TotalRecords;
                        }
                        else if (_Request.Limit == 0)
                        {
                            _Request.Limit = DefaultLimit;
                        }
                        #endregion
                        #region Get Data
                        List<OUserActivity> Data = (from n in _HCoreContext.HCUAccountActivity
                                                    select new OUserActivity
                                                    {
                                                        ActivityId = n.Id,
                                                        ActivityKey = n.Guid,


                                                        PermissionKey = n.Feature.Guid,
                                                        PermissionName = n.Feature.Name,

                                                        ActivityTypeCode = n.ActivityType.SystemName,
                                                        ActivityTypeName = n.ActivityType.Name,

                                                        ReferenceKey = n.ReferenceKey,
                                                        ParentReferenceKey = n.SubReferenceKey,

                                                        EntityName = n.EntityName,
                                                        FieldName = n.FieldName,

                                                        OldValue = n.OldValue,
                                                        NewValue = n.NewValue,

                                                        Description = n.Description,


                                                        CreateDate = n.CreateDate,
                                                        Status = n.StatusId,
                                                    })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        if (Data.Count > 0)
                        {
                            foreach (var Details in Data)
                            {
                                string Description = "";
                                if (!string.IsNullOrEmpty(Details.OldValue) && !string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.FieldName + " value changed from " + Details.OldValue + " to " + Details.NewValue;
                                }
                                else if (string.IsNullOrEmpty(Details.OldValue) && string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.FieldName + "Updated";
                                }
                                else if (string.IsNullOrEmpty(Details.OldValue) && !string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.OldValue + " value added to " + Details.NewValue;
                                }
                                else if (!string.IsNullOrEmpty(Details.OldValue) && string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.OldValue + " removed from " + Details.FieldName;
                                }
                                else
                                {
                                    Description = Details.FieldName + " Updated";
                                }
                                Details.Description = Description;


                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        int TotalRecords = (from n in _HCoreContext.HCUAccountActivity
                                            select new OUserActivity
                                            {
                                                ActivityId = n.Id,
                                                ActivityKey = n.Guid,

                                                AccountTypeCode = n.Account.AccountType.SystemName,
                                                AccountTypeName = n.Account.AccountType.Name,



                                                UserDeviceKey = n.Device.Guid,
                                                UserDeviceSerialNumber = n.Device.SerialNumber,


                                                UserAccountKey = n.Account.Guid,
                                                UserAccountName = n.Account.DisplayName,
                                                UserAccountIconUrl = n.Account.IconStorage.Path,


                                                PermissionKey = n.Feature.Guid,
                                                PermissionName = n.Feature.Name,

                                                ActivityTypeCode = n.ActivityType.SystemName,
                                                ActivityTypeName = n.ActivityType.Name,

                                                ReferenceKey = n.ReferenceKey,
                                                ParentReferenceKey = n.SubReferenceKey,

                                                EntityName = n.EntityName,
                                                FieldName = n.FieldName,
                                                OldValue = n.OldValue,
                                                NewValue = n.NewValue,

                                                Description = n.Description,


                                                CreateDate = n.CreateDate,
                                                Status = n.StatusId,
                                            })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                        #region Set Default Limit
                        if (_Request.Limit == -1)
                        {
                            _Request.Limit = TotalRecords;
                        }
                        else if (_Request.Limit == 0)
                        {
                            _Request.Limit = DefaultLimit;
                        }
                        #endregion
                        #region Get Data
                        List<OUserActivity> Data = (from n in _HCoreContext.HCUAccountActivity
                                                    select new OUserActivity
                                                    {
                                                        ActivityId = n.Id,
                                                        ActivityKey = n.Guid,

                                                        AccountTypeCode = n.Account.AccountType.SystemName,
                                                        AccountTypeName = n.Account.AccountType.Name,



                                                        UserDeviceKey = n.Device.Guid,
                                                        UserDeviceSerialNumber = n.Device.SerialNumber,


                                                        UserAccountKey = n.Account.Guid,
                                                        UserAccountName = n.Account.DisplayName,
                                                        UserAccountIconUrl = n.Account.IconStorage.Path,



                                                        PermissionKey = n.Feature.Guid,
                                                        PermissionName = n.Feature.Name,

                                                        ActivityTypeCode = n.ActivityType.SystemName,
                                                        ActivityTypeName = n.ActivityType.Name,

                                                        ReferenceKey = n.ReferenceKey,
                                                        ParentReferenceKey = n.SubReferenceKey,

                                                        EntityName = n.EntityName,
                                                        FieldName = n.FieldName,
                                                        OldValue = n.OldValue,
                                                        NewValue = n.NewValue,

                                                        Description = n.Description,


                                                        CreateDate = n.CreateDate,
                                                        Status = n.StatusId,
                                                    })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        if (Data.Count > 0)
                        {
                            foreach (var Details in Data)
                            {
                                string Description = "";
                                if (!string.IsNullOrEmpty(Details.OldValue) && !string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.FieldName + " value changed from " + Details.OldValue + " to " + Details.NewValue;
                                }
                                else if (string.IsNullOrEmpty(Details.OldValue) && string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.FieldName + "Updated";
                                }
                                else if (string.IsNullOrEmpty(Details.OldValue) && !string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.OldValue + " value added to " + Details.NewValue;
                                }
                                else if (!string.IsNullOrEmpty(Details.OldValue) && string.IsNullOrEmpty(Details.NewValue))
                                {
                                    Description = Details.OldValue + " removed from " + Details.FieldName;
                                }
                                else
                                {
                                    Description = Details.FieldName + " Updated";
                                }
                                Details.Description = Description;


                                if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                                {
                                    Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                                }
                                else
                                {
                                    Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #region Expire Session
        /// <summary>
        /// Description: Expires the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ExpireUserSession(OUserSession _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM052");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _ValidateDetails = (from n in _HCoreContext.HCUAccountSession
                                                where n.Guid == _Request.ReferenceKey
                                                select n).FirstOrDefault();
                        if (_ValidateDetails != null)
                        {
                            if (_ValidateDetails.StatusId == 2)
                            {
                                _ValidateDetails.LogoutDate = HCoreHelper.GetGMTDateTime();
                                _ValidateDetails.StatusId = StatusBlocked;
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCAM049");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM051");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM050");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("ExpireSystemUserSession", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the user session list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserSessions(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "LoginDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCUAccountSession
                                        select new OUserSession
                                        {
                                            ReferenceId = n.Id,
                                            ReferenceKey = n.Guid,


                                            UserAccountKey = n.Account.Guid,
                                            UserAccountDisplayName = n.Account.DisplayName,

                                            UserAccountDeviceKey = n.Device.Guid,
                                            UserAccountDeviceName = n.Device.SerialNumber,

                                            //OsKey = n.AppVersion.Parent.Helper.SystemName,
                                            //OsName = n.AppVersion.Parent.Helper.Name,

                                            //AppKey = n.AppVersion.Parent.Guid,
                                            //AppName = n.AppVersion.Parent.Name,


                                            //AppVersionKey = n.AppVersion.Guid,
                                            //AppVersionName = n.AppVersion.Name,

                                            LastActivityDate = n.LastActivityDate,
                                            LoginDate = n.LoginDate,
                                            LogoutDate = n.LogoutDate,

                                            IpAddress = n.IPAddress,

                                            Latitude = n.Latitude,
                                            Longitude = n.Longitude,

                                            Status = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserSession> Data = (from n in _HCoreContext.HCUAccountSession
                                               select new OUserSession
                                               {
                                                   ReferenceId = n.Id,
                                                   ReferenceKey = n.Guid,


                                                   UserAccountKey = n.Account.Guid,
                                                   UserAccountDisplayName = n.Account.DisplayName,

                                                   UserAccountDeviceKey = n.Device.Guid,
                                                   UserAccountDeviceName = n.Device.SerialNumber,

                                                   //OsKey = n.AppVersion.Parent.Helper.SystemName,
                                                   //OsName = n.AppVersion.Parent.Helper.Name,

                                                   //AppKey = n.AppVersion.Parent.Guid,
                                                   //AppName = n.AppVersion.Parent.Name,

                                                   //AppVersionKey = n.AppVersion.Guid,
                                                   //AppVersionName = n.AppVersion.Name,

                                                   LastActivityDate = n.LastActivityDate,
                                                   LoginDate = n.LoginDate,
                                                   LogoutDate = n.LogoutDate,

                                                   IpAddress = n.IPAddress,

                                                   Latitude = n.Latitude,
                                                   Longitude = n.Longitude,

                                                   Status = n.StatusId,
                                                   StatusCode = n.Status.SystemName,
                                                   StatusName = n.Status.Name
                                               })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetControllerSessions", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion
    }
}
