//==================================================================================
// FileName: FrameworkSystemVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to system verification
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkSystemVerification
    {
        #region Declare
        #endregion
        #region References
        // HCoreSecurity _HCoreSecurity;
        Random _Random;
        #endregion
        #region Entities
        HCoreContext _HCoreContext;
        HCCoreVerification _HCCoreVerification;
        #endregion
        #region Obects
        OSystemVerification.Response _VerificationResponse;
        //public const string CheckMobieApiKey = "CF45507C-608C-419C-9C89-2803F6F5121C";
        #endregion 

        #region Manage Requests
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtp(OSystemVerification.Request _Request)
        {
            try
            {
                #region Declare

                #endregion
                #region Manage Operations
                if (_Request.Type == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV001");
                    #endregion
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002");
                    #endregion
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003");
                    #endregion
                }
                else if (_Request.Type == 2 && string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004");
                    #endregion
                }
                else if (_Request.Type == 3 && string.IsNullOrEmpty(_Request.EmailAddress) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV005");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = null;
                        string VEmailAddress = null;
                        #region Set V Mode
                        if (_Request.Type == 1 || _Request.Type == 3)
                        {
                            VMobileNumber = _Request.MobileNumber;
                            var CountryDetails = _HCoreContext.HCCoreCommon.Where(x => x.SubValue == _Request.CountryIsd).Select(x => new { CountryIsd = x.SubValue }).FirstOrDefault();
                            if (CountryDetails != null)
                            {
                                if (_Request.MobileNumber.StartsWith("0"))
                                {
                                    VMobileNumber = CountryDetails.CountryIsd + _Request.MobileNumber.Substring(1, (_Request.MobileNumber.Length - 1));
                                }
                                else if (_Request.MobileNumber.StartsWith(CountryDetails.CountryIsd))
                                {
                                    VMobileNumber = _Request.MobileNumber;
                                }
                                else
                                {
                                    VMobileNumber = CountryDetails.CountryIsd + _Request.MobileNumber;
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006");
                                #endregion
                            }
                        }
                        if (_Request.Type == 2 || _Request.Type == 3)
                        {
                            VEmailAddress = _Request.EmailAddress;
                        }
                        #endregion
                        #region  Process Request
                        if (_Request.Type == 1)
                        {
                            #region Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);

                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       MobileMessage = n.MobileMessage,
                                                       MobileNumber = n.MobileNumber,
                                                       ReferenceKey = n.ReferenceKey,
                                                       CountryIsd = n.CountryIsd,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Message
                                FrameworkSmsSend.SendSMS(PreviousRequest.CountryIsd, PreviousRequest.MobileNumber, PreviousRequest.MobileMessage);
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.ReferenceKey;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string JsonResponse = "";
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                                if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Local)
                                {
                                    NumberVerificationToken = "1234";
                                }
                                string AccessKey = HCoreHelper.GenerateGuid(); ;
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMessage = NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationToken + " is your verification code";
                                }
                                #endregion

                                //#region  Try SMS Broadcast
                                //OCheckMobi.OtpRequest _OtpRequest = new OCheckMobi.OtpRequest();
                                //_OtpRequest.number = "+" + VMobileNumber;
                                //_OtpRequest.type = "sms";
                                //OCheckMobi.OtpResponse _OtpResponse = CheckMobieRequestOtp(_OtpRequest, CheckMobiApiKey);
                                //if (_OtpResponse.code == "0")
                                //{
                                //    Guid = _OtpResponse.id;
                                //    JsonResponse = _OtpResponse.responsejson;
                                //    if (string.IsNullOrEmpty(JsonResponse))
                                //    {
                                //        JsonResponse = "MobiCheck";
                                //    }
                                //}
                                //else
                                //{
                                //    #region Send Message
                                //    string TJsonResponse = SendSMS(_Request.CountryIsd, VMobileNumber, TMessage);
                                //    #endregion
                                //    JsonResponse = _OtpResponse.responsejson;
                                //    if (string.IsNullOrEmpty(JsonResponse))
                                //    {
                                //        JsonResponse = "FALLBACK";
                                //    }
                                //}
                                //#endregion
                                #region Send Message
                                string TJsonResponse = FrameworkSmsSend.SendSMS(_Request.CountryIsd, VMobileNumber, TMessage);
                                #endregion
                                if (string.IsNullOrEmpty(JsonResponse))
                                {
                                    JsonResponse = "FALLBACK";
                                }
                                else
                                {
                                    JsonResponse = TJsonResponse;
                                }

                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = "CODE";
                                _HCCoreVerification.AccessCode = NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.EmailMessage = JsonResponse;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddMonths(1);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = _HCCoreVerification.Id;
                                #endregion

                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = "CODE";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 2)
                        {
                            #region Email Address Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);

                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,
                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Email 


                                #endregion
                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "HC" + _Random.Next(100, 999) + "-";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string AccessKey = HCoreHelper.GenerateGuid(); ;
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                #region Send Email

                                #endregion
                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 3)
                        {
                            #region Email And Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);

                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,

                                                       CountryIsd = n.CountryIsd,

                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,

                                                       MobileNumber = n.MobileNumber,
                                                       MobileMessage = n.MobileMessage,

                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,

                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Message
                                FrameworkSmsSend.SendSMS(PreviousRequest.CountryIsd, PreviousRequest.MobileNumber, PreviousRequest.MobileMessage);
                                #endregion
                                #region Send Email

                                #endregion
                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV011");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "HC" + _Random.Next(100, 999) + "-";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string AccessKey = HCoreHelper.GenerateGuid(); ;
                                #endregion
                                #region Build Mobile Message
                                string TMobileMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Build Email Message
                                string TEmailMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMobileMessage;

                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TEmailMessage;

                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                #region Send Message
                                FrameworkSmsSend.SendSMS(_Request.CountryIsd, VMobileNumber, TMobileMessage);
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OSystemVerification.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV012");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013");
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014");
                #endregion
            }
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtp(OSystemVerification.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015");
                        #endregion
                    }
                    else
                    {
                        string CheckMobiApiKey = HCoreHelper.GetConfiguration("checkmobiapikey");
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                //_Request.AccessCode = _Request.AccessCode.Replace(RequestDetails.AccessCodeStart, "");
                                if (RequestDetails.StatusId == 1)
                                {
                                    if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016");
                                        #endregion
                                    }
                                    else if (RequestDetails.VerificationTypeId == 2 && string.IsNullOrEmpty(_Request.AccessCode) && string.IsNullOrEmpty(_Request.AccessKey))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV017");
                                        #endregion
                                    }
                                    else
                                    {
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        if (RequestDetails.ExpiaryDate > StartDate)
                                        {
                                            if (RequestDetails.VerificationTypeId == 1)
                                            {
                                                //bool IsValidated = false;
                                                //string JsonResponse = null;
                                                #region  Try SMS Broadcast
                                                //OCheckMobi.VerifyRequest _VerifyRequest = new OCheckMobi.VerifyRequest();
                                                //_VerifyRequest.id = _Request.RequestToken;
                                                //_VerifyRequest.pin = _Request.AccessCode;
                                                //OCheckMobi.VerifyResponse _OtpResponse = CheckMobieVerifyOtp(_VerifyRequest, CheckMobiApiKey);
                                                //if (_OtpResponse.code == "0")
                                                //{
                                                //    JsonResponse = _OtpResponse.responsejson;
                                                //    IsValidated = _OtpResponse.validated;
                                                //}
                                                //else
                                                //{
                                                //    JsonResponse = _OtpResponse.responsejson;
                                                //    IsValidated = false;
                                                //}
                                                #endregion
                                                //if (IsValidated == false && RequestDetails.AccessCode == _Request.AccessCode)
                                                //{
                                                //    IsValidated = true;
                                                //}
                                                if (RequestDetails.AccessCode == _Request.AccessCode)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OSystemVerification.Response();
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV018");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode || RequestDetails.AccessKey == _Request.AccessKey)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OSystemVerification.Response();
                                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021");
                                                    #endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 3;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            _VerificationResponse = new OSystemVerification.Response();
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022");
                                            #endregion
                                        }
                                    }
                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OSystemVerification.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026");
                #endregion
            }
        }
        #endregion
        //#region Manage Requests
        //internal OResponse RequestOtp(OSystemVerification.Request _Request)
        //{
        //    try
        //    {
        //        #region Declare
        //        
        //        #endregion
        //        #region Manage Operations
        //        if (_Request.Type == 0)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV001");
        //            #endregion
        //        }
        //        else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.CountryIsd))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002");
        //            #endregion
        //        }
        //        else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.MobileNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003");
        //            #endregion
        //        }
        //        else if (_Request.Type == 2 && string.IsNullOrEmpty(_Request.EmailAddress))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004");
        //            #endregion
        //        }
        //        else if (_Request.Type == 3 && string.IsNullOrEmpty(_Request.EmailAddress) && string.IsNullOrEmpty(_Request.MobileNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV005");
        //            #endregion
        //        }
        //        else
        //        {
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                string VMobileNumber = null;
        //                string VEmailAddress = null;
        //                #region Set V Mode
        //                if (_Request.Type == 1 || _Request.Type == 3)
        //                {
        //                    VMobileNumber = _Request.MobileNumber;
        //                    var CountryDetails = _HCoreContext.HCCountry.Where(x => x.Isd == _Request.CountryIsd).Select(x => new { CountryIsd = x.Isd }).FirstOrDefault();
        //                    if (CountryDetails != null)
        //                    {
        //                        if (_Request.MobileNumber.StartsWith("0"))
        //                        {
        //                            VMobileNumber = CountryDetails.CountryIsd + _Request.MobileNumber.Substring(1, (_Request.MobileNumber.Length - 1));
        //                        }
        //                        else if (_Request.MobileNumber.StartsWith(CountryDetails.CountryIsd))
        //                        {
        //                            VMobileNumber = _Request.MobileNumber;
        //                        }
        //                        else
        //                        {
        //                            VMobileNumber = CountryDetails.CountryIsd + _Request.MobileNumber;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006");
        //                        #endregion
        //                    }
        //                }
        //                if (_Request.Type == 2 || _Request.Type == 3)
        //                {
        //                    VEmailAddress = _Request.EmailAddress;
        //                }
        //                #endregion
        //                #region  Process Request
        //                if (_Request.Type == 1)
        //                {
        //                    #region Mobile Number Verification
        //                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
        //                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
        //                    var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
        //                                           where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
        //                                           select new
        //                                           {
        //                                               ReferenceId = n.Id,
        //                                               RequestToken = n.Guid,
        //                                               AccessCode = n.AccessCode,
        //                                               AccessCodeStart = n.AccessCodeStart,
        //                                               MobileMessage = n.MobileMessage,
        //                                               MobileNumber = n.MobileNumber,
        //                                               ReferenceKey = n.ReferenceKey,
        //                                               CountryIsd = n.CountryIsd,
        //                                               Type = n.VerificationTypeId,
        //                                           }).FirstOrDefault();
        //                    if (PreviousRequest != null)
        //                    {
        //                        #region Send Message
        //                        SendSMS(PreviousRequest.CountryIsd, PreviousRequest.MobileNumber, PreviousRequest.MobileMessage);
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
        //                        _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
        //                        _VerificationResponse.Type = PreviousRequest.Type;
        //                        _VerificationResponse.Reference = PreviousRequest.ReferenceKey;
        //                        _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
        //                        _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007");
        //                        #endregion
        //                    }
        //                    else
        //                    {
        //                        #region Get code
        //                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
        //                        _Random = new Random();
        //                        String NumberVerificationPrefix = "HC" + _Random.Next(100, 999) + "-";
        //                        String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
        //                        string Guid = HCoreHelper.GenerateGuid();;
        //                        string AccessKey = HCoreHelper.GenerateGuid();;
        //                        #endregion
        //                        #region Build Message
        //                        string TMessage = null;
        //                        if (!string.IsNullOrEmpty(_Request.MobileMessage))
        //                        {
        //                            TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
        //                        }
        //                        else
        //                        {
        //                            TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
        //                        }
        //                        #endregion
        //                        #region Save Reqest
        //                        _HCCoreVerification = new HCCoreVerification();
        //                        _HCCoreVerification.Guid = Guid;
        //                        _HCCoreVerification.VerificationTypeId = _Request.Type;
        //                        _HCCoreVerification.CountryIsd = _Request.CountryIsd;
        //                        _HCCoreVerification.MobileNumber = VMobileNumber;
        //                        _HCCoreVerification.MobileMessage = TMessage;
        //                        _HCCoreVerification.AccessKey = AccessKey;
        //                        _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
        //                        _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
        //                        _HCCoreVerification.CreateDate = CurrentTime;
        //                        _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
        //                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
        //                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
        //                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
        //                        _HCCoreVerification.ReferenceKey = _Request.Reference;
        //                        _HCCoreVerification.StatusId = 1;
        //                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
        //                        _HCoreContext.SaveChanges();
        //                        long VerificationId = 0;
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
        //                        }
        //                        #endregion
        //                        #region Send Message
        //                        SendSMS(_Request.CountryIsd, VMobileNumber, TMessage);
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = VerificationId;
        //                        _VerificationResponse.MobileNumber = VMobileNumber;
        //                        _VerificationResponse.Type = _Request.Type;
        //                        _VerificationResponse.Reference = _Request.Reference;
        //                        _VerificationResponse.RequestToken = Guid;
        //                        _VerificationResponse.CodeStart = NumberVerificationPrefix;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008");
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //                else if (_Request.Type == 2)
        //                {
        //                    #region Email Address Verification
        //                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
        //                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
        //                    var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
        //                                           where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
        //                                           select new
        //                                           {
        //                                               ReferenceId = n.Id,
        //                                               RequestToken = n.Guid,
        //                                               AccessCode = n.AccessCode,
        //                                               AccessCodeStart = n.AccessCodeStart,
        //                                               EmailAddress = n.EmailAddress,
        //                                               EmailMessage = n.EmailMessage,
        //                                               Reference = n.ReferenceKey,
        //                                               Type = n.VerificationTypeId,
        //                                           }).FirstOrDefault();
        //                    if (PreviousRequest != null)
        //                    {
        //                        #region Send Email 
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
        //                        _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
        //                        _VerificationResponse.Type = PreviousRequest.Type;
        //                        _VerificationResponse.Reference = PreviousRequest.Reference;
        //                        _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
        //                        _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009");
        //                        #endregion
        //                    }
        //                    else
        //                    {
        //                        #region Get code
        //                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
        //                        _Random = new Random();
        //                        String NumberVerificationPrefix = "HC" + _Random.Next(100, 999) + "-";
        //                        String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
        //                        string Guid = HCoreHelper.GenerateGuid();;
        //                        string AccessKey = HCoreHelper.GenerateGuid();;
        //                        #endregion
        //                        #region Build Message
        //                        string TMessage = null;
        //                        if (!string.IsNullOrEmpty(_Request.EmailMessage))
        //                        {
        //                            TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
        //                        }
        //                        else
        //                        {
        //                            TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
        //                        }
        //                        #endregion
        //                        #region Save Reqest
        //                        _HCCoreVerification = new HCCoreVerification();
        //                        _HCCoreVerification.Guid = Guid;
        //                        _HCCoreVerification.VerificationTypeId = _Request.Type;
        //                        _HCCoreVerification.EmailAddress = VEmailAddress;
        //                        _HCCoreVerification.EmailMessage = TMessage;
        //                        _HCCoreVerification.AccessKey = AccessKey;
        //                        _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
        //                        _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
        //                        _HCCoreVerification.CreateDate = CurrentTime;
        //                        _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
        //                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
        //                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
        //                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
        //                        _HCCoreVerification.ReferenceKey = _Request.Reference;
        //                        _HCCoreVerification.StatusId = 1;
        //                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
        //                        _HCoreContext.SaveChanges();
        //                        long VerificationId = 0;
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
        //                        }
        //                        #endregion
        //                        #region Send Email
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = VerificationId;
        //                        _VerificationResponse.EmailAddress = VEmailAddress;
        //                        _VerificationResponse.Type = _Request.Type;
        //                        _VerificationResponse.Reference = _Request.Reference;
        //                        _VerificationResponse.RequestToken = Guid;
        //                        _VerificationResponse.CodeStart = NumberVerificationPrefix;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010");
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //                else if (_Request.Type == 3)
        //                {
        //                    #region Email And Mobile Number Verification
        //                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
        //                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
        //                    var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
        //                                           where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
        //                                           select new
        //                                           {
        //                                               ReferenceId = n.Id,
        //                                               RequestToken = n.Guid,
        //                                               CountryIsd = n.CountryIsd,
        //                                               AccessCode = n.AccessCode,
        //                                               AccessCodeStart = n.AccessCodeStart,
        //                                               MobileNumber = n.MobileNumber,
        //                                               MobileMessage = n.MobileMessage,
        //                                               EmailAddress = n.EmailAddress,
        //                                               EmailMessage = n.EmailMessage,
        //                                               Reference = n.ReferenceKey,
        //                                               Type = n.VerificationTypeId,
        //                                           }).FirstOrDefault();
        //                    if (PreviousRequest != null)
        //                    {
        //                        #region Send Message
        //                        SendSMS(PreviousRequest.CountryIsd, PreviousRequest.MobileNumber, PreviousRequest.MobileMessage);
        //                        #endregion
        //                        #region Send Email
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
        //                        _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
        //                        _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
        //                        _VerificationResponse.Type = PreviousRequest.Type;
        //                        _VerificationResponse.Reference = PreviousRequest.Reference;
        //                        _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
        //                        _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV011");
        //                        #endregion
        //                    }
        //                    else
        //                    {
        //                        #region Get code
        //                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
        //                        _Random = new Random();
        //                        String NumberVerificationPrefix = "HC" + _Random.Next(100, 999) + "-";
        //                        String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
        //                        string Guid = HCoreHelper.GenerateGuid();;
        //                        string AccessKey = HCoreHelper.GenerateGuid();;
        //                        #endregion
        //                        #region Build Mobile Message
        //                        string TMobileMessage = null;
        //                        if (!string.IsNullOrEmpty(_Request.MobileMessage))
        //                        {
        //                            TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
        //                        }
        //                        else
        //                        {
        //                            TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
        //                        }
        //                        #endregion
        //                        #region Build Email Message
        //                        string TEmailMessage = null;
        //                        if (!string.IsNullOrEmpty(_Request.EmailMessage))
        //                        {
        //                            TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
        //                        }
        //                        else
        //                        {
        //                            TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
        //                        }
        //                        #endregion
        //                        #region Save Reqest
        //                        _HCCoreVerification = new HCCoreVerification();
        //                        _HCCoreVerification.Guid = Guid;
        //                        _HCCoreVerification.VerificationTypeId = _Request.Type;
        //                        _HCCoreVerification.CountryIsd = _Request.CountryIsd;
        //                        _HCCoreVerification.MobileNumber = VMobileNumber;
        //                        _HCCoreVerification.MobileMessage = TMobileMessage;
        //                        _HCCoreVerification.EmailAddress = VEmailAddress;
        //                        _HCCoreVerification.EmailMessage = TEmailMessage;
        //                        _HCCoreVerification.AccessKey = AccessKey;
        //                        _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
        //                        _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
        //                        _HCCoreVerification.CreateDate = CurrentTime;
        //                        _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
        //                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
        //                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
        //                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
        //                        _HCCoreVerification.ReferenceKey = _Request.Reference;
        //                        _HCCoreVerification.StatusId = 1;
        //                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
        //                        _HCoreContext.SaveChanges();
        //                        long VerificationId = 0;
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
        //                        }
        //                        #endregion
        //                        #region Send Message
        //                        SendSMS(_Request.CountryIsd, VMobileNumber, TMobileMessage);
        //                        #endregion
        //                        #region Build Response
        //                        _VerificationResponse = new OSystemVerification.Response();
        //                        _VerificationResponse.ReferenceId = VerificationId;
        //                        _VerificationResponse.EmailAddress = VEmailAddress;
        //                        _VerificationResponse.MobileNumber = VMobileNumber;
        //                        _VerificationResponse.Type = _Request.Type;
        //                        _VerificationResponse.Reference = _Request.Reference;
        //                        _VerificationResponse.RequestToken = Guid;
        //                        _VerificationResponse.CodeStart = NumberVerificationPrefix;
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV012");
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013");
        //                    #endregion
        //                }
        //                #endregion
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        _FrameworkLog = new FrameworkLog();
        //        _FrameworkLog.LogException("RequestOtp", _Exception,_Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014");
        //        #endregion
        //    }
        //}
        //internal OResponse VerifyOtp(OSystemVerification.RequestVerify _Request)
        //{
        //    try
        //    {
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Declare
        //            
        //            #endregion
        //            #region Process Request
        //            if (string.IsNullOrEmpty(_Request.RequestToken))
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015");
        //                #endregion
        //            }
        //            else
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    var RequestDetails = (from n in _HCoreContext.HCCoreVerification
        //                                          where n.Guid == _Request.RequestToken
        //                                          select n).FirstOrDefault();
        //                    if (RequestDetails != null)
        //                    {
        //                        if (RequestDetails.Status == 1)
        //                        {
        //                            if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016");
        //                                #endregion
        //                            }
        //                            else if (RequestDetails.VerificationTypeId == 2 && string.IsNullOrEmpty(_Request.AccessCode) && string.IsNullOrEmpty(_Request.AccessKey))
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV017");
        //                                #endregion
        //                            }
        //                            else
        //                            {
        //                                DateTime StartDate = HCoreHelper.GetGMTDateTime();
        //                                if (RequestDetails.ExpiaryDate > StartDate)
        //                                {
        //                                    if (RequestDetails.VerificationTypeId == 1)
        //                                    {
        //                                        if (RequestDetails.AccessCode == _Request.AccessCode)
        //                                        {
        //                                            #region Update Request
        //                                            RequestDetails.Status = 2;
        //                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
        //                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
        //                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
        //                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
        //                                            _HCoreContext.SaveChanges();
        //                                            #endregion
        //                                            #region Build Response
        //                                            _VerificationResponse = new OSystemVerification.Response();
        //                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
        //                                            #endregion
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV018");
        //                                            #endregion
        //                                        }
        //                                        else
        //                                        {
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019");
        //                                            #endregion
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (RequestDetails.AccessCode == _Request.AccessCode || RequestDetails.AccessKey == _Request.AccessKey)
        //                                        {
        //                                            #region Update Request
        //                                            RequestDetails.Status = 2;
        //                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
        //                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
        //                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
        //                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
        //                                            _HCoreContext.SaveChanges();
        //                                            #endregion
        //                                            #region Build Response
        //                                            _VerificationResponse = new OSystemVerification.Response();
        //                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
        //                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
        //                                            #endregion
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
        //                                            #endregion
        //                                        }
        //                                        else
        //                                        {
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021");
        //                                            #endregion
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    #region Update Request
        //                                    RequestDetails.Status = 3;
        //                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
        //                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
        //                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
        //                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
        //                                    _HCoreContext.SaveChanges();
        //                                    #endregion
        //                                    #region Send Response
        //                                    _VerificationResponse = new OSystemVerification.Response();
        //                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
        //                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
        //                                    #endregion
        //                                    #region Send Response
        //                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022");
        //                                    #endregion
        //                                }
        //                            }
        //                        }
        //                        else if (RequestDetails.Status == 2)
        //                        {
        //                            #region Build Response
        //                            _VerificationResponse = new OSystemVerification.Response();
        //                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
        //                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
        //                            #endregion
        //                            #region Send Response
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023");
        //                            #endregion
        //                        }
        //                        else
        //                        {
        //                            #region Send Response
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024");
        //                            #endregion
        //                        }
        //                    }
        //                    else
        //                    {
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025");
        //                        #endregion
        //                    }
        //                }
        //            }
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        _FrameworkLog = new FrameworkLog();
        //        _FrameworkLog.LogException("VerifyOtp", _Exception,_Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026");
        //        #endregion
        //    }
        //}
        //#endregion
        #region Update
        /// <summary>
        /// Description: Expires the verification request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ExpireVerificationRequest(OSystemVerificationDetails _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV027");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        HCCoreVerification Data = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.ReferenceKey
                                                   select n).FirstOrDefault();
                        if (Data != null)
                        {
                            if (Data.StatusId == 3)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV028");
                                #endregion
                            }
                            else if (Data.StatusId == 2)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV029");
                                #endregion
                            }
                            else
                            {
                                Data.StatusId = StatusBlocked;
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HCV030");
                                #endregion
                            }

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV031");
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ExpireVerificationRequest", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV032");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the system verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemVerification(OSystemVerificationDetails _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV033");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OSystemVerificationDetails Data = (from n in _HCoreContext.HCCoreVerification
                                                       where n.Guid == _Request.ReferenceKey
                                                       select new OSystemVerificationDetails
                                                       {
                                                           ReferenceKey = n.Guid,
                                                           TypeCode = n.VerificationType.SystemName,
                                                           TypeName = n.VerificationType.Name,
                                                           CountryIsd = n.CountryIsd,
                                                           MobileNumber = n.MobileNumber,
                                                           EmailAddress = n.EmailAddress,
                                                           AccessKey = n.AccessKey,
                                                           AccessCode = n.AccessCode,
                                                           AccessCodeStart = n.AccessCodeStart,
                                                           EmailMessage = n.EmailMessage,
                                                           MobileMessage = n.MobileMessage,
                                                           ExpiaryDate = n.ExpiaryDate,
                                                           VerifyDate = n.VerifyDate,
                                                           RequestIpAddress = n.RequestIpAddress,
                                                           RequestLatitude = n.RequestLatitude,
                                                           RequestLongitude = n.RequestLongitude,
                                                           RequestLocation = n.RequestLocation,
                                                           VerifyAttemptCount = n.VerifyAttemptCount,
                                                           VerifyIpAddress = n.VerifyIpAddress,
                                                           VerifyLatitude = n.VerifyLatitude,
                                                           VerifyLongitude = n.VerifyLongitude,
                                                           VerifyAddress = n.VerifyAddress,
                                                           ExternalReferenceKey = n.ReferenceKey,
                                                           CreateDate = n.CreateDate,
                                                           Status = n.StatusId,
                                                           StatusCode = n.Status.SystemName,
                                                           StatusName = n.Status.Name
                                                       }).FirstOrDefault();
                    if (Data != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSystemVerification", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the system verifications.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemVerifications(OList.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCCoreVerification
                                        select new OSystemVerificationList
                                        {
                                            ReferenceKey = n.Guid,
                                            TypeCode = n.VerificationType.SystemName,
                                            TypeName = n.VerificationType.Name,
                                            CountryIsd = n.CountryIsd,
                                            MobileNumber = n.MobileNumber,
                                            EmailAddress = n.EmailAddress,
                                            AccessCodeStart = n.AccessCodeStart,
                                            ExpiaryDate = n.ExpiaryDate,
                                            VerifyDate = n.VerifyDate,
                                            CreateDate = n.CreateDate,
                                            Status = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    var Data = (from n in _HCoreContext.HCCoreVerification
                                select new
                                {
                                    ReferenceKey = n.Guid,
                                    TypeCode = n.VerificationType.SystemName,
                                    TypeName = n.VerificationType.Name,
                                    CountryIsd = n.CountryIsd,
                                    MobileNumber = n.MobileNumber,
                                    EmailAddress = n.EmailAddress,
                                    AccessCodeStart = n.AccessCodeStart,
                                    ExpiaryDate = n.ExpiaryDate,
                                    VerifyDate = n.VerifyDate,
                                    CreateDate = n.CreateDate,
                                    Status = n.StatusId,
                                    StatusCode = n.Status.SystemName,
                                    StatusName = n.Status.Name
                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSystemVerifications", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion

        /// <summary>
        /// Description: Checks the mobie request otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="ApiKey">The API key.</param>
        /// <returns>OCheckMobi.OtpResponse.</returns>
        private OCheckMobi.OtpResponse CheckMobieRequestOtp(OCheckMobi.OtpRequest _Request, string? ApiKey)
        {
            #region Call External Api
            string ResponseCode = "";
            String ResponseData = String.Empty;
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://api.checkmobi.com/v1/validation/request");
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add("Authorization", ApiKey);
            _HttpWebRequest.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                string TokenData = JsonConvert.SerializeObject(_Request);
                streamWriter.Write(TokenData);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                Stream _DataStream = _HttpWebResponse.GetResponseStream();
                StreamReader _StreamReader = new StreamReader(_DataStream);
                ResponseData = _StreamReader.ReadToEnd();
                ResponseCode = _HttpWebResponse.StatusCode.ToString();
                _StreamReader.Close();
                _DataStream.Close();
            }
            catch (WebException _WebException)
            {
                if (_WebException.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = _WebException.Response as HttpWebResponse;
                    if (response != null)
                    {
                        ResponseCode = response.StatusCode.ToString();
                        Console.WriteLine("HTTP Status Code: " + (int)response.StatusCode);
                    }
                }
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseData = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion
            if (ResponseCode == "200" || ResponseCode == "204")
            {
                OCheckMobi.OtpResponse _Response = JsonConvert.DeserializeObject<OCheckMobi.OtpResponse>(ResponseData);
                _Response.code = "0";
                _Response.responsejson = ResponseData;
                return _Response;
            }
            else
            {
                OCheckMobi.OtpResponse _Response = JsonConvert.DeserializeObject<OCheckMobi.OtpResponse>(ResponseData);
                _Response.responsejson = ResponseData;
                return _Response;
            }
        }

        /// <summary>
        /// Description: Checks the mobie verify otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="ApiKey">The API key.</param>
        /// <returns>OCheckMobi.VerifyResponse.</returns>
        private OCheckMobi.VerifyResponse CheckMobieVerifyOtp(OCheckMobi.VerifyRequest _Request, string? ApiKey)
        {
            #region Call External Api
            string ResponseCode = "";
            String ResponseData = String.Empty;
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://api.checkmobi.com/v1/validation/verify");
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add("Authorization", ApiKey);
            _HttpWebRequest.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                string TokenData = JsonConvert.SerializeObject(_Request);
                streamWriter.Write(TokenData);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                Stream _DataStream = _HttpWebResponse.GetResponseStream();
                StreamReader _StreamReader = new StreamReader(_DataStream);
                ResponseData = _StreamReader.ReadToEnd();
                ResponseCode = _HttpWebResponse.StatusCode.ToString();
                _StreamReader.Close();
                _DataStream.Close();
            }
            catch (WebException _WebException)
            {
                if (_WebException.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = _WebException.Response as HttpWebResponse;
                    if (response != null)
                    {
                        ResponseCode = response.StatusCode.ToString();
                    }
                }
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseData = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion
            if (ResponseCode == "200" || ResponseCode == "204")
            {
                OCheckMobi.VerifyResponse _Response = JsonConvert.DeserializeObject<OCheckMobi.VerifyResponse>(ResponseData);
                _Response.code = "0";
                _Response.responsejson = ResponseData;
                return _Response;
            }
            else
            {
                OCheckMobi.VerifyResponse _Response = JsonConvert.DeserializeObject<OCheckMobi.VerifyResponse>(ResponseData);
                _Response.responsejson = ResponseData;
                _Response.validated = false;
                return _Response;
            }
        }
    }
}
