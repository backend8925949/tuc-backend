//==================================================================================
// FileName: FrameworkUserAccountAccess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to useraccount access
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Background;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Framework
{
    public class FrameworkUserAccountAccess
    {
        #region Declare
        FrameworkUserDevice _FrameworkUserDevice;
        #endregion
        #region References
        HCUAccountSession _HCUAccountSession;
        #endregion
        #region Entities
        Random _Random;
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccountDevice _HCUAccountDevice;
        HCUAccount _HCUAccount;
        HCUAccountOwner _HCUAccountOwner;
        HCUAccountParameter _HCUAccountParameter;
        #endregion
        #region Objects
        List<HCUAccountDevice> _UserDevices;
        OUserDeviceSave _OUserDeviceSave;
        OUserAccountAccess.OResponse _UserAuthResponse;
        OUserAccountAccess.OUser _ObjectUser;
        OUserAccountAccess.OUserAccount _ObjectUserAccount;
        OUserAccountAccess.OSubscription _OSubscription;
        List<OUserAccountAccess.OUserAccountRolePermission> _Permisssions;
        List<OUserAccountAccess.UserAccountTypes> _UserAccountTypes;
        const string rolesrights = "{\"StoreManager\":[{\"Name\":\"AllCustomers\",\"SystemName\":\"allcustomer\"},{\"Name\":\"CustomerVisits\",\"SystemName\":\"customervisit\"},{\"Name\":\"Rewards&Redeem\",\"SystemName\":\"rewardsredeem\"},{\"Name\":\"RedeemHistory\",\"SystemName\":\"redeemhistory\"},{\"Name\":\"RewardHistory\",\"SystemName\":\"rewardhistory\"},{\"Name\":\"PendingRewardHistory\",\"SystemName\":\"pendingrewardhistory\"},{\"Name\":\"MySales\",\"SystemName\":\"mysales\"},{\"Name\":\"AccountSettings\",\"SystemName\":\"accountsettings\"},{\"Name\":\"POSTerminals\",\"SystemName\":\"posterminals\"},{\"Name\":\"Cashier\",\"SystemName\":\"cashier\"},{\"Name\":\"Help\",\"SystemName\":\"help\"},{\"Name\":\"FAQ\",\"SystemName\":\"faq\"},{\"Name\":\"ProductTour\",\"SystemName\":\"producttour\"},{\"Name\":\"Profile\",\"SystemName\":\"profile\"},{\"Name\":\"BusinessSettings\",\"SystemName\":\"businesssettings\"},{\"Name\":\"Stores\",\"SystemName\":\"bsstores\"},{\"Name\":\"POS\",\"SystemName\":\"bspos\"},{\"Name\":\"Cashiers\",\"SystemName\":\"bscashiers\"}],\"Admin\":[{\"Name\":\"AllCustomers\",\"SystemName\":\"allcustomer\"},{\"Name\":\"CustomerVisits\",\"SystemName\":\"customervisit\"},{\"Name\":\"Rewards&Redeem\",\"SystemName\":\"rewardsredeem\"},{\"Name\":\"RedeemHistory\",\"SystemName\":\"redeemhistory\"},{\"Name\":\"RewardHistory\",\"SystemName\":\"rewardhistory\"},{\"Name\":\"PendingRewardHistory\",\"SystemName\":\"pendingrewardhistory\"},{\"Name\":\"MarketingTool\",\"SystemName\":\"marketingtool\"},{\"Name\":\"SMSMarketing\",\"SystemName\":\"smsmarketing\"},{\"Name\":\"EmailMarketing\",\"SystemName\":\"emailmarketing\"},{\"Name\":\"Survey\",\"SystemName\":\"survey\"},{\"Name\":\"MySales\",\"SystemName\":\"mysales\"},{\"Name\":\"Stores\",\"SystemName\":\"stores\"},{\"Name\":\"AccountSettings\",\"SystemName\":\"accountsettings\"},{\"Name\":\"POSTerminals\",\"SystemName\":\"posterminals\"},{\"Name\":\"Cashier\",\"SystemName\":\"cashier\"},{\"Name\":\"Managers\",\"SystemName\":\"managers\"},{\"Name\":\"FAQ\",\"SystemName\":\"faq\"},{\"Name\":\"ProductTour\",\"SystemName\":\"producttour\"},{\"Name\":\"Profile\",\"SystemName\":\"profile\"},{\"Name\":\"BusinessSettings\",\"SystemName\":\"businesssettings\"},{\"Name\":\"Stores\",\"SystemName\":\"bsstores\"},{\"Name\":\"POS\",\"SystemName\":\"bspos\"},{\"Name\":\"Cashiers\",\"SystemName\":\"bscashiers\"},{\"Name\":\"SubAccounts\",\"SystemName\":\"subaccounts\"},{\"Name\":\"LoyaltyConfiguration\",\"SystemName\":\"loyaltyconfiguration\"},{\"Name\":\"RewardsPercentage\",\"SystemName\":\"rewardpercentage\"}],\"Merchant\":[{\"Name\":\"AllCustomers\",\"SystemName\":\"allcustomer\"},{\"Name\":\"CustomerVisits\",\"SystemName\":\"customervisit\"},{\"Name\":\"Rewards&Redeem\",\"SystemName\":\"rewardsredeem\"},{\"Name\":\"RedeemHistory\",\"SystemName\":\"redeemhistory\"},{\"Name\":\"RewardHistory\",\"SystemName\":\"rewardhistory\"},{\"Name\":\"PendingRewardHistory\",\"SystemName\":\"pendingrewardhistory\"},{\"Name\":\"MarketingTool\",\"SystemName\":\"marketingtool\"},{\"Name\":\"SMSMarketing\",\"SystemName\":\"smsmarketing\"},{\"Name\":\"EmailMarketing\",\"SystemName\":\"emailmarketing\"},{\"Name\":\"Survey\",\"SystemName\":\"survey\"},{\"Name\":\"MySales\",\"SystemName\":\"mysales\"},{\"Name\":\"Stores\",\"SystemName\":\"stores\"},{\"Name\":\"Subscription\",\"SystemName\":\"subscription\"},{\"Name\":\"AccountSettings\",\"SystemName\":\"accountsettings\"},{\"Name\":\"POSTerminals\",\"SystemName\":\"posterminals\"},{\"Name\":\"Cashier\",\"SystemName\":\"cashier\"},{\"Name\":\"Managers\",\"SystemName\":\"managers\"},{\"Name\":\"Help\",\"SystemName\":\"help\"},{\"Name\":\"FAQ\",\"SystemName\":\"faq\"},{\"Name\":\"ProductTour\",\"SystemName\":\"producttour\"},{\"Name\":\"Profile\",\"SystemName\":\"profile\"},{\"Name\":\"BusinessSettings\",\"SystemName\":\"businesssettings\"},{\"Name\":\"Stores\",\"SystemName\":\"bsstores\"},{\"Name\":\"POS\",\"SystemName\":\"bspos\"},{\"Name\":\"Cashiers\",\"SystemName\":\"bscashiers\"},{\"Name\":\"SubAccounts\",\"SystemName\":\"subaccounts\"},{\"Name\":\"LoyaltyConfiguration\",\"SystemName\":\"loyaltyconfiguration\"},{\"Name\":\"RewardsPercentage\",\"SystemName\":\"rewardpercentage\"},{\"Name\":\"Subscription\",\"SystemName\":\"lssubscription\"},{\"Name\":\"Apps&Keys\",\"SystemName\":\"appskeys\"}]}";
        #endregion
        #region Get User Profile
        /// <summary>
        /// Description: Gets the user profile.
        /// </summary>
        /// <param name="UserAccountKey">The user account key.</param>
        /// <param name="DeviceKey">The device key.</param>
        /// <param name="SessionKey">The session key.</param>
        /// <param name="NotificationUrl">The notification URL.</param>
        /// <param name="_UserReference">The user reference.</param>
        /// <param name="PlatformCode">The platform code.</param>
        /// <returns>OResponse.</returns>
        private OResponse GetUserProfile(string UserAccountKey, string? DeviceKey, string? SessionKey, string? NotificationUrl, OUserReference _UserReference, string? PlatformCode)
        {
            try
            {
                #region Declare 
                _UserAuthResponse = new OUserAccountAccess.OResponse();
                _ObjectUser = new OUserAccountAccess.OUser();
                _ObjectUserAccount = new OUserAccountAccess.OUserAccount();
                _UserAccountTypes = new List<OUserAccountAccess.UserAccountTypes>();
                string UserAccountSessionKey = HCoreHelper.GenerateGuid();
                #endregion
                #region Build Details
                using (_HCoreContext = new HCoreContext())
                {
                    var userAccount = _HCoreContext.HCUAccount.FirstOrDefault(x => x.Guid == UserAccountKey);
                    if (userAccount != null && string.IsNullOrEmpty(userAccount.ReferralCode))
                    {
                        userAccount.ReferralCode = userAccount.MobileNumber;
                        _HCoreContext.SaveChanges();
                    }

                    var UserDetails = (from n in _HCoreContext.HCUAccount
                                       where n.Guid == UserAccountKey
                                       select new
                                       {
                                           UserId = n.UserId,
                                           UserKey = n.Guid,

                                           UserAccountId = n.Id,
                                           UserAccountKey = n.Guid,

                                           AccountTypeId = n.AccountTypeId,

                                           OwnerId = n.OwnerId,
                                           OwnerCountryId = n.Owner.CountryId,
                                           SubOwnerId = n.SubOwnerId,
                                           UserName = n.User.Username,
                                           DisplayName = n.DisplayName,
                                           Name = n.Name,
                                           FirstName = n.FirstName,
                                           LastName = n.LastName,

                                           EmailAddress = n.EmailAddress,
                                           ContactNumber = n.ContactNumber,
                                           MobileNumber = n.MobileNumber,

                                           DateOfBirth = n.DateOfBirth,

                                           Gender = n.Gender.Name,
                                           GenderCode = n.Gender.SystemName,


                                           Address = n.Address,
                                           AddressLatitude = n.Latitude,
                                           AddressLongitude = n.Longitude,

                                           AccountTypeName = n.AccountType.Name,
                                           AccountTypeCode = n.AccountType.SystemName,

                                           RoleId = n.RoleId,
                                           RoleName = n.Role.Name,
                                           RoleKey = n.Role.Guid,

                                           ReferralCode = n.MobileNumber,
                                           ReferralUrl = n.ReferralUrl,

                                           AccountCode = n.AccountCode,
                                           AccessPin = n.AccessPin,

                                           IconUrl = n.IconStorage.Path,
                                           PosterUrl = n.PosterStorage.Path,

                                           EmailVerificationStatus = n.EmailVerificationStatus,
                                           ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                           CountryId = n.CountryId,
                                           CreateDate = n.CreateDate,
                                           ModifyDate = n.ModifyDate,
                                           LastLoginDate = n.LastLoginDate,
                                           IsTempPin = n.IsTempPin,
                                           SubscriptionId = n.SubscriptionId
                                       }).FirstOrDefault();
                    if (UserDetails != null)
                    {
                        long DeviceId = _HCoreContext.HCUAccountDevice.Where(x => x.Guid == DeviceKey).Select(x => x.Id).FirstOrDefault();

                        #region Set User Object
                        _ObjectUser = new OUserAccountAccess.OUser();
                        _ObjectUser.UserName = UserDetails.UserName;

                        _ObjectUser.Name = UserDetails.Name;
                        _ObjectUser.FirstName = UserDetails.FirstName;
                        _ObjectUser.LastName = UserDetails.LastName;

                        _ObjectUser.ContactNumber = UserDetails.ContactNumber;
                        _ObjectUser.MobileNumber = UserDetails.MobileNumber;
                        _ObjectUser.EmailAddress = UserDetails.EmailAddress;

                        _ObjectUser.Gender = UserDetails.Gender;
                        _ObjectUser.GenderCode = UserDetails.Gender;

                        _ObjectUser.DateOfBirth = UserDetails.DateOfBirth;

                        _ObjectUser.Address = UserDetails.Address;
                        _ObjectUser.AddressLatitude = UserDetails.AddressLatitude;
                        _ObjectUser.AddressLongitude = UserDetails.AddressLongitude;

                        _ObjectUser.EmailVerificationStatus = UserDetails.EmailVerificationStatus;
                        _ObjectUser.ContactNumberVerificationStatus = UserDetails.ContactNumberVerificationStatus;


                        _ObjectUser.LinkedAccounts = (from n in _HCoreContext.HCUAccount
                                                      where n.UserId == UserDetails.UserId
                                                      select new OUserAccountAccess.OUserLinkedAccounts
                                                      {
                                                          AccountId = n.Id,
                                                          AccountTypeId = n.AccountTypeId,
                                                          OwnerName = n.Owner.DisplayName,
                                                          DisplayName = n.DisplayName,
                                                          AccountKey = n.Guid,
                                                          AccountCode = n.AccountCode,
                                                          AccountType = n.AccountType.Name,
                                                          AccountTypeCode = n.AccountType.SystemName,
                                                          Status = n.StatusId,
                                                          IsActiveSession = 0,
                                                          CardNumber = n.Card.CardNumber,
                                                          CardTypeName = n.Card.CardType.Name,
                                                          CreateDate = n.CreateDate

                                                      }).ToList();
                        foreach (var LinkedAccount in _ObjectUser.LinkedAccounts)
                        {
                            if (LinkedAccount.AccountId == UserDetails.UserAccountId)
                            {
                                LinkedAccount.IsActiveSession = 1;
                            }
                            else
                            {
                                LinkedAccount.IsActiveSession = 0;
                            }

                            if (!string.IsNullOrEmpty(LinkedAccount.IconUrl))
                            {
                                LinkedAccount.IconUrl = _AppConfig.StorageUrl + LinkedAccount.IconUrl;
                            }
                            else
                            {
                                LinkedAccount.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _UserAuthResponse.User = _ObjectUser;
                        #endregion
                        #region Country
                        _UserAuthResponse.UserCountry = (from n in _HCoreContext.HCCoreCountry
                                                         where n.Id == UserDetails.CountryId
                                                         select new OUserAccountAccess.OUserAccountCountry
                                                         {
                                                             CountryId = n.Id,
                                                             CountryKey = n.Guid,
                                                             CountryName = n.Name,
                                                             CountryIso = n.Iso,
                                                             CountryIsd = n.Isd,
                                                             CurrencyName = n.CurrencyName,
                                                             CurrencyNotation = n.CurrencyNotation,
                                                             CurrencyCode = n.CurrencyHex,
                                                             CurrencySymbol = n.CurrencySymbol,
                                                         }).FirstOrDefault();
                        if (_UserAuthResponse.UserCountry == null)
                        {
                            _UserAuthResponse.UserCountry = (from n in _HCoreContext.HCCoreCountry
                                                             where n.Id == UserDetails.OwnerCountryId
                                                             select new OUserAccountAccess.OUserAccountCountry
                                                             {
                                                                 CountryId = n.Id,
                                                                 CountryKey = n.Guid,
                                                                 CountryName = n.Name,
                                                                 CountryIso = n.Iso,
                                                                 CountryIsd = n.Isd,
                                                                 CurrencyName = n.CurrencyName,
                                                                 CurrencyNotation = n.CurrencyNotation,
                                                                 CurrencyCode = n.CurrencyHex,
                                                                 CurrencySymbol = n.CurrencySymbol,
                                                             }).FirstOrDefault();
                        }
                        #endregion
                        #region Owner
                        if (UserDetails.OwnerId != null && UserDetails.OwnerId != 0)
                        {
                            _UserAuthResponse.UserOwner = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.OwnerId).Select(x => new OUserAccountAccess.OUserAccountOwner
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                AccountCode = x.AccountCode,
                                DisplayName = x.DisplayName,
                                Name = x.Name,
                                IconUrl = x.IconStorage.Path,
                                AccountTypeId = x.AccountTypeId,
                                UserAccountId = x.Id,
                                AccountTypeCode = x.AccountType.SystemName,
                            }).FirstOrDefault();
                            if (_UserAuthResponse.UserOwner != null)
                            {
                                if (!string.IsNullOrEmpty(_UserAuthResponse.UserOwner.IconUrl))
                                {
                                    _UserAuthResponse.UserOwner.IconUrl = _AppConfig.StorageUrl + _UserAuthResponse.UserOwner.IconUrl;
                                }
                                else
                                {
                                    _UserAuthResponse.UserOwner.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        if (UserDetails.SubOwnerId != null && UserDetails.SubOwnerId != 0)
                        {
                            _UserAuthResponse.UserSubOwner = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.SubOwnerId).Select(x => new OUserAccountAccess.OUserAccountOwner
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                AccountCode = x.AccountCode,
                                DisplayName = x.DisplayName,
                                Name = x.Name,
                                IconUrl = x.IconStorage.Path,
                                AccountTypeId = x.AccountTypeId,
                                UserAccountId = x.Id,
                                AccountTypeCode = x.AccountType.SystemName,
                            }).FirstOrDefault();
                            if (_UserAuthResponse.UserSubOwner != null)
                            {
                                if (!string.IsNullOrEmpty(_UserAuthResponse.UserSubOwner.IconUrl))
                                {
                                    _UserAuthResponse.UserSubOwner.IconUrl = _AppConfig.StorageUrl + _UserAuthResponse.UserSubOwner.IconUrl;
                                }
                                else
                                {
                                    _UserAuthResponse.UserSubOwner.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        #endregion
                        #region Set user account
                        _ObjectUserAccount = new OUserAccountAccess.OUserAccount();
                        _ObjectUserAccount.UserKey = UserDetails.UserKey;
                        _ObjectUserAccount.DisplayName = UserDetails.DisplayName;
                        _ObjectUserAccount.AccountId = UserDetails.UserAccountId;
                        _ObjectUserAccount.AccountKey = UserDetails.UserAccountKey;
                        _ObjectUserAccount.AccountType = UserDetails.AccountTypeName;
                        _ObjectUserAccount.AccountTypeCode = UserDetails.AccountTypeCode;

                        _ObjectUserAccount.RoleName = UserDetails.RoleName;
                        _ObjectUserAccount.RoleKey = UserDetails.RoleKey;

                        _ObjectUserAccount.ReferralCode = UserDetails.ReferralCode;
                        _ObjectUserAccount.ReferralUrl = UserDetails.ReferralUrl;
                        _ObjectUserAccount.AccountCode = UserDetails.AccountCode;
                        _ObjectUserAccount.CreateDate = UserDetails.CreateDate;
                        _ObjectUserAccount.ProgramDetails = _HCoreContext.TUCLProgram
                                                            .Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                    && x.ProgramType.SystemName == "programtype.grouployalty"
                                                                    && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new OUserAccountAccess.ProgramDetails
                                                            {
                                                                ProgramId = x.Id,
                                                                ProgramReferenceKey = x.Guid,
                                                                SystemName = x.ProgramType.SystemName
                                                            }).FirstOrDefault();
                        if (_ObjectUserAccount.ProgramDetails != null)
                        {
                            _ObjectUserAccount.IsGroupLoyalty = _ObjectUserAccount.ProgramDetails.ProgramId > 0 ? true : false;
                        }
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant)
                        {
                            _ObjectUserAccount.CloseOpenDetails = _HCoreContext.TUCLProgram
                                                                .Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                        && (x.ProgramType.SystemName == "programtype.openloyalty"
                                                                        || x.ProgramType.SystemName == "programtype.closeloyalty")
                                                                        && x.StatusId == HelperStatus.Default.Active)
                                                                .Select(x => new OUserAccountAccess.ProgramDetails
                                                                {
                                                                    ProgramId = x.Id,
                                                                    ProgramReferenceKey = x.Guid,
                                                                    SystemName = x.ProgramType.SystemName
                                                                }).FirstOrDefault();

                            _ObjectUserAccount.IsOpenCloseLoyalty = false;
                            if (_ObjectUserAccount.CloseOpenDetails != null)
                            {
                                _ObjectUserAccount.IsOpenCloseLoyalty = (_ObjectUserAccount.CloseOpenDetails.ProgramId > 0
                                                                        && _ObjectUserAccount.CloseOpenDetails.SystemName == "programtype.closeloyalty") ? true : false;
                            }
                            else
                            {
                                var details = _HCoreContext.HCUAccountParameter
                                                .Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                    && x.TypeId == HelperType.ThankUCashLoyalty
                                                    && (x.SubTypeId == HelperType.ThankUCashLoyalties.OpenLoyalty || x.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty)
                                                    ).
                                                    Select(x => new
                                                    {
                                                        SubTypeId = x.SubTypeId
                                                    }).FirstOrDefault();
                                if (details != null)
                                {
                                    _ObjectUserAccount.IsOpenCloseLoyalty = (details.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty) ? true : false;
                                }
                            }
                        }
                        #region Store Access
                        if (UserDetails.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.OwnerId)
                                .Select(x => new
                                {
                                    MerchantId = x.Id,
                                }).FirstOrDefault();
                            _ObjectUserAccount.CloseOpenDetails = _HCoreContext.TUCLProgram
                                                                .Where(x => (x.AccountId == MerchantDetails.MerchantId)
                                                                        && (x.ProgramType.SystemName == "programtype.openloyalty"
                                                                        || x.ProgramType.SystemName == "programtype.closeloyalty")
                                                                        && x.StatusId == HelperStatus.Default.Active)
                                                                .Select(x => new OUserAccountAccess.ProgramDetails
                                                                {
                                                                    ProgramId = x.Id,
                                                                    ProgramReferenceKey = x.Guid,
                                                                    SystemName = x.ProgramType.SystemName
                                                                }).FirstOrDefault();

                            _ObjectUserAccount.IsOpenCloseLoyalty = false;
                            if (_ObjectUserAccount.CloseOpenDetails != null)
                            {
                                _ObjectUserAccount.IsOpenCloseLoyalty = (_ObjectUserAccount.CloseOpenDetails.ProgramId > 0
                                                                        && _ObjectUserAccount.CloseOpenDetails.SystemName == "programtype.closeloyalty") ? true : false;
                            }
                            else
                            {
                                var details = _HCoreContext.HCUAccountParameter
                                                .Where(x => (x.AccountId == MerchantDetails.MerchantId)
                                                    && x.TypeId == HelperType.ThankUCashLoyalty
                                                    && (x.SubTypeId == HelperType.ThankUCashLoyalties.OpenLoyalty || x.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty)
                                                    ).
                                                    Select(x => new
                                                    {
                                                        SubTypeId = x.SubTypeId
                                                    }).FirstOrDefault();
                                if (details != null)
                                {
                                    _ObjectUserAccount.IsOpenCloseLoyalty = (details.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty) ? true : false;
                                }
                            }
                        }
                        #endregion
                        if (!string.IsNullOrEmpty(UserDetails.AccessPin))
                        {
                            _ObjectUserAccount.IsAccessPinSet = 1;
                        }
                        else
                        {
                            _ObjectUserAccount.IsAccessPinSet = 0;
                        }
                        if (!string.IsNullOrEmpty(UserDetails.IconUrl))
                        {
                            _ObjectUserAccount.IconUrl = _AppConfig.StorageUrl + UserDetails.IconUrl;
                        }
                        else
                        {
                            _ObjectUserAccount.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(UserDetails.PosterUrl))
                        {
                            _ObjectUserAccount.PosterUrl = _AppConfig.StorageUrl + UserDetails.PosterUrl;
                        }
                        else
                        {
                            _ObjectUserAccount.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #endregion
                        #region Save User Account Session
                        long UserSessionId = 0;
                        string UserAccessKey = null;
                        int PlatformId = Helpers.Platform.Other;
                        if (!string.IsNullOrEmpty(PlatformCode))
                        {
                            if (PlatformCode == "web")
                            {
                                PlatformId = Helpers.Platform.Web;
                            }
                            else if (PlatformCode == "mobile")
                            {
                                PlatformId = Helpers.Platform.Mobile;
                            }
                        }
                        DateTime _LogOutDate = HCoreHelper.GetGMTDateTime();
                        if (string.IsNullOrEmpty(SessionKey))
                        {
                            if (PlatformId > 0)
                            {
                                var UserActiveSessions = (from n in _HCoreContext.HCUAccountSession
                                                          where n.Account.Guid == UserAccountKey && n.StatusId == StatusActive && n.PlatformId == PlatformId
                                                          select n).ToList();
                                foreach (var UserActiveSession in UserActiveSessions)
                                {
                                    UserActiveSession.LogoutDate = _LogOutDate;
                                    UserActiveSession.StatusId = StatusBlocked;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {

                                var UserActiveSessions = (from n in _HCoreContext.HCUAccountSession
                                                          where n.Account.Guid == UserAccountKey && n.StatusId == StatusActive
                                                          select n).ToList();
                                foreach (var UserActiveSession in UserActiveSessions)
                                {
                                    UserActiveSession.LogoutDate = _LogOutDate;
                                    UserActiveSession.StatusId = StatusBlocked;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            #region Subscription And Feature
                            _OSubscription = new OUserAccountAccess.OSubscription();
                            _OSubscription.IsSubscriptionActive = false;
                            //_OSubscription.IsSubscriptionAvailable = false;
                            if (UserDetails.AccountTypeId == UserAccountType.Merchant || UserDetails.AccountTypeId == UserAccountType.MerchantStore || UserDetails.AccountTypeId == UserAccountType.MerchantSubAccount)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    long SubScriptionAccId = UserDetails.UserAccountId;
                                    if (UserDetails.AccountTypeId != UserAccountType.Merchant)
                                    {
                                        SubScriptionAccId = (long)UserDetails.OwnerId;
                                    }
                                    //  long AccountSubScriptionId = _HCoreContext.HCUAccountSubscription.Where(x =>
                                    //x.AccountId == SubScriptionAccId &&
                                    //x.StatusId == HelperStatus.Default.Active)
                                    //    .Select(x => x.SubscriptionId)
                                    //    .FirstOrDefault();
                                    long AccountSubScriptionId = 0;
                                    var MerchantSubscription = _HCoreContext.HCUAccountSubscription
                                                                .Where(x => x.AccountId == SubScriptionAccId
                                                                && x.StatusId == HelperStatus.Default.Active)
                                                                // && x.Subscription.Guid == "merchantstoresubscription")
                                                                .OrderByDescending(x => x.Id)
                                                                .Select(x => new
                                                                {
                                                                    PlanId = x.SubscriptionId,
                                                                    PlanKey = x.Subscription.Guid,
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    Title = x.Subscription.Name,
                                                                    StartDate = x.StartDate,
                                                                    EndDate = x.EndDate,
                                                                    StatusId = x.StatusId,
                                                                    Amount = x.SellingPrice,
                                                                })
                                                                .FirstOrDefault();
                                    if (MerchantSubscription != null)
                                    {
                                        if (MerchantSubscription.StatusId == HelperStatus.Default.Active)
                                        {
                                            AccountSubScriptionId = MerchantSubscription.PlanId;
                                            _OSubscription.PlanReferenceId = MerchantSubscription.PlanId;
                                            _OSubscription.PlanReferenceKey = MerchantSubscription.PlanKey;
                                            _OSubscription.SubscriptionId = MerchantSubscription.ReferenceId;
                                            _OSubscription.SubscriptionKey = MerchantSubscription.ReferenceKey;
                                            _OSubscription.Amount = (double)MerchantSubscription.Amount;
                                            _OSubscription.Title = MerchantSubscription.Title;
                                            _OSubscription.StartDate = MerchantSubscription.StartDate;
                                            _OSubscription.EndDate = MerchantSubscription.EndDate;
                                            int RemainingDays = (MerchantSubscription.EndDate - HCoreHelper.GetGMTDate()).Days;
                                            _OSubscription.DaysLeft = RemainingDays;

                                            if (_ObjectUserAccount.IsOpenCloseLoyalty)
                                            {
                                                var IsSubscription = _HCoreContext.HCUAccountSubscription
                                                                        .Where(x => x.AccountId == SubScriptionAccId
                                                                            && x.StatusId == HelperStatus.Default.Active
                                                                            && x.Subscription.Guid == "merchantstoresubscription"
                                                                            && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date)
                                                                        .ToList();
                                                if (IsSubscription.Count() == 0)
                                                {
                                                    _OSubscription.IsSubscriptionActive = false;
                                                }
                                                else
                                                {
                                                    _OSubscription.IsSubscriptionActive = true;
                                                }
                                            }
                                            else
                                            {
                                                if (RemainingDays < 0)
                                                {
                                                    _OSubscription.IsSubscriptionActive = false;
                                                }
                                                else
                                                {
                                                    _OSubscription.IsSubscriptionActive = true;
                                                }
                                            }

                                            _UserAuthResponse.Subscription = _OSubscription;
                                        }
                                        else
                                        {
                                            _OSubscription.PlanReferenceId = MerchantSubscription.PlanId;
                                            _OSubscription.PlanReferenceKey = MerchantSubscription.PlanKey;
                                            _OSubscription.SubscriptionId = MerchantSubscription.ReferenceId;
                                            _OSubscription.SubscriptionKey = MerchantSubscription.ReferenceKey;
                                            _OSubscription.Amount = (double)MerchantSubscription.Amount;
                                            _OSubscription.Title = MerchantSubscription.Title;
                                            _OSubscription.StartDate = MerchantSubscription.StartDate;
                                            _OSubscription.EndDate = MerchantSubscription.EndDate;
                                            int RemainingDays = (MerchantSubscription.EndDate - HCoreHelper.GetGMTDate()).Days;
                                            _OSubscription.DaysLeft = RemainingDays;
                                            if (RemainingDays < 0)
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                            else
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                            //_OSubscription.IsSubscriptionAvailable = true;
                                            _UserAuthResponse.Subscription = _OSubscription;
                                        }
                                    }
                                    else
                                    {
                                        _OSubscription.IsSubscriptionActive = false;
                                        //_OSubscription.IsSubscriptionAvailable = false;
                                        _UserAuthResponse.Subscription = _OSubscription;
                                    }
                                    var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.AccountTypeId == UserAccountType.Merchant).FirstOrDefault();
                                    if (AccountSubScriptionId == 0)
                                    {
                                        if (!_ObjectUserAccount.IsOpenCloseLoyalty)
                                        {
                                            HCUAccountSubscription _HCUAccountSubscription = new HCUAccountSubscription();
                                            _HCUAccountSubscription = new HCUAccountSubscription();
                                            _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                                            _HCUAccountSubscription.AccountId = SubScriptionAccId;
                                            _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                                            _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                                            _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(SubscriptionDetails.TotalDays);
                                            _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                                            _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                                            _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                                            _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountSubscription.CreatedById = 1;
                                            _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                                            _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                                            _HCoreContext.SaveChanges();
                                            AccountSubScriptionId = _HCUAccountSubscription.Id;
                                            using (HCoreContext _HCoreContext1 = new HCoreContext())
                                            {
                                                var AccX = _HCoreContext1.HCUAccount.Where(x => x.Id == SubScriptionAccId).FirstOrDefault();
                                                AccX.SubscriptionId = AccountSubScriptionId;
                                                _HCoreContext1.SaveChanges();
                                                _HCoreContext1.Dispose();
                                            }
                                            _OSubscription.PlanReferenceId = SubscriptionDetails.Id;
                                            _OSubscription.PlanReferenceKey = SubscriptionDetails.Guid;
                                            _OSubscription.SubscriptionId = _HCUAccountSubscription.Id;
                                            _OSubscription.SubscriptionKey = _HCUAccountSubscription.Guid;
                                            _OSubscription.Amount = (double)_HCUAccountSubscription.SellingPrice;
                                            _OSubscription.Title = SubscriptionDetails.Name;
                                            _OSubscription.StartDate = _HCUAccountSubscription.StartDate;
                                            _OSubscription.EndDate = _HCUAccountSubscription.EndDate;
                                            int RemainingDays = (_HCUAccountSubscription.EndDate - HCoreHelper.GetGMTDate()).Days;
                                            _OSubscription.DaysLeft = RemainingDays;
                                            if (RemainingDays > 0)
                                            {
                                                _OSubscription.IsSubscriptionActive = true;
                                            }
                                            else
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                        }
                                        else
                                        {
                                            var IsSubscription = _HCoreContext.HCUAccountSubscription
                                                                        .Where(x => x.AccountId == SubScriptionAccId
                                                                            && x.StatusId == HelperStatus.Default.Active
                                                                            && x.Subscription.Guid == "merchantstoresubscription"
                                                                            && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date)
                                                                        .ToList();
                                            if (IsSubscription.Count() == 0)
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                            else
                                            {
                                                _OSubscription.IsSubscriptionActive = true;
                                            }
                                        }
                                        // _OSubscription.IsSubscriptionActive = false;
                                        _UserAuthResponse.Subscription = _OSubscription;
                                    }
                                    using (HCoreContext _HCoreContext_ = new HCoreContext())
                                    {
                                        if (UserDetails.RoleKey == "merchantstoremanager")
                                        {
                                            string json = rolesrights;
                                            OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                            _UserAuthResponse.Features = items.StoreManager;
                                        }
                                        else if (UserDetails.RoleKey == "merchantadmin")
                                        {
                                            string json = rolesrights;
                                            OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                            _UserAuthResponse.Features = items.Admin;
                                        }
                                        else if (UserDetails.AccountTypeId == UserAccountType.Merchant)
                                        {
                                            string json = rolesrights;
                                            OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                            _UserAuthResponse.Features = items.Merchant;
                                        }
                                        else if (UserDetails.AccountTypeId == UserAccountType.MerchantStore)
                                        {
                                            string json = rolesrights;
                                            OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                            _UserAuthResponse.Features = items.Merchant;
                                            _OSubscription.IsSubscriptionActive = true;
                                        }
                                        else
                                        {
                                            _UserAuthResponse.Features = _HCoreContext_.HCSubscriptionFeature
                                                                                      .Where(x => x.SubscriptionId == AccountSubScriptionId
                                                                                      && x.StatusId == HelperStatus.Default.Active)
                                                                                      .Select(x => new OUserAccountAccess.OFeatures
                                                                                      {
                                                                                          Name = x.Name,
                                                                                          SystemName = x.Guid
                                                                                          //PermissionId = x.Id,
                                                                                          //Name = x.SystemFeature.Name,
                                                                                          //SystemName = x.SystemFeature.SystemName,
                                                                                          //IsAccessPinRequired = false,
                                                                                          //IsDefault = false,
                                                                                          //IsPasswordRequired = false,
                                                                                          //MinimumLimit = x.MinimumLimit,
                                                                                          //MaximumLimit = x.MaximumLimit,
                                                                                          //MinimumValue = x.MinimumAmount,
                                                                                          //MaximumValue = x.MaximumAmount,
                                                                                      }).ToList();
                                            //_UserAuthResponse.Permissions = _HCoreContext.HCSubscriptionFeatureItem
                                            //    .Where(x => x.SubscriptionFeature.Subscription.Id == AccountSubScriptionId
                                            //    && x.StatusId == HelperStatus.Default.Active)
                                            //    .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                            //    {
                                            //        PermissionId = x.Id,
                                            //        Name = x.SystemFeature.Name,
                                            //        SystemName = x.SystemFeature.SystemName,
                                            //        IsAccessPinRequired = false,
                                            //        IsDefault = false,
                                            //        IsPasswordRequired = false,
                                            //        MinimumLimit = x.MinimumLimit,
                                            //        MaximumLimit = x.MaximumLimit,
                                            //        MinimumValue = x.MinimumAmount,
                                            //        MaximumValue = x.MaximumAmount,
                                            //    }).ToList();
                                        }
                                        _UserAuthResponse.Permissions = _HCoreContext_.HCCoreCommon
                                                 .Where(x => x.TypeId == HelperType.Feature && x.HelperId == UserAccountType.Merchant
                                                 && x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                                 {
                                                     PermissionId = x.Id,
                                                     Name = x.Name,
                                                     SystemName = x.SystemName,
                                                     IsAccessPinRequired = false,
                                                     IsDefault = false,
                                                     IsPasswordRequired = false,
                                                     MinimumLimit = 1,
                                                     MaximumLimit = 100,
                                                     MinimumValue = 1,
                                                     MaximumValue = 100,

                                                 }).ToList();

                                        _HCoreContext_.Dispose();
                                    }
                                    if (AccountSubScriptionId > 0)
                                    {
                                        if (HostEnvironment != HostEnvironmentType.Live)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _UserAuthResponse.Permissions = _HCoreContext.HCCoreCommon
                                                      .Where(x => x.TypeId == HelperType.Feature
                                                      && x.StatusId == HelperStatus.Default.Active)
                                                      .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                                      {
                                                          PermissionId = x.Id,
                                                          Name = x.Name,
                                                          SystemName = x.SystemName,
                                                          IsAccessPinRequired = false,
                                                          IsDefault = false,
                                                          IsPasswordRequired = false,
                                                          MinimumLimit = 1,
                                                          MaximumLimit = 100,
                                                          MinimumValue = 1,
                                                          MaximumValue = 100,

                                                      }).ToList();
                                                _HCoreContext.Dispose();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        _Permisssions = new List<OUserAccountAccess.OUserAccountRolePermission>();
                                        _UserAuthResponse.Permissions = _Permisssions;
                                    }

                                    if (UserDetails.RoleKey == "merchantstoremanager")
                                    {
                                        List<OUserAccountAccess.Stores> StoreData = new List<OUserAccountAccess.Stores>();
                                        List<OUserAccountAccess.Stores> StoresWithStoreManagers = new List<OUserAccountAccess.Stores>();
                                        //Store Managers will default have one store assigned.
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            StoreData = _HCoreContext.HCUAccount.Where(x => (x.Id == UserDetails.UserAccountId
                                                                && x.SubOwnerId! == null)
                                                                && x.StatusId == HelperStatus.Default.Active
                                                                && x.SubscriptionId != null
                                                                && x.Subscription.StatusId == HelperStatus.Default.Active)
                                                                .Select(x => new OUserAccountAccess.Stores
                                                                {
                                                                    ReferenceId = x.SubOwnerId,
                                                                    ReferenceKey = x.SubOwner.Guid,
                                                                    DisplayName = x.SubOwner.DisplayName,
                                                                    Name = x.SubOwner.Name
                                                                })
                                                          .ToList();
                                            // As per change request by Suraj Sir that Stores will have drop down of Store Managers that ManagerId is saved in SubownerId.
                                            StoresWithStoreManagers = _HCoreContext.HCUAccount.Where(x => x.SubOwnerId == UserDetails.UserAccountId
                                                                 && x.StatusId == HelperStatus.Default.Active
                                                                 && x.SubscriptionId != null
                                                                 && x.Subscription.StatusId == HelperStatus.Default.Active)
                                                                .Select(x => new OUserAccountAccess.Stores
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name
                                                                })
                                                          .ToList();
                                            if (StoreData.Count() > 0 && StoresWithStoreManagers.Count() > 0)
                                            {
                                                _UserAuthResponse.Stores = Enumerable.Union(StoreData, StoresWithStoreManagers).ToList();
                                            }
                                            else if (StoreData.Count() > 0 && StoresWithStoreManagers.Count() == 0)
                                            {
                                                _UserAuthResponse.Stores = StoreData;
                                            }
                                            else if (StoreData.Count() == 0 && StoresWithStoreManagers.Count() > 0)
                                            {
                                                _UserAuthResponse.Stores = StoresWithStoreManagers;
                                            }
                                            _HCoreContext.Dispose();
                                        }

                                    }
                                    else
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var StoreData = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserDetails.UserAccountId
                                                                    && x.AccountTypeId == UserAccountType.MerchantStore
                                                                    && x.StatusId == HelperStatus.Default.Active
                                                                    && x.SubscriptionId != null
                                                                    && x.Subscription.StatusId == HelperStatus.Default.Active)
                                                                    .Select(x => new OUserAccountAccess.Stores
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name
                                                                    })
                                                              .ToList();
                                            _UserAuthResponse.Stores = StoreData;
                                            _HCoreContext.Dispose();
                                        }
                                    }
                                }
                            }
                            else if (UserDetails.AccountTypeId == UserAccountType.Admin)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var subfeaturesdata = _HCoreContext.HCCoreRoleFeature.Where(x => x.RoleId == UserDetails.RoleId)
                                                .Select(x => new OUserAccountAccess.ListFeatures
                                                {
                                                    Id = x.FeatureId,
                                                    Add = (bool)x.isAdd ? new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.FeatureId,
                                                        Name = x.Feature.Name,
                                                        SystemName = x.Feature.Add,
                                                        ActionType = "activitytype.add",
                                                        ParentId = x.Feature.ParentId,
                                                        HelperId = x.Feature.HelperId
                                                    } : null,
                                                    Edit = (bool)x.isUpdate ? new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.FeatureId,
                                                        Name = x.Feature.Name,
                                                        SystemName = x.Feature.Edit,
                                                        ActionType = "activitytype.update",
                                                        ParentId = x.Feature.ParentId,
                                                        HelperId = x.Feature.HelperId
                                                    } : null,
                                                    Delete = (bool)x.isDelete ? new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.FeatureId,
                                                        Name = x.Feature.Name,
                                                        SystemName = x.Feature.Delete,
                                                        ActionType = "activitytype.delete",
                                                        ParentId = x.Feature.ParentId,
                                                        HelperId = x.Feature.HelperId
                                                    } : null,
                                                    View = (bool)x.isView ? new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.FeatureId,
                                                        Name = x.Feature.Name,
                                                        SystemName = x.Feature.View,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.Feature.ParentId,
                                                        HelperId = x.Feature.HelperId
                                                    } : null,
                                                    ViewAll = (bool)x.isViewAll ? new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.FeatureId,
                                                        Name = x.Feature.Name,
                                                        SystemName = x.Feature.ViewAll,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.Feature.ParentId,
                                                        HelperId = x.Feature.HelperId
                                                    } : null

                                                }).ToList();
                                    List<OUserAccountAccess.OFeatures> subfeatures = new List<OUserAccountAccess.OFeatures>();
                                    foreach (var i in subfeaturesdata)
                                    {
                                        if (i.Add != null) { subfeatures.Add(i.Add); }
                                        if (i.View != null) { subfeatures.Add(i.View); }
                                        if (i.ViewAll != null) { subfeatures.Add(i.ViewAll); }
                                        if (i.Delete != null) { subfeatures.Add(i.Delete); }
                                        if (i.Edit != null) { subfeatures.Add(i.Edit); }
                                    }

                                    var subparentfeaturedata = _HCoreContext.HCCoreFeature.Where(x => subfeatures.Select(x => x.ParentId).Contains(x.Id))
                                                .Select(x => new OUserAccountAccess.ListFeatures
                                                {
                                                    Id = x.Id,
                                                    Add = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Add,
                                                        ActionType = "activitytype.add",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    Edit = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Edit,
                                                        ActionType = "activitytype.update",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    Delete = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Delete,
                                                        ActionType = "activitytype.delete",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    View = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.View,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    ViewAll = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.ViewAll,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    }

                                                }).ToList();

                                    List<OUserAccountAccess.OFeatures> subparentfeatures = new List<OUserAccountAccess.OFeatures>();
                                    foreach (var i in subparentfeaturedata)
                                    {
                                        if (!subfeaturesdata.Any(x => x.Id == i.Id))
                                        {
                                            if (i.Add != null) { subparentfeatures.Add(i.Add); }
                                            if (i.View != null) { subparentfeatures.Add(i.View); }
                                            if (i.ViewAll != null) { subparentfeatures.Add(i.ViewAll); }
                                            if (i.Delete != null) { subparentfeatures.Add(i.Delete); }
                                            if (i.Edit != null) { subparentfeatures.Add(i.Edit); }
                                        }

                                    }

                                    var parentfeaturedata = _HCoreContext.HCCoreFeature.Where(x => subparentfeatures.Select(x => x.ParentId).Contains(x.Id))
                                                .Select(x => new OUserAccountAccess.ListFeatures
                                                {
                                                    Id = x.Id,
                                                    Add = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Add,
                                                        ActionType = "activitytype.add",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    Edit = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Edit,
                                                        ActionType = "activitytype.update",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    Delete = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.Delete,
                                                        ActionType = "activitytype.delete",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    View = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.View,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    },
                                                    ViewAll = new OUserAccountAccess.OFeatures
                                                    {
                                                        Id = x.Id,
                                                        Name = x.Name,
                                                        SystemName = x.ViewAll,
                                                        ActionType = "activitytype.view",
                                                        ParentId = x.ParentId,
                                                        HelperId = x.HelperId
                                                    }

                                                }).ToList();

                                    List<OUserAccountAccess.OFeatures> parentfeatures = new List<OUserAccountAccess.OFeatures>();
                                    foreach (var i in parentfeaturedata)
                                    {
                                        if (!subparentfeaturedata.Any(x => x.Id == i.Id))
                                        {
                                            if (i.Add != null) { parentfeatures.Add(i.Add); }
                                            if (i.View != null) { parentfeatures.Add(i.View); }
                                            if (i.ViewAll != null) { parentfeatures.Add(i.ViewAll); }
                                            if (i.Delete != null) { parentfeatures.Add(i.Delete); }
                                            if (i.Edit != null) { parentfeatures.Add(i.Edit); }
                                        }

                                    }
                                    subfeatures = subfeatures.Count() > 0 ? subfeatures.Where(x => x.SystemName != null).ToList() : subfeatures;
                                    subparentfeatures = subparentfeatures.Count() > 0 ? subparentfeatures.Where(x => x.SystemName != null).ToList() : subparentfeatures;
                                    parentfeatures = parentfeatures.Count() > 0 ? parentfeatures.Where(x => x.SystemName != null).ToList() : parentfeatures;
                                    List<OUserAccountAccess.OFeatures> Union = subfeatures.Union(subparentfeatures).Distinct().ToList();
                                    _UserAuthResponse.Features = Enumerable.Union(parentfeatures, Union).Distinct().ToList();
                                    _HCoreContext.Dispose();
                                }
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (UserDetails.RoleKey == "superadmin" || UserDetails.RoleKey == "administrator" || UserDetails.RoleKey == "manager")
                                    {
                                        _UserAuthResponse.Features = _HCoreContext.HCCoreCommon
                                             .Where(x => x.TypeId == HelperType.FeatureOption
                                             && x.StatusId == HelperStatus.Default.Active)
                                             .Select(x => new OUserAccountAccess.OFeatures
                                             {
                                                 Name = x.Name,
                                                 SystemName = x.SystemName,
                                                 ActionType = x.Helper.SystemName
                                                 //PermissionId = x.Id,
                                                 //Name = x.SystemFeature.Name,
                                                 //SystemName = x.SystemFeature.SystemName,
                                                 //IsAccessPinRequired = false,
                                                 //IsDefault = false,
                                                 //IsPasswordRequired = false,
                                                 //MinimumLimit = x.MinimumLimit,
                                                 //MaximumLimit = x.MaximumLimit,
                                                 //MinimumValue = x.MinimumAmount,
                                                 //MaximumValue = x.MaximumAmount,

                                             }).ToList();
                                    }
                                    else
                                    {
                                        _UserAuthResponse.Features = _HCoreContext.HCCoreParameter
                                         .Where(x => x.Parent.Guid == UserDetails.RoleKey
                                         && x.TypeId == HelperType.RoleFeatures
                                         && x.StatusId == HelperStatus.Default.Active)
                                         .Select(x => new OUserAccountAccess.OFeatures
                                         {
                                             Name = x.Common.Name,
                                             SystemName = x.Common.SystemName,
                                             ActionType = x.Common.Helper.SystemName
                                             //PermissionId = x.Id,
                                             //Name = x.SystemFeature.Name,
                                             //SystemName = x.SystemFeature.SystemName,
                                             //IsAccessPinRequired = false,
                                             //IsDefault = false,
                                             //IsPasswordRequired = false,
                                             //MinimumLimit = x.MinimumLimit,
                                             //MaximumLimit = x.MaximumLimit,
                                             //MinimumValue = x.MinimumAmount,
                                             //MaximumValue = x.MaximumAmount,
                                         }).ToList();
                                    }

                                    _HCoreContext.Dispose();
                                }
                            }
                            #endregion


                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountSession = new HCUAccountSession();
                                _HCUAccountSession.Guid = UserAccountSessionKey;
                                _HCUAccountSession.AccountId = UserDetails.UserAccountId;
                                if (PlatformId != 0)
                                {
                                    _HCUAccountSession.PlatformId = PlatformId;
                                }
                                if (DeviceId != 0)
                                {
                                    _HCUAccountSession.DeviceId = DeviceId;
                                }
                                _HCUAccountSession.AppVersionId = _UserReference.AppVersionId;
                                _HCUAccountSession.LastActivityDate = HCoreHelper.GetGMTDate();
                                _HCUAccountSession.LoginDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountSession.IPAddress = _UserReference.RequestIpAddress;
                                _HCUAccountSession.Latitude = _UserReference.RequestLatitude;
                                _HCUAccountSession.Longitude = _UserReference.RequestLongitude;
                                _HCUAccountSession.StatusId = StatusActive;
                                _HCUAccountSession.NotificationUrl = NotificationUrl;
                                _HCoreContext.HCUAccountSession.Add(_HCUAccountSession);
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                                UserSessionId = _HCUAccountSession.Id;
                            }
                            #region Set User Access Key
                            UserAccessKey = HCoreEncrypt.EncryptHash(
                             _UserReference.OsId + "|" +
                             _UserReference.AppId + "|" +
                             _UserReference.AppVersionId + "|" +
                             UserDetails.AccountTypeId + "|" +
                             UserDetails.UserId + "|" +
                             UserDetails.UserAccountId + "|" +
                             DeviceId + "|" +
                             UserSessionId);
                            #endregion
                        }
                        #endregion
                        #region Dispose Conntection
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccountD = (from n in _HCoreContext.HCUAccount
                                                where n.Id == UserDetails.UserAccountId
                                                select n).FirstOrDefault();
                            if (UserAccountD != null)
                            {
                                UserAccountD.LastLoginDate = HCoreHelper.GetGMTDateTime();
                                UserAccountD.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        #endregion
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant || UserDetails.AccountTypeId == UserAccountType.MerchantSubAccount)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _UserAuthResponse.UserAccountTypes = _HCoreContext.HCUAccountParameter.Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                     && (x.TypeId == HelperType.ThankUCashDeals || x.TypeId == HelperType.ThankUCashLoyalty))
                                                                     .Select(x => new OUserAccountAccess.UserAccountTypes
                                                                     {
                                                                         TypeId = x.TypeId,
                                                                         TypeCode = x.Type.SystemName,
                                                                         Value = x.Value,
                                                                         SubTypeId = x.SubTypeId
                                                                     })
                                                                     .ToList();
                            }
                        }
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", UserDetails.UserAccountId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = false;
                            }
                        }
                        if (UserDetails.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", UserDetails.OwnerId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = false;
                            }
                        }
                        if (_UserAuthResponse.UserOwner != null && _UserAuthResponse.UserOwner.AccountTypeId == UserAccountType.Merchant)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _UserAuthResponse.UserOwner.UserAccountId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _UserAuthResponse.UserOwner.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _UserAuthResponse.UserOwner.IsTucPlusEnabled = false;
                            }
                        }

                        if (UserDetails.AccountTypeId == UserAccountType.Appuser)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _UserAuthResponse.Address = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == UserDetails.UserAccountId
                                                            && x.Account.Guid == UserDetails.UserAccountKey
                                                            && x.IsPrimaryAddress == 1)
                                                            .Select(x => new OUserAccountAccess.Address
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                DisplayName = x.DisplayName,
                                                                Instructions = x.Instructions,
                                                                Landmark = x.Landmark,
                                                                Latitude = x.Latitude,
                                                                Longitude = x.Longitude,
                                                                MapAddress = x.MapAddress,
                                                                IsPrimaryAddress = x.IsPrimaryAddress,

                                                                CountryId = x.CountryId,
                                                                CountryKey = x.Country.Guid,
                                                                CountryName = x.Country.Name,

                                                                StateId = x.StateId,
                                                                StateKey = x.State.Guid,
                                                                StateName = x.State.Name,

                                                                CityId = x.CityId,
                                                                CityKey = x.City.Guid,
                                                                CityName = x.City.Name,

                                                                CityAreaId = x.CityAreaId,
                                                                CityAreaKey = x.CityArea.Guid,
                                                                CityAreaName = x.CityArea.Name,

                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                ContactNumber = x.ContactNumber,
                                                                AlternateMobileNumber = x.AdditionalContactNumber,
                                                                AddressLine1 = x.AddressLine1,
                                                                AddressLine2 = x.AddressLine2,
                                                                LocationTypeId = x.LocationTypeId,
                                                                LocationTypeName = x.LocationType.Name,
                                                                ZipCode = x.ZipCode,

                                                                CreateDate = x.CreateDate,
                                                                CreatedById = x.CreatedById,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyById = x.ModifyById,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                            }).FirstOrDefault();

                                _HCoreContext.Dispose();
                            }
                        }
                        _UserAuthResponse.UserAccount = _ObjectUserAccount;
                        _UserAuthResponse.LoginTime = HCoreHelper.GetGMTDateTime();
                        _UserAuthResponse.AccessKey = UserAccessKey;
                        _UserAuthResponse.PublicKey = UserAccountSessionKey;
                        _UserAuthResponse.IsTempPin = UserDetails.IsTempPin;
                        if (UserDetails.LastLoginDate == null)
                        {
                            _UserAuthResponse.IsFirstLogin = true;
                        }
                        else
                        {
                            _UserAuthResponse.IsFirstLogin = false;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Success, _UserAuthResponse, "HC104");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Error, null, "HC115");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                #region Send Response
                return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Error, null, "HC500");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the user profile token.
        /// </summary>
        /// <param name="AccessKey">The access key.</param>
        /// <param name="PublicKey">The public key.</param>
        /// <param name="_UserReference">The user reference.</param>
        /// <returns>OResponse.</returns>
        private OResponse GetUserProfileToken(string AccessKey, string? PublicKey, OUserReference _UserReference)
        {
            // Public Key = UserSessionKey
            try
            {
                #region Declare 
                _UserAuthResponse = new OUserAccountAccess.OResponse();
                _ObjectUser = new OUserAccountAccess.OUser();
                _ObjectUserAccount = new OUserAccountAccess.OUserAccount();
                #endregion
                #region Build Details
                using (_HCoreContext = new HCoreContext())
                {
                    var SessionDetails = _HCoreContext.HCUAccountSession.Where(x => x.Guid == PublicKey && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                    if (SessionDetails == null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Error, null, "HC115");
                        #endregion
                    }
                    string[] AccessKeyDecrypt = HCoreEncrypt.DecryptHash(AccessKey).Split('|'); ;

                    long DeviceId = Convert.ToInt64(AccessKeyDecrypt[6]);
                    //_UserReference.OsId + "|" +
                    //         _UserReference.AppId + "|" +
                    //         _UserReference.AppVersionId + "|" +
                    //         UserDetails.AccountTypeId + "|" +
                    //         UserDetails.UserId + "|" +
                    //         UserDetails.UserAccountId + "|" +
                    //         DeviceId + "|" +
                    //         UserSessionId

                    var UserDetails = (from n in _HCoreContext.HCUAccount
                                       where n.Id == SessionDetails.AccountId
                                       select new
                                       {
                                           UserId = n.UserId,
                                           UserKey = n.Guid,

                                           UserAccountId = n.Id,
                                           UserAccountKey = n.Guid,

                                           AccountTypeId = n.AccountTypeId,

                                           OwnerId = n.OwnerId,
                                           SubOwnerId = n.SubOwnerId,
                                           UserName = n.User.Username,
                                           DisplayName = n.DisplayName,
                                           Name = n.Name,
                                           FirstName = n.FirstName,
                                           LastName = n.LastName,

                                           EmailAddress = n.EmailAddress,
                                           ContactNumber = n.ContactNumber,
                                           MobileNumber = n.MobileNumber,

                                           DateOfBirth = n.DateOfBirth,

                                           Gender = n.Gender.Name,
                                           GenderCode = n.Gender.SystemName,


                                           Address = n.Address,
                                           AddressLatitude = n.Latitude,
                                           AddressLongitude = n.Longitude,

                                           AccountTypeName = n.AccountType.Name,
                                           AccountTypeCode = n.AccountType.SystemName,

                                           RoleId = n.RoleId,
                                           RoleName = n.Role.Name,
                                           RoleKey = n.Role.Guid,

                                           ReferralCode = n.MobileNumber,
                                           ReferralUrl = n.ReferralUrl,

                                           AccountCode = n.AccountCode,
                                           AccessPin = n.AccessPin,

                                           IconUrl = n.IconStorage.Path,
                                           PosterUrl = n.PosterStorage.Path,

                                           EmailVerificationStatus = n.EmailVerificationStatus,
                                           ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                           CountryId = n.CountryId,
                                           CreateDate = n.CreateDate,
                                           ModifyDate = n.ModifyDate,
                                           LastLoginDate = n.LastLoginDate,
                                       }).FirstOrDefault();
                    if (UserDetails != null)
                    {


                        #region Set User Object
                        _ObjectUser = new OUserAccountAccess.OUser();
                        _ObjectUser.UserName = UserDetails.UserName;

                        _ObjectUser.Name = UserDetails.Name;
                        _ObjectUser.FirstName = UserDetails.FirstName;
                        _ObjectUser.LastName = UserDetails.LastName;

                        _ObjectUser.ContactNumber = UserDetails.ContactNumber;
                        _ObjectUser.MobileNumber = UserDetails.MobileNumber;
                        _ObjectUser.EmailAddress = UserDetails.EmailAddress;

                        _ObjectUser.Gender = UserDetails.Gender;
                        _ObjectUser.GenderCode = UserDetails.Gender;

                        _ObjectUser.DateOfBirth = UserDetails.DateOfBirth;

                        _ObjectUser.Address = UserDetails.Address;
                        _ObjectUser.AddressLatitude = UserDetails.AddressLatitude;
                        _ObjectUser.AddressLongitude = UserDetails.AddressLongitude;

                        _ObjectUser.EmailVerificationStatus = UserDetails.EmailVerificationStatus;
                        _ObjectUser.ContactNumberVerificationStatus = UserDetails.ContactNumberVerificationStatus;


                        _ObjectUser.LinkedAccounts = (from n in _HCoreContext.HCUAccount
                                                      where n.UserId == UserDetails.UserId
                                                      select new OUserAccountAccess.OUserLinkedAccounts
                                                      {
                                                          AccountId = n.Id,
                                                          AccountTypeId = n.AccountTypeId,
                                                          OwnerName = n.Owner.DisplayName,
                                                          DisplayName = n.DisplayName,
                                                          AccountKey = n.Guid,
                                                          AccountCode = n.AccountCode,
                                                          AccountType = n.AccountType.Name,
                                                          AccountTypeCode = n.AccountType.SystemName,
                                                          Status = n.StatusId,
                                                          IsActiveSession = 0,
                                                          CardNumber = n.Card.CardNumber,
                                                          CardTypeName = n.Card.CardType.Name,
                                                          CreateDate = n.CreateDate

                                                      }).ToList();
                        foreach (var LinkedAccount in _ObjectUser.LinkedAccounts)
                        {
                            if (LinkedAccount.AccountId == UserDetails.UserAccountId)
                            {
                                LinkedAccount.IsActiveSession = 1;
                            }
                            else
                            {
                                LinkedAccount.IsActiveSession = 0;
                            }

                            if (!string.IsNullOrEmpty(LinkedAccount.IconUrl))
                            {
                                LinkedAccount.IconUrl = _AppConfig.StorageUrl + LinkedAccount.IconUrl;
                            }
                            else
                            {
                                LinkedAccount.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _UserAuthResponse.User = _ObjectUser;
                        #endregion
                        #region Country
                        _UserAuthResponse.UserCountry = (from n in _HCoreContext.HCCoreCountry
                                                         where n.Id == UserDetails.CountryId
                                                         select new OUserAccountAccess.OUserAccountCountry
                                                         {
                                                             CountryId = n.Id,
                                                             CountryKey = n.Guid,
                                                             CountryName = n.Name,
                                                             CountryIso = n.Iso,
                                                             CountryIsd = n.Isd,
                                                             CurrencyName = n.CurrencyName,
                                                             CurrencyNotation = n.CurrencyNotation,
                                                             CurrencyCode = n.CurrencyHex,
                                                             CurrencySymbol = n.CurrencySymbol,
                                                         }).FirstOrDefault();
                        #endregion
                        #region Owner
                        if (UserDetails.OwnerId != null && UserDetails.OwnerId != 0)
                        {
                            _UserAuthResponse.UserOwner = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.OwnerId).Select(x => new OUserAccountAccess.OUserAccountOwner
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                AccountCode = x.AccountCode,
                                DisplayName = x.DisplayName,
                                Name = x.Name,
                                IconUrl = x.IconStorage.Path,
                                AccountTypeId = x.AccountTypeId,
                                UserAccountId = x.Id,
                                AccountTypeCode = x.AccountType.SystemName,
                            }).FirstOrDefault();
                            if (_UserAuthResponse.UserOwner != null)
                            {
                                if (!string.IsNullOrEmpty(_UserAuthResponse.UserOwner.IconUrl))
                                {
                                    _UserAuthResponse.UserOwner.IconUrl = _AppConfig.StorageUrl + _UserAuthResponse.UserOwner.IconUrl;
                                }
                                else
                                {
                                    _UserAuthResponse.UserOwner.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        if (UserDetails.SubOwnerId != null && UserDetails.SubOwnerId != 0)
                        {
                            _UserAuthResponse.UserSubOwner = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.SubOwnerId).Select(x => new OUserAccountAccess.OUserAccountOwner
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                AccountCode = x.AccountCode,
                                DisplayName = x.DisplayName,
                                Name = x.Name,
                                IconUrl = x.IconStorage.Path,
                                AccountTypeId = x.AccountTypeId,
                                UserAccountId = x.Id,
                                AccountTypeCode = x.AccountType.SystemName,
                            }).FirstOrDefault();
                            if (_UserAuthResponse.UserSubOwner != null)
                            {
                                if (!string.IsNullOrEmpty(_UserAuthResponse.UserSubOwner.IconUrl))
                                {
                                    _UserAuthResponse.UserSubOwner.IconUrl = _AppConfig.StorageUrl + _UserAuthResponse.UserSubOwner.IconUrl;
                                }
                                else
                                {
                                    _UserAuthResponse.UserSubOwner.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        #endregion
                        #region Set user account
                        _ObjectUserAccount = new OUserAccountAccess.OUserAccount();
                        _ObjectUserAccount.UserKey = UserDetails.UserKey;
                        _ObjectUserAccount.DisplayName = UserDetails.DisplayName;
                        _ObjectUserAccount.AccountId = UserDetails.UserAccountId;
                        _ObjectUserAccount.AccountKey = UserDetails.UserAccountKey;
                        _ObjectUserAccount.AccountType = UserDetails.AccountTypeName;
                        _ObjectUserAccount.AccountTypeCode = UserDetails.AccountTypeCode;

                        _ObjectUserAccount.RoleName = UserDetails.RoleName;
                        _ObjectUserAccount.RoleKey = UserDetails.RoleKey;

                        _ObjectUserAccount.ReferralCode = UserDetails.ReferralCode;
                        _ObjectUserAccount.ReferralUrl = UserDetails.ReferralUrl;
                        _ObjectUserAccount.AccountCode = UserDetails.AccountCode;
                        _ObjectUserAccount.CreateDate = UserDetails.CreateDate;
                        _ObjectUserAccount.ProgramDetails = _HCoreContext.TUCLProgram
                                                            .Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                    && x.ProgramType.SystemName == "programtype.grouployalty"
                                                                    && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new OUserAccountAccess.ProgramDetails
                                                            {
                                                                ProgramId = x.Id,
                                                                ProgramReferenceKey = x.Guid,
                                                                SystemName = x.ProgramType.SystemName
                                                            }).FirstOrDefault();

                        _ObjectUserAccount.CloseOpenDetails = _HCoreContext.TUCLProgram
                                                            .Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                    && (x.ProgramType.SystemName == "programtype.openloyalty"
                                                                    || x.ProgramType.SystemName == "programtype.closeloyalty")
                                                                    && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new OUserAccountAccess.ProgramDetails
                                                            {
                                                                ProgramId = x.Id,
                                                                ProgramReferenceKey = x.Guid,
                                                                SystemName = x.ProgramType.SystemName
                                                            }).FirstOrDefault();
                        if (_ObjectUserAccount.ProgramDetails != null)
                        {
                            _ObjectUserAccount.IsGroupLoyalty = _ObjectUserAccount.ProgramDetails.ProgramId > 0 ? true : false;
                        }
                        _ObjectUserAccount.IsOpenCloseLoyalty = false;
                        if (_ObjectUserAccount.CloseOpenDetails != null)
                        {
                            _ObjectUserAccount.IsOpenCloseLoyalty = (_ObjectUserAccount.CloseOpenDetails.ProgramId > 0
                                                                    && _ObjectUserAccount.CloseOpenDetails.SystemName == "programtype.closeloyalty") ? true : false;
                        }
                        else
                        {
                            var details = _HCoreContext.HCUAccountParameter
                                            .Where(x => x.AccountId == UserDetails.UserAccountId
                                                && x.TypeId == HelperType.ThankUCashLoyalty
                                                && (x.SubTypeId == HelperType.ThankUCashLoyalties.OpenLoyalty || x.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty)
                                                ).
                                                Select(x => new
                                                {
                                                    SubTypeId = x.SubTypeId
                                                }).FirstOrDefault();
                            if (details != null)
                            {
                                _ObjectUserAccount.IsOpenCloseLoyalty = (details.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty) ? true : false;
                            }
                        }
                        if (!string.IsNullOrEmpty(UserDetails.AccessPin))
                        {
                            _ObjectUserAccount.IsAccessPinSet = 1;
                        }
                        else
                        {
                            _ObjectUserAccount.IsAccessPinSet = 0;
                        }
                        if (!string.IsNullOrEmpty(UserDetails.IconUrl))
                        {
                            _ObjectUserAccount.IconUrl = _AppConfig.StorageUrl + UserDetails.IconUrl;
                        }
                        else
                        {
                            _ObjectUserAccount.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(UserDetails.PosterUrl))
                        {
                            _ObjectUserAccount.PosterUrl = _AppConfig.StorageUrl + UserDetails.PosterUrl;
                        }
                        else
                        {
                            _ObjectUserAccount.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #endregion
                        #region Save User Account Session
                        #region Subscription And Feature
                        _OSubscription = new OUserAccountAccess.OSubscription();
                        _OSubscription.IsSubscriptionActive = false;
                        _OSubscription.Amount = 0;
                        //_OSubscription.IsSubscriptionAvailable = false;
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant || UserDetails.AccountTypeId == UserAccountType.MerchantStore || UserDetails.AccountTypeId == UserAccountType.MerchantSubAccount)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long SubScriptionAccId = UserDetails.UserAccountId;
                                if (UserDetails.AccountTypeId != UserAccountType.Merchant)
                                {
                                    SubScriptionAccId = (long)UserDetails.OwnerId;
                                }
                                //  long AccountSubScriptionId = _HCoreContext.HCUAccountSubscription.Where(x =>
                                //x.AccountId == SubScriptionAccId &&
                                //x.StatusId == HelperStatus.Default.Active)
                                //    .Select(x => x.SubscriptionId)
                                //    .FirstOrDefault();
                                long AccountSubScriptionId = 0;
                                var MerchantSubscription = _HCoreContext.HCUAccountSubscription
                                                            .Where(x => x.AccountId == SubScriptionAccId)
                                                            .OrderByDescending(x => x.Id)
                                                            .Select(x => new
                                                            {
                                                                PlanId = x.SubscriptionId,
                                                                PlanKey = x.Subscription.Guid,
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                Title = x.Subscription.Name,
                                                                StartDate = x.StartDate,
                                                                EndDate = x.EndDate,
                                                                StatusId = x.StatusId,
                                                                Amount = x.SellingPrice,
                                                            })
                                                            .FirstOrDefault();
                                if (MerchantSubscription != null)
                                {
                                    if (MerchantSubscription.StatusId == HelperStatus.Default.Active)
                                    {
                                        AccountSubScriptionId = MerchantSubscription.PlanId;
                                        _OSubscription.PlanReferenceId = MerchantSubscription.PlanId;
                                        _OSubscription.PlanReferenceKey = MerchantSubscription.PlanKey;
                                        _OSubscription.SubscriptionId = MerchantSubscription.ReferenceId;
                                        _OSubscription.SubscriptionKey = MerchantSubscription.ReferenceKey;
                                        _OSubscription.Amount = (double)MerchantSubscription.Amount;
                                        _OSubscription.Title = MerchantSubscription.Title;
                                        _OSubscription.StartDate = MerchantSubscription.StartDate;
                                        _OSubscription.EndDate = MerchantSubscription.EndDate;
                                        int RemainingDays = (MerchantSubscription.EndDate - HCoreHelper.GetGMTDate()).Days;
                                        _OSubscription.DaysLeft = RemainingDays;

                                        if (_ObjectUserAccount.IsOpenCloseLoyalty)
                                        {
                                            var IsSubscription = _HCoreContext.HCUAccountSubscription
                                                                         .Where(x => x.AccountId == SubScriptionAccId
                                                                             && x.StatusId == HelperStatus.Default.Active
                                                                             && x.Subscription.Guid == "merchantstoresubscription"
                                                                             && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date)
                                                                         .ToList();
                                            if (IsSubscription.Count() == 0)
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                            else
                                            {
                                                _OSubscription.IsSubscriptionActive = true;
                                            }
                                        }
                                        else
                                        {
                                            if (RemainingDays < 0)
                                            {
                                                _OSubscription.IsSubscriptionActive = false;
                                            }
                                            else
                                            {
                                                _OSubscription.IsSubscriptionActive = true;
                                            }
                                        }
                                        //_OSubscription.IsSubscriptionAvailable = true;
                                        _UserAuthResponse.Subscription = _OSubscription;
                                    }
                                    else
                                    {
                                        _OSubscription.PlanReferenceId = MerchantSubscription.PlanId;
                                        _OSubscription.PlanReferenceKey = MerchantSubscription.PlanKey;
                                        _OSubscription.SubscriptionId = MerchantSubscription.ReferenceId;
                                        _OSubscription.SubscriptionKey = MerchantSubscription.ReferenceKey;
                                        _OSubscription.Amount = (double)MerchantSubscription.Amount;
                                        _OSubscription.Title = MerchantSubscription.Title;
                                        _OSubscription.StartDate = MerchantSubscription.StartDate;
                                        _OSubscription.EndDate = MerchantSubscription.EndDate;
                                        int RemainingDays = (MerchantSubscription.EndDate - HCoreHelper.GetGMTDate()).Days;
                                        _OSubscription.DaysLeft = RemainingDays;
                                        if (RemainingDays < 0)
                                        {
                                            _OSubscription.IsSubscriptionActive = false;
                                        }
                                        else
                                        {
                                            _OSubscription.IsSubscriptionActive = false;
                                        }
                                        //_OSubscription.IsSubscriptionAvailable = true;
                                        _UserAuthResponse.Subscription = _OSubscription;
                                    }

                                }
                                else
                                {
                                    _OSubscription.IsSubscriptionActive = false;
                                    _OSubscription.Amount = 0;
                                    //_OSubscription.IsSubscriptionAvailable = false;
                                    _UserAuthResponse.Subscription = _OSubscription;
                                }
                                //var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.AccountTypeId == UserAccountType.Merchant).FirstOrDefault();
                                //if (AccountSubScriptionId == 0)
                                //{
                                //    HCUAccountSubscription _HCUAccountSubscription = new HCUAccountSubscription();
                                //    _HCUAccountSubscription = new HCUAccountSubscription();
                                //    _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                                //    _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                                //    _HCUAccountSubscription.AccountId = SubScriptionAccId;
                                //    _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                                //    _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                                //    _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddYears(1);
                                //    _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.StartDate.AddYears(1);
                                //    _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                                //    _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                                //    _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                                //    _HCUAccountSubscription.CreatedById = 1;
                                //    _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                                //    _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                                //    _HCoreContext.SaveChanges();
                                //    AccountSubScriptionId = _HCUAccountSubscription.Id;
                                //}
                                using (HCoreContext _HCoreContext_ = new HCoreContext())
                                {
                                    if (UserDetails.RoleKey == "merchantstoremanager")
                                    {
                                        string json = rolesrights;
                                        OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                        _UserAuthResponse.Features = items.StoreManager;
                                    }
                                    else if (UserDetails.RoleKey == "merchantadmin")
                                    {
                                        string json = rolesrights;
                                        OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                        _UserAuthResponse.Features = items.Admin;
                                    }
                                    else if (UserDetails.AccountTypeId == UserAccountType.Merchant)
                                    {
                                        string json = rolesrights;
                                        OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                        _UserAuthResponse.Features = items.Merchant;
                                    }
                                    else if (UserDetails.AccountTypeId == UserAccountType.MerchantSubAccount)
                                    {
                                        string json = rolesrights;
                                        OUserAccountAccess.RolesRights items = JsonConvert.DeserializeObject<OUserAccountAccess.RolesRights>(json);
                                        _UserAuthResponse.Features = items.Merchant;
                                    }
                                    else
                                    {
                                        _UserAuthResponse.Features = _HCoreContext_.HCSubscriptionFeature
                                                                                  .Where(x => x.SubscriptionId == AccountSubScriptionId
                                                                                  && x.StatusId == HelperStatus.Default.Active)
                                                                                  .Select(x => new OUserAccountAccess.OFeatures
                                                                                  {
                                                                                      Name = x.Name,
                                                                                      SystemName = x.Guid
                                                                                      //PermissionId = x.Id,
                                                                                      //Name = x.SystemFeature.Name,
                                                                                      //SystemName = x.SystemFeature.SystemName,
                                                                                      //IsAccessPinRequired = false,
                                                                                      //IsDefault = false,
                                                                                      //IsPasswordRequired = false,
                                                                                      //MinimumLimit = x.MinimumLimit,
                                                                                      //MaximumLimit = x.MaximumLimit,
                                                                                      //MinimumValue = x.MinimumAmount,
                                                                                      //MaximumValue = x.MaximumAmount,
                                                                                  }).ToList();
                                        //_UserAuthResponse.Permissions = _HCoreContext.HCSubscriptionFeatureItem
                                        //    .Where(x => x.SubscriptionFeature.Subscription.Id == AccountSubScriptionId
                                        //    && x.StatusId == HelperStatus.Default.Active)
                                        //    .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                        //    {
                                        //        PermissionId = x.Id,
                                        //        Name = x.SystemFeature.Name,
                                        //        SystemName = x.SystemFeature.SystemName,
                                        //        IsAccessPinRequired = false,
                                        //        IsDefault = false,
                                        //        IsPasswordRequired = false,
                                        //        MinimumLimit = x.MinimumLimit,
                                        //        MaximumLimit = x.MaximumLimit,
                                        //        MinimumValue = x.MinimumAmount,
                                        //        MaximumValue = x.MaximumAmount,
                                        //    }).ToList();
                                    }
                                    _UserAuthResponse.Permissions = _HCoreContext_.HCCoreCommon
                                             .Where(x => x.TypeId == HelperType.Feature && x.HelperId == UserAccountType.Merchant
                                             && x.StatusId == HelperStatus.Default.Active)
                                             .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                             {
                                                 PermissionId = x.Id,
                                                 Name = x.Name,
                                                 SystemName = x.SystemName,
                                                 IsAccessPinRequired = false,
                                                 IsDefault = false,
                                                 IsPasswordRequired = false,
                                                 MinimumLimit = 1,
                                                 MaximumLimit = 100,
                                                 MinimumValue = 1,
                                                 MaximumValue = 100,

                                             }).ToList();
                                    _HCoreContext_.Dispose();

                                }
                                if (AccountSubScriptionId > 0)
                                {
                                    if (HostEnvironment != HostEnvironmentType.Live)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _UserAuthResponse.Permissions = _HCoreContext.HCCoreCommon
                                                  .Where(x => x.TypeId == HelperType.Feature
                                                  && x.StatusId == HelperStatus.Default.Active)
                                                  .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                                  {
                                                      PermissionId = x.Id,
                                                      Name = x.Name,
                                                      SystemName = x.SystemName,
                                                      IsAccessPinRequired = false,
                                                      IsDefault = false,
                                                      IsPasswordRequired = false,
                                                      MinimumLimit = 1,
                                                      MaximumLimit = 100,
                                                      MinimumValue = 1,
                                                      MaximumValue = 100,

                                                  }).ToList();
                                        }
                                    }
                                }
                                else
                                {
                                    _Permisssions = new List<OUserAccountAccess.OUserAccountRolePermission>();
                                    _UserAuthResponse.Permissions = _Permisssions;
                                }

                                if (UserDetails.RoleKey == "merchantstoremanager")
                                {
                                    List<OUserAccountAccess.Stores> StoreData = new List<OUserAccountAccess.Stores>();
                                    List<OUserAccountAccess.Stores> StoresWithStoreManagers = new List<OUserAccountAccess.Stores>();
                                    //Store Managers will default have one store assigned.
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        StoreData = _HCoreContext.HCUAccount.Where(x => (x.Id == UserDetails.UserAccountId
                                                            && x.SubOwnerId! == null)
                                                            && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new OUserAccountAccess.Stores
                                                            {
                                                                ReferenceId = x.SubOwnerId,
                                                                ReferenceKey = x.SubOwner.Guid,
                                                                DisplayName = x.SubOwner.DisplayName,
                                                                Name = x.SubOwner.Name
                                                            })
                                                      .ToList();
                                        // As per change request by Suraj Sir that Stores will have drop down of Store Managers that ManagerId is saved in SubownerId.
                                        StoresWithStoreManagers = _HCoreContext.HCUAccount.Where(x => x.SubOwnerId == UserDetails.UserAccountId
                                                             && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new OUserAccountAccess.Stores
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name
                                                            })
                                                      .ToList();

                                        if (StoreData.Count() > 0 && StoresWithStoreManagers.Count() > 0)
                                        {
                                            _UserAuthResponse.Stores = Enumerable.Union(StoreData, StoresWithStoreManagers).ToList();
                                        }
                                        else if (StoreData.Count() > 0 && StoresWithStoreManagers.Count() == 0)
                                        {
                                            _UserAuthResponse.Stores = StoreData;
                                        }
                                        else if (StoreData.Count() == 0 && StoresWithStoreManagers.Count() > 0)
                                        {
                                            _UserAuthResponse.Stores = StoresWithStoreManagers;
                                        }
                                        _HCoreContext.Dispose();
                                    }

                                }
                                else
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var StoreData = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserDetails.UserAccountId
                                                                && x.AccountTypeId == UserAccountType.MerchantStore
                                                                && x.StatusId == HelperStatus.Default.Active)
                                                                .Select(x => new OUserAccountAccess.Stores
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name
                                                                })
                                                          .ToList();
                                        _UserAuthResponse.Stores = StoreData;
                                        _HCoreContext.Dispose();
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                if (UserDetails.RoleKey == "superadmin" || UserDetails.RoleKey == "administrator" || UserDetails.RoleKey == "manager")
                                {
                                    _UserAuthResponse.Features = _HCoreContext.HCCoreCommon
                                         .Where(x => x.TypeId == HelperType.FeatureOption
                                         && x.StatusId == HelperStatus.Default.Active)
                                         .Select(x => new OUserAccountAccess.OFeatures
                                         {
                                             Name = x.Name,
                                             SystemName = x.SystemName,
                                             ActionType = x.Helper.SystemName
                                             //PermissionId = x.Id,
                                             //Name = x.SystemFeature.Name,
                                             //SystemName = x.SystemFeature.SystemName,
                                             //IsAccessPinRequired = false,
                                             //IsDefault = false,
                                             //IsPasswordRequired = false,
                                             //MinimumLimit = x.MinimumLimit,
                                             //MaximumLimit = x.MaximumLimit,
                                             //MinimumValue = x.MinimumAmount,
                                             //MaximumValue = x.MaximumAmount,

                                         }).ToList();
                                }
                                else
                                {
                                    _UserAuthResponse.Features = _HCoreContext.HCCoreParameter
                                     .Where(x => x.Parent.Guid == UserDetails.RoleKey
                                     && x.TypeId == HelperType.RoleFeatures
                                     && x.StatusId == HelperStatus.Default.Active)
                                     .Select(x => new OUserAccountAccess.OFeatures
                                     {
                                         Name = x.Common.Name,
                                         SystemName = x.Common.SystemName,
                                         ActionType = x.Common.Helper.SystemName
                                         //PermissionId = x.Id,
                                         //Name = x.SystemFeature.Name,
                                         //SystemName = x.SystemFeature.SystemName,
                                         //IsAccessPinRequired = false,
                                         //IsDefault = false,
                                         //IsPasswordRequired = false,
                                         //MinimumLimit = x.MinimumLimit,
                                         //MaximumLimit = x.MaximumLimit,
                                         //MinimumValue = x.MinimumAmount,
                                         //MaximumValue = x.MaximumAmount,
                                     }).ToList();
                                }

                                _HCoreContext.Dispose();
                            }
                        }
                        #endregion
                        #endregion
                        #region Dispose Conntection
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccountD = (from n in _HCoreContext.HCUAccount
                                                where n.Id == UserDetails.UserAccountId
                                                select n).FirstOrDefault();
                            if (UserAccountD != null)
                            {
                                UserAccountD.LastLoginDate = HCoreHelper.GetGMTDateTime();
                                UserAccountD.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        #endregion
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant || UserDetails.AccountTypeId == UserAccountType.MerchantSubAccount)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _ObjectUserAccount.UserAccountTypes = _HCoreContext.HCUAccountParameter.Where(x => (x.AccountId == UserDetails.UserAccountId || x.AccountId == UserDetails.OwnerId)
                                                                     && (x.TypeId == HelperType.ThankUCashDeals || x.TypeId == HelperType.ThankUCashLoyalty))
                                                                     .Select(x => new OUserAccountAccess.UserAccountTypes
                                                                     {
                                                                         TypeId = x.TypeId,
                                                                         TypeCode = x.Type.SystemName,
                                                                         Value = x.Value,
                                                                         SubTypeId = x.SubTypeId
                                                                     })
                                                                     .ToList();
                            }
                        }
                        if (UserDetails.AccountTypeId == UserAccountType.Merchant)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", UserDetails.UserAccountId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = false;
                            }
                        }
                        if (UserDetails.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", UserDetails.OwnerId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _ObjectUserAccount.IsTucPlusEnabled = false;
                            }
                        }
                        if (_UserAuthResponse.UserOwner != null && _UserAuthResponse.UserOwner.AccountTypeId == UserAccountType.Merchant)
                        {
                            long IsTucPlusEnabled = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _UserAuthResponse.UserOwner.UserAccountId));
                            if (IsTucPlusEnabled == 1)
                            {
                                _UserAuthResponse.UserOwner.IsTucPlusEnabled = true;
                            }
                            else
                            {
                                _UserAuthResponse.UserOwner.IsTucPlusEnabled = false;
                            }
                        }
                        _UserAuthResponse.UserAccount = _ObjectUserAccount;
                        _UserAuthResponse.LoginTime = HCoreHelper.GetGMTDateTime();
                        _UserAuthResponse.AccessKey = AccessKey;
                        _UserAuthResponse.PublicKey = PublicKey;
                        #region Send Response
                        return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Success, _UserAuthResponse, "HC104");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Error, null, "HC115");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                #region Send Response
                return HCoreHelper.SendResponse(_UserReference, ResponseStatus.Error, null, "HC500");
                #endregion
            }
        }

        #endregion
        #region Create Account And Login
        /// <summary>
        /// Description: Logins the application user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse LoginAppUser(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                }
                long OsId = _Request.UserReference.OsId;
                int GenderId = 0;
                //long CountryId = 0;
                #endregion
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response 
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.CountryIso))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC162");
                    #endregion
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get gender details if available
                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                        {
                            string GenderCode = _Request.GenderCode;
                            if (!_Request.GenderCode.Contains("gender."))
                            {
                                GenderCode = "gender." + _Request.GenderCode;
                            }

                            GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == GenderCode && x.ParentId == HelperType.Gender && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                            if (GenderId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC163");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get Country Id
                        var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Iso == _Request.CountryIso && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MSISDNLength = x.MobileNumberLength
                        }).FirstOrDefault();
                        if (CountryDetails == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164");
                            #endregion
                        }
                        #endregion
                        #region Format Username
                        string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.MobileNumber, CountryDetails.MSISDNLength);
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                        #endregion
                        #region  Process Registration
                        var UserDetails = (from n in _HCoreContext.HCUAccount
                                           where n.User.Username == UserName && n.AccountTypeId == UserAccountType.Appuser
                                           select new
                                           {
                                               UserAccountId = n.Id,
                                               UserAccountKey = n.Guid,
                                               UserKey = n.Guid,
                                               UserStatus = n.StatusId,
                                               UserAccountStatus = n.StatusId,
                                               AccessPin = n.AccessPin,
                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.UserStatus == StatusActive && UserDetails.UserAccountStatus == StatusActive)
                            {
                                string DeviceKey = HCoreHelper.GenerateGuid();
                                var DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.SerialNumber == _Request.SerialNumber).FirstOrDefault();
                                if (DeviceDetails != null)
                                {
                                    DeviceKey = DeviceDetails.Guid;
                                    if (_Request.NotificationUrl != null)
                                    {
                                        DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                                    }
                                    DeviceDetails.AccountId = UserDetails.UserAccountId;
                                    //DeviceDetails.AppId = _Request.UserReference.AppId;
                                    DeviceDetails.AppVersionId = _Request.UserReference.AppVersionId;
                                    DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    #region User Device Id
                                    _HCUAccountDevice = new HCUAccountDevice();
                                    _HCUAccountDevice.Guid = DeviceKey;
                                    _HCUAccountDevice.AccountId = UserDetails.UserAccountId;
                                    _HCUAccountDevice.SerialNumber = _Request.SerialNumber;
                                    //_HCUAccountDevice.OsId = _Request.UserReference.OsId;
                                    //_HCUAccountDevice.AppId = _Request.UserReference.AppId;
                                    _HCUAccountDevice.AppVersionId = _Request.UserReference.AppVersionId;
                                    _HCUAccountDevice.NotificationUrl = _Request.NotificationUrl;
                                    _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountDevice.StatusId = StatusActive;
                                    _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                }

                                using (_HCoreContext = new HCoreContext())
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && Account.FirstName != _Request.FirstName)
                                        {
                                            Account.FirstName = _Request.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.LastName) && Account.LastName != _Request.LastName)
                                        {
                                            Account.LastName = _Request.LastName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Account.EmailAddress != _Request.EmailAddress)
                                        {
                                            Account.EmailAddress = _Request.EmailAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                                        {
                                            Account.Name = _Request.FirstName + " " + _Request.LastName;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        if (GenderId != 0)
                                        {
                                            Account.GenderId = GenderId;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        Account.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                _Request.AccountId = UserDetails.UserAccountId;
                                _Request.UserType = "old";
                                var system = ActorSystem.Create("AppUserSignin");
                                var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                                greeter.Tell(_Request);
                                return GetUserProfile(UserDetails.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC168");
                                #endregion
                            }
                        }
                        else
                        {
                            string UserKey = HCoreHelper.GenerateGuid();
                            string UserAccountKey = HCoreHelper.GenerateGuid();
                            string UserDeviceKey = HCoreHelper.GenerateGuid();
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = UserName;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountAuth.StatusId = StatusActive;
                                _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                                _HCoreContext.SaveChanges();
                                #region Save User Account
                                _UserDevices = new List<HCUAccountDevice>();
                                _UserDevices.Add(new HCUAccountDevice
                                {
                                    Guid = UserDeviceKey,
                                    SerialNumber = _Request.SerialNumber,
                                    NotificationUrl = _Request.NotificationUrl,
                                    //OsId = _Request.UserReference.OsId,
                                    //AppId = _Request.UserReference.AppId,
                                    AppVersionId = _Request.UserReference.AppVersionId,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = StatusActive,
                                });
                                _Random = new Random();
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                                _HCUAccount.UserId = _HCUAccountAuth.Id;
                                // _HCUAccount.AccountCode = "20" + _Random.Next(0000000, 9999999).ToString() + _Random.Next(00, 99).ToString();
                                _HCUAccount.AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                if (!string.IsNullOrEmpty(_Request.ReferralCode))
                                {
                                    long ReferralUser = _HCoreContext.HCUAccount.Where(x =>
                                                                                          x.AccountTypeId == UserAccountType.Appuser &&
                                                                                          (
                                                                                              x.ContactNumber == _Request.ReferralCode
                                                                                          || x.MobileNumber == _Request.ReferralCode
                                                                                          || x.ReferralCode == _Request.ReferralCode)
                                                                                         ).Select(x => x.Id).FirstOrDefault();
                                    if (ReferralUser != 0)
                                    {
                                        _HCUAccount.OwnerId = ReferralUser;
                                    }
                                }
                                if (!string.IsNullOrEmpty(_Request.FirstName))
                                {
                                    _HCUAccount.DisplayName = _Request.FirstName;
                                }
                                else
                                {
                                    _HCUAccount.DisplayName = "User";
                                }
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);

                                _HCUAccount.Name = _Request.FirstName + _Request.LastName;
                                _HCUAccount.FirstName = _Request.FirstName;
                                _HCUAccount.LastName = _Request.LastName;
                                _HCUAccount.MobileNumber = MobileNumber;
                                _HCUAccount.ContactNumber = MobileNumber;
                                _HCUAccount.EmailAddress = _Request.EmailAddress;
                                if (_Request.UserReference.RequestLatitude != 0)
                                {
                                    _HCUAccount.Latitude = _Request.UserReference.RequestLatitude;
                                }
                                if (_Request.UserReference.RequestLongitude != 0)
                                {
                                    _HCUAccount.Longitude = _Request.UserReference.RequestLongitude;
                                }
                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_Request.DateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                }
                                if (CountryDetails != null)
                                {
                                    _HCUAccount.CountryId = CountryDetails.Id;
                                }
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatus = 1;
                                _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();

                                if (!string.IsNullOrEmpty(_Request.AccessPin))
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                }
                                else
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                                }
                                _HCUAccount.ReferralCode = MobileNumber;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.System;
                                _HCUAccount.StatusId = StatusActive;
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.ApplicationStatusId = CoreHelpers.UserApplicationStatus.AppInstalled;
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.HCUAccountDeviceAccount = _UserDevices;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                                //_ManageUserTransaction = new ManageUserTransaction();
                                //ORewardBalance BalanceDetails = _ManageUserTransaction.GetAppUserBalanceSummary(_HCUAccount.Id, _Request.UserReference);
                                //if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)
                                //{
                                //    if (!string.IsNullOrEmpty(_HCUAccount.AccessPin))
                                //    {
                                //        string DAccessPin = HCoreEncrypt.DecryptHash(_HCUAccount.AccessPin);
                                //        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, "Welcome to Thank U Cash: Your Bal: N" + BalanceDetails.Balance + ". Your redeeming PIN is: " + DAccessPin + ".");
                                //    }
                                //}
                                //#region Send Email 
                                //if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                //{
                                //    var _EmailParameters = new
                                //    {
                                //        Credit = BalanceDetails.Credit,
                                //        Debit = BalanceDetails.Debit,
                                //        Balance = BalanceDetails.Balance,
                                //        FirstName = _Request.FirstName,
                                //    };
                                //    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.AppUser_Welcome, _Request.FirstName, _Request.EmailAddress, _EmailParameters, _Request.UserReference);
                                //}
                                //#endregion
                                _Request.AccountId = _HCUAccount.Id;
                                _Request.UserType = "new";
                                var system = ActorSystem.Create("AppUserSignin");
                                var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                                greeter.Tell(_Request);
                                return GetUserProfile(UserAccountKey, UserDeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                                #endregion
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("LoginAppUser", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Logins the application user v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse LoginAppUserV2(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                }
                long OsId = _Request.UserReference.OsId;
                int GenderId = 0;
                #endregion
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                }
                else if (string.IsNullOrEmpty(_Request.CountryIso))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC162");
                }
                else
                {
                    #region Get gender details if available
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        _Request.GenderCode = _Request.GenderCode.ToLower();
                        if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Male;
                        }
                        if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Female;
                        }
                    }
                    #endregion
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Country Id
                        var Country = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.AddressComponent.CountryId && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                        if (Country == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Country details not found.");
                            #endregion
                        }
                        #endregion
                        #region Format Username
                        string MobileNumber = HCoreHelper.FormatMobileNumber(Country.Isd, _Request.MobileNumber, Country.MobileNumberLength);
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                        #endregion
                        #region  Process Registration
                        var UserDetails = (from n in _HCoreContext.HCUAccount
                                           where n.User.Username == UserName && n.AccountTypeId == UserAccountType.Appuser
                                           select new
                                           {
                                               UserAccountId = n.Id,
                                               UserAccountKey = n.Guid,
                                               UserKey = n.Guid,
                                               UserStatus = n.StatusId,
                                               UserAccountStatus = n.StatusId,
                                               AccessPin = n.AccessPin,
                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.UserStatus == StatusActive && UserDetails.UserAccountStatus == StatusActive)
                            {
                                string DeviceKey = HCoreHelper.GenerateGuid();
                                var DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.SerialNumber == _Request.SerialNumber && x.AccountId == UserDetails.UserAccountId).FirstOrDefault();
                                if (DeviceDetails != null)
                                {
                                    DeviceKey = DeviceDetails.Guid;
                                    if (_Request.NotificationUrl != null)
                                    {
                                        DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                                    }
                                    DeviceDetails.AccountId = UserDetails.UserAccountId;
                                    DeviceDetails.AppVersionId = _Request.UserReference.AppVersionId;
                                    DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    #region User Device Id
                                    _HCUAccountDevice = new HCUAccountDevice();
                                    _HCUAccountDevice.Guid = DeviceKey;
                                    _HCUAccountDevice.AccountId = UserDetails.UserAccountId;
                                    _HCUAccountDevice.SerialNumber = _Request.SerialNumber;
                                    _HCUAccountDevice.AppVersionId = _Request.UserReference.AppVersionId;
                                    _HCUAccountDevice.NotificationUrl = _Request.NotificationUrl;
                                    _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountDevice.StatusId = StatusActive;
                                    _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                }

                                using (_HCoreContext = new HCoreContext())
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        if (_Request.AddressComponent != null)
                                        {
                                            if (_Request.AddressComponent.CityId > 0)
                                            {
                                                Account.CityId = _Request.AddressComponent.CityId;
                                            }
                                            if (_Request.AddressComponent.StateId > 0)
                                            {
                                                Account.StateId = _Request.AddressComponent.StateId;
                                            }
                                            if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                                            {
                                                Account.Address = _Request.AddressComponent.AddressLine1;
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && Account.FirstName != _Request.FirstName)
                                        {
                                            Account.FirstName = _Request.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.LastName) && Account.LastName != _Request.LastName)
                                        {
                                            Account.LastName = _Request.LastName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Account.EmailAddress != _Request.EmailAddress)
                                        {
                                            Account.EmailAddress = _Request.EmailAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                                        {
                                            Account.Name = _Request.FirstName + " " + _Request.LastName;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        if (GenderId != 0)
                                        {
                                            Account.GenderId = GenderId;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.AccessPin))
                                        {
                                            Account.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                        }
                                        Account.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        Account.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                _Request.AccountId = UserDetails.UserAccountId;
                                _Request.UserType = "old";
                                var system = ActorSystem.Create("AppUserSignin");
                                var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                                greeter.Tell(_Request);
                                return GetUserProfile(UserDetails.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC168", "Your account is not avtive. Please contact support to activate account.");
                                #endregion
                            }
                        }
                        else
                        {
                            string UserKey = HCoreHelper.GenerateGuid();
                            string UserAccountKey = HCoreHelper.GenerateGuid();
                            string UserDeviceKey = HCoreHelper.GenerateGuid();
                            using (_HCoreContext = new HCoreContext())
                            {


                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = UserName;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountAuth.StatusId = StatusActive;
                                _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                                _HCoreContext.SaveChanges();
                                #region Save User Account
                                _UserDevices = new List<HCUAccountDevice>();
                                _UserDevices.Add(new HCUAccountDevice
                                {
                                    Guid = UserDeviceKey,
                                    SerialNumber = _Request.SerialNumber,
                                    NotificationUrl = _Request.NotificationUrl,
                                    AppVersionId = _Request.UserReference.AppVersionId,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = StatusActive,
                                });
                                _Random = new Random();
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                                _HCUAccount.UserId = _HCUAccountAuth.Id;
                                // _HCUAccount.AccountCode = "20" + _Random.Next(0000000, 9999999).ToString() + _Random.Next(00, 99).ToString();
                                _HCUAccount.AccountCode = HCoreHelper.GenerateAccountCode(11, "20");

                                if (!string.IsNullOrEmpty(_Request.ReferralCode))
                                {
                                    long ReferralUser = _HCoreContext.HCUAccount.Where(x =>
                                                                                          x.AccountTypeId == UserAccountType.Appuser &&
                                                                                          (
                                                                                              x.ContactNumber == _Request.ReferralCode
                                                                                          || x.MobileNumber == _Request.ReferralCode
                                                                                          || x.ReferralCode == _Request.ReferralCode)
                                                                                         ).Select(x => x.Id).FirstOrDefault();
                                    if (ReferralUser != 0)
                                    {
                                        _HCUAccount.OwnerId = ReferralUser;
                                    }
                                }
                                if (!string.IsNullOrEmpty(_Request.FirstName))
                                {
                                    _HCUAccount.DisplayName = _Request.FirstName;
                                }
                                else
                                {
                                    _HCUAccount.DisplayName = "User";
                                }
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.Name = _Request.FirstName + _Request.LastName;
                                _HCUAccount.FirstName = _Request.FirstName;
                                _HCUAccount.LastName = _Request.LastName;
                                _HCUAccount.MobileNumber = MobileNumber;
                                _HCUAccount.ContactNumber = MobileNumber;
                                _HCUAccount.EmailAddress = _Request.EmailAddress;
                                if (_Request.UserReference.RequestLatitude != 0)
                                {
                                    _HCUAccount.Latitude = _Request.UserReference.RequestLatitude;
                                }
                                if (_Request.UserReference.RequestLongitude != 0)
                                {
                                    _HCUAccount.Longitude = _Request.UserReference.RequestLongitude;
                                }
                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_Request.DateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                }
                                if (Country != null)
                                {
                                    _HCUAccount.CountryId = Country.Id;
                                }
                                if (_Request.AddressComponent != null)
                                {
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCUAccount.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCUAccount.StateId = _Request.AddressComponent.StateId;
                                    }
                                }

                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatus = 1;
                                _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                if (!string.IsNullOrEmpty(_Request.AccessPin))
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                }
                                else
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                                }
                                _HCUAccount.ReferralCode = MobileNumber;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.App;
                                _HCUAccount.StatusId = StatusActive;
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.ApplicationStatusId = CoreHelpers.UserApplicationStatus.AppInstalled;
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.HCUAccountDeviceAccount = _UserDevices;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                                //_ManageUserTransaction = new ManageUserTransaction();
                                //ORewardBalance BalanceDetails = _ManageUserTransaction.GetAppUserBalanceSummary(_HCUAccount.Id, _Request.UserReference);
                                //if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)
                                //{
                                //    if (!string.IsNullOrEmpty(_HCUAccount.AccessPin))
                                //    {
                                //        string DAccessPin = HCoreEncrypt.DecryptHash(_HCUAccount.AccessPin);
                                //        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, "Welcome to Thank U Cash: Your Bal: N" + BalanceDetails.Balance + ". Your redeeming PIN is: " + DAccessPin + ".");
                                //    }
                                //}
                                //#region Send Email 
                                //if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                //{
                                //    var _EmailParameters = new
                                //    {
                                //        Credit = BalanceDetails.Credit,
                                //        Debit = BalanceDetails.Debit,
                                //        Balance = BalanceDetails.Balance,
                                //        FirstName = _Request.FirstName,
                                //    };
                                //    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.AppUser_Welcome, _Request.FirstName, _Request.EmailAddress, _EmailParameters, _Request.UserReference);
                                //}
                                //#endregion
                                using (_HCoreContext = new HCoreContext())
                                {

                                    #region Save Address
                                    if (_Request.AddressComponent != null && _Request.AddressComponent.CityId > 0 && _Request.AddressComponent.StateId > 0)
                                    {
                                        HCCoreAddress _HCCoreAddress;
                                        _HCCoreAddress = new HCCoreAddress();
                                        _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                        _HCCoreAddress.AccountId = _HCUAccount.Id;
                                        //_HCCoreAddress.IsPrimaryAddress = 1;
                                        //_HCCoreAddress.DisplayName = "Other";
                                        _HCCoreAddress.LocationTypeId = 800;
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _HCCoreAddress.Name = _Request.Name;
                                        }
                                        else
                                        {
                                            _HCCoreAddress.Name = _Request.FirstName + " " + _Request.LastName;
                                        }

                                        if (!string.IsNullOrEmpty(_Request.AddressComponent.MobileNumber))
                                        {
                                            _HCCoreAddress.ContactNumber = _Request.AddressComponent.MobileNumber;
                                        }
                                        else
                                        {
                                            _HCCoreAddress.ContactNumber = MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                        {
                                            _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                        }
                                        else
                                        {
                                            _HCCoreAddress.EmailAddress = _Request.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                                        {
                                            _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                        }
                                        else
                                        {
                                            _HCCoreAddress.AddressLine1 = "";
                                        }
                                        _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                        _HCCoreAddress.ZipCode = _Request.AddressComponent.ZipCode;
                                        _HCCoreAddress.Landmark = _Request.AddressComponent.Landmark;
                                        if (_Request.AddressComponent.CityAreaId > 0)
                                        {
                                            _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                        }
                                        if (_Request.AddressComponent.CityId > 0)
                                        {
                                            _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                        }
                                        if (_Request.AddressComponent.StateId > 0)
                                        {
                                            _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                        }
                                        _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                        _HCCoreAddress.CountryId = Country.Id;
                                        _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                        if (_Request.UserReference.AccountId > 0)
                                        {
                                            _HCCoreAddress.CreatedById = _Request.UserReference.AccountId;
                                        }
                                        _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                        _HCoreContext.SaveChanges();
                                    }
                                }

                                #endregion

                                _Request.AccountId = _HCUAccount.Id;
                                _Request.UserType = "new";
                                var system = ActorSystem.Create("AppUserSignin");
                                var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                                greeter.Tell(_Request);
                                return GetUserProfile(UserAccountKey, UserDeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                                #endregion
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("LoginAppUser", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Logins the application user with pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse LoginAppUserWithPin(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                }
                long OsId = _Request.UserReference.OsId;
                int GenderId = 0;
                #endregion
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                }
                else if (string.IsNullOrEmpty(_Request.CountryIso))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC162");
                }
                else
                {
                    #region Get gender details if available
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        _Request.GenderCode = _Request.GenderCode.ToLower();
                        if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Male;
                        }
                        if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Female;
                        }
                    }
                    #endregion
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Country Id
                        var Country = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                        if (Country == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Country details not found.");
                            #endregion
                        }
                        #endregion
                        #region Format Username
                        string MobileNumber = HCoreHelper.FormatMobileNumber(Country.Isd, _Request.MobileNumber, Country.MobileNumberLength);
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                        #endregion
                        #region  Process Registration
                        var UserDetails = (from n in _HCoreContext.HCUAccount
                                           where n.User.Username == UserName && n.AccountTypeId == UserAccountType.Appuser
                                           select new
                                           {
                                               UserAccountId = n.Id,
                                               UserAccountKey = n.Guid,
                                               UserKey = n.Guid,
                                               UserStatus = n.StatusId,
                                               UserAccountStatus = n.StatusId,
                                               AccessPin = n.AccessPin,
                                               IsTempPin = n.IsTempPin,
                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.UserStatus == StatusActive && UserDetails.UserAccountStatus == StatusActive)
                            {
                                string AccessPin = HCoreEncrypt.DecryptHash(UserDetails.AccessPin);
                                if (_Request.AccessPin != AccessPin)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Invalid mobile number or pin");
                                }

                                string DeviceKey = HCoreHelper.GenerateGuid();
                                var DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.SerialNumber == _Request.SerialNumber && x.AccountId == UserDetails.UserAccountId).FirstOrDefault();
                                if (DeviceDetails != null)
                                {
                                    DeviceKey = DeviceDetails.Guid;
                                    if (_Request.NotificationUrl != null)
                                    {
                                        DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                                    }
                                    DeviceDetails.AccountId = UserDetails.UserAccountId;
                                    DeviceDetails.AppVersionId = _Request.UserReference.AppVersionId;
                                    DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    #region User Device Id
                                    _HCUAccountDevice = new HCUAccountDevice();
                                    _HCUAccountDevice.Guid = DeviceKey;
                                    _HCUAccountDevice.AccountId = UserDetails.UserAccountId;
                                    _HCUAccountDevice.SerialNumber = _Request.SerialNumber;
                                    _HCUAccountDevice.AppVersionId = _Request.UserReference.AppVersionId;
                                    _HCUAccountDevice.NotificationUrl = _Request.NotificationUrl;
                                    _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountDevice.StatusId = StatusActive;
                                    _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        if (_Request.AddressComponent != null)
                                        {
                                            if (_Request.AddressComponent.CityId > 0)
                                            {
                                                Account.CityId = _Request.AddressComponent.CityId;
                                            }
                                            if (_Request.AddressComponent.StateId > 0)
                                            {
                                                Account.StateId = _Request.AddressComponent.StateId;
                                            }
                                            if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                                            {
                                                Account.Address = _Request.AddressComponent.AddressLine1;
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && Account.FirstName != _Request.FirstName)
                                        {
                                            Account.FirstName = _Request.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.LastName) && Account.LastName != _Request.LastName)
                                        {
                                            Account.LastName = _Request.LastName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Account.EmailAddress != _Request.EmailAddress)
                                        {
                                            Account.EmailAddress = _Request.EmailAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                                        {
                                            Account.Name = _Request.FirstName + " " + _Request.LastName;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        if (GenderId != 0)
                                        {
                                            Account.GenderId = GenderId;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            Account.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.AccessPin))
                                        {
                                            Account.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                        }
                                        Account.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        Account.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                _Request.AccountId = UserDetails.UserAccountId;
                                _Request.UserType = "old";
                                var _AppUserSignin = ActorSystem.Create("AppUserSignin");
                                var _AppUserSigninCaller = _AppUserSignin.ActorOf<AppUserSignin>("AppUserSignin");
                                _AppUserSigninCaller.Tell(_AppUserSignin);
                                return GetUserProfile(UserDetails.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC168", "Your account is not avtive. Please contact support to activate account.");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Invalid mobile number or pin");
                            #endregion
                            //string UserKey = HCoreHelper.GenerateGuid();
                            //string UserAccountKey = HCoreHelper.GenerateGuid();
                            //string UserDeviceKey = HCoreHelper.GenerateGuid();
                            //using (_HCoreContext = new HCoreContext())
                            //{


                            //    #region Save User
                            //    _HCUAccountAuth = new HCUAccountAuth();
                            //    _HCUAccountAuth.Guid = UserKey;
                            //    _HCUAccountAuth.Username = UserName;
                            //    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                            //    _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                            //    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                            //    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            //    _HCUAccountAuth.StatusId = StatusActive;
                            //    _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                            //    _HCoreContext.SaveChanges();
                            //    #region Save User Account
                            //    _UserDevices = new List<HCUAccountDevice>();
                            //    _UserDevices.Add(new HCUAccountDevice
                            //    {
                            //        Guid = UserDeviceKey,
                            //        SerialNumber = _Request.SerialNumber,
                            //        NotificationUrl = _Request.NotificationUrl,
                            //        AppVersionId = _Request.UserReference.AppVersionId,
                            //        CreateDate = HCoreHelper.GetGMTDateTime(),
                            //        StatusId = StatusActive,
                            //    });
                            //    _Random = new Random();
                            //    _HCUAccount = new HCUAccount();
                            //    _HCUAccount.User = _HCUAccountAuth;
                            //    _HCUAccount.Guid = UserAccountKey;
                            //    _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                            //    _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                            //    _HCUAccount.UserId = _HCUAccountAuth.Id;
                            //    _HCUAccount.AccountCode = HCoreHelper.GenerateAccountCode(9, "20");

                            //    if (!string.IsNullOrEmpty(_Request.ReferralCode))
                            //    {
                            //        long ReferralUser = _HCoreContext.HCUAccount.Where(x =>
                            //                                                              x.AccountTypeId == UserAccountType.Appuser &&
                            //                                                              (
                            //                                                                  x.ContactNumber == _Request.ReferralCode
                            //                                                              || x.MobileNumber == _Request.ReferralCode
                            //                                                              || x.ReferralCode == _Request.ReferralCode)
                            //                                                             ).Select(x => x.Id).FirstOrDefault();
                            //        if (ReferralUser != 0)
                            //        {
                            //            _HCUAccount.OwnerId = ReferralUser;
                            //        }
                            //    }
                            //    if (!string.IsNullOrEmpty(_Request.FirstName))
                            //    {
                            //        _HCUAccount.DisplayName = _Request.FirstName;
                            //    }
                            //    else
                            //    {
                            //        _HCUAccount.DisplayName = "User";
                            //    }
                            //    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                            //    _HCUAccount.Name = _Request.FirstName + _Request.LastName;
                            //    _HCUAccount.FirstName = _Request.FirstName;
                            //    _HCUAccount.LastName = _Request.LastName;
                            //    _HCUAccount.MobileNumber = MobileNumber;
                            //    _HCUAccount.ContactNumber = MobileNumber;
                            //    _HCUAccount.EmailAddress = _Request.EmailAddress;

                            //    if (GenderId != 0)
                            //    {
                            //        _HCUAccount.GenderId = GenderId;
                            //    }
                            //    if (_Request.DateOfBirth != null)
                            //    {
                            //        _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                            //    }
                            //    if (Country != null)
                            //    {
                            //        _HCUAccount.CountryId = Country.Id;
                            //    }
                            //    if (_Request.UserReference.RequestLatitude != 0)
                            //    {
                            //        _HCUAccount.Latitude = _Request.UserReference.RequestLatitude;
                            //    }
                            //    if (_Request.UserReference.RequestLongitude != 0)
                            //    {
                            //        _HCUAccount.Longitude = _Request.UserReference.RequestLongitude;
                            //    }
                            //    if (_Request.AddressComponent != null)
                            //    {
                            //        if (_Request.AddressComponent.CityAreaId > 0)
                            //        {
                            //            _HCUAccount.CityAreaId = _Request.AddressComponent.CityAreaId;
                            //        }
                            //        if (_Request.AddressComponent.CityId > 0)
                            //        {
                            //            _HCUAccount.CityId = _Request.AddressComponent.CityId;
                            //        }
                            //        if (_Request.AddressComponent.StateId > 0)
                            //        {
                            //            _HCUAccount.StateId = _Request.AddressComponent.StateId;
                            //        }
                            //        if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                            //        {
                            //            _HCUAccount.Address = _Request.AddressComponent.AddressLine1;
                            //        }
                            //    }

                            //    _HCUAccount.EmailVerificationStatus = 0;
                            //    _HCUAccount.NumberVerificationStatus = 1;
                            //    _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            //    if (!string.IsNullOrEmpty(_Request.AccessPin))
                            //    {
                            //        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                            //    }
                            //    else
                            //    {
                            //        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                            //    }
                            //    _HCUAccount.ReferralCode = MobileNumber;
                            //    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            //    _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.App;
                            //    _HCUAccount.StatusId = StatusActive;
                            //    if (_Request.UserReference.AccountId != 0)
                            //    {
                            //        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            //    }
                            //    _HCUAccount.ApplicationStatusId = CoreHelpers.UserApplicationStatus.AppInstalled;
                            //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            //    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            //    _HCUAccount.HCUAccountDeviceAccount = _UserDevices;
                            //    _HCoreContext.HCUAccount.Add(_HCUAccount);
                            //    _HCoreContext.SaveChanges();
                            //    #endregion
                            //    _Request.AccountId = _HCUAccount.Id;
                            //    _Request.UserType = "new";
                            //    _Request.CountryId = Country.Id;
                            //    var system = ActorSystem.Create("AppUserSignin");
                            //    var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                            //    greeter.Tell(_Request);
                            //    return GetUserProfile(UserAccountKey, UserDeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                            //    #endregion
                            //}
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("LoginAppUser", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Applications the user register.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse AppUserRegister(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                }
                long OsId = _Request.UserReference.OsId;
                int GenderId = 0;
                #endregion
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161", "Name required");
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                }
                else if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161", "Email address required");
                }
                else if (!HCoreHelper.ValidateEmailAddress(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161", "Email address required");
                }
                else if (string.IsNullOrEmpty(_Request.CountryIso))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC162");
                }
                else
                {
                    #region Get gender details if available
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        _Request.GenderCode = _Request.GenderCode.ToLower();
                        if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Male;
                        }
                        if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                        {
                            GenderId = HCoreConstant.Helpers.Gender.Female;
                        }
                    }
                    #endregion
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Country Id
                        var Country = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                        if (Country == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Country details not found.");
                            #endregion
                        }
                        #endregion
                        #region Format Username
                        string MobileNumber = HCoreHelper.FormatMobileNumber(Country.Isd, _Request.MobileNumber, Country.MobileNumberLength);
                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(MobileNumber);
                            if (!MobileNumberValidate.IsNumberValid)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Invalid mobile number");

                            }
                        }
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                        #endregion
                        #region  Process Registration
                        var UserDetails = (from n in _HCoreContext.HCUAccount
                                           where n.User.Username == UserName && n.AccountTypeId == UserAccountType.Appuser
                                           select new
                                           {
                                               UserAccountId = n.Id,
                                               UserAccountKey = n.Guid,
                                               UserKey = n.Guid,
                                               UserStatus = n.StatusId,
                                               UserAccountStatus = n.StatusId,
                                               AccessPin = n.AccessPin,
                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Account already registered");

                            //if (UserDetails.UserStatus == StatusActive && UserDetails.UserAccountStatus == StatusActive)
                            //{
                            //    string DeviceKey = HCoreHelper.GenerateGuid();
                            //    var DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.SerialNumber == _Request.SerialNumber && x.AccountId == UserDetails.UserAccountId).FirstOrDefault();
                            //    if (DeviceDetails != null)
                            //    {
                            //        DeviceKey = DeviceDetails.Guid;
                            //        if (_Request.NotificationUrl != null)
                            //        {
                            //            DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                            //        }
                            //        DeviceDetails.AccountId = UserDetails.UserAccountId;
                            //        DeviceDetails.AppVersionId = _Request.UserReference.AppVersionId;
                            //        DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        _HCoreContext.SaveChanges();
                            //    }
                            //    else
                            //    {
                            //        #region User Device Id
                            //        _HCUAccountDevice = new HCUAccountDevice();
                            //        _HCUAccountDevice.Guid = DeviceKey;
                            //        _HCUAccountDevice.AccountId = UserDetails.UserAccountId;
                            //        _HCUAccountDevice.SerialNumber = _Request.SerialNumber;
                            //        _HCUAccountDevice.AppVersionId = _Request.UserReference.AppVersionId;
                            //        _HCUAccountDevice.NotificationUrl = _Request.NotificationUrl;
                            //        _HCUAccountDevice.CreateDate = HCoreHelper.GetGMTDateTime();
                            //        _HCUAccountDevice.StatusId = StatusActive;
                            //        _HCoreContext.HCUAccountDevice.Add(_HCUAccountDevice);
                            //        _HCoreContext.SaveChanges();
                            //        #endregion
                            //    }
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        var Account = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefault();
                            //        if (Account != null)
                            //        {
                            //            if (_Request.AddressComponent != null)
                            //            {
                            //                if (_Request.AddressComponent.CityId > 0)
                            //                {
                            //                    Account.CityId = _Request.AddressComponent.CityId;
                            //                }
                            //                if (_Request.AddressComponent.StateId > 0)
                            //                {
                            //                    Account.StateId = _Request.AddressComponent.StateId;
                            //                }
                            //                if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                            //                {
                            //                    Account.Address = _Request.AddressComponent.AddressLine1;
                            //                }
                            //            }
                            //            if (!string.IsNullOrEmpty(_Request.FirstName) && Account.FirstName != _Request.FirstName)
                            //            {
                            //                Account.FirstName = _Request.FirstName;
                            //            }
                            //            if (!string.IsNullOrEmpty(_Request.LastName) && Account.LastName != _Request.LastName)
                            //            {
                            //                Account.LastName = _Request.LastName;
                            //            }
                            //            if (!string.IsNullOrEmpty(_Request.EmailAddress) && Account.EmailAddress != _Request.EmailAddress)
                            //            {
                            //                Account.EmailAddress = _Request.EmailAddress;
                            //            }
                            //            if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                            //            {
                            //                Account.Name = _Request.FirstName + " " + _Request.LastName;
                            //            }
                            //            if (_Request.DateOfBirth != null)
                            //            {
                            //                Account.DateOfBirth = _Request.DateOfBirth;
                            //            }
                            //            if (GenderId != 0)
                            //            {
                            //                Account.GenderId = GenderId;
                            //            }
                            //            if (_Request.DateOfBirth != null)
                            //            {
                            //                Account.DateOfBirth = _Request.DateOfBirth;
                            //            }
                            //            if (!string.IsNullOrEmpty(_Request.AccessPin))
                            //            {
                            //                Account.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                            //            }
                            //            Account.LastActivityDate = HCoreHelper.GetGMTDateTime();
                            //            Account.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //            _HCoreContext.SaveChanges();
                            //        }
                            //    }
                            //    _Request.AccountId = UserDetails.UserAccountId;
                            //    _Request.UserType = "old";
                            //    var _AppUserSignin = ActorSystem.Create("AppUserSignin");
                            //    var _AppUserSigninCaller = _AppUserSignin.ActorOf<AppUserSignin>("AppUserSignin");
                            //    _AppUserSigninCaller.Tell(_AppUserSignin);
                            //    return GetUserProfile(UserDetails.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                            //}
                            //else
                            //{
                            //    _HCoreContext.Dispose();
                            //    #region Send Response
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC168", "Your account is not avtive. Please contact support to activate account.");
                            //    #endregion
                            //}
                        }
                        else
                        {

                            string UserKey = HCoreHelper.GenerateGuid();
                            string UserAccountKey = HCoreHelper.GenerateGuid();
                            string UserDeviceKey = HCoreHelper.GenerateGuid();
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = UserName;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber());
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountAuth.StatusId = StatusActive;
                                _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                                _HCoreContext.SaveChanges();
                                #region Save User Account
                                _UserDevices = new List<HCUAccountDevice>();
                                _UserDevices.Add(new HCUAccountDevice
                                {
                                    Guid = UserDeviceKey,
                                    SerialNumber = _Request.SerialNumber,
                                    NotificationUrl = _Request.NotificationUrl,
                                    AppVersionId = _Request.UserReference.AppVersionId,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    StatusId = StatusActive,
                                });
                                _Random = new Random();
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                                _HCUAccount.UserId = _HCUAccountAuth.Id;
                                _HCUAccount.AccountCode = HCoreHelper.GenerateAccountCode(11, "20");

                                if (!string.IsNullOrEmpty(_Request.ReferralCode))
                                {
                                    long ReferralUser = _HCoreContext.HCUAccount.Where(x =>
                                                                                          x.AccountTypeId == UserAccountType.Appuser &&
                                                                                          (
                                                                                              x.ContactNumber == _Request.ReferralCode
                                                                                          || x.MobileNumber == _Request.ReferralCode
                                                                                          || x.ReferralCode == _Request.ReferralCode)
                                                                                         ).Select(x => x.Id).FirstOrDefault();
                                    if (ReferralUser != 0)
                                    {
                                        _HCUAccount.OwnerId = ReferralUser;
                                    }
                                }
                                string[] FullName = _Request.Name.Split(" ");
                                string FirstName = string.Empty, LastName = string.Empty;
                                if (FullName.Length == 0 || FullName.Length == 1)
                                {
                                    _Request.FirstName = _Request.Name;
                                }
                                else if (FullName.Length == 2)
                                {
                                    _Request.FirstName = FullName[0];
                                    _Request.LastName = FullName[1];
                                }
                                else if (FullName.Length == 3)
                                {
                                    _Request.FirstName = FullName[0];
                                    _Request.LastName = FullName[2];
                                }
                                if (!string.IsNullOrEmpty(_Request.FirstName))
                                {
                                    _HCUAccount.DisplayName = _Request.FirstName;
                                }
                                else
                                {
                                    _HCUAccount.DisplayName = "User";
                                }
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.Name = _Request.FirstName + _Request.LastName;
                                _HCUAccount.FirstName = _Request.FirstName;
                                _HCUAccount.LastName = _Request.LastName;
                                _HCUAccount.MobileNumber = MobileNumber;
                                _HCUAccount.ContactNumber = MobileNumber;
                                _HCUAccount.EmailAddress = _Request.EmailAddress;

                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_Request.DateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                }
                                if (Country != null)
                                {
                                    _HCUAccount.CountryId = Country.Id;
                                }
                                if (_Request.UserReference.RequestLatitude != 0)
                                {
                                    _HCUAccount.Latitude = _Request.UserReference.RequestLatitude;
                                }
                                if (_Request.UserReference.RequestLongitude != 0)
                                {
                                    _HCUAccount.Longitude = _Request.UserReference.RequestLongitude;
                                }
                                if (_Request.AddressComponent != null)
                                {
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCUAccount.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCUAccount.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCUAccount.StateId = _Request.AddressComponent.StateId;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                                    {
                                        _HCUAccount.Address = _Request.AddressComponent.AddressLine1;
                                    }
                                }

                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatus = 1;
                                _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                if (!string.IsNullOrEmpty(_Request.AccessPin))
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                }
                                else
                                {
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                                }
                                _HCUAccount.ReferralCode = MobileNumber;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.App;
                                _HCUAccount.StatusId = StatusActive;
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.ApplicationStatusId = CoreHelpers.UserApplicationStatus.AppInstalled;
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.HCUAccountDeviceAccount = _UserDevices;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                                _Request.AccountId = _HCUAccount.Id;
                                _Request.UserType = "new";
                                _Request.CountryId = Country.Id;
                                var system = ActorSystem.Create("AppUserSignin");
                                var greeter = system.ActorOf<AppUserSignin>("AppUserSignin");
                                greeter.Tell(_Request);
                                return GetUserProfile(UserAccountKey, UserDeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                                #endregion
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("LoginAppUser", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Updates the temporary pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateTempPin(OUserAccountAccess.PinUpdateRequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare               
                #endregion
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Country Id
                        var Country = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                        if (Country == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Country details not found.");
                            #endregion
                        }
                        #endregion
                        #region Format Username
                        string MobileNumber = HCoreHelper.FormatMobileNumber(Country.Isd, _Request.MobileNumber, Country.MobileNumberLength);
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                        #endregion
                        #region  Process Registration
                        var UserDetails = _HCoreContext.HCUAccount
                                           .Where(n => n.User.Username == UserName && n.AccountTypeId == UserAccountType.Appuser
                                           ).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            UserDetails.Name = _Request.Name;
                            UserDetails.EmailAddress = _Request.EmailAddress;
                            UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                            UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            UserDetails.ModifyById = _Request.UserReference.AccountId;
                            UserDetails.IsTempPin = false;
                            _HCoreContext.HCUAccount.Update(UserDetails);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1180", "Profile Updated Successfully.");
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Invalid mobile number or pin");
                            #endregion

                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateTempPin", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        #endregion
        #region User Login
        //OUserAccountAccess.ORequest _LoginRequest;
        //ManageUserTransaction _ManageUserTransaction;
        /// <summary>
        /// Description: Logins the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Login(OUserAccountAccess.ORequest _Request)
        {
            if (!string.IsNullOrEmpty(_Request.AccessKey) && !string.IsNullOrEmpty(_Request.PublicKey))
            {
                return GetUserProfileToken(_Request.AccessKey, _Request.PublicKey, _Request.UserReference);
            }
            #region Manage Exception
            try
            {
                if (!string.IsNullOrEmpty(_Request.UserName))
                {
                    _Request.UserName = _Request.UserName.Trim();
                }
                if (!string.IsNullOrEmpty(_Request.Password))
                {
                    _Request.Password = _Request.Password.Trim();
                }
                #region Set Device Serial Number
                _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC101");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Password) && string.IsNullOrEmpty(_Request.SecondaryPassword) && string.IsNullOrEmpty(_Request.AccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC102");
                    #endregion
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Account Type
                        long AccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.AccountType))
                        {
                            AccountTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.AccountType).Select(x => x.Id).FirstOrDefault();
                            if (AccountTypeId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC103");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get User Details
                        var Details = (from n in _HCoreContext.HCUAccountAuth
                                       where n.Username == _Request.UserName
                                       select new
                                       {
                                           UserId = n.Id,
                                           UserKey = n.Guid,


                                           Password = n.Password,
                                           SecondaryPassword = n.SecondaryPassword,
                                           SystemPassword = n.SystemPassword,

                                           Status = n.StatusId,


                                           CreateDate = n.CreateDate,
                                           ModifyDate = n.ModifyDate,
                                       }).FirstOrDefault();
                        if (Details != null)
                        {
                            string UserPassword = "x11321y38d88a";
                            string UserSecondaryPassword = "x11321y38d88a";
                            string AccountKey = "abc";
                            string SysAccountKey = "2134";

                            if (!string.IsNullOrEmpty(Details.Password))
                            {
                                UserPassword = HCoreEncrypt.DecryptHash(Details.Password);
                            }
                            if (!string.IsNullOrEmpty(Details.SecondaryPassword))
                            {
                                UserSecondaryPassword = HCoreEncrypt.DecryptHash(Details.SecondaryPassword);
                            }
                            if (!string.IsNullOrEmpty(_Request.AccountKey))
                            {
                                AccountKey = _Request.AccountKey;
                                SysAccountKey = _HCoreContext.HCUAccount.Where(x => x.UserId == Details.UserId && x.Guid == _Request.AccountKey).Select(x => x.Guid).FirstOrDefault();
                            }
                            string UserSystemPassword = HCoreEncrypt.DecryptHash(Details.SystemPassword);
                            if (_Request.Password == UserPassword
                            || _Request.PasswordKey == UserSystemPassword
                            || _Request.Password == UserSystemPassword
                            || _Request.Password == UserSecondaryPassword
                            || _Request.SecondaryPassword == UserSecondaryPassword
                            || SysAccountKey == AccountKey
                            )
                            {
                                if (Details.Status == StatusActive)
                                {
                                    OUserAccountAccessDetails UserAccount = null;
                                    if (AccountTypeId != 0)
                                    {
                                        UserAccount = (from n in _HCoreContext.HCUAccount
                                                       where n.UserId == Details.UserId && n.AccountTypeId == AccountTypeId
                                                       select new OUserAccountAccessDetails
                                                       {
                                                           DisplayName = n.DisplayName,
                                                           UserKey = n.Guid,
                                                           UserAccountId = n.Id,
                                                           UserAccountKey = n.Guid,
                                                           AccountCode = n.AccountCode,
                                                           IconUrl = n.IconStorage.Path,
                                                           PosterUrl = n.PosterStorage.Path,
                                                           ReferralCode = n.ReferralCode,
                                                           ReferralUrl = n.ReferralUrl,
                                                           AccountTypeId = n.AccountTypeId,
                                                           AccountTypeCode = n.AccountType.SystemName,
                                                           AccountTypeName = n.AccountType.Name,
                                                           OwnerId = n.OwnerId,
                                                           Name = n.Name,
                                                           FirstName = n.FirstName,
                                                           LastName = n.LastName,

                                                           EmailAddress = n.EmailAddress,
                                                           ContactNumber = n.ContactNumber,

                                                           DateOfBirth = n.DateOfBirth,

                                                           Gender = n.Gender.Name,
                                                           GenderCode = n.Gender.SystemName,


                                                           Address = n.Address,
                                                           AddressLatitude = n.Latitude,
                                                           AddressLongitude = n.Longitude,

                                                           EmailVerificationStatus = n.EmailVerificationStatus,
                                                           ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                                           CountryId = n.CountryId,
                                                           Status = n.StatusId
                                                       }).FirstOrDefault();
                                    }
                                    else
                                    {
                                        if (_Request.Type == "web")
                                        {
                                            UserAccount = (from n in _HCoreContext.HCUAccount
                                                           where n.UserId == Details.UserId
                                                           && n.StatusId == StatusActive
                                                           && (n.AccountTypeId == UserAccountType.Merchant
                                                               || n.AccountTypeId == UserAccountType.MerchantStore
                                                               || n.AccountTypeId == UserAccountType.MerchantSubAccount
                                                               || n.AccountTypeId == UserAccountType.PgAccount
                                                               || n.AccountTypeId == UserAccountType.PosAccount
                                                               || n.AccountTypeId == UserAccountType.Acquirer
                                                              || n.AccountTypeId == UserAccountType.AcquirerSubAccount
                                                                || n.AccountTypeId == UserAccountType.Agent
                                                              )
                                                           orderby n.LastLoginDate descending
                                                           select new OUserAccountAccessDetails
                                                           {
                                                               AccessPin = n.AccessPin,
                                                               DisplayName = n.DisplayName,
                                                               UserAccountId = n.Id,
                                                               UserAccountKey = n.Guid,
                                                               UserKey = n.Guid,
                                                               IconUrl = n.IconStorage.Path,
                                                               PosterUrl = n.PosterStorage.Path,
                                                               AccountCode = n.AccountCode,
                                                               ReferralCode = n.ReferralCode,
                                                               ReferralUrl = n.ReferralUrl,
                                                               AccountTypeId = n.AccountTypeId,
                                                               AccountTypeCode = n.AccountType.SystemName,
                                                               AccountTypeName = n.AccountType.Name,
                                                               OwnerId = n.OwnerId,
                                                               Name = n.Name,
                                                               FirstName = n.FirstName,
                                                               LastName = n.LastName,

                                                               EmailAddress = n.EmailAddress,
                                                               ContactNumber = n.ContactNumber,

                                                               DateOfBirth = n.DateOfBirth,

                                                               Gender = n.Gender.Name,
                                                               GenderCode = n.Gender.SystemName,


                                                               Address = n.Address,
                                                               AddressLatitude = n.Latitude,
                                                               AddressLongitude = n.Longitude,

                                                               EmailVerificationStatus = n.EmailVerificationStatus,
                                                               ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                                               CountryId = n.CountryId,
                                                               Status = n.StatusId,

                                                           }).FirstOrDefault();
                                        }
                                        else
                                        {
                                            UserAccount = (from n in _HCoreContext.HCUAccount
                                                           where n.UserId == Details.UserId
                                                           orderby n.LastLoginDate descending
                                                           select new OUserAccountAccessDetails
                                                           {
                                                               AccessPin = n.AccessPin,
                                                               DisplayName = n.DisplayName,
                                                               UserAccountId = n.Id,
                                                               UserAccountKey = n.Guid,
                                                               UserKey = n.Guid,
                                                               IconUrl = n.IconStorage.Path,
                                                               PosterUrl = n.PosterStorage.Path,
                                                               AccountCode = n.AccountCode,
                                                               ReferralCode = n.ReferralCode,
                                                               ReferralUrl = n.ReferralUrl,
                                                               AccountTypeId = n.AccountTypeId,
                                                               AccountTypeCode = n.AccountType.SystemName,
                                                               AccountTypeName = n.AccountType.Name,
                                                               OwnerId = n.OwnerId,
                                                               Name = n.Name,
                                                               FirstName = n.FirstName,
                                                               LastName = n.LastName,

                                                               EmailAddress = n.EmailAddress,
                                                               ContactNumber = n.ContactNumber,

                                                               DateOfBirth = n.DateOfBirth,

                                                               Gender = n.Gender.Name,
                                                               GenderCode = n.Gender.SystemName,


                                                               Address = n.Address,
                                                               AddressLatitude = n.Latitude,
                                                               AddressLongitude = n.Longitude,

                                                               EmailVerificationStatus = n.EmailVerificationStatus,
                                                               ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                                               CountryId = n.CountryId,
                                                               Status = n.StatusId,
                                                           }).FirstOrDefault();
                                        }
                                    }
                                    if (UserAccount != null)
                                    {
                                        if (UserAccount.Status == StatusActive)
                                        {
                                            AccountTypeId = UserAccount.AccountTypeId;
                                            _FrameworkUserDevice = new FrameworkUserDevice();
                                            string DeviceKey = _FrameworkUserDevice.SaveUserDevice(UserAccount.UserAccountId, _Request.SerialNumber, _Request.NotificationUrl, _Request.Brand, _Request.Model, _Request.Height, _Request.Width, _Request.UserReference);
                                            #region Send Response
                                            return GetUserProfile(UserAccount.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                                            #endregion
                                        }
                                        else if (UserAccount.Status == StatusArchived)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC105");
                                            #endregion
                                        }
                                        else if (UserAccount.Status == StatusInactive)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC106");
                                            #endregion
                                        }
                                        else if (UserAccount.Status == StatusBlocked)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC107");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC108");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC109");
                                        #endregion
                                    }
                                }
                                else if (Details.Status == StatusInactive)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC110");
                                    #endregion
                                }
                                else if (Details.Status == StatusBlocked)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC111");
                                    #endregion
                                }
                                else if (Details.Status == StatusArchived)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC112");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC113");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC114");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC115");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("Login", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC116");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Class OAuthRequest.
        /// </summary>
        internal class OAuthRequest
        {
            public long AccountId { get; set; }
            public string? AuthPin { get; set; }
            public string? RequestKey { get; set; }
        }

        internal static List<OAuthRequest> _AuthRequests = new List<OAuthRequest>();
        /// <summary>
        /// Description: Admins the login request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse AdminLoginRequest(OUserAccountAccess.ORequest _Request)
        {
            if (!string.IsNullOrEmpty(_Request.AccessKey) && !string.IsNullOrEmpty(_Request.PublicKey))
            {
                return GetUserProfileToken(_Request.AccessKey, _Request.PublicKey, _Request.UserReference);
            }
            #region Manage Exception
            try
            {
                #region Set Device Serial Number
                _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC101");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Password) && string.IsNullOrEmpty(_Request.SecondaryPassword) && string.IsNullOrEmpty(_Request.AccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC102");
                    #endregion
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Account Type
                        long AccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.AccountType))
                        {
                            AccountTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.AccountType).Select(x => x.Id).FirstOrDefault();
                            if (AccountTypeId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC103");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get User Details
                        var UserAccount = (from n in _HCoreContext.HCUAccount
                                           where n.User.Username == _Request.UserName && (n.AccountTypeId == UserAccountType.Admin || n.AccountTypeId == UserAccountType.Controller)
                                           select new
                                           {
                                               UserId = n.User.Id,
                                               UserKey = n.User.Guid,

                                               ReferenceId = n.Id,
                                               ReferenceKey = n.Guid,

                                               Password = n.User.Password,
                                               SecondaryPassword = n.User.SecondaryPassword,
                                               SystemPassword = n.User.SystemPassword,

                                               DisplayName = n.DisplayName,

                                               Status = n.StatusId,
                                               EmailAddress = n.EmailAddress,

                                               CreateDate = n.CreateDate,
                                               ModifyDate = n.ModifyDate,
                                           }).FirstOrDefault();
                        if (UserAccount != null)
                        {
                            string UserPassword = "x11321y38d88a";
                            string UserSecondaryPassword = "x11321y38d88a";
                            string AccountKey = "abc";
                            string SysAccountKey = "2134";

                            if (!string.IsNullOrEmpty(UserAccount.Password))
                            {
                                UserPassword = HCoreEncrypt.DecryptHash(UserAccount.Password);
                            }
                            if (!string.IsNullOrEmpty(UserAccount.SecondaryPassword))
                            {
                                UserSecondaryPassword = HCoreEncrypt.DecryptHash(UserAccount.SecondaryPassword);
                            }
                            if (!string.IsNullOrEmpty(_Request.AccountKey))
                            {
                                AccountKey = _Request.AccountKey;
                                SysAccountKey = _HCoreContext.HCUAccount.Where(x => x.UserId == UserAccount.UserId && x.Guid == _Request.AccountKey).Select(x => x.Guid).FirstOrDefault();
                            }
                            string UserSystemPassword = HCoreEncrypt.DecryptHash(UserAccount.SystemPassword);
                            if (_Request.Password == UserPassword
                            || _Request.PasswordKey == UserSystemPassword
                            || _Request.Password == UserSystemPassword
                            || _Request.Password == UserSecondaryPassword
                            || _Request.SecondaryPassword == UserSecondaryPassword
                            || SysAccountKey == AccountKey
                            )
                            {
                                if (UserAccount.Status == StatusActive)
                                {
                                    if (UserAccount != null)
                                    {
                                        if (UserAccount.Status == StatusActive)
                                        {

                                            string Pin = HCoreHelper.GenerateRandomNumber(8);
                                            if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Local || HostEnvironment == HostEnvironmentType.Dev)
                                            {
                                                Pin = "123456";
                                            }
                                            if (HostEnvironment == HostEnvironmentType.Tech)
                                            {
                                                Pin = HCoreHelper.GenerateRandomNumber(8);
                                            }
                                            var Acc = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.ReferenceId).FirstOrDefault();
                                            Acc.ReferenceNumber = Pin;
                                            Acc.RequestKey = HCoreHelper.GenerateGuid();
                                            _HCoreContext.SaveChanges();
                                            _AuthRequests.Add(new OAuthRequest
                                            {
                                                AccountId = UserAccount.ReferenceId,
                                                AuthPin = Pin,
                                                RequestKey = Acc.RequestKey,
                                            });


                                            #region Send Email 
                                            if (!string.IsNullOrEmpty(UserAccount.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    Code = Pin,
                                                    DisplayName = UserAccount.DisplayName,
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.AuthCodeEmail_Admin, UserAccount.DisplayName, UserAccount.EmailAddress, _EmailParameters, _Request.UserReference);
                                            }
                                            var _Response = new
                                            {
                                                EmailAddress = UserAccount.EmailAddress,
                                                RequestKey = Acc.RequestKey
                                            };
                                            #endregion
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "EMS", "Enter login code received on your email address");
                                            //AccountTypeId = UserAccount.AccountTypeId;
                                            //_FrameworkUserDevice = new FrameworkUserDevice();
                                            //string DeviceKey = _FrameworkUserDevice.SaveUserDevice(UserAccount.UserAccountId, _Request.SerialNumber, _Request.NotificationUrl, _Request.Brand, _Request.Model, _Request.Height, _Request.Width, _Request.UserReference);
                                            //#region Send Response
                                            //return GetUserProfile(UserAccount.UserAccountKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference , _Request.PlatformCode);
                                            //#endregion
                                        }
                                        else if (UserAccount.Status == StatusArchived)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC105");
                                            #endregion
                                        }
                                        else if (UserAccount.Status == StatusInactive)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC106");
                                            #endregion
                                        }
                                        else if (UserAccount.Status == StatusBlocked)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC107");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC108");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC109");
                                    }
                                }
                                else if (UserAccount.Status == StatusInactive)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC110");
                                    #endregion
                                }
                                else if (UserAccount.Status == StatusBlocked)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC111");
                                    #endregion
                                }
                                else if (UserAccount.Status == StatusArchived)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC112");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC113");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC114");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC115");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("Login", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC116");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Admins the login confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse AdminLoginConfirm(OUserAccountAccess.ORequest _Request)
        {
            #region Manage Exception
            try
            {
                _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                #region Code Block
                if (string.IsNullOrEmpty(_Request.SerialNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC101", "Access key required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Code))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC101", "Code required to access your account");
                    #endregion
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #endregion
                        #region Get User Details
                        var UserAccount = (from n in _HCoreContext.HCUAccount
                                           where n.RequestKey == _Request.AccessKey && n.ReferenceNumber == _Request.Code && (n.AccountTypeId == UserAccountType.Admin || n.AccountTypeId == UserAccountType.Controller)
                                           select new
                                           {
                                               UserId = n.User.Id,
                                               UserKey = n.User.Guid,

                                               ReferenceId = n.Id,
                                               ReferenceKey = n.Guid,

                                               Password = n.User.Password,
                                               SecondaryPassword = n.User.SecondaryPassword,
                                               SystemPassword = n.User.SystemPassword,

                                               DisplayName = n.DisplayName,

                                               Status = n.StatusId,
                                               EmailAddress = n.EmailAddress,

                                               CreateDate = n.CreateDate,
                                               ModifyDate = n.ModifyDate,
                                           }).FirstOrDefault();
                        if (UserAccount != null)
                        {
                            if (UserAccount.Status == StatusActive)
                            {
                                if (UserAccount != null)
                                {
                                    if (UserAccount.Status == StatusActive)
                                    {
                                        _FrameworkUserDevice = new FrameworkUserDevice();
                                        string DeviceKey = _FrameworkUserDevice.SaveUserDevice(UserAccount.ReferenceId, _Request.SerialNumber, _Request.NotificationUrl, _Request.Brand, _Request.Model, _Request.Height, _Request.Width, _Request.UserReference);
                                        #region Send Response
                                        return GetUserProfile(UserAccount.ReferenceKey, DeviceKey, null, _Request.NotificationUrl, _Request.UserReference, _Request.PlatformCode);
                                        #endregion
                                    }
                                    else if (UserAccount.Status == StatusArchived)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC105");
                                        #endregion
                                    }
                                    else if (UserAccount.Status == StatusInactive)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC106");
                                        #endregion
                                    }
                                    else if (UserAccount.Status == StatusBlocked)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC107");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC108");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC109");
                                }
                            }
                            else if (UserAccount.Status == StatusInactive)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC110");
                                #endregion
                            }
                            else if (UserAccount.Status == StatusBlocked)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC111");
                                #endregion
                            }
                            else if (UserAccount.Status == StatusArchived)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC112");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC113");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC115", "Invalid code. Please try again");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("Login", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC116");
                #endregion
            }
            #endregion
        }

        #region User Logout
        /// <summary>
        /// Description: Logouts the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Logout(OUserAccountAccess.ORequest _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var SessionDetails = (from n in _HCoreContext.HCUAccountSession
                                          where n.Id == _Request.UserReference.AccountSessionId
                                          select n).FirstOrDefault();
                    if (SessionDetails != null)
                    {
                        SessionDetails.LogoutDate = HCoreHelper.GetGMTDateTime();
                        SessionDetails.StatusId = StatusBlocked;
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC131");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC132");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("Logout", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Create Account 
        /// <summary>
        /// Description: Registers the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Register(OUserAccountRegister _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                if (string.IsNullOrEmpty(_Request.AccountType))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC133");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC134");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Password))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC135");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC136");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.CountryIso))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC137");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccountOperationTypeCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC138");
                    #endregion
                }
                else
                {
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Request.SerialNumber = _Request.UserReference.DeviceSerialNumber;
                        long OsId = _Request.UserReference.OsId;

                        #region Get Status if available
                        if (!string.IsNullOrEmpty(_Request.StatusCode))
                        {
                            int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                            if (StatusId != 0)
                            {
                                _Request.StatusId = StatusId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC139");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get owner details if available
                        if (!string.IsNullOrEmpty(_Request.OwnerKey))
                        {
                            long UserOwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.OwnerKey && (x.StatusId == StatusActive || x.StatusId == StatusInactive)).Select(x => x.Id).FirstOrDefault();
                            if (UserOwnerId != 0)
                            {
                                _Request.OwnerId = UserOwnerId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC140");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get sub owner details if available
                        if (!string.IsNullOrEmpty(_Request.SubOwnerKey))
                        {
                            long UserOwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubOwnerKey && (x.StatusId == StatusActive || x.StatusId == StatusInactive)).Select(x => x.Id).FirstOrDefault();
                            if (UserOwnerId != 0)
                            {
                                _Request.SubOwnerId = UserOwnerId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC141");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get gender details if available
                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                        {
                            int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.GenderCode && x.ParentId == HelperType.Gender && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                            if (GenderId != 0)
                            {
                                _Request.GenderId = GenderId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC142");
                                #endregion
                            }
                        }
                        #endregion
                        #region Get Account Type
                        int AccountTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.AccountType && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                        if (AccountTypeId != 0)
                        {
                            _Request.AccountTypeId = AccountTypeId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC143");
                            #endregion
                        }
                        #endregion
                        #region Get Country Id
                        var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Iso == _Request.CountryIso && x.StatusId == StatusActive).Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            Key = x.Guid,
                            MSISDNLength = x.MobileNumberLength
                        }).FirstOrDefault();
                        if (CountryDetails != null)
                        {
                            _Request.CountryId = CountryDetails.Id;
                            _Request.CountryKey = CountryDetails.Key;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC144");
                            #endregion
                        }
                        #endregion
                        #region Get Registration Source Id
                        int RegistrationSourceId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.RegistrationSourceCode && x.ParentId == HelperType.RegistrationSource && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                        if (RegistrationSourceId != 0)
                        {
                            _Request.RegistrationSourceId = RegistrationSourceId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC145");
                            #endregion
                        }
                        #endregion
                        #region Get Account Operation type
                        int AccountOperationTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.AccountOperationTypeCode && x.ParentId == HelperType.AccountOperationType && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                        if (AccountOperationTypeId != 0)
                        {
                            _Request.AccountOperationTypeId = AccountOperationTypeId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC146");
                            #endregion
                        }
                        #endregion
                        #region Format Mobile Number 
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.MobileNumber, CountryDetails.MSISDNLength);
                        }
                        #endregion
                        #region  Process Registration
                        var UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                           where n.Username == _Request.UserName
                                           select new
                                           {
                                               UserId = n.Id,
                                               Status = n.StatusId,
                                               Password = n.Password
                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.Status == StatusActive)
                            {
                                #region Get User Existing Account Type
                                var UserAccount = (from n in _HCoreContext.HCUAccount
                                                   where n.UserId == UserDetails.UserId && n.AccountTypeId == _Request.AccountTypeId
                                                   select new
                                                   {
                                                       UserOwnerId = n.OwnerId,
                                                       UserAccountId = n.Id,
                                                   }).FirstOrDefault();
                                if (UserAccount != null)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC147");
                                    #endregion
                                }
                                else
                                {
                                    #region Create New Account Type with Existing User
                                    _Request.UserId = UserDetails.UserId;
                                    #region Save User Account
                                    long UserAccountId = SaveUserAccount(_Request);
                                    if (UserAccountId != 0)
                                    {
                                        _Request.UserAccountId = UserAccountId;
                                        #region Save Device
                                        if (!string.IsNullOrEmpty(_Request.SerialNumber))
                                        {
                                            _OUserDeviceSave = new OUserDeviceSave();
                                            _OUserDeviceSave.UserId = _Request.UserId;
                                            _OUserDeviceSave.UserAccountId = _Request.UserAccountId;
                                            _OUserDeviceSave.SerialNumber = _Request.SerialNumber;
                                            _OUserDeviceSave.OsVersion = _Request.OsVersion;
                                            _OUserDeviceSave.Brand = _Request.Brand;
                                            _OUserDeviceSave.Model = _Request.Model;
                                            _OUserDeviceSave.Width = _Request.Width;
                                            _OUserDeviceSave.Height = _Request.Height;
                                            _OUserDeviceSave.NotificationUrl = _Request.NotificationUrl;
                                            _OUserDeviceSave.UserReference = _Request.UserReference;
                                            _FrameworkUserDevice = new FrameworkUserDevice();
                                            OResponse _DeviceSaveResponse = _FrameworkUserDevice.SaveUserDevice(_OUserDeviceSave);
                                            if (_DeviceSaveResponse.Status == StatusSuccess)
                                            {
                                                _Request.UserAccountDeviceId = (long)_DeviceSaveResponse.Result;

                                            }
                                        }
                                        #endregion
                                        #region Save Roles
                                        long RoleSetupStatus = SaveUserAccountRole(_Request.OwnerId, _Request.SubOwnerId, _Request.UserAccountId, _Request.AccountTypeId, _Request.CountryKey, _Request.Owners, _Request.UserReference);
                                        #endregion
                                        if (RoleSetupStatus != 0)
                                        {
                                            if (_Request.Configurations != null)
                                            {
                                                if (_Request.Configurations.Count > 0)
                                                {
                                                    SaveUserAccountConfiguration(_Request.UserAccountId, _Request.Configurations, _Request.UserReference);
                                                }
                                            }
                                            if (_Request.AllowAccountKey == 1)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success,
                                                        _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId).Select(x => x.Guid).FirstOrDefault()
                                                                                     , "HC148");
                                                    #endregion
                                                }

                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC148");
                                                #endregion
                                            }

                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC149");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        SaveUserRollback(_Request);
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC150");
                                        #endregion
                                    }
                                    #endregion
                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC151");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Process Normal Registration
                            #region Save User
                            long UserId = SaveUser(_Request);
                            if (UserId != 0)
                            {
                                _Request.UserId = UserId;
                                #region Save User Account
                                long UserAccountId = SaveUserAccount(_Request);
                                if (UserAccountId != 0)
                                {
                                    _Request.UserAccountId = UserAccountId;
                                    #region Save Device
                                    if (!string.IsNullOrEmpty(_Request.SerialNumber))
                                    {
                                        _OUserDeviceSave = new OUserDeviceSave();
                                        _OUserDeviceSave.UserId = _Request.UserId;
                                        _OUserDeviceSave.UserAccountId = _Request.UserAccountId;
                                        _OUserDeviceSave.SerialNumber = _Request.SerialNumber;
                                        _OUserDeviceSave.OsVersion = _Request.OsVersion;
                                        _OUserDeviceSave.Brand = _Request.Brand;
                                        _OUserDeviceSave.Model = _Request.Model;
                                        _OUserDeviceSave.Width = _Request.Width;
                                        _OUserDeviceSave.Height = _Request.Height;
                                        _OUserDeviceSave.NotificationUrl = _Request.NotificationUrl;
                                        _OUserDeviceSave.UserReference = _Request.UserReference;
                                        _FrameworkUserDevice = new FrameworkUserDevice();
                                        OResponse _DeviceSaveResponse = _FrameworkUserDevice.SaveUserDevice(_OUserDeviceSave);
                                        if (_DeviceSaveResponse.Status == StatusSuccess)
                                        {
                                            _Request.UserAccountDeviceId = (long)_DeviceSaveResponse.Result;
                                            #region Save Roles
                                            long RoleSetupStatus = SaveUserAccountRole(_Request.OwnerId, _Request.SubOwnerId, _Request.UserAccountId, _Request.AccountTypeId, _Request.CountryKey, _Request.Owners, _Request.UserReference);
                                            #endregion
                                            if (RoleSetupStatus != 0)
                                            {
                                                if (_Request.Configurations != null)
                                                {
                                                    if (_Request.Configurations.Count > 0)
                                                    {
                                                        SaveUserAccountConfiguration(_Request.UserAccountId, _Request.Configurations, _Request.UserReference);
                                                    }
                                                }
                                                if (_Request.AllowAccountKey == 1)
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success,
                                                            _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId).Select(x => x.Guid).FirstOrDefault()
                                                                                         , "HC152");
                                                        #endregion
                                                    }

                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC153");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                SaveUserDeviceRollback(_Request);
                                                SaveUserAccountRollback(_Request);
                                                SaveUserRollback(_Request);
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC154");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            SaveUserAccountRollback(_Request);
                                            SaveUserRollback(_Request);
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC155");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Save Roles
                                        long RoleSetupStatus = SaveUserAccountRole(_Request.OwnerId, _Request.SubOwnerId, _Request.UserAccountId, _Request.AccountTypeId, _Request.CountryKey, _Request.Owners, _Request.UserReference);
                                        #endregion
                                        if (RoleSetupStatus != 0)
                                        {
                                            if (_Request.Configurations != null)
                                            {
                                                if (_Request.Configurations.Count > 0)
                                                {
                                                    SaveUserAccountConfiguration(_Request.UserAccountId, _Request.Configurations, _Request.UserReference);
                                                }
                                            }
                                            if (_Request.AllowAccountKey == 1 || _Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success,
                                                        _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId).Select(x => x.Guid).FirstOrDefault()
                                                                                     , "HC156");
                                                    #endregion
                                                }

                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC157");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            SaveUserDeviceRollback(_Request);
                                            SaveUserAccountRollback(_Request);
                                            SaveUserRollback(_Request);
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC158");
                                            #endregion
                                        }

                                    }

                                    #endregion
                                }
                                else
                                {
                                    SaveUserRollback(_Request);
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC159");
                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC160");
                                #endregion
                            }
                            #endregion
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("Register", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Creates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OCreateUser.</returns>
        internal OCreateUser CreateAccount(OCreateUser _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                #endregion
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Status if available
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId != 0)
                        {
                            _Request.StatusId = StatusId;
                        }
                        else
                        {
                            _Request.StatusId = StatusHelperId.Default_Active;
                        }
                    }
                    else
                    {
                        _Request.StatusId = StatusHelperId.Default_Active;
                    }
                    #endregion
                    #region Get gender details if available
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.GenderCode && x.ParentId == HelperType.Gender && x.StatusId == StatusActive).Select(x => x.Id).FirstOrDefault();
                        if (GenderId != 0)
                        {
                            _Request.GenderId = GenderId;
                        }
                        else
                        {
                            _Request.GenderId = 0;
                        }
                    }
                    #endregion
                    #region Format Mobile Number
                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.UserReference.CountryIsd).Select(x => new
                    {
                        Id = x.Id,
                        Isd = x.Isd,
                        Key = x.Guid,
                        MSISDNLength = x.MobileNumberLength
                    }).FirstOrDefault();
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, CountryDetails.MSISDNLength);
                    }
                    #endregion
                    #region  Process Registration
                    var UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                       where n.Username == _AppConfig.AppUserPrefix + _Request.MobileNumber
                                       select new
                                       {
                                           UserId = n.Id,
                                           UserKey = n.Guid,
                                           Status = n.StatusId,
                                           Password = n.Password
                                       }).FirstOrDefault();
                    if (UserDetails != null)
                    {
                        _Request.UserId = UserDetails.UserId;
                        _Request.UserKey = UserDetails.UserKey;
                        if (_Request.CardAccount != null)
                        {
                            var AccountDetails = _HCoreContext.HCUAccount
                                .Where(x => x.UserId == UserDetails.UserId
                                && x.AccountCode == _Request.CardAccount.AccountCode
                                //&& x.AccountTypeId == UserAccountType.Carduser
                                )
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    AccountCode = x.AccountCode
                                }).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                _Request.CardAccount.AccountId = AccountDetails.AccountId;
                                _Request.CardAccount.AccountKey = AccountDetails.AccountKey;
                                _Request.CardAccount.AccountCode = AccountDetails.AccountCode;
                            }
                            else
                            {
                                #region Offline Card Account
                                if (_Request.CardAccount != null)
                                {
                                    _HCUAccount = new HCUAccount();
                                    _HCUAccount.Guid = HCoreHelper.GenerateGuid(); ;
                                    _HCUAccount.UserId = UserDetails.UserId;
                                    if (_Request.UserReference.AccountOwnerId != null && _Request.UserReference.AccountOwnerId != 0)
                                    {
                                        _HCUAccount.OwnerId = _Request.UserReference.AccountOwnerId;
                                    }
                                    //_HCUAccount.AccountTypeId = UserAccountType.Carduser;
                                    _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Offline;
                                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                                    {
                                        _HCUAccount.DisplayName = _Request.DisplayName;
                                    }
                                    else
                                    {
                                        _HCUAccount.DisplayName = _Request.MobileNumber;
                                    }
                                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                    if (!string.IsNullOrEmpty(_Request.CardAccount.AccountCode))
                                    {
                                        _HCUAccount.AccountCode = _Request.CardAccount.AccountCode;
                                    }
                                    else
                                    {

                                        string RandomOnline = HCoreHelper.GenerateAccountCode(10);
                                        _HCUAccount.AccountCode = RandomOnline;
                                    }
                                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                                    _HCUAccount.CreateDate = CreateDate;
                                    _HCUAccount.StatusId = _Request.StatusId;
                                    if (_Request.UserReference.AccountId != 0)
                                    {
                                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                                    {
                                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.CardAccount.ReferralCode))
                                    {
                                        _HCUAccount.ReferralCode = _Request.CardAccount.ReferralCode;
                                    }
                                    if (_Request.CardAccount.CardId != 0)
                                    {
                                        _HCUAccount.CardId = _Request.CardAccount.CardId;
                                    }
                                    if (_Request.UserReference.AppVersionId != 0)
                                    {
                                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                    }
                                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                                    _HCoreContext.SaveChanges();

                                    _Request.CardAccount.AccountId = _HCUAccount.Id;
                                    _Request.CardAccount.AccountKey = _HCUAccount.Guid;
                                    _Request.CardAccount.AccountCode = _HCUAccount.AccountCode;
                                }
                                #endregion
                            }
                        }
                        if (_Request.AppAccount != null)
                        {
                            var AccountDetails = _HCoreContext.HCUAccount
                                .Where(x => x.UserId == UserDetails.UserId && x.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    AccountCode = x.AccountCode
                                }).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                _Request.AppAccount.AccountId = AccountDetails.AccountId;
                                _Request.AppAccount.AccountKey = AccountDetails.AccountKey;
                                _Request.AppAccount.AccountCode = AccountDetails.AccountCode;
                            }
                            else
                            {
                                #region Online Account
                                if (_Request.AppAccount != null)
                                {
                                    _HCUAccount = new HCUAccount();
                                    _HCUAccount.Guid = HCoreHelper.GenerateGuid(); ;
                                    _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                    _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                                    _HCUAccount.UserId = UserDetails.UserId;
                                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                                    {
                                        _HCUAccount.DisplayName = _Request.DisplayName;
                                    }
                                    else
                                    {
                                        _HCUAccount.DisplayName = _Request.MobileNumber;
                                    }
                                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                    if (string.IsNullOrEmpty(_Request.AppAccount.AccountCode))
                                    {
                                        _HCUAccount.AccountCode = _Request.AppAccount.AccountCode;
                                    }
                                    else
                                    {
                                        _Random = new Random();
                                        string RandomOnline = HCoreHelper.GenerateAccountCode(11, "20");
                                        _HCUAccount.AccountCode = RandomOnline;
                                    }
                                    _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.System;

                                    _HCUAccount.CreateDate = CreateDate;
                                    _HCUAccount.StatusId = _Request.StatusId;
                                    if (!string.IsNullOrEmpty(_Request.AppAccount.ReferralCode))
                                    {
                                        _HCUAccount.ReferralCode = _Request.AppAccount.ReferralCode;
                                    }
                                    if (_Request.AppAccount.CardId != 0)
                                    {
                                        _HCUAccount.CardId = _Request.AppAccount.CardId;
                                    }
                                    if (_Request.UserReference.AccountId != 0)
                                    {
                                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                                    {
                                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                    }
                                    if (_Request.UserReference.AppVersionId != 0)
                                    {
                                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                    }
                                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                                    _HCoreContext.SaveChanges();
                                    _Request.AppAccount.AccountId = _HCUAccount.Id;
                                    _Request.AppAccount.AccountKey = _HCUAccount.Guid;
                                    _Request.AppAccount.AccountCode = _HCUAccount.AccountCode;
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        string UserKey = HCoreHelper.GenerateGuid(); ;
                        string UserOnlineAccountKey = HCoreHelper.GenerateGuid();
                        string UserOfflineAccountKey = HCoreHelper.GenerateGuid();
                        _Random = new Random();
                        string AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                        List<HCUAccount> _UserAccounts = new List<HCUAccount>();
                        #region Online Account
                        if (_Request.AppAccount != null)
                        {
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = UserOnlineAccountKey;
                            _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                            _HCUAccount.AccountOperationTypeId = CoreHelpers.AccountOperationType.Online;
                            _HCUAccount.AccessPin = AccessPin;
                            if (!string.IsNullOrEmpty(_Request.DisplayName))
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName;
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.MobileNumber;
                            }


                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                _HCUAccount.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                _HCUAccount.ContactNumber = _Request.MobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                _HCUAccount.MobileNumber = _Request.MobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                _HCUAccount.EmailAddress = _Request.EmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.Address))
                            {
                                _HCUAccount.Address = _Request.Address;
                            }
                            if (_Request.AddressLatitude != 0)
                            {
                                _HCUAccount.Latitude = _Request.AddressLatitude;
                            }
                            if (_Request.AddressLongitude != 0)
                            {
                                _HCUAccount.Longitude = _Request.AddressLongitude;
                            }
                            if (_Request.GenderId != 0)
                            {
                                _HCUAccount.GenderId = _Request.GenderId;
                            }
                            _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                            _HCUAccount.EmailVerificationStatus = 0;
                            _HCUAccount.EmailVerificationStatusDate = CreateDate;
                            _HCUAccount.NumberVerificationStatus = 0;
                            _HCUAccount.NumberVerificationStatusDate = CreateDate;
                            if (!string.IsNullOrEmpty(_Request.AppAccount.AccountCode))
                            {
                                _HCUAccount.AccountCode = _Request.AppAccount.AccountCode;
                            }
                            else
                            {
                                _Random = new Random();
                                string RandomOnline = HCoreHelper.GenerateAccountCode(11, "20");
                                _HCUAccount.AccountCode = RandomOnline;
                            }
                            if (!string.IsNullOrEmpty(_Request.AppAccount.ReferralCode))
                            {
                                _HCUAccount.ReferralCode = _Request.AppAccount.ReferralCode;
                            }
                            if (_Request.AppAccount.CardId != 0)
                            {
                                _HCUAccount.CardId = _Request.AppAccount.CardId;
                            }
                            if (_Request.OwnerId != 0)
                            {
                                _HCUAccount.OwnerId = _Request.OwnerId;
                            }
                            if (_Request.AppAccount.OwnerId != 0)
                            {
                                _HCUAccount.OwnerId = _Request.AppAccount.OwnerId;
                            }
                            _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.System;
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.CreateDate = CreateDate;
                            _HCUAccount.StatusId = _Request.StatusId;
                            _UserAccounts.Add(_HCUAccount);
                        }
                        #endregion
                        #region Save User
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = UserKey;
                        _HCUAccountAuth.Username = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.StatusId = _Request.StatusId;
                        #region Set Created By
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                        #endregion
                        _HCUAccountAuth.HCUAccount = _UserAccounts;
                        _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                        _HCoreContext.SaveChanges();

                        #endregion
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                            {
                                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                {
                                    string DAccessPin = HCoreEncrypt.DecryptHash(AccessPin);
                                    string UMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber, CountryDetails.MSISDNLength);
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", UMobileNumber, "Welcome to Thank U Cash. Your redeeming PIN is: " + DAccessPin + ".  Download Android http://bit.do/tucan ios http://bit.do/tuci . ThankUCash", 0, null);
                                }
                            }

                            var UserInfo = _HCoreContext.HCUAccountAuth
                                 .Where(x => x.Guid == UserKey)
                                 .Select(x => new
                                 {
                                     UserId = x.Id,
                                     UserKey = x.Guid,
                                 }).FirstOrDefault();

                            if (UserInfo != null)
                            {
                                _Request.UserId = UserInfo.UserId;
                                _Request.UserKey = UserInfo.UserKey;
                            }
                            if (_Request.AppAccount != null)
                            {
                                var UserOnlineAccountInfo = _HCoreContext.HCUAccount
                                .Where(x => x.Guid == UserOnlineAccountKey && x.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    AccountCode = x.AccountCode
                                }).FirstOrDefault();
                                if (UserOnlineAccountInfo != null)
                                {
                                    _Request.AppAccount.AccountId = UserOnlineAccountInfo.AccountId;
                                    _Request.AppAccount.AccountKey = UserOnlineAccountInfo.AccountKey;
                                    _Request.AppAccount.AccountCode = UserOnlineAccountInfo.AccountCode;
                                }
                            }
                            if (_Request.CardAccount != null)
                            {
                                var UserCardAccountInfo = _HCoreContext.HCUAccount
                                  .Where(x => x.Guid == UserOfflineAccountKey)
                                  .Select(x => new
                                  {
                                      AccountId = x.Id,
                                      AccountKey = x.Guid,
                                      AccountCode = x.AccountCode
                                  }).FirstOrDefault();
                                if (UserCardAccountInfo != null)
                                {
                                    _Request.CardAccount.AccountId = UserCardAccountInfo.AccountId;
                                    _Request.CardAccount.AccountKey = UserCardAccountInfo.AccountKey;
                                    _Request.CardAccount.AccountCode = UserCardAccountInfo.AccountCode;
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion
                return _Request;
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("CreateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }
        #endregion
        #region Private Operations
        /// <summary>
        /// Description: Saves the user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Int64.</returns>
        private long SaveUser(OUserAccountRegister _Request)
        {
            #region Manage Exception
            try
            {
                string UserKey = HCoreHelper.GenerateGuid();
                using (_HCoreContext = new HCoreContext())
                {
                    #region Save User
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = UserKey;
                    _HCUAccountAuth.Username = _Request.UserName;
                    if (!string.IsNullOrEmpty(_Request.Password))
                    {
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    }
                    if (!string.IsNullOrEmpty(_Request.SecondaryPassword))
                    {
                        _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.SecondaryPassword);
                    }
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.StatusId != 0)
                    {
                        _HCUAccountAuth.StatusId = _Request.StatusId;
                    }
                    else
                    {
                        _HCUAccountAuth.StatusId = StatusActive;
                    }
                    #region Set Created By
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    #endregion
                    _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                    _HCoreContext.SaveChanges();
                    return _HCUAccountAuth.Id;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Int64.</returns>
        private long SaveUserAccount(OUserAccountRegister _Request)
        {
            #region Manage Exception
            try
            {
                _Random = new Random();
                string UserAccountKey = HCoreHelper.GenerateGuid();
                string AccountCode = null;
                if (_Request.AccountTypeId == UserAccountType.Admin)
                {
                    AccountCode = _Random.Next(1, 9).ToString() + _Random.Next(000000000, 999999999).ToString();
                }
                else if (_Request.AccountTypeId == UserAccountType.Merchant)
                {
                    AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                }
                else if (_Request.AccountTypeId == UserAccountType.MerchantStore)
                {
                    AccountCode = _Random.Next(100000000, 999999999).ToString();
                }
                else if (_Request.AccountTypeId == UserAccountType.MerchantCashier)
                {
                    AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                }
                else if (_Request.AccountTypeId == UserAccountType.Appuser)
                {
                    AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                }
                else if (_Request.AccountTypeId == UserAccountType.PgAccount)
                {
                    AccountCode = _Random.Next(10, 99).ToString() + _Random.Next(000000000, 999999999).ToString();
                }
                else if (_Request.AccountTypeId == UserAccountType.PosAccount)
                {
                    AccountCode = _Random.Next(10000, 99999).ToString() + _Random.Next(000000000, 999999999).ToString();
                }
                //else if (_Request.AccountTypeId == UserAccountType.TerminalAccount)
                //{
                //    AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                //}
                else
                {
                    AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                }

                using (_HCoreContext = new HCoreContext())
                {

                    #region Save User Account
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = UserAccountKey;
                    _HCUAccount.AccountTypeId = _Request.AccountTypeId;
                    _HCUAccount.AccountOperationTypeId = _Request.AccountOperationTypeId;
                    _HCUAccount.UserId = _Request.UserId;
                    if (_Request.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = _Request.OwnerId;
                    }
                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _HCUAccount.DisplayName = _Request.DisplayName;
                    }
                    else
                    {
                        _HCUAccount.DisplayName = "User";
                    }
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    if (!string.IsNullOrEmpty(_Request.AccessPin))
                    {
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                    }
                    else
                    {
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Random.Next(1111, 9999).ToString());
                    }
                    if (!string.IsNullOrEmpty(_Request.Name))
                    {
                        _HCUAccount.Name = _Request.Name;
                    }
                    if (!string.IsNullOrEmpty(_Request.FirstName))
                    {
                        _HCUAccount.FirstName = _Request.FirstName;
                    }
                    if (!string.IsNullOrEmpty(_Request.LastName))
                    {
                        _HCUAccount.LastName = _Request.LastName;
                    }
                    if (!string.IsNullOrEmpty(_Request.ContactNumber))
                    {
                        _HCUAccount.ContactNumber = _Request.ContactNumber;
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _HCUAccount.ContactNumber = _Request.MobileNumber;
                    }
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                    }
                    if (!string.IsNullOrEmpty(_Request.Address))
                    {
                        _HCUAccount.Address = _Request.Address;
                    }
                    if (_Request.AddressLatitude != 0)
                    {
                        _HCUAccount.Latitude = _Request.AddressLatitude;
                    }
                    if (_Request.AddressLongitude != 0)
                    {
                        _HCUAccount.Longitude = _Request.AddressLongitude;
                    }
                    if (_Request.GenderId != 0)
                    {
                        _HCUAccount.GenderId = _Request.GenderId;
                    }
                    if (_Request.DateOfBirth != null)
                    {
                        _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                    }
                    if (_Request.CountryId != 0)
                    {
                        _HCUAccount.CountryId = _Request.CountryId;
                    }
                    if (!string.IsNullOrEmpty(_Request.WebsiteUrl))
                    {
                        _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    }
                    _HCUAccount.EmailVerificationStatus = _Request.EmailVerificationStatus;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = _Request.NumberVerificationStatus;
                    _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();


                    _HCUAccount.AccountCode = AccountCode;
                    _HCUAccount.ReferralCode = _HCUAccount.AccountCode;
                    _HCUAccount.Description = _Request.Description;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.RegistrationSourceId != 0)
                    {
                        _HCUAccount.RegistrationSourceId = _Request.RegistrationSourceId;
                    }
                    else
                    {
                        _HCUAccount.RegistrationSourceId = CoreHelpers.RegistrationSource.System;
                    }
                    if (_Request.StatusId != 0)
                    {
                        _HCUAccount.StatusId = _Request.StatusId;
                    }
                    else
                    {
                        _HCUAccount.StatusId = StatusActive;
                    }
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    if (_Request.IsAppUser == true)
                    {
                        _HCUAccount.ApplicationStatusId = CoreHelpers.UserApplicationStatus.AppInstalled;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }

                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();

                    return _HCUAccount.Id;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user account role.
        /// </summary>
        /// <param name="OwnerId">The owner identifier.</param>
        /// <param name="SubOwnerId">The sub owner identifier.</param>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="AccountTypeId">The account type identifier.</param>
        /// <param name="CountryKey">The country key.</param>
        /// <param name="Owners">The owners.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Int64.</returns>
        private long SaveUserAccountRole(long OwnerId, long SubOwnerId, long UserAccountId, int AccountTypeId, string? CountryKey, List<string> Owners, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                #region Set Role By Account Type 
                using (_HCoreContext = new HCoreContext())
                {
                    //long AccountTypeDefaultRole = _HCoreContext.HCUAccountRole.Where(x => x.OwnerId == 1 && x.AccountTypeIdId == AccountTypeId).Select(x => x.Id).FirstOrDefault();
                    //if (AccountTypeDefaultRole != 0)
                    //{
                    #region Save Role Access
                    //_HCUAccountRoleAccess = new HCUAccountRoleAccess();
                    //_HCUAccountRoleAccess.Guid = HCoreHelper.GenerateGuid();;
                    //_HCUAccountRoleAccess.UserAccountRoleId = AccountTypeDefaultRole;
                    //_HCUAccountRoleAccess.UserAccountId = UserAccountId;
                    //_HCUAccountRoleAccess.StartDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountRoleAccess.CreateDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountRoleAccess.Status = StatusActive;
                    //#region Set Created By
                    //if (UserReference.AccountId != 0 && UserReference.AccountTypeId != 0)
                    //{
                    //    if (UserReference.AccountTypeId == 1)
                    //    {
                    //        _HCUAccountRoleAccess.CreatedByIdController = UserReference.AccountId;
                    //        _HCUAccountRoleAccess.CreateDateByController = HCoreHelper.GetGMTDateTime();
                    //    }
                    //    else
                    //    {
                    //        _HCUAccountRoleAccess.CreatedById = UserReference.AccountId;
                    //    }
                    //}
                    //else
                    //{
                    //    _HCUAccountRoleAccess.CreatedById = UserAccountId;
                    //}
                    //#endregion
                    //_HCoreContext.HCUAccountRoleAccess.Add(_HCUAccountRoleAccess);
                    //_HCoreContext.SaveChanges();
                    //long RoleAccessId = _HCUAccountRoleAccess.Id;
                    //#region Save User Account Configuration
                    //using (_HCoreContext = new HCoreContext())
                    //{
                    //    try
                    //    {
                    //        List<OUserRegConfiguration> AccountTypeConfigurations = _HCoreContext.HCUAccountTypeConfiguration
                    //                                              .Where(x => x.AccountTypeIdId == AccountTypeId)
                    //                                                                 .Select(x => new OUserRegConfiguration
                    //                                                                 {
                    //                                                                     ConfigurationId = x.ConfigurationId,
                    //                                                                     SystemName = x.Configuration.SystemName,
                    //                                                                 })
                    //                                              .ToList();
                    //        if (AccountTypeConfigurations.Count > 0)
                    //        {
                    //            foreach (var AccountTypeConfiguration in AccountTypeConfigurations)
                    //            {
                    //                _ManageConfiguration = new ManageConfiguration();
                    //                string ConfigurationValue = _ManageConfiguration.GetConfigurationValue(AccountTypeConfiguration.SystemName, CountryKey, UserReference);
                    //                if (!string.IsNullOrEmpty(ConfigurationValue))
                    //                {
                    //                    AccountTypeConfiguration.Value = ConfigurationValue;
                    //                }
                    //            }
                    //            using (_HCoreContext = new HCoreContext())
                    //            {
                    //                foreach (var AccountTypeConfiguration in AccountTypeConfigurations)
                    //                {
                    //                    if (!string.IsNullOrEmpty(AccountTypeConfiguration.Value))
                    //                    {
                    //                        _HCUAccountConfiguration = new HCUAccountConfiguration();
                    //                        _HCUAccountConfiguration.Guid = HCoreHelper.GenerateGuid();;
                    //                        _HCUAccountConfiguration.UserAccountId = UserAccountId;
                    //                        _HCUAccountConfiguration.ConfigurationId = (long)AccountTypeConfiguration.ConfigurationId;
                    //                        _HCUAccountConfiguration.Value = AccountTypeConfiguration.Value;
                    //                        _HCUAccountConfiguration.Status = StatusHelperId.Default_Active;
                    //                        _HCUAccountConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                    //                        if (UserReference.AccountTypeId == UserAccountType.Controller)
                    //                        {
                    //                            _HCUAccountConfiguration.CreatedByIdController = UserReference.AccountId;
                    //                            _HCUAccountConfiguration.CreateDateByController = HCoreHelper.GetGMTDateTime();
                    //                        }
                    //                        else
                    //                        {
                    //                            _HCUAccountConfiguration.CreatedById = UserReference.AccountId;
                    //                        }
                    //                        _HCoreContext.HCUAccountConfiguration.Add(_HCUAccountConfiguration);
                    //                    }
                    //                }
                    //                _HCoreContext.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //    catch (Exception _Exception)
                    //    {
                    //        //using (_HCoreContext = new HCoreContext())
                    //        //{
                    //        //    var RoleAccess = _HCoreContext.HCUAccountRoleAccess.Where(x => x.Id == RoleAccessId).FirstOrDefault();
                    //        //    if (RoleAccess != null)
                    //        //    {
                    //        //        _HCoreContext.HCUAccountRoleAccess.Remove(RoleAccess);
                    //        //        _HCoreContext.SaveChanges();
                    //        //    }
                    //        //}
                    //        #region  Log Exception
                    //        _ManageLog = new ManageLog();
                    //        _ManageLog.LogException("SaveUserConfiguration", _Exception,_Request.UserReference);
                    //        #endregion
                    //        #region Send Response
                    //        return 0;
                    //        #endregion
                    //    }
                    //}
                    //#endregion
                    #region Save User Owner 
                    if (OwnerId != 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            _HCUAccountOwner = new HCUAccountOwner();
                            _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid(); ;
                            _HCUAccountOwner.OwnerId = OwnerId;
                            _HCUAccountOwner.AccountId = UserAccountId;
                            _HCUAccountOwner.AccountTypeId = AccountTypeId;
                            _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (SubOwnerId != 0)
                            {
                                _HCUAccountOwner.CreatedById = SubOwnerId;
                            }
                            else
                            {
                                _HCUAccountOwner.CreatedById = OwnerId;
                            }
                            _HCUAccountOwner.StatusId = StatusHelperId.Default_Active;
                            _HCoreContext.HCUAccountOwner.Add(_HCUAccountOwner);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    if (SubOwnerId != 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            _HCUAccountOwner = new HCUAccountOwner();
                            _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid(); ;
                            _HCUAccountOwner.OwnerId = SubOwnerId;
                            _HCUAccountOwner.AccountId = UserAccountId;
                            _HCUAccountOwner.AccountTypeId = AccountTypeId;
                            _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountOwner.CreatedById = SubOwnerId;
                            _HCUAccountOwner.StatusId = StatusHelperId.Default_Active;
                            _HCoreContext.HCUAccountOwner.Add(_HCUAccountOwner);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    if (Owners != null)
                    {
                        if (Owners.Count > 0)
                        {

                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var Owner in Owners)
                                {
                                    long SubUserOwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == Owner).Select(x => x.Id).FirstOrDefault();
                                    if (SubUserOwnerId != 0)
                                    {
                                        _HCUAccountOwner = new HCUAccountOwner();
                                        _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid(); ;
                                        _HCUAccountOwner.OwnerId = SubUserOwnerId;
                                        _HCUAccountOwner.AccountId = UserAccountId;
                                        _HCUAccountOwner.AccountTypeId = AccountTypeId;
                                        _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountOwner.CreatedById = SubOwnerId;
                                        _HCUAccountOwner.StatusId = StatusHelperId.Default_Active;
                                        _HCoreContext.HCUAccountOwner.Add(_HCUAccountOwner);
                                    }
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    #endregion
                    return 1;
                    #endregion
                    //}
                    //else
                    //{
                    //    return 0;
                    //}
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserAccountRole", _Exception, UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user account configuration.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="Parameters">The parameters.</param>
        /// <param name="UserReference">The user reference.</param>
        private void SaveUserAccountConfiguration(long UserAccountId, List<OParameter> Parameters, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                #region Set Role By Account Type 
                using (_HCoreContext = new HCoreContext())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        foreach (var Parameter in Parameters)
                        {
                            long ParameterId = _HCoreContext.HCCoreCommon
                             .Where(x => x.SystemName == Parameter.Name)
                             .Select(x => x.Id)
                             .FirstOrDefault();
                            if (ParameterId != 0)
                            {
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.AccountId = UserAccountId;
                                _HCUAccountParameter.CommonId = ParameterId;
                                _HCUAccountParameter.Value = Parameter.Value.ToString();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (UserReference.AccountId != 0)
                                {
                                    _HCUAccountParameter.CreatedById = UserReference.AccountId;
                                }
                                _HCUAccountParameter.StatusId = StatusActive;
                                _HCUAccountParameter.RequestKey = UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            }
                        }
                        _HCoreContext.SaveChanges();
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserAccountConfiguration", _Exception, UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Private Operations Rollback
        /// <summary>
        /// Description: Saves the user rollback.
        /// </summary>
        /// <param name="_Request">The request.</param>
        private void SaveUserRollback(OUserAccountRegister _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = (from n in _HCoreContext.HCUAccountAuth where n.Id == _Request.UserId select n).FirstOrDefault();
                    if (UserDetails != null)
                    {
                        _HCoreContext.HCUAccountAuth.Remove(UserDetails);
                        _HCoreContext.SaveChanges();
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserRollback", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user account rollback.
        /// </summary>
        /// <param name="_Request">The request.</param>
        private void SaveUserAccountRollback(OUserAccountRegister _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = (from n in _HCoreContext.HCUAccountOwner where n.AccountId == _Request.UserAccountId select n).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.HCUAccountOwner.Remove(Details);
                        _HCoreContext.SaveChanges();
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = (from n in _HCoreContext.HCUAccount where n.Id == _Request.UserAccountId select n).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.HCUAccount.Remove(Details);
                        _HCoreContext.SaveChanges();
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserRollback", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user device rollback.
        /// </summary>
        /// <param name="_Request">The request.</param>
        private void SaveUserDeviceRollback(OUserAccountRegister _Request)
        {

            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = (from n in _HCoreContext.HCUAccountDevice where n.Id == _Request.UserAccountDeviceId select n).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.HCUAccountDevice.Remove(Details);
                        _HCoreContext.SaveChanges();
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveUserDeviceRollback", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion

    }
}
