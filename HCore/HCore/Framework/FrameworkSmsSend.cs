//==================================================================================
// FileName: FrameworkSmsSend.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to sms send
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using RestSharp;
using System.Net;

namespace HCore.Framework
{
    internal class FrameworkSmsSend
    {
        #region Private
        /// <summary>
        /// Sends the SMS.
        /// </summary>
        /// <param name="CountryIsd">The country isd.</param>
        /// <param name="MobileNumber">The mobile number.</param>
        /// <param name="Message">The message.</param>
        /// <returns>System.String.</returns>
        internal static string SendSMS(string CountryIsd, string? MobileNumber, string? Message)
        {
            try
            {
                #region Send SMS
                if (CountryIsd == "234")
                {
                    var client = new RestSharp.RestClient("https://3jwl1.api.infobip.com/sms/2/text/single");
                    var request = new RestSharp.RestRequest();
                    request.Method = RestSharp.Method.Post;
                    request.AddHeader("accept", "application/json");
                    request.AddHeader("content-type", "application/json");
                    request.AddHeader("authorization", "Basic VGhhbmtVQ2FzaDpTcGVlZGRlbW9AMTIz");
                    request.AddParameter("application/json", "{\"from\":\"ThankUCash\", \"to\":\"" + MobileNumber + "\",\"text\":\"" + Message + "\"}", RestSharp.ParameterType.RequestBody);
                    var response = client.Execute(request);
                    return "sent";
                }
                else if (CountryIsd == "234")
                {
                    string UserName = "ThankU";
                    string Password = "qo6tPZO7";
                    string FinalMobileNumber = MobileNumber.Replace("+", "");
                    //if (MobileNumber.StartsWith("0", StringComparison.Ordinal))
                    //{
                    //    string TCon = MobileNumber.Substring(1, (MobileNumber.Length - 1));
                    //    FinalMobileNumber = "234" + TCon;
                    //}

                    #region Send SMS Messagae
                    var smsapiurl = "http://ngn.rmlconnect.net/bulksms/bulksms?username=" + UserName
                        + "&password=" + Password
                        + "&type=0"
                        + "&dlr=1"
                        + "&destination=" + FinalMobileNumber
                        + "&source=ThankU"
                        + "&message=" + Message;

                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                    string responseString = _StreamReader.ReadToEnd();
                    if (responseString.StartsWith("OK", StringComparison.Ordinal))
                    {
                        _StreamReader.Close();
                        _HttpWebResponse.Close();
                        return responseString;
                    }
                    else
                    {
                        _StreamReader.Close();
                        _HttpWebResponse.Close();
                        return responseString;
                    }
                    #endregion
                    #region Send SMS Messagae
                    //var smsapiurl = "http://www.smslive247.com/http/index.aspx?"
                    //                  + "cmd=sendquickmsg"
                    //                  + "&owneremail=simeon@myads.ng"
                    //                  + "&subacct=MYADS_AIRTEL"
                    //                  + "&subacctpwd=myads123"
                    //                  + "&message=" + Message
                    //                  + "&sender=MyAds"
                    //                  + "&sendto=" + MobileNumber
                    //                  + "&msgtype=0";
                    //HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                    //HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    //System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                    //string responseString = _StreamReader.ReadToEnd();
                    //if (responseString.StartsWith("OK"))
                    //{
                    //    _StreamReader.Close();
                    //    _HttpWebResponse.Close(); 
                    //    return true;
                    //}
                    //else
                    //{
                    //    _StreamReader.Close();
                    //    _HttpWebResponse.Close();
                    //    return false;
                    //}
                    #endregion
                }
                else if (CountryIsd == "91")
                {
                    #region Send SMS Messagae
                    var smsapiurl = "http://bhashsms.com/api/sendmsg.php?"
                                      + "user=Myads"
                                      + "&pass=123"
                                      + "&sender=HCORE"
                                      + "&text=" + Message
                                      + "&phone=" + MobileNumber
                                      + "&stype=normal"
                                      + "&priority=ndnd";
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                    string responseString = _StreamReader.ReadToEnd();
                    return responseString;
                    #endregion
                }
                else
                {
                    return null;
                }
                #endregion
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion
    }
}

