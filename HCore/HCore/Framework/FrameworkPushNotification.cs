//==================================================================================
// FileName: FrameworkPushNotification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to notifications
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using HCore.Object;
using Newtonsoft.Json;

namespace HCore.Framework
{

    public class ONotification
    {
        public string? title { get; set; }
        public string? body { get; set; }
    }
    public class ONotificationDevicePushBody
    {

        public string? Task { get; set; }
        public string? Subject { get; set; }
        public string? Message { get; set; }
        public string? NavigateTo { get; set; }
        public string? ReferenceKey { get; set; }
        public string? ConfirmText { get; set; }
    }
    public class ONotificationDevicePush
    {
        public ONotification notification { get; set; }
        public string? to { get; set; }
        public string? priority { get; set; }
        public object data { get; set; }
    }
    public class FCMResponse
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMResult> results { get; set; }
    }

    public class FCMResult
    {
        public string? message_id { get; set; }
    }
    internal class FrameworkPushNotification
    {
        ONotification _ONotification;
        internal const string PrivateKey = "AIzaSyBY3JMwZnp8hi2qxRFWjkJ-7g2HC7ucVVo";
        internal const string SenderId = "968203048909";
        ONotificationDevicePushBody _ONotificationDevicePushBody;
        ONotificationDevicePush _ONotificationDevicePush;
        /// <summary>
        /// Sends the push to device.
        /// </summary>
        /// <param name="DeviceKey">The device key.</param>
        /// <param name="Task">The task.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="NavigateTo">The navigate to.</param>
        /// <param name="ReferenceKey">The reference key.</param>
        /// <param name="ConfirmText">The confirm text.</param>
        internal void SendPushToDevice(string DeviceKey, string? Task, string? Subject, string? Message, string? NavigateTo, string? ReferenceKey, string? ConfirmText)
        {
            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKey));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;

                _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;

                _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.to = DeviceKey;
                _ONotificationDevicePush.priority = "high";
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();
                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion
        }
        /// <summary>
        /// Sends the push to topic.
        /// </summary>
        /// <param name="TopicName">Name of the topic.</param>
        /// <param name="Task">The task.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="NavigateTo">The navigate to.</param>
        /// <param name="ReferenceKey">The reference key.</param>
        /// <param name="ConfirmText">The confirm text.</param>
        internal void SendPushToTopic(string TopicName, string? Task, string? Subject, string? Message, string? NavigateTo, string? ReferenceKey, string? ConfirmText)
        {

            #region Send Push
            string RequestString = "";
            string ResponseString = "";
            string ApiUrl = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ApiUrl);
            _HttpWebRequest.ContentType = "application/json; charset=utf-8";
            _HttpWebRequest.Method = "POST";
            _HttpWebRequest.Headers.Add(string.Format("Authorization: key={0}", PrivateKey));
            using (var _StreamWriter = new StreamWriter(_HttpWebRequest.GetRequestStream()))
            {
                _ONotificationDevicePushBody = new ONotificationDevicePushBody();
                _ONotificationDevicePushBody.Task = Task;
                _ONotificationDevicePushBody.Subject = Subject;
                _ONotificationDevicePushBody.Message = Message;
                _ONotificationDevicePushBody.NavigateTo = NavigateTo;
                _ONotificationDevicePushBody.ReferenceKey = ReferenceKey;
                _ONotificationDevicePushBody.ConfirmText = ConfirmText;

                _ONotification = new ONotification();
                _ONotification.title = Subject;
                _ONotification.body = Message;

                _ONotificationDevicePush = new ONotificationDevicePush();
                _ONotificationDevicePush.to = "/topics/" + TopicName;
                _ONotificationDevicePush.notification = _ONotification;
                _ONotificationDevicePush.data = _ONotificationDevicePushBody;
                RequestString = JsonConvert.SerializeObject(_ONotificationDevicePush,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                _StreamWriter.Write(RequestString);
                _StreamWriter.Flush();
                _StreamWriter.Close();
            }
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                using (var _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    ResponseString = _StreamReader.ReadToEnd();
                    _StreamReader.Close();
                }
            }
            catch (WebException _WebException)
            {
                if (_WebException.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)_WebException.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            ResponseString = reader.ReadToEnd();
                        }
                    }
                }
            }
            #endregion

        }
    }
}
