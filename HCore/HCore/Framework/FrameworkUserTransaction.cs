//==================================================================================
// FileName: FrameworkUserTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user transactions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using static HCore.CoreConstant;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Data.Logging;

namespace HCore.Framework
{
    public class FrameworkUserTransaction
    {

        #region Entities
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        HCUAccountTransaction _HCUAccountTransaction;
        #endregion
        #region Objects
        OList.Response _OListResponse;
        OTransactionResponse _OTransactionResponse;
        OTransaction.Balance.Response _BalanceResponse;
        OTransactionHelper _OTransactionHelperSender;
        OTransactionHelper _OTransactionHelperReceiver;
        OTransactionReference _OTransactionReference;
        OTransaction.List _OTransactionListSum;
        #endregion
        /// <summary>
        /// Description: Processes the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ProcessTransaction(OProcessTransaction _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Type))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                    #endregion
                }
                else if (_Request.Amount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                    #endregion
                }
                //else if (_Request.AmountType != "currency" && _Request.AmountType != "points")
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                //    #endregion
                //}
                else if (_Request.Sender == null && _Request.Receiver == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                    #endregion
                }
                else
                {
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.Type, HelperType.TransactionType, _Request.UserReference);
                    if (TypeId == null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        //int AmountType = 1;
                        //if (_Request.AmountType == "currency")
                        //{
                        //    AmountType = 2;
                        //}
                        //else
                        //{
                        //    AmountType = 1;
                        //}


                        if (_Request.Sender != null && _Request.Receiver != null)
                        {
                            #region Credit And Debit
                            if (string.IsNullOrEmpty(_Request.Sender.UserAccountKey) || string.IsNullOrEmpty(_Request.Receiver.UserAccountKey))
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            else if (_Request.Sender.UserAccountKey == _Request.Receiver.UserAccountKey)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            else
                            {
                                #region Debit Only
                                int SenderSourceId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.Sender.Source && x.ParentId == HelperType.TransactionSource).Select(x => x.Id).FirstOrDefault();
                                if (SenderSourceId == 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                    #endregion
                                }
                                OTransactionUserDetails SenderDetails = _HCoreContext.HCUAccount
                                                                 .Where(x => x.Guid == _Request.Sender.UserAccountKey &&
                                                                        (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive) &&
                                                                        (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive))
                                                                   .Select(x => new OTransactionUserDetails
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       ContactNumber = x.ContactNumber,
                                                                       CountryKey = x.Country.Guid,
                                                                       AccountType = x.AccountTypeId,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerAccountType = x.Owner.AccountTypeId,
                                                                       OwnerParentId = x.Owner.OwnerId,
                                                                       OwnerParentAccountType = x.Owner.Owner.AccountTypeId,
                                                                   }).FirstOrDefault();
                                if (SenderDetails == null)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                    #endregion
                                }
                                else
                                {
                                    OTransactionUserDetails ReceiverDetails = _HCoreContext.HCUAccount
                                                                              .Where(x => x.Guid == _Request.Receiver.UserAccountKey &&
                                                                                     (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive) &&
                                                                                     (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive))
                                                                                .Select(x => new OTransactionUserDetails
                                                                                {
                                                                                    ReferenceId = x.Id,
                                                                                    DisplayName = x.DisplayName,
                                                                                    Name = x.Name,
                                                                                    EmailAddress = x.EmailAddress,
                                                                                    ContactNumber = x.ContactNumber,
                                                                                    AccountType = x.AccountTypeId,
                                                                                    OwnerId = x.OwnerId,
                                                                                    OwnerAccountType = x.Owner.AccountTypeId,
                                                                                    OwnerParentId = x.Owner.OwnerId,
                                                                                    OwnerParentAccountType = x.Owner.Owner.AccountTypeId,
                                                                                }).FirstOrDefault();
                                    if (ReceiverDetails == null)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                        #endregion
                                    }
                                    else
                                    {

                                        int ReceiverSourceId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.Receiver.Source && x.ParentId == HelperType.TransactionSource).Select(x => x.Id).FirstOrDefault();
                                        if (ReceiverSourceId == 0)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                            #endregion
                                        }
                                        double SenderTotalCredit = _HCoreContext.HCUAccountTransaction
                                                                                .Where(x => x.AccountId == SenderDetails.ReferenceId &&
                                                                                       x.SourceId == SenderSourceId &&
                                                                                         x.ModeId == TransactionMode.Credit &&
                                                                                         x.StatusId == StatusHelperId.Transaction_Success)
                                                                                        .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        double SenderTotalDebit = _HCoreContext.HCUAccountTransaction
                                                                               .Where(x => x.AccountId == SenderDetails.ReferenceId &&
                                                                                      x.SourceId == SenderSourceId &&
                                                                                      x.ModeId == TransactionMode.Debit &&
                                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        double SenderOldBalance = Math.Round((SenderTotalCredit - SenderTotalDebit), 2);

                                        double SenderAmount = Math.Round(_Request.Amount / 100, 2);
                                        double SenderDiscountPercentage = Math.Round(_Request.Sender.DiscountPercentage / 100, 2);
                                        double SenderDiscountAmount = 0;
                                        #region Calculate Discount Amount
                                        if (SenderDiscountPercentage > 0)
                                        {
                                            double TSenderDiscountAmount = Math.Round((SenderAmount * SenderDiscountPercentage) / 100, 2);
                                            if (TSenderDiscountAmount > -1)
                                            {
                                                SenderDiscountAmount = TSenderDiscountAmount;
                                            }
                                            else
                                            {
                                                SenderDiscountAmount = 0;
                                            }
                                        }
                                        else
                                        {
                                            SenderDiscountAmount = 0;
                                        }
                                        if (SenderDiscountAmount > SenderAmount)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                            #endregion
                                        }
                                        #endregion
                                        double SenderDiscountedAmount = SenderAmount - SenderDiscountAmount;
                                        double SenderCharge = Math.Round(_Request.Sender.Charge / 100, 2);
                                        double SenderComissionPercentage = Math.Round(_Request.Sender.ComissionPercentage / 100, 2);
                                        double SenderComissionAmount = 0;

                                        #region Calculate Comission Amount
                                        if (SenderComissionAmount > 0)
                                        {
                                            double TSenderComissionAmount = Math.Round((SenderDiscountedAmount * SenderComissionPercentage) / 100, 2);
                                            if (TSenderComissionAmount > -1)
                                            {
                                                SenderComissionAmount = TSenderComissionAmount;
                                            }
                                            else
                                            {
                                                SenderComissionAmount = 0;
                                            }
                                        }
                                        else
                                        {
                                            SenderComissionAmount = 0;
                                        }
                                        #endregion
                                        double SenderPayableAmount = Math.Round(SenderDiscountedAmount + SenderCharge + SenderComissionAmount, 2);
                                        double SenderTotalAmount = SenderPayableAmount;
                                        //double SenderTotalAmount = Math.Round(SenderDiscountedAmount + SenderComissionAmount, 2);
                                        double SenderConversionAmount = SenderTotalAmount;
                                        //if (AmountType == 1)
                                        //{
                                        //    SenderConversionAmount = SenderTotalAmount / 10;
                                        //}
                                        double SenderNewBalance = SenderOldBalance - SenderTotalAmount;
                                        if ((SenderOldBalance + 1) > SenderTotalAmount && SenderNewBalance > -1)
                                        {


                                            double ReceiverTotalCredit = _HCoreContext.HCUAccountTransaction
                                                                                      .Where(x => x.AccountId == ReceiverDetails.ReferenceId &&
                                                                                             x.SourceId == ReceiverSourceId &&
                                                                                             x.ModeId == TransactionMode.Credit &&
                                                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                                                            .Sum(x => (double?)x.TotalAmount) ?? 0;

                                            double ReceiverTotalDebit = _HCoreContext.HCUAccountTransaction
                                                                                     .Where(x => x.AccountId == ReceiverDetails.ReferenceId &&
                                                                                            x.SourceId == ReceiverSourceId &&
                                                                                     x.ModeId == TransactionMode.Debit &&
                                                                                     x.StatusId == StatusHelperId.Transaction_Success)
                                                                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                                            double ReceiverOldBalance = Math.Round((ReceiverTotalCredit - ReceiverTotalDebit), 2);

                                            double ReceiverAmount = Math.Round(_Request.Amount / 100, 2);
                                            double ReceiverCharge = Math.Round(_Request.Receiver.Charge / 100, 2);
                                            double ReceiverComissionPercentage = Math.Round(_Request.Receiver.ComissionPercentage / 100, 2);
                                            double ReceiverComissionAmount = 0;
                                            #region Calculate Comission Amount
                                            if (ReceiverComissionAmount > 0)
                                            {
                                                double TReceiverComissionAmount = Math.Round(((ReceiverAmount - ReceiverAmount) * ReceiverComissionPercentage) / 100, 2);
                                                if (TReceiverComissionAmount > -1)
                                                {
                                                    ReceiverComissionAmount = TReceiverComissionAmount;
                                                }
                                                else
                                                {
                                                    ReceiverComissionAmount = 0;
                                                }
                                            }
                                            else
                                            {
                                                ReceiverComissionAmount = 0;
                                            }
                                            #endregion
                                            double ReceiverPayableAmount = Math.Round(ReceiverAmount + ReceiverCharge + ReceiverComissionAmount, 2);
                                            double ReceiverTotalAmount = ReceiverPayableAmount;
                                            double ReceiverNewBalance = ReceiverOldBalance + ReceiverTotalAmount;
                                            double ReceiverConversionAmount = ReceiverTotalAmount;
                                            //if (AmountType == 1)
                                            //{
                                            //    ReceiverConversionAmount = ReceiverTotalAmount / 10;
                                            //}
                                            #region Save Transaction
                                            _HCUAccountTransaction = new HCUAccountTransaction();
                                            _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid(); ;
                                            if (SenderDetails.AccountType == UserAccountType.Merchant)
                                            {
                                                _HCUAccountTransaction.ParentId = SenderDetails.ReferenceId;
                                            }
                                            else if (SenderDetails.OwnerAccountType != null && SenderDetails.OwnerAccountType == UserAccountType.Merchant)
                                            {
                                                _HCUAccountTransaction.ParentId = SenderDetails.OwnerId;
                                            }
                                            else if (SenderDetails.OwnerParentAccountType != null && SenderDetails.OwnerAccountType == UserAccountType.Merchant)
                                            {
                                                _HCUAccountTransaction.ParentId = SenderDetails.OwnerParentId;
                                            }
                                            //_HCUAccountTransaction.InoviceNumber = (_HCoreContext.HCUAccountTransaction.Count() + 1).ToString();
                                            _HCUAccountTransaction.AccountId = SenderDetails.ReferenceId;
                                            _HCUAccountTransaction.ModeId = TransactionMode.Debit;
                                            _HCUAccountTransaction.TypeId = (int)TypeId;
                                            _HCUAccountTransaction.SourceId = SenderSourceId;
                                            //_HCUAccountTransaction.AmountType = 2;
                                            _HCUAccountTransaction.Amount = SenderAmount;
                                            _HCUAccountTransaction.Charge = SenderCharge;
                                            //_HCUAccountTransaction.ComissionPercentage = SenderComissionPercentage;
                                            _HCUAccountTransaction.ComissionAmount = SenderComissionAmount;
                                            //_HCUAccountTransaction.DiscountPercentage = SenderDiscountPercentage;
                                            //_HCUAccountTransaction.DiscountAmount = SenderDiscountAmount;
                                            //_HCUAccountTransaction.PayableAmount = SenderPayableAmount;
                                            _HCUAccountTransaction.TotalAmount = SenderTotalAmount;
                                            _HCUAccountTransaction.Balance = SenderNewBalance;
                                            //_HCUAccountTransaction.PointsValue = 10;
                                            //_HCUAccountTransaction.ConversionAmount = SenderConversionAmount;
                                            _HCUAccountTransaction.PurchaseAmount = _Request.PurchaseAmount / 100 ?? 0;
                                            _HCUAccountTransaction.TransactionDate = _Request.TransactionDate;
                                            _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                                            _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                                            //_HCUAccountTransaction.Description = _Request.Description;
                                            //_HCUAccountTransaction.Data = _Request.Data;
                                            _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                            //_HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                                            _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                                            _HCUAccountTransaction.StatusId = (int)StatusId;
                                            _HCUAccountTransaction.GroupKey = _HCUAccountTransaction.Guid;
                                            _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                            _HCoreContext.SaveChanges();
                                            string ParentTransactionKey = _HCUAccountTransaction.Guid;
                                            long ParentTransactionId = _HCUAccountTransaction.Id;
                                            #endregion

                                            #region Build Sender Response
                                            _OTransactionHelperSender = new OTransactionHelper();
                                            _OTransactionHelperSender.TransactionId = _HCUAccountTransaction.Id;
                                            _OTransactionHelperSender.ReferenceKey = _Request.Sender.ReferenceKey;
                                            _OTransactionHelperSender.InoviceNumber = _HCUAccountTransaction.Id.ToString();
                                            _OTransactionHelperSender.Amount = _HCUAccountTransaction.Amount;
                                            #endregion


                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                #region Credit Only

                                                #region Save Transaction
                                                _HCUAccountTransaction = new HCUAccountTransaction();
                                                _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid(); ;
                                                //_HCUAccountTransaction.InoviceNumber = (_HCoreContext.HCUAccountTransaction.Count() + 1).ToString();
                                                if (ReceiverDetails.AccountType == UserAccountType.Merchant)
                                                {
                                                    _HCUAccountTransaction.ParentId = ReceiverDetails.ReferenceId;
                                                }
                                                else if (ReceiverDetails.OwnerAccountType != null && ReceiverDetails.OwnerAccountType == UserAccountType.Merchant)
                                                {
                                                    _HCUAccountTransaction.ParentId = SenderDetails.OwnerId;
                                                }
                                                else if (ReceiverDetails.OwnerParentAccountType != null && ReceiverDetails.OwnerAccountType == UserAccountType.Merchant)
                                                {
                                                    _HCUAccountTransaction.ParentId = SenderDetails.OwnerParentId;
                                                }
                                                _HCUAccountTransaction.AccountId = ReceiverDetails.ReferenceId;
                                                _HCUAccountTransaction.ModeId = TransactionMode.Credit;
                                                _HCUAccountTransaction.TypeId = (int)TypeId;
                                                _HCUAccountTransaction.SourceId = ReceiverSourceId;
                                                //_HCUAccountTransaction.AmountType = 2;
                                                _HCUAccountTransaction.Amount = ReceiverAmount;
                                                _HCUAccountTransaction.Charge = ReceiverCharge;
                                                //_HCUAccountTransaction.ComissionPercentage = ReceiverComissionPercentage;
                                                _HCUAccountTransaction.ComissionAmount = ReceiverComissionAmount;
                                                //_HCUAccountTransaction.DiscountPercentage = 0;
                                                //_HCUAccountTransaction.DiscountAmount = 0;
                                                //_HCUAccountTransaction.PayableAmount = ReceiverPayableAmount;
                                                _HCUAccountTransaction.TotalAmount = ReceiverTotalAmount;
                                                _HCUAccountTransaction.Balance = ReceiverNewBalance;
                                                //_HCUAccountTransaction.PointsValue = 10;
                                                _HCUAccountTransaction.ParentTransactionId = ParentTransactionId;
                                                //_HCUAccountTransaction.ConversionAmount = ReceiverConversionAmount;
                                                _HCUAccountTransaction.PurchaseAmount = _Request.PurchaseAmount / 100 ?? 0;
                                                _HCUAccountTransaction.TransactionDate = _Request.TransactionDate;
                                                _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                                                //_HCUAccountTransaction.DeviceId
                                                //ToUserAccountId
                                                _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                                                //_HCUAccountTransaction.Description = _Request.Description;
                                                //_HCUAccountTransaction.Data = _Request.Data;

                                                _HCUAccountTransaction.GroupKey = ParentTransactionKey;

                                                _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                                //_HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                                                _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                                                _HCUAccountTransaction.StatusId = (int)StatusId;
                                                _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                                _HCoreContext.SaveChanges();
                                                #endregion
                                                #endregion
                                                #region Build Receiver Response
                                                _OTransactionHelperReceiver = new OTransactionHelper();
                                                _OTransactionHelperReceiver.TransactionId = _HCUAccountTransaction.Id;
                                                _OTransactionHelperReceiver.ReferenceKey = _Request.Receiver.ReferenceKey;
                                                _OTransactionHelperReceiver.InoviceNumber = _HCUAccountTransaction.Id.ToString();
                                                _OTransactionHelperReceiver.Amount = _HCUAccountTransaction.Amount;
                                                #endregion
                                                #region Build Response
                                                _OTransactionResponse = new OTransactionResponse();
                                                _OTransactionResponse.Amount = _HCUAccountTransaction.Amount;
                                                _OTransactionResponse.Sender = _OTransactionHelperSender;
                                                _OTransactionResponse.Receiver = _OTransactionHelperReceiver;
                                                #endregion
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionResponse, "HCUT110");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                            #endregion
                                        }
                                    }

                                }
                                #endregion

                            }
                            #endregion
                        }
                        else if (_Request.Sender != null && _Request.Receiver == null)
                        {
                            #region Debit Only
                            int SenderSourceId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.Sender.Source && x.ParentId == HelperType.TransactionSource).Select(x => x.Id).FirstOrDefault();
                            if (SenderSourceId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            OTransactionUserDetails SenderDetails = _HCoreContext.HCUAccount
                                                             .Where(x => x.Guid == _Request.Sender.UserAccountKey &&
                                                                    (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive) &&
                                                                    (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive))
                                                               .Select(x => new OTransactionUserDetails
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   EmailAddress = x.EmailAddress,
                                                                   ContactNumber = x.ContactNumber,
                                                                   CountryKey = x.Country.Guid,
                                                                   AccountType = x.AccountTypeId,
                                                                   OwnerId = x.OwnerId,
                                                                   OwnerAccountType = x.Owner.AccountTypeId,
                                                                   OwnerParentId = x.Owner.OwnerId,
                                                                   OwnerParentAccountType = x.Owner.Owner.AccountTypeId,
                                                               }).FirstOrDefault();
                            if (SenderDetails == null)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            else
                            {
                                double SenderTotalCredit = _HCoreContext.HCUAccountTransaction
                                                                        .Where(x => x.AccountId == SenderDetails.ReferenceId &&
                                                                               x.SourceId == SenderSourceId &&
                                                                                 x.ModeId == TransactionMode.Credit &&
                                                                               x.StatusId == StatusHelperId.Transaction_Success)
                                                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double SenderTotalDebit = _HCoreContext.HCUAccountTransaction
                                                                       .Where(x => x.AccountId == SenderDetails.ReferenceId &&
                                                                              x.SourceId == SenderSourceId &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                              x.StatusId == StatusHelperId.Transaction_Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                double SenderOldBalance = Math.Round((SenderTotalCredit - SenderTotalDebit), 2);

                                double SenderAmount = Math.Round(_Request.Amount / 100, 2);
                                double SenderDiscountPercentage = Math.Round(_Request.Sender.DiscountPercentage / 100, 2);
                                double SenderDiscountAmount = 0;
                                #region Calculate Discount Amount
                                if (SenderDiscountPercentage > 0)
                                {
                                    double TSenderDiscountAmount = Math.Round((SenderAmount * SenderDiscountPercentage) / 100, 2);
                                    if (TSenderDiscountAmount > -1)
                                    {
                                        SenderDiscountAmount = TSenderDiscountAmount;
                                    }
                                    else
                                    {
                                        SenderDiscountAmount = 0;
                                    }
                                }
                                else
                                {
                                    SenderDiscountAmount = 0;
                                }
                                if (SenderDiscountAmount > SenderAmount)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                    #endregion
                                }
                                #endregion
                                double SenderDiscountedAmount = SenderAmount - SenderDiscountAmount;
                                double SenderCharge = Math.Round(_Request.Sender.Charge / 100, 2);
                                double SenderComissionPercentage = Math.Round(_Request.Sender.ComissionPercentage / 100, 2);
                                double SenderComissionAmount = 0;

                                #region Calculate Comission Amount
                                if (SenderComissionAmount > 0)
                                {
                                    double TSenderComissionAmount = Math.Round((SenderDiscountedAmount * SenderComissionPercentage) / 100, 2);
                                    if (TSenderComissionAmount > -1)
                                    {
                                        SenderComissionAmount = TSenderComissionAmount;
                                    }
                                    else
                                    {
                                        SenderComissionAmount = 0;
                                    }
                                }
                                else
                                {
                                    SenderComissionAmount = 0;
                                }
                                #endregion
                                double SenderPayableAmount = Math.Round(SenderDiscountedAmount + SenderCharge + SenderComissionAmount, 2);
                                double SenderTotalAmount = SenderPayableAmount;
                                //double SenderTotalAmount = Math.Round(SenderDiscountedAmount + SenderComissionAmount, 2);
                                double SenderConversionAmount = SenderTotalAmount;
                                //if (AmountType == 1)
                                //{
                                //    SenderConversionAmount = SenderTotalAmount / 10;
                                //}
                                double SenderNewBalance = SenderOldBalance - SenderTotalAmount;
                                if ((SenderOldBalance + 1) > SenderTotalAmount && SenderNewBalance > -1)
                                {
                                    #region Save Transaction
                                    _HCUAccountTransaction = new HCUAccountTransaction();
                                    _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid(); ;
                                    //  _HCUAccountTransaction.InoviceNumber = (_HCoreContext.HCUAccountTransaction.Count() + 1).ToString();
                                    if (SenderDetails.AccountType == UserAccountType.Merchant)
                                    {
                                        _HCUAccountTransaction.ParentId = SenderDetails.ReferenceId;
                                    }
                                    else if (SenderDetails.OwnerAccountType != null && SenderDetails.OwnerAccountType == UserAccountType.Merchant)
                                    {
                                        _HCUAccountTransaction.ParentId = SenderDetails.OwnerId;
                                    }
                                    else if (SenderDetails.OwnerParentAccountType != null && SenderDetails.OwnerAccountType == UserAccountType.Merchant)
                                    {
                                        _HCUAccountTransaction.ParentId = SenderDetails.OwnerParentId;
                                    }
                                    _HCUAccountTransaction.AccountId = SenderDetails.ReferenceId;
                                    _HCUAccountTransaction.ModeId = TransactionMode.Debit;
                                    _HCUAccountTransaction.TypeId = (int)TypeId;
                                    _HCUAccountTransaction.SourceId = SenderSourceId;
                                    //_HCUAccountTransaction.AmountType = 2;
                                    _HCUAccountTransaction.Amount = SenderAmount;
                                    _HCUAccountTransaction.Charge = SenderCharge;
                                    //_HCUAccountTransaction.ComissionPercentage = SenderComissionPercentage;
                                    _HCUAccountTransaction.ComissionAmount = SenderComissionAmount;
                                    //_HCUAccountTransaction.DiscountPercentage = SenderDiscountPercentage;
                                    //_HCUAccountTransaction.DiscountAmount = SenderDiscountAmount;
                                    //_HCUAccountTransaction.PayableAmount = SenderPayableAmount;
                                    _HCUAccountTransaction.TotalAmount = SenderTotalAmount;
                                    _HCUAccountTransaction.Balance = SenderNewBalance;
                                    //_HCUAccountTransaction.PointsValue = 10;
                                    //_HCUAccountTransaction.ConversionAmount = SenderConversionAmount;
                                    _HCUAccountTransaction.PurchaseAmount = _Request.PurchaseAmount / 100 ?? 0;
                                    _HCUAccountTransaction.TransactionDate = _Request.TransactionDate;
                                    _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                                    //_HCUAccountTransaction.DeviceId
                                    //ToUserAccountId
                                    _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                                    //_HCUAccountTransaction.Description = _Request.Description;
                                    //_HCUAccountTransaction.Data = _Request.Data;
                                    _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                    //_HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                                    _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountTransaction.StatusId = (int)StatusId;
                                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                    #region Build Response
                                    _OTransactionResponse = new OTransactionResponse();
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionResponse, "HCUT110");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                    #endregion
                                }

                            }
                            #endregion
                        }
                        else if (_Request.Sender == null && _Request.Receiver != null)
                        {
                            #region Credit Only
                            int ReceiverSourceId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.Receiver.Source && x.ParentId == HelperType.TransactionSource).Select(x => x.Id).FirstOrDefault();
                            if (ReceiverSourceId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            OTransactionUserDetails ReceiverDetails = _HCoreContext.HCUAccount
                                                              .Where(x => x.Guid == _Request.Receiver.UserAccountKey &&
                                                                     (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive) &&
                                                                     (x.StatusId == StatusHelperId.Default_Active || x.StatusId == StatusHelperId.Default_Inactive))
                                                                .Select(x => new OTransactionUserDetails
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    ContactNumber = x.ContactNumber,
                                                                    AccountType = x.AccountTypeId,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerAccountType = x.Owner.AccountTypeId,
                                                                    OwnerParentId = x.Owner.OwnerId,
                                                                    OwnerParentAccountType = x.Owner.Owner.AccountTypeId,
                                                                }).FirstOrDefault();
                            if (ReceiverDetails == null)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                                #endregion
                            }
                            else
                            {
                                double ReceiverTotalCredit = _HCoreContext.HCUAccountTransaction
                                                                          .Where(x => x.AccountId == ReceiverDetails.ReferenceId &&
                                                                                 x.SourceId == ReceiverSourceId &&
                                                                                 x.ModeId == TransactionMode.Credit &&
                                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double ReceiverTotalDebit = _HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.AccountId == ReceiverDetails.ReferenceId &&
                                                                                x.SourceId == ReceiverSourceId &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                         x.StatusId == StatusHelperId.Transaction_Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                double ReceiverOldBalance = Math.Round((ReceiverTotalCredit - ReceiverTotalDebit), 2);
                                double ReceiverAmount = Math.Round(_Request.Amount / 100, 2);
                                double ReceiverCharge = Math.Round(_Request.Receiver.Charge / 100, 2);
                                double ReceiverComissionPercentage = Math.Round(_Request.Receiver.ComissionPercentage / 100, 2);
                                double ReceiverComissionAmount = 0;
                                #region Calculate Comission Amount
                                if (ReceiverComissionAmount > 0)
                                {
                                    double TReceiverComissionAmount = Math.Round(((ReceiverAmount + ReceiverAmount) * ReceiverComissionPercentage) / 100, 2);
                                    if (TReceiverComissionAmount > -1)
                                    {
                                        ReceiverComissionAmount = TReceiverComissionAmount;
                                    }
                                    else
                                    {
                                        ReceiverComissionAmount = 0;
                                    }
                                }
                                else
                                {
                                    ReceiverComissionAmount = 0;
                                }
                                #endregion
                                double ReceiverPayableAmount = Math.Round(ReceiverAmount + ReceiverCharge + ReceiverComissionAmount, 2);
                                double ReceiverTotalAmount = ReceiverPayableAmount;
                                //double ReceiverTotalAmount = Math.Round(ReceiverAmount, 2);
                                double ReceiverNewBalance = ReceiverOldBalance + ReceiverTotalAmount;
                                double ReceiverConversionAmount = ReceiverTotalAmount;
                                //if (AmountType == 1)
                                //{
                                //    ReceiverConversionAmount = ReceiverTotalAmount / 10;
                                //}
                                #region Save Transaction
                                _HCUAccountTransaction = new HCUAccountTransaction();
                                _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid(); ;
                                if (ReceiverDetails.AccountType == UserAccountType.Merchant)
                                {
                                    _HCUAccountTransaction.ParentId = ReceiverDetails.ReferenceId;
                                }
                                else if (ReceiverDetails.OwnerAccountType != null && ReceiverDetails.OwnerAccountType == UserAccountType.Merchant)
                                {
                                    _HCUAccountTransaction.ParentId = ReceiverDetails.OwnerId;
                                }
                                else if (ReceiverDetails.OwnerParentAccountType != null && ReceiverDetails.OwnerAccountType == UserAccountType.Merchant)
                                {
                                    _HCUAccountTransaction.ParentId = ReceiverDetails.OwnerParentId;
                                }
                                //_HCUAccountTransaction.InoviceNumber = (_HCoreContext.HCUAccountTransaction.Count() + 1).ToString();
                                _HCUAccountTransaction.AccountId = ReceiverDetails.ReferenceId;
                                _HCUAccountTransaction.ModeId = TransactionMode.Credit;
                                _HCUAccountTransaction.TypeId = (int)TypeId;
                                _HCUAccountTransaction.SourceId = ReceiverSourceId;
                                //_HCUAccountTransaction.AmountType = 2;
                                _HCUAccountTransaction.Amount = ReceiverAmount;
                                _HCUAccountTransaction.Charge = ReceiverCharge;
                                //_HCUAccountTransaction.ComissionPercentage = ReceiverComissionPercentage;
                                _HCUAccountTransaction.ComissionAmount = ReceiverComissionAmount;
                                //_HCUAccountTransaction.DiscountPercentage = 0;
                                //_HCUAccountTransaction.DiscountAmount = 0;
                                //_HCUAccountTransaction.PayableAmount = ReceiverPayableAmount;
                                _HCUAccountTransaction.TotalAmount = ReceiverTotalAmount;
                                _HCUAccountTransaction.Balance = ReceiverNewBalance;
                                //_HCUAccountTransaction.PointsValue = 10;
                                //_HCUAccountTransaction.ConversionAmount = ReceiverConversionAmount;
                                _HCUAccountTransaction.PurchaseAmount = _Request.PurchaseAmount / 100 ?? 0;
                                _HCUAccountTransaction.TransactionDate = _Request.TransactionDate;
                                _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                                _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                                //_HCUAccountTransaction.Description = _Request.Description;
                                //_HCUAccountTransaction.Data = _Request.Data;
                                _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                //_HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                                _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountTransaction.StatusId = (int)StatusId;
                                _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                _HCoreContext.SaveChanges();
                                #endregion
                            }
                            #endregion
                            #region Build Response
                            _OTransactionResponse = new OTransactionResponse();
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionResponse, "HCUT110");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                            #endregion
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ProcessTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OTransactionReference.</returns>
        internal OTransactionReference SaveTransaction(OSaveTransaction _Request)
        {
            #region Declare

            _OTransactionReference = new OTransactionReference();
            #endregion
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    string TransactionKey = HCoreHelper.GenerateGuid();
                    #region Save Transaction
                    _HCUAccountTransaction = new HCUAccountTransaction();
                    _HCUAccountTransaction.Guid = TransactionKey;
                    _HCUAccountTransaction.InoviceNumber = _Request.InvoiceNumber;
                    if (_Request.UserAccountId != 0)
                    {
                        _HCUAccountTransaction.AccountId = _Request.UserAccountId;
                    }
                    _HCUAccountTransaction.ModeId = _Request.ModeId;
                    _HCUAccountTransaction.TypeId = _Request.TypeId;
                    _HCUAccountTransaction.SourceId = _Request.SourceId;

                    //_HCUAccountTransaction.AmountType = 2;
                    //_HCUAccountTransaction.AmountPercentage = _Request.AmountPercentage;
                    _HCUAccountTransaction.Amount = _Request.Amount;

                    _HCUAccountTransaction.Charge = _Request.Charge;
                    //_HCUAccountTransaction.ChargePercentage = _Request.ChargePercentage;

                    //_HCUAccountTransaction.ComissionPercentage = _Request.ComissionPercentage;
                    _HCUAccountTransaction.ComissionAmount = _Request.ComissionAmount;

                    //_HCUAccountTransaction.DiscountPercentage = 0;
                    //_HCUAccountTransaction.DiscountAmount = 0;

                    //_HCUAccountTransaction.PayableAmount = _Request.PayableAmount;
                    _HCUAccountTransaction.TotalAmount = _Request.TotalAmount;
                    //_HCUAccountTransaction.TotalAmountPercentage = _Request.TotalAmountPercentage;
                    _HCUAccountTransaction.ReferenceAmount = _Request.ReferenceAmount;

                    _HCUAccountTransaction.Balance = 0;
                    //_HCUAccountTransaction.PointsValue = 10;
                    //_HCUAccountTransaction.ConversionAmount = _Request.TotalAmount;
                    _HCUAccountTransaction.PurchaseAmount = _Request.PurchaseAmount;
                    if (_Request.TransactionDate != null)
                    {
                        _HCUAccountTransaction.TransactionDate = (DateTime)_Request.TransactionDate;
                    }
                    else
                    {
                        _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
                    }

                    _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                    if (_Request.ParentTransactionId != 0)
                    {
                        _HCUAccountTransaction.ParentTransactionId = _Request.ParentTransactionId;
                    }
                    _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                    //_HCUAccountTransaction.Description = _Request.Description;
                    //_HCUAccountTransaction.Data = _Request.Data;

                    _HCUAccountTransaction.ParentId = _Request.ParentId;
                    _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();


                    if (_Request.CreatedById != 0)
                    {
                        _HCUAccountTransaction.CreatedById = _Request.CreatedById;
                    }
                    else
                    {

                        if (_Request.UserReference != null && _Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                        }
                    }



                    //if (_Request.UserReference != null && _Request.UserReference.AccountSessionId != 0)
                    //{
                    //    _HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                    //}

                    if (!string.IsNullOrEmpty(_Request.GroupKey))
                    {
                        _HCUAccountTransaction.GroupKey = _Request.GroupKey;
                    }
                    else
                    {
                        _HCUAccountTransaction.GroupKey = TransactionKey;
                    }
                    _HCUAccountTransaction.StatusId = _Request.Status;
                    if (_Request.UserReference != null)
                    {
                        _HCUAccountTransaction.RequestKey = _Request.UserReference.RequestKey;
                    }
                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                    _HCoreContext.SaveChanges();
                    long ParentTransactionId = _HCUAccountTransaction.Id;
                    #region Send Response
                    _OTransactionReference.Status = 1;
                    _OTransactionReference.TransactionDate = _HCUAccountTransaction.CreateDate;
                    _OTransactionReference.ReferenceId = _HCUAccountTransaction.Id;
                    _OTransactionReference.ReferenceKey = _HCUAccountTransaction.Guid;
                    if (!string.IsNullOrEmpty(_Request.GroupKey))
                    {
                        _OTransactionReference.GroupKey = _Request.GroupKey;
                    }
                    else
                    {
                        _OTransactionReference.GroupKey = TransactionKey;
                    }
                    _OTransactionReference.Message = "Transaction complete";
                    return _OTransactionReference;
                    #endregion
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                if (_Request.UserReference != null)
                {
                    HCoreHelper.LogException(LogLevel.High, "ProcessTransaction", _Exception, _Request.UserReference);
                }
                else
                {
                    HCoreHelper.LogException("ProcessTransaction", _Exception);
                }
                #endregion
                #region Send Response
                _OTransactionReference.Status = 0;
                _OTransactionReference.Message = "Error occured while processing request";
                return _OTransactionReference;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OTransactionReference.</returns>
        internal OTransactionReference SaveTransaction(OSaveTransactions _Request)
        {
            #region Declare
            _OTransactionReference = new OTransactionReference();
            #endregion
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    long ParentTransactionId = 0;
                    string GroupKey = null;
                    string SubGroupKey = null;
                    foreach (OTransactionItem Transaction in _Request.Transactions)
                    {

                        _HCUAccountTransaction = new HCUAccountTransaction();
                        _HCUAccountTransaction.InoviceNumber = _Request.InvoiceNumber;
                        if (string.IsNullOrEmpty(GroupKey) && !string.IsNullOrEmpty(_Request.GroupKey))
                        {
                            GroupKey = _Request.GroupKey;
                            _HCUAccountTransaction.Guid = GroupKey;
                        }
                        else
                        {
                            _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                        }
                        if (!string.IsNullOrEmpty(_Request.ParentTransactionKey))
                        {
                            GroupKey = _Request.ParentTransactionKey;
                            if (string.IsNullOrEmpty(SubGroupKey))
                            {
                                SubGroupKey = _HCUAccountTransaction.Guid;
                            }
                        }
                        _HCUAccountTransaction.ParentId = _Request.ParentId;

                        if (Transaction.UserAccountId != 0)
                        {
                            _HCUAccountTransaction.AccountId = Transaction.UserAccountId;
                        }
                        #region Save Transaction
                        _HCUAccountTransaction.ModeId = Transaction.ModeId;
                        _HCUAccountTransaction.TypeId = Transaction.TypeId;
                        _HCUAccountTransaction.SourceId = Transaction.SourceId;

                        //_HCUAccountTransaction.AmountType = 2;

                        _HCUAccountTransaction.Amount = Transaction.Amount;
                        //_HCUAccountTransaction.AmountPercentage = HCoreHelper.GetAmountPercentage(Transaction.Amount, Transaction.ReferenceAmount);

                        _HCUAccountTransaction.Charge = Transaction.Charge;
                        //_HCUAccountTransaction.ChargePercentage = HCoreHelper.GetAmountPercentage(Transaction.Charge, Transaction.ReferenceAmount);

                        _HCUAccountTransaction.ComissionAmount = Transaction.Comission;
                        //_HCUAccountTransaction.ComissionPercentage = HCoreHelper.GetAmountPercentage(Transaction.Comission, Transaction.ReferenceAmount);

                        //_HCUAccountTransaction.DiscountAmount = 0;
                        //_HCUAccountTransaction.DiscountPercentage = 0;

                        //_HCUAccountTransaction.PayableAmount = Transaction.TotalAmount;
                        _HCUAccountTransaction.TotalAmount = Transaction.TotalAmount;
                        //_HCUAccountTransaction.TotalAmountPercentage = HCoreHelper.GetAmountPercentage(Transaction.TotalAmount, Transaction.ReferenceAmount);

                        _HCUAccountTransaction.Balance = 0;
                        //_HCUAccountTransaction.PointsValue = 10;
                        //_HCUAccountTransaction.ConversionAmount = Transaction.TotalAmount;
                        _HCUAccountTransaction.PurchaseAmount = _Request.InvoiceAmount;
                        _HCUAccountTransaction.ReferenceAmount = Transaction.ReferenceAmount;
                        if (Transaction.TransactionDate != null)
                        {
                            _HCUAccountTransaction.TransactionDate = (DateTime)Transaction.TransactionDate;
                        }
                        else
                        {
                            _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                        //if (_Request.UserReference.DeviceId != 0)
                        //{
                        //    _HCUAccountTransaction.DeviceId = _Request.UserReference.DeviceId;
                        //}
                        _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                        _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                        //_HCUAccountTransaction.Description = Transaction.Description;
                        _HCUAccountTransaction.Comment = Transaction.Comment;
                        //_HCUAccountTransaction.Data = _Request.Data;

                        _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();

                        if (ParentTransactionId != 0)
                        {
                            _HCUAccountTransaction.ParentTransactionId = ParentTransactionId;
                        }

                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccountTransaction.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        //if (_Request.UserReference.AccountSessionId != 0)
                        //{
                        //    _HCUAccountTransaction.SessionId = _Request.UserReference.AccountSessionId;
                        //}
                        if (!string.IsNullOrEmpty(GroupKey))
                        {
                            _HCUAccountTransaction.GroupKey = GroupKey;
                        }
                        if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                        {
                            _HCUAccountTransaction.RequestKey = _Request.UserReference.RequestKey;
                        }
                        _HCUAccountTransaction.StatusId = _Request.StatusId;
                        //List<string> PrimaryTags = new List<string>
                        //{
                        //    _HCUAccountTransaction.Guid,
                        //    _HCUAccountTransaction.GroupKey,
                        //    _HCUAccountTransaction.Amount.ToString(),
                        //    _HCUAccountTransaction.AmountPercentage.ToString(),
                        //    _HCUAccountTransaction.Charge.ToString(),
                        //    _HCUAccountTransaction.ChargePercentage.ToString(),

                        //    _HCUAccountTransaction.ComissionAmount.ToString(),
                        //    _HCUAccountTransaction.ComissionPercentage.ToString(),

                        //    _HCUAccountTransaction.DiscountAmount.ToString(),
                        //    _HCUAccountTransaction.DiscountPercentage.ToString(),

                        //     _HCUAccountTransaction.PayableAmount.ToString(),

                        //    _HCUAccountTransaction.TotalAmount.ToString(),
                        //    _HCUAccountTransaction.TotalAmountPercentage.ToString(),
                        //     _HCUAccountTransaction.ConversionAmount.ToString(),
                        //     _HCUAccountTransaction.PurchaseAmount.ToString(),
                        //     _HCUAccountTransaction.ReferenceAmount.ToString(),
                        //     _HCUAccountTransaction.TransactionDate.ToString(),
                        //     _HCUAccountTransaction.InoviceNumber,
                        //     _HCUAccountTransaction.AccountNumber,
                        //     _HCUAccountTransaction.ReferenceNumber,
                        //};
                        //string PrimaryTag = "";
                        //foreach (var PrimaryTagItem in PrimaryTags)
                        //{
                        //    if (!string.IsNullOrEmpty(PrimaryTagItem))
                        //    {
                        //        if (PrimaryTagItem != "0")
                        //        {
                        //            PrimaryTag = PrimaryTag + " | " + PrimaryTagItem;
                        //        }
                        //    }
                        //}
                        //_HCCoreTag = new HCCoreTag();
                        //_HCCoreTag.Guid = HCoreHelper.GenerateGuid();
                        //_HCCoreTag.PrimaryTag = PrimaryTag;
                        //_HCCoreTag.CreateDate = HCoreHelper.GetGMTDateTime();
                        //if (_Request.UserReference.AccountId != 0)
                        //{
                        //    _HCCoreTag.CreatedById = _Request.UserReference.AccountId;
                        //}
                        //_HCUAccountTransaction.Tag = _HCCoreTag;
                        _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                        #endregion
                    }
                    _HCoreContext.SaveChanges();
                    if (string.IsNullOrEmpty(_Request.ParentTransactionKey))
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            ParentTransactionId = _HCoreContext.HCUAccountTransaction.Where(x => x.Guid == GroupKey).Select(x => x.Id).FirstOrDefault();
                            if (ParentTransactionId != 0)
                            {
                                List<HCUAccountTransaction> Transactions = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == GroupKey && x.Id != ParentTransactionId).ToList();
                                foreach (HCUAccountTransaction Transaction in Transactions)
                                {
                                    Transaction.ParentTransactionId = ParentTransactionId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    #region Send Response
                    _OTransactionReference.Status = CoreHelperStatus.Transaction.Success;
                    _OTransactionReference.TransactionDate = _HCUAccountTransaction.CreateDate;
                    _OTransactionReference.ReferenceId = ParentTransactionId;
                    _OTransactionReference.ReferenceKey = GroupKey;
                    if (!string.IsNullOrEmpty(_Request.GroupKey))
                    {
                        _OTransactionReference.GroupKey = _Request.GroupKey;
                    }
                    _OTransactionReference.Message = "Transaction complete";
                    return _OTransactionReference;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ProcessTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                _OTransactionReference.Status = CoreHelperStatus.Transaction.Error;
                _OTransactionReference.Message = "Error occured while processing request";
                return _OTransactionReference;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Rolls the back transaction.
        /// </summary>
        /// <param name="ReferenceId">The reference identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        internal void RollBackTransaction(long ReferenceId, OUserReference UserReference)
        {
            #region Declare

            _OTransactionReference = new OTransactionReference();
            #endregion
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == ReferenceId).FirstOrDefault();
                    if (TransactionDetails != null)
                    {
                        _HCoreContext.HCUAccountTransaction.Remove(TransactionDetails);
                        _HCoreContext.SaveChanges();
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException(LogLevel.High, "ProcessTransaction", _Exception, UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountBalance(OTransaction.Balance.Request _Request)
        {
            #region Declare
            _BalanceResponse = new OTransaction.Balance.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                        {
                            var UserDetails = _HCoreContext.HCUAccount
                                                           .Where(x => x.Guid == _Request.UserAccountKey)
                                                           .Select(x => new
                                                           {
                                                               UserAccountId = x.Id,
                                                               UserAccountType = x.AccountTypeId,
                                                               UserId = x.UserId
                                                           }).FirstOrDefault();
                            if (UserDetails != null && UserDetails.UserAccountType == UserAccountType.Appuser)
                            {
                                DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.UserId == UserDetails.UserId
                                                                                                             && (x.Type.Value == TransactionTypeValue.Reward
                                                                                                             || x.Type.Value == TransactionTypeValue.Redeem
                                                                                                             || x.Type.Value == TransactionSource.PaymentsS))
                                                                        .OrderByDescending(x => x.CreateDate).Select(x => x.TransactionDate).FirstOrDefault();
                                int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;

                                if (LastTransactionDate != null)
                                {
                                    if (_Request.UserReference.OsId == 5 || _Request.UserReference.OsId == 6)
                                    {
                                        double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(x => x.Account.UserId == UserDetails.UserId &&
                                                                          x.SourceId == TransactionSource.TUC &&
                                                                 x.ModeId == TransactionMode.Credit &&
                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.Account.UserId == UserDetails.UserId &&
                                                                                x.SourceId == TransactionSource.TUC &&
                                                                                 x.ModeId == TransactionMode.Debit &&
                                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                                        //_BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100) * 10;
                                        //_BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100) * 10;
                                        //_BalanceResponse.Balance = Convert.ToInt64(Balance * 100) * 10;
                                        _BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100);
                                        _BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100);
                                        _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                                        if (DaysDifference < (_AppConfig.AppUserBalanceValidityDays + 1))
                                        {
                                            int Validity = _AppConfig.AppUserBalanceValidityDays - DaysDifference;
                                            _BalanceResponse.BalanceValidity = Validity;
                                        }
                                        else
                                        {
                                            _BalanceResponse.Balance = 0;
                                            _BalanceResponse.BalanceValidity = 0;
                                        }
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                        #endregion
                                    }
                                    else
                                    {
                                        double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(x => x.Account.UserId == UserDetails.UserId &&
                                                                 x.SourceId == TransactionSource.TUC &&
                                                                 x.ModeId == TransactionMode.Credit &&
                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                                        double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.Account.UserId == UserDetails.UserId &&
                                                                        x.SourceId == TransactionSource.TUC &&
                                                                                 x.ModeId == TransactionMode.Debit &&
                                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                                        _BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100);
                                        _BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100);
                                        _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                                        if (DaysDifference < (_AppConfig.AppUserBalanceValidityDays + 1))
                                        {
                                            int Validity = _AppConfig.AppUserBalanceValidityDays - DaysDifference;
                                            _BalanceResponse.BalanceValidity = Validity;
                                        }
                                        else
                                        {
                                            _BalanceResponse.Balance = 0;
                                            _BalanceResponse.BalanceValidity = 0;
                                        }
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    _BalanceResponse.Credit = 0;
                                    _BalanceResponse.Debit = 0;
                                    _BalanceResponse.Balance = 0;
                                    _BalanceResponse.BalanceValidity = 0;
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                    #endregion
                                }
                            }
                            else
                            {
                                double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                                                x.Source.SystemName == _Request.Source &&
                                                                x.ModeId == TransactionMode.Credit &&
                                                                x.StatusId == StatusHelperId.Transaction_Success)
                                                         .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                                                         x.Source.SystemName == _Request.Source &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                         x.StatusId == StatusHelperId.Transaction_Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                                _BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100);
                                _BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100);
                                _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                                if (_Request.Source == TransactionSource.PaymentsS)
                                {
                                    _BalanceResponse.TotalInvoice = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey).Count();
                                    _BalanceResponse.PaidInvoice = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Paid).Count();
                                    _BalanceResponse.UnpaidInvoice = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Pending).Count();
                                    _BalanceResponse.InvoiceAmountPaid = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Paid).Sum(x => x.TotalAmount);
                                    _BalanceResponse.InvoiceAmountUnpaid = _HCoreContext.HCUAccountInvoice.Where(x => x.Account.Guid == _Request.UserAccountKey && x.StatusId == StatusHelperId.Invoice_Pending).Sum(x => x.TotalAmount);
                                }

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                #endregion
                            }

                        }
                        else if (!string.IsNullOrEmpty(_Request.UserKey))
                        {

                            DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.Guid == _Request.UserKey
                                                                                                         && (x.Type.Value == TransactionTypeValue.Reward
                                                                                                         || x.Type.Value == TransactionTypeValue.Redeem
                                                                                                         || x.Type.Value == TransactionSource.PaymentsS))
                                                                     .OrderByDescending(x => x.CreateDate).Select(x => x.TransactionDate).FirstOrDefault();

                            if (LastTransactionDate != null)
                            {
                                int DaysDifference = (HCoreHelper.GetGMTDate() - LastTransactionDate).Value.Days;
                                int SecondsDifference = (HCoreHelper.GetGMTDate() - LastTransactionDate).Value.Seconds;
                                double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(x => x.Account.Guid == _Request.UserKey &&
                                                                          x.SourceId == TransactionSource.TUC &&
                                                                 x.ModeId == TransactionMode.Credit &&
                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.Account.Guid == _Request.UserKey &&
                                                                        x.SourceId == TransactionSource.TUC &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                         x.StatusId == StatusHelperId.Transaction_Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                                _BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100);
                                _BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100);
                                _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                                if (DaysDifference < (_AppConfig.AppUserBalanceValidityDays + 1))
                                {
                                    int Validity = _AppConfig.AppUserBalanceValidityDays - DaysDifference;
                                    _BalanceResponse.BalanceValidity = Validity;
                                    _BalanceResponse.ExpiaryTimeMinutes = Validity * 1440;
                                    _BalanceResponse.BalanceValidity = DaysDifference;
                                    _BalanceResponse.LastTransactionTime = LastTransactionDate;
                                }
                                else
                                {
                                    _BalanceResponse.Balance = 0;
                                    _BalanceResponse.BalanceValidity = 0;
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                #endregion
                            }
                            else
                            {
                                _BalanceResponse.Credit = 0;
                                _BalanceResponse.Debit = 0;
                                _BalanceResponse.Balance = 0;
                                _BalanceResponse.BalanceValidity = 0;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                #endregion
                            }
                        }
                        else
                        {
                            double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                          .Where(x => x.Source.SystemName == _Request.Source &&
                                                                 x.ModeId == TransactionMode.Credit &&
                                                                 x.StatusId == StatusHelperId.Transaction_Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0;

                            double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                             .Where(x => x.Source.SystemName == _Request.Source &&
                                                                     x.ModeId == TransactionMode.Debit &&
                                                                     x.StatusId == StatusHelperId.Transaction_Success)
                                                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                            _BalanceResponse.Credit = Convert.ToInt64(TotalCredit * 100);
                            _BalanceResponse.Debit = Convert.ToInt64(TotalDebit * 100);
                            _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                            if (_Request.Source == TransactionSource.PaymentsS)
                            {
                                _BalanceResponse.TotalInvoice = _HCoreContext.HCUAccountInvoice.Count();
                                _BalanceResponse.PaidInvoice = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Count();
                                _BalanceResponse.UnpaidInvoice = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending).Count();
                                _BalanceResponse.InvoiceAmountPaid = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Paid).Sum(x => x.TotalAmount);
                                _BalanceResponse.InvoiceAmountUnpaid = _HCoreContext.HCUAccountInvoice.Where(x => x.StatusId == StatusHelperId.Invoice_Pending).Sum(x => x.TotalAmount);
                            }

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                            #endregion
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="UserAccountKey">The user account key.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        internal double GetUserAccountBalance(long UserAccountKey, int SourceId, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == UserAccountKey &&
                                                             x.SourceId == SourceId &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;

                    double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == UserAccountKey &&
                                                             x.SourceId == SourceId &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                    #region Send Response
                    return Balance;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountBalance", _Exception, UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserBalance(long UserAccountId, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.TUC &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;

                    double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.TUC &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                    DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                                        && (x.Type.Value == TransactionTypeValue.Reward
                                                                                                            || x.Type.Value == TransactionTypeValue.Redeem
                                                                                                            || x.Type.Value == TransactionSource.PaymentsS))
                                                                        .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                    if (LastTransactionDate != null)
                    {
                        int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                        if (DaysDifference < 91)
                        {
                            #region Send Response
                            return Balance;
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return 0;
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return 0;
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountBalance", _Exception, UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }

        ORewardBalance _ORewardBalance;
        /// <summary>
        /// Description: Gets the application user balance summary.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>ORewardBalance.</returns>
        internal ORewardBalance GetAppUserBalanceSummary(long UserAccountId, OUserReference UserReference)
        {

            _ORewardBalance = new ORewardBalance();
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.TUC &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;

                    double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.TUC &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                    DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                                        && (x.Type.Value == TransactionTypeValue.Reward
                                                                                                            || x.Type.Value == TransactionTypeValue.Redeem
                                                                                                            || x.Type.Value == TransactionSource.PaymentsS))
                                                                        .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                    if (LastTransactionDate != null)
                    {
                        int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                        if (DaysDifference < 91)
                        {
                            _ORewardBalance.Credit = Math.Round(TotalCredit, 2);
                            _ORewardBalance.Debit = Math.Round(TotalDebit, 2);
                            _ORewardBalance.Balance = Math.Round(Balance, 2);
                            return _ORewardBalance;
                        }
                        else
                        {
                            _ORewardBalance.Credit = 0;
                            _ORewardBalance.Debit = 0;
                            _ORewardBalance.Balance = 0;
                            return _ORewardBalance;
                        }
                    }
                    else
                    {
                        _ORewardBalance.Credit = 0;
                        _ORewardBalance.Debit = 0;
                        _ORewardBalance.Balance = 0;
                        return _ORewardBalance;
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalanceSummary", _Exception, UserReference);
                #endregion
                #region Send Response
                _ORewardBalance.Credit = 0;
                _ORewardBalance.Debit = 0;
                _ORewardBalance.Balance = 0;
                return _ORewardBalance;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the application user thank u cash plus balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserThankUCashPlusBalance(long UserAccountId, OUserReference UserReference)
        {

            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.ThankUCashPlus &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;

                    double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == Helpers.TransactionSource.ThankUCashPlus &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == StatusHelperId.Transaction_Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                    DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                                    && x.SourceId == Helpers.TransactionSource.ThankUCashPlus
                                                                                                        ).OrderByDescending(x => x.TransactionDate)
                                                                                        .Select(x => x.TransactionDate).FirstOrDefault();

                    if (LastTransactionDate != null)
                    {
                        int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                        if (DaysDifference < 91)
                        {
                            #region Send Response
                            return Balance;
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return 0;
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return 0;
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountBalance", _Exception, UserReference);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransaction(OTransaction.Details _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD013");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        OTransaction.Details Data = (from n in _HCoreContext.HCUAccountTransaction
                                                     where n.Guid == _Request.ReferenceKey
                                                     select new OTransaction.Details
                                                     {
                                                         ReferenceId = n.Id,

                                                         ReferenceKey = n.Guid,
                                                         InoviceNumber = n.InoviceNumber,


                                                         UserAccountKey = n.Account.Guid,
                                                         UserAccountName = n.Account.DisplayName,
                                                         UserAccountContactNumber = n.Account.ContactNumber,
                                                         UserAccountEmailAddress = n.Account.EmailAddress,

                                                         UserAccountTypeCode = n.Account.AccountType.SystemName,
                                                         UserAccountTypeName = n.Account.AccountType.Name,

                                                         ModeKey = n.Mode.SystemName,
                                                         ModeName = n.Mode.Name,

                                                         SourceKey = n.Source.SystemName,
                                                         SourceName = n.Source.Name,

                                                         TypeKey = n.Type.SystemName,
                                                         TypeValue = n.Type.Value,
                                                         TypeName = n.Type.Name,

                                                         //AmountType = n.AmountType,

                                                         //AmountPercentage = n.AmountPercentage,
                                                         Amount = n.Amount,
                                                         Charge = n.Charge,

                                                         //ComissionPercentage = n.ComissionPercentage,
                                                         ComissionAmount = n.ComissionAmount,

                                                         //DiscountPercentage = n.DiscountPercentage,
                                                         //DiscountAmount = n.DiscountAmount,

                                                         //PayableAmount = n.PayableAmount,
                                                         TotalAmount = n.TotalAmount,

                                                         Balance = n.Balance,

                                                         //PointsValue = n.PointsValue,
                                                         //ConversionAmount = n.ConversionAmount,
                                                         PurchaseAmount = n.PurchaseAmount,
                                                         TransactionDate = n.TransactionDate,

                                                         AccountNumber = n.AccountNumber,

                                                         ParentTransactionKey = n.ParentTransaction.Guid,
                                                         ReferenceNumber = n.ReferenceNumber,

                                                         RefTransactionKey = n.ParentTransaction.Guid,
                                                         RefTransactionTotalAmount = n.ParentTransaction.TotalAmount,
                                                         //RefTransactionConversionAmount = n.ParentTransaction.ConversionAmount,
                                                         RefTransactionComission = n.ParentTransaction.ComissionAmount,
                                                         //RefTransactionComissionPercentage = n.ParentTransaction.ComissionPercentage,

                                                         ParentKey = n.Parent.Guid,
                                                         ParentName = n.Parent.DisplayName,


                                                         OwnerKey = n.CreatedBy.Owner.Guid,
                                                         OwnerDisplayName = n.CreatedBy.Owner.DisplayName,
                                                         OwnerAddress = n.CreatedBy.Owner.Address,

                                                         OwnerParentKey = n.CreatedBy.Owner.Owner.Guid,
                                                         OwnerParentName = n.CreatedBy.Owner.Owner.DisplayName,

                                                         //Description = n.Description,
                                                         //Data = n.Data,


                                                         //Latitude = n.Session.Latitude,
                                                         //Longitude = n.Session.Longitude,

                                                         CreateDate = n.CreateDate,
                                                         CreatedByKey = n.CreatedBy.Guid,
                                                         CreatedByDisplayName = n.CreatedBy.DisplayName,

                                                         CreatedByAccountTypeName = n.CreatedBy.AccountType.Name,
                                                         CreatedByAccountTypeCode = n.CreatedBy.AccountType.SystemName,



                                                         ModifyDate = n.ModifyDate,

                                                         GroupKey = n.GroupKey,

                                                         Status = n.StatusId,
                                                         StatusCode = n.Status.SystemName,
                                                         StatusName = n.Status.Name,
                                                         RequestKey = n.RequestKey,
                                                     }).FromCache().FirstOrDefault();
                        if (Data != null)
                        {

                            if (!string.IsNullOrEmpty(Data.GroupKey))
                            {
                                Data.RelatedTransactions = (from n in _HCoreContext.HCUAccountTransaction
                                                            where n.GroupKey == Data.GroupKey
                                                            select new OTransaction.Details
                                                            {
                                                                ReferenceId = n.Id,

                                                                ReferenceKey = n.Guid,
                                                                InoviceNumber = n.InoviceNumber,

                                                                UserAccountKey = n.Account.Guid,
                                                                UserAccountName = n.Account.DisplayName,
                                                                UserAccountContactNumber = n.Account.ContactNumber,
                                                                UserAccountEmailAddress = n.Account.EmailAddress,

                                                                UserAccountTypeCode = n.Account.AccountType.SystemName,
                                                                UserAccountTypeName = n.Account.AccountType.Name,

                                                                ModeKey = n.Mode.SystemName,
                                                                ModeName = n.Mode.Name,

                                                                SourceKey = n.Source.SystemName,
                                                                SourceName = n.Source.Name,

                                                                TypeKey = n.Type.SystemName,
                                                                TypeValue = n.Type.Value,
                                                                TypeName = n.Type.Name,

                                                                //AmountType = n.AmountType,

                                                                //AmountPercentage = n.AmountPercentage,
                                                                Amount = n.Amount,
                                                                Charge = n.Charge,

                                                                //ComissionPercentage = n.ComissionPercentage,
                                                                ComissionAmount = n.ComissionAmount,

                                                                //DiscountPercentage = n.DiscountPercentage,
                                                                //DiscountAmount = n.DiscountAmount,

                                                                //PayableAmount = n.PayableAmount,
                                                                TotalAmount = n.TotalAmount,

                                                                Balance = n.Balance,

                                                                //PointsValue = n.PointsValue,
                                                                //ConversionAmount = n.ConversionAmount,
                                                                PurchaseAmount = n.PurchaseAmount,
                                                                TransactionDate = n.TransactionDate,

                                                                AccountNumber = n.AccountNumber,

                                                                ParentKey = n.Parent.Guid,
                                                                ParentName = n.Parent.DisplayName,
                                                                ParentTransactionKey = n.ParentTransaction.Guid,
                                                                ReferenceNumber = n.ReferenceNumber,

                                                                RefTransactionKey = n.ParentTransaction.Guid,
                                                                RefTransactionTotalAmount = n.ParentTransaction.TotalAmount,
                                                                //RefTransactionConversionAmount = n.ParentTransaction.ConversionAmount,
                                                                RefTransactionComission = n.ParentTransaction.ComissionAmount,
                                                                //RefTransactionComissionPercentage = n.ParentTransaction.ComissionPercentage,

                                                                OwnerKey = n.CreatedBy.Owner.Guid,
                                                                OwnerDisplayName = n.CreatedBy.Owner.DisplayName,
                                                                OwnerAddress = n.CreatedBy.Owner.Address,

                                                                OwnerParentKey = n.CreatedBy.Owner.Owner.Guid,
                                                                OwnerParentName = n.CreatedBy.Owner.Owner.DisplayName,

                                                                //Description = n.Description,
                                                                //Data = n.Data,


                                                                //Latitude = n.Session.Latitude,
                                                                //Longitude = n.Session.Longitude,

                                                                CreateDate = n.CreateDate,
                                                                CreatedByKey = n.CreatedBy.Guid,
                                                                CreatedByDisplayName = n.CreatedBy.DisplayName,

                                                                CreatedByAccountTypeName = n.CreatedBy.AccountType.Name,
                                                                CreatedByAccountTypeCode = n.CreatedBy.AccountType.SystemName,



                                                                ModifyDate = n.ModifyDate,


                                                                GroupKey = n.GroupKey,

                                                                Status = n.StatusId,
                                                                StatusCode = n.Status.SystemName,
                                                                StatusName = n.Status.Name,

                                                            }).ToList();
                            }
                            _HCoreContext.Dispose();
                            if (!string.IsNullOrEmpty(Data.RequestKey))
                            {
                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    Data.RequestLog = (from n in _HCoreContextLogging.HCLCoreUsage
                                                       where n.Guid == Data.RequestKey
                                                       select new OTransaction.RequestLog
                                                       {
                                                           Request = n.Request,
                                                           Response = n.Response,
                                                       }).FirstOrDefault();
                                    _HCoreContextLogging.Dispose();
                                }

                            }

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC002");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactions(OList.Request _Request)
        {
            if (_Request.ReferenceId == 1)
            {
                return GetTransactionsSum(_Request);
            }
            else
            {
                #region Manage Exception
                try
                {
                    #region Add Default Request
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        #region Default Conditions
                        HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                        #endregion
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        #region Default Conditions
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                        #endregion
                    }
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = 10;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;
                    }
                    #endregion
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.Type == ListType.Rewards)
                        {
                            int TotalRecords = 0;
                            List<OTransaction.List> Data = new List<OTransaction.List>();
                            #region Rewards All
                            if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                            {
                                TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                where x.Type.Value == "reward"
                                                && x.SourceId == TransactionSource.TUC
                                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ModeId == TransactionMode.Credit
                                                && _HCoreContext.HCUAccountTransaction
                                                    .Where(a =>
                                                           a.Account.Guid == _Request.ReferenceKey
                                                           && a.GroupKey == x.GroupKey
                                                           && a.ModeId == TransactionMode.Credit
                                                           && a.SourceId == TransactionSource.Settlement
                                                          ).Count() > 0
                                                select new OTransaction.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    UserKey = x.Account.Guid,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserAccountDisplayName = x.Account.DisplayName,
                                                    UserAccountMobileNumber = x.Account.MobileNumber,
                                                    TypeKey = x.Type.SystemName,
                                                    TypeName = x.Type.Name,


                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    ParentKey = x.Parent.Guid,
                                                    ParentDisplayName = x.Parent.DisplayName,

                                                    SubParentKey = x.SubParent.Guid,
                                                    SubParentDisplayName = x.SubParent.DisplayName,

                                                    CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                    CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,

                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                    ParentTransactionAmount = x.ParentTransaction.Amount,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBinNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,

                                                    RewardAmount = x.ParentTransaction.TotalAmount,

                                                    UserAmount = x.TotalAmount,

                                                    CommissionAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(a =>
                                                                  a.Account.Guid == _Request.ReferenceKey
                                                                  && a.GroupKey == x.GroupKey
                                                                  && a.ModeId == TransactionMode.Credit
                                                                  && a.SourceId == TransactionSource.Settlement)
                                                           .Select(a => a.TotalAmount).FirstOrDefault(),
                                                    AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),
                                                })
                              .Where(_Request.SearchCondition).FromCache().Count();
                                Data = (from x in _HCoreContext.HCUAccountTransaction
                                        where x.Type.Value == "reward"
                                        && x.SourceId == TransactionSource.TUC
                                        && x.Account.AccountTypeId == UserAccountType.Appuser
                                        && x.ModeId == TransactionMode.Credit
                                         && _HCoreContext.HCUAccountTransaction
                                                    .Where(a =>
                                                           a.Account.Guid == _Request.ReferenceKey
                                                           && a.GroupKey == x.GroupKey
                                                           && a.ModeId == TransactionMode.Credit
                                                           && a.SourceId == TransactionSource.Settlement
                                                          ).Count() > 0
                                        select new OTransaction.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            UserKey = x.Account.Guid,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountMobileNumber = x.Account.MobileNumber,


                                            TypeKey = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            CardBankName = x.CardBank.Name,
                                            CardBrandName = x.CardBrand.Name,
                                            CardBinNumber = x.AccountNumber,

                                            InvoiceAmount = x.PurchaseAmount,
                                            TransactionDate = x.TransactionDate,

                                            ParentKey = x.Parent.Guid,
                                            ParentDisplayName = x.Parent.DisplayName,

                                            SubParentKey = x.SubParent.Guid,
                                            SubParentDisplayName = x.SubParent.DisplayName,

                                            CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                            CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,

                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                            ParentTransactionAmount = x.ParentTransaction.Amount,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,

                                            ReferenceNumber = x.ReferenceNumber,

                                            RewardAmount = x.ParentTransaction.TotalAmount,

                                            OwnerId = x.Account.OwnerId,
                                            UserAmount = x.TotalAmount,
                                            GroupKey = x.GroupKey,

                                            CommissionAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(a =>
                                                                  a.Account.Guid == _Request.ReferenceKey
                                                                  && a.GroupKey == x.GroupKey
                                                                  && a.ModeId == TransactionMode.Credit
                                                                  && a.SourceId == TransactionSource.Settlement)
                                                           .Select(a => a.TotalAmount).FirstOrDefault(),
                                            AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),
                                        })
                                                       .Where(_Request.SearchCondition)
                                                       .OrderBy(_Request.SortExpression)
                                                       .Skip(_Request.Offset)
                                                       .Take(_Request.Limit)
                                                       .FromCache().ToList();
                            }
                            else
                            {
                                TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                where x.Type.Value == "reward"
                                                && x.SourceId == TransactionSource.TUC
                                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ModeId == TransactionMode.Credit
                                                select new OTransaction.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    UserKey = x.Account.Guid,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserAccountDisplayName = x.Account.DisplayName,
                                                    UserAccountMobileNumber = x.Account.MobileNumber,


                                                    TypeKey = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBinNumber = x.AccountNumber,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    ParentKey = x.Parent.Guid,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentKey = x.SubParent.Guid,
                                                    SubParentDisplayName = x.SubParent.DisplayName,

                                                    CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                    CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,

                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                    ParentTransactionAmount = x.ParentTransaction.Amount,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    ReferenceNumber = x.ReferenceNumber,

                                                    RewardAmount = x.ParentTransaction.TotalAmount,

                                                    UserAmount = x.TotalAmount,

                                                    AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),
                                                })
                              .Where(_Request.SearchCondition).FromCache().Count();
                                Data = (from x in _HCoreContext.HCUAccountTransaction
                                        where x.Type.Value == "reward"
                                        && x.SourceId == TransactionSource.TUC
                                        && x.Account.AccountTypeId == UserAccountType.Appuser
                                        && x.ModeId == TransactionMode.Credit
                                        select new OTransaction.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            UserKey = x.Account.Guid,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountMobileNumber = x.Account.MobileNumber,


                                            TypeKey = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            CardBankName = x.CardBank.Name,
                                            CardBrandName = x.CardBrand.Name,
                                            CardBinNumber = x.AccountNumber,
                                            InvoiceAmount = x.PurchaseAmount,
                                            TransactionDate = x.TransactionDate,

                                            ParentKey = x.Parent.Guid,
                                            ParentDisplayName = x.Parent.DisplayName,
                                            SubParentKey = x.SubParent.Guid,
                                            SubParentDisplayName = x.SubParent.DisplayName,

                                            CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                            CreatedByOwnerDisplayName = x.CreatedBy.Owner.DisplayName,

                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                            ParentTransactionAmount = x.ParentTransaction.Amount,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,

                                            ReferenceNumber = x.ReferenceNumber,

                                            RewardAmount = x.ParentTransaction.TotalAmount,

                                            OwnerId = x.Account.OwnerId,
                                            UserAmount = x.TotalAmount,
                                            GroupKey = x.GroupKey,
                                            AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),


                                        })
                                                       .Where(_Request.SearchCondition)
                                                       .OrderBy(_Request.SortExpression)
                                                       .Skip(_Request.Offset)
                                                       .Take(_Request.Limit)
                                                       .FromCache().ToList();
                            }
                            #endregion
                            if (Data != null)
                            {
                                if (_Request.UserReference.AccountTypeId == UserAccountType.Admin || _Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    foreach (var DataItem in Data)
                                    {
                                        DataItem.ThankUAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(a =>
                                                     a.AccountId == ThankUAccountId
                                                     && a.GroupKey == DataItem.GroupKey
                                                     && a.ModeId == TransactionMode.Credit
                                                     && a.SourceId == TransactionSource.Settlement)
                                            .Select(a => a.TotalAmount).FirstOrDefault();

                                        DataItem.AcquirerAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(a =>
                                                      a.Account.AccountTypeId == UserAccountType.Acquirer
                                                      && a.GroupKey == DataItem.GroupKey
                                                      && a.ModeId == TransactionMode.Credit
                                                      && a.SourceId == TransactionSource.Settlement)
                                            .Select(a => a.TotalAmount).FirstOrDefault();

                                        DataItem.IssuerAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(a =>
                                                      a.AccountId == DataItem.OwnerId
                                                      && a.GroupKey == DataItem.GroupKey
                                                      && a.ModeId == TransactionMode.Credit
                                                      && a.SourceId == TransactionSource.Settlement)
                                            .Select(a => a.TotalAmount).FirstOrDefault();


                                        DataItem.PSSPAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(a =>
                                                      (a.Account.AccountTypeId == UserAccountType.PgAccount || a.Account.AccountTypeId == UserAccountType.PosAccount)
                                                      && a.GroupKey == DataItem.GroupKey
                                                      && a.ModeId == TransactionMode.Credit
                                                      && a.SourceId == TransactionSource.Settlement)
                                            .Select(a => a.TotalAmount).FirstOrDefault();
                                    }
                                }

                                #region Create DataTable Response Object
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else
                            {
                                #region Create DataTable Response Object
                                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                                #endregion
                            }
                        }
                        else if (_Request.Type == ListType.Payments)
                        {
                            int TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                where
                                                x.SourceId == TransactionSource.Payments
                                                && x.ModeId == TransactionMode.Credit
                                                && x.AccountId != 3
                                                select new OTransactionList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    UserKey = x.Account.Guid,
                                                    UserProfileKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserAccountTypeId = x.Account.AccountTypeId,
                                                    UserAccountMobileNumber = x.Account.MobileNumber,
                                                    TypeKey = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    MerchantKey = x.Parent.Guid,
                                                    MerchantDisplayName = x.Parent.DisplayName,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                    TransactionDate = x.TransactionDate,
                                                    CreateDate = x.CreateDate,
                                                    MerchantAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(a =>
                                                                               a.AccountId == x.ParentId
                                                                               && a.GroupKey == x.GroupKey
                                                                               && a.ModeId == TransactionMode.Credit
                                                                               && a.SourceId == TransactionSource.Settlement
                                                                              )
                                                                        .Select(a => a.TotalAmount).FirstOrDefault(),
                                                    RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(a => a.AccountId == x.AccountId
                                                                               && a.GroupKey == x.GroupKey
                                                                               && a.ModeId == TransactionMode.Credit
                                                                               && a.SourceId == TransactionSource.TUC)
                                                                        .Select(a => a.TotalAmount).FirstOrDefault(),
                                                    ThankUComission = _HCoreContext.HCUAccountTransaction
                                                                        .Where(a =>
                                                                               a.AccountId == 3
                                                                               && a.GroupKey == x.GroupKey
                                                                               && a.ModeId == TransactionMode.Credit
                                                                               && a.SourceId == TransactionSource.Settlement)
                                                                        .Select(a => a.TotalAmount).FirstOrDefault(),
                                                }).Where(_Request.SearchCondition).Count();

                            List<OTransactionList> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                           where
                                                           x.SourceId == TransactionSource.Payments
                                       && x.ModeId == TransactionMode.Credit
                                       && x.AccountId != 3
                                                           select new OTransactionList
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               // InvoiceNumber = x.Description,
                                                               UserKey = x.Account.Guid,
                                                               UserProfileKey = x.Account.Guid,
                                                               UserDisplayName = x.Account.DisplayName,
                                                               UserAccountTypeId = x.Account.AccountTypeId,
                                                               UserAccountMobileNumber = x.Account.MobileNumber,
                                                               TypeKey = x.Type.SystemName,
                                                               TypeName = x.Type.Name,
                                                               Amount = x.Amount,
                                                               Charge = x.Charge,
                                                               TotalAmount = x.TotalAmount,
                                                               MerchantKey = x.Parent.Guid,
                                                               MerchantDisplayName = x.Parent.DisplayName,
                                                               CreatedByKey = x.CreatedBy.Guid,
                                                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                               CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                               TransactionDate = x.TransactionDate,
                                                               CreateDate = x.CreateDate,
                                                               MerchantAmount = x.InverseParentTransaction
                                                                                 .Where(a =>
                                                                      a.AccountId == x.ParentId
                                                                      && a.GroupKey == x.GroupKey
                                                                      && a.ModeId == TransactionMode.Credit
                                                                      && a.SourceId == TransactionSource.Settlement
                                                                     )
                                                               .Select(a => a.TotalAmount).FirstOrDefault(),

                                                               RewardAmount = x.InverseParentTransaction
                                                                                  .Where(a => a.AccountId == x.AccountId
                                                                               && a.GroupKey == x.GroupKey
                                                                               && a.ModeId == TransactionMode.Credit
                                                                               && a.SourceId == TransactionSource.TUC)
                                                                        .Select(a => a.TotalAmount).FirstOrDefault(),

                                                               ThankUComission = x.InverseParentTransaction
                                                                                .Where(a =>
                                                                               a.AccountId == 3
                                                                               && a.GroupKey == x.GroupKey
                                                                               && a.ModeId == TransactionMode.Credit
                                                                               && a.SourceId == TransactionSource.Settlement)
                                                                        .Select(a => a.TotalAmount).FirstOrDefault(),
                                                           })
                                .Where(_Request.SearchCondition)
                            .OrderBy(_Request.SortExpression)
                            .Skip(_Request.Offset)
                            .Take(_Request.Limit)
                            .ToList();

                            if (Data != null)
                            {

                                _OListResponse = new OList.Response();
                                _OListResponse.TotalRecords = TotalRecords;
                                _OListResponse.Data = Data;
                                _OListResponse.Offset = _Request.Offset;
                                _OListResponse.Limit = _Request.Limit;
                                if (Data.Count > 0)
                                {
                                    foreach (var DataItem in Data)
                                    {
                                    }
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OListResponse, "HC0001");
                                #endregion
                            }
                            else
                            {
                                #region Create DataTable Response Object
                                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                                #endregion
                            }
                        }
                        else
                        {
                            int TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                select new OTransaction.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InoviceNumber = x.InoviceNumber,

                                                    UserKey = x.Account.Guid,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserAccountDisplayName = x.Account.DisplayName,
                                                    UserAccountMobileNumber = x.Account.MobileNumber,


                                                    UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                    UserAccountTypeName = x.Account.AccountType.Name,

                                                    ModeKey = x.Mode.SystemName,
                                                    ModeName = x.Mode.Name,

                                                    TypeKey = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    TypeCategory = x.Type.Value,

                                                    SourceKey = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    //ConversionAmount = x.ConversionAmount,

                                                    ParentTransactionAmount = x.ParentTransaction.Amount,
                                                    ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                    SubParentKey = x.SubParent.Guid,
                                                    SubParentDisplayName = x.SubParent.DisplayName,

                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandName = x.CardBrand.Name,

                                                    TransactionDate = x.TransactionDate,

                                                    ParentKey = x.Parent.Guid,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentIconUrl = x.Parent.IconStorage.Path,
                                                    CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                    CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                    CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                    Status = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    //InvoiceId = x.InvoiceId,
                                                    //InvoiceItemId = x.InvoiceItemId,
                                                    AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),
                                                })
                                .Where(_Request.SearchCondition).Count();
                            List<OTransaction.List> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                            select new OTransaction.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                InoviceNumber = x.InoviceNumber,

                                                                UserKey = x.Account.Guid,
                                                                UserAccountKey = x.Account.Guid,
                                                                UserAccountDisplayName = x.Account.DisplayName,
                                                                UserAccountMobileNumber = x.Account.MobileNumber,

                                                                UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                                UserAccountTypeName = x.Account.AccountType.Name,

                                                                ModeKey = x.Mode.SystemName,
                                                                ModeName = x.Mode.Name,

                                                                TypeKey = x.Type.SystemName,
                                                                TypeName = x.Type.Name,
                                                                TypeCategory = x.Type.Value,

                                                                SourceKey = x.Source.SystemName,
                                                                SourceName = x.Source.Name,



                                                                Amount = x.Amount,
                                                                Charge = x.Charge,
                                                                TotalAmount = x.TotalAmount,
                                                                InvoiceAmount = x.PurchaseAmount,
                                                                //ConversionAmount = x.ConversionAmount,

                                                                ParentTransactionAmount = x.ParentTransaction.Amount,
                                                                ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                                TransactionDate = x.TransactionDate,

                                                                ParentKey = x.Parent.Guid,
                                                                ParentDisplayName = x.Parent.DisplayName,
                                                                ParentIconUrl = x.Parent.IconStorage.Path,

                                                                SubParentKey = x.SubParent.Guid,
                                                                SubParentDisplayName = x.SubParent.DisplayName,

                                                                CardBankName = x.CardBank.Name,
                                                                CardBrandName = x.CardBrand.Name,


                                                                CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                Status = x.StatusId,
                                                                StatusName = x.Status.Name,
                                                                ReferenceNumber = x.ReferenceNumber,
                                                                //InvoiceId = x.InvoiceId,
                                                                //InvoiceItemId = x.InvoiceItemId,
                                                                AcquirerName = _HCoreContext.HCUAccountOwner.Where(a => a.AccountId == x.CreatedById && a.Owner.AccountTypeId == UserAccountType.Acquirer).Select(a => a.Owner.DisplayName).FirstOrDefault(),
                                                            })
                            .Where(_Request.SearchCondition)
                            .OrderBy(_Request.SortExpression)
                            .Skip(_Request.Offset)
                            .Take(_Request.Limit)
                            .ToList();


                            if (Data != null)
                            {
                                if (Data.Count > 0)
                                {
                                    foreach (var DataItem in Data)
                                    {
                                        if (!string.IsNullOrEmpty(DataItem.ParentIconUrl))
                                        {
                                            DataItem.ParentIconUrl = HCoreConstant._AppConfig.StorageUrl + DataItem.ParentIconUrl;
                                        }
                                    }
                                }
                                #region Create DataTable Response Object
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else
                            {
                                #region Create DataTable Response Object
                                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                                #endregion
                            }
                        }

                    }
                }
                catch (Exception _Exception)
                {
                    #region Log Bug
                    HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                    #endregion
                    #region Create DataTable Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                    #endregion
                }
                #endregion

            }
        }
        /// <summary>
        /// Description: Gets the transactions sum.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactionsSum(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _OTransactionListSum = new OTransaction.List();
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    #endregion
                }
                #endregion
                #region Set Default Limit
                if (_Request.Limit == -1)
                {
                    _Request.Limit = 10;
                }
                else if (_Request.Limit == 0)
                {
                    _Request.Limit = DefaultLimit;
                }
                #endregion
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == ListType.Rewards)
                    {


                        #region Rewards All
                        if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                        {
                            #region Sum  - Invoice Amount
                            _OTransactionListSum.InvoiceAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                  where x.Type.Value == "reward"
                                                                  && x.SourceId == TransactionSource.TUC
                                                                  && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                  && x.ModeId == TransactionMode.Credit
                                                                  && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                  select new OTransaction.List
                                                                  {
                                                                      ReferenceKey = x.Guid,
                                                                      UserKey = x.Account.Guid,
                                                                      UserAccountKey = x.Account.Guid,
                                                                      UserAccountDisplayName = x.Account.DisplayName,

                                                                      TypeKey = x.Type.SystemName,
                                                                      TypeName = x.Type.Name,

                                                                      InvoiceAmount = x.PurchaseAmount,
                                                                      TransactionDate = x.TransactionDate,

                                                                      ParentKey = x.Parent.Guid,
                                                                      ParentDisplayName = x.Parent.DisplayName,

                                                                      CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                      CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                      CreatedByKey = x.CreatedBy.Guid,
                                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                      CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                      ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                      StatusCode = x.Status.SystemName,

                                                                      ReferenceNumber = x.ReferenceNumber,

                                                                      RewardAmount = x.ParentTransaction.TotalAmount,

                                                                      UserAmount = x.TotalAmount,

                                                                  })
                            .Where(_Request.SearchCondition).Sum(x => x.InvoiceAmount);
                            #endregion
                            #region Sum  - Reward Amount
                            _OTransactionListSum.RewardAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                 where x.Type.Value == "reward"
                                                                 && x.SourceId == TransactionSource.TUC
                                                                 && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                 && x.ModeId == TransactionMode.Credit
                                                                 && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                 select new OTransaction.List
                                                                 {
                                                                     ReferenceKey = x.Guid,
                                                                     UserKey = x.Account.Guid,
                                                                     UserAccountKey = x.Account.Guid,
                                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                                     TypeKey = x.Type.SystemName,
                                                                     TypeName = x.Type.Name,

                                                                     InvoiceAmount = x.PurchaseAmount,
                                                                     TransactionDate = x.TransactionDate,

                                                                     ParentKey = x.Parent.Guid,
                                                                     ParentDisplayName = x.Parent.DisplayName,

                                                                     CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                     CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByKey = x.CreatedBy.Guid,
                                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                     ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                     StatusCode = x.Status.SystemName,

                                                                     ReferenceNumber = x.ReferenceNumber,

                                                                     RewardAmount = x.ParentTransaction.TotalAmount,

                                                                     UserAmount = x.TotalAmount,

                                                                 })
                            .Where(_Request.SearchCondition).Sum(x => x.RewardAmount);
                            #endregion
                            #region Sum  - User Amount
                            _OTransactionListSum.UserAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                               where x.Type.Value == "reward"
                                                               && x.SourceId == TransactionSource.TUC
                                                               && x.Account.AccountTypeId == UserAccountType.Appuser
                                                               && x.ModeId == TransactionMode.Credit
                                                               && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                               select new OTransaction.List
                                                               {
                                                                   ReferenceKey = x.Guid,
                                                                   UserKey = x.Account.Guid,
                                                                   UserAccountKey = x.Account.Guid,
                                                                   UserAccountDisplayName = x.Account.DisplayName,

                                                                   TypeKey = x.Type.SystemName,
                                                                   TypeName = x.Type.Name,

                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   TransactionDate = x.TransactionDate,

                                                                   ParentKey = x.Parent.Guid,
                                                                   ParentDisplayName = x.Parent.DisplayName,

                                                                   CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                   CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                   CreatedByKey = x.CreatedBy.Guid,
                                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                   CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                   ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                   StatusCode = x.Status.SystemName,

                                                                   ReferenceNumber = x.ReferenceNumber,

                                                                   RewardAmount = x.ParentTransaction.TotalAmount,

                                                                   UserAmount = x.TotalAmount,

                                                               })
                            .Where(_Request.SearchCondition).Sum(x => x.UserAmount);
                            #endregion
                            #region Sum  - Thank U Amount
                            _OTransactionListSum.ThankUAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                 where x.Type.Value == "reward"
                                                                 && x.SourceId == TransactionSource.TUC
                                                                 && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                 && x.ModeId == TransactionMode.Credit
                                                                 && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                 select new OTransaction.List
                                                                 {
                                                                     ReferenceKey = x.Guid,
                                                                     UserKey = x.Account.Guid,
                                                                     UserAccountKey = x.Account.Guid,
                                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                                     TypeKey = x.Type.SystemName,
                                                                     TypeName = x.Type.Name,

                                                                     InvoiceAmount = x.PurchaseAmount,
                                                                     TransactionDate = x.TransactionDate,

                                                                     ParentKey = x.Parent.Guid,
                                                                     ParentDisplayName = x.Parent.DisplayName,

                                                                     CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                     CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByKey = x.CreatedBy.Guid,
                                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                     ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                     StatusCode = x.Status.SystemName,

                                                                     ReferenceNumber = x.ReferenceNumber,

                                                                     RewardAmount = x.ParentTransaction.TotalAmount,

                                                                     UserAmount = x.TotalAmount,
                                                                     ThankUAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(a =>
                                                                                   a.AccountId == ThankUAccountId
                                                                                   && a.GroupKey == x.GroupKey
                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                   && a.SourceId == TransactionSource.Settlement)
                                                                                    .Select(a => a.TotalAmount).FirstOrDefault()
                                                                 })
                            .Where(_Request.SearchCondition).Sum(x => x.ThankUAmount);
                            #endregion
                            #region Sum  - Acquirer  Amount
                            _OTransactionListSum.AcquirerAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                   where x.Type.Value == "reward"
                                                                   && x.SourceId == TransactionSource.TUC
                                                                   && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                   && x.ModeId == TransactionMode.Credit
                                                                   && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                   select new OTransaction.List
                                                                   {
                                                                       ReferenceKey = x.Guid,
                                                                       UserKey = x.Account.Guid,
                                                                       UserAccountKey = x.Account.Guid,
                                                                       UserAccountDisplayName = x.Account.DisplayName,

                                                                       TypeKey = x.Type.SystemName,
                                                                       TypeName = x.Type.Name,

                                                                       InvoiceAmount = x.PurchaseAmount,
                                                                       TransactionDate = x.TransactionDate,

                                                                       ParentKey = x.Parent.Guid,
                                                                       ParentDisplayName = x.Parent.DisplayName,

                                                                       CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                       CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                       CreatedByKey = x.CreatedBy.Guid,
                                                                       CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                       CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                       ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                       StatusCode = x.Status.SystemName,

                                                                       ReferenceNumber = x.ReferenceNumber,

                                                                       RewardAmount = x.ParentTransaction.TotalAmount,

                                                                       UserAmount = x.TotalAmount,
                                                                       AcquirerAmount = _HCoreContext.HCUAccountTransaction
                                         .Where(a =>
                                                a.Account.AccountTypeId == UserAccountType.Acquirer
                                                && a.GroupKey == x.GroupKey
                                                && a.ModeId == TransactionMode.Credit
                                                && a.SourceId == TransactionSource.Settlement)
                                      .Select(a => a.TotalAmount).FirstOrDefault()
                                                                   })
                            .Where(_Request.SearchCondition).Sum(x => x.AcquirerAmount);
                            #endregion
                            #region Sum  - Issuer   Amount
                            _OTransactionListSum.IssuerAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                 where x.Type.Value == "reward"
                                                                 && x.SourceId == TransactionSource.TUC
                                                                 && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                 && x.ModeId == TransactionMode.Credit
                                                                 && (_HCoreContext.HCUAccountTransaction
                                                .Where(a =>
                                                       a.Account.Guid == _Request.ReferenceKey
                                                       && a.GroupKey == x.GroupKey
                                                       && a.ModeId == TransactionMode.Credit
                                                       && a.SourceId == TransactionSource.Settlement
                                                      ).Count() > 0)
                                                                 select new OTransaction.List
                                                                 {
                                                                     ReferenceKey = x.Guid,
                                                                     UserKey = x.Account.Guid,
                                                                     UserAccountKey = x.Account.Guid,
                                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                                     TypeKey = x.Type.SystemName,
                                                                     TypeName = x.Type.Name,

                                                                     InvoiceAmount = x.PurchaseAmount,
                                                                     TransactionDate = x.TransactionDate,

                                                                     ParentKey = x.Parent.Guid,
                                                                     ParentDisplayName = x.Parent.DisplayName,

                                                                     CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                     CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByKey = x.CreatedBy.Guid,
                                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                     ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                     StatusCode = x.Status.SystemName,

                                                                     ReferenceNumber = x.ReferenceNumber,

                                                                     RewardAmount = x.ParentTransaction.TotalAmount,

                                                                     UserAmount = x.TotalAmount,
                                                                     IssuerAmount = _HCoreContext.HCUAccountTransaction
                                     .Where(a =>
                                            a.AccountId == x.ParentId
                                            && a.GroupKey == x.GroupKey
                                            && a.ModeId == TransactionMode.Credit
                                            && a.SourceId == TransactionSource.Settlement)
                                                                                                 .Select(a => a.TotalAmount).FirstOrDefault()
                                                                 })
                            .Where(_Request.SearchCondition).Sum(x => x.IssuerAmount);
                            #endregion
                            #region Sum  - PTSP | PSSP   Amount
                            _OTransactionListSum.PSSPAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                               where x.Type.Value == "reward"
                                                               && x.SourceId == TransactionSource.TUC
                                                               && x.Account.AccountTypeId == UserAccountType.Appuser
                                                               && x.ModeId == TransactionMode.Credit
                                                               && (_HCoreContext.HCUAccountTransaction
                                              .Where(a =>
                                                     a.Account.Guid == _Request.ReferenceKey
                                                     && a.GroupKey == x.GroupKey
                                                     && a.ModeId == TransactionMode.Credit
                                                     && a.SourceId == TransactionSource.Settlement
                                                    ).Count() > 0)
                                                               select new OTransaction.List
                                                               {
                                                                   ReferenceKey = x.Guid,
                                                                   UserKey = x.Account.Guid,
                                                                   UserAccountKey = x.Account.Guid,
                                                                   UserAccountDisplayName = x.Account.DisplayName,

                                                                   TypeKey = x.Type.SystemName,
                                                                   TypeName = x.Type.Name,

                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   TransactionDate = x.TransactionDate,

                                                                   ParentKey = x.Parent.Guid,
                                                                   ParentDisplayName = x.Parent.DisplayName,

                                                                   CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                   CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                   CreatedByKey = x.CreatedBy.Guid,
                                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                   CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                   ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                   StatusCode = x.Status.SystemName,

                                                                   ReferenceNumber = x.ReferenceNumber,

                                                                   RewardAmount = x.ParentTransaction.TotalAmount,

                                                                   UserAmount = x.TotalAmount,
                                                                   PSSPAmount = _HCoreContext.HCUAccountTransaction
                                     .Where(a =>
                                            (a.Account.AccountTypeId == UserAccountType.PgAccount || a.Account.AccountTypeId == UserAccountType.PosAccount)
                                            && a.GroupKey == x.GroupKey
                                            && a.ModeId == TransactionMode.Credit
                                            && a.SourceId == TransactionSource.Settlement)
                                  .Select(a => a.TotalAmount).FirstOrDefault()
                                                               })
                            .Where(_Request.SearchCondition).Sum(x => x.PSSPAmount);
                            #endregion
                        }
                        else
                        {
                            #region Sum  - Invoice Amount
                            _OTransactionListSum.InvoiceAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                  where x.Type.Value == "reward"
                                                                  && x.SourceId == TransactionSource.TUC
                                                                  && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                  && x.ModeId == TransactionMode.Credit
                                                                  select new OTransaction.List
                                                                  {
                                                                      ReferenceKey = x.Guid,
                                                                      UserKey = x.Account.Guid,
                                                                      UserAccountKey = x.Account.Guid,
                                                                      UserAccountDisplayName = x.Account.DisplayName,

                                                                      TypeKey = x.Type.SystemName,
                                                                      TypeName = x.Type.Name,

                                                                      InvoiceAmount = x.PurchaseAmount,
                                                                      TransactionDate = x.TransactionDate,

                                                                      ParentKey = x.Parent.Guid,
                                                                      ParentDisplayName = x.Parent.DisplayName,

                                                                      CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                      CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                      CreatedByKey = x.CreatedBy.Guid,
                                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                      CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                      ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                      StatusCode = x.Status.SystemName,

                                                                      ReferenceNumber = x.ReferenceNumber,

                                                                      RewardAmount = x.ParentTransaction.TotalAmount,

                                                                      UserAmount = x.TotalAmount,

                                                                  }).Select(a => a.InvoiceAmount).Sum();
                            #endregion
                            #region Sum  - Reward Amount
                            _OTransactionListSum.RewardAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                 where x.Type.Value == "reward"
                                                                 && x.SourceId == TransactionSource.TUC
                                                                 && x.Account.AccountTypeId == UserAccountType.Appuser
                                                                 && x.ModeId == TransactionMode.Credit

                                                                 select new OTransaction.List
                                                                 {
                                                                     ReferenceKey = x.Guid,
                                                                     UserKey = x.Account.Guid,
                                                                     UserAccountKey = x.Account.Guid,
                                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                                     TypeKey = x.Type.SystemName,
                                                                     TypeName = x.Type.Name,

                                                                     InvoiceAmount = x.PurchaseAmount,
                                                                     TransactionDate = x.TransactionDate,

                                                                     ParentKey = x.Parent.Guid,
                                                                     ParentDisplayName = x.Parent.DisplayName,

                                                                     CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                     CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByKey = x.CreatedBy.Guid,
                                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                     ParentTransactionAmount = x.ParentTransaction.Amount,

                                                                     StatusCode = x.Status.SystemName,

                                                                     ReferenceNumber = x.ReferenceNumber,

                                                                     RewardAmount = x.ParentTransaction.TotalAmount,

                                                                     UserAmount = x.TotalAmount,

                                                                 })
                            .Where(_Request.SearchCondition).Select(x => x.RewardAmount ?? 0).DefaultIfEmpty(0).Sum();
                            #endregion
                            //#region Sum  - User Amount
                            //_OTransactionListSum.UserAmount = (from x in _HCoreContext.HCUAccountTransaction
                            //                                   where x.Type.Value == "reward"
                            //                                   && x.SourceId == TransactionSource.TUC
                            //                                   && (x.Account.AccountTypeId == UserAccountType.Appuser
                            //                                       || x.Account.AccountTypeId == UserAccountType.Carduser)
                            //                                   && x.ModeId == TransactionMode.Credit
                            //                                   select new OTransaction.List
                            //                                   {
                            //                                       ReferenceKey = x.Guid,
                            //                                       UserKey = x.Account.Guid,
                            //                                       UserAccountKey = x.Account.Guid,
                            //                                       UserAccountDisplayName = x.Account.DisplayName,
                            //                                       TypeKey = x.Type.SystemName,
                            //                                       TypeName = x.Type.Name,
                            //                                       InvoiceAmount = x.PurchaseAmount,
                            //                                       TransactionDate = x.TransactionDate,
                            //                                       ParentKey = x.Parent.Guid,
                            //                                       ParentDisplayName = x.Parent.DisplayName,
                            //                                       CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                            //                                       CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,
                            //                                       CreatedByKey = x.CreatedBy.Guid,
                            //                                       CreatedByDisplayName = x.CreatedBy.DisplayName,
                            //                                       CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                            //                                       ParentTransactionAmount = x.ParentTransaction.Amount,
                            //                                       StatusCode = x.Status.SystemName,
                            //                                       ReferenceNumber = x.ReferenceNumber,
                            //                                       RewardAmount = x.ParentTransaction.TotalAmount,

                            //                                       UserAmount = x.TotalAmount,

                            //                                   })
                            //.Where(_Request.SearchCondition).Select(x => x.UserAmount).Sum();
                            //#endregion
                            //#region Sum  - Thank U Amount
                            //_OTransactionListSum.ThankUAmount = (from x in _HCoreContext.HCUAccountTransaction
                            //                                     where x.Type.Value == "reward"
                            //                                     && x.SourceId == TransactionSource.TUC
                            //                                     && (x.Account.AccountTypeId == UserAccountType.Appuser
                            //                                         || x.Account.AccountTypeId == UserAccountType.Carduser)
                            //                                     && x.ModeId == TransactionMode.Credit

                            //                                     select new OTransaction.List
                            //                                     {
                            //                                         ReferenceKey = x.Guid,
                            //                                         UserKey = x.Account.Guid,
                            //                                         UserAccountKey = x.Account.Guid,
                            //                                         UserAccountDisplayName = x.Account.DisplayName,

                            //                                         TypeKey = x.Type.SystemName,
                            //                                         TypeName = x.Type.Name,

                            //                                         InvoiceAmount = x.PurchaseAmount,
                            //                                         TransactionDate = x.TransactionDate,

                            //                                         ParentKey = x.Parent.Guid,
                            //                                         ParentDisplayName = x.Parent.DisplayName,

                            //                                         CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                            //                                         CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                            //                                         CreatedByKey = x.CreatedBy.Guid,
                            //                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                            //                                         CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                            //                                         ParentTransactionAmount = x.ParentTransaction.Amount,

                            //                                         StatusCode = x.Status.SystemName,

                            //                                         ReferenceNumber = x.ReferenceNumber,

                            //                                         RewardAmount = x.ParentTransaction.TotalAmount,

                            //                                         UserAmount = x.TotalAmount,
                            //                                         GroupKey = x.GroupKey,
                            //                                         ThankUAmount = _HCoreContext.HCUAccountTransaction
                            //                                                        .Where(a =>
                            //                                                       a.AccountId == ThankUAccountId
                            //                                                       && a.GroupKey == x.GroupKey
                            //                                                       && a.ModeId == TransactionMode.Credit
                            //                                                       && a.SourceId == TransactionSource.Settlement)
                            //                                                        .Select(a => a.TotalAmount).FirstOrDefault(),
                            //                                     })
                            //    .Where(_Request.SearchCondition).Select(x => x.ThankUAmount).Sum();
                            //#endregion
                            //#region Sum  - Acquirer  Amount
                            //_OTransactionListSum.AcquirerAmount = (from x in _HCoreContext.HCUAccountTransaction
                            //                                       where x.Type.Value == "reward"
                            //                                       && x.SourceId == TransactionSource.TUC
                            //                                       && (x.Account.AccountTypeId == UserAccountType.Appuser
                            //                                           || x.Account.AccountTypeId == UserAccountType.Carduser)
                            //                                       && x.ModeId == TransactionMode.Credit

                            //                                       select new OTransaction.List
                            //                                       {
                            //                                           ReferenceKey = x.Guid,
                            //                                           UserKey = x.Account.Guid,
                            //                                           UserAccountKey = x.Account.Guid,
                            //                                           UserAccountDisplayName = x.Account.DisplayName,

                            //                                           TypeKey = x.Type.SystemName,
                            //                                           TypeName = x.Type.Name,

                            //                                           InvoiceAmount = x.PurchaseAmount,
                            //                                           TransactionDate = x.TransactionDate,

                            //                                           ParentKey = x.Parent.Guid,
                            //                                           ParentDisplayName = x.Parent.DisplayName,

                            //                                           CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                            //                                           CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                            //                                           CreatedByKey = x.CreatedBy.Guid,
                            //                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                            //                                           CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                            //                                           ParentTransactionAmount = x.ParentTransaction.Amount,

                            //                                           StatusCode = x.Status.SystemName,

                            //                                           ReferenceNumber = x.ReferenceNumber,

                            //                                           RewardAmount = x.ParentTransaction.TotalAmount,

                            //                                           UserAmount = x.TotalAmount,
                            //                                           AcquirerAmount = _HCoreContext.HCUAccountTransaction
                            //             .Where(a =>
                            //                    a.Account.AccountTypeId == UserAccountType.Acquirer
                            //                    && a.GroupKey == x.GroupKey
                            //                    && a.ModeId == TransactionMode.Credit
                            //                    && a.SourceId == TransactionSource.Settlement)
                            //          .Select(a => a.TotalAmount).FirstOrDefault()
                            //                                       })
                            //.Where(_Request.SearchCondition).Select(x => x.AcquirerAmount).Sum();
                            //#endregion
                            //#region Sum  - Issuer Amount
                            //_OTransactionListSum.IssuerAmount = (from x in _HCoreContext.HCUAccountTransaction
                            //                                     where x.Type.Value == "reward"
                            //                                     && x.SourceId == TransactionSource.TUC
                            //                                     && (x.Account.AccountTypeId == UserAccountType.Appuser
                            //                                         || x.Account.AccountTypeId == UserAccountType.Carduser)
                            //                                     && x.ModeId == TransactionMode.Credit

                            //                                     select new OTransaction.List
                            //                                     {
                            //                                         ReferenceKey = x.Guid,
                            //                                         UserKey = x.Account.Guid,
                            //                                         UserAccountKey = x.Account.Guid,
                            //                                         UserAccountDisplayName = x.Account.DisplayName,

                            //                                         TypeKey = x.Type.SystemName,
                            //                                         TypeName = x.Type.Name,

                            //                                         InvoiceAmount = x.PurchaseAmount,
                            //                                         TransactionDate = x.TransactionDate,

                            //                                         ParentKey = x.Parent.Guid,
                            //                                         ParentDisplayName = x.Parent.DisplayName,

                            //                                         CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                            //                                         CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                            //                                         CreatedByKey = x.CreatedBy.Guid,
                            //                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                            //                                         CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                            //                                         ParentTransactionAmount = x.ParentTransaction.Amount,

                            //                                         StatusCode = x.Status.SystemName,

                            //                                         ReferenceNumber = x.ReferenceNumber,

                            //                                         RewardAmount = x.ParentTransaction.TotalAmount,

                            //                                         UserAmount = x.TotalAmount,
                            //                                         IssuerAmount = _HCoreContext.HCUAccountTransaction
                            //         .Where(a =>
                            //                a.AccountId == x.ParentId
                            //                && a.GroupKey == x.GroupKey
                            //                && a.ModeId == TransactionMode.Credit
                            //                && a.SourceId == TransactionSource.Settlement)
                            //                                                                     .Select(a => a.TotalAmount).FirstOrDefault()
                            //                                     })
                            //.Where(_Request.SearchCondition).Select(x => x.IssuerAmount).Sum();
                            //#endregion
                            //#region Sum  - PTSP | PSSP   Amount
                            //_OTransactionListSum.PSSPAmount = (from x in _HCoreContext.HCUAccountTransaction
                            //                                   where x.Type.Value == "reward"
                            //                                   && x.SourceId == TransactionSource.TUC
                            //                                   && (x.Account.AccountTypeId == UserAccountType.Appuser
                            //                                       || x.Account.AccountTypeId == UserAccountType.Carduser)
                            //                                   && x.ModeId == TransactionMode.Credit

                            //                                   select new OTransaction.List
                            //                                   {
                            //                                       ReferenceKey = x.Guid,
                            //                                       UserKey = x.Account.Guid,
                            //                                       UserAccountKey = x.Account.Guid,
                            //                                       UserAccountDisplayName = x.Account.DisplayName,

                            //                                       TypeKey = x.Type.SystemName,
                            //                                       TypeName = x.Type.Name,

                            //                                       InvoiceAmount = x.PurchaseAmount,
                            //                                       TransactionDate = x.TransactionDate,

                            //                                       ParentKey = x.Parent.Guid,
                            //                                       ParentDisplayName = x.Parent.DisplayName,

                            //                                       CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                            //                                       CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                            //                                       CreatedByKey = x.CreatedBy.Guid,
                            //                                       CreatedByDisplayName = x.CreatedBy.DisplayName,

                            //                                       CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                            //                                       ParentTransactionAmount = x.ParentTransaction.Amount,

                            //                                       StatusCode = x.Status.SystemName,

                            //                                       ReferenceNumber = x.ReferenceNumber,

                            //                                       RewardAmount = x.ParentTransaction.TotalAmount,

                            //                                       UserAmount = x.TotalAmount,
                            //                                       PSSPAmount = _HCoreContext.HCUAccountTransaction
                            //         .Where(a =>
                            //                (a.Account.AccountTypeId == UserAccountType.PgAccount || a.Account.AccountTypeId == UserAccountType.PosAccount)
                            //                && a.GroupKey == x.GroupKey
                            //                && a.ModeId == TransactionMode.Credit
                            //                && a.SourceId == TransactionSource.Settlement)
                            //      .Select(a => a.TotalAmount).FirstOrDefault()
                            //                                   })
                            //.Where(_Request.SearchCondition).Select(x => x.PSSPAmount).Sum();
                            //#endregion
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionListSum, "HC0001");
                        #endregion

                    }
                    else if (_Request.Type == ListType.Payments)
                    {
                        int TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                            where
                                            x.SourceId == TransactionSource.Payments
                                            && x.ModeId == TransactionMode.Credit
                                            && x.AccountId != 3
                                            select new OTransactionList
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                UserKey = x.Account.Guid,
                                                UserProfileKey = x.Account.Guid,
                                                UserDisplayName = x.Account.DisplayName,
                                                UserAccountTypeId = x.Account.AccountTypeId,
                                                TypeKey = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                                Amount = x.Amount,
                                                Charge = x.Charge,
                                                TotalAmount = x.TotalAmount,
                                                MerchantKey = x.Parent.Guid,
                                                MerchantDisplayName = x.Parent.DisplayName,
                                                CreatedByKey = x.CreatedBy.Guid,
                                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                TransactionDate = x.TransactionDate,
                                                CreateDate = x.CreateDate,
                                                MerchantAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(a =>
                                                                           a.AccountId == x.ParentId
                                                                           && a.GroupKey == x.GroupKey
                                                                           && a.ModeId == TransactionMode.Credit
                                                                           && a.SourceId == TransactionSource.Settlement
                                                                          )
                                                                    .Select(a => a.TotalAmount).FirstOrDefault(),
                                                RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(a => a.AccountId == x.AccountId
                                                                           && a.GroupKey == x.GroupKey
                                                                           && a.ModeId == TransactionMode.Credit
                                                                           && a.SourceId == TransactionSource.TUC)
                                                                    .Select(a => a.TotalAmount).FirstOrDefault(),
                                                ThankUComission = _HCoreContext.HCUAccountTransaction
                                                                    .Where(a =>
                                                                           a.AccountId == 3
                                                                           && a.GroupKey == x.GroupKey
                                                                           && a.ModeId == TransactionMode.Credit
                                                                           && a.SourceId == TransactionSource.Settlement)
                                                                    .Select(a => a.TotalAmount).FirstOrDefault(),
                                            }).Where(_Request.SearchCondition).Count();

                        List<OTransactionList> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                       where
                                                       x.SourceId == TransactionSource.Payments
                                   && x.ModeId == TransactionMode.Credit
                                   && x.AccountId != 3
                                                       select new OTransactionList
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           // InvoiceNumber = x.Description,
                                                           UserKey = x.Account.Guid,
                                                           UserProfileKey = x.Account.Guid,
                                                           UserDisplayName = x.Account.DisplayName,
                                                           UserAccountTypeId = x.Account.AccountTypeId,
                                                           TypeKey = x.Type.SystemName,
                                                           TypeName = x.Type.Name,
                                                           Amount = x.Amount,
                                                           Charge = x.Charge,
                                                           TotalAmount = x.TotalAmount,
                                                           MerchantKey = x.Parent.Guid,
                                                           MerchantDisplayName = x.Parent.DisplayName,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                           TransactionDate = x.TransactionDate,
                                                           CreateDate = x.CreateDate,
                                                           MerchantAmount = x.InverseParentTransaction
                                                                             .Where(a =>
                                                                  a.AccountId == x.ParentId
                                                                  && a.GroupKey == x.GroupKey
                                                                  && a.ModeId == TransactionMode.Credit
                                                                  && a.SourceId == TransactionSource.Settlement
                                                                 )
                                                           .Select(a => a.TotalAmount).FirstOrDefault(),

                                                           RewardAmount = x.InverseParentTransaction
                                                                              .Where(a => a.AccountId == x.AccountId
                                                                           && a.GroupKey == x.GroupKey
                                                                           && a.ModeId == TransactionMode.Credit
                                                                           && a.SourceId == TransactionSource.TUC)
                                                                    .Select(a => a.TotalAmount).FirstOrDefault(),

                                                           ThankUComission = x.InverseParentTransaction
                                                                            .Where(a =>
                                                                           a.AccountId == 3
                                                                           && a.GroupKey == x.GroupKey
                                                                           && a.ModeId == TransactionMode.Credit
                                                                           && a.SourceId == TransactionSource.Settlement)
                                                                    .Select(a => a.TotalAmount).FirstOrDefault(),
                                                       })
                            .Where(_Request.SearchCondition)
                        .OrderBy(_Request.SortExpression)
                        .Skip(_Request.Offset)
                        .Take(_Request.Limit)
                        .ToList();

                        if (Data != null)
                        {

                            _OListResponse = new OList.Response();
                            _OListResponse.TotalRecords = TotalRecords;
                            _OListResponse.Data = Data;
                            _OListResponse.Offset = _Request.Offset;
                            _OListResponse.Limit = _Request.Limit;
                            if (Data.Count > 0)
                            {

                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OListResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Create DataTable Response Object
                            OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Sum Amount
                        _OTransactionListSum.TotalAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                            select new OTransaction.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                InoviceNumber = x.InoviceNumber,

                                                                UserKey = x.Account.Guid,
                                                                UserAccountKey = x.Account.Guid,
                                                                UserAccountDisplayName = x.Account.DisplayName,

                                                                UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                                UserAccountTypeName = x.Account.AccountType.Name,

                                                                ModeKey = x.Mode.SystemName,
                                                                ModeName = x.Mode.Name,

                                                                TypeKey = x.Type.SystemName,
                                                                TypeName = x.Type.Name,
                                                                TypeCategory = x.Type.Value,

                                                                SourceKey = x.Source.SystemName,
                                                                SourceName = x.Source.Name,

                                                                Amount = x.Amount,
                                                                Charge = x.Charge,
                                                                TotalAmount = x.TotalAmount,
                                                                //ConversionAmount = x.ConversionAmount,
                                                                InvoiceAmount = x.PurchaseAmount,

                                                                ParentTransactionAmount = x.ParentTransaction.Amount,
                                                                ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                                TransactionDate = x.TransactionDate,

                                                                ParentKey = x.Parent.Guid,
                                                                ParentDisplayName = x.Parent.DisplayName,

                                                                CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                Status = x.StatusId,
                                                                StatusName = x.Status.Name,
                                                                ReferenceNumber = x.ReferenceNumber,
                                                            })
                       .Where(_Request.SearchCondition)
                            .Sum(x => x.TotalAmount);
                        #endregion
                        #region Sum Amount
                        _OTransactionListSum.ParentTransactionTotalAmount = (from x in _HCoreContext.HCUAccountTransaction
                                                                             select new OTransaction.List
                                                                             {
                                                                                 ReferenceId = x.Id,
                                                                                 ReferenceKey = x.Guid,
                                                                                 InoviceNumber = x.InoviceNumber,

                                                                                 UserKey = x.Account.Guid,
                                                                                 UserAccountKey = x.Account.Guid,
                                                                                 UserAccountDisplayName = x.Account.DisplayName,

                                                                                 UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                                                 UserAccountTypeName = x.Account.AccountType.Name,

                                                                                 ModeKey = x.Mode.SystemName,
                                                                                 ModeName = x.Mode.Name,

                                                                                 TypeKey = x.Type.SystemName,
                                                                                 TypeName = x.Type.Name,
                                                                                 TypeCategory = x.Type.Value,

                                                                                 SourceKey = x.Source.SystemName,
                                                                                 SourceName = x.Source.Name,

                                                                                 Amount = x.Amount,
                                                                                 Charge = x.Charge,
                                                                                 TotalAmount = x.TotalAmount,
                                                                                 //ConversionAmount = x.ConversionAmount,
                                                                                 InvoiceAmount = x.PurchaseAmount,

                                                                                 ParentTransactionAmount = x.ParentTransaction.Amount,
                                                                                 ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                                                 TransactionDate = x.TransactionDate,

                                                                                 ParentKey = x.Parent.Guid,
                                                                                 ParentDisplayName = x.Parent.DisplayName,

                                                                                 CreatedByOwnerKey = x.CreatedBy.Owner.Guid,
                                                                                 CreatedByOwnerDisplayName = x.CreatedBy.DisplayName,

                                                                                 CreatedByKey = x.CreatedBy.Guid,
                                                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                                 CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                                 CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                                 Status = x.StatusId,
                                                                                 StatusName = x.Status.Name,
                                                                                 ReferenceNumber = x.ReferenceNumber,
                                                                             })
                       .Where(_Request.SearchCondition)
                            .Sum(x => x.ParentTransactionTotalAmount);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionListSum, "HC0001");
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transactions overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactionsOverview(OTransaction.Overview.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.UserReference.Task == HCoreTask.Get_MerchantTransactionOverview)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        OTransaction.Overview.Response _OverviewResponse = _HCoreContext.HCCore
                            .Where(x => x.SystemName == _Request.SourceCode)
                            .Select(x => new OTransaction.Overview.Response
                            {
                                ReferenceId = x.Id,
                                Name = x.Name,
                            }).FirstOrDefault();
                        if (_OverviewResponse != null)
                        {
                            _OverviewResponse.Types = _HCoreContext.HCCore
                                .Where(x => x.ParentId == HelperType.TransactionType && x.StatusId == StatusActive)
                                                                                    .Select(x => new OTransactionType
                                                                                    {
                                                                                        ReferenceId = x.Id,
                                                                                        Name = x.Name,
                                                                                    }).ToList();

                            foreach (var TransactionType in _OverviewResponse.Types)
                            {
                                OTransactionModeValue _OTransactionModeValueCredit = new OTransactionModeValue();
                                OTransactionModeValue _OTransactionModeValueDebit = new OTransactionModeValue();

                                _OTransactionModeValueCredit.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                                                .Select(x => (double?)x.Amount).Sum() ?? 0;
                                _OTransactionModeValueCredit.ComissionAmount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                    .Select(x => (double?)x.ComissionAmount).Sum() ?? 0;
                                //_OTransactionModeValueCredit.DiscountAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                //.Select(x => (double?)x.DiscountAmount).Sum() ?? 0;
                                //_OTransactionModeValueCredit.PayableAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                //.Select(x => (double?)x.PayableAmount).Sum() ?? 0;
                                _OTransactionModeValueCredit.TotalAmount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                    .Select(x => (double?)x.TotalAmount).Sum() ?? 0;
                                //_OTransactionModeValueCredit.ConversionAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                //.Select(x => (double?)x.ConversionAmount).Sum() ?? 0;
                                _OTransactionModeValueCredit.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Credit)
                                    .Select(x => (double?)x.PurchaseAmount).Sum() ?? 0;


                                _OTransactionModeValueDebit.Amount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                    .Select(x => (double?)x.Amount).Sum() ?? 0;
                                _OTransactionModeValueDebit.ComissionAmount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                    .Select(x => (double?)x.ComissionAmount).Sum() ?? 0;
                                //_OTransactionModeValueDebit.DiscountAmount = _HCoreContext.HCUAccountTransaction
                                //    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                //    .Select(x => (double?)x.DiscountAmount).Sum() ?? 0;
                                //_OTransactionModeValueDebit.PayableAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                //.Select(x => (double?)x.PayableAmount).Sum() ?? 0;
                                _OTransactionModeValueDebit.TotalAmount = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                    .Select(x => (double?)x.TotalAmount).Sum() ?? 0;

                                //_OTransactionModeValueDebit.ConversionAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                //.Select(x => (double?)x.ConversionAmount).Sum() ?? 0;



                                _OTransactionModeValueDebit.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(x => x.Account.Guid == _Request.UserAccountKey && x.SourceId == _OverviewResponse.ReferenceId && x.TypeId == TransactionType.ReferenceId && x.ModeId == TransactionMode.Debit)
                                                                  .Select(x => (double?)x.PurchaseAmount).Sum() ?? 0;

                                TransactionType.Credit = _OTransactionModeValueCredit;
                                TransactionType.Debit = _OTransactionModeValueDebit;
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OverviewResponse, "HCUAD013");
                        #endregion
                    }
                }
                else
                {

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCUAD013");
                    #endregion
                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
