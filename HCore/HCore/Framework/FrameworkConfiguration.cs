//==================================================================================
// FileName: FrameworkConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to system configuaration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkConfiguration
    {
        #region Entities
        HCoreContext _HCoreContext;
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationValue(OConfigurationValue.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (_HCoreContext = new HCoreContext())
                {
                    OConfigurationValue.Response _Configuration = (from n in _HCoreContext.HCCoreCommon
                                                                   where n.SystemName == _Request.ConfigurationCode && n.StatusId == StatusActive && n.TypeId == HelperType.Configuration
                                                                   select new OConfigurationValue.Response
                                                                   {
                                                                       ReferenceId = n.Id,
                                                                       Value = n.Value,
                                                                       IsDefaultValue = 1,
                                                                       TypeCode = n.Helper.SystemName,
                                                                       TypeName = n.Helper.Name,
                                                                   }).FirstOrDefault();
                    if (_Configuration != null)
                    {
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).FirstOrDefault();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            _Configuration.Value = ConfigurationValue;
                            _Configuration.IsDefaultValue = 0;
                        }
                        if (!string.IsNullOrEmpty(_Request.CountryKey))
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                                where n.ParentId == _Configuration.ReferenceId && n.SubParent.Guid == _Request.CountryKey && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                _Configuration.Value = CountryConfigurationValue;
                                _Configuration.IsDefaultValue = 0;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Configuration, "HC0001");
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetConfigurationValue", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        //internal string GetConfigurationValue(string ConfigurationCode, string? CountryKey, OUserReference UserReference)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Perform Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            OConfigurationValue.Response _Configuration = (from n in _HCoreContext.HCCoreCommon
        //                                                           where n.SystemName == ConfigurationCode && n.StatusId == StatusActive && n.TypeId == HelperType.Configuration
        //                                                           select new OConfigurationValue.Response
        //                                                           {
        //                                                               ReferenceId = n.Id,
        //                                                               Value = n.Value,
        //                                                               IsDefaultValue = 1,
        //                                                               TypeCode = n.Helper.SystemName,
        //                                                               TypeName = n.Helper.Name,
        //                                                           }).FirstOrDefault();
        //            if (_Configuration != null)
        //            {
        //                string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
        //                                             where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
        //                                             select n.Value).FirstOrDefault();
        //                if (!string.IsNullOrEmpty(ConfigurationValue))
        //                {
        //                    _Configuration.Value = ConfigurationValue;
        //                    _Configuration.IsDefaultValue = 0;
        //                }
        //                if (!string.IsNullOrEmpty(CountryKey))
        //                {
        //                    string CountryConfigurationValue = (from n in _HCoreContext.HCCoreCommon
        //                                                        where n.ParentId == _Configuration.ReferenceId && n.SubParent.Guid == CountryKey && n.StatusId == StatusActive
        //                                                        select n.Value).FirstOrDefault();
        //                    if (!string.IsNullOrEmpty(CountryConfigurationValue))
        //                    {
        //                        _Configuration.Value = CountryConfigurationValue;
        //                        _Configuration.IsDefaultValue = 0;
        //                    }
        //                }
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                return _Configuration.Value;
        //                #endregion
        //            }
        //            else
        //            {
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                return null;
        //                #endregion
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception

        //        HCoreHelper.LogException("GetConfigurationValue", _Exception, UserReference);
        //        #endregion
        //        return null;
        //    }
        //    #endregion
        //}
        //internal string GetConfigurationValueByUser(string ConfigurationCode, string? UserAccountKey, OUserReference UserReference)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Perform Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            OConfigurationValue.Response _Configuration = (from n in _HCoreContext.HCCoreCommon
        //                                                           where n.SystemName == ConfigurationCode && n.StatusId == StatusActive && n.TypeId == HelperType.Configuration
        //                                                           select new OConfigurationValue.Response
        //                                                           {
        //                                                               ReferenceId = n.Id,
        //                                                               Value = n.Value,
        //                                                               IsDefaultValue = 1,
        //                                                               TypeCode = n.Helper.SystemName,
        //                                                               TypeName = n.Helper.Name,
        //                                                           }).FirstOrDefault();
        //            if (_Configuration != null)
        //            {
        //                string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
        //                                             where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
        //                                             select n.Value).FirstOrDefault();
        //                if (!string.IsNullOrEmpty(ConfigurationValue))
        //                {
        //                    _Configuration.Value = ConfigurationValue;
        //                    _Configuration.IsDefaultValue = 0;
        //                }
        //                if (!string.IsNullOrEmpty(UserAccountKey))
        //                {
        //                    string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
        //                                                        where n.CommonId == _Configuration.ReferenceId && n.Account.Guid == UserAccountKey && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
        //                                                        select n.Value).FirstOrDefault();
        //                    if (!string.IsNullOrEmpty(CountryConfigurationValue))
        //                    {
        //                        _Configuration.Value = CountryConfigurationValue;
        //                        _Configuration.IsDefaultValue = 0;
        //                    }
        //                }
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                if (!string.IsNullOrEmpty(_Configuration.Value))
        //                {
        //                    return _Configuration.Value;
        //                }
        //                else
        //                {
        //                    return "0";
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                return "0";
        //                #endregion
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception

        //        HCoreHelper.LogException("GetConfigurationValue", _Exception, UserReference);
        //        #endregion
        //        return "0";
        //    }
        //    #endregion
        //}
        //internal string GetConfigurationValueByUser(string ConfigurationCode, long UserAccountId, OUserReference UserReference)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Perform Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            OConfigurationValue.Response _Configuration = (from n in _HCoreContext.HCCoreCommon
        //                                                           where n.SystemName == ConfigurationCode && n.StatusId == StatusActive && n.TypeId == HelperType.Configuration
        //                                                           select new OConfigurationValue.Response
        //                                                           {
        //                                                               ReferenceId = n.Id,
        //                                                               Value = n.Value,
        //                                                               IsDefaultValue = 1,
        //                                                               TypeCode = n.Helper.SystemName,
        //                                                               TypeName = n.Helper.Name,
        //                                                           }).FirstOrDefault();
        //            if (_Configuration != null)
        //            {
        //                string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
        //                                             where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
        //                                             select n.Value).FirstOrDefault();
        //                if (!string.IsNullOrEmpty(ConfigurationValue))
        //                {
        //                    _Configuration.Value = ConfigurationValue;
        //                    _Configuration.IsDefaultValue = 0;
        //                }
        //                if (UserAccountId != 0)
        //                {
        //                    string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
        //                                                        where n.CommonId == _Configuration.ReferenceId && n.UserAccountId == UserAccountId && n.StatusId == StatusActive && n.TypeId == HelperType.ConfigurationValue
        //                                                        select n.Value).FirstOrDefault();
        //                    if (!string.IsNullOrEmpty(CountryConfigurationValue))
        //                    {
        //                        _Configuration.Value = CountryConfigurationValue;
        //                        _Configuration.IsDefaultValue = 0;
        //                    }
        //                }
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                if (!string.IsNullOrEmpty(_Configuration.Value))
        //                {
        //                    return _Configuration.Value;
        //                }
        //                else
        //                {
        //                    return "0";
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                _HCoreContext.Dispose();
        //                #region Send Response
        //                return "0";
        //                #endregion
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception

        //        HCoreHelper.LogException("GetConfigurationValue", _Exception, UserReference);
        //        #endregion
        //        return "0";
        //    }
        //    #endregion
        //}
        #endregion
        #region Get App Configuration
        List<OCBank> _OCBank;
        List<OCBanks> _OCBanksList;
        OAppConfiguration.Response _OAppConfigResponse;
        /// <summary>
        /// Description: Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppConfiguration(OAppConfiguration.Request _Request)
        {
            #region Declare
            _OAppConfigResponse = new OAppConfiguration.Response();
            #endregion
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    #region Countries
                    _OAppConfigResponse.Countries = _HCoreContext.HCCoreCommon
                        .Where(x => x.StatusId == StatusActive)
                        .Select(x => new OAppConfigurationListParameters
                        {
                            SystemName = x.Value,
                            Name = x.Name,
                            SystemCode = x.SubValue
                        }).ToList();
                    #endregion
                    #region Genders
                    _OAppConfigResponse.Genders = _HCoreContext.HCCore
                      .Where(x => x.ParentId == HelperType.Gender && x.StatusId == StatusActive)
                      .Select(x => new OAppConfigurationListParameters
                      {
                          SystemName = x.SystemName,
                          Name = x.Name
                      }).ToList();
                    #endregion
                    #region Registration Sources
                    _OAppConfigResponse.RegistrationSources = _HCoreContext.HCCore
                      .Where(x => x.ParentId == HelperType.RegistrationSource && x.StatusId == StatusActive)
                      .Select(x => new OAppConfigurationListParameters
                      {
                          SystemName = x.SystemName,
                          Name = x.Name
                      }).ToList();
                    #endregion
                    #endregion

                    _OAppConfigResponse.SystemKey = "systemkey";
                    _OAppConfigResponse.AccessKey = "accesskey";
                    _OAppConfigResponse.CountryIsd = "234";
                    _OAppConfigResponse.AccessValidity = DateTime.Now.Date.AddDays(1);



                    #region Bank  List
                    _OCBank = new List<OCBank>();
                    _OCBank.Add(new OCBank
                    {
                        Name = "ACCESS BANK PLC",
                        Code = "044"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "KEYSTONE BANK PLC",
                        Code = "082"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "DIAMOND BANK PLC",
                        Code = "063"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "ECOCBank NIGERIA PLC",
                        Code = "050"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "FIDELITY BANK PLC",
                        Code = "070"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "FIRST BANK OF NIGERIA PLC",
                        Code = "011"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "FIRST CITY MONUMENT BANK PLC",
                        Code = "214"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "GUARANTY TRUST BANK PLC",
                        Code = "058"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "STANBIC IBTC BANK PLC",
                        Code = "039"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "CITI BANK",
                        Code = "023"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "SKYE BANK PLC",
                        Code = "076"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "STANDARD CHARTERED BANK PLC",
                        Code = "068"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "STERLING BANK PLC",
                        Code = "232"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "UNION BANK OF NIGERIA PLC",
                        Code = "032"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "UNITED BANK FOR AFRICA PLC",
                        Code = "033"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "UNITY BANK PLC",
                        Code = "215"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "WEMA BANK PLC",
                        Code = "035"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "ZENITH INTERNATIONAL BANK PLC",
                        Code = "057"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "HERITAGE BANK",
                        Code = "030"
                    });
                    _OCBank.Add(new OCBank
                    {
                        Name = "JAIZ BANK",
                        Code = "301"
                    });
                    #endregion
                    _OCBanksList = new List<OCBanks>();
                    _OCBanksList.Add(new OCBanks
                    {
                        Banks = _OCBank,
                        IsActive = true,
                        Name = "eTranzact",
                    });

                    #region Process Token Payment
                    _OCBank = new List<OCBank>();
                    string ResponseCode = "";
                    String ResponseData = String.Empty;
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://api.paystack.co/bank?gateway=emandate&pay_with_bank=true");
                    _HttpWebRequest.Method = "GET";
                    _HttpWebRequest.ContentType = "application/json";
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    Stream _DataStream = _HttpWebResponse.GetResponseStream();
                    StreamReader _StreamReader = new StreamReader(_DataStream);
                    ResponseData = _StreamReader.ReadToEnd();
                    ResponseCode = _HttpWebResponse.StatusCode.ToString();
                    _StreamReader.Close();
                    _DataStream.Close();
                    #endregion
                    ICBankResponse _IBankResponse = JsonConvert.DeserializeObject<ICBankResponse>(ResponseData);
                    if (_IBankResponse.status == true)
                    {
                        if (_IBankResponse.data != null)
                        {
                            foreach (var BankItem in _IBankResponse.data)
                            {
                                _OCBank.Add(new OCBank
                                {
                                    Code = BankItem.code,
                                    Name = BankItem.name,
                                });
                            }
                            _OCBanksList.Add(new OCBanks
                            {
                                Banks = _OCBank,
                                IsActive = true,
                                Name = "PayStack",
                            });
                        }
                    }
                    _OAppConfigResponse.Payments = _OCBanksList;
                    _HCoreContext.Dispose();
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OAppConfigResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetAppConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion

    }
}
