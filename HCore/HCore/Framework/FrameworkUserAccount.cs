//==================================================================================
// FileName: FrameworkUserAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user account
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using System.Xml.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Framework
{
    internal class FrameworkUserAccount
    {
        #region Declare
        //ManageConfiguration _ManageConfiguration;
        #endregion
        #region References
        ManageSystemVerification _ManageSystemVerification;
        #endregion
        #region Entity
        HCUAccountParameter _HCUAccountParameter;
        HCoreContext _HCoreContext;
        #endregion
        #region Objects
        OUserPasswordUpdate _OUserPasswordUpdate;
        OUserUserNameUpdate _OUserUserNameUpdate;
        OAccountSetupOverview _AccountSetupOverview;
        ManageStorage _ManageStorage;
        OStorage.Delete _StorageDeleteRequest;
        OUserVerificationResponse _OUserVerificationResponse;
        OSystemVerification.Request _OSystemVerificationRequest;
        OSystemVerification.RequestVerify _OSystemVerificationRequestVerify;
        OUserDetailsChangeLogSave _OUserDetailsChangeLogSave;
        OUserAccountAccess.OResponse _UserAuthResponse;
        OUserAccountAccess.OUser _ObjectUser;
        OUserAccountAccess.OUserAccount _ObjectUserAccount;
        //List<OUserAccountAccess.OUserAccountRole> _ObjectUserRole;
        #endregion
        #region Update Profile
        /// <summary>
        /// Description: Updates the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccount(OUserAccount _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.UserReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1095");
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.User.GenderCode))
                    {
                        long? GenderId = HCoreHelper.GetSystemHelperId(_Request.User.GenderCode, HelperType.Gender, _Request.UserReference);
                        if (GenderId == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1096");
                            #endregion
                        }
                    }
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                         where n.Guid == _Request.AccountReferenceKey
                                                         select n).FirstOrDefault();
                        if (UserAccountDetails != null)
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Id == UserAccountDetails.UserId && n.Guid == _Request.UserReferenceKey
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                long? OldIconStorageId = UserAccountDetails.IconStorageId;
                                long? OldPosterStorageId = UserAccountDetails.PosterStorageId;

                                #region Update user account details
                                if (!string.IsNullOrEmpty(_Request.DisplayName))
                                {
                                    UserAccountDetails.DisplayName = _Request.DisplayName;
                                }
                                if (!string.IsNullOrEmpty(_Request.ReferralCode))
                                {
                                    UserAccountDetails.ReferralCode = _Request.ReferralCode;
                                }
                                if (!string.IsNullOrEmpty(_Request.ReferralUrl))
                                {
                                    UserAccountDetails.ReferralUrl = _Request.ReferralUrl;
                                }
                                if (!string.IsNullOrEmpty(_Request.Description))
                                {
                                    UserAccountDetails.Description = _Request.Description;
                                }
                                if (!string.IsNullOrEmpty(_Request.StatusCode))
                                {
                                    #region Status Id
                                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                                    if (StatusId == 0)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1100");
                                        #endregion
                                    }
                                    else
                                    {
                                        UserAccountDetails.StatusId = StatusId;
                                    }
                                    #endregion
                                }
                                #endregion
                                #region Update User Information
                                if (_Request.User != null)
                                {
                                    if (!string.IsNullOrEmpty(_Request.User.StatusCode))
                                    {
                                        #region Status Id
                                        int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.User.StatusCode).Select(x => x.Id).FirstOrDefault();
                                        if (StatusId == 0)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1098");
                                            #endregion
                                        }
                                        else
                                        {
                                            UserDetails.StatusId = StatusId;
                                        }
                                        #endregion
                                    }
                                    if (!string.IsNullOrEmpty(_Request.User.FirstName))
                                    {
                                        UserAccountDetails.FirstName = _Request.User.FirstName;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.User.LastName))
                                    {
                                        UserAccountDetails.LastName = _Request.User.LastName;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.User.Name))
                                    {
                                        UserAccountDetails.Name = _Request.User.Name;
                                    }
                                    if (_Request.User.DateOfBirth != null)
                                    {
                                        UserAccountDetails.DateOfBirth = _Request.User.DateOfBirth;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.User.Address))
                                    {
                                        UserAccountDetails.Address = _Request.User.Address;
                                    }

                                    if (_Request.User.AddressLatitude != 0)
                                    {
                                        UserAccountDetails.Latitude = _Request.User.AddressLatitude;
                                    }
                                    if (_Request.User.AddressLongitude != 0)
                                    {
                                        UserAccountDetails.Longitude = _Request.User.AddressLongitude;
                                    }

                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;

                                    UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                }
                                #endregion

                                #region Save Changes
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion
                                long IconStorageId = 0;
                                long PosterStorageId = 0;
                                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                                {
                                    #region Delete Old Storage 
                                    if (OldIconStorageId != null)
                                    {
                                        _StorageDeleteRequest = new OStorage.Delete();
                                        _StorageDeleteRequest.ReferenceId = (long)OldIconStorageId;
                                        _StorageDeleteRequest.UserReference = _Request.UserReference;
                                        _ManageStorage = new ManageStorage();
                                        _ManageStorage.DeleteStorageByReferenceId(_StorageDeleteRequest);
                                    }
                                    #endregion

                                    _Request.IconContent.UserReference = _Request.UserReference;
                                    _Request.IconContent.TypeCode = CoreHelpers.StorageType.Image;

                                    _ManageStorage = new ManageStorage();
                                    OResponse _SaveIconContentResponse = _ManageStorage.SaveStorage(_Request.IconContent);
                                    if (_SaveIconContentResponse.Status == StatusSuccess)
                                    {
                                        OStorage.Response _StorageDetails = (OStorage.Response)_SaveIconContentResponse.Result;
                                        IconStorageId = _StorageDetails.ReferenceId;
                                    }
                                }
                                if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                                {
                                    #region Delete Old Storage 
                                    if (OldPosterStorageId != null)
                                    {
                                        _StorageDeleteRequest = new OStorage.Delete();
                                        _StorageDeleteRequest.ReferenceId = (long)OldPosterStorageId;
                                        _StorageDeleteRequest.UserReference = _Request.UserReference;
                                        _ManageStorage = new ManageStorage();
                                        _ManageStorage.DeleteStorageByReferenceId(_StorageDeleteRequest);
                                    }
                                    #endregion
                                    _Request.PosterContent.UserReference = _Request.UserReference;
                                    _Request.PosterContent.TypeCode = CoreHelpers.StorageType.Image;
                                    _ManageStorage = new ManageStorage();
                                    OResponse _SavePosterContentResponse = _ManageStorage.SaveStorage(_Request.PosterContent);
                                    if (_SavePosterContentResponse.Status == StatusSuccess)
                                    {
                                        OStorage.Response _StorageDetails = (OStorage.Response)_SavePosterContentResponse.Result;
                                        PosterStorageId = _StorageDetails.ReferenceId;
                                    }
                                }
                                if (IconStorageId != 0 || PosterStorageId != 0)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.AccountReferenceKey).FirstOrDefault();
                                        if (UserAccDetails != null)
                                        {
                                            if (IconStorageId != 0)
                                            {
                                                UserAccDetails.IconStorageId = IconStorageId;
                                            }
                                            if (PosterStorageId != 0)
                                            {
                                                UserAccDetails.PosterStorageId = PosterStorageId;
                                            }
                                            _HCoreContext.SaveChanges();

                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_Request.NewPassword))
                                {
                                    _OUserPasswordUpdate = new OUserPasswordUpdate();
                                    _OUserPasswordUpdate.NewPassword = _Request.NewPassword;
                                    _OUserPasswordUpdate.UserAccountReferenceKey = _Request.AccountReferenceKey;
                                    _OUserPasswordUpdate.UserReferenceKey = _Request.UserReferenceKey;
                                    _OUserPasswordUpdate.UserReference = _Request.UserReference;
                                    UpdateUserPassword(_OUserPasswordUpdate);
                                }
                                if (!string.IsNullOrEmpty(_Request.NewUserName))
                                {
                                    _OUserUserNameUpdate = new OUserUserNameUpdate();
                                    _OUserUserNameUpdate.Username = _Request.NewUserName;
                                    _OUserUserNameUpdate.UserAccountReferenceKey = _Request.AccountReferenceKey;
                                    _OUserUserNameUpdate.UserReferenceKey = _Request.UserReferenceKey;
                                    _OUserUserNameUpdate.UserReference = _Request.UserReference;
                                    UpdateUserUserName(_OUserUserNameUpdate);
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1097");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1099");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user account self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccountSelf(OUserAccount _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                     where n.Id == _Request.UserReference.AccountId
                                                     select n).FirstOrDefault();
                    if (UserAccountDetails != null)
                    {

                        if (UserAccountDetails.StatusId == StatusActive)
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Id == UserAccountDetails.UserId
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                if (UserDetails.StatusId == StatusActive)
                                {
                                    long? OldIconStorageId = UserAccountDetails.IconStorageId;
                                    long? OldPosterStorageId = UserAccountDetails.PosterStorageId;

                                    #region Update user account details
                                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                                    {
                                        UserAccountDetails.DisplayName = _Request.DisplayName;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.ReferralCode))
                                    {
                                        UserAccountDetails.ReferralCode = _Request.ReferralCode;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.ReferralUrl))
                                    {
                                        UserAccountDetails.ReferralUrl = _Request.ReferralUrl;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.Description))
                                    {
                                        UserAccountDetails.Description = _Request.Description;
                                    }
                                    UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                    #endregion
                                    #region Update User Information
                                    if (_Request.User != null)
                                    {
                                        if (!string.IsNullOrEmpty(_Request.User.FirstName))
                                        {
                                            UserAccountDetails.FirstName = _Request.User.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.User.LastName))
                                        {
                                            UserAccountDetails.LastName = _Request.User.LastName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.User.Name))
                                        {
                                            UserAccountDetails.Name = _Request.User.Name;
                                        }
                                        if (_Request.User.DateOfBirth != null)
                                        {
                                            UserAccountDetails.DateOfBirth = _Request.User.DateOfBirth;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.User.Address))
                                        {
                                            UserAccountDetails.Address = _Request.User.Address;
                                        }

                                        if (_Request.User.AddressLatitude != 0)
                                        {
                                            UserAccountDetails.Latitude = _Request.User.AddressLatitude;
                                        }
                                        if (_Request.User.AddressLongitude != 0)
                                        {
                                            UserAccountDetails.Longitude = _Request.User.AddressLongitude;
                                        }

                                        if (!string.IsNullOrEmpty(_Request.User.GenderCode))
                                        {
                                            int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.User.GenderCode && x.ParentId == HelperType.Gender).Select(x => x.Id).FirstOrDefault();
                                            if (GenderId == 0)
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1103");
                                                #endregion
                                            }
                                            else
                                            {
                                                UserAccountDetails.GenderId = GenderId;
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(_Request.User.EmailAddress))
                                        {
                                            if (UserAccountDetails.EmailAddress != _Request.User.EmailAddress)
                                            {
                                                UserAccountDetails.EmailVerificationStatus = 1;
                                                UserAccountDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                UserAccountDetails.EmailAddress = _Request.User.EmailAddress;
                                            }
                                        }
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    }
                                    #endregion
                                    #region Save Changes
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                    long IconStorageId = 0;
                                    long PosterStorageId = 0;
                                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                                    {
                                        #region Delete Old Storage 
                                        if (OldIconStorageId != null)
                                        {
                                            _StorageDeleteRequest = new OStorage.Delete();
                                            _StorageDeleteRequest.ReferenceId = (long)OldIconStorageId;
                                            _StorageDeleteRequest.UserReference = _Request.UserReference;
                                            _ManageStorage = new ManageStorage();
                                            _ManageStorage.DeleteStorageByReferenceId(_StorageDeleteRequest);
                                        }
                                        #endregion

                                        _Request.IconContent.UserReference = _Request.UserReference;
                                        _Request.IconContent.TypeCode = CoreHelpers.StorageType.Image;
                                        _ManageStorage = new ManageStorage();
                                        OResponse _SaveIconContentResponse = _ManageStorage.SaveStorage(_Request.IconContent);
                                        if (_SaveIconContentResponse.Status == StatusSuccess)
                                        {
                                            OStorage.Response _StorageDetails = (OStorage.Response)_SaveIconContentResponse.Result;
                                            IconStorageId = _StorageDetails.ReferenceId;
                                        }
                                    }
                                    if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                                    {
                                        #region Delete Old Storage 
                                        if (OldPosterStorageId != null)
                                        {
                                            _StorageDeleteRequest = new OStorage.Delete();
                                            _StorageDeleteRequest.ReferenceId = (long)OldPosterStorageId;
                                            _StorageDeleteRequest.UserReference = _Request.UserReference;
                                            _ManageStorage = new ManageStorage();
                                            _ManageStorage.DeleteStorageByReferenceId(_StorageDeleteRequest);
                                        }
                                        #endregion
                                        _Request.PosterContent.UserReference = _Request.UserReference;
                                        _Request.PosterContent.TypeCode = CoreHelpers.StorageType.Image;

                                        _ManageStorage = new ManageStorage();
                                        OResponse _SavePosterContentResponse = _ManageStorage.SaveStorage(_Request.PosterContent);
                                        if (_SavePosterContentResponse.Status == StatusSuccess)
                                        {
                                            OStorage.Response _StorageDetails = (OStorage.Response)_SavePosterContentResponse.Result;
                                            PosterStorageId = _StorageDetails.ReferenceId;
                                        }
                                    }
                                    if (IconStorageId != 0 || PosterStorageId != 0)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var ControllerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).FirstOrDefault();
                                            if (ControllerDetails != null)
                                            {
                                                if (IconStorageId != 0)
                                                {
                                                    ControllerDetails.IconStorageId = IconStorageId;
                                                }
                                                if (PosterStorageId != 0)
                                                {
                                                    ControllerDetails.PosterStorageId = PosterStorageId;
                                                }
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                    }
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1104");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1105");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1106");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1107");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1108");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccountSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1109");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Update Contact Number
        /// <summary>
        /// Description: Updates the user contact number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserContactNumber(OUserContactNumberUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1110");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1111");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1112");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UserAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountReferenceKey).Select(x => new
                        {
                            Id = x.Id,
                            OwnerId = x.OwnerId
                        }).FirstOrDefault();

                        if (UserAccountInfo == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1113");
                            #endregion
                        }

                        if (_Request.UserReference.AccountTypeId == UserAccountType.Controller ||
                            _Request.UserReference.AccountTypeId == UserAccountType.Admin ||
                           (UserAccountInfo.OwnerId == _Request.UserReference.AccountOwnerId) ||
                           (UserAccountInfo.OwnerId == _Request.UserReference.AccountId)
                          )
                        {
                            HCUAccount UInfo = (from n in _HCoreContext.HCUAccount
                                                where n.Guid == _Request.UserAccountReferenceKey
                                                select n).FirstOrDefault();
                            if (UInfo != null)
                            {
                                if (UInfo.StatusId == StatusActive)
                                {
                                    if ((UInfo.ContactNumber != _Request.ContactNumber) || UInfo.NumberVerificationStatus == 1)
                                    {
                                        string OldValue = UInfo.ContactNumber;
                                        long ValidateContactNumber = (from n in _HCoreContext.HCUAccount
                                                                      where n.ContactNumber == _Request.ContactNumber
                                                                      select n.Id).FirstOrDefault();
                                        if (ValidateContactNumber != 0)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1114");
                                            #endregion
                                        }
                                        else
                                        {
                                            string OldVerificationRequestReferenceKey = null;
                                            string OldVerificationRequestSystemVerificationToken = null;

                                            var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                                          where n.Account.UserId == UInfo.Id && n.SubValue == _Request.ContactNumber
                                                                          && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ChangeContactNumber
                                                                          orderby n.CreateDate descending
                                                                          select new
                                                                          {
                                                                              OldVerificationRequestReferenceKey = n.Guid,
                                                                              OldVerificationRequestSystemVerificationToken = n.Data
                                                                          }).FirstOrDefault();
                                            if (OldVerificationRequest != null)
                                            {
                                                OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                                OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                            }
                                            #region Call verification system 
                                            _OSystemVerificationRequest = new OSystemVerification.Request();
                                            _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                            _OSystemVerificationRequest.Type = 1;
                                            _OSystemVerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                                            _OSystemVerificationRequest.MobileNumber = _Request.ContactNumber;
                                            _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                            _ManageSystemVerification = new ManageSystemVerification();
                                            OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                            if (_VResponse.Status == StatusSuccess)
                                            {
                                                OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                              where n.Guid == _Request.UserReferenceKey
                                                                              select n).FirstOrDefault();
                                                    UserDetails.ContactNumber = _VResponseDetails.MobileNumber;
                                                    UserDetails.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                    UserDetails.NumberVerificationStatus = 1;
                                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                                    #region Save Changes
                                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                                    _HCoreContext.SaveChanges();
                                                    #endregion

                                                    #region Save Log 
                                                    _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                                    _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                                    _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid();
                                                    _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeContactNumber;
                                                    //_OUserDetailsChangeLogSave.UserId = UInfo.Id;
                                                    _OUserDetailsChangeLogSave.UserAccountId = UserAccountInfo.Id;
                                                    _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                                    _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                                    _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                                    _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                                    _OUserDetailsChangeLogSave.OldValue = OldValue;
                                                    _OUserDetailsChangeLogSave.NewValue = _VResponseDetails.MobileNumber;
                                                    _OUserDetailsChangeLogSave.Status = StatusInactive;
                                                    _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                                    SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                                    #endregion
                                                    #region Build Response
                                                    _OUserVerificationResponse = new OUserVerificationResponse();
                                                    _OUserVerificationResponse.ReferenceKey = _OUserDetailsChangeLogSave.Guid;
                                                    _OUserVerificationResponse.MobileNumber = _VResponseDetails.MobileNumber;
                                                    _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                                    _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1115");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                return _VResponse;
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1201");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1116");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1117");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1118");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserContactNumber", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user contact number self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserContactNumberSelf(OUserContactNumberUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1119");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccount
                                     where n.Id == _Request.UserReference.AccountId
                                     select new
                                     {
                                         UserId = n.User.Id,
                                         Status = n.StatusId,
                                         ContactNumber = n.ContactNumber,
                                         NumberVerificationStatus = n.NumberVerificationStatus,
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusActive)
                            {
                                if ((UInfo.ContactNumber != _Request.ContactNumber) || UInfo.NumberVerificationStatus == 1)
                                {
                                    long ValidateContactNumber = (from n in _HCoreContext.HCUAccount
                                                                  where n.ContactNumber == _Request.ContactNumber
                                                                  select n.Id).FirstOrDefault();
                                    if (ValidateContactNumber != 0)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1120");
                                        #endregion
                                    }
                                    else
                                    {
                                        string OldVerificationRequestReferenceKey = null;
                                        string OldVerificationRequestSystemVerificationToken = null;

                                        var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                                      where n.Account.UserId == UInfo.UserId && n.SubValue == _Request.ContactNumber
                                                                      && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ChangeContactNumber
                                                                      orderby n.CreateDate descending
                                                                      select new
                                                                      {
                                                                          OldVerificationRequestReferenceKey = n.Guid,
                                                                          OldVerificationRequestSystemVerificationToken = n.Data
                                                                      }).FirstOrDefault();
                                        if (OldVerificationRequest != null)
                                        {
                                            OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                            OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                        }
                                        else
                                        {
                                            OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                        }
                                        #region Call verification system 
                                        _OSystemVerificationRequest = new OSystemVerification.Request();
                                        _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                        _OSystemVerificationRequest.Type = 1;
                                        _OSystemVerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                                        _OSystemVerificationRequest.MobileNumber = _Request.ContactNumber;
                                        _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                        _ManageSystemVerification = new ManageSystemVerification();
                                        OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                        if (_VResponse.Status == StatusSuccess)
                                        {
                                            OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                //long UserId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.UserId).FirstOrDefault();

                                                HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                          where n.Id == _Request.UserReference.AccountId
                                                                          select n).FirstOrDefault();
                                                string OldValue = UserDetails.ContactNumber;
                                                UserDetails.ContactNumber = _VResponseDetails.MobileNumber;
                                                UserDetails.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                UserDetails.NumberVerificationStatus = 1;
                                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                                #region Save Changes
                                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                                _HCoreContext.SaveChanges();
                                                #endregion
                                                #region Save Log 
                                                _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                                _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                                _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                                //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changecontactnumber);
                                                _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeContactNumber;
                                                //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                                _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                                _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                                _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                                _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                                _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                                _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                                _OUserDetailsChangeLogSave.OldValue = OldValue;
                                                _OUserDetailsChangeLogSave.NewValue = _VResponseDetails.MobileNumber;
                                                _OUserDetailsChangeLogSave.Status = StatusInactive;
                                                _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                                SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                                #endregion
                                                #region Build Response
                                                _OUserVerificationResponse = new OUserVerificationResponse();
                                                _OUserVerificationResponse.ReferenceKey = _OUserDetailsChangeLogSave.Guid;
                                                _OUserVerificationResponse.MobileNumber = _VResponseDetails.MobileNumber;
                                                _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                                _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                                #endregion
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1121");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            return _VResponse;
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1201");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1122");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1123");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserContactNumberSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1124");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user contact number verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserContactNumberVerify(OUserVerificationResponse _Request)
        {
            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1202");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1203");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1204");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter UInfo = (from n in _HCoreContext.HCUAccountParameter
                                                     where n.Guid == _Request.ReferenceKey
                                                     select n).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.StatusId == StatusInactive)
                            {
                                #region Call verification system 
                                _OSystemVerificationRequestVerify = new OSystemVerification.RequestVerify();
                                _OSystemVerificationRequestVerify.RequestToken = UInfo.Data;
                                _OSystemVerificationRequestVerify.AccessCode = _Request.AccessCode;
                                _OSystemVerificationRequestVerify.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.VerifyOtp(_OSystemVerificationRequestVerify);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                  where n.Id == UInfo.AccountId
                                                                  select n).FirstOrDefault();
                                        UserDetails.ContactNumber = _VResponseDetails.MobileNumber;
                                        UserDetails.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.NumberVerificationStatus = 2;
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;
                                        var ChangeLog = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                        if (ChangeLog != null)
                                        {
                                            ChangeLog.SubValue = _VResponseDetails.MobileNumber;
                                            ChangeLog.EndTime = HCoreHelper.GetGMTDateTime();
                                            ChangeLog.StatusId = StatusActive;
                                        }
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1205");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1206");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1207");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserContactNumberVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1208");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Update Email Address
        /// <summary>
        /// Description: Updates the user email address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserEmailAddress(OUserEmailAddressUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1125");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1126");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1127");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {

                        var UserAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountReferenceKey).Select(x => new
                        {
                            Id = x.Id,
                            OwnerId = x.OwnerId
                        }).FirstOrDefault();

                        if (UserAccountInfo == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1128");
                            #endregion
                        }
                        if (_Request.UserReference.AccountTypeId == UserAccountType.Controller ||
                            _Request.UserReference.AccountTypeId == UserAccountType.Admin ||
                          (UserAccountInfo.OwnerId == _Request.UserReference.AccountOwnerId) ||
                          (UserAccountInfo.OwnerId == _Request.UserReference.AccountId)
                         )
                        {

                            HCUAccount UInfo = (from n in _HCoreContext.HCUAccount
                                                where n.Guid == _Request.UserAccountReferenceKey
                                                select n).FirstOrDefault();
                            if (UInfo != null)
                            {
                                if (UInfo.StatusId == StatusActive)
                                {
                                    if ((UInfo.EmailAddress != _Request.EmailAddress) || UInfo.EmailVerificationStatus == 1)
                                    {
                                        long ValidateEmailAddress = (from n in _HCoreContext.HCUAccount
                                                                     where n.EmailAddress == _Request.EmailAddress
                                                                     select n.Id).FirstOrDefault();
                                        if (ValidateEmailAddress != 0)
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1129", "Email address already exists");
                                            #endregion
                                        }
                                        else
                                        {
                                            string OldVerificationRequestReferenceKey = null;
                                            string OldVerificationRequestSystemVerificationToken = null;

                                            var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                                          where n.Account.UserId == UInfo.Id && n.SubValue == _Request.EmailAddress
                                                                          && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ChangeEmailAddress
                                                                          orderby n.CreateDate descending
                                                                          select new
                                                                          {
                                                                              OldVerificationRequestReferenceKey = n.Guid,
                                                                              OldVerificationRequestSystemVerificationToken = n.Data
                                                                          }).FirstOrDefault();
                                            if (OldVerificationRequest != null)
                                            {
                                                OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                                OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                            }
                                            else
                                            {
                                                OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                            }
                                            #region Call verification system 
                                            _OSystemVerificationRequest = new OSystemVerification.Request();
                                            _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                            _OSystemVerificationRequest.Type = 2;
                                            _OSystemVerificationRequest.EmailAddress = _Request.EmailAddress;
                                            _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                            _ManageSystemVerification = new ManageSystemVerification();
                                            OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                            if (_VResponse.Status == StatusSuccess)
                                            {
                                                OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                              where n.Guid == _Request.UserAccountReferenceKey
                                                                              select n).FirstOrDefault();

                                                    string OldValue = UserDetails.EmailAddress;
                                                    UserDetails.EmailAddress = _VResponseDetails.EmailAddress;
                                                    UserDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                    UserDetails.EmailVerificationStatus = 1;
                                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                                    #region Save Changes
                                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Save Log 
                                                    _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                                    _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                                    _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                                    _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeEmailAddress;

                                                    //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeemailaddress);
                                                    //_OUserDetailsChangeLogSave.UserId = UserDetails.Id;
                                                    _OUserDetailsChangeLogSave.UserAccountId = UserAccountInfo.Id;
                                                    _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                                    _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                                    _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                                    _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                                    _OUserDetailsChangeLogSave.OldValue = OldValue;
                                                    _OUserDetailsChangeLogSave.NewValue = _Request.EmailAddress;
                                                    _OUserDetailsChangeLogSave.Status = StatusInactive;
                                                    _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                                    SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                                    #endregion
                                                    #region Build Response
                                                    _OUserVerificationResponse = new OUserVerificationResponse();
                                                    _OUserVerificationResponse.ReferenceKey = OldVerificationRequestReferenceKey;
                                                    _OUserVerificationResponse.EmailAddress = _VResponseDetails.EmailAddress;
                                                    _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                                    _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1130");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                return _VResponse;
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1209");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1131");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1132");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1133");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserEmailAddressSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1134");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user email address self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserEmailAddressSelf(OUserEmailAddressUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1135");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {

                        HCUAccount UInfo = (from n in _HCoreContext.HCUAccount
                                            where n.Id == _Request.UserReference.AccountId
                                            select n).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.StatusId == StatusActive)
                            {
                                if ((UInfo.EmailAddress != _Request.EmailAddress) || UInfo.EmailVerificationStatus == 1)
                                {
                                    long ValidateEmailAddress = (from n in _HCoreContext.HCUAccount
                                                                 where n.EmailAddress == _Request.EmailAddress
                                                                 select n.Id).FirstOrDefault();
                                    if (ValidateEmailAddress != 0)
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1136");
                                        #endregion
                                    }
                                    else
                                    {
                                        string OldVerificationRequestReferenceKey = null;
                                        string OldVerificationRequestSystemVerificationToken = null;

                                        var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                                      where n.Account.UserId == UInfo.Id && n.SubValue == _Request.EmailAddress
                                                                      && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ChangeEmailAddress
                                                                      orderby n.CreateDate descending
                                                                      select new
                                                                      {
                                                                          OldVerificationRequestReferenceKey = n.Guid,
                                                                          OldVerificationRequestSystemVerificationToken = n.Data
                                                                      }).FirstOrDefault();
                                        if (OldVerificationRequest != null)
                                        {
                                            OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                            OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                        }
                                        else
                                        {
                                            OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                        }
                                        #region Call verification system 
                                        _OSystemVerificationRequest = new OSystemVerification.Request();
                                        _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                        _OSystemVerificationRequest.Type = 2;
                                        _OSystemVerificationRequest.EmailAddress = _Request.EmailAddress;
                                        _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                        _ManageSystemVerification = new ManageSystemVerification();
                                        OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                        if (_VResponse.Status == StatusSuccess)
                                        {
                                            OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                          where n.Id == _Request.UserReference.AccountId
                                                                          select n).FirstOrDefault();

                                                string OldValue = UserDetails.EmailAddress;
                                                UserDetails.EmailAddress = _VResponseDetails.EmailAddress;
                                                UserDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                UserDetails.EmailVerificationStatus = 1;
                                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                                #region Save Changes
                                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                                _HCoreContext.SaveChanges();
                                                #endregion
                                                #region Save Log 
                                                _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                                _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                                _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                                _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeEmailAddress;

                                                //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeemailaddress);
                                                //_OUserDetailsChangeLogSave.UserId = UserDetails.Id;
                                                _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                                _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                                _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                                _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                                _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                                _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                                _OUserDetailsChangeLogSave.OldValue = OldValue;
                                                _OUserDetailsChangeLogSave.NewValue = _Request.EmailAddress;
                                                _OUserDetailsChangeLogSave.Status = StatusInactive;
                                                _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                                SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                                #endregion
                                                #region Build Response
                                                _OUserVerificationResponse = new OUserVerificationResponse();
                                                _OUserVerificationResponse.ReferenceKey = OldVerificationRequestReferenceKey;
                                                _OUserVerificationResponse.EmailAddress = _VResponseDetails.EmailAddress;
                                                _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                                _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                                #endregion
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1130");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            return _VResponse;
                                        }
                                        #endregion


                                        //string OldValue = UserDetails.EmailAddress;
                                        //UserDetails.EmailAddress = _Request.EmailAddress;
                                        //UserDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                        //UserDetails.EmailVerificationStatus = 1;
                                        //if (_Request.UserReference.AccountTypeId == 1)
                                        //{
                                        //    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        //    UserDetails.ModifyByIdController = _Request.UserReference.AccountId;
                                        //    UserDetails.ModifyDateByController = HCoreHelper.GetGMTDateTime();
                                        //}
                                        //else
                                        //{
                                        //    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        //    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                        //}
                                        //#region Save Changes
                                        //_HCoreContext.SaveChanges();
                                        //#endregion
                                        //#region Save Log 
                                        //_OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        //_OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid();;
                                        //_OUserDetailsChangeLogSave.Type = UserDetailsChangeLogType.EmailAddress;
                                        //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                        //_OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                        //_OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.AccountDeviceId;
                                        //_OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        //_OUserDetailsChangeLogSave.OldValue = OldValue;
                                        //_OUserDetailsChangeLogSave.NewValue = _Request.EmailAddress;
                                        //_OUserDetailsChangeLogSave.Status = StatusInactive;
                                        //_OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        //SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        //#endregion
                                        //#region Send Response
                                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1137");
                                        //#endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1210");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1138");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1139");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserEmailAddressSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1140");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user email address verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserEmailAddressVerify(OUserVerificationResponse _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1211");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1212");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1213");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter UInfo = (from n in _HCoreContext.HCUAccountParameter
                                                     where n.Guid == _Request.ReferenceKey && n.Data == _Request.RequestToken
                                                     select n).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.StatusId == StatusInactive)
                            {
                                #region Call verification system 
                                _OSystemVerificationRequestVerify = new OSystemVerification.RequestVerify();
                                _OSystemVerificationRequestVerify.RequestToken = UInfo.Data;
                                _OSystemVerificationRequestVerify.AccessCode = _Request.AccessCode;
                                _OSystemVerificationRequestVerify.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.VerifyOtp(_OSystemVerificationRequestVerify);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                  where n.Id == UInfo.AccountId
                                                                  select n).FirstOrDefault();
                                        UserDetails.EmailAddress = _VResponseDetails.EmailAddress;
                                        UserDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.EmailVerificationStatus = 2;
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;

                                        var ChangeLog = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                        if (ChangeLog != null)
                                        {
                                            ChangeLog.EndTime = HCoreHelper.GetGMTDateTime();
                                            ChangeLog.StatusId = StatusActive;
                                        }
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1214");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1215");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1216");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserEmailAddressVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1217");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Username Change
        /// <summary>
        /// Description: Updates the name of the user user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserUserName(OUserUserNameUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1141");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1142");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Username))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1143");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UserAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountReferenceKey).Select(x => new
                        {
                            Id = x.Id,
                            OwnerId = x.OwnerId
                        }).FirstOrDefault();

                        if (UserAccountInfo == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1144");
                            #endregion
                        }

                        if (_Request.UserReference.AccountTypeId == UserAccountType.Controller ||
                            _Request.UserReference.AccountTypeId == UserAccountType.Admin ||
                            (UserAccountInfo.OwnerId == _Request.UserReference.AccountOwnerId) ||
                            (UserAccountInfo.OwnerId == _Request.UserReference.AccountId)
                           )
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Guid == _Request.UserReferenceKey
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                if (UserDetails.StatusId == StatusActive)
                                {
                                    #region Validate Username
                                    long ValidateUsername = (from n in _HCoreContext.HCUAccountAuth
                                                             where n.Username == _Request.Username
                                                             select n.Id).FirstOrDefault();
                                    if (ValidateUsername != 0)
                                    {
                                        #region Save Changes
                                        _HCoreContext.Dispose();
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1145");
                                        #endregion
                                    }
                                    else
                                    {
                                        string OldValue = UserDetails.Username;
                                        UserDetails.Username = _Request.Username;
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Save Log 
                                        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid();
                                        _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeUserName;

                                        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeusername);

                                        //_OUserDetailsChangeLogSave.UserId = UserDetails.Id;
                                        _OUserDetailsChangeLogSave.UserAccountId = UserAccountInfo.Id;
                                        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.ResetDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.OldValue = OldValue;
                                        _OUserDetailsChangeLogSave.NewValue = _Request.Username;
                                        _OUserDetailsChangeLogSave.Status = StatusActive;
                                        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        #endregion
                                    }
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1146");
                                    #endregion
                                }
                                else
                                {
                                    #region Save Changes
                                    _HCoreContext.Dispose();
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1147");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Save Changes
                                _HCoreContext.Dispose();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1148");
                                #endregion
                            }

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1149");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserUserNameSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1150");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user user name self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserUserNameSelf(OUserUserNameUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.Username))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1151");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserId = (from n in _HCoreContext.HCUAccount
                                       where n.Id == _Request.UserReference.AccountId
                                       select n.UserId).FirstOrDefault();
                        if (UserId != 0)
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Id == UserId
                                                          select n).FirstOrDefault();
                            if (UserDetails.StatusId == StatusActive)
                            {
                                #region Validate Username
                                long ValidateUsername = (from n in _HCoreContext.HCUAccountAuth
                                                         where n.Username == _Request.Username
                                                         select n.Id).FirstOrDefault();
                                if (ValidateUsername != 0)
                                {
                                    #region Save Changes
                                    _HCoreContext.Dispose();
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1152");
                                    #endregion
                                }
                                else
                                {
                                    string OldValue = UserDetails.Username;
                                    UserDetails.Username = _Request.Username;
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    #region Save Changes
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #endregion

                                    #region Save Log 
                                    _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                    _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid(); ;
                                    //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeusername);
                                    _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeUserName;

                                    //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                    _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                    _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                    _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.ResetDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.OldValue = OldValue;
                                    _OUserDetailsChangeLogSave.NewValue = _Request.Username;
                                    _OUserDetailsChangeLogSave.Status = StatusActive;
                                    _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                    SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                    #endregion
                                }
                                #endregion

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1153");
                                #endregion
                            }
                            else
                            {
                                #region Save Changes
                                _HCoreContext.Dispose();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1154");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Save Changes
                            _HCoreContext.Dispose();
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1155");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserUserNameSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1156");
                #endregion
            }
            #endregion
        }
        #endregion
        #region User Access  Pin
        /// <summary>
        /// Description: Sets the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetUserAccessPin(OUserAccessPinUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region  Validate
                if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1157");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.NewAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1158");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                  where n.Guid == _Request.UserAccountReferenceKey
                                                  select n).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.StatusId == StatusActive)
                            {
                                //if (string.IsNullOrEmpty(UserDetails.AccessPin))
                                //{

                                UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewAccessPin);
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                #region Save Changes
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1159");
                                #endregion
                                //}
                                //else
                                //{
                                //    #region Send Response
                                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1160");
                                //    #endregion
                                //}
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1161");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1162");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccessPinSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1163");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Sets the user access pin self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetUserAccessPinSelf(OUserAccessPinUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.NewAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1164");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                  where n.Id == _Request.UserReference.AccountId
                                                  select n).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.StatusId == StatusActive)
                            {
                                //if (string.IsNullOrEmpty(UserDetails.AccessPin))
                                //{

                                UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewAccessPin);
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                #region Save Changes
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1165");
                                #endregion
                                //}
                                //else
                                //{
                                //    #region Send Response
                                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1166");
                                //    #endregion
                                //}
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1167");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1168");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SetUserAccessPinSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1169");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccessPin(OUserAccessPinUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1170");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.NewAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1171");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                  where n.Guid == _Request.UserAccountReferenceKey
                                                  select n).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller ||
                            _Request.UserReference.AccountTypeId == UserAccountType.Admin ||
                                (UserDetails.OwnerId == _Request.UserReference.AccountOwnerId) ||
                                (UserDetails.OwnerId == _Request.UserReference.AccountId)
                           )
                            {
                                if (UserDetails.StatusId == StatusActive)
                                {
                                    if (!string.IsNullOrEmpty(UserDetails.AccessPin))
                                    {
                                        string OldValue = UserDetails.AccessPin;

                                        string TOldAccessPin = HCoreEncrypt.DecryptHash(UserDetails.AccessPin);
                                        UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewAccessPin);
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Save Log 
                                        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid(); ;
                                        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeaccesspin);
                                        _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeAccessPin;

                                        //_OUserDetailsChangeLogSave.UserId = UserDetails.UserId;
                                        _OUserDetailsChangeLogSave.UserAccountId = UserDetails.Id;
                                        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.OldValue = OldValue;
                                        _OUserDetailsChangeLogSave.NewValue = UserDetails.AccessPin;
                                        _OUserDetailsChangeLogSave.Status = StatusInactive;
                                        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        #endregion

                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1172");
                                        #endregion

                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1173");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1174");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1175");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1176");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccessPin", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1177");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user access pin self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccessPinSelf(OUserAccessPinUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.OldAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1178");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.NewAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1179");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                  where n.Id == _Request.UserReference.AccountId
                                                  select n).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.StatusId == StatusActive)
                            {
                                if (!string.IsNullOrEmpty(UserDetails.AccessPin))
                                {
                                    string OldValue = UserDetails.AccessPin;

                                    string TOldAccessPin = HCoreEncrypt.DecryptHash(UserDetails.AccessPin);
                                    if (TOldAccessPin == _Request.OldAccessPin)
                                    {
                                        UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewAccessPin);
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Save Log 
                                        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid(); ;
                                        _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChangeAccessPin;
                                        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changeaccesspin);
                                        //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                        _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                        _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.OldValue = OldValue;
                                        _OUserDetailsChangeLogSave.NewValue = UserDetails.AccessPin;
                                        _OUserDetailsChangeLogSave.Status = StatusInactive;
                                        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1180");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1181");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1182");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1183");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1184");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccessPinSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1185");
                #endregion
            }
            #endregion
        }
        #endregion
        #region User Password
        /// <summary>
        /// Description: Updates the user password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserPassword(OUserPasswordUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Set User Reference Key 
                if (string.IsNullOrEmpty(_Request.UserReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1186");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1187");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1188");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UserAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountReferenceKey).Select(x => new
                        {
                            Id = x.Id,
                            OwnerId = x.OwnerId
                        }).FirstOrDefault();

                        if (UserAccountInfo == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1189");
                            #endregion
                        }

                        if (_Request.UserReference.AccountTypeId == UserAccountType.Controller ||
                            _Request.UserReference.AccountTypeId == UserAccountType.Admin ||
                            (UserAccountInfo.OwnerId == _Request.UserReference.AccountOwnerId) ||
                            (UserAccountInfo.OwnerId == _Request.UserReference.AccountId)
                           )
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Guid == _Request.UserReferenceKey
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {

                                string OldValue = UserDetails.Password;
                                string NewValue = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                UserDetails.Password = NewValue;
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                #region Save Changes
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion
                                #region Save Log 
                                _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid();
                                _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChanagePassword;

                                //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changepassword);

                                //_OUserDetailsChangeLogSave.UserId = UserDetails.Id;
                                _OUserDetailsChangeLogSave.UserAccountId = UserAccountInfo.Id;
                                _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                _OUserDetailsChangeLogSave.ResetDate = HCoreHelper.GetGMTDateTime();
                                _OUserDetailsChangeLogSave.OldValue = OldValue;
                                _OUserDetailsChangeLogSave.NewValue = NewValue;
                                _OUserDetailsChangeLogSave.Status = StatusActive;
                                _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1190");
                                #endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1191");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1192");
                            #endregion
                        }

                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserPasswordSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1193");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user password self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserPasswordSelf(OUserPasswordUpdate _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.OldPassword))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1194");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1195");
                    #endregion
                }
                #endregion
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.UserId).FirstOrDefault();
                        HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                      where n.Id == UserId
                                                      select n).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.StatusId == StatusActive)
                            {
                                string TOldPassword = HCoreEncrypt.DecryptHash(UserDetails.Password);
                                if (TOldPassword == _Request.OldPassword)
                                {
                                    string OldValue = UserDetails.Password;
                                    string NewValue = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    UserDetails.Password = NewValue;
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    #region Save Changes
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                    #region Save Log 
                                    _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                    _OUserDetailsChangeLogSave.Guid = HCoreHelper.GenerateGuid();
                                    _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ChanagePassword;

                                    //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_changepassword);
                                    //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                    _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                    _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                    _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.ResetDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.ResetDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.OldValue = OldValue;
                                    _OUserDetailsChangeLogSave.NewValue = NewValue;
                                    _OUserDetailsChangeLogSave.Status = StatusActive;
                                    _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                    SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1196");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1197");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1198");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1199");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserPasswordSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1200");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Forgot Access Pin
        /// <summary>
        /// Description: Forgets the user access pin request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgetUserAccessPinRequest(OForgotAccessPin.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var UInfo = (from n in _HCoreContext.HCUAccount
                                 where n.Id == _Request.UserReference.AccountId
                                 select new
                                 {
                                     Id = n.Id,
                                     Status = n.StatusId,
                                     ContactNumber = n.ContactNumber,
                                     AccessPin = n.AccessPin,
                                 }).FirstOrDefault();
                    if (UInfo != null)
                    {
                        if (UInfo.Status == StatusActive)
                        {
                            if (!string.IsNullOrEmpty(UInfo.AccessPin))
                            {
                                string OldVerificationRequestReferenceKey = null;
                                string OldVerificationRequestSystemVerificationToken = null;
                                var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                              where n.Account.UserId == UInfo.Id
                                                              && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ForgotAccessPin
                                                              orderby n.CreateDate descending
                                                              select new
                                                              {
                                                                  OldVerificationRequestReferenceKey = n.Guid,
                                                                  OldVerificationRequestSystemVerificationToken = n.Data
                                                              }).FirstOrDefault();
                                if (OldVerificationRequest != null)
                                {
                                    OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                    OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                }
                                else
                                {
                                    OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                }
                                #region Call verification system 
                                _OSystemVerificationRequest = new OSystemVerification.Request();
                                _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                _OSystemVerificationRequest.Type = 1;
                                _OSystemVerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                                _OSystemVerificationRequest.MobileNumber = UInfo.ContactNumber;
                                _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        #region Save Log 
                                        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                        _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                        _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ForgotAccessPin;

                                        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_forgotaccesspin);
                                        //_OUserDetailsChangeLogSave.UserId = _Request.UserReference.UserId;
                                        _OUserDetailsChangeLogSave.UserAccountId = _Request.UserReference.AccountId;
                                        _OUserDetailsChangeLogSave.UserDeviceId = _Request.UserReference.DeviceId;
                                        _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                        _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                        _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.OldValue = UInfo.AccessPin;
                                        _OUserDetailsChangeLogSave.NewValue = UInfo.AccessPin;
                                        _OUserDetailsChangeLogSave.Status = StatusInactive;
                                        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        #endregion
                                        #region Build Response
                                        _OUserVerificationResponse = new OUserVerificationResponse();
                                        _OUserVerificationResponse.ReferenceKey = OldVerificationRequestReferenceKey;
                                        _OUserVerificationResponse.MobileNumber = _VResponseDetails.MobileNumber;
                                        _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                        _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1247");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1248");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1249");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1250");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgetUserAccessPinRequest", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1251");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Forgets the user access pin verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgetUserAccessPinVerify(OForgotAccessPin.Verify _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1252");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1253");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1254");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1255");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccountParameter
                                     where n.Guid == _Request.ReferenceKey
                                     select new

                                     {
                                         Status = n.StatusId,
                                         SystemVerificationToken = n.Data,
                                         UserId = n.Account.UserId,
                                         AccountId = n.AccountId
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusInactive)
                            {
                                #region Call verification system 
                                _OSystemVerificationRequestVerify = new OSystemVerification.RequestVerify();
                                _OSystemVerificationRequestVerify.RequestToken = UInfo.SystemVerificationToken;
                                _OSystemVerificationRequestVerify.AccessCode = _Request.AccessCode;
                                _OSystemVerificationRequestVerify.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.VerifyOtp(_OSystemVerificationRequestVerify);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        HCUAccount UserDetails = (from n in _HCoreContext.HCUAccount
                                                                  where n.Id == UInfo.UserId
                                                                  select n).FirstOrDefault();

                                        UserDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                        UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserDetails.ModifyById = _Request.UserReference.AccountId;

                                        var ChangeLog = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                        if (ChangeLog != null)
                                        {
                                            ChangeLog.SubValue = UserDetails.AccessPin;
                                            ChangeLog.EndTime = HCoreHelper.GetGMTDateTime();
                                            ChangeLog.StatusId = StatusActive;
                                        }
                                        #region Save Changes
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1256");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1257");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1258");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgetUserAccessPinVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1259");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Forgot Password
        /// <summary>
        /// Forgots the password request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgotPasswordRequest(ForgotPassword.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1218");
                    #endregion
                }
                //else if (string.IsNullOrEmpty(_Request.ContactNumber))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1219");
                //    #endregion
                //}
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccount
                                     where n.User.Username == _Request.UserName
                                     && (n.AccountTypeId == UserAccountType.Merchant
                                        || n.AccountTypeId == UserAccountType.Acquirer
                                        || n.AccountTypeId == UserAccountType.PgAccount
                                        || n.AccountTypeId == UserAccountType.PosAccount
                                        || n.AccountTypeId == UserAccountType.AcquirerSubAccount
                                        )
                                     select new
                                     {
                                         UserId = n.User.Id,
                                         UserAccountId = n.Id,
                                         Status = n.User.StatusId,
                                         ContactNumber = n.ContactNumber,
                                         EmailAddress = n.EmailAddress,
                                         CountryIsd = n.Country.Isd,
                                         Password = n.User.Password,
                                         DisplayName = n.DisplayName
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusActive || UInfo.Status == StatusInactive)
                            {
                                if (!string.IsNullOrEmpty(UInfo.EmailAddress))
                                {
                                    string VKey = HCoreHelper.GenerateGuid();
                                    #region Save Log 
                                    _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                    _OUserDetailsChangeLogSave.ReferenceKey = VKey;
                                    _OUserDetailsChangeLogSave.Guid = VKey;
                                    _OUserDetailsChangeLogSave.Type = UserDetailsChangeLog.ForgotPassword;
                                    //_OUserDetailsChangeLogSave.UserId = UInfo.UserId;
                                    _OUserDetailsChangeLogSave.UserAccountId = UInfo.UserAccountId;
                                    //_OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                    _OUserDetailsChangeLogSave.SystemVerificationToken = VKey;
                                    _OUserDetailsChangeLogSave.SystemVerificationCode = VKey;
                                    _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                    _OUserDetailsChangeLogSave.OldValue = UInfo.Password;
                                    _OUserDetailsChangeLogSave.NewValue = UInfo.Password;
                                    _OUserDetailsChangeLogSave.Status = StatusInactive;
                                    _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                    SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                    #endregion
                                    var _EmailParameters = new
                                    {
                                        UserDisplayName = UInfo.DisplayName,
                                        Link = "https://merchant.thankucash.com/resetpassword/" + VKey,
                                    };
                                    HCoreHelper.BroadCastEmail(CoreConstant.SendGridEmailTemplateIds.ForgotPassword, UInfo.DisplayName, UInfo.EmailAddress, _EmailParameters, _Request.UserReference);

                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1225", "Password reset link sent to your email address. Please check your mailbox to reset your account password.");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1225", "Email address not set for the account. Please contact support.");
                                    #endregion
                                }


                                //string OldValue = UInfo.Password;
                                //string OldVerificationRequestReferenceKey = null;
                                //string OldVerificationRequestSystemVerificationToken = null;
                                //var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                //                              where n.UserId == UInfo.UserId
                                //                              && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLogType_ForgotPassword
                                //                              orderby n.CreateDate descending
                                //                              select new
                                //                              {
                                //                                  OldVerificationRequestReferenceKey = n.Guid,
                                //                                  OldVerificationRequestSystemVerificationToken = n.Data
                                //                              }).FirstOrDefault();
                                //if (OldVerificationRequest != null)
                                //{
                                //    OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                //    OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                //}
                                //else
                                //{
                                //    OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                //}
                                //#region Call verification system 
                                //_OSystemVerificationRequest = new OSystemVerification.Request();
                                //_OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                //if (!string.IsNullOrEmpty(UInfo.EmailAddress))
                                //{
                                //    _OSystemVerificationRequest.Type = 2;
                                //}
                                //else
                                //{
                                //    _OSystemVerificationRequest.Type = 1;
                                //}
                                //_OSystemVerificationRequest.CountryIsd = UInfo.CountryIsd;
                                //_OSystemVerificationRequest.MobileNumber = UInfo.ContactNumber;
                                //_OSystemVerificationRequest.EmailAddress = UInfo.EmailAddress;
                                //_OSystemVerificationRequest.UserReference = _Request.UserReference;
                                //_ManageSystemVerification = new ManageSystemVerification();
                                //OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                //if (_VResponse.Status == StatusSuccess)
                                //{
                                //    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                //    using (_HCoreContext = new HCoreContext())
                                //    {
                                //        #region Save Log 
                                //        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                //        _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                //        _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                //        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_forgotaccesspin);
                                //        _OUserDetailsChangeLogSave.UserId = UInfo.UserId;
                                //        _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                //        _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                //        _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                //        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                //        _OUserDetailsChangeLogSave.OldValue = OldValue;
                                //        _OUserDetailsChangeLogSave.NewValue = OldValue;
                                //        _OUserDetailsChangeLogSave.Status = StatusInactive;
                                //        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                //        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                //        #endregion
                                //        #region Build Response
                                //        _OUserVerificationResponse = new OUserVerificationResponse();
                                //        _OUserVerificationResponse.ReferenceKey = OldVerificationRequestReferenceKey;
                                //        _OUserVerificationResponse.MobileNumber = _VResponseDetails.MobileNumber;
                                //        _OUserVerificationResponse.EmailAddress = _VResponseDetails.EmailAddress;
                                //        _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                //        _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                //        #endregion
                                //        #region Send Response
                                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1220");
                                //        #endregion
                                //    }
                                //}
                                //else
                                //{
                                //    return _VResponse;
                                //}
                                //#endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1221", "Your account is blocked or inactive please contact support.");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1222", "Please enter valid username / login id to reset your password.");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgotPasswordRequest", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1223");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Forgots the password request verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgotPasswordRequestVerify(ForgotPassword.Verify _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                //if (string.IsNullOrEmpty(_Request.ReferenceKey))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1224");
                //    #endregion
                //}
                if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1225", "Request token missing");
                    #endregion
                }
                //else if (string.IsNullOrEmpty(_Request.AccessCode))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1226");
                //    #endregion
                //}
                else if (string.IsNullOrEmpty(_Request.Password))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1227", "Password missing");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccountParameter
                                     where n.Data == _Request.RequestToken
                                     select new
                                     {
                                         Status = n.StatusId,
                                         SystemVerificationToken = n.Data,
                                         UserId = n.Account.UserId,
                                         UserAccountId = n.AccountId,
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusInactive)
                            {
                                HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                              where n.HCUAccount.Where(x => x.Id == UInfo.UserAccountId).Count() > 0
                                                              select n).FirstOrDefault();
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                var ChangeLog = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey || x.Data == _Request.RequestToken).FirstOrDefault();
                                if (ChangeLog != null)
                                {
                                    ChangeLog.SubValue = UserDetails.Password;
                                    ChangeLog.EndTime = HCoreHelper.GetGMTDateTime();
                                    ChangeLog.StatusId = StatusActive;
                                }
                                #region Save Changes
                                //HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1228", "Password updated successfully");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1229", "Reset link expired. Please start process again");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1230", "Invalid request");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgotPasswordRequestVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1231");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Forgot Username
        /// <summary>
        /// Forgots the user name request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgotUserNameRequest(ForgotUserName.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1232");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1233");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccount
                                     where n.EmailAddress == _Request.EmailAddress && n.ContactNumber == _Request.ContactNumber
                                     select new
                                     {
                                         UserId = n.Id,
                                         Status = n.StatusId,
                                         ContactNumber = n.ContactNumber,
                                         EmailAddress = n.EmailAddress,
                                         CountryIsd = n.Country.Isd,
                                         UserName = n.User.Username,
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusActive)
                            {
                                string OldValue = UInfo.UserName;
                                string OldVerificationRequestReferenceKey = null;
                                string OldVerificationRequestSystemVerificationToken = null;

                                var OldVerificationRequest = (from n in _HCoreContext.HCUAccountParameter
                                                              where n.Account.UserId == UInfo.UserId && n.SubValue == _Request.ContactNumber
                                                              && n.StatusId == StatusInactive && n.HelperId == UserDetailsChangeLog.ForgotUserName
                                                              orderby n.CreateDate descending
                                                              select new
                                                              {
                                                                  OldVerificationRequestReferenceKey = n.Guid,
                                                                  OldVerificationRequestSystemVerificationToken = n.Data
                                                              }).FirstOrDefault();
                                if (OldVerificationRequest != null)
                                {
                                    OldVerificationRequestReferenceKey = OldVerificationRequest.OldVerificationRequestReferenceKey;
                                    OldVerificationRequestSystemVerificationToken = OldVerificationRequest.OldVerificationRequestSystemVerificationToken;
                                }
                                else
                                {
                                    OldVerificationRequestReferenceKey = HCoreHelper.GenerateGuid(); ;
                                }
                                #region Call verification system 
                                _OSystemVerificationRequest = new OSystemVerification.Request();
                                _OSystemVerificationRequest.RequestToken = OldVerificationRequestSystemVerificationToken;
                                if (!string.IsNullOrEmpty(UInfo.EmailAddress))
                                {
                                    _OSystemVerificationRequest.Type = 3;
                                }
                                else
                                {
                                    _OSystemVerificationRequest.Type = 1;
                                }
                                _OSystemVerificationRequest.CountryIsd = UInfo.CountryIsd;
                                _OSystemVerificationRequest.MobileNumber = UInfo.ContactNumber;
                                _OSystemVerificationRequest.EmailAddress = UInfo.EmailAddress;
                                _OSystemVerificationRequest.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.RequestOtp(_OSystemVerificationRequest);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        #region Save Log 
                                        _OUserDetailsChangeLogSave = new OUserDetailsChangeLogSave();
                                        _OUserDetailsChangeLogSave.ReferenceKey = OldVerificationRequestReferenceKey;
                                        _OUserDetailsChangeLogSave.Guid = OldVerificationRequestReferenceKey;
                                        _OUserDetailsChangeLogSave.Type = HCoreConstant.UserDetailsChangeLog.ForgotUserName;

                                        //_OUserDetailsChangeLogSave.Type = CoreHelper.GetSystemHelperId(SystemHelperTypeCode.userdetailschange_forgotusername);
                                        //_OUserDetailsChangeLogSave.UserId = UInfo.UserId;
                                        _OUserDetailsChangeLogSave.SystemVerificationId = _VResponseDetails.ReferenceId;
                                        _OUserDetailsChangeLogSave.SystemVerificationToken = _VResponseDetails.RequestToken;
                                        _OUserDetailsChangeLogSave.SystemVerificationCode = _VResponseDetails.CodeStart;
                                        _OUserDetailsChangeLogSave.RequestDate = HCoreHelper.GetGMTDateTime();
                                        _OUserDetailsChangeLogSave.OldValue = OldValue;
                                        _OUserDetailsChangeLogSave.NewValue = OldValue;
                                        _OUserDetailsChangeLogSave.Status = StatusInactive;
                                        _OUserDetailsChangeLogSave.UserReference = _Request.UserReference;
                                        SaveUserDetailsChangeLog(_OUserDetailsChangeLogSave);
                                        #endregion
                                        #region Build Response
                                        _OUserVerificationResponse = new OUserVerificationResponse();
                                        _OUserVerificationResponse.ReferenceKey = OldVerificationRequestReferenceKey;
                                        _OUserVerificationResponse.MobileNumber = _VResponseDetails.MobileNumber;
                                        _OUserVerificationResponse.EmailAddress = _VResponseDetails.EmailAddress;
                                        _OUserVerificationResponse.CodeStart = _VResponseDetails.CodeStart;
                                        _OUserVerificationResponse.RequestToken = _VResponseDetails.RequestToken;
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserVerificationResponse, "HC1234");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1235");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1236");
                            #endregion
                        }

                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgotUserNameRequest", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1237");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Forgots the user name request verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ForgotUserNameRequestVerify(ForgotUserName.Verify _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1238");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1239");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1240");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1241");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UInfo = (from n in _HCoreContext.HCUAccountParameter
                                     where n.Guid == _Request.ReferenceKey
                                     select new
                                     {
                                         Status = n.StatusId,
                                         SystemVerificationToken = n.Data,
                                         UserId = n.Account.UserId,
                                     }).FirstOrDefault();
                        if (UInfo != null)
                        {
                            if (UInfo.Status == StatusInactive)
                            {
                                #region Call verification system 
                                _OSystemVerificationRequestVerify = new OSystemVerification.RequestVerify();
                                _OSystemVerificationRequestVerify.RequestToken = UInfo.SystemVerificationToken;
                                _OSystemVerificationRequestVerify.AccessCode = _Request.AccessCode;
                                _OSystemVerificationRequestVerify.UserReference = _Request.UserReference;
                                _ManageSystemVerification = new ManageSystemVerification();
                                OResponse _VResponse = _ManageSystemVerification.VerifyOtp(_OSystemVerificationRequestVerify);
                                if (_VResponse.Status == StatusSuccess)
                                {
                                    OSystemVerification.Response _VResponseDetails = (OSystemVerification.Response)_VResponse.Result;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        long CheckUserName = (from n in _HCoreContext.HCUAccountAuth
                                                              where n.Username == _Request.UserName
                                                              select n.Id).FirstOrDefault();
                                        if (CheckUserName == 0)
                                        {
                                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                                          where n.Id == UInfo.UserId
                                                                          select n).FirstOrDefault();
                                            UserDetails.Username = _Request.UserName;
                                            UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            var ChangeLog = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                            if (ChangeLog != null)
                                            {
                                                ChangeLog.SubValue = UserDetails.Username;
                                                ChangeLog.EndTime = HCoreHelper.GetGMTDateTime();
                                                ChangeLog.StatusId = StatusActive;
                                            }
                                            #region Save Changes
                                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1242");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1243");
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    return _VResponse;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1244");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1245");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ForgotUserNameRequestVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1246");
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get User Details Change Log
        /// <summary>
        /// Gets the user details change log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserDetailsChangeLog(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCUAccountParameter
                                        where n.TypeId == 185
                                        select new OUserDetailsChangeLog
                                        {
                                            ReferenceKey = n.Guid,

                                            TypeCode = n.Helper.SystemName,
                                            TypeName = n.Helper.Name,

                                            UserKey = n.Account.User.Guid,
                                            UserAccountKey = n.Account.Guid,
                                            UserDeviceKey = n.Device.Guid,

                                            OldValue = n.Value,
                                            NewValue = n.SubValue,

                                            RequestDate = n.StartTime,
                                            ResetDate = n.EndTime,

                                            //IpAddress = n.Request.IpAddress,
                                            //Latitude = n.Request.Latitude,
                                            //Longitude = n.Request.Longitude,

                                            SystemVerificationToken = n.Data,
                                            SystemVerificationCode = n.Name,

                                            CreateDate = n.CreateDate,
                                            CreatedByKey = n.CreatedBy.Guid,
                                            CreatedByDisplayName = n.CreatedBy.DisplayName,

                                            StatusId = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OUserDetailsChangeLog> Data = (from n in _HCoreContext.HCUAccountParameter
                                                        where n.TypeId == 185
                                                        select new OUserDetailsChangeLog
                                                        {
                                                            ReferenceKey = n.Guid,

                                                            TypeCode = n.Helper.SystemName,
                                                            TypeName = n.Helper.Name,

                                                            UserKey = n.Account.User.Guid,
                                                            UserAccountKey = n.Account.Guid,
                                                            UserDeviceKey = n.Device.Guid,

                                                            OldValue = n.Value,
                                                            NewValue = n.SubValue,

                                                            RequestDate = n.StartTime,
                                                            ResetDate = n.EndTime,

                                                            //IpAddress = n.Request.IpAddress,
                                                            //Latitude = n.Request.Latitude,
                                                            //Longitude = n.Request.Longitude,

                                                            SystemVerificationToken = n.Data,
                                                            SystemVerificationCode = n.Name,

                                                            CreateDate = n.CreateDate,
                                                            CreatedByKey = n.CreatedBy.Guid,
                                                            CreatedByDisplayName = n.CreatedBy.DisplayName,

                                                            StatusId = n.StatusId,
                                                            StatusCode = n.Status.SystemName,
                                                            StatusName = n.Status.Name,
                                                        })
                                               .Where(_Request.SearchCondition)
                                               .OrderBy(_Request.SortExpression)
                                               .Skip(_Request.Offset)
                                               .Take(_Request.Limit)
                                               .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserDetailsChangeLog", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion
        /// <summary>
        /// Description: Saves the user details change log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void SaveUserDetailsChangeLog(OUserDetailsChangeLogSave _Request)
        {
            #region Manage Exception
            try
            {

                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        HCUAccountParameter RequestDetails = (from n in _HCoreContext.HCUAccountParameter
                                                              where n.Guid == _Request.ReferenceKey
                                                              select n).FirstOrDefault();
                        if (RequestDetails != null)
                        {
                            RequestDetails.TypeId = 185;
                            RequestDetails.HelperId = _Request.Type;
                            //RequestDetails.UserId = _Request.UserId;
                            RequestDetails.AccountId = _Request.UserAccountId;
                            RequestDetails.DeviceId = _Request.UserDeviceId;

                            RequestDetails.VerificationId = _Request.SystemVerificationId;
                            RequestDetails.Data = _Request.SystemVerificationToken;
                            RequestDetails.Name = _Request.SystemVerificationCode;

                            RequestDetails.StartTime = _Request.RequestDate;
                            RequestDetails.EndTime = _Request.ResetDate;

                            RequestDetails.Value = _Request.OldValue;
                            RequestDetails.SubValue = _Request.NewValue;
                            RequestDetails.StatusId = _Request.Status;
                            RequestDetails.Description = _Request.UserReference.RequestIpAddress;
                            RequestDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            RequestDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                        }
                        else
                        {
                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = _Request.Guid;
                            _HCUAccountParameter.TypeId = 185;
                            _HCUAccountParameter.HelperId = (int)_Request.Type;
                            //_HCUAccountParameter.UserId = _Request.UserId;
                            _HCUAccountParameter.AccountId = _Request.UserAccountId;
                            _HCUAccountParameter.DeviceId = _Request.UserDeviceId;

                            _HCUAccountParameter.VerificationId = _Request.SystemVerificationId;
                            _HCUAccountParameter.Data = _Request.SystemVerificationToken;
                            _HCUAccountParameter.Name = _Request.SystemVerificationCode;

                            _HCUAccountParameter.StartTime = _Request.RequestDate;
                            _HCUAccountParameter.EndTime = _Request.ResetDate;


                            _HCUAccountParameter.Value = _Request.OldValue;
                            _HCUAccountParameter.SubValue = _Request.NewValue;

                            _HCUAccountParameter.StatusId = _Request.Status;
                            _HCUAccountParameter.Description = _Request.UserReference.RequestIpAddress;
                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    else
                    {
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = _Request.Guid;
                        _HCUAccountParameter.TypeId = 185;
                        _HCUAccountParameter.HelperId = (int)_Request.Type;
                        //_HCUAccountParameter.UserId = _Request.UserId;
                        _HCUAccountParameter.AccountId = _Request.UserAccountId;
                        _HCUAccountParameter.DeviceId = _Request.UserDeviceId;

                        _HCUAccountParameter.VerificationId = _Request.SystemVerificationId;
                        _HCUAccountParameter.Data = _Request.SystemVerificationToken;
                        _HCUAccountParameter.Name = _Request.SystemVerificationCode;

                        _HCUAccountParameter.StartTime = _Request.RequestDate;
                        _HCUAccountParameter.EndTime = _Request.ResetDate;


                        _HCUAccountParameter.Value = _Request.OldValue;
                        _HCUAccountParameter.SubValue = _Request.NewValue;

                        _HCUAccountParameter.StatusId = _Request.Status;
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();


                        _HCUAccountParameter.Description = _Request.UserReference.RequestIpAddress;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        }


                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        _HCoreContext.SaveChanges();
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveUserDetailsChangeLog", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccount(OUserAccount _Request)
        {

            #region Manage Exception
            try
            {
                #region Set User Reference Key 
                if (string.IsNullOrEmpty(_Request.AccountReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    OUserAccount Data = (from n in _HCoreContext.HCUAccount
                                         where n.Guid == _Request.AccountReferenceKey
                                         select new OUserAccount
                                         {
                                             ReferenceId = n.Id,
                                             UserId = n.UserId,
                                             SystemKey = HCoreEncrypt.DecryptHash(n.User.SystemPassword),
                                             AccountReferenceKey = n.Guid,

                                             AccountTypeId = n.AccountTypeId,
                                             AccountTypeCode = n.AccountType.SystemName,
                                             AccountTypeName = n.AccountType.Name,

                                             AccountOperationTypeCode = n.AccountOperationType.SystemName,
                                             AccountOperationTypeName = n.AccountOperationType.Name,

                                             OwnerKey = n.Owner.Guid,
                                             OwnerDisplayName = n.Owner.DisplayName,
                                             OwnerIconUrl = n.Owner.IconStorage.Path,

                                             OwnerParentKey = n.Owner.Owner.Guid,
                                             OwnerParentDisplayName = n.Owner.Owner.DisplayName,

                                             DisplayName = n.DisplayName,

                                             AccountCode = n.AccountCode,

                                             CardTypeName = n.Card.CardType.Name,
                                             PrepaidCardTypeName = n.Card.PrepaidCardType.Name,

                                             ReferralCode = n.ReferralCode,
                                             ReferralUrl = n.ReferralUrl,

                                             AccessPin = n.AccessPin,
                                             Description = n.Description,

                                             RegistrationSourceName = n.RegistrationSource.Name,
                                             RegistrationSourceCode = n.RegistrationSource.SystemName,

                                             IconUrl = n.IconStorage.Path,
                                             PosterUrl = n.PosterStorage.Path,

                                             LastLoginDate = n.LastLoginDate,

                                             CreateDate = n.CreateDate,
                                             CreatedByKey = n.CreatedBy.Guid,
                                             CreatedByDisplayName = n.CreatedBy.DisplayName,
                                             CreatedById = n.CreatedById,
                                             CreatedByOwnerKey = n.CreatedBy.Owner.Guid,
                                             CreatedByOwnerDisplayName = n.CreatedBy.Owner.DisplayName,
                                             CreatedByAccountTypeId = n.CreatedBy.AccountTypeId,
                                             ModifyDate = n.ModifyDate,
                                             ModifyByKey = n.ModifyBy.Guid,
                                             ModifyByDisplayName = n.ModifyBy.DisplayName,

                                             AppName = n.AppVersion.Parent.Name,

                                             Status = n.StatusId,
                                             StatusCode = n.Status.SystemName,
                                             StatusName = n.Status.Name,
                                         }).FirstOrDefault();

                    if (Data != null)
                    {

                        if (Data.AccountTypeId == UserAccountType.Merchant)
                        {
                            _AccountSetupOverview = new OAccountSetupOverview();
                            //_AccountSetupOverview.ApprovedCards = _HCoreContext.TUCardOrder.Where(x => x.MerchantId == Data.ReferenceId && x.StatusId == StatusHelperId.CardOrder_Approved).Select(x => (long?)x.ApprovedQuantity).Sum() ?? 0;
                            //_AccountSetupOverview.AllocatedDeviceToMerchant = _HCoreContext.TUDeviceAllocation.Where(x => x.ParentId == Data.ReferenceId && x.UserAccountId == Data.ReferenceId && x.StatusId == StatusHelperId.Default_Active).Count();
                            //_AccountSetupOverview.AllocatedDeviceToStore = _HCoreContext.TUDeviceAllocation.Where(x => x.ParentId == Data.ReferenceId && x.UserAccountId != Data.ReferenceId && x.StatusId == StatusHelperId.Default_Active).Count();

                            ////_AccountSetupOverview.BankAccounts = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == Data.ReferenceId && x.StatusId == StatusHelperId.Default_Active).Count();
                            //_AccountSetupOverview.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == Data.ReferenceId && x.AccountTypeId == UserAccountType.MerchantStore && x.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline).Count();
                            //_AccountSetupOverview.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == Data.ReferenceId && x.AccountTypeId == UserAccountType.MerchantCashier && x.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline).Count();
                            //_AccountSetupOverview.OnlineStores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == Data.ReferenceId && x.AccountTypeId == UserAccountType.MerchantStore && x.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online).Count();
                            //_AccountSetupOverview.OnlineApps = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == Data.ReferenceId && x.AccountTypeId == UserAccountType.MerchantCashier && x.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online).Count();


                            //double TotalMerchantCredit = _HCoreContext.HCUAccountTransaction
                            //                                          .Where(x => x.AccountId == Data.ReferenceId &&
                            //                                     x.SourceId == TransactionSource.Merchant &&
                            //                                     x.ModeId == TransactionMode.Credit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.Amount) ?? 0;

                            //double TotalMerchantDebit = _HCoreContext.HCUAccountTransaction
                            //                                          .Where(x => x.AccountId == Data.ReferenceId &&
                            //                                     x.SourceId == TransactionSource.Merchant &&
                            //                                     x.ModeId == TransactionMode.Debit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            //_AccountSetupOverview.PointsAvailable = Math.Round((TotalMerchantCredit - TotalMerchantDebit), 2);




                            //double TotalStoreCredit = _HCoreContext.HCUAccountTransaction
                            //                                          .Where(x => x.Account.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline &&
                            //                                     x.SourceId == TransactionSource.Store &&
                            //                                     x.ModeId == TransactionMode.Credit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.Amount) ?? 0;

                            //double TotalStoreDebit = _HCoreContext.HCUAccountTransaction
                            //                                      .Where(x => x.Account.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline &&
                            //                                     x.SourceId == TransactionSource.Store &&
                            //                                                 x.ModeId == TransactionMode.Debit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            //_AccountSetupOverview.StorePointAvailable = Math.Round((TotalStoreCredit - TotalStoreDebit), 2);






                            //double TotalCashierCredit = _HCoreContext.HCUAccountTransaction
                            //                                         .Where(x => x.Account.Owner.OwnerId == Data.ReferenceId &&
                            //                                                x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline &&
                            //                                    x.SourceId == TransactionSource.Cashier &&
                            //                                    x.ModeId == TransactionMode.Credit &&
                            //                                    x.StatusId == StatusHelperId.Transaction_Success)
                            //                             .Sum(x => (double?)x.Amount) ?? 0;

                            //double TotalCashierDebit = _HCoreContext.HCUAccountTransaction
                            //                                        .Where(x => x.Account.Owner.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Offline &&
                            //                                                 x.SourceId == TransactionSource.Cashier &&
                            //                                                 x.ModeId == TransactionMode.Debit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            //_AccountSetupOverview.CashierPointAvailable = Math.Round((TotalCashierCredit - TotalCashierDebit), 2);







                            //double TotalOnlineStoreCredit = _HCoreContext.HCUAccountTransaction
                            //                                             .Where(x => x.Account.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online &&
                            //                                     x.SourceId == TransactionSource.Store &&
                            //                                     x.ModeId == TransactionMode.Credit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.Amount) ?? 0;

                            //double TotalOnlineStoreDebit = _HCoreContext.HCUAccountTransaction
                            //                                            .Where(x => x.Account.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online &&
                            //                                     x.SourceId == TransactionSource.Store &&
                            //                                                 x.ModeId == TransactionMode.Debit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            //_AccountSetupOverview.OnlineStorePointAvailable = Math.Round((TotalOnlineStoreCredit - TotalOnlineStoreDebit), 2);






                            //double TotalOnlineCashierCredit = _HCoreContext.HCUAccountTransaction
                            //                                               .Where(x => x.Account.Owner.OwnerId == Data.ReferenceId &&
                            //                                                x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online &&
                            //                                    x.SourceId == TransactionSource.Cashier &&
                            //                                    x.ModeId == TransactionMode.Credit &&
                            //                                    x.StatusId == StatusHelperId.Transaction_Success)
                            //                             .Sum(x => (double?)x.Amount) ?? 0;

                            //double TotalOnlineCashierDebit = _HCoreContext.HCUAccountTransaction
                            //                                              .Where(x => x.Account.Owner.OwnerId == Data.ReferenceId &&
                            //                                                 x.Account.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online &&
                            //                                                 x.SourceId == TransactionSource.Cashier &&
                            //                                                 x.ModeId == TransactionMode.Debit &&
                            //                                     x.StatusId == StatusHelperId.Transaction_Success)
                            //                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                            //_AccountSetupOverview.OnlineAppPointAvailable = Math.Round((TotalOnlineCashierCredit - TotalOnlineCashierDebit), 2);



                            //Data.AccountSetupOverview = _AccountSetupOverview;


                        }

                        //if (Data.AccountTypeId == UserAccountType.TerminalAccount)
                        //{
                        //    var MerchantDetails = _HCoreContext.HCUAccountOwner.Where(x => x.Owner.AccountTypeId == UserAccountType.Merchant
                        //    && x.UserAccountId == Data.ReferenceId).Select(x => new
                        //    {
                        //        x.Owner.Guid,
                        //        x.Owner.DisplayName
                        //    }).FirstOrDefault();
                        //    if (MerchantDetails != null)
                        //    {
                        //        Data.MerchantKey = MerchantDetails.Guid;
                        //        Data.MerchantDisplayName = MerchantDetails.DisplayName;
                        //    }

                        //    var StoreDetails = _HCoreContext.HCUAccountOwner.Where(x => x.Owner.AccountTypeId == UserAccountType.MerchantStore
                        //    && x.UserAccountId == Data.ReferenceId).Select(x => new
                        //    {
                        //        x.Owner.Guid,
                        //        x.Owner.DisplayName,
                        //        x.Owner.Address,
                        //    }).FirstOrDefault();
                        //    if (StoreDetails != null)
                        //    {
                        //        Data.StoreKey = StoreDetails.Guid;
                        //        Data.StoreDisplayName = StoreDetails.DisplayName;
                        //        Data.StoreAddress = StoreDetails.Address;
                        //    }
                        //    var AcquirerDetails = _HCoreContext.HCUAccountOwner.Where(x => x.Owner.AccountTypeId == UserAccountType.Acquirer
                        //   && x.UserAccountId == Data.ReferenceId).Select(x => new
                        //   {
                        //       x.Owner.Guid,
                        //       x.Owner.DisplayName
                        //   }).FirstOrDefault();
                        //    if (AcquirerDetails != null)
                        //    {
                        //        Data.AcquirerKey = AcquirerDetails.Guid;
                        //        Data.AcquirerDisplayName = AcquirerDetails.DisplayName;
                        //    }
                        //}
                        if (Data.AccountTypeId == UserAccountType.Appuser && Data.CreatedByAccountTypeId != null)
                        {
                            //if (Data.CreatedByAccountTypeId == UserAccountType.TerminalAccount)
                            //{
                            //    if (Data.CreatedById != null)
                            //    {
                            //        var SubOwnerDetails = _HCoreContext.HCUAccountOwner
                            //   .Where(x => x.AccountId == Data.CreatedById
                            //           && x.Owner.AccountTypeId == UserAccountType.MerchantStore)
                            //           .Select(x => new
                            //           {
                            //               SubOwnerKey = x.Owner.Guid,
                            //               SubOwnerDisplayName = x.Owner.DisplayName,
                            //               SubOwnerAddress = x.Owner.Address,

                            //           }).FirstOrDefault();
                            //        if (SubOwnerDetails != null)
                            //        {
                            //            Data.SubOwnerKey = SubOwnerDetails.SubOwnerKey;
                            //            Data.SubOwnerDisplayName = SubOwnerDetails.SubOwnerDisplayName;
                            //            Data.SubOwnerAddress = SubOwnerDetails.SubOwnerAddress;
                            //        }
                            //    }
                            //}
                        }

                        //Data.LastActivityTime = _HCoreContext.HCApiUsage.Where(x => x.AccountId == Data.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                        if (!string.IsNullOrEmpty(Data.AccessPin))
                        {
                            Data.IsAccessPinSet = 1;
                        }
                        else
                        {
                            Data.IsAccessPinSet = 0;
                        }
                        if (!string.IsNullOrEmpty(Data.IconUrl))
                        {
                            Data.IconUrl = HCoreConstant._AppConfig.StorageUrl + Data.IconUrl;
                        }
                        else
                        {
                            Data.IconUrl = HCoreConstant._AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Data.OwnerIconUrl))
                        {
                            Data.OwnerIconUrl = HCoreConstant._AppConfig.StorageUrl + Data.OwnerIconUrl;
                        }
                        else
                        {
                            Data.OwnerIconUrl = HCoreConstant._AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Data.PosterUrl))
                        {
                            Data.PosterUrl = HCoreConstant._AppConfig.StorageUrl + Data.PosterUrl;
                        }
                        else
                        {
                            Data.PosterUrl = HCoreConstant._AppConfig.Default_Poster;
                        }
                        #region Get User
                        Data.User = (from n in _HCoreContext.HCUAccount
                                     where n.Id == Data.ReferenceId
                                     select new OUser
                                     {
                                         ReferenceId = n.User.Id,
                                         ReferenceKey = n.User.Guid,

                                         UserName = n.User.Username,
                                         Name = n.Name,
                                         FirstName = n.FirstName,
                                         LastName = n.LastName,

                                         ContactNumber = n.ContactNumber,
                                         EmailAddress = n.EmailAddress,

                                         Gender = n.Gender.Name,
                                         GenderCode = n.Gender.SystemName,

                                         DateOfBirth = n.DateOfBirth,

                                         Address = n.Address,
                                         AddressLatitude = n.Latitude,
                                         AddressLongitude = n.Longitude,

                                         EmailVerificationStatus = n.EmailVerificationStatus,
                                         EmailVerificationStatusDate = n.EmailVerificationStatusDate,

                                         NumberVerificationStatus = n.NumberVerificationStatus,
                                         NumberVerificationStatusDate = n.NumberVerificationStatusDate,

                                         CountryKey = n.Country.Guid,
                                         CountryName = n.Country.Name,


                                         RegionKey = n.State.Guid,
                                         RegionName = n.State.Name,

                                         RegionAreaKey = n.State.Guid,
                                         RegionAreaName = n.State.Name,

                                         CityKey = n.City.Guid,
                                         CityName = n.City.Name,

                                         CityAreaKey = n.CityArea.Guid,
                                         CityAreaName = n.CityArea.Name,



                                         CreateDate = n.CreateDate,
                                         CreatedByKey = n.CreatedBy.Guid,
                                         CreatedByDisplayName = n.CreatedBy.DisplayName,

                                         ModifyDate = n.ModifyDate,
                                         ModifyByKey = n.ModifyBy.Guid,
                                         ModifyByDisplayName = n.ModifyBy.DisplayName,

                                         Status = n.StatusId,
                                         StatusCode = n.Status.SystemName,
                                         StatusName = n.Status.Name,
                                     }).FirstOrDefault();
                        #endregion
                        #region Linked Accounts
                        Data.User.LinkedAccounts = (from n in _HCoreContext.HCUAccount
                                                    where n.UserId == Data.User.ReferenceId
                                                    select new OUserAccountAccess.OUserLinkedAccounts
                                                    {
                                                        AccountId = n.Id,
                                                        OwnerName = n.Owner.DisplayName,
                                                        DisplayName = n.DisplayName,
                                                        AccountKey = n.Guid,
                                                        AccountType = n.AccountType.Name,
                                                        AccountTypeCode = n.AccountType.SystemName,
                                                        IconUrl = n.IconStorage.Path,
                                                        Status = n.StatusId,
                                                        CreateDate = n.CreateDate,
                                                        IsActiveSession = 0,
                                                    }).ToList();
                        foreach (var LinkedAccount in Data.User.LinkedAccounts)
                        {
                            if (LinkedAccount.AccountId == Data.ReferenceId)
                            {
                                LinkedAccount.IsActiveSession = 1;
                            }
                            else
                            {
                                LinkedAccount.IsActiveSession = 0;
                            }

                            if (!string.IsNullOrEmpty(LinkedAccount.IconUrl))
                            {
                                LinkedAccount.IconUrl = HCoreConstant._AppConfig.StorageUrl + LinkedAccount.IconUrl;
                            }
                            else
                            {
                                LinkedAccount.IconUrl = HCoreConstant._AppConfig.Default_Icon;
                            }
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }



                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountSelf(OUserAccount _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare

                #endregion
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get User Details
                    var Details = (from n in _HCoreContext.HCUAccount
                                   where n.Id == _Request.UserReference.AccountId
                                   select new
                                   {
                                       UserAccountId = n.Id,
                                       UserAccountKey = n.Guid,

                                       AccountTypeCode = n.AccountType.SystemName,
                                       AccountTypeName = n.AccountType.Name,

                                       UserId = n.UserId,
                                       UserKey = n.Guid,
                                       Password = n.User.Password,

                                       OwnerId = n.OwnerId,
                                       Status = n.StatusId,

                                       UserName = n.User.Username,

                                       DisplayName = n.DisplayName,
                                       Name = n.Name,
                                       FirstName = n.FirstName,
                                       LastName = n.LastName,

                                       EmailAddress = n.EmailAddress,
                                       ContactNumber = n.ContactNumber,

                                       DateOfBirth = n.DateOfBirth,

                                       Gender = n.Gender.Name,
                                       GenderCode = n.Gender.SystemName,

                                       AccountCode = n.AccountCode,
                                       ReferralCode = n.ReferralCode,
                                       ReferralUrl = n.ReferralUrl,

                                       IconUrl = n.IconStorage.Path,
                                       PosterUrl = n.PosterStorage.Path,

                                       Address = n.Address,
                                       AddressLatitude = n.Latitude,
                                       AddressLongitude = n.Longitude,

                                       EmailVerificationStatus = n.EmailVerificationStatus,
                                       ContactNumberVerificationStatus = n.NumberVerificationStatus,

                                       CountryId = n.CountryId,
                                       CreateDate = n.CreateDate,
                                       ModifyDate = n.ModifyDate,
                                       AccessPin = n.AccessPin,
                                   }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.Status == StatusActive)
                        {
                            _HCoreContext.Dispose();
                            #region Variables & Declarations
                            _UserAuthResponse = new OUserAccountAccess.OResponse();
                            #endregion
                            #region Country  || Roles || Owner || Account  || Linked Accounts
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Set User Object
                                _ObjectUser = new OUserAccountAccess.OUser();
                                _ObjectUser.UserName = Details.UserName;

                                _ObjectUser.Name = Details.Name;
                                _ObjectUser.FirstName = Details.FirstName;
                                _ObjectUser.LastName = Details.LastName;

                                _ObjectUser.ContactNumber = Details.ContactNumber;
                                _ObjectUser.EmailAddress = Details.EmailAddress;

                                _ObjectUser.Gender = Details.Gender;
                                _ObjectUser.GenderCode = Details.Gender;

                                _ObjectUser.DateOfBirth = Details.DateOfBirth;

                                _ObjectUser.Address = Details.Address;
                                _ObjectUser.AddressLatitude = Details.AddressLatitude;
                                _ObjectUser.AddressLongitude = Details.AddressLongitude;

                                _ObjectUser.EmailVerificationStatus = Details.EmailVerificationStatus;
                                _ObjectUser.ContactNumberVerificationStatus = Details.ContactNumberVerificationStatus;
                                _ObjectUser.LinkedAccounts = (from n in _HCoreContext.HCUAccount
                                                              where n.UserId == Details.UserId
                                                              select new OUserAccountAccess.OUserLinkedAccounts
                                                              {
                                                                  AccountId = n.Id,
                                                                  AccountTypeId = n.AccountTypeId,
                                                                  OwnerName = n.Owner.DisplayName,
                                                                  DisplayName = n.DisplayName,
                                                                  AccountKey = n.Guid,
                                                                  AccountCode = n.AccountCode,
                                                                  AccountType = n.AccountType.Name,
                                                                  AccountTypeCode = n.AccountType.SystemName,
                                                                  Status = n.StatusId,
                                                                  IsActiveSession = 0,
                                                                  CardNumber = n.Card.CardNumber,
                                                                  CardTypeName = n.Card.CardType.Name,
                                                                  CreateDate = n.CreateDate
                                                              }).ToList();
                                foreach (var LinkedAccount in _ObjectUser.LinkedAccounts)
                                {
                                    if (LinkedAccount.AccountTypeId == UserAccountType.Appuser)
                                    {
                                        OTransaction.Balance.Request _BalanceRequest;
                                        ManageUserTransaction _ManageUserTransaction;
                                        _BalanceRequest = new OTransaction.Balance.Request();
                                        _BalanceRequest.UserReference = _Request.UserReference;
                                        _BalanceRequest.UserAccountKey = LinkedAccount.AccountKey;
                                        if (LinkedAccount.AccountTypeId == UserAccountType.Appuser)
                                        {
                                            _BalanceRequest.Source = TransactionSource.AppS;
                                        }
                                        //if (LinkedAccount.AccountTypeId == UserAccountType.Carduser)
                                        //{
                                        //    _BalanceRequest.Source = TransactionSource.AppS;
                                        //}
                                        //_BalanceRequest.Source = LinkedAccount.AccountKey;
                                        _ManageUserTransaction = new ManageUserTransaction();
                                        var ResponseData = _ManageUserTransaction.GetUserAccountBalance(_BalanceRequest);
                                        if (ResponseData.Status == StatusSuccess)
                                        {
                                            LinkedAccount.Balance = (OTransaction.Balance.Response)ResponseData.Result;
                                            LinkedAccount.Balance.Credit = (LinkedAccount.Balance.Credit / 100) * 10;
                                            LinkedAccount.Balance.Debit = (LinkedAccount.Balance.Debit / 100) * 10;
                                            LinkedAccount.Balance.Balance = (LinkedAccount.Balance.Balance / 100) * 10;
                                        }
                                    }
                                    if (LinkedAccount.AccountId == _Request.UserReference.AccountId)
                                    {
                                        LinkedAccount.IsActiveSession = 1;
                                    }
                                    else
                                    {
                                        LinkedAccount.IsActiveSession = 0;
                                    }

                                    if (!string.IsNullOrEmpty(LinkedAccount.IconUrl))
                                    {
                                        LinkedAccount.IconUrl = HCoreConstant._AppConfig.StorageUrl + LinkedAccount.IconUrl;
                                    }
                                    else
                                    {
                                        LinkedAccount.IconUrl = HCoreConstant._AppConfig.Default_Icon;
                                    }
                                }
                                _UserAuthResponse.User = _ObjectUser;
                                #endregion
                                #region Country
                                _UserAuthResponse.UserCountry = (from n in _HCoreContext.HCCoreCommon
                                                                 where n.Id == Details.CountryId
                                                                 select new OUserAccountAccess.OUserAccountCountry
                                                                 {
                                                                     CountryKey = n.Guid,
                                                                     CountryName = n.Name,
                                                                     CountryIso = n.Value,
                                                                     CountryIsd = n.SubValue,
                                                                     //CountryIconUrl = n.IconUrl,
                                                                     CurrencyName = n.Description,
                                                                     CurrencyNotation = n.Data,
                                                                     CurrencySymbol = n.Data,
                                                                 }).FirstOrDefault();
                                #endregion
                                #region Roles And Permissions
                                //_ObjectUserRole = new List<OUserAccountAccess.OUserAccountRole>();
                                //var UserRoles = (from n in _HCoreContext.HCUAccountRoleAccess
                                //                 where n.UserAccountId == Details.UserAccountId && n.StatusId == StatusActive
                                //                 select new
                                //                 {
                                //                     ReferenceId = n.Id,
                                //                     RoleId = n.UserAccountRoleId,
                                //                     RoleName = n.UserAccountRole.Name,
                                //                     SystemName = n.UserAccountRole.SystemName,
                                //                 }).ToList();
                                //foreach (var UserRole in UserRoles)
                                //{
                                //    #region Get User Role Permissions
                                //    //List<OUserAccountAccess.OUserAccountRolePermission> UserRolePermissions = (from n in _HCoreContext.HCUAccountRolePermission
                                //    //                                                                           where n.AccountTypeRoleId == UserRole.RoleId && n.StatusId == StatusActive
                                //    //                                                                           select new OUserAccountAccess.OUserAccountRolePermission
                                //    //                                                                           {
                                //    //                                                                               PermissionId = n.AccountTypePermission.SystemPermissionId,
                                //    //                                                                               Name = n.AccountTypePermission.SystemPermission.Name,
                                //    //                                                                               SystemName = n.AccountTypePermission.SystemPermission.SystemName,
                                //    //                                                                               IsDefault = n.AccountTypePermission.SystemPermission.IsDefaultPermission,
                                //    //                                                                           }).ToList();

                                //    List<OUserAccountAccess.OUserAccountRolePermission> UserRolePermissions = HCoreDataStore.UserRolePermission
                                //        .Where(x => x.AccountTypeRoleId == UserRole.RoleId)
                                //        .Select(x => new OUserAccountAccess.OUserAccountRolePermission
                                //        {
                                //            PermissionId = x.SystemPermissionId,
                                //            Name = x.Name,
                                //            SystemName = x.SystemName,
                                //            IsDefault = x.IsDefault,
                                //        }).ToList();
                                //    foreach (var UserRolePermission in UserRolePermissions)
                                //    {
                                //        //int AccessPinRequiredCount = _HCoreContext.HCApi.Where(x => x.SystemPermissionId == UserRolePermission.PermissionId && x.IsAccessPinRequired == 1).Select(x => x.Id).Count();
                                //        int AccessPinRequiredCount = HCoreDataStore.Api.Where(x => x.SystemPermissionId == UserRolePermission.PermissionId && x.IsAccessPinRequired == 1).Select(x => x.ReferenceId).Count();
                                //        if (AccessPinRequiredCount > 0)
                                //        {
                                //            UserRolePermission.IsAccessPinRequired = 1;
                                //        }
                                //        else
                                //        {
                                //            UserRolePermission.IsAccessPinRequired = 0;
                                //        }

                                //        //int PasswordRequiredCount = _HCoreContext.HCApi.Where(x => x.SystemPermissionId == UserRolePermission.PermissionId && x.IsPasswordRequired == 1).Select(x => x.Id).Count();
                                //        int PasswordRequiredCount = HCoreDataStore.Api.Where(x => x.SystemPermissionId == UserRolePermission.PermissionId && x.IsPasswordRequired == 1).Select(x => x.ReferenceId).Count();
                                //        if (PasswordRequiredCount > 0)
                                //        {
                                //            UserRolePermission.IsPasswordRequired = 1;
                                //        }
                                //        else
                                //        {
                                //            UserRolePermission.IsPasswordRequired = 0;
                                //        }
                                //    }
                                //    _ObjectUserRole.Add(new OUserAccountAccess.OUserAccountRole
                                //    {
                                //        Name = UserRole.RoleName,
                                //        SystemName = UserRole.SystemName,
                                //        RolePermissions = UserRolePermissions
                                //    });
                                //    #endregion
                                //}
                                //_UserAuthResponse.UserRoles = _ObjectUserRole;
                                #endregion
                                #region Owner
                                if (Details.OwnerId != null && Details.OwnerId != 0)
                                {
                                    _UserAuthResponse.UserOwner = _HCoreContext.HCUAccount.Where(x => x.Id == Details.OwnerId).Select(x => new OUserAccountAccess.OUserAccountOwner
                                    {
                                        AccountCode = x.AccountCode,
                                        DisplayName = x.DisplayName,
                                        Name = x.Name,
                                        IconUrl = x.IconStorage.Path,
                                    }).FirstOrDefault();
                                    if (_UserAuthResponse.UserOwner != null)
                                    {
                                        if (!string.IsNullOrEmpty(_UserAuthResponse.UserOwner.IconUrl))
                                        {
                                            _UserAuthResponse.UserOwner.IconUrl = HCoreConstant._AppConfig.StorageUrl + _UserAuthResponse.UserOwner.IconUrl;
                                        }
                                        else
                                        {
                                            _UserAuthResponse.UserOwner.IconUrl = HCoreConstant._AppConfig.Default_Icon;
                                        }
                                    }
                                }
                                #endregion
                                #region User Account


                                _ObjectUserAccount = new OUserAccountAccess.OUserAccount();
                                _ObjectUserAccount.UserKey = Details.UserKey;
                                _ObjectUserAccount.DisplayName = Details.DisplayName;
                                _ObjectUserAccount.AccountKey = Details.UserAccountKey;
                                _ObjectUserAccount.AccountType = Details.AccountTypeName;
                                _ObjectUserAccount.AccountTypeCode = Details.AccountTypeCode;

                                _ObjectUserAccount.ReferralCode = Details.ReferralCode;
                                _ObjectUserAccount.ReferralUrl = Details.ReferralUrl;
                                _ObjectUserAccount.AccountCode = Details.AccountCode;
                                _ObjectUserAccount.CreateDate = Details.CreateDate;
                                if (!string.IsNullOrEmpty(Details.AccessPin))
                                {
                                    _ObjectUserAccount.IsAccessPinSet = 1;
                                }
                                else
                                {
                                    _ObjectUserAccount.IsAccessPinSet = 0;
                                }
                                if (!string.IsNullOrEmpty(Details.IconUrl))
                                {
                                    _ObjectUserAccount.IconUrl = HCoreConstant._AppConfig.StorageUrl + Details.IconUrl;
                                }
                                else
                                {
                                    _ObjectUserAccount.IconUrl = HCoreConstant._AppConfig.Default_Icon;
                                }
                                if (!string.IsNullOrEmpty(Details.PosterUrl))
                                {
                                    _ObjectUserAccount.PosterUrl = HCoreConstant._AppConfig.StorageUrl + Details.PosterUrl;
                                }
                                else
                                {
                                    _ObjectUserAccount.PosterUrl = HCoreConstant._AppConfig.Default_Poster;
                                }
                                _UserAuthResponse.UserAccount = _ObjectUserAccount;
                                #endregion
                            }
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _UserAuthResponse, "HC1087");
                            #endregion
                        }
                        else if (Details.Status == StatusInactive)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1088");
                            #endregion
                        }
                        else if (Details.Status == StatusBlocked)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1089");
                            #endregion
                        }
                        else if (Details.Status == StatusArchived)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1090");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1091");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1092");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetUserAccountSelf", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1093");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the user account status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccountStatus(OUserUpdateAccountStatus _Request)
        {

            #region Manage Exception
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(_Request.UserAccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAA100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAA103");
                    #endregion
                }
                #endregion
                #region Operation
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAA104");
                            #endregion
                        }
                        var UserDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            UserDetails.StatusId = StatusId;
                            var UserProfile = _HCoreContext.HCUAccountAuth.Where(x => x.Id == UserDetails.UserId).FirstOrDefault();
                            UserProfile.StatusId = StatusId;

                            var UserSubAccounts = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserDetails.Id).ToList();
                            foreach (var UserSubAccount in UserSubAccounts)
                            {
                                UserSubAccount.StatusId = StatusId;
                                var UserProfileS = _HCoreContext.HCUAccount.Where(x => x.Id == UserSubAccount.UserId).FirstOrDefault();
                                UserProfileS.StatusId = StatusId;

                                var UserSubSubAccounts = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserSubAccount.Id).ToList();
                                foreach (var UserSubSubAccount in UserSubSubAccounts)
                                {
                                    UserSubSubAccount.StatusId = StatusId;
                                    var UserProfileC = _HCoreContext.HCUAccount.Where(x => x.Id == UserSubSubAccount.UserId).FirstOrDefault();
                                    UserProfileC.StatusId = StatusId;

                                }
                            }
                            _HCoreContext.SaveChanges();

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCAA102");
                            #endregion
                        }
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAA101");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccountStatus", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1150");
                #endregion
            }
            #endregion
        }
    }
}
