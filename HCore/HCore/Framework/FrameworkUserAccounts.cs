//==================================================================================
// FileName: FrameworkUserAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Framework
{
    internal class FrameworkUserAccounts
    {
        #region Entity
        HCoreContext _HCoreContext;
        #endregion
        #region Objects
        ORewardBalance _ORewardBalance;
        OSettlement _OSettlement;
        ORewards _ORewards;
        OVistors _OVistors;
        OPayments _OPayments;
        OCardsOverview _OCardsOverview;
        OUserOverview.Response _OUserOverviewResponse;
        OChart.Request _OChartRequest;
        List<OChart.RequestDateRange> _OChartRequestRange;
        List<OChart.RequestDateRange> _TChartRequestRange;
        List<OChart.RequestData> _OChartRequestRangeValue;
        #endregion 
        #region Get 
        /// <summary>
        /// Gets the user account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountOverview(OUserOverview.Request _Request)
        {
            #region Declare

            _OUserOverviewResponse = new OUserOverview.Response();
            #endregion
            #region Manage Exception
            try
            {
                #region Set User Reference Key 
                if (string.IsNullOrEmpty(_Request.UserAccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime StartTime = DateTime.UtcNow.Date.AddDays(-1);
                    DateTime EndTime = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1);
                    OUserOverview.Account UserAccount = _HCoreContext.HCUAccount
                                                   .Where(x => x.Guid == _Request.UserAccountKey)
                                                   .Select(x => new OUserOverview.Account
                                                   {
                                                       OwnerId = x.Owner.Id,
                                                       OwnerParentId = x.Owner.Owner.Id,
                                                       ReferenceId = x.Id,
                                                       CreatedById = x.CreatedById,
                                                       CreateDate = x.CreateDate,
                                                       AccountOperationTypeId = x.AccountOperationTypeId,
                                                   }).FirstOrDefault();
                    if (UserAccount != null)
                    {
                        if (_Request.StartTime != null)
                        {
                            StartTime = (DateTime)_Request.StartTime;
                        }
                        if (_Request.EndTime != null)
                        {
                            EndTime = (DateTime)_Request.EndTime;
                        }
                        #region System Helpers
                        var TransactionTypes = _HCoreContext.HCCore
                                                            .Where(x => x.ParentId == HelperType.TransactionType)
                                                      .Select(x => new
                                                      {
                                                          ReferenceId = x.Id,
                                                          Name = x.Name,
                                                          Value = x.Value
                                                      }).ToList();
                        #endregion
                        if (_Request.UserReference.Task == HCoreTask.Get_MerchantOverview)
                        {
                            #region RewardBalance
                            _ORewardBalance = new ORewardBalance();
                            _ORewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Id == UserAccount.ReferenceId &&
                                       x.ModeId == TransactionMode.Credit &&
                                       x.TypeId == TransactionType.MerchantCredit &&
                                       x.SourceId == TransactionSource.Merchant
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Balance = _ORewardBalance.Credit - _ORewardBalance.Debit;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewardBalance.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewardBalance.Comission = _ORewardBalance.Comission + _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Id == UserAccount.ReferenceId &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewardBalance.Charge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                            ).Sum(x => (double?)x.Charge) ?? 0;

                                _ORewardBalance.Charge = _ORewardBalance.Charge + _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Id == UserAccount.ReferenceId &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.RewardBalance = _ORewardBalance;
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.AccountId).Distinct().Count();
                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OVistors.SingleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                       a.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId).Count() == 1)
                                    .Select(x => x.Id).Count();

                            _OVistors.MultipleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                    a.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId).Count() > 1)
                                    .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                         x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                         x.Type.Value == TransactionTypeValue.Reward &&
                                         x.ModeId == TransactionMode.Debit &&
                                         x.SourceId == TransactionSource.Cashier
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                        x.Type.Value == TransactionTypeValue.Redeem &&
                                       x.ModeId == TransactionMode.Debit &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;

                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward Overview
                            _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.PaymentsOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id && a.ModeId == TransactionMode.Debit
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id && a.ModeId == TransactionMode.Debit
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id && a.ModeId == TransactionMode.Debit
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit &&
                                           a.TypeId == RewardType.Id &&
                                        (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments
                                  && x.ModeId == TransactionMode.Debit &&
                                       x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();


                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                    x.ModeId == TransactionMode.Debit &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                    x.ModeId == TransactionMode.Debit &&

                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                    x.ModeId == TransactionMode.Debit &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                    x.ModeId == TransactionMode.Debit &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion

                            #region Settlements
                            _OSettlement = new OSettlement();

                            _OSettlement.Completed = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                      (x.TypeId == TransactionType.BankSettlement || x.TypeId == TransactionType.PointSettlement) &&
                                      x.ModeId == TransactionMode.Debit
                                      )
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OSettlement.Pending = (_HCoreContext.HCUAccountTransaction
                              .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                  x.ModeId == TransactionMode.Credit &&
                                     x.SourceId == TransactionSource.Settlement
                                     )
                              .Sum(x => (double?)x.TotalAmount) ?? 0) - _OSettlement.Completed;
                            _OUserOverviewResponse.Settlements = _OSettlement;
                            #endregion



                            #region Cards Overview 
                            _OCardsOverview = new OCardsOverview();
                            _OCardsOverview.Total = _HCoreContext.TUCard
                                .Where(x => x.ActiveMerchantId == UserAccount.ReferenceId)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.Assigned = _HCoreContext.TUCard
                                .Where(x => x.ActiveMerchantId == UserAccount.ReferenceId && x.ActiveUserAccountId != null)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.NotAssigned = _OCardsOverview.Total - _OCardsOverview.Assigned;
                            _OUserOverviewResponse.CardsOverview = _OCardsOverview;
                            #endregion



                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_StoreOverview)
                        {

                            #region RewardBalance
                            _ORewardBalance = new ORewardBalance();
                            _ORewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.Store
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Balance = _ORewardBalance.Credit - _ORewardBalance.Debit;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewardBalance.Comission = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewardBalance.Comission = _ORewardBalance.Comission + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewardBalance.Charge = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                            ).Sum(x => (double?)x.Charge) ?? 0;

                                _ORewardBalance.Charge = _ORewardBalance.Charge + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.RewardBalance = _ORewardBalance;
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.AccountId).Distinct().Count();
                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OVistors.SingleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                       a.CreatedBy.Owner.Id == UserAccount.ReferenceId).Count() == 1)
                                    .Select(x => x.Id).Count();

                            _OVistors.MultipleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                    a.CreatedBy.Owner.Id == UserAccount.ReferenceId).Count() > 1)
                                    .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem)
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              (
                                            x.Type.Value == TransactionTypeValue.Reward) &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                     .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                            (x.Type.Value == TransactionTypeValue.Redeem) &&
                                            x.ModeId == TransactionMode.Debit && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                             (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                //_ORewards.TotalComission = _ORewards.TotalComission + _HCoreContext.HCUAccountTransaction
                                // .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                //        (x.Account.AccountTypeId == UserAccountType.Carduser || x.Account.AccountTypeId == UserAccountType.Appuser))
                                //.Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;


                                //_ORewards.TotalCharge = _ORewards.TotalCharge + _HCoreContext.HCUAccountTransaction
                                // .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                //        (x.Account.AccountTypeId == UserAccountType.Carduser || x.Account.AccountTypeId == UserAccountType.Appuser))
                                //.Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                               .Select(x => new OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                               .Select(x => new OUserTransactionType
                               {
                                   Id = x.ReferenceId,
                                   Name = x.Name
                               }).ToList();
                            }
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                            .Select(x => new OUserTransactionType
                            {
                                Id = x.ReferenceId,
                                Name = x.Name
                            }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                                            .Select(x => new OUserTransactionType
                                                            {
                                                                Id = x.ReferenceId,
                                                                Name = x.Name
                                                            }).ToList();
                            }

                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit &&
                                           a.TypeId == RewardType.Id &&
                                        (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Cards Overview 
                            _OCardsOverview = new OCardsOverview();
                            _OCardsOverview.Total = _HCoreContext.TUCard
                                .Where(x => x.ActiveMerchantId == UserAccount.OwnerId)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.Assigned = _HCoreContext.HCUAccount
                                .Where(x => x.OwnerId == UserAccount.ReferenceId && x.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.NotAssigned = _OCardsOverview.Total - _OCardsOverview.Assigned;
                            _OUserOverviewResponse.CardsOverview = _OCardsOverview;
                            #endregion



                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments &&
                                       x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion
                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_CashierOverview)
                        {

                            #region RewardBalance
                            _ORewardBalance = new ORewardBalance();
                            _ORewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Balance = _ORewardBalance.Credit - _ORewardBalance.Debit;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewardBalance.Comission = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewardBalance.Comission = _ORewardBalance.Comission + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewardBalance.Charge = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                            ).Sum(x => (double?)x.Charge) ?? 0;

                                _ORewardBalance.Charge = _ORewardBalance.Charge + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                             x.SourceId == TransactionSource.Merchant
                                            ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.RewardBalance = _ORewardBalance;
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.AccountId).Distinct().Count();
                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OVistors.SingleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                       a.CreatedBy.Id == UserAccount.ReferenceId).Count() == 1)
                                    .Select(x => x.Id).Count();

                            _OVistors.MultipleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                    a.CreatedBy.Id == UserAccount.ReferenceId).Count() > 1)
                                    .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                               (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                                x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                              (
                                            x.Type.Value == TransactionTypeValue.Reward) &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                     .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                            (x.Type.Value == TransactionTypeValue.Redeem) &&
                                            x.ModeId == TransactionMode.Debit && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                             (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                //_ORewards.TotalComission = _ORewards.TotalComission + _HCoreContext.HCUAccountTransaction
                                // .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                //        (x.Account.AccountTypeId == UserAccountType.Carduser || x.Account.AccountTypeId == UserAccountType.Appuser))
                                //.Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;


                                //_ORewards.TotalCharge = _ORewards.TotalCharge + _HCoreContext.HCUAccountTransaction
                                // .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                //        (x.Account.AccountTypeId == UserAccountType.Carduser || x.Account.AccountTypeId == UserAccountType.Appuser))
                                //.Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit &&
                                           a.TypeId == RewardType.Id &&
                                        (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region Cards Overview 
                            _OCardsOverview = new OCardsOverview();
                            _OCardsOverview.Total = _HCoreContext.TUCard
                                .Where(x => x.ActiveMerchantId == UserAccount.OwnerParentId)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.Assigned = _HCoreContext.HCUAccount
                                .Where(x => x.CreatedById == UserAccount.ReferenceId &&
                                x.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.NotAssigned = _OCardsOverview.Total - _OCardsOverview.Assigned;
                            _OUserOverviewResponse.CardsOverview = _OCardsOverview;
                            #endregion


                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments &&
                                       x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion
                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_CardUserOverview && _Request.Type == ListType.Merchants)
                        {

                            #region RewardBalance
                            _ORewardBalance = new ORewardBalance();
                            _ORewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&

                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.TUC
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.TUC
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Balance = _ORewardBalance.Credit - _ORewardBalance.Debit;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewardBalance.Comission = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewardBalance.Comission = _ORewardBalance.Comission + _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.SourceId == TransactionSource.TUC
                                            ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewardBalance.Charge = _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                            ).Sum(x => (double?)x.Charge) ?? 0;

                                _ORewardBalance.Charge = _ORewardBalance.Charge + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.SourceId == TransactionSource.TUC
                                            ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.RewardBalance = _ORewardBalance;
                            #endregion
                            #region Visitors
                            _OVistors = new OVistors();

                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.SubUserAccountKey &&
                                               x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();

                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion
                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.SubUserAccountKey &&
                                               x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                               (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                                 x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              (
                                            x.Type.Value == TransactionTypeValue.Reward) &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.TUC
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                     .Where(x =>
                                            x.Account.Guid == _Request.SubUserAccountKey &&
                                            x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                            (x.Type.Value == TransactionTypeValue.Redeem) &&
                                            x.ModeId == TransactionMode.Debit &&
                                            x.SourceId == TransactionSource.TUC)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                             (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                //_ORewards.TotalComission = _ORewards.TotalComission + _HCoreContext.HCUAccountTransaction
                                // .Where(x =>
                                //        x.Account.Guid == _Request.SubUserAccountKey &&
                                //        x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&

                                //        x.Account.AccountTypeId == UserAccountType.Carduser)
                                //.Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x =>
                                           x.Account.Guid == _Request.SubUserAccountKey &&
                                           x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;


                                //_ORewards.TotalCharge = _ORewards.TotalCharge + _HCoreContext.HCUAccountTransaction
                                // .Where(x =>
                                //        x.Account.Guid == _Request.SubUserAccountKey &&
                                //        x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //        x.Account.AccountTypeId == UserAccountType.Carduser)
                                //.Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion
                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.PaymentsOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a =>
                                        a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region  Reward OVerview
                            _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Credit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a =>
                                          a.Account.Guid == _Request.SubUserAccountKey &&
                                          a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region  Redeem Overview
                            _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a =>
                                          a.Account.Guid == _Request.SubUserAccountKey &&
                                          a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                         a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a =>
                                        a.Account.Guid == _Request.SubUserAccountKey &&
                                        a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit &&
                                           a.TypeId == RewardType.Id &&
                                        a.SourceId == TransactionSource.TUC
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser).Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                      x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments &&
                                       x.Account.AccountTypeId == UserAccountType.Appuser)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion
                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_CardUserOverview && _Request.Type == ListType.CardUsers)
                        {

                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.PaymentsOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a =>
                                               a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.UserAccountKey)
                                .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem))
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem)
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey
                                           ).Sum(x => (double?)x.Charge) ?? 0;

                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward OVerview
                            _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.TUC
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                           a.TypeId == RewardType.Id && a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                               a.TypeId == RewardType.Id && a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                               a.TypeId == RewardType.Id && a.SourceId == TransactionSource.TUC
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion
                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_AppUserOverview && _Request.Type == ListType.Merchants)
                        {

                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a =>
                                        a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region RewardBalance
                            _ORewardBalance = new ORewardBalance();
                            _ORewardBalance.Credit = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.TUC
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Debit = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.TUC
                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewardBalance.Balance = _ORewardBalance.Credit - _ORewardBalance.Debit;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewardBalance.Comission = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewardBalance.Comission = _ORewardBalance.Comission + _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.SourceId == TransactionSource.TUC
                                            ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewardBalance.Charge = _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                            ).Sum(x => (double?)x.Charge) ?? 0;

                                _ORewardBalance.Charge = _ORewardBalance.Charge + _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.SourceId == TransactionSource.TUC
                                            ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.RewardBalance = _ORewardBalance;
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();

                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.SubUserAccountKey &&
                                               x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();

                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.SubUserAccountKey &&
                                               x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                               (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                                 x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                       .Where(x =>
                                              x.Account.Guid == _Request.SubUserAccountKey &&
                                              x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                              (
                                            x.Type.Value == TransactionTypeValue.Reward) &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.SourceId == TransactionSource.TUC
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                     .Where(x =>
                                            x.Account.Guid == _Request.SubUserAccountKey &&
                                            x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                            (x.Type.Value == TransactionTypeValue.Redeem) &&
                                            x.ModeId == TransactionMode.Debit &&
                                            x.SourceId == TransactionSource.TUC)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                      .Where(x =>
                                             x.Account.Guid == _Request.SubUserAccountKey &&
                                             x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                             (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                //_ORewards.TotalComission = _ORewards.TotalComission + _HCoreContext.HCUAccountTransaction
                                // .Where(x =>
                                //        x.Account.Guid == _Request.SubUserAccountKey &&
                                //        x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey && x.Account.AccountTypeId == UserAccountType.Appuser
                                //        )
                                //.Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x =>
                                           x.Account.Guid == _Request.SubUserAccountKey &&
                                           x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;


                                //_ORewards.TotalCharge = _ORewards.TotalCharge + _HCoreContext.HCUAccountTransaction
                                // .Where(x =>
                                //        x.Account.Guid == _Request.SubUserAccountKey &&
                                //        x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey && x.Account.AccountTypeId == UserAccountType.Appuser
                                //        )
                                //.Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward OVerview
                            _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Credit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a =>
                                          a.Account.Guid == _Request.SubUserAccountKey &&
                                          a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a =>
                                           a.Account.Guid == _Request.SubUserAccountKey &&
                                           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a =>
                                          a.Account.Guid == _Request.SubUserAccountKey &&
                                          a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                          a.TypeId == RewardType.Id && a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a =>
                                        a.Account.Guid == _Request.SubUserAccountKey &&
                                        a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit && a.SourceId == TransactionSource.TUC &&
                                           a.TypeId == RewardType.Id
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                             .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                    x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                    x.Type.Value == TransactionTypeValue.Payments)
                             .Count();
                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                      x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.SubUserAccountKey &&
                                       x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion

                        }
                        else if (_Request.UserReference.Task == HCoreTask.Get_AppUserOverview && _Request.Type == ListType.AppUsers)
                        {

                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.PaymentsOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(x =>
                                           x.Account.Guid == _Request.UserAccountKey &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.TypeId == RewardType.Id &&
                                           x.StatusId == StatusHelperId.Transaction_Success
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.UserAccountKey &&
                                               x.ModeId == TransactionMode.Debit &&
                                               x.TypeId == RewardType.Id &&
                                               x.StatusId == StatusHelperId.Transaction_Success
                                              ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(x =>
                                               x.Account.Guid == _Request.UserAccountKey &&
                                               x.ModeId == TransactionMode.Debit &&
                                               x.TypeId == RewardType.Id &&
                                               x.StatusId == StatusHelperId.Transaction_Success
                                              ).Sum(x => (double?)x.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                        x.Type.Value == TransactionTypeValue.Redeem))
                                        .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                       (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem))
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.SourceId == TransactionSource.TUC
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;


                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.SourceId == TransactionSource.TUC
                                           ).Sum(x => (double?)x.Charge) ?? 0;

                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward Overview
                            _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id &&
                                               a.SourceId == TransactionSource.TUC
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                             a.TypeId == RewardType.Id &&
                                               a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                .Select(x => new OUserTransactionType
                                {
                                    Id = x.ReferenceId,
                                    Name = x.Name
                                }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.TUC
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&

                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.TUC
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                           a.TypeId == RewardType.Id &&
                                        a.SourceId == TransactionSource.TUC
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                      x.Type.Value == TransactionTypeValue.Payments)
                               .Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.AccountId == UserAccount.ReferenceId &&
                                       x.Type.Value == TransactionTypeValue.Payments)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion

                        }
                        else
                        {
                            #region Visitors
                            _OVistors = new OVistors();
                            _OVistors.Total = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedById == UserAccount.ReferenceId
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.AccountId).Distinct().Count();
                            _OVistors.TotalVisits = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == UserAccount.ReferenceId
                                               && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OVistors.SingleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                                                                   a.CreatedById == UserAccount.ReferenceId).Count() == 1)
                                    .Select(x => x.Id).Count();

                            _OVistors.MultipleVisit = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser &&
                                       x.HCUAccountTransactionAccount.Where(a => a.AccountId == x.Id &&
                                    a.CreatedBy.Id == UserAccount.ReferenceId).Count() > 1)
                                    .Select(x => x.Id).Count();
                            _OUserOverviewResponse.VisitorsOverview = _OVistors;
                            #endregion

                            #region Rewards
                            _ORewards = new ORewards();
                            _ORewards.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedById == UserAccount.ReferenceId &&
                                               (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                                x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            _ORewards.TotalReward = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                              (
                                              x.Type.Value == TransactionTypeValue.Reward) &&
                                              x.ModeId == TransactionMode.Debit &&
                                              x.SourceId == TransactionSource.Cashier
                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _ORewards.TotalRedeem = _HCoreContext.HCUAccountTransaction
                                     .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                            (x.Type.Value == TransactionTypeValue.Redeem) &&
                                            x.ModeId == TransactionMode.Debit && x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.TotalAmount) ?? 0;

                            if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                            {
                                _ORewards.TotalComission = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                             (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.SourceId == TransactionSource.Cashier
                                             ).Sum(x => (double?)x.ComissionAmount) ?? 0;

                                _ORewards.TotalCharge = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey &&
                                           (x.Type.Value == TransactionTypeValue.Reward ||
                                            x.Type.Value == TransactionTypeValue.Redeem) &&
                                           x.ModeId == TransactionMode.Debit &&
                                           x.SourceId == TransactionSource.Cashier
                                           ).Sum(x => (double?)x.Charge) ?? 0;
                            }
                            _OUserOverviewResponse.Rewards = _ORewards;
                            #endregion

                            #region  Reward Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RewardOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           a.SourceId == TransactionSource.Cashier
                                          ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          a.SourceId == TransactionSource.Cashier
                                         ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion

                            #region  Redeem Overview
                            if (UserAccount.AccountOperationTypeId == CoreHelpers.AccountOperationType.Online)
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            else
                            {
                                _OUserOverviewResponse.RedeemOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            }
                            foreach (var RewardType in _OUserOverviewResponse.RedeemOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                           a.ModeId == TransactionMode.Debit &&
                                              a.TypeId == RewardType.Id &&
                                           (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;


                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                   .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                          a.ModeId == TransactionMode.Debit &&
                                             a.TypeId == RewardType.Id &&
                                          (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                         ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                 .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                        a.ModeId == TransactionMode.Debit &&
                                           a.TypeId == RewardType.Id &&
                                        (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUC)
                                    ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region  Payment Overview
                            _OUserOverviewResponse.PaymentsOverview = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                                    .Select(x => new OUserTransactionType
                                    {
                                        Id = x.ReferenceId,
                                        Name = x.Name
                                    }).ToList();
                            foreach (var RewardType in _OUserOverviewResponse.RewardOverview)
                            {
                                RewardType.Points = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                {
                                    RewardType.Comission = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                    RewardType.Charge = _HCoreContext.HCUAccountTransaction
                                    .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                              a.TypeId == RewardType.Id
                                              ).Sum(a => (double?)a.Charge) ?? 0;
                                }
                            }
                            #endregion
                            #region Cards Overview 
                            _OCardsOverview = new OCardsOverview();
                            _OCardsOverview.Total = _HCoreContext.TUCard
                                .Where(x => x.ActiveMerchantId == UserAccount.OwnerParentId)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.Assigned = _HCoreContext.HCUAccount
                                .Where(x => x.CreatedById == UserAccount.ReferenceId && x.AccountTypeId == UserAccountType.Appuser)
                                .Select(x => x.Id).Count();
                            _OCardsOverview.NotAssigned = _OCardsOverview.Total - _OCardsOverview.Assigned;
                            _OUserOverviewResponse.CardsOverview = _OCardsOverview;
                            #endregion


                            #region Payments
                            _OPayments = new OPayments();
                            _OPayments.Transactions = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                  x.Type.Value == TransactionTypeValue.Payments &&
                                       x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();

                            _OPayments.TotalPurchase = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            _OPayments.TotalAmount = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;

                            _OPayments.TotalComission = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                             .Sum(x => (double?)x.ComissionAmount) ?? 0;


                            _OPayments.TotalCharge = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId &&
                                   x.Type.Value == TransactionTypeValue.Payments &&
                                        x.Account.AccountTypeId == UserAccountType.Appuser)
                                .Sum(x => (double?)x.Charge) ?? 0;
                            _OUserOverviewResponse.Payments = _OPayments;
                            #endregion

                            //#region Send Response
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                            //#endregion
                        }

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OUserOverviewResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the user analytics.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAnalytics(OChart.Request _Request)
        {
            #region Intialize Object

            _OChartRequest = new OChart.Request();
            _OChartRequestRange = new List<OChart.RequestDateRange>();
            #endregion
            #region Build Date Range
            _TChartRequestRange = new List<OChart.RequestDateRange>();
            int Days = (_Request.EndTime - _Request.StartTime).Days;
            for (int i = 0; i < Days; i++)
            {
                DateTime TStartTime = _Request.StartTime;
                DateTime TEndTime = _Request.StartTime.AddDays(1).AddSeconds(-1);
                _Request.StartTime = TEndTime.AddSeconds(1);
                _TChartRequestRange.Add(new OChart.RequestDateRange()
                {
                    StartTime = TStartTime,
                    EndTime = TEndTime,
                });
            }
            _Request.DateRange = _TChartRequestRange;
            #endregion
            #region Initialize Connection
            using (_HCoreContext = new HCoreContext())
            {
                #region System Helpers
                var TransactionTypes = _HCoreContext.HCCore
                                       .Where(x => x.ParentId == HelperType.TransactionType)
                                              .Select(x => new
                                              {
                                                  ReferenceId = x.Id,
                                                  Name = x.Name,
                                                  Value = x.Value,
                                              }).ToList();
                #endregion
                var UserAccount = _HCoreContext.HCUAccount
                                                   .Where(x => x.Guid == _Request.UserAccountKey)
                                                   .Select(x => new
                                                   {
                                                       ReferenceId = x.Id,
                                                       CreateDate = x.CreateDate,
                                                       AccountTypeId = x.AccountTypeId,
                                                   }).FirstOrDefault();
                #region Loop Date Range
                if (_Request.Type == ListType.Rewards)
                {
                    var THelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Reward)
                              .Select(x => new
                              {
                                  ReferenceId = x.ReferenceId,
                                  Name = x.Name
                              }).ToList();
                    foreach (var _RangeItem in _Request.DateRange)
                    {
                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Merchant && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Credit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        //else if (UserAccount.AccountTypeId == UserAccountType.Carduser)
                        //{
                        //    foreach (var HelperType in THelperTypes)
                        //    {
                        //        double? Points = _HCoreContext.HCUAccountTransaction
                        //        .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                        //               a.ModeId == TransactionMode.Credit &&
                        //               a.TypeId == HelperType.ReferenceId &&
                        //               a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                        //                  ).Sum(a => (double?)a.TotalAmount) ?? 0;

                        //        _OChartRequestRangeValue.Add(new OChart.RequestData
                        //        {
                        //            Name = HelperType.Name,
                        //            Value = Points.ToString(),
                        //        });
                        //    }
                        //}
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Credit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Credit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a =>
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
                else if (_Request.Type == ListType.Redeems)
                {
                    var THelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Redeem)
                             .Select(x => new
                             {
                                 ReferenceId = x.ReferenceId,
                                 Name = x.Name
                             }).ToList();
                    foreach (var _RangeItem in _Request.DateRange)
                    {

                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            foreach (var HelperType in THelperTypes)
                            {

                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Merchant && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Credit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Credit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Credit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;

                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Debit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Debit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                       a.ModeId == TransactionMode.Credit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Credit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.ModeId == TransactionMode.Credit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;

                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a =>
                                       a.ModeId == TransactionMode.Debit &&
                                       a.TypeId == HelperType.ReferenceId &&
                                       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                // if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                // {
                                //     double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a =>
                                //       a.ModeId == TransactionMode.Debit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Comission",
                                //         Value = Comission.ToString(),
                                //     });


                                //     double? Charge = _HCoreContext.HCUAccountTransaction
                                //     .Where(a =>
                                //       a.ModeId == TransactionMode.Debit &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.Charge) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Charge",
                                //         Value = Charge.ToString(),
                                //     });
                                // }

                            }
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
                else if (_Request.Type == ListType.Payments)
                {
                    var THelperTypes = TransactionTypes.Where(x => x.Value == TransactionTypeValue.Payments)
                          .Select(x => new
                          {
                              ReferenceId = x.ReferenceId,
                              Name = x.Name
                          }).ToList();
                    foreach (var _RangeItem in _Request.DateRange)
                    {
                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                // if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                // {
                                //     double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.ComissionAmount) ?? 0;

                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Comission",
                                //         Value = Comission.ToString(),
                                //     });


                                //     double? Charge = _HCoreContext.HCUAccountTransaction
                                //     .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.Charge) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Charge",
                                //         Value = Charge.ToString(),
                                //     });
                                // }
                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                // if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                // {
                                //     double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId &&
                                //       a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Comission",
                                //         Value = Comission.ToString(),
                                //     });


                                //     double? Charge = _HCoreContext.HCUAccountTransaction
                                //     .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.Charge) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Charge",
                                //         Value = Charge.ToString(),
                                //     });
                                // }

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                // if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                // {
                                //     double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Comission",
                                //         Value = Comission.ToString(),
                                //     });


                                //     double? Charge = _HCoreContext.HCUAccountTransaction
                                //     .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.Charge) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Charge",
                                //         Value = Charge.ToString(),
                                //     });
                                // }

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Merchant && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;

                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                //       a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                //if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                //{
                                //    double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Comission",
                                //        Value = Comission.ToString(),
                                //    });


                                //    double? Charge = _HCoreContext.HCUAccountTransaction
                                //.Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //          ).Sum(a => (double?)a.Charge) ?? 0;

                                //    _OChartRequestRangeValue.Add(new OChart.RequestData
                                //    {
                                //        Name = HelperType.Name + " - Charge",
                                //        Value = Charge.ToString(),
                                //    });
                                //}

                            }
                        }
                        else
                        {
                            foreach (var HelperType in THelperTypes)
                            {
                                double? Points = _HCoreContext.HCUAccountTransaction
                                .Where(a =>
                                       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                          ).Sum(a => (double?)a.TotalAmount) ?? 0;

                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = HelperType.Name,
                                    Value = Points.ToString(),
                                });

                                // if (_Request.UserReference.AccountTypeId == UserAccountType.Controller)
                                // {
                                //     double? Comission = _HCoreContext.HCUAccountTransaction
                                //.Where(a =>
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.ComissionAmount) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Comission",
                                //         Value = Comission.ToString(),
                                //     });


                                //     double? Charge = _HCoreContext.HCUAccountTransaction
                                //     .Where(a =>
                                //       a.TypeId == HelperType.ReferenceId && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                //      ).Sum(a => (double?)a.Charge) ?? 0;


                                //     _OChartRequestRangeValue.Add(new OChart.RequestData
                                //     {
                                //         Name = HelperType.Name + " - Charge",
                                //         Value = Charge.ToString(),
                                //     });
                                // }

                            }
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
                else if (_Request.Type == ListType.CardUsers)
                {
                    foreach (var _RangeItem in _Request.DateRange)
                    {
                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            long Value = _HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Cards Assigned",
                                Value = Value.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            long Value = _HCoreContext.HCUAccount.Where(x => x.Owner.Id == UserAccount.ReferenceId && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Cards Assigned",
                                Value = Value.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            long Value = _HCoreContext.HCUAccount.Where(x => x.CreatedById == UserAccount.ReferenceId && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Cards Assigned",
                                Value = Value.ToString(),
                            });
                        }
                        else
                        {
                            long Value = _HCoreContext.HCUAccount.Where(x => x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime && x.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Cards Assigned",
                                Value = Value.ToString(),
                            });
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
                else if (_Request.Type == ListType.Visitors)
                {
                    foreach (var _RangeItem in _Request.DateRange)
                    {
                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Owner.Owner.Id == UserAccount.ReferenceId
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Visitors",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",
                                //Name = "Store Visits",
                                Value = TotalVisits.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Owner.Guid == _Request.UserAccountKey
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Owner.Id == UserAccount.ReferenceId
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Visitors",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",

                                Value = TotalVisits.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Guid == _Request.UserAccountKey
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.CreatedBy.Id == UserAccount.ReferenceId
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Visitors",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",

                                Value = TotalVisits.ToString(),
                            });
                        }
                        //else if (UserAccount.AccountTypeId == UserAccountType.Carduser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        //{
                        //    long TotalVisits = _HCoreContext.HCUAccountTransaction
                        //                .Where(x => x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                        //                       && x.Account.Guid == _Request.UserAccountKey
                        //                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                        //                       &&  x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                        //    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                        //                        .Where(x =>
                        //                               x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                        //                       && x.Account.Guid == _Request.UserAccountKey
                        //                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                        //                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                        //    //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Visits",
                        //        Value = TotalVisitors.ToString(),
                        //    });

                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Transactions",

                        //        Value = TotalVisits.ToString(),
                        //    });
                        //}
                        //else if (UserAccount.AccountTypeId == UserAccountType.Carduser)
                        //{
                        //    long TotalVisits = _HCoreContext.HCUAccountTransaction
                        //                .Where(x => x.Account.Guid == _Request.UserAccountKey
                        //                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                        //                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                        //    long TotalVisitors = _HCoreContext.HCUAccountTransaction
                        //                        .Where(x =>
                        //                               x.Account.Guid == _Request.UserAccountKey
                        //                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                        //                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                        //    //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Visits",
                        //        Value = TotalVisitors.ToString(),
                        //    });

                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Transactions",

                        //        Value = TotalVisits.ToString(),
                        //    });
                        //}
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                                               && x.Account.Guid == _Request.UserAccountKey
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x =>
                                                       x.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey
                                               && x.Account.Guid == _Request.UserAccountKey
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Visits",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",

                                Value = TotalVisits.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Account.Guid == _Request.UserAccountKey
                                               && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x =>
                                                       x.Account.Guid == _Request.UserAccountKey
                                                       && x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Visits",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",

                                Value = TotalVisits.ToString(),
                            });
                        }
                        else
                        {

                            long TotalVisits = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                               && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).Count();
                            long TotalVisitors = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.TransactionDate > _RangeItem.StartTime && x.TransactionDate < _RangeItem.EndTime
                                                       && x.Account.AccountTypeId == UserAccountType.Appuser).Select(x => x.AccountId).Distinct().Count();
                            //_HCoreContext.HCUAccount.Where(x => x.Owner.Owner.Id == UserAccount.ReferenceId && x.CreateDate > _RangeItem.StartTime && x.CreateDate < _RangeItem.EndTime).Select(x => x.Id).Count();
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Visitors",
                                Value = TotalVisitors.ToString(),
                            });

                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Transactions",

                                Value = TotalVisits.ToString(),
                            });
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
                else if (_Request.Type == ListType.Sale)
                {
                    foreach (var _RangeItem in _Request.DateRange)
                    {
                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                        if (UserAccount.AccountTypeId == UserAccountType.Merchant)
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                            .Where(a => a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                   a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                      ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Sale",
                                Value = Points.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantStore)
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                            .Where(a => a.CreatedBy.Owner.Guid == _Request.UserAccountKey &&
                                   a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                      ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Sale",
                                Value = Points.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                            .Where(a => a.CreatedBy.Guid == _Request.UserAccountKey &&
                                   a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                      ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Sale",
                                Value = Points.ToString(),
                            });
                        }
                        //else if (UserAccount.AccountTypeId == UserAccountType.Carduser 
                        //&& !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        //{
                        //    double? Points = _HCoreContext.HCUAccountTransaction
                        //    .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                        //           a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                        //           a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                        //              ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Purchase",
                        //        Value = Points.ToString(),
                        //    });
                        //}
                        //else if (UserAccount.AccountTypeId == UserAccountType.Carduser)
                        //{
                        //    double? Points = _HCoreContext.HCUAccountTransaction
                        //    .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                        //           a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                        //              ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                        //    _OChartRequestRangeValue.Add(new OChart.RequestData
                        //    {
                        //        Name = "Purchase",
                        //        Value = Points.ToString(),
                        //    });
                        //}
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser && !string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                            .Where(a => a.Account.Guid == _Request.SubUserAccountKey &&
                                   a.CreatedBy.Owner.Owner.Guid == _Request.UserAccountKey &&
                                   a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                      ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Purchase",
                                Value = Points.ToString(),
                            });
                        }
                        else if (UserAccount.AccountTypeId == UserAccountType.Appuser)
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                            .Where(a => a.Account.Guid == _Request.UserAccountKey &&
                                   a.SourceId == TransactionSource.TUC && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                      ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Purchase",
                                Value = Points.ToString(),
                            });
                        }
                        else
                        {
                            double? Points = _HCoreContext.HCUAccountTransaction
                           .Where(a =>
                                  a.SourceId == TransactionSource.Cashier && a.TransactionDate > _RangeItem.StartTime && a.TransactionDate < _RangeItem.EndTime
                                     ).Sum(a => (double?)a.PurchaseAmount) ?? 0;
                            _OChartRequestRangeValue.Add(new OChart.RequestData
                            {
                                Name = "Store Sale",
                                Value = Points.ToString(),
                            });
                        }
                        #region Add Values To Range
                        _OChartRequestRange.Add(new OChart.RequestDateRange
                        {
                            EndTime = _RangeItem.EndTime,
                            StartTime = _RangeItem.StartTime,
                            Label = _RangeItem.Label,
                            Data = _OChartRequestRangeValue
                        });
                        #endregion
                    }
                }
            }
            #endregion
            #region Build Response
            _OChartRequest.DateRange = _OChartRequestRange;
            _OChartRequest.StartTime = _Request.StartTime;
            _OChartRequest.EndTime = _Request.EndTime;
            _OChartRequest.Type = _Request.Type;
            #endregion
            #region Send Response
            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
            #endregion
            #endregion
        }
        #endregion
    }
}
