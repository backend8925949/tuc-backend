//==================================================================================
// FileName: FrameworkCity.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkCity
    {
        //#region Declare
        ////HCCore _HCCore;
        //HCoreContext _HCoreContext;
        //HCCoreParameter _HCCoreParameter;
        //#endregion

        //internal OResponse SaveCity(OCity.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);


        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long? RegionAreaId = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.RegionAreaReferenceKey).Select(x => x.Id).FirstOrDefault();

        //            if (StatusId == null)
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC499_InvalidStatusCode);
        //                #endregion
        //            }
        //            if (RegionAreaId == null)
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC068");
        //                #endregion
        //            }
        //            else
        //            {
        //                long CheckConfiguration = _HCoreContext.HCCoreParameter.Where(x => x.SystemName == _Request.SystemName && x.TypeId == HelperType.CountryCity).Select(x => x.Id).FirstOrDefault();
        //                if (CheckConfiguration == 0)
        //                {
        //                    _HCCoreParameter = new HCCoreParameter();
        //                    _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
        //                    _HCCoreParameter.Name = _Request.Name;
        //                    _HCCoreParameter.TypeId = HelperType.CountryCity;
        //                    _HCCoreParameter.SystemName = _Request.SystemName;
        //                    _HCCoreParameter.ParentId = RegionAreaId;
        //                    _HCCoreParameter.Description = _Request.Description;
        //                    _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
        //                    _HCCoreParameter.StatusId = (int)StatusId;
        //                    _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
        //                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                    _HCoreContext.SaveChanges();
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _HCCoreParameter.Guid, HCoreResponseCode.System.HC003_DetailsSaved);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC498_DetailsAlreadyPresent);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("SaveCity", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse UpdateCity(OCity.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {

        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long? CityId = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();


        //            #region Code Block

        //            int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

        //            if (CityId == null)
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //                #endregion
        //            }

        //            if (StatusId == null)
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //                #endregion
        //            }
        //            HCCoreParameter ItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (ItemDetails != null)
        //            {
        //                if (!string.IsNullOrEmpty(_Request.Name) && ItemDetails.Name != _Request.Name)
        //                {
        //                    ItemDetails.Name = _Request.Name;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.SystemName) && ItemDetails.SystemName != _Request.SystemName)
        //                {
        //                    ItemDetails.SystemName = _Request.SystemName;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.Description) && ItemDetails.Description != _Request.Description)
        //                {
        //                    ItemDetails.Description = _Request.Description;
        //                }

        //                if (StatusId != null && ItemDetails.StatusId != StatusId)
        //                {
        //                    ItemDetails.StatusId = (int)StatusId;
        //                }

        //                ItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                ItemDetails.ModifyById = _Request.UserReference.AccountId;
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC004_DetailsUpdated);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("UpdateCity", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse DeleteCity(OCity.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            HCCoreParameter DeleteItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (DeleteItemDetails != null)
        //            {
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.HCCoreParameter.Remove(DeleteItemDetails);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC005_DetailsDeleted);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("DeleteCity", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetCityDetailsByRegionAreaId(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                #region Total Records
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Parent.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryCity)
        //                    .Select(x => new OCity.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        TypeCode = x.Type.SystemName,
        //                        TypeName = x.Type.Name,
        //                        SubTypeCode = x.SubType.SystemName,
        //                        SubTypeName = x.SubType.Name,
        //                        ParentKey = x.Parent.Guid,
        //                        ParentCode = x.Parent.SystemName,
        //                        ParentName = x.Parent.Name,
        //                        SubParentKey = x.SubParent.Guid,
        //                        SubParentCode = x.SubParent.SystemName,
        //                        SubParentName = x.SubParent.Name,
        //                        //AccountKey = x.Account.Guid,
        //                        //AccountDisplayName = x.Account.DisplayName,
        //                        //AccountIconUrl = x.Account.PrimaryStorage.Path,
        //                        HelperCode = x.Helper.SystemName,
        //                        HelperName = x.Helper.Name,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Value = x.Value,
        //                        SubValue = x.SubValue,
        //                        Data = x.Data,
        //                        Description = x.Description,
        //                        //PrimaryStorageUrl = x.PrimaryStorage.Path,
        //                        //SecondaryStorageUrl = x.SecondaryStorage.Path,
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    }).Count();
        //                }

        //                #endregion
        //                #region Set Default Limit
        //                if (_Request.Limit == -1)
        //                {
        //                    _Request.Limit = _Request.TotalRecords;
        //                }
        //                else if (_Request.Limit == 0)
        //                {
        //                    _Request.Limit = _AppConfig.DefaultRecordsLimit;

        //                }
        //                #endregion

        //                List<OCity.List> Details = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Parent.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryCity)
        //                    .Select(x => new OCity.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        TypeCode = x.Type.SystemName,
        //                        TypeName = x.Type.Name,
        //                        SubTypeCode = x.SubType.SystemName,
        //                        SubTypeName = x.SubType.Name,
        //                        ParentKey = x.Parent.Guid,
        //                        ParentCode = x.Parent.SystemName,
        //                        ParentName = x.Parent.Name,
        //                        SubParentKey = x.SubParent.Guid,
        //                        SubParentCode = x.SubParent.SystemName,
        //                        SubParentName = x.SubParent.Name,
        //                        //AccountKey = x.Account.Guid,
        //                        //AccountDisplayName = x.Account.DisplayName,
        //                        //AccountIconUrl = x.Account.PrimaryStorage.Path,
        //                        HelperCode = x.Helper.SystemName,
        //                        HelperName = x.Helper.Name,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Value = x.Value,
        //                        SubValue = x.SubValue,
        //                        Data = x.Data,
        //                        Description = x.Description,
        //                        //PrimaryStorageUrl = x.PrimaryStorage.Path,
        //                        //SecondaryStorageUrl = x.SecondaryStorage.Path,
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    })
        //                    .ToList();
        //                _HCoreContext.Dispose();

        //                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Details, _Request.Offset, _Request.Limit);

        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, HCoreResponseCode.System.HC000_DetailsLoaded);
        //                #endregion

        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetCityDetailsByRegionAreaId", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetCityDetails(OCity.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                OCity.List Details = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryCity)
        //                    .Select(x => new OCity.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        TypeCode = x.Type.SystemName,
        //                        TypeName = x.Type.Name,
        //                        SubTypeCode = x.SubType.SystemName,
        //                        SubTypeName = x.SubType.Name,
        //                        ParentKey = x.Parent.Guid,
        //                        ParentCode = x.Parent.SystemName,
        //                        ParentName = x.Parent.Name,
        //                        SubParentKey = x.SubParent.Guid,
        //                        SubParentCode = x.SubParent.SystemName,
        //                        SubParentName = x.SubParent.Name,
        //                        //AccountKey = x.Account.Guid,
        //                        //AccountDisplayName = x.Account.DisplayName,
        //                        //AccountIconUrl = x.Account.PrimaryStorage.Path,
        //                        HelperCode = x.Helper.SystemName,
        //                        HelperName = x.Helper.Name,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Value = x.Value,
        //                        SubValue = x.SubValue,
        //                        Data = x.Data,
        //                        Description = x.Description,
        //                        //PrimaryStorageUrl = x.PrimaryStorage.Path,
        //                        //SecondaryStorageUrl = x.SecondaryStorage.Path,
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    })
        //                    .FirstOrDefault();
        //                _HCoreContext.Dispose();


        //                if (Details != null)
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, HCoreResponseCode.System.HCC113);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetCityDetails", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetCityByRegionArea(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }

        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                #region Total Records
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Parent.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryCity)
        //                    .Select(x => new OCity.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        TypeCode = x.Type.SystemName,
        //                        TypeName = x.Type.Name,
        //                        SubTypeCode = x.SubType.SystemName,
        //                        SubTypeName = x.SubType.Name,
        //                        ParentKey = x.Parent.Guid,
        //                        ParentCode = x.Parent.SystemName,
        //                        ParentName = x.Parent.Name,
        //                        SubParentKey = x.SubParent.Guid,
        //                        SubParentCode = x.SubParent.SystemName,
        //                        SubParentName = x.SubParent.Name,
        //                        //AccountKey = x.Account.Guid,
        //                        //AccountDisplayName = x.Account.DisplayName,
        //                        //AccountIconUrl = x.Account.PrimaryStorage.Path,
        //                        HelperCode = x.Helper.SystemName,
        //                        HelperName = x.Helper.Name,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Value = x.Value,
        //                        SubValue = x.SubValue,
        //                        Data = x.Data,
        //                        Description = x.Description,
        //                        //PrimaryStorageUrl = x.PrimaryStorage.Path,
        //                        //SecondaryStorageUrl = x.SecondaryStorage.Path,
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    }).Where(_Request.SearchCondition)
        //                    .Count();
        //                }

        //                #endregion
        //                #region Set Default Limit
        //                if (_Request.Limit == -1)
        //                {
        //                    _Request.Limit = _Request.TotalRecords;
        //                }
        //                else if (_Request.Limit == 0)
        //                {
        //                    _Request.Limit = _AppConfig.DefaultRecordsLimit;

        //                }
        //                #endregion

        //                List<ORegionArea.List> Details = _HCoreContext.HCCoreParameter
        //                .Where(a => a.Parent.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryCity)
        //                .Select(x => new ORegionArea.List
        //                {
        //                    ReferenceId = x.Id,
        //                    ReferenceKey = x.Guid,
        //                    TypeCode = x.Type.SystemName,
        //                    TypeName = x.Type.Name,
        //                    SubTypeCode = x.SubType.SystemName,
        //                    SubTypeName = x.SubType.Name,
        //                    ParentKey = x.Parent.Guid,
        //                    ParentCode = x.Parent.SystemName,
        //                    ParentName = x.Parent.Name,
        //                    SubParentKey = x.SubParent.Guid,
        //                    SubParentCode = x.SubParent.SystemName,
        //                    SubParentName = x.SubParent.Name,
        //                    //AccountKey = x.Account.Guid,
        //                    //AccountDisplayName = x.Account.DisplayName,
        //                    //AccountIconUrl = x.Account.PrimaryStorage.Path,
        //                    HelperCode = x.Helper.SystemName,
        //                    HelperName = x.Helper.Name,
        //                    Name = x.Name,
        //                    SystemName = x.SystemName,
        //                    Value = x.Value,
        //                    SubValue = x.SubValue,
        //                    Data = x.Data,
        //                    Description = x.Description,
        //                    //PrimaryStorageUrl = x.PrimaryStorage.Path,
        //                    //SecondaryStorageUrl = x.SecondaryStorage.Path,
        //                    CreateDate = x.CreateDate,
        //                    CreatedByKey = x.CreatedBy.Guid,
        //                    CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
        //                    ModifyDate = x.ModifyDate,
        //                    ModifyByKey = x.ModifyBy.Guid,
        //                    ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                    ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
        //                    StatusId = x.StatusId,
        //                    StatusCode = x.Status.SystemName,
        //                    StatusName = x.Status.Name
        //                }).Where(_Request.SearchCondition)
        //                                  .OrderBy(_Request.SortExpression)
        //                                  .Skip(_Request.Offset)
        //                                  .Take(_Request.Limit)
        //                .ToList();
        //                _HCoreContext.Dispose();

        //                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Details, _Request.Offset, _Request.Limit);

        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, HCoreResponseCode.System.HC000_DetailsLoaded);
        //                #endregion

        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetRegionAreaDetailsByRegionId", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}


    }
}
