//==================================================================================
// FileName: FrameworkCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core helper
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Data.Logging;

namespace HCore.Framework
{
    internal class FrameworkCoreHelper
    {
        #region Context
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        HCCore _HCCore;
        HCCoreCommon _HCCoreCommon;
        HCCoreParameter _HCCoreParameter;
        #endregion
        /// <summary>
        /// Description: Saves the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    long? IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    long? PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);

                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? ParentId = HCoreHelper.GetSystemHelperId(_Request.ParentCode, _Request.UserReference);
                    int? SubParentId = HCoreHelper.GetSystemHelperId(_Request.SubParentCode, _Request.UserReference);
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        long VSystemName = _HCoreContext.HCCore
                            .Where(x => x.SystemName == _Request.SystemName)
                            .Select(x => x.Id).FirstOrDefault();
                        if (VSystemName == 0)
                        {
                            _HCCore = new HCCore();
                            _HCCore.Guid = HCoreHelper.GenerateGuid();
                            _HCCore.ParentId = ParentId;
                            _HCCore.SubParentId = SubParentId;
                            _HCCore.Name = _Request.Name;
                            _HCCore.SystemName = _Request.SystemName;
                            _HCCore.TypeName = _Request.TypeName;
                            _HCCore.Value = _Request.Value;
                            //_HCCore.Description = _Request.Description;
                            _HCCore.Sequence = _Request.Sequence;
                            //_HCCore.IconStorageId = IconStorageId;
                            //_HCCore.PosterStorageId = PosterStorageId;
                            _HCCore.Sequence = _Request.Sequence;
                            //_HCCore.CreateDate = HCoreHelper.GetGMTDateTime();
                            //_HCCore.CreatedById = _Request.UserReference.AccountId;
                            _HCCore.StatusId = (int)StatusId;
                            _HCoreContext.HCCore.Add(_HCCore);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            _Request.ReferenceKey = _HCCore.Guid;
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// return static list of merchants 
        /// </summary>
        /// <returns></returns>
        internal object getdealmerchants()
        {
            List<object> lsMerchants = new List<object>();
            lsMerchants.Add(new { id = 1, name = "Dealday" });
            lsMerchants.Add(new { id = 2, name = "De favour Dental Clinic" });
            lsMerchants.Add(new { id = 3, name = "Jecinee Beauty" });
            lsMerchants.Add(new { id = 4, name = "Syntax Merchandise" });
            lsMerchants.Add(new { id = 5, name = "Robert Dental Clinic" });
            lsMerchants.Add(new { id = 6, name = "House of Tay" });
            lsMerchants.Add(new { id = 7, name = "D'haritage Spa" });
            lsMerchants.Add(new { id = 8, name = "Firstpoint Gift" });
            lsMerchants.Add(new { id = 9, name = "Best Western Hotel" });
            lsMerchants.Add(new { id = 10, name = "Tudor House" });
            lsMerchants.Add(new { id = 11, name = "Glee Spa" });
            lsMerchants.Add(new { id = 12, name = "Rapid Paintball" });
            lsMerchants.Add(new { id = 13, name = "Amazon Kayak" });
            lsMerchants.Add(new { id = 14, name = "VrPlace Nigeria" });
            lsMerchants.Add(new { id = 15, name = "Reines Aesthetic" });

            return lsMerchants;
        }
        /// <summary>
        /// Description: Saves the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    long? ParentId = HCoreHelper.GetCoreCommonId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreCommonId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    if (ParentId != 0 && _Request.DisableChild == true)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ChildList = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == ParentId && x.StatusId == CoreConstant.StatusActive).ToList();
                            if (ChildList.Count > 0)
                            {
                                foreach (var ChildItem in ChildList)
                                {
                                    ChildItem.StatusId = CoreConstant.StatusBlocked;
                                    ChildItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ChildItem.ModifyById = _Request.UserReference.AccountId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCCoreCommon = new HCCoreCommon();
                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreCommon.ParentId = ParentId;
                        _HCCoreCommon.SubParentId = SubParentId;
                        _HCCoreCommon.AccountId = UserAccountId;
                        _HCCoreCommon.TypeId = (int)TypeId;
                        _HCCoreCommon.Name = _Request.Name;
                        _HCCoreCommon.SystemName = _Request.SystemName;
                        _HCCoreCommon.Value = _Request.Value;
                        _HCCoreCommon.SubValue = _Request.SubValue;
                        _HCCoreCommon.Data = _Request.Data;
                        _HCCoreCommon.Description = _Request.Description;
                        _HCCoreCommon.HelperId = HelperId;
                        _HCCoreCommon.Sequence = _Request.Sequence;
                        _HCCoreCommon.Count = _Request.Count;
                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreCommon.StatusId = (int)StatusId;
                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCCoreCommon.Guid;
                        #region Send Response
                        var _Response = new
                        {
                            ReferenceId = _HCCoreCommon.Id,
                            ReferenceKey = _HCCoreCommon.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreCommon", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName) && !string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    long? ParentId = HCoreHelper.GetCoreParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreParameterId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    long? SubCommonId = HCoreHelper.GetCoreCommonId(_Request.SubCommonKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCCoreParameter = new HCCoreParameter();
                        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreParameter.ParentId = ParentId;
                        _HCCoreParameter.SubParentId = SubParentId;
                        _HCCoreParameter.AccountId = UserAccountId;
                        _HCCoreParameter.TypeId = TypeId;
                        _HCCoreParameter.Name = _Request.Name;
                        _HCCoreParameter.SystemName = _Request.SystemName;
                        _HCCoreParameter.Value = _Request.Value;
                        _HCCoreParameter.SubValue = _Request.SubValue;
                        _HCCoreParameter.Data = _Request.Data;
                        _HCCoreParameter.Description = _Request.Description;
                        _HCCoreParameter.HelperId = HelperId;
                        _HCCoreParameter.CommonId = CommonId;
                        _HCCoreParameter.SubCommonId = SubCommonId;
                        _HCCoreParameter.Sequence = _Request.Sequence;
                        _HCCoreParameter.Count = _Request.Count;
                        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreParameter.StatusId = (int)StatusId;
                        _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCCoreParameter.Guid;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    #region Operations
                    int? ParentId = HCoreHelper.GetSystemHelperId(_Request.ParentCode, _Request.UserReference);
                    int? SubParentId = HCoreHelper.GetSystemHelperId(_Request.SubParentCode, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

                    using (_HCoreContext = new HCoreContext())
                    {
                        var Details = _HCoreContext.HCCore.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            long VSystemName = _HCoreContext.HCCore
                            .Where(x => x.SystemName == _Request.SystemName && x.Id != Details.Id)
                            .Select(x => x.Id).FirstOrDefault();
                            if (VSystemName != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                #endregion
                            }

                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.TypeName))
                            {
                                Details.TypeName = _Request.TypeName;
                            }
                            //if (!string.IsNullOrEmpty(_Request.Description))
                            //{
                            //    Details.Description = _Request.Description;
                            //}
                            if (_Request.Sequence != 0)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            //Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            //long? TIconStorageId = Details.IconStorageId;
                            //long? TPosterStorageId = Details.PosterStorageId;
                            long HelperId = Details.Id;
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            //long? IconStorageId = null;
                            //long? PosterStorageId = null;
                            //if (_Request.IconContent != null)
                            //{
                            //    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                            //}
                            //if (_Request.IconContent != null)
                            //{
                            //    PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                            //}
                            //if (IconStorageId != null || PosterStorageId != null)
                            //{
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        var UpdateDetails = _HCoreContext.HCCore.Where(x => x.Id == HelperId).FirstOrDefault();
                            //        if (UpdateDetails != null)
                            //        {
                            //            if (IconStorageId != null)
                            //            {
                            //                UpdateDetails.IconStorageId = IconStorageId;
                            //            }

                            //            if (PosterStorageId != null)
                            //            {
                            //                UpdateDetails.PosterStorageId = PosterStorageId;
                            //            }
                            //            UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //            UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                            //            _HCoreContext.SaveChanges();
                            //        }
                            //    }
                            //}

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                //else if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                //    #endregion
                //}
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetCoreCommonId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreCommonId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon Details = _HCoreContext.HCCoreCommon
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            //if (Details.SystemName != _Request.SystemName)
                            //{
                            //    long VSystemName = _HCoreContext.HCCoreCommon
                            //.Where(x => x.SystemName == _Request.SystemName && x.TypeId == TypeId && x.Id != Details.Id)
                            //.Select(x => x.Id).FirstOrDefault();
                            //    if (VSystemName != 0)
                            //    {
                            //        #region Send Response
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            //        #endregion
                            //    }
                            //}
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }

                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }
                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }
                            if (TypeId != null)
                            {
                                Details.TypeId = (int)TypeId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data))
                            {
                                Details.Data = _Request.Data;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue))
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                Details.Description = _Request.Description;
                            }
                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (_Request.Sequence != null)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            if (_Request.Count != null)
                            {
                                Details.Count = _Request.Count;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName) && !string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetCoreParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreParameterId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    long? SubCommonId = HCoreHelper.GetCoreCommonId(_Request.SubCommonKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreParameter Details = _HCoreContext.HCCoreParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (TypeId != null)
                            {
                                Details.TypeId = TypeId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue))
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data))
                            {
                                Details.Data = _Request.Data;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                Details.Description = _Request.Description;
                            }
                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (CommonId != null)
                            {
                                Details.CommonId = CommonId;
                            }
                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }
                            if (_Request.Sequence != null)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            if (_Request.Count != null)
                            {
                                Details.Count = _Request.Count;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var Details = _HCoreContext.HCCore.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCCore.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon Details = _HCoreContext.HCCoreCommon
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreCommon", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreParameter Details = _HCoreContext.HCCoreParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey) && string.IsNullOrEmpty(_Request.SystemName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreHelper.Details Details = _HCoreContext.HCCore
                            .Where(x => x.Guid == _Request.ReferenceKey || x.SystemName == _Request.SystemName)
                            .Select(x => new OCoreHelper.Details
                            {
                                ReferenceKey = x.Guid,
                                SystemName = x.SystemName,

                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                Name = x.Name,
                                Value = x.Value,
                                TypeName = x.TypeName,
                                //Description = x.Description,
                                Sequence = x.Sequence,

                                //CreateDate = x.CreateDate,
                                //CreatedByKey = x.CreatedBy.Guid,
                                //CreatedByDisplayName = x.CreatedBy.DisplayName,

                                //ModifyDate = x.ModifyDate,
                                //ModifyByKey = x.ModifyBy.Guid,
                                //ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .FirstOrDefault();
                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreHelper(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCCore
                                        select new OCoreHelper.Details
                                        {
                                            ReferenceKey = n.Guid,
                                            SystemName = n.SystemName,

                                            ParentCode = n.Parent.SystemName,
                                            ParentName = n.Parent.Name,

                                            Name = n.Name,
                                            TypeName = n.TypeName,
                                            Value = n.Value,
                                            //Description = n.Description,

                                            //CreateDate = n.CreateDate,
                                            //CreatedByKey = n.CreatedBy.Guid,
                                            //CreatedByDisplayName = n.CreatedBy.DisplayName,

                                            //ModifyDate = n.ModifyDate,
                                            //ModifyByKey = n.ModifyBy.Guid,
                                            //ModifyByDisplayName = n.ModifyBy.DisplayName,

                                            StatusId = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = CoreConstant.DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreHelper.Details> Data = (from n in _HCoreContext.HCCore
                                                      select new OCoreHelper.Details
                                                      {
                                                          ReferenceKey = n.Guid,
                                                          SystemName = n.SystemName,

                                                          ParentCode = n.Parent.SystemName,
                                                          ParentName = n.Parent.Name,

                                                          Name = n.Name,
                                                          TypeName = n.TypeName,
                                                          Value = n.Value,
                                                          //Description = n.Description,

                                                          //CreateDate = n.CreateDate,
                                                          //CreatedByKey = n.CreatedBy.Guid,
                                                          //CreatedByDisplayName = n.CreatedBy.DisplayName,

                                                          //ModifyDate = n.ModifyDate,
                                                          //ModifyByKey = n.ModifyBy.Guid,
                                                          //ModifyByDisplayName = n.ModifyBy.DisplayName,

                                                          StatusId = n.StatusId,
                                                          StatusCode = n.Status.SystemName,
                                                          StatusName = n.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreHelper", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreCommon.Details Details = _HCoreContext.HCCoreCommon
                            .Where(x => x.Guid == _Request.ReferenceKey)
                            .Select(x => new OCoreCommon.Details
                            {
                                ReferenceKey = x.Guid,
                                SystemName = x.SystemName,

                                ParentKey = x.Parent.Guid,
                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                SubParentKey = x.SubParent.Guid,
                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,

                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,

                                Name = x.Name,

                                Value = x.Value,
                                SubValue = x.SubValue,
                                Data = x.Data,
                                Description = x.Description,

                                UserAccountKey = x.Account.Guid,
                                UserAccountDisplayName = x.Account.DisplayName,

                                HelperCode = x.Helper.SystemName,
                                HelperName = x.Helper.Name,


                                Sequence = x.Sequence,
                                Count = x.Count,

                                IconUrl = x.IconStorage.Path,
                                PosterUrl = x.PosterStorage.Path,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .FirstOrDefault();
                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreCommon", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreCommon(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCCoreCommon
                                        select new OCoreCommon.Details
                                        {
                                            ReferenceKey = n.Guid,
                                            SystemName = n.SystemName,

                                            TypeCode = n.Type.SystemName,
                                            TypeName = n.Type.Name,

                                            HelperCode = n.Helper.SystemName,
                                            HelperName = n.Helper.Name,

                                            ParentKey = n.Parent.Guid,
                                            ParentCode = n.Parent.SystemName,
                                            ParentName = n.Parent.Name,

                                            SubParentKey = n.SubParent.Guid,
                                            SubParentCode = n.SubParent.SystemName,
                                            SubParentName = n.SubParent.Name,

                                            UserAccountKey = n.Account.Guid,
                                            UserAccountDisplayName = n.Account.DisplayName,

                                            Name = n.Name,
                                            Value = n.Value,
                                            SubValue = n.SubValue,
                                            Data = n.Data,
                                            Description = n.Description,
                                            Sequence = n.Sequence,
                                            Count = n.Count,

                                            CreateDate = n.CreateDate,
                                            CreatedByKey = n.CreatedBy.Guid,
                                            CreatedByDisplayName = n.CreatedBy.DisplayName,

                                            ModifyDate = n.ModifyDate,
                                            ModifyByKey = n.ModifyBy.Guid,
                                            ModifyByDisplayName = n.ModifyBy.DisplayName,

                                            StatusId = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = CoreConstant.DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreCommon.Details> Data = (from n in _HCoreContext.HCCoreCommon
                                                      select new OCoreCommon.Details
                                                      {
                                                          ReferenceKey = n.Guid,
                                                          SystemName = n.SystemName,

                                                          TypeCode = n.Type.SystemName,
                                                          TypeName = n.Type.Name,

                                                          HelperCode = n.Helper.SystemName,
                                                          HelperName = n.Helper.Name,

                                                          ParentKey = n.Parent.Guid,
                                                          ParentCode = n.Parent.SystemName,
                                                          ParentName = n.Parent.Name,

                                                          SubParentKey = n.SubParent.Guid,
                                                          SubParentCode = n.SubParent.SystemName,
                                                          SubParentName = n.SubParent.Name,

                                                          UserAccountKey = n.Account.Guid,
                                                          UserAccountDisplayName = n.Account.DisplayName,

                                                          Name = n.Name,
                                                          Value = n.Value,
                                                          SubValue = n.SubValue,
                                                          Data = n.Data,
                                                          Description = n.Description,
                                                          Sequence = n.Sequence,
                                                          Count = n.Count,

                                                          CreateDate = n.CreateDate,
                                                          CreatedByKey = n.CreatedBy.Guid,
                                                          CreatedByDisplayName = n.CreatedBy.DisplayName,

                                                          ModifyDate = n.ModifyDate,
                                                          ModifyByKey = n.ModifyBy.Guid,
                                                          ModifyByDisplayName = n.ModifyBy.DisplayName,

                                                          StatusId = n.StatusId,
                                                          StatusCode = n.Status.SystemName,
                                                          StatusName = n.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreCommon", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreParameter.Details Details = _HCoreContext.HCCoreParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                            .Select(x => new OCoreParameter.Details
                            {
                                ReferenceKey = x.Guid,
                                SystemName = x.SystemName,

                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,

                                HelperCode = x.Helper.SystemName,
                                HelperName = x.Helper.Name,

                                CommonKey = x.Common.Guid,
                                CommonCode = x.Common.SystemName,
                                CommonName = x.Common.Name,

                                SubCommonKey = x.SubCommon.Guid,
                                SubCommonCode = x.SubCommon.SystemName,
                                SubCommonName = x.SubCommon.Name,

                                UserAccountKey = x.Account.Guid,
                                UserAccountDisplayName = x.Account.DisplayName,

                                ParentKey = x.Parent.Guid,
                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                SubParentKey = x.SubParent.Guid,
                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,

                                Name = x.Name,
                                Value = x.Value,
                                SubValue = x.SubValue,
                                Data = x.Data,
                                Description = x.Description,
                                Sequence = x.Sequence,
                                Count = x.Count,
                                IconUrl = x.IconStorage.Path,
                                PosterUrl = x.PosterStorage.Path,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .FirstOrDefault();
                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreParameter(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                        select new OCoreParameter.Details
                                        {
                                            ReferenceKey = x.Guid,
                                            SystemName = x.SystemName,

                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,

                                            CommonKey = x.Common.Guid,
                                            CommonCode = x.Common.SystemName,
                                            CommonName = x.Common.Name,

                                            SubCommonKey = x.SubCommon.Guid,
                                            SubCommonCode = x.SubCommon.SystemName,
                                            SubCommonName = x.SubCommon.Name,

                                            SubParentKey = x.SubParent.Guid,
                                            SubParentCode = x.SubParent.SystemName,
                                            SubParentName = x.SubParent.Name,

                                            ParentKey = x.Parent.Guid,
                                            ParentCode = x.Parent.SystemName,
                                            ParentName = x.Parent.Name,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,

                                            Name = x.Name,
                                            Sequence = x.Sequence,
                                            Count = x.Count,

                                            IconUrl = x.IconStorage.Path,
                                            PosterUrl = x.PosterStorage.Path,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = CoreConstant.DefaultLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreParameter.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                         select new OCoreParameter.Details
                                                         {
                                                             ReferenceKey = x.Guid,
                                                             SystemName = x.SystemName,

                                                             TypeCode = x.Type.SystemName,
                                                             TypeName = x.Type.Name,

                                                             HelperCode = x.Helper.SystemName,
                                                             HelperName = x.Helper.Name,

                                                             CommonKey = x.Common.Guid,
                                                             CommonCode = x.Common.SystemName,
                                                             CommonName = x.Common.Name,

                                                             SubParentKey = x.SubParent.Guid,
                                                             SubParentCode = x.SubParent.SystemName,
                                                             SubParentName = x.SubParent.Name,

                                                             SubCommonKey = x.SubCommon.Guid,
                                                             SubCommonCode = x.SubCommon.SystemName,
                                                             SubCommonName = x.SubCommon.Name,

                                                             ParentKey = x.Parent.Guid,
                                                             ParentCode = x.Parent.SystemName,
                                                             ParentName = x.Parent.Name,

                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,

                                                             Name = x.Name,

                                                             Sequence = x.Sequence,
                                                             Count = x.Count,

                                                             IconUrl = x.IconStorage.Path,
                                                             PosterUrl = x.PosterStorage.Path,

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core usage details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreUsageDetails(OCoreUsage.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    return null;


                    //#region Operations
                    //using (_HCoreContextLogging = new HCoreContextLogging())
                    //{

                    //    OCoreUsage.Details Details = (from x in _HCoreContextLogging.HCLVCoreUsage
                    //                                  where x.ReferenceKey == _Request.ReferenceKey
                    //                                  select new OCoreUsage.Details
                    //                                  {
                    //                                      ReferenceId = x.ReferenceId,
                    //                                      ReferenceKey = x.ReferenceKey,
                    //                                      ApiKey = x.ApiKey,
                    //                                      ApiName = x.ApiName,
                    //                                      AppKey = x.AppKey,
                    //                                      AppName = x.AppName,
                    //                                      AppOwnerKey = x.AppOwnerKey,
                    //                                      AppOwnerName = x.AppOwnerName,
                    //                                      AppVersionKey = x.AppVersionKey,
                    //                                      AppVersionName = x.AppVersionName,
                    //                                      FeatureKey = x.FeatureKey,
                    //                                      FeatureName = x.FeatureName,
                    //                                      SessionKey = x.SessionKey,
                    //                                      SessionId = x.SessionId,
                    //                                      UserAccountDisplayName = x.UserAccountDisplayName,
                    //                                      UserAccountKey = x.UserAccountKey,
                    //                                      UserAccountTypeCode = x.UserAccountTypeCode,
                    //                                      UserAccountTypeName = x.UserAccountTypeName,
                    //                                      IpAddress = x.IpAddress,
                    //                                      Latitude = x.Latitude,
                    //                                      Longitude = x.Longitude,
                    //                                      Request = x.Request,
                    //                                      Response = x.Response,
                    //                                      RequestTime = x.RequestTime,
                    //                                      ResponseTime = x.ResponseTime,
                    //                                      ProcessingTime = x.ProcessingTime,
                    //                                      StatusId = x.StatusId,
                    //                                  }).FirstOrDefault();
                    //    _HCoreContextLogging.Dispose();

                    //    if (Details != null)
                    //    {
                    //        #region Send Response
                    //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                    //        #endregion
                    //    }
                    //    else
                    //    {
                    //        #region Send Response
                    //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    //        #endregion
                    //    }
                    //}
                    //#endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreUsage(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "RequestTime", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    return null;

                    //#region Total Records
                    //int TotalRecords = (from x in _HCoreContextLogging.HCLVCoreUsage
                    //                    select new OCoreUsage.Details
                    //                    {
                    //                        ReferenceId = x.ReferenceId,
                    //                        ReferenceKey = x.ReferenceKey,
                    //                        ApiKey = x.ApiKey,
                    //                        ApiName = x.ApiName,
                    //                        AppKey = x.AppKey,
                    //                        AppName = x.AppName,
                    //                        AppOwnerKey = x.AppOwnerKey,
                    //                        AppOwnerName = x.AppOwnerName,
                    //                        AppVersionKey = x.AppVersionKey,
                    //                        AppVersionName = x.AppVersionName,
                    //                        FeatureKey = x.FeatureKey,
                    //                        FeatureName = x.FeatureName,
                    //                        SessionKey = x.SessionKey,
                    //                        SessionId = x.SessionId,
                    //                        UserAccountDisplayName = x.UserAccountDisplayName,
                    //                        UserAccountKey = x.UserAccountKey,
                    //                        UserAccountTypeCode = x.UserAccountTypeCode,
                    //                        UserAccountTypeName = x.UserAccountTypeName,
                    //                        IpAddress = x.IpAddress,
                    //                        Latitude = x.Latitude,
                    //                        Longitude = x.Longitude,
                    //                        RequestTime = x.RequestTime,
                    //                        ResponseTime = x.ResponseTime,
                    //                        ProcessingTime = x.ProcessingTime,
                    //                        StatusId = x.StatusId,
                    //                    })
                    //                    .Where(_Request.SearchCondition)
                    //            .Count();
                    //#endregion
                    //#region Set Default Limit
                    //if (_Request.Limit == -1)
                    //{
                    //    _Request.Limit = TotalRecords;
                    //}
                    //else if (_Request.Limit == 0)
                    //{
                    //    _Request.Limit = CoreConstant.DefaultLimit;

                    //}
                    //#endregion
                    //#region Get Data
                    //List<OCoreUsage.Details> Data = (from x in _HCoreContextLogging.HCLVCoreUsage
                    //                                 select new OCoreUsage.Details
                    //                                 {
                    //                                     ReferenceId = x.ReferenceId,
                    //                                     ReferenceKey = x.ReferenceKey,
                    //                                     ApiKey = x.ApiKey,
                    //                                     ApiName = x.ApiName,
                    //                                     AppKey = x.AppKey,
                    //                                     AppName = x.AppName,
                    //                                     AppOwnerKey = x.AppOwnerKey,
                    //                                     AppOwnerName = x.AppOwnerName,
                    //                                     AppVersionKey = x.AppVersionKey,
                    //                                     AppVersionName = x.AppVersionName,
                    //                                     FeatureKey = x.FeatureKey,
                    //                                     FeatureName = x.FeatureName,
                    //                                     SessionKey = x.SessionKey,
                    //                                     SessionId = x.SessionId,
                    //                                     UserAccountDisplayName = x.UserAccountDisplayName,
                    //                                     UserAccountKey = x.UserAccountKey,
                    //                                     UserAccountTypeCode = x.UserAccountTypeCode,
                    //                                     UserAccountTypeName = x.UserAccountTypeName,
                    //                                     IpAddress = x.IpAddress,
                    //                                     Latitude = x.Latitude,
                    //                                     Longitude = x.Longitude,
                    //                                     RequestTime = x.RequestTime,
                    //                                     ResponseTime = x.ResponseTime,
                    //                                     ProcessingTime = x.ProcessingTime,
                    //                                     StatusId = x.StatusId,
                    //                                 })
                    //                          .Where(_Request.SearchCondition)
                    //                          .OrderBy(_Request.SortExpression)
                    //                          .Skip(_Request.Offset)
                    //                          .Take(_Request.Limit)
                    //                          .ToList();
                    //#endregion
                    //#region Create  Response Object
                    //OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    //#endregion
                    //#region Send Response
                    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    //#endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreUsage", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        //internal OResponse GetCoreUsageDetails(OCoreUsage.Manage _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContextLogging = new HCoreContextLogging())
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    OCoreUsage.Details Details = (from x in _HCoreContextLogging.HCLCoreUsage
        //                                                  where x.Guid == _Request.ReferenceKey
        //                                                  select new OCoreUsage.Details
        //                                                  {
        //                                                      ReferenceId = x.Id,
        //                                                      ReferenceKey = x.Guid,

        //                                                      FeatureKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Guid).FirstOrDefault(),
        //                                                      FeatureName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Name).FirstOrDefault(),

        //                                                      ApiKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Guid).FirstOrDefault(),
        //                                                      ApiName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Name).FirstOrDefault(),

        //                                                      AppVersionKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Guid).FirstOrDefault(),
        //                                                      AppVersionName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Name).FirstOrDefault(),

        //                                                      AppKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Guid).FirstOrDefault(),
        //                                                      AppName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Name).FirstOrDefault(),

        //                                                      AppOwnerKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.Guid).FirstOrDefault(),
        //                                                      AppOwnerName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.DisplayName).FirstOrDefault(),

        //                                                      UserAccountKey = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.Guid).FirstOrDefault(),
        //                                                      UserAccountDisplayName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.DisplayName).FirstOrDefault(),

        //                                                      UserAccountTypeCode = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.SystemName).FirstOrDefault(),
        //                                                      UserAccountTypeName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.Name).FirstOrDefault(),


        //                                                      SessionId = x.SessionId,
        //                                                      //SessionKey = x.Session.Guid,

        //                                                      IpAddress = x.IpAddress,
        //                                                      Latitude = x.Latitude,
        //                                                      Longitude = x.Longitude,


        //                                                      Request = x.Request,
        //                                                      Response = x.Response,

        //                                                      RequestTime = x.RequestTime,
        //                                                      ResponseTime = x.ResponseTime,
        //                                                      ProcessingTime = x.TimeDifference,

        //                                                      StatusId = x.StatusId,
        //                                                      StatusCode = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.SystemName).FirstOrDefault(),
        //                                                      StatusName = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.Name).FirstOrDefault(),

        //                                                  }).FirstOrDefault();

        //                    if (Details != null)
        //                    {
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
        //                        #endregion
        //                    }
        //                    else
        //                    {
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
        //                        #endregion
        //                    }
        //                }
        //            }
        //            #endregion

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}
        //internal OResponse GetCoreUsage(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Add Default Request
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //            #endregion
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSortCondition(_Request, "RequestTime", "desc");
        //            #endregion
        //        }
        //        #endregion
        //        #region Operation
        //        using (_HCoreContextLogging = new HCoreContextLogging())
        //        {
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                #region Total Records
        //                int TotalRecords = (from x in _HCoreContextLogging.HCLCoreUsage
        //                                    select new OCoreUsage.Details
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        FeatureKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Guid).FirstOrDefault(),
        //                                        FeatureName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Name).FirstOrDefault(),

        //                                        ApiKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Guid).FirstOrDefault(),
        //                                        ApiName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Name).FirstOrDefault(),

        //                                        AppVersionKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Guid).FirstOrDefault(),
        //                                        AppVersionName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Name).FirstOrDefault(),

        //                                        AppKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Guid).FirstOrDefault(),
        //                                        AppName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Name).FirstOrDefault(),

        //                                        AppOwnerKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.Guid).FirstOrDefault(),
        //                                        AppOwnerName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.DisplayName).FirstOrDefault(),

        //                                        UserAccountKey = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.Guid).FirstOrDefault(),
        //                                        UserAccountDisplayName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.DisplayName).FirstOrDefault(),

        //                                        UserAccountTypeCode = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.SystemName).FirstOrDefault(),
        //                                        UserAccountTypeName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.Name).FirstOrDefault(),


        //                                        SessionId = x.SessionId,
        //                                        //SessionKey = x.Session.Guid,

        //                                        IpAddress = x.IpAddress,
        //                                        Latitude = x.Latitude,
        //                                        Longitude = x.Longitude,

        //                                        RequestTime = x.RequestTime,
        //                                        ResponseTime = x.ResponseTime,
        //                                        ProcessingTime = x.TimeDifference,

        //                                        StatusId = x.StatusId,
        //                                        StatusCode = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.SystemName).FirstOrDefault(),
        //                                        StatusName = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.Name).FirstOrDefault(),

        //                                    })
        //                                    .Where(_Request.SearchCondition)
        //                            .Count();
        //                #endregion
        //                #region Set Default Limit
        //                if (_Request.Limit == -1)
        //                {
        //                    _Request.Limit = TotalRecords;
        //                }
        //                else if (_Request.Limit == 0)
        //                {
        //                    _Request.Limit = CoreConstant.DefaultLimit;

        //                }
        //                #endregion
        //                #region Get Data
        //                List<OCoreUsage.Details> Data = (from x in _HCoreContextLogging.HCLCoreUsage
        //                                                 select new OCoreUsage.Details
        //                                                 {
        //                                                     ReferenceId = x.Id,
        //                                                     ReferenceKey = x.Guid,

        //                                                     FeatureKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Guid).FirstOrDefault(),
        //                                                     FeatureName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.FeatureId).Select(n => n.Name).FirstOrDefault(),

        //                                                     ApiKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Guid).FirstOrDefault(),
        //                                                     ApiName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.ApiId).Select(n => n.Name).FirstOrDefault(),

        //                                                     AppVersionKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Guid).FirstOrDefault(),
        //                                                     AppVersionName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Name).FirstOrDefault(),

        //                                                     AppKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Guid).FirstOrDefault(),
        //                                                     AppName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Name).FirstOrDefault(),

        //                                                     AppOwnerKey = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.Guid).FirstOrDefault(),
        //                                                     AppOwnerName = _HCoreContext.HCCoreCommon.Where(n => n.Id == x.AppVersionId).Select(n => n.Parent.Account.DisplayName).FirstOrDefault(),

        //                                                     UserAccountKey = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.Guid).FirstOrDefault(),
        //                                                     UserAccountDisplayName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.DisplayName).FirstOrDefault(),

        //                                                     UserAccountTypeCode = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.SystemName).FirstOrDefault(),
        //                                                     UserAccountTypeName = _HCoreContext.HCUAccount.Where(n => n.Id == x.UserAccountId).Select(n => n.AccountType.Name).FirstOrDefault(),


        //                                                     SessionId = x.SessionId,
        //                                                     //SessionKey = x.Session.Guid,

        //                                                     IpAddress = x.IpAddress,
        //                                                     Latitude = x.Latitude,
        //                                                     Longitude = x.Longitude,

        //                                                     RequestTime = x.RequestTime,
        //                                                     ResponseTime = x.ResponseTime,
        //                                                     ProcessingTime = x.TimeDifference,

        //                                                     StatusId = x.StatusId,
        //                                                     StatusCode = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.SystemName).FirstOrDefault(),
        //                                                     StatusName = _HCoreContext.HCCore.Where(n => n.Id == x.StatusId).Select(n => n.Name).FirstOrDefault(),

        //                                                 })
        //                                          .Where(_Request.SearchCondition)
        //                                          .OrderBy(_Request.SortExpression)
        //                                          .Skip(_Request.Offset)
        //                                          .Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion
        //                #region Create  Response Object
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GetCoreUsage", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreLog(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {

                    #region Total Records
                    int TotalRecords = (from n in _HCoreContextLogging.HCLCoreLog
                                        select new OCoreLog
                                        {
                                            ReferenceId = n.Id,
                                            //ReferenceKey = n.Id,
                                            // TypeCode = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                            // TypeName = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),

                                            Title = n.Title,
                                            Description = n.Message,

                                            CreateDate = n.CreateDate,
                                            //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                            //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),

                                            ModifyDate = n.CreateDate,


                                            StatusId = 1,
                                            // StatusCode = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                            // StatusName = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = CoreConstant.DefaultLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OCoreLog> Data = (from n in _HCoreContextLogging.HCLCoreLog
                                           select new OCoreLog
                                           {
                                               ReferenceId = n.Id,
                                               //ReferenceKey = n.Id,
                                               // TypeCode = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                               // TypeName = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),

                                               Title = n.Title,
                                               Description = n.Message,

                                               CreateDate = n.CreateDate,
                                               //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                               //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),

                                               ModifyDate = n.CreateDate,


                                               StatusId = 1,
                                               // StatusCode = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                               // StatusName = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLogs", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreLog(OCoreLog _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Request, "HCL008");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    #region Get Data
                    OCoreLog Data = (from n in _HCoreContextLogging.HCLCoreLog
                                     where n.Id == _Request.ReferenceId
                                     select new OCoreLog
                                     {
                                         ReferenceId = n.Id,
                                         //ReferenceKey = n.Guid,
                                         //  TypeCode = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                         //  TypeName = _HCoreContext.HCCore.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),


                                         Title = n.Title,
                                         Description = n.Message,
                                         //Comment = n.Comment,
                                         Reference = n.RequestReference,
                                         Data = n.Data,

                                         CreateDate = n.CreateDate,
                                         //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                         //   CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),


                                         ModifyDate = n.CreateDate,
                                         // ModifyByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.Guid).FirstOrDefault(),
                                         // ModifyByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.DisplayName).FirstOrDefault(),


                                         StatusId = 1,
                                         // StatusCode = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                         // StatusName = _HCoreContext.HCCore.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                     }).FirstOrDefault();
                    if (Data != null)
                    {
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, Data, "HC0002");
                        #endregion
                    }
                    #endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLog", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
