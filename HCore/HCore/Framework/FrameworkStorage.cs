//==================================================================================
// FileName: FrameworkStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to storage
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using System.IO;
using Amazon.S3;
using Amazon.S3.Model;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using Google.Apis.Auth.OAuth2;
using System.Threading;

namespace HCore.Framework
{
    public class FrameworkStorage
    {
        #region Declare
        OStorage.Response _StorageResponse;
        #endregion
        #region Entity
        HCCoreStorage _HCCoreStorage;
        HCoreContext _HCoreContext;
        #endregion
        #region Aws
        IAmazonS3 _IAmazonS3;
        #endregion
        #region Save
        /// <summary>
        /// Description: Saves the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveStorage(OStorage.Save _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TypeCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS101");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Extension))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS102");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Content))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS103");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Type Details
                        int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, HelperType.StorageType, _Request.UserReference);
                        if (TypeId == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS104");
                            #endregion
                        }
                        #endregion
                        #region Helper Id 
                        int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                        #endregion
                        long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                        long? UserId = HCoreHelper.GetUserAccountId(_Request.UserKey, _Request.UserReference);
                        #region Get Settings 
                        if (HCoreConstant._AppConfig != null)
                        {

                            if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                            {
                                bool IsFileSaved = false;
                                string FileServerName = HCoreHelper.GenerateGuid() + "." + _Request.Extension;
                                string FileDirectory = HCoreHelper.GetGMTDate().Year + "/" + HCoreHelper.GetGMTDate().Month + "/";
                                string FilePath = FileDirectory + FileServerName;
                                #region Aws
                                if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                {
                                    #region Save Image To S3 
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(_Request.Content);
                                    using (_IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.USEast1))
                                    {
                                        using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                        {
                                            try
                                            {
                                                PutObjectRequest _PutObjectRequest = new PutObjectRequest
                                                {
                                                    BucketName = _AppConfig.StorageLocation,
                                                    Key = FilePath,
                                                    InputStream = _MemoryStream,
                                                    CannedACL = S3CannedACL.PublicRead,
                                                    StorageClass = S3StorageClass.Standard,
                                                };
                                                _PutObjectRequest.Headers.ContentLength = Base64FileContentBytes.Length;
                                                PutObjectResponse response = _IAmazonS3.PutObjectAsync(_PutObjectRequest).Result;
                                                IsFileSaved = true;
                                            }
                                            catch (AmazonS3Exception _AmazonS3Exception)
                                            {
                                                IsFileSaved = false;
                                                #region  Log Exception
                                                HCoreHelper.LogException("SaveStorage-AWSSave", _AmazonS3Exception, _Request.UserReference);
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                                #region Hosting
                                else if (_AppConfig.StorageSourceCode == "storagesource.folder")
                                {
                                    try
                                    {
                                        byte[] Base64FileContentBytes = Convert.FromBase64String(_Request.Content);
                                        if (!Directory.Exists(FileDirectory))
                                        {
                                            System.IO.Directory.CreateDirectory(FileDirectory);
                                        }
                                        if (Directory.Exists(FileDirectory))
                                        {
                                            using (FileStream Writer = new System.IO.FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
                                            {
                                                Writer.Write(Base64FileContentBytes, 0, Base64FileContentBytes.Length);
                                            }
                                        }
                                        IsFileSaved = true;
                                    }
                                    catch (Exception _Exception)
                                    {
                                        IsFileSaved = false;
                                        #region  Log Exception

                                        HCoreHelper.LogException("SaveStorage-FolderSave", _Exception, _Request.UserReference);
                                        #endregion
                                    }
                                }
                                else if (_AppConfig.StorageSourceCode == "storagesource.gcloud")
                                {
                                    var _ClientSecrets = new ClientSecrets();
                                    _ClientSecrets.ClientId = _AppConfig.StorageAccessKey;
                                    _ClientSecrets.ClientSecret = _AppConfig.StoragePrivateKey;
                                    var _GScopes = new[] { @"https://www.googleapis.com/auth/devstorage.full_control" };
                                    var _CancellationTokenSource = new CancellationTokenSource();
                                    var _UserCredentials = GoogleWebAuthorizationBroker.AuthorizeAsync(_ClientSecrets, _GScopes, "accounts@thankucash.com", _CancellationTokenSource.Token);
                                    var _StorageService = new Google.Apis.Storage.v1.StorageService();
                                    var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                                    {
                                        Bucket = _AppConfig.StorageLocation,
                                        Name = FilePath,
                                    };
                                    byte[] Base64FileContentBytes = Convert.FromBase64String(_Request.Content);
                                    using (var _MemoryStream = new MemoryStream(Base64FileContentBytes))
                                    {
                                        try
                                        {
                                            var _UploadRequest = new Google.Apis.Storage.v1.ObjectsResource.InsertMediaUpload(_StorageService, _UploadObject,
                                            _AppConfig.StorageLocation, _MemoryStream, "image/" + _Request.Extension);
                                            _UploadRequest.OauthToken = _UserCredentials.Result.Token.AccessToken;
                                            _UploadRequest.Upload();
                                            IsFileSaved = true;
                                        }
                                        catch (Exception _Exception)
                                        {
                                            IsFileSaved = false;
                                            #region  Log Exception
                                            HCoreHelper.LogException("SaveStorage-GCloud", _Exception, _Request.UserReference);
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                                #region Error
                                else
                                {
                                    IsFileSaved = false;
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS105");
                                    #endregion
                                }
                                #endregion
                                if (IsFileSaved == true)
                                {
                                    _HCCoreStorage = new HCCoreStorage();
                                    _HCCoreStorage.Guid = HCoreHelper.GenerateGuid();
                                    //if (UserId != null)
                                    //{
                                    //    _HCCoreStorage.UserId = UserId;
                                    //}
                                    //else
                                    //{
                                    //    //if (_Request.UserReference.UserId != 0)
                                    //    //{
                                    //    //    _HCCoreStorage.UserId = _Request.UserReference.UserId;
                                    //    //}
                                    //}

                                    if (UserAccountId != null)
                                    {
                                        _HCCoreStorage.AccountId = UserAccountId;
                                    }
                                    else
                                    {
                                        if (_Request.UserReference.AccountId != 0)
                                        {
                                            _HCCoreStorage.AccountId = _Request.UserReference.AccountId;
                                        }
                                    }
                                    _HCCoreStorage.StorageTypeId = (int)TypeId;
                                    _HCCoreStorage.SourceId = _AppConfig.StorageSourceId;
                                    _HCCoreStorage.Name = _Request.Name;
                                    _HCCoreStorage.Path = FilePath;
                                    _HCCoreStorage.Extension = _Request.Extension;
                                    _HCCoreStorage.HelperId = HelperId;
                                    _HCCoreStorage.Size = 0;
                                    _HCCoreStorage.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreStorage.StatusId = StatusActive;
                                    _HCoreContext.HCCoreStorage.Add(_HCCoreStorage);
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _StorageResponse = new OStorage.Response();
                                        _StorageResponse.ReferenceId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _HCCoreStorage.Guid).Select(x => x.Id).FirstOrDefault();
                                        _StorageResponse.ReferenceKey = _HCCoreStorage.Guid;
                                        _StorageResponse.Name = _Request.Name;
                                        _StorageResponse.Url = _AppConfig.StorageUrl + FilePath;
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StorageResponse, "HCS106");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS107");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS108");
                                #endregion
                            }

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS110");
                            #endregion
                        }
                        #endregion
                        #region Send Response
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS110");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception

                return HCoreHelper.LogException("SaveStorage", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Delete
        /// <summary>
        /// Description: Deletes the storage by reference identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteStorageByReferenceId(OStorage.Delete _Request)
        {

            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS111");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var FileDetails = _HCoreContext.HCCoreStorage.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                        if (FileDetails != null)
                        {
                            #region Get Settings 
                            if (_AppConfig != null)
                            {

                                if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                                {
                                    bool IsFileDeleted = false;
                                    #region Aws
                                    if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                    {
                                        #region Save Image To S3
                                        using (_IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.USEast1))
                                        {
                                            try
                                            {
                                                DeleteObjectRequest _DeleteObjectRequest = new DeleteObjectRequest
                                                {
                                                    BucketName = _AppConfig.StorageLocation,
                                                    Key = FileDetails.Path,
                                                };
                                                DeleteObjectResponse _DeleteObjectResponse = _IAmazonS3.DeleteObjectAsync(_DeleteObjectRequest).Result;
                                                IsFileDeleted = true;
                                            }
                                            catch (AmazonS3Exception _AmazonS3Exception)
                                            {
                                                IsFileDeleted = false;
                                                #region  Log Exception
                                                HCoreHelper.LogException("DeleteStorageByReferenceId-AWSSave", _AmazonS3Exception, _Request.UserReference);
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                    #endregion
                                    #region Hosting
                                    else if (_AppConfig.StorageSourceCode == "storagesource.folder")
                                    {
                                        try
                                        {
                                            if (File.Exists(FileDetails.Path))
                                            {
                                                File.Delete(FileDetails.Path);
                                            }
                                            IsFileDeleted = true;
                                        }
                                        catch (Exception _Exception)
                                        {
                                            IsFileDeleted = false;
                                            #region  Log Exception

                                            HCoreHelper.LogException("DeleteStorageByReferenceId-FolderSave", _Exception, _Request.UserReference);
                                            #endregion
                                        }
                                    }
                                    #endregion
                                    #region Error
                                    else
                                    {
                                        IsFileDeleted = false;
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS112");
                                        #endregion
                                    }
                                    #endregion
                                    if (IsFileDeleted == true)
                                    {
                                        _HCoreContext.HCCoreStorage.Remove(FileDetails);
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCS113");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS114");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS115");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS117");
                                #endregion
                            }
                            #endregion

                            #region Send Response
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS117");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS118");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception

                return HCoreHelper.LogException("DeleteStorageByReferenceId", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the storage by reference key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteStorageByReferenceKey(OStorage.Delete _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS119");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var FileDetails = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (FileDetails != null)
                        {
                            #region Get Settings 
                            if (_AppConfig != null)
                            {

                                if (!string.IsNullOrEmpty(_AppConfig.StorageSourceCode))
                                {
                                    bool IsFileDeleted = false;
                                    #region Aws
                                    if (_AppConfig.StorageSourceCode == "storagesource.amazons3")
                                    {
                                        #region Save Image To S3
                                        using (_IAmazonS3 = new AmazonS3Client(_AppConfig.StorageAccessKey, _AppConfig.StoragePrivateKey, Amazon.RegionEndpoint.USEast1))
                                        {
                                            try
                                            {
                                                DeleteObjectRequest _DeleteObjectRequest = new DeleteObjectRequest
                                                {
                                                    BucketName = _AppConfig.StorageLocation,
                                                    Key = FileDetails.Path,
                                                };
                                                DeleteObjectResponse _DeleteObjectResponse = _IAmazonS3.DeleteObjectAsync(_DeleteObjectRequest).Result;
                                                IsFileDeleted = true;
                                            }
                                            catch (AmazonS3Exception _AmazonS3Exception)
                                            {
                                                IsFileDeleted = false;
                                                #region  Log Exception

                                                HCoreHelper.LogException("DeleteStorageByReferenceKey-AWSSave", _AmazonS3Exception, _Request.UserReference);
                                                #endregion
                                            }
                                        }
                                        #endregion

                                    }
                                    #endregion
                                    #region Hosting
                                    else if (_AppConfig.StorageSourceCode == "storagesource.folder")
                                    {
                                        try
                                        {

                                            if (File.Exists(FileDetails.Path))
                                            {
                                                File.Delete(FileDetails.Path);
                                            }
                                            IsFileDeleted = true;

                                        }
                                        catch (Exception _Exception)
                                        {
                                            IsFileDeleted = false;
                                            #region  Log Exception

                                            HCoreHelper.LogException("DeleteStorageByReferenceKey-FolderSave", _Exception, _Request.UserReference);
                                            #endregion
                                        }

                                    }
                                    #endregion
                                    #region Error
                                    else
                                    {
                                        IsFileDeleted = false;
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS120");
                                        #endregion
                                    }
                                    #endregion

                                    if (IsFileDeleted == true)
                                    {
                                        _HCoreContext.HCCoreStorage.Remove(FileDetails);
                                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                        _HCoreContext.SaveChanges();
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCS121");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS122");
                                        #endregion
                                    }


                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS123");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS125");
                                #endregion
                            }
                            #endregion

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS126");
                            #endregion
                        }

                        #region Send Response
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS126");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception

                return HCoreHelper.LogException("DeleteStorageByReferenceKey", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorage(OStorage.Details _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS127");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        OStorage.Details Data = (from n in _HCoreContext.HCCoreStorage
                                                 where n.Guid == _Request.ReferenceKey
                                                 select new OStorage.Details
                                                 {
                                                     ReferenceKey = n.Guid,
                                                     TypeCode = n.StorageType.SystemName,
                                                     TypeName = n.StorageType.Name,
                                                     //HelperCode =n.HelperId.SystemName,
                                                     //HelperName   = n.Helper.Name,

                                                     Name = n.Name,
                                                     Url = _AppConfig.StorageUrl + n.Path,
                                                     Extension = n.Extension,
                                                     CreateDate = n.CreateDate,
                                                     Size = n.Size,
                                                     Status = n.StatusId,
                                                     StatusCode = n.Status.SystemName,
                                                     StatusName = n.Status.Name
                                                 }).FirstOrDefault();
                        if (Data != null)
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                            #endregion
                        }
                        #endregion

                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorage", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage by reference identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorageByReferenceId(OStorage.Details _Request)
        {

            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS128");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        OStorage.Details Data = (from n in _HCoreContext.HCCoreStorage
                                                 where n.Id == _Request.ReferenceId && n.StatusId == StatusActive
                                                 select new OStorage.Details
                                                 {
                                                     Url = _AppConfig.StorageUrl + n.Path,
                                                 }).FirstOrDefault();
                        if (Data != null)
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data.Url, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageByReferenceId", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage by reference key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorageByReferenceKey(OStorage.Details _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCS129");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        OStorage.Details Data = (from n in _HCoreContext.HCCoreStorage
                                                 where n.Guid == _Request.ReferenceKey
                                                 select new OStorage.Details
                                                 {
                                                     Url = _AppConfig.StorageUrl + n.Path,
                                                 }).FirstOrDefault();
                        if (Data != null)
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data.Url, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002");
                            #endregion
                        }
                        #endregion

                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetStorageByReferenceKey", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorageList(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "Status", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCCoreStorage
                                        select new OStorage.List
                                        {
                                            ReferenceKey = n.Guid,
                                            //UserKey = n.User.Guid,
                                            UserAccountKey = n.Account.Guid,

                                            TypeCode = n.StorageType.SystemName,
                                            TypeName = n.StorageType.Name,

                                            HelperCode = n.Helper.SystemName,
                                            HelperName = n.Helper.Name,

                                            Name = n.Name,
                                            Url = _AppConfig.StorageUrl + n.Path,
                                            Extension = n.Extension,
                                            CreateDate = n.CreateDate,
                                            Size = n.Size,
                                            Status = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = DefaultLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OStorage.List> Data = (from n in _HCoreContext.HCCoreStorage
                                                select new OStorage.List
                                                {
                                                    ReferenceKey = n.Guid,
                                                    //UserKey = n.User.Guid,
                                                    UserAccountKey = n.Account.Guid,

                                                    TypeCode = n.StorageType.SystemName,
                                                    TypeName = n.StorageType.Name,

                                                    HelperCode = n.Helper.SystemName,
                                                    HelperName = n.Helper.Name,

                                                    Name = n.Name,
                                                    Url = _AppConfig.StorageUrl + n.Path,
                                                    Extension = n.Extension,
                                                    CreateDate = n.CreateDate,
                                                    Size = n.Size,
                                                    Status = n.StatusId,
                                                    StatusCode = n.Status.SystemName,
                                                    StatusName = n.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion
    }
}
