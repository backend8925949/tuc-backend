//==================================================================================
// FileName: FrameworkRegion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkRegion
    {
        //#region Declare
        ////HCCore _HCCore;
        //HCoreContext _HCoreContext;
        //HCCoreParameter _HCCoreParameter;
        //#endregion

        //internal OResponse SaveRegion(ORegion.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

        //        if (StatusId == null)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC499_InvalidStatusCode);
        //            #endregion
        //        }
        //        if (_Request.Name == null)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC007_NameRequired);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                long CountryId = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.CountryReferenceKey).Select(x => x.Id).FirstOrDefault();
        //                if (CountryId == 0)
        //                {
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC066");
        //                }
        //                string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                long CheckConfiguration = _HCoreContext.HCCoreParameter.Where(x => x.SystemName == SystemName && x.CommonId == CountryId).Select(x => x.Id).FirstOrDefault();
        //                if (CheckConfiguration == 0)
        //                {
        //                    _HCCoreParameter = new HCCoreParameter();
        //                    _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
        //                    _HCCoreParameter.Name = _Request.Name;
        //                    _HCCoreParameter.TypeId = HelperType.CountryRegion;
        //                    _HCCoreParameter.SystemName = _Request.SystemName;
        //                    _HCCoreParameter.CommonId = CountryId;
        //                    _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
        //                    _HCCoreParameter.StatusId = (int)StatusId;
        //                    _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
        //                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                    _HCoreContext.SaveChanges();
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _HCCoreParameter.Guid, HCoreResponseCode.System.HC003_DetailsSaved);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC498_DetailsAlreadyPresent);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("SaveRegion", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse UpdateRegion(ORegion.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        #region Code Block

        //        int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {

        //            long CountryId = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.CountryReferenceKey).Select(x => x.Id).FirstOrDefault();
        //            if (StatusId == 0)
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCC100);
        //            }
        //            if (CountryId == 0)
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC068");
        //            }
        //            HCCoreParameter ItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (ItemDetails != null)
        //            {
        //                if (CountryId != 0)
        //                {
        //                    ItemDetails.CommonId = CountryId;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.Name) && ItemDetails.Name != _Request.Name)
        //                {
        //                    ItemDetails.Name = _Request.Name;
        //                    ItemDetails.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                }
        //                if (StatusId != null && ItemDetails.StatusId != StatusId)
        //                {
        //                    ItemDetails.StatusId = (int)StatusId;
        //                }
        //                ItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                ItemDetails.ModifyById = _Request.UserReference.AccountId;
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC004_DetailsUpdated);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("UpdateRegion", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse DeleteRegion(ORegion.Save _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        #region Operations
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            HCCoreParameter DeleteItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (DeleteItemDetails != null)
        //            {
        //                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
        //                _HCoreContext.HCCoreParameter.Remove(DeleteItemDetails);
        //                _HCoreContext.SaveChanges();
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, HCoreResponseCode.System.HC005_DetailsDeleted);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                #endregion
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("DeleteRegion", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetRegionsByCountry(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                #region Total Records
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Common.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryRegion)
        //                    .Select(x => new ORegion.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,

        //                        Cities = x.InverseParent.Count(),

        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,

        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    }).Where(_Request.SearchCondition).Count();
        //                }
        //                #endregion
        //                #region Set Default Limit
        //                if (_Request.Limit == -1)
        //                {
        //                    _Request.Limit = _Request.TotalRecords;
        //                }
        //                else if (_Request.Limit == 0)
        //                {
        //                    _Request.Limit = _AppConfig.DefaultRecordsLimit;

        //                }
        //                #endregion

        //                List<ORegion.List> Details = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Common.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryRegion)
        //                    .Select(x => new ORegion.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        Cities = x.InverseParent.Count(),
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    }).Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                    .ToList();
        //                _HCoreContext.Dispose();

        //                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Details, _Request.Offset, _Request.Limit);

        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, HCoreResponseCode.System.HC000_DetailsLoaded);
        //                #endregion

        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetRegionDetailsByCountryId", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}

        //internal OResponse GetRegionDetails(ORegion.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Code Block
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HCREF);
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Operations
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                ORegion.List Details = _HCoreContext.HCCoreParameter
        //                    .Where(a => a.Guid == _Request.ReferenceKey && a.TypeId == HelperType.CountryRegion)
        //                    .Select(x => new ORegion.List
        //                    {
        //                        ReferenceId = x.Id,
        //                        ReferenceKey = x.Guid,
        //                        Name = x.Name,
        //                        SystemName = x.SystemName,
        //                        CreateDate = x.CreateDate,
        //                        CreatedByKey = x.CreatedBy.Guid,
        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,
        //                        ModifyDate = x.ModifyDate,
        //                        ModifyByKey = x.ModifyBy.Guid,
        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,
        //                        StatusId = x.StatusId,
        //                        StatusCode = x.Status.SystemName,
        //                        StatusName = x.Status.Name
        //                    })
        //                    .FirstOrDefault();
        //                _HCoreContext.Dispose();


        //                if (Details != null)
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, HCoreResponseCode.System.HCC113);
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, HCoreResponseCode.System.HC002_DetailsNotFound);
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        return HCoreHelper.LogException("GetRegionDetails", _Exception, _Request.UserReference);
        //        #endregion
        //    }
        //    #endregion
        //}


    }
}
