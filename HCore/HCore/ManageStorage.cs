//==================================================================================
// FileName: ManageStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageStorage
    {
        #region Declare       
        FrameworkStorage _FrameworkStorage;
        #endregion
        #region Operations
        /// <summary>
        /// Description: Saves the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveStorage(OStorage.Save _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.SaveStorage(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Deletes the storage by reference identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteStorageByReferenceId(OStorage.Delete _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.DeleteStorageByReferenceId(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Deletes the storage by reference key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteStorageByReferenceKey(OStorage.Delete _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.DeleteStorageByReferenceKey(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorage(OStorage.Details _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.GetStorage(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage by reference identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorageByReferenceId(OStorage.Details _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.GetStorageByReferenceId(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage by reference key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorageByReferenceKey(OStorage.Details _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.GetStorageByReferenceKey(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorageList(OList.Request _Request)
        {
            #region Send Response
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.GetStorageList(_Request);
            #endregion
        }
        #endregion
    }
}
