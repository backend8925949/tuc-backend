//==================================================================================
// FileName: ManagePushNotification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Framework;

namespace HCore
{
    public class ManagePushNotification
    {
        FrameworkPushNotification _FrameworkPushNotification;
        /// <summary>
        /// Sends the push to device.
        /// </summary>
        /// <param name="DeviceKey">The device key.</param>
        /// <param name="Task">The task.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="NavigateTo">The navigate to.</param>
        /// <param name="ReferenceKey">The reference key.</param>
        /// <param name="ConfirmText">The confirm text.</param>
        public void SendPushToDevice(string DeviceKey, string? Task, string? Subject, string? Message, string? NavigateTo, string? ReferenceKey, string? ConfirmText)
        {
            _FrameworkPushNotification = new FrameworkPushNotification();
            _FrameworkPushNotification.SendPushToDevice(DeviceKey, Task, Subject, Message, NavigateTo, ReferenceKey, ConfirmText);
        }
        /// <summary>
        /// Sends the push to topic.
        /// </summary>
        /// <param name="TopicName">Name of the topic.</param>
        /// <param name="Task">The task.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="NavigateTo">The navigate to.</param>
        /// <param name="ReferenceKey">The reference key.</param>
        /// <param name="ConfirmText">The confirm text.</param>
        public void SendPushToTopic(string TopicName, string? Task, string? Subject, string? Message, string? NavigateTo, string? ReferenceKey, string? ConfirmText)
        {
            _FrameworkPushNotification = new FrameworkPushNotification();
            _FrameworkPushNotification.SendPushToTopic(TopicName, Task, Subject, Message, NavigateTo, ReferenceKey, ConfirmText);
        }
    }
}
