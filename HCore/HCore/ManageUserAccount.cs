//==================================================================================
// FileName: ManageUserAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore
{
    public class ManageUserAccount
    {
        #region Declare
        FrameworkUserAccount _FrameworkUserAccount;
        //FrameworkUserAccounts _FrameworkUserAccounts;
        //FrameworkUserAccountList _FrameworkUserAccountList;
        #endregion
        #region Delete user and user account
        //public OResponse DeleteUser(OUserAccount _Request)
        //{
        //    #region Send Response
        //    _FrameworkUserAccount = new FrameworkUserAccount();
        //    return _FrameworkUserAccount.DeleteUser(_Request);
        //    #endregion
        //}
        //public OResponse DeleteUserAccount(OUserAccount _Request)
        //{
        //    #region Send Response
        //    _FrameworkUserAccount = new FrameworkUserAccount();
        //    return _FrameworkUserAccount.DeleteUserAccount(_Request);
        //    #endregion
        //}
        #endregion

        #region Update profile
        /// <summary>
        /// Description: Updates the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccount(OUserAccount _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserAccount(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user account self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccountSelf(OUserAccount _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserAccountSelf(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user account status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccountStatus(OUserUpdateAccountStatus _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserAccountStatus(_Request);
            #endregion
        }
        #endregion
        #region Contact Number Manager
        /// <summary>
        /// Description: Updates the user contact number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserContactNumber(OUserContactNumberUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserContactNumber(_Request);
            #endregion
        }
        /// <summary>
        /// Updates the user contact number self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserContactNumberSelf(OUserContactNumberUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserContactNumberSelf(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user contact number verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserContactNumberVerify(OUserVerificationResponse _Request)
        {

            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserContactNumberVerify(_Request);
            #endregion
        }


        #endregion
        #region Email Address Manager
        /// <summary>
        /// Description: Updates the user email address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserEmailAddress(OUserEmailAddressUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserEmailAddress(_Request);
            #endregion
        }
        /// <summary>
        /// Updates the user email address self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserEmailAddressSelf(OUserEmailAddressUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserEmailAddressSelf(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user email address verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserEmailAddressVerify(OUserVerificationResponse _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserEmailAddressVerify(_Request);
            #endregion
        }
        #endregion
        #region Username manager
        /// <summary>
        /// Description: Updates the name of the user user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserUserName(OUserUserNameUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserUserName(_Request);
            #endregion
        }
        /// <summary>
        /// Updates the user user name self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserUserNameSelf(OUserUserNameUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserUserNameSelf(_Request);
            #endregion
        }
        #endregion

        #region Access Pin Manager
        /// <summary>
        /// Description: Sets the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetUserAccessPin(OUserAccessPinUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.SetUserAccessPin(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Sets the user access pin self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetUserAccessPinSelf(OUserAccessPinUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.SetUserAccessPinSelf(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccessPin(OUserAccessPinUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserAccessPin(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user access pin self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccessPinSelf(OUserAccessPinUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserAccessPinSelf(_Request);
            #endregion
        }
        #endregion

        #region Passwords Manager
        /// <summary>
        /// Description: Updates the user password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserPassword(OUserPasswordUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserPassword(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Updates the user password self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserPasswordSelf(OUserPasswordUpdate _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.UpdateUserPasswordSelf(_Request);
            #endregion
        }
        #endregion

        #region Forgot Access Pin
        /// <summary>
        /// Description: Forgets the user access pin request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgetUserAccessPinRequest(OForgotAccessPin.Request _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgetUserAccessPinRequest(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Forgets the user access pin verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgetUserAccessPinVerify(OForgotAccessPin.Verify _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgetUserAccessPinVerify(_Request);
            #endregion
        }
        #endregion
        #region Forgot  Password
        /// <summary>
        /// Description: Forgots the password request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgotPasswordRequest(ForgotPassword.Request _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgotPasswordRequest(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Forgots the password request verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgotPasswordRequestVerify(ForgotPassword.Verify _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgotPasswordRequestVerify(_Request);
            #endregion
        }
        #endregion

        #region Forgot  Username
        /// <summary>
        /// Description: Forgots the user name request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgotUserNameRequest(ForgotUserName.Request _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgotUserNameRequest(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Forgots the user name request verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ForgotUserNameRequestVerify(ForgotUserName.Verify _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.ForgotUserNameRequestVerify(_Request);
            #endregion
        }
        #endregion

        #region Get
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccount(OUserAccount _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.GetUserAccount(_Request);
            #endregion
        }

        /// <summary>
        /// Description: Gets the user account self.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountSelf(OUserAccount _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.GetUserAccountSelf(_Request);
            #endregion
        }
        //public OResponse GetUserAccounts(OList.Request _Request)
        //{
        //    #region Send Response
        //    _FrameworkUserAccountList = new FrameworkUserAccountList();
        //    return _FrameworkUserAccountList.GetUserAccounts(_Request);
        //    #endregion
        //}
        /// <summary>
        /// Description: Gets the user details change log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserDetailsChangeLog(OList.Request _Request)
        {
            #region Send Response
            _FrameworkUserAccount = new FrameworkUserAccount();
            return _FrameworkUserAccount.GetUserDetailsChangeLog(_Request);
            #endregion
        }
        //public OResponse GetUserAccountOverview(OUserOverview.Request _Request)
        //{
        //    #region Send Response
        //    _FrameworkUserAccounts = new FrameworkUserAccounts();
        //    return _FrameworkUserAccounts.GetUserAccountOverview(_Request);
        //    #endregion
        //}
        //public OResponse GetUserAnalytics(OChart.Request _Request)
        //{
        //    #region Send Response
        //    _FrameworkUserAccounts = new FrameworkUserAccounts();
        //    return _FrameworkUserAccounts.GetUserAnalytics(_Request);
        //    #endregion
        //}
        #endregion
    }
 
    public class ManageUserAccountAccess
    {
        #region Declare
        FrameworkUserAccountAccess _FrameworkUserAccountAccess;
        #endregion
        #region Manage Operations
        /// <summary>
        /// Description: Logins the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse Login(OUserAccountAccess.ORequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.Login(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Admins the login request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse AdminLoginRequest(OUserAccountAccess.ORequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.AdminLoginRequest(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Admins the login confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse AdminLoginConfirm(OUserAccountAccess.ORequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.AdminLoginConfirm(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Logins the application user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse LoginAppUser(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.LoginAppUser(_Request);
            #endregion
        }

        /// <summary>
        /// Description: Logins the application user v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse LoginAppUserV2(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.LoginAppUserV2(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Logins the application user with pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse LoginAppUserWithPin(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.LoginAppUserWithPin(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Applications the user register.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse AppUserRegister(OUserAccountAccess.AppProcessRequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.AppUserRegister(_Request);
            #endregion
        }


        /// <summary>
        /// Description: Logouts the specified o user reference.
        /// </summary>
        /// <param name="_OUserReference">The o user reference.</param>
        /// <returns>OResponse.</returns>
        public OResponse Logout(OUserAccountAccess.ORequest _OUserReference)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.Logout(_OUserReference);
            #endregion
        }
        /// <summary>
        /// Description: Registers the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse Register(OUserAccountRegister _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.Register(_Request);
            #endregion
        }

        /// <summary>
        /// Description: Creates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OCreateUser.</returns>
        public OCreateUser CreateAccount(OCreateUser _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.CreateAccount(_Request);
            #endregion
        }

        /// <summary>
        /// Description: Updates the temporary pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateTempPin(OUserAccountAccess.PinUpdateRequest _Request)
        {
            #region Send Response
            _FrameworkUserAccountAccess = new FrameworkUserAccountAccess();
            return _FrameworkUserAccountAccess.UpdateTempPin(_Request);
            #endregion
        }
        #endregion
    }
}
