//==================================================================================
// FileName: OUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using static HCore.CoreConstant;

using HCore.Helper;
namespace HCore.Object
{
    public class OUserParameter
    {
        public class Manage
        {
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? UserKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDeviceKey { get; set; }

            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }

            public string? CommonKey { get; set; }
            public string? ParameterKey { get; set; }
            public string? HelperCode { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }
            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }

            public OUserReference? UserReference { get; set; }

            public string? StatusCode { get; set; }
        }

        public class Details
        {
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentName { get; set; }

            public string? UserKey { get; set; }
            public string? UserName { get; set; }

            public string? UserAccountOwnerKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? DeviceKey { get; set; }
            public string? DeviceSerialNumber { get; set; }

            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }

            public string? CommonKey { get; set; }
            public string? CommonName { get; set; }
            public string? CommonSystemName { get; set; }

            public string? ParameterKey { get; set; }
            public string? ParameterName { get; set; }



            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }

            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
             
            public string? RequestKey { get; set; }


            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }

    }

    public class OUserSession
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }

        public string? UserAccountKey { get; set; }
        public string? UserAccountDisplayName { get; set; }

        public string? UserAccountDeviceKey { get; set; }
        public string? UserAccountDeviceName { get; set; }

        public string? OsKey { get; set; }
        public string? OsName { get; set; }

        public string? AppKey { get; set; }
        public string? AppName { get; set; }

        public string? AppVersionKey { get; set; }
        public string? AppVersionName { get; set; }

        public DateTime LastActivityDate { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }

        public string? IpAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string? Location { get; set; }

        public string? BrowserName { get; set; }
        public string? BrowserVersion { get; set; }


        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }
    
    public class OUserActivity
    {
        public long ActivityId { get; set; }
        public string? ActivityKey { get; set; }

        public string? AccountTypeCode { get; set; }
        public string? AccountTypeName { get; set; }

        public string? UserDeviceKey { get; set; }
        public string? UserDeviceSerialNumber { get; set; }

        public string? UserAccountKey { get; set; }
        public string? UserAccountName { get; set; }
        public string? UserAccountIconUrl { get; set; }
         

        public string? PermissionKey { get; set; }
        public string? PermissionName { get; set; }


        public string? ActivityTypeCode { get; set; }
        public string? ActivityTypeName { get; set; }

        public string? ParentReferenceKey { get; set; }
        public string? ReferenceKey { get; set; }

        public string? FieldName { get; set; }
        public string? EntityName { get; set; }
        public string? OldValue { get; set; }
        public string? NewValue { get; set; }

        public string? Description { get; set; }

        public DateTime CreateDate { get; set; }

        public long Status { get; set; }
    }




    //public class OUserActivityLog
    //{
    //    public List<OActivity> Activities { get; set; }
    //    public string? EntityName { get; set; }
    //    public string? ParentReferenceKey { get; set; }
    //    public string? ReferenceKey { get; set; }
    //    public List<OChangeLog> ChangeLog { get; set; }
    //    public OUserReference? UserReference { get; set; }
    //}

    //public class OActivity
    //{
    //    public ActivityType ActivityType { get; set; }
    //    public string? ParentReferenceKey { get; set; }
    //    public string? ReferenceKey { get; set; }
    //    public string? OldValue { get; set; }
    //    public string? NewValue { get; set; }
    //    public string? Description { get; set; }
    //    public string? ActivityCode { get; set; }
    //}

    //public class OChangeLog
    //{
    //    public ActivityType ActivityType { get; set; }
    //    public string? FieldName { get; set; }
    //    public string? OldValue { get; set; }
    //    public string? NewValue { get; set; }
    //}


}
