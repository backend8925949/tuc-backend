//==================================================================================
// FileName: OTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Object
{
    public class OTransactionUserDetails
    {
        public long ReferenceId { get; set; }
        public string? DisplayName { get; set; }
        public string? Name { get; set; }
        public string? EmailAddress { get; set; }
        public string? ContactNumber { get; set; }
        public long AccountType { get; set; }
        public string? CountryKey { get; set; }
        public long? OwnerId { get; set; }
        public long? OwnerAccountType { get; set; }
        public long? OwnerParentId { get; set; }
        public long? OwnerParentAccountType { get; set; }
    }
    public class OTransactionReference
    {
        public long Status { get; set; }
        public string? Message { get; set; }
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? GroupKey { get; set; }
        public DateTime TransactionDate { get; set; }
    }
    public class OSaveTransactions
    {

        public string? GroupKey { get; set; }
        public string? ParentTransactionKey { get; set; }

        public long ParentId { get; set; }

        public string? AccountNumber { get; set; }
        public string? ReferenceNumber { get; set; }

        public double InvoiceAmount { get; set; }
        public string? InvoiceNumber { get; set; }
        public string? Data { get; set; }
        public int StatusId { get; set; }
        public OUserReference? UserReference { get; set; }
        public List<OTransactionItem> Transactions { get; set; }
        public long CreatedById { get; set; }

    }
    public class OTransactionItem
    {
        public long UserAccountId { get; set; }

        public int ModeId { get; set; }
        public int TypeId { get; set; }
        public int SourceId { get; set; }


        public double Amount { get; set; }

        public double Charge { get; set; }

        public double Comission { get; set; }

        public double TotalAmount { get; set; }

        public double ReferenceAmount { get; set; }

        public string? Description { get; set; }
        public string? Comment { get; set; }
        public DateTime? TransactionDate { get; set; }

    }
    public class OSaveTransaction
    {
        public long UserAccountId { get; set; }

        public int ModeId { get; set; }
        public int TypeId { get; set; }
        public int SourceId { get; set; }


        public double Amount { get; set; }
        public double AmountPercentage { get; set; }

        public double Charge { get; set; }
        public double ChargePercentage { get; set; }

        public double ComissionAmount { get; set; }
        public double ComissionPercentage { get; set; }

        public double PayableAmount { get; set; }
        public double TotalAmount { get; set; }
        //public double TotalAmountPercentage { get; set; }
        public double? ReferenceAmount { get; set; }


        //public double CardBalance { get; set; }

        public double PurchaseAmount { get; set; }

        public long ParentTransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }

        public long ParentId { get; set; }

        public string? AccountNumber { get; set; }
        public string? InvoiceNumber { get; set; }
        public string? ReferenceNumber { get; set; }
        public string? Description { get; set; }

        public string? Data { get; set; }

        public int Status { get; set; }

        public long GroupId { get; set; }
        public string? GroupKey { get; set; }

        public long CreatedById { get; set; }

        public OUserReference? UserReference { get; set; }


    }
    public class OTransactionResponse
    {
        public double Amount { get; set; }
        public OTransactionHelper Sender { get; set; }
        public OTransactionHelper Receiver { get; set; }
    }
    public class OProcessTransaction
    {
        public string? Type { get; set; }
        //public string? AmountType { get; set; }

        public double Amount { get; set; }
        public double? CardBalance { get; set; }
        public double? PurchaseAmount { get; set; }

        public DateTime TransactionDate { get; set; }
        public string? AccountNumber { get; set; }
        public string? ReferenceNumber { get; set; }
        public string? Description { get; set; }
        public string? Data { get; set; }

        public OTransactionHelper Sender { get; set; }
        public OTransactionHelper Receiver { get; set; }
        public string? StatusCode { get; set; }

        public OUserReference? UserReference { get; set; }
    }
    public class OTransactionHelper
    {
        public string? UserAccountKey { get; set; }
        public string? Source { get; set; }
        public double Charge { get; set; }
        public double ComissionPercentage { get; set; }
        public double DiscountPercentage { get; set; }

        public long TransactionId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? InoviceNumber { get; set; }
        public double Amount { get; set; }
    }
    public class OTransaction
    {
        public class RequestLog
        {
            public string? Request { get; set; }
            public string? Response { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountName { get; set; }
            public string? UserAccountContactNumber { get; set; }
            public string? UserAccountEmailAddress { get; set; }
            public string? UserAccountUserName { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }


            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }


            public string? TypeKey { get; set; }
            public string? TypeValue { get; set; }
            public string? TypeName { get; set; }

            public int AmountType { get; set; }

            public double? AmountPercentage { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }

            public double DiscountPercentage { get; set; }
            public double DiscountAmount { get; set; }

            public double ComissionPercentage { get; set; }
            public double ComissionAmount { get; set; }

            public double PayableAmount { get; set; }

            public double TotalAmount { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }


            public double Balance { get; set; }
            public double? CardBalance { get; set; }


            public double PointsValue { get; set; }
            public double ConversionAmount { get; set; }
            public double PurchaseAmount { get; set; }

            public DateTime TransactionDate { get; set; }

            public string? AccountNumber { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? RefTransactionKey { get; set; }
            public double? RefTransactionTotalAmount { get; set; }
            public double? RefTransactionConversionAmount { get; set; }
            public double? RefTransactionComission { get; set; }
            public double? RefTransactionComissionPercentage { get; set; }

            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public string? OwnerAddress { get; set; }

            public string? OwnerParentKey { get; set; }
            public string? OwnerParentName { get; set; }



            public string? ParentTransactionKey { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }

            public string? IpAddress { get; set; }

            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public string? CreatedByControllerKey { get; set; }
            public string? CreatedByControllerDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public DateTime? ModifyDateByController { get; set; }

            public string? ModifyByControllerKey { get; set; }
            public string? ModifyByControllerDisplayName { get; set; }

            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? GroupKey { get; set; }

            public List<OTransaction.Details> RelatedTransactions { get; set; }
            public OTransaction.RequestLog RequestLog { get; set; }

            public OUserReference? UserReference { get; set; }
            public string? RequestKey { get; set; }

        }
        public class TList
        {
            #region Parameters
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountName { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }

            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public double PurchaseAmount { get; set; }
            public double ReferenceAmount { get; set; }


            public double? UserAmount { get; set; }
            public double? ThankUAmount { get; set; }
            public double? MerchantAmount { get; set; }
            public double? AcquirerAmount { get; set; }
            public double? PTSPAmount { get; set; }
            public double? PGAmount { get; set; }
            public double? PTSAAmount { get; set; }


            public DateTime TransactionDate { get; set; }
            public DateTime? CreateDate { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
            #endregion

            public double ComissionPercentage { get; set; }
            public double Balance { get; set; }
            public double ConversionAmount { get; set; }


            public string? AccountNumber { get; set; }


            public double? RefTransactionTotalAmount { get; set; }
            public double? RefTransactionConversionAmount { get; set; }
            public double? RefTransactionComission { get; set; }
            public double? RefTransactionComissionPercentage { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }

            public string? OwnerParentKey { get; set; }
            public string? OwnerParentDisplayName { get; set; }
        }
        public class Balance
        {
            public class Request
            {
                public string? UserKey { get; set; }
                public string? UserAccountKey { get; set; }
                public string? Source { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long Credit { get; set; }
                public long Debit { get; set; }
                public long Balance { get; set; }

                public DateTime? LastTransactionTime { get; set; }
                public double? ExpiaryTimeMinutes { get; set; }
                public double? InvoiceAmountPaid { get; set; }

                public long? TotalInvoice { get; set; }
                public long? PaidInvoice { get; set; }
                public double? InvoiceAmountUnpaid { get; set; }
                public long? UnpaidInvoice { get; set; }
                public long? BalanceValidity { get; set; }

            }
        }
        public class Overview
        {
            public class Request
            {
                public string? Type { get; set; }

                public string? ReferenceKey { get; set; }
                public string? UserAccountKey { get; set; }

                public string? UserAccountTypeCode { get; set; }

                public string? SourceCode { get; set; }
                public string? TypeCode { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                internal long ReferenceId { get; set; }
                public string? Name { get; set; }

                public OTransactionModeValue Credit { get; set; }
                public OTransactionModeValue Debit { get; set; }

                public double TotalCredit { get; set; }
                public double TotalDebit { get; set; }
                public double TotalBalance { get; set; }

                public List<OTransactionType> Types { get; set; }
            }
        }
        public class List
        {
            #region Parameters
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }


            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public double ConversionAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }


            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }

            public double? ParentTransactionAmount { get; set; }
            public double? ParentTransactionTotalAmount { get; set; }

            public long? OwnerId { get; set; }
            //public long? InvoiceId { get; set; }
            //public long? InvoiceItemId { get; set; }
            public long Status { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? GroupKey { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? AcquirerAmount { get; set; }
            public double? PTSAAmount { get; set; }
            public double? PSSPAmount { get; set; }
            public double? IssuerAmount { get; set; }
            public double? ThankUAmount { get; set; }

            public string? CardBrandName { get; set; }
            public string? CardBankName { get; set; }
            public string? CardBinNumber { get; set; }
            public string? AcquirerName { get; set; }
            #endregion

            #region Tags
            #endregion

            public OUserReference? UserReference { get; set; }

        }
    }
    public class OTransactionType
    {
        internal int ReferenceId { get; set; }
        public string? Name { get; set; }
        public string? SystemName { get; set; }
        public OTransactionModeValue Credit { get; set; }
        public OTransactionModeValue Debit { get; set; }
        public double TotalCredit { get; set; }
        public double TotalDebit { get; set; }
        public double TotalBalance { get; set; }
    }
    public class OTransactionModeValue
    {
        internal int ReferenceId { get; set; }
        public double? Amount { get; set; }
        public double? ComissionAmount { get; set; }
        public double? DiscountAmount { get; set; }
        public double? PayableAmount { get; set; }
        public double? TotalAmount { get; set; }

        public double? ConversionAmount { get; set; }
        public double? PurchaseAmount { get; set; }

        public double? Total { get; set; }
    }
    public partial class OTransactionList
    {

        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }

        public string? InoviceNumber { get; set; }
        public string? ParentOwnerKey { get; set; }


        internal long? ParentId { get; set; }

        public string? ParentKey { get; set; }
        public string? ParentDisplayName { get; set; }
        public string? ParentIconUrl { get; set; }


        internal long? UserAccountId { get; set; }
        public string? UserKey { get; set; }
        public string? UserAccountKey { get; set; }
        public string? UserAccountDisplayName { get; set; }
        public string? UserAccountMobileNumber { get; set; }

        public string? UserAccountTypeCode { get; set; }
        public string? UserAccountTypeName { get; set; }

        public string? ModeKey { get; set; }
        public string? ModeName { get; set; }

        public string? SourceKey { get; set; }
        public string? SourceName { get; set; }

        public string? TypeKey { get; set; }
        public string? TypeName { get; set; }
        public string? TypeCategory { get; set; }

        public double Amount { get; set; }
        public double Charge { get; set; }
        public double ComissionAmount { get; set; }
        public double TotalAmount { get; set; }

        public double ConversionAmount { get; set; }
        public double PurchaseAmount { get; set; }

        public double? UserAmount { get; set; }
        public double? MerchantAmount { get; set; }
        public double? ThankUAmount { get; set; }

        public double? RefTrTotalAmount { get; set; }

        public DateTime TransactionDate { get; set; }


        public string? OwnerKey { get; set; }
        public string? OwnerDisplayName { get; set; }
        public string? OwnerIconUrl { get; set; }

        public DateTime? CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }

        public string? CreatedByAccountTypeName { get; set; }
        public string? CreatedByAccountTypeCode { get; set; }

        public string? CreatedByControllerKey { get; set; }
        public string? CreatedByControllerDisplayName { get; set; }

        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }
        public long? InId { get; set; }



        public string? GroupKey { get; set; }

        public string? UserProfileKey { get; set; }
        public string? UserDisplayName { get; set; }
        public long? UserAccountTypeId { get; set; }

        public string? MerchantKey { get; set; }
        public string? MerchantDisplayName { get; set; }
        public long? CreatedByAccountTypeId { get; set; }

        public double? RewardAmount { get; set; }
        public double? ThankUComission { get; set; }

        #region Tags
        #endregion
    }

}
