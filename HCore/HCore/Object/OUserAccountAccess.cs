//==================================================================================
// FileName: OUserAccountAccess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Object
{
    public class OUserAccountAccess
    {
        public class AppProcessRequest
        {
            public long AccountId { get; set; }
            public string? CountryIso { get; set; }
            public int CountryId { get; set; }
            public string? PlatformCode { get; set; } = "mobile";
            public string? MobileNumber { get; set; }
            public string? AccessPin { get; set; }
            public string? Address { get; set; }

            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }

            public string? EmailAddress { get; set; }

            public string? GenderCode { get; set; }

            public DateTime? DateOfBirth { get; set; }
            public string? ReferralCode { get; set; }

            public string? SerialNumber { get; set; }

            public string? Brand { get; set; }
            public string? Model { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string? OsVersion { get; set; }
            public string? CarrierName { get; set; }
            public string? Mcc { get; set; }
            public string? Mnc { get; set; }
            public string? NotificationUrl { get; set; }

            public string? Pin { get; set; }
            public string? UserType { get; set; }
            public OAddress AddressComponent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class OAddress
        {
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? Landmark { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public long? CityAreaId { get; set; }
            public string? CityAreaName { get; set; }

            public long? CityId { get; set; }
            public string? CityName { get; set; }
            public long? StateId { get; set; }
            public string? StateName { get; set; }
            public long? CountryId { get; set; }
            public string? CountryName { get; set; }
            public string? ZipCode { get; set; }
            public string? Instructions { get; set; }
            public string? MapAddress { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

        }


        public class ORequest
        {

            public string? AccessKey { get; set; }
            public string? PublicKey { get; set; }

            public string? Code { get; set; }

            public string? Type { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? AccountKey { get; set; }
            public string? SecondaryPassword { get; set; }
            public string? PasswordKey { get; set; }
            public string? AccountType { get; set; }
            public string? SerialNumber { get; set; }
            public string? NotificationUrl { get; set; }
            public string? PlatformCode { get; set; }

            public string? Brand { get; set; }
            public string? Model { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string? OsVersion { get; set; }
            public string? CarrierName { get; set; }
            public string? Mcc { get; set; }
            public string? Mnc { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class OResponse
        {
            public string? AccessKey { get; set; }
            public string? PublicKey { get; set; }
            public DateTime LoginTime { get; set; }
            public bool IsFirstLogin { get; set; } = false;
            public bool? IsTempPin { get; set; }
            public OUser User { get; set; }
            public OUserAccount UserAccount { get; set; }
            public OUserAccountOwner UserOwner { get; set; }
            public OUserAccountOwner UserSubOwner { get; set; }
            public OUserAccountCountry UserCountry { get; set; }
            public OSubscription Subscription { get; set; }
            public Address? Address { get; set; }
            public List<UserAccountTypes> UserAccountTypes { get; set; }
            public List<OUserAccountRolePermission> Permissions { get; set; }
            public List<OFeatures> Features { get; set; }
            public List<OUserAccountRole> UserRoles { get; set; }
            public List<Stores> Stores { get; set; }
        }
        public class UserAccountTypes
        {
            public int? TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? Value { get; set; }
            public int? SubTypeId { get; set; }
        }

        public class Address
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int? LocationTypeId { get; set; }
            public string? LocationTypeName { get; set; }

            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public string? AdditionalMobile { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? ZipCode { get; set; }
            public string? Landmark { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public int CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? MapAddress { get; set; }
            public string? Instructions { get; set; }
            public sbyte? IsPrimaryAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class OSubscription
        {
            public long PlanReferenceId { get; set; }
            public string? PlanReferenceKey { get; set; }
            public long SubscriptionId { get; set; }
            public string? SubscriptionKey { get; set; }
            public string? Title { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int DaysLeft { get; set; }
            //public bool IsSubscriptionAvailable { get; set; }
            public double Amount { get; set; }
            public bool IsSubscriptionActive { get; set; }
        }
        public class OFeatures
        {
            public string? SystemName { get; set; }
            public string? Name { get; set; }
            public string? ActionType { get; set; }
            public long? ParentId { get; set; }
            public long? HelperId { get; set; }
            public long? Id { get; set; }
        }
        public class ListFeatures
        {
            public long? Id { get; set; }
            public OFeatures Add { get; set; }
            public OFeatures View { get; set; }
            public OFeatures Edit { get; set; }
            public OFeatures Delete { get; set; }
            public OFeatures ViewAll { get; set; }
        }
        public class OUser
        {
            public string? UserName { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }

            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? Gender { get; set; }
            public string? GenderCode { get; set; }

            public DateTime? DateOfBirth { get; set; }

            public string? Address { get; set; }

            public double? AddressLatitude { get; set; }
            public double? AddressLongitude { get; set; }

            public sbyte EmailVerificationStatus { get; set; }
            public sbyte ContactNumberVerificationStatus { get; set; }
            public List<OUserLinkedAccounts> LinkedAccounts { get; set; }
        }
        public class OUserAccount
        {
            public long AccountId { get; set; }
            public string? DisplayName { get; set; }
            public bool IsTucPlusEnabled { get; set; }

            public string? UserKey { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountType { get; set; }
            public string? AccountTypeCode { get; set; }

            public string? AccountCode { get; set; }

            public string? RoleName { get; set; }
            public string? RoleKey { get; set; }


            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public string? ReferralUrl { get; set; }
            public string? ReferralCode { get; set; }

            public sbyte IsAccessPinSet { get; set; }
            public bool IsGroupLoyalty { get; set; }
            public ProgramDetails ProgramDetails { get; set; }
            public bool IsOpenCloseLoyalty { get; set; }
            public ProgramDetails CloseOpenDetails { get; set; }

            public DateTime CreateDate { get; set; }
            public List<UserAccountTypes> UserAccountTypes { get; set; }


        }
        public class ProgramDetails {
            public long ProgramId { get; set; }
            public string? ProgramReferenceKey { get; set; }
            public string? SystemName { get; set; }
        }
        public class OUserLinkedAccounts
        {
            internal long AccountId { get; set; }
            internal long AccountTypeId { get; set; }
            public string? AccountCode { get; set; }
            public string? OwnerName { get; set; }
            public string? DisplayName { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountType { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? IconUrl { get; set; }
            public long Status { get; set; }
            public sbyte IsActiveSession { get; set; }

            public string? CardNumber { get; set; }
            public string? CardTypeName { get; set; }

            public OTransaction.Balance.Response Balance { get; set; }
            public DateTime CreateDate { get; set; }
        }
        public class OUserAccountOwner
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? AccountCode { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? IconUrl { get; set; }
            public bool IsTucPlusEnabled { get; set; }
            internal long AccountTypeId { get; set; }
            internal long UserAccountId { get; set; }
        }
        public class OUserAccountCountry
        {
            public int CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }
            public string? CountryIconUrl { get; set; }
            public string? CountryIso { get; set; }
            public string? CountryIsd { get; set; }

            public string? CurrencyName { get; set; }
            public string? CurrencyNotation { get; set; }
            public string? CurrencySymbol { get; set; }
            public string? CurrencyCode { get; set; }
        }
        public class OUserAccountRole
        {
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public List<OUserAccountRolePermission> RolePermissions { get; set; }
        }
        public class OUserAccountRolePermission
        {
            internal long PermissionId { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public bool IsDefault { get; set; }
            public bool IsAccessPinRequired { get; set; }
            public bool IsPasswordRequired { get; set; }

            public double? MinimumValue { get; set; }
            public double? MaximumValue { get; set; }
            public long? MinimumLimit { get; set; }
            public long? MaximumLimit { get; set; }
        }
        public class PinUpdateRequest
        {
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? AccessPin { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public OAddress AddressComponent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class RolesRights
        {
            public List<OFeatures> StoreManager { get; set; }
            public List<OFeatures> Admin { get; set; }
            public List<OFeatures> Merchant { get; set; }

        }
        public class Stores
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
        }
    }
    internal class OUserAccountAccessDetails
    {
        internal string AccessPin { get; set; }
        internal long AccountTypeId { get; set; }
        internal string DisplayName { get; set; }
        internal long UserAccountId { get; set; }
        internal string UserAccountKey { get; set; }
        internal string AccountCode { get; set; }
        internal string UserKey { get; set; }
        internal string ReferralCode { get; set; }
        internal string ReferralUrl { get; set; }
        internal string AccountTypeCode { get; set; }
        internal string AccountTypeName { get; set; }
        internal long? OwnerId { get; set; }
        internal string IconUrl { get; set; }
        internal string PosterUrl { get; set; }
        internal string Name { get; set; }
        internal string FirstName { get; set; }
        internal string LastName { get; set; }
        internal string EmailAddress { get; set; }
        internal string ContactNumber { get; set; }
        internal DateTime? DateOfBirth { get; set; }
        internal string Gender { get; set; }
        internal string GenderCode { get; set; }
        internal string Address { get; set; }
        internal double? AddressLatitude { get; set; }
        internal double? AddressLongitude { get; set; }
        internal sbyte? EmailVerificationStatus { get; set; }
        internal sbyte? ContactNumberVerificationStatus { get; set; }
        internal long? CountryId { get; set; }
        internal long Status { get; set; }
    }
    internal class OUserOperations
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
    }
    internal class OUserRegConfiguration
    {
        public long? ConfigurationId { get; set; }
        public string? SystemName { get; set; }
        public string? Value { get; set; }
    }
    public class OUserAccountRegister
    {
        internal bool IsAppUser { get; set; }
        #region Internal Options
        internal sbyte AllowAccountKey { get; set; }
        internal long OwnerId { get; set; }
        internal long SubOwnerId { get; set; }
        internal long UserId { get; set; }
        internal long UserAccountId { get; set; }
        internal long UserAccountDeviceId { get; set; }
        internal long UserAccountRoleId { get; set; }
        internal int CountryId { get; set; }
        internal int GenderId { get; set; }
        internal int AccountTypeId { get; set; }
        internal int RegistrationSourceId { get; set; }
        internal long OsId { get; set; }
        internal int StatusId { get; set; }
        internal int AccountOperationTypeId { get; set; }
        internal string CountryKey { get; set; }
        #endregion

        public string? RegistrationSourceCode { get; set; }
        public string? AccountOperationTypeCode { get; set; }

        public string? OwnerKey { get; set; }
        public string? SubOwnerKey { get; set; }
        public string? RoleKey { get; set; }

        public string? AccountType { get; set; }
        public string? SerialNumber { get; set; }

        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? SecondaryPassword { get; set; }
        public string? AccessPin { get; set; }
        public string? DisplayName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Name { get; set; }
        public string? ContactNumber { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }
        public string? Description { get; set; }


        public string? OsVersion { get; set; }
        public string? Brand { get; set; }
        public string? Model { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string? NotificationUrl { get; set; }

        public List<OParameter> Configurations { get; set; }

        public string? WebsiteUrl { get; set; }
        public string? Address { get; set; }
        public double AddressLatitude { get; set; }
        public double AddressLongitude { get; set; }

        public sbyte EmailVerificationStatus { get; set; }
        public sbyte NumberVerificationStatus { get; set; }

        public string? GenderCode { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public string? CountryIso { get; set; }
        public List<string> Owners { get; set; }

        public string? StatusCode { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OCreateUser
    {
        public long UserId { get; set; }
        public string? UserKey { get; set; }


        public string? MobileNumber { get; set; }
        public string? Name { get; set; }
        public string? DisplayName { get; set; }
        public string? EmailAddress { get; set; }

        public string? Address { get; set; }
        public double AddressLatitude { get; set; }
        public double AddressLongitude { get; set; }

        public int GenderId { get; set; }
        public string? GenderCode { get; set; }
        public long OwnerId { get; set; }

        public OCreateUserAccount AppAccount { get; set; }
        public OCreateUserAccount CardAccount { get; set; }
        public string? CardNumber { get; set; }
        public string? CardSerialNumber { get; set; }
        public OUserReference? UserReference { get; set; }
        public string? StatusCode { get; set; }
        public int StatusId { get; set; }
    }
    public class OCreateUserAccount
    {
        public long OwnerId { get; set; }
        public long AccountId { get; set; }
        public string? AccountKey { get; set; }
        public string? AccountCode { get; set; }
        public string? ReferralCode { get; set; }
        public long CardId { get; set; }

    }
    public class OUserAccess
    {
        public class Request
        {
            public string? RegistrationSourceCode { get; set; }
            public string? AccountOperationTypeCode { get; set; }

            public string? RoleKey { get; set; }

            public string? AccountType { get; set; }

            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? SecondaryPassword { get; set; }
            public string? AccessPin { get; set; }
            public string? DisplayName { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }


            public string? SerialNumber { get; set; }
            public string? OsVersion { get; set; }
            public string? Brand { get; set; }
            public string? Model { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string? NotificationUrl { get; set; }


            public string? Address { get; set; }
            public double AddressLatitude { get; set; }
            public double AddressLongitude { get; set; }

            public sbyte EmailVerificationStatus { get; set; }
            public sbyte NumberVerificationStatus { get; set; }

            public string? GenderCode { get; set; }
            public DateTime? DateOfBirth { get; set; }

            public string? CountryIso { get; set; }
            public List<string> Owners { get; set; }

            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }

        }
    }
}
