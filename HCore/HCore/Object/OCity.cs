//==================================================================================
// FileName: OCity.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Object
{
    public class OCity
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Save
        {
            public string? ReferenceKey { get; set; }
            public string? RegionAreaReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? StatusCode { get; set; }
            public string? Description { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DataTypeCode { get; set; }
            public string? DataTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }

            public string? PrimaryValue { get; set; }
            public string? PrimaryValueCustom { get; set; }
            public string? SecondaryValue { get; set; }
            public string? SecondaryValueCustom { get; set; }

            public long? IsSystemLevelConfiguration { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public string? SubTypeCode { get; set; }
            public string? SubTypeName { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }

            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }

            public long? NValue1 { get; set; }
            public long? NValue2 { get; set; }
            public double? NValue3 { get; set; }
            public double? NValue4 { get; set; }
            public int? Sequence { get; set; }
            public string? CoreKey { get; set; }
            public string? CoreName { get; set; }
            public string? PrimaryStorageUrl { get; set; }
            public string? SecondaryStorageUrl { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }
            public string? ModifyByAccountTypeCode { get; set; }

            public string? ParentSystemName { get; set; }
            public string? SubParentSystemName { get; set; }

        }
    }
}
