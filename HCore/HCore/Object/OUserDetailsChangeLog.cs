//==================================================================================
// FileName: OUserDetailsChangeLog.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Object
{
    public class OUserDetailsChangeLog
    {



        public string? ReferenceKey { get; set; }
        public string? UserKey { get; set; }
        public string? UserAccountKey { get; set; }
        public string? UserDeviceKey { get; set; }

        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }


        public string? OldValue { get; set; }
        public string? NewValue { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? ResetDate { get; set; }

        public long? SystemVerificationId { get; set; }
        public string? SystemVerificationToken { get; set; }
        public string? SystemVerificationCode { get; set; }

        public string? IpAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public DateTime? CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }


        public long? StatusId { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }


    public class OUserDetailsChangeLogSave
    {
        internal string ReferenceKey { get; set; }
        internal string Guid { get; set; }
        internal int? Type { get; set; }
        //internal long UserId { get; set; }
        internal long? UserAccountId { get; set; }
        internal long? UserDeviceId { get; set; }

        internal long? SystemVerificationId { get; set; }
        internal string SystemVerificationToken { get; set; }
        internal string SystemVerificationCode { get; set; }

        internal DateTime? RequestDate { get; set; }
        internal DateTime? ResetDate { get; set; }

        internal string OldValue { get; set; }
        internal string NewValue { get; set; }

        internal int Status { get; set; }

        public OUserReference? UserReference { get; set; }

    }
}
