//==================================================================================
// FileName: ONotification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.CoreConstant;

namespace HCore.Object
{
    public class OSystemNotification
    {
        public class Type
        {
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class Gateway
        {
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class Account
        {
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? GatewayCode { get; set; }
            public string? GatewayName { get; set; }

            public string? PrivateKey { get; set; }
            public string? PublicKey { get; set; }
            public string? ServerName { get; set; }
            public int ServerPort { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? FromName { get; set; }
            public string? FromAddress { get; set; }
            public string? ReplyToName { get; set; }
            public string? ReplyToAddress { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Category
        {
            public string? ReferenceKey { get; set; }


            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public long Templates { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Parameter
        {
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }

            public sbyte IsDefault { get; set; }
            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Template
        {
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? NotificationAccountKey { get; set; }

            //public string? GatewayKey { get; set; }
            //public string? GatewayName { get; set; }

            //public string? SecEmailAccountKey { get; set; }
            //public string? SecGatewayKey { get; set; }
            //public string? SecGatewayName { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public sbyte IsDefault { get; set; }

            public string? Subject { get; set; }
            public string? Message { get; set; }
            public string? Parameter { get; set; }

            public string? CcAddress { get; set; }
            public string? BccAddress { get; set; }

            public long TotalNotificationsSent { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Log
        {
            public string? ReferenceKey { get; set; }

            public string? NotificationTypeCode { get; set; }
            public string? NotificationTypeName { get; set; }

            public string? GatewayKey { get; set; }
            public string? GatewayName { get; set; }

            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? TemplateKey { get; set; }
            public string? TemplateName { get; set; }

            public string? AccountKey { get; set; }

            public string? UserKey { get; set; }
            public string? UserDisplayName { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserDeviceKey { get; set; }
            public string? UserDeviceSerialNumber { get; set; }


            public string? ControllerKey { get; set; }
            public string? ControllerName { get; set; }
            public string? ControllerIconUrl { get; set; }

            public int Priority { get; set; }

            public string? ToAddress { get; set; }
            public string? ToName { get; set; }

            public string? FromName { get; set; }
            public string? FromAddress { get; set; }

            public string? ReplyToName { get; set; }
            public string? ReplyToAddress { get; set; }

            public string? CcAddress { get; set; }
            public string? BccAddress { get; set; }


            public string? Subject { get; set; }
            public string? Message { get; set; }

            public DateTime SendDate { get; set; }
            public DateTime? ActualSendDate { get; set; }
            public DateTime? OpenDate { get; set; }

            public long NotificationAttempts { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public string? ReferenceNumber { get; set; }
            public string? Description { get; set; }

            public int Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }

    public class ONotifications
    {
        public string? ReferenceKey { get; set; }

        public string? NotificationTypeCode { get; set; }
        public string? NotificationTypeName { get; set; }

        public string? UserAccountKey { get; set; }
        public string? UserAccountName { get; set; }
        public string? UserAccountIconUrl { get; set; }

        public string? UserDeviceKey { get; set; }
        public string? UserDeviceSerialNumber { get; set; }


        public string? ControllerKey { get; set; }
        public string? ControllerName { get; set; }
        public string? ControllerIconUrl { get; set; }

        public int Priority { get; set; }

        public string? ToAddress { get; set; }
        public string? ToName { get; set; }

        public string? FromName { get; set; }
        public string? FromAddress { get; set; }

        public string? ReplyToName { get; set; }
        public string? ReplyToAddress { get; set; }

        public string? Subject { get; set; }
        public string? Message { get; set; }

        public DateTime SendDate { get; set; }
        public DateTime? ActualSendDate { get; set; }
        public DateTime? OpenDate { get; set; }

        public DateTime CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string? ModifyByKey { get; set; }
        public string? ModifyByDisplayName { get; set; }

        public int Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }

    internal class ONotificationDevicePush
    {
        public string? to { get; set; }
        public object notification { get; set; }
        public object data { get; set; }
    }
    internal class ONotificationDevicePushBody
    {
        public string? Task { get; set; }
        public string? Subject { get; set; }
        public object Message { get; set; }
    }
    public class ONotificationBroadcast
    {
        public DateTime SendDate { get; set; }
        public sbyte Priority { get; set; }
        public CoreHelpers.NotificationTypeE TypeId { get; set; }
        public string? TemplateCode { get; set; }

        public string? ToName { get; set; }
        public string? ToAddress { get; set; }


        public string? UserAccountKey { get; set; }
        public string? UserDeviceKey { get; set; }
        public string? ControllerKey { get; set; }

        public long UserAccountId { get; set; }
        public long UserDeviceId { get; set; }
        public long ControllerId { get; set; }

        public List<ORecepient> Cc { get; set; }
        public List<OParameter> Parameters { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    //public class OParameter
    //{
    //    public string? Name { get; set; }
    //    public string? Value { get; set; }
    //}
    public class ORecepient
    {
        public string? Name { get; set; }
        public string? EmailAddress { get; set; }
    }
    public class MailGunResponse
    {
        public string? id { get; set; }
    }
}
