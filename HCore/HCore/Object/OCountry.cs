//==================================================================================
// FileName: OCountry.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.ComponentModel.DataAnnotations;
using HCore.Helper;

namespace HCore.Object
{
    public class OCountry
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Save
        {
            public string? ReferenceKey { get; set; }
            [Required(ErrorMessage = "Name is Required")]
            public string? Name { get; set; }
           
            [Required]
            public string? Iso { get; set; } //Value
            [Required]
            public string? Isd { get; set; } // SubValue
            public string? CurrencyName { get; set; }
            public string? CurrencyNotation { get; set; }
            public string? StatusCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; } //Value
            public string? SystemName { get; set; } //Value
            public string? Iso { get; set; } //Value
            public string? Isd { get; set; } // SubValue
            public string? CurrencyName { get; set; }
            public string? CurrencyNotation { get; set; }

            public long? Regions { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
}
