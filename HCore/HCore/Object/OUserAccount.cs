//==================================================================================
// FileName: OUserAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Object
{
    public class OUser
    {
        internal long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }

        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? SecondaryPassword { get; set; }

        public string? FirstName { get; set; }
        public string? LastName { get; set; }

        public string? Name { get; set; }

        public string? ContactNumber { get; set; }
        public string? EmailAddress { get; set; }

        public string? Gender { get; set; }
        public string? GenderCode { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string? Address { get; set; }
        public double AddressLatitude { get; set; }
        public double AddressLongitude { get; set; }

        public string? CountryKey { get; set; }
        public string? CountryName { get; set; }


        public string? RegionKey { get; set; }
        public string? RegionName { get; set; }

        public string? RegionAreaKey { get; set; }
        public string? RegionAreaName { get; set; }

        public string? CityKey { get; set; }
        public string? CityName { get; set; }

        public string? CityAreaKey { get; set; }
        public string? CityAreaName { get; set; }

        public sbyte? EmailVerificationStatus { get; set; }
        public DateTime? EmailVerificationStatusDate { get; set; }

        public sbyte? NumberVerificationStatus { get; set; }
        public DateTime? NumberVerificationStatusDate { get; set; }



        public DateTime CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string? ModifyByKey { get; set; }
        public string? ModifyByDisplayName { get; set; }



        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public List<OUserAccountAccess.OUserLinkedAccounts> LinkedAccounts { get; set; }
    }
    public class OUserAccount
    {

        public string? SystemKey { get; set; }
        public string? AppName { get; set; }


        internal string Type { get; set; }

        internal long ReferenceId { get; set; }
        internal long UserId { get; set; }

        internal string AccessPin { get; set; }
        public string? AccountReferenceKey { get; set; }
        public string? UserReferenceKey { get; set; }

        public string? Description { get; set; }

        public string? SubOwnerKey { get; set; }
        public string? SubOwnerDisplayName { get; set; }
        public string? SubOwnerAddress { get; set; }


        internal long AccountTypeId { get; set; }
        public string? AccountTypeCode { get; set; }
        public string? AccountTypeKey { get; set; }
        public string? AccountTypeName { get; set; }

        public string? AccountOperationTypeKey { get; set; }
        public string? AccountOperationTypeCode { get; set; }
        public string? AccountOperationTypeName { get; set; }

        public string? OwnerKey { get; set; }
        public string? OwnerDisplayName { get; set; }
        public string? OwnerIconUrl { get; set; }

        public string? OwnerParentKey { get; set; }
        public string? OwnerParentDisplayName { get; set; }


        public string? DisplayName { get; set; }

        public string? ReferralCode { get; set; }
        public string? ReferralUrl { get; set; }
        public string? AccountCode { get; set; }

        public string? RegistrationSourceKey { get; set; }
        public string? RegistrationSourceCode { get; set; }
        public string? RegistrationSourceName { get; set; }

        public string? CardTypeName { get; set; }
        public string? PrepaidCardTypeName { get; set; }


        public sbyte IsAccessPinSet { get; set; }

        public string? IconUrl { get; set; }
        public OStorage.Save IconContent { get; set; }

        public string? PosterUrl { get; set; }
        public OStorage.Save PosterContent { get; set; }


        public DateTime CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }

        public long? CreatedByAccountTypeId { get; set; }
        public long? CreatedById { get; set; }

        public string? CreatedByOwnerKey { get; set; }
        public string? CreatedByOwnerDisplayName { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string? ModifyByKey { get; set; }
        public string? ModifyByDisplayName { get; set; }


        public DateTime? LastLoginDate { get; set; }
        public DateTime? LastActivityTime { get; set; }


        //public long Status { get; set; }

        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }


        public string? NewUserName { get; set; }
        public string? NewPassword { get; set; }

        public string? AcquirerKey { get; set; }
        public string? AcquirerDisplayName { get; set; }

        public string? MerchantKey { get; set; }
        public string? MerchantDisplayName { get; set; }

        public string? StoreKey { get; set; }
        public string? StoreDisplayName { get; set; }
        public string? StoreAddress { get; set; }

        public OAccountSetupOverview AccountSetupOverview { get; set; }
        public OUser User { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserAccountList
    {
        internal long UserId { get; set; }
        internal long ReferenceId { get; set; }
        public long? OwnerId { get; set; }
        public string? ReferenceKey { get; set; }

        public string? UserKey { get; set; }

        public string? AccountTypeCode { get; set; }
        public string? AccountTypeName { get; set; }

        public string? StoreDisplayName { get; set; }

        public string? AccountOperationTypeCode { get; set; }
        public string? AccountOperationTypeName { get; set; }

        public string? OwnerKey { get; set; }
        public string? OwnerDisplayName { get; set; }
        public string? OwnerAddress { get; set; }
        public string? OwnerAccountTypeCode { get; set; }

        public string? OwnerParentKey { get; set; }
        public string? OwnerParentDisplayName { get; set; }
        public string? OwnerParentAddress { get; set; }

        public string? AccountCode { get; set; }
        public string? ReferralCode { get; set; }

        public string? Description { get; set; }

        public string? UserAccountKey { get; set; }

        public double? Distance { get; set; }

        public string? Name { get; set; }
        public string? DisplayName { get; set; }
        public string? EmailAddress { get; set; }
        public string? UserName { get; set; }
        public string? ContactNumber { get; set; }
        public string? Address { get; set; }

        public string? RegistrationSourceCode { get; set; }
        public string? RegistrationSourceName { get; set; }

        public long? AndroidAppRequest { get; set; }
        public long? IosAppRequest { get; set; }

        public string? AppKey { get; set; }
        public string? AppName { get; set; }

        public string? CardKey { get; set; }

        public string? CountryKey { get; set; }
        public string? CountryName { get; set; }

        public double? RewardPercentage { get; set; }
        public double? AddressLatitude { get; set; }
        public double? AddressLongitude { get; set; }
        public double? ReferralEarning { get; set; }

        public long? CountValue { get; set; }
        public double? AverageValue { get; set; }



        public string? IconUrl { get; set; }
        public string? PosterUrl { get; set; }
        public List<string> PosterUrls { get; set; }

        public string? CreatedByKey { get; set; }
        public string? CreatedByOwnerKey { get; set; }
        public string? CreatedByName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? CreateDateByController { get; set; }
        public string? CreatedByControllerKey { get; set; }
        public string? CreatedByControllerName { get; set; }
        public string? CreatedByAccountType { get; set; }

        public DateTime? ModifyDateByController { get; set; }

        public long? Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public double? AmountCredit { get; set; }
        public double? AmountDebit { get; set; }
        public double? Balance { get; set; }
        //public DateTime? LastVisit { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? TotalVisits { get; set; }
        public double? TotalPurchase { get; set; }
        public double? TotalPointReward { get; set; }
        public double? TotalPointRedeem { get; set; }

        public double? PendingSettlement { get; set; }


        public double? Credit { get; set; }
        public double? Debit { get; set; }


        public long? TotalInvoices { get; set; }
        public long? PaidInvoices { get; set; }
        public long? UnpaidInvoices { get; set; }


        public string? AcquirerKey { get; set; }
        public string? AcquirerName { get; set; }

        public string? Providerkey { get; set; }
        public string? ProviderName { get; set; }

        public List<OParameter> Categories { get; set; }

    }
    public class OStoreList
    {
        public string? ReferenceKey { get; set; }

        public string? StoreKey { get; set; }
        public string? StoreDisplayName { get; set; }
        public string? Address { get; set; }
        public double? AddressLatitude { get; set; }
        public double? AddressLongitude { get; set; }


        public string? MerchantKey { get; set; }
        public string? MerchantDisplayName { get; set; }
        public string? IconUrl { get; set; }
        public string? PosterUrl { get; set; }

        public int? TotalVisits { get; set; }
        public double? TotalPurchase { get; set; }
        public double? TotalPointReward { get; set; }
        public double? TotalPointRedeem { get; set; }


        public double? AmountCredit { get; set; }
        public double? AmountDebit { get; set; }
        public double? Balance { get; set; }
        public DateTime? LastVisit { get; set; }
        public OParameter Categories { get; set; }

        public string? Tag { get; set; }


    }
    public class OUserAccountListLite
    {
        public string? ReferenceKey { get; set; }

        //public string? AccountTypeKey { get; set; }
        public string? OwnerDisplayName { get; set; }
        public string? AccountTypeCode { get; set; }
        public string? AccountOperationTypeCode { get; set; }
        public string? OwnerKey { get; set; }
        public string? OwnerParentKey { get; set; }
        public string? AccountCode { get; set; }
        public string? Name { get; set; }
        public string? DisplayName { get; set; }
        public string? EmailAddress { get; set; }
        public string? UserName { get; set; }
        public string? ContactNumber { get; set; }

        public string? Tag { get; set; }

        public string? CountryKey { get; set; }



        public long Status { get; set; }
        public string? StatusCode { get; set; }

        public string? CreatedByDisplayName { get; set; }
        public string? CreatedByKey { get; set; }

        public DateTime CreateDate { get; set; }
    }
    public class OUserUpdateAccountStatus
    {
        public string? UserAccountKey { get; set; }
        public string? StatusCode { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserUserNameUpdate
    {
        public string? UserAccountReferenceKey { get; set; }
        public string? UserReferenceKey { get; set; }
        public string? Username { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OAccountSetupOverview
    {
        public double PointsAvailable { get; set; }

        public long Stores { get; set; }
        public double StorePointAvailable { get; set; }

        public long Cashiers { get; set; }
        public double CashierPointAvailable { get; set; }

        public long? ApprovedCards { get; set; }
        public long? AllocatedDeviceToMerchant { get; set; }

        public long? AllocatedDeviceToStore { get; set; }

        public long BankAccounts { get; set; }

        public long OnlineStores { get; set; }
        public double OnlineStorePointAvailable { get; set; }

        public long OnlineApps { get; set; }
        public double OnlineAppPointAvailable { get; set; }

    }
    public class OUserPasswordUpdate
    {
        public string? UserReferenceKey { get; set; }
        public string? UserAccountReferenceKey { get; set; }
        public string? OldPassword { get; set; }
        public string? NewPassword { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserAccessPinUpdate
    {
        public string? UserAccountReferenceKey { get; set; }
        public string? OldAccessPin { get; set; }
        public string? NewAccessPin { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserEmailAddressUpdate
    {
        public string? UserReferenceKey { get; set; }
        public string? UserAccountReferenceKey { get; set; }
        public string? EmailAddress { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserContactNumberUpdate
    {
        public string? UserReferenceKey { get; set; }
        public string? UserAccountReferenceKey { get; set; }
        public string? ContactNumber { get; set; }
        public string? AccessCode { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserVerificationResponse
    {
        public string? ReferenceKey { get; set; }

        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }

        public string? CodeStart { get; set; }


        public string? RequestToken { get; set; }
        public string? AccessCode { get; set; }

        public OUserReference? UserReference { get; set; }

    }
    public class ForgotPassword
    {
        public class Request
        {
            public string? UserName { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public string? ReferenceKey { get; set; }

            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? CodeStart { get; set; }



            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Verify
        {
            public string? ReferenceKey { get; set; }
            public string? Password { get; set; }
            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
    public class ForgotUserName
    {
        public class Request
        {
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public string? ReferenceKey { get; set; }

            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? CodeStart { get; set; }

            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Verify
        {
            public string? ReferenceKey { get; set; }
            public string? UserName { get; set; }
            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
    public class OForgotAccessPin
    {
        public class Request
        {
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public string? ReferenceKey { get; set; }

            public string? MobileNumber { get; set; }

            public string? CodeStart { get; set; }

            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Verify
        {
            public string? ReferenceKey { get; set; }
            public string? AccessPin { get; set; }
            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
}
