//==================================================================================
// FileName: OUserOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Object
{
    public class OUserOverview
    {
        internal class Account
        {
            internal long? AccountOperationTypeId { get; set; }
            internal long? OwnerParentId { get; set; }
            internal long? OwnerId { get; set; }
            internal long ReferenceId { get; set; }
            internal long? CreatedById { get; set; }
            internal long AccountTypeId { get; set; }
            internal DateTime CreateDate { get; set; }
        }
        public class Request
        {
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public string? UserAccountKey { get; set; }
            public string? SubUserAccountKey { get; set; }
            public string? Type { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public ORewardBalance RewardBalance { get; set; }
            public OVistors VisitorsOverview { get; set; }
            public ORewards Rewards { get; set; }
            public OPayments Payments { get; set; }
            public OSettlement Settlements { get; set; }

            public List<OUserTransactionType> RewardOverview { get; set; }
            public List<OUserTransactionType> RedeemOverview { get; set; }
            public List<OUserTransactionType> PaymentsOverview { get; set; }


            public OCardsOverview CardsOverview { get; set; }


            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
    }

    public class OSettlement
    {
        public double? Completed { get; set; }
        public double? Pending { get; set; }
    }

    public class OUserTransactionType
    {
        internal long Id { get; set; }
        public string? Name { get; set; }
        public double? Points { get; set; }
        public double? Comission { get; set; }
        public double? Charge { get; set; }
    }
    public class ORewards
    {
        public double TotalPurchase { get; set; }
        public double TotalReward { get; set; }
        public double TotalRedeem { get; set; }
        public double TotalComission { get; set; }
        public double TotalCharge { get; set; }
    }

    public class OPayments
    {
        public long Transactions { get; set; }
        public double TotalPurchase { get; set; }
        public double TotalAmount { get; set; }
        public double TotalComission { get; set; }
        public double TotalCharge { get; set; }
    }

   

    public class ORewardBalance
    {
        public double Credit { get; set; }
        public double Debit { get; set; }
        public double Balance { get; set; }
        public double? Comission { get; set; }
        public double? Charge { get; set; }
    }

    public class OVistors
    {
        public double? Total { get; set; }
        public double? TotalVisits { get; set; }
        public double? SingleVisit { get; set; }
        public double? MultipleVisit { get; set; }
    }

    public class OCardsOverview
    {
        public double Total { get; set; }
        public double Assigned { get; set; }
        public double NotAssigned { get; set; }
    }
    
}
