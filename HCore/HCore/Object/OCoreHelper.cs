//==================================================================================
// FileName: OCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.CoreConstant;

namespace HCore.Object
{
    public class OParameter
    {
        public string? ReferenceKey { get; set; }
        public string? Name { get; set; }
        public object Value { get; set; }
    }

    public class OCoreHelper
    {
        public class Manage
        {
            public string? ReferenceKey { get; set; }
            public string? ParentCode { get; set; }
            public string? SubParentCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? TypeName { get; set; }
            public string? Description { get; set; }
            public int Sequence { get; set; }
            public OStorage.Save IconContent { get; set; }
            public OStorage.Save PosterContent { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {

            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }

            public string? Name { get; set; }
            public string? Value { get; set; }
            public string? TypeName { get; set; }
            public string? Description { get; set; }
            public int? Sequence { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }


            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreCommon
    {
        public class Manage
        {
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? HelperCode { get; set; }

            public string? UserAccountKey { get; set; }
            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public bool DisableChild { get; set; }

            public OStorage.Save IconContent { get; set; }

            public OStorage.Save PosterContent { get; set; }

            public string? StatusCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }

            public string? Name { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public string? IconUrl { get; set; }
            public OStorage.Save IconContent { get; set; }

            public string? PosterUrl { get; set; }
            public OStorage.Save PosterContent { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreParameter
    {
        public class Manage
        {
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? TypeCode { get; set; }

            public string? Name { get; set; }

            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public string? HelperCode { get; set; }

            public string? CommonKey { get; set; }
            public string? SubCommonKey { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public OStorage.Save IconContent { get; set; }
            public OStorage.Save PosterContent { get; set; }

            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }


            public string? Name { get; set; }

            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? CommonKey { get; set; }
            public string? CommonCode { get; set; }
            public string? CommonName { get; set; }

            public string? SubCommonKey { get; set; }
            public string? SubCommonCode { get; set; }
            public string? SubCommonName { get; set; }


            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public string? IconUrl { get; set; }
            public OStorage.Save IconContent { get; set; }
            public string? PosterUrl { get; set; }
            public OStorage.Save PosterContent { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreUsage
    {
        public class Manage
        {
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? AppOwnerKey { get; set; }
            public string? AppOwnerName { get; set; }

            public string? AppKey { get; set; }
            public string? AppName { get; set; }

            public string? AppVersionKey { get; set; }
            public string? AppVersionName { get; set; }

            public string? FeatureKey { get; set; }
            public string? FeatureName { get; set; }

            public string? ApiKey { get; set; }
            public string? ApiName { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public long? SessionId { get; set; }
            public string? SessionKey { get; set; }

            public string? IpAddress { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? Request { get; set; }
            public string? Response { get; set; }

            public long? StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }
            public DateTime RequestTime { get; set; }
            public DateTime? ResponseTime { get; set; }
            public double? ProcessingTime { get; set; }
        }
    }
    public class OSystemVerification
    {
        public class Request
        {
            public int Type { get; set; }
            public string? RequestToken { get; set; }

            public string? Reference { get; set; }

            public string? CountryIsd { get; set; }

            public string? MobileNumber { get; set; }
            public string? MobileMessage { get; set; }

            public string? EmailAddress { get; set; }
            public string? EmailMessage { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            internal long ReferenceId { get; set; }
            public long Type { get; set; }
            public string? Reference { get; set; }

            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? CodeStart { get; set; }

            public string? RequestToken { get; set; }
        }
        public class RequestVerify
        {
            public string? RequestToken { get; set; }

            public string? AccessKey { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
    public class OSystemVerificationList
    {
        public string? ReferenceKey { get; set; }
        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }
        public string? CountryIsd { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }
        public string? AccessCodeStart { get; set; }
        public DateTime? ExpiaryDate { get; set; }
        public DateTime? VerifyDate { get; set; }
        public DateTime CreateDate { get; set; }

        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }
    }
    public class OSystemVerificationDetails
    {
        public string? ReferenceKey { get; set; }
        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }
        public string? CountryIsd { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailAddress { get; set; }

        public string? AccessKey { get; set; }
        public string? AccessCode { get; set; }
        public string? AccessCodeStart { get; set; }


        public string? EmailMessage { get; set; }
        public string? MobileMessage { get; set; }


        public DateTime? ExpiaryDate { get; set; }
        public DateTime? VerifyDate { get; set; }


        public string? RequestIpAddress { get; set; }
        public double? RequestLatitude { get; set; }
        public double? RequestLongitude { get; set; }
        public string? RequestLocation { get; set; }
        public int? VerifyAttemptCount { get; set; }
        public string? VerifyIpAddress { get; set; }
        public double? VerifyLatitude { get; set; }
        public double? VerifyLongitude { get; set; }
        public string? VerifyAddress { get; set; }
        public string? ExternalReferenceKey { get; set; }



        public DateTime CreateDate { get; set; }

        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }
    public class OCheckMobi
    {
        public class OtpRequest
        {
            public string? number { get; set; }
            public string? type { get; set; }
        }

        public class OtpResponse
        {
            public string? responsejson { get; set; }
            public string? code { get; set; }
            public string? error { get; set; }
            public string? id { get; set; }
            public string? type { get; set; }
            public string? dialing_number { get; set; }
            public validation_info validation_info { get; set; }
        }

        public class validation_info
        {
            public string? country_code { get; set; }
            public string? country_iso_code { get; set; }
            public string? carrier { get; set; }
            public string? is_mobile { get; set; }
            public string? e164_format { get; set; }
            public string? formatting { get; set; }
        }


        public class VerifyRequest
        {
            public string? id { get; set; }
            public string? pin { get; set; }
        }
        public class VerifyResponse
        {
            public string? responsejson { get; set; }
            public string? code { get; set; }
            public string? error { get; set; }
            public string? number { get; set; }
            public bool validated { get; set; }
            public string? validation_date { get; set; }
            public string? charged_amount { get; set; }
        }
    }
    // public class OResponse
    // {
    //     public string? Task { get; set; }
    //     public string? Status { get; set; }
    //     public string? Message { get; set; }
    //     public object Result { get; set; }
    //     public string? ResponseCode { get; set; }
    //     public string? Action { get; set; }
    // }
    // public class OUserReference
    // {
    //     public string? Data { get; set; }
    //     internal long StatusId { get; set; }
    //     public string? Task { get; set; }
    //     #region User
    //     public long UserId { get; set; }
    //     public string? UserKey { get; set; }
    //     #endregion
    //     #region Account Type
    //     public long AccountTypeId { get; set; }
    //     public string? AccountTypeKey { get; set; }
    //     public string? AccountTypeName { get; set; }
    //     public string? AccountTypeSystemName { get; set; }
    //     #endregion
    //     #region Account
    //     public long AccountId { get; set; }
    //     public string? AccountKey { get; set; }
    //     public string? AccountCode { get; set; }
    //     #endregion
    //     #region Account Session
    //     public long AccountSessionId { get; set; }
    //     #endregion
    //     #region User Details
    //     public string? UserName { get; set; }
    //     public string? DisplayName { get; set; }
    //     public string? Name { get; set; }
    //     public string? EmailAddress { get; set; }
    //     public int EmailVerificationStatus { get; set; }
    //     public string? ContactNumber { get; set; }
    //     public int ContactNumberVerificationStatus { get; set; }
    //     #endregion
    //     #region Account Owner
    //     public long? AccountOwnerId { get; set; }
    //     public string? AccountOwnerKey { get; set; }
    //     public string? AccountOwnerName { get; set; }
    //     public string? AccountOwnerDisplayName { get; set; }
    //     #endregion
    //     #region User Account Device
    //     public long DeviceId { get; set; }
    //     public string? DeviceKey { get; set; }
    //     public string? DeviceSerialNumber { get; set; }
    //     #endregion
    //     #region  Country
    //     public long? CountryId { get; set; }
    //     public string? CountryKey { get; set; }
    //     public string? CountryName { get; set; }
    //     public string? CountryIsd { get; set; }
    //     public string? CountryIso { get; set; }
    //     #endregion
    //     #region Request Location
    //     public double RequestLatitude { get; set; }
    //     public double RequestLongitude { get; set; }
    //     public string? RequestIpAddress { get; set; }
    //     #endregion
    //     //#region Api Details
    //     //internal sbyte IsDefaultPermission { get; set; }
    //     //internal sbyte IsPasswordRequired { get; set; }
    //     //internal sbyte IsAccessPinRequired { get; set; }
    //     //public long SystemPermissionId { get; set; }
    //     //public long? AccountTypePermissionId { get; set; }
    //     //#endregion
    //     #region Api | App  | App Versions | Os
    //     public string? RequestKey { get; set; }
    //     public long RequestId { get; set; }
    //     public long AppId { get; set; }
    //     //public string? AppKey { get; set; }
    //     //public string? AppName { get; set; }
    //     public long AppVersionId { get; set; }
    //     public long OsId { get; set; }
    //     //public string? OsKey { get; set; }
    //     //public string? OsName { get; set; }
    //     #endregion
    //     #region Database Context
    //     public string? DbContext { get; set; }
    //     #endregion
    // }

    // public class OList
    // {
    //     public class Request
    //     {
    //         public int Offset { get; set; }
    //         public int Limit { get; set; }
    //         public string? SearchParameter { get; set; }
    //         public string? SearchCondition { get; set; }
    //         public string? SortExpression { get; set; }
    //         public string? ReferenceKey { get; set; }
    //         public string? SubReferenceKey { get; set; }
    //         public long ReferenceId { get; set; }
    //         public long SubReferenceId { get; set; }
    //         public string? Type { get; set; }
    //         public string? CategoryIds { get; set; }
    //         public double Latitude { get; set; }
    //         public double Longitude { get; set; }
    //         public int ListType { get; set; }
    //         public long Status { get; set; }
    //         public int Radius { get; set; }
    //         public DateTime? StartDate { get; set; }
    //         public DateTime? EndDate { get; set; }
    //         public OUserReference? UserReference { get; set; }
    //     }
    //     public class Response
    //     {
    //         public int Offset { get; set; }
    //         public int? Limit { get; set; }
    //         public int TotalRecords { get; set; }
    //         public object Data { get; set; }
    //         public object SubData { get; set; }
    //         public double? Amount { get; set; }
    //         public double? TotalAmount { get; set; }
    //         public double? TotalCharge { get; set; }
    //         public double? TotalComission { get; set; }
    //         public double? Total { get; set; }
    //         public double? PurchaseAmount { get; set; }
    //         public double? UserAmount { get; set; }
    //         public double? ThankUAmount { get; set; }
    //         public double? MerchantAmount { get; set; }
    //     }
    // }

    public class OAuthApi
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public long ApiCategoryId { get; set; }
        public string? SystemName { get; set; }
        public long SystemPermissionId { get; set; }
    }
    public class OAuthAppVersion
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public int OsId { get; set; }
        public string? SystemName { get; set; }
        public string? AppVersion { get; set; }
        public string? AccessKey { get; set; }
        public sbyte AllowAccess { get; set; }
        public long Status { get; set; }
    }
    public class OAuthApp
    {
        public long AppVersionId { get; set; }
        public string? AppVersionAccessKey { get; set; }
        public string? AppVersionName { get; set; }


        public long AppId { get; set; }
        public string? AppKey { get; set; }
        public string? AppName { get; set; }

        public int OsId { get; set; }
        public string? OsKey { get; set; }
        public string? OsName { get; set; }

        public sbyte AllowAccess { get; set; }
        public long Status { get; set; }
    }
    public class OChart
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? SubReferenceKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? SubUserAccountKey { get; set; }
            public string? Type { get; set; }


            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public List<RequestDateRange> DateRange { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class RequestDateRange
        {
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string? Label { get; set; }
            public List<RequestData> Data { get; set; }
        }



        public class RequestData
        {
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
        }
    }
    public class OCoreLog
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Comment { get; set; }
        public string? Reference { get; set; }
        public string? Data { get; set; }

        public DateTime? CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string? ModifyByKey { get; set; }
        public string? ModifyByDisplayName { get; set; }

        public int StatusId { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }
    internal class OHCoreDataStore
    {
        internal class CoreUsage
        {
            public string? RequestKey { get; set; }
            public long? AppVersionId { get; set; }
            public long? UserAccountId { get; set; }
            public string? IpAddress { get; set; }
            public string? Request { get; set; }
            public string? Response { get; set; }
            public DateTime RequestTime { get; set; }
            public DateTime ResponseTime { get; set; }
            public long ProcessingTime { get; set; }
            public int StatusId { get; set; }
            public bool IsSynced { get; set; }
        }
        internal class SystemHelper
        {
            public long ReferenceId { get; set; }
            public long? ParentId { get; set; }
            public string? ParentCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? TypeName { get; set; }
            public string? Value { get; set; }
            public int? Sequence { get; set; }
        }
        internal class Resource
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
        }


        internal class CResource
        {
            public string? Name { get; set; }
            public string? Value { get; set; }
            public string? Description { get; set; }
        }

    }
    // public class Globals
    // {
    //     public string? Name { get; set; }
    //     public string? WebsiteUrl { get; set; }
    //     public string? DomainName { get; set; }
    //     public string? CopyRightsText { get; set; }
    //     public string? Default_Icon { get; set; }
    //     public string? Default_Poster { get; set; }
    //     public long StorageSourceId { get; set; }
    //     public string? StorageSourceCode { get; set; }
    //     public string? StorageUrl { get; set; }
    //     public string? StorageLocation { get; set; }
    //     public string? StorageAccessKey { get; set; }
    //     public string? StoragePrivateKey { get; set; }
    // }
    public class OStorage
    {
        public class Save
        {
            public string? UserKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? TypeCode { get; set; }
            public string? HelperCode { get; set; }
            public string? Name { get; set; }
            public string? Extension { get; set; }
            public string? Content { get; set; }
            public string? Reference { get; set; }
            public string? Tags { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Delete
        {
            public string? ReferenceKey { get; set; }
            public long ReferenceId { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Url { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? Name { get; set; }
            public string? Url { get; set; }
            public string? Extension { get; set; }
            public string? Tags { get; set; }
            public double? Size { get; set; }
            public DateTime CreateDate { get; set; }
            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public string? ReferenceKey { get; set; }
            public string? UserKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }
            public string? Name { get; set; }
            public string? Url { get; set; }
            public string? Extension { get; set; }
            public string? Tags { get; set; }
            public double? Size { get; set; }
            public DateTime CreateDate { get; set; }
            public long Status { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OConfigurationValue
    {
        public class Request
        {

            public string? ConfigurationCode { get; set; }
            public string? CountryKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Response
        {
            internal long ReferenceId { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCode { get; set; }
            public string? Value { get; set; }
            public sbyte IsDefaultValue { get; set; }
        }
    }


}
