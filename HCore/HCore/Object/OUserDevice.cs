//==================================================================================
// FileName: OUserDevice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
namespace HCore.Object
{
    public class OUserDevice
    {
        public string? ReferenceKey { get; set; }


        public string? UserAccountKey { get; set; }
        public string? UserAccountDisplayName { get; set; }

        public string? SerialNumber { get; set; }

        public string? OsKey { get; set; }
        public string? OsName { get; set; }

        public string? OsVersionKey { get; set; }
        public string? OsVersionName { get; set; }


        public string? AppKey { get; set; }
        public string? AppName { get; set; }


        public string? AppVersionKey { get; set; }
        public string? AppVersionName { get; set; }

        public string? BrandKey { get; set; }
        public string? BrandName { get; set; }

        public string? ModelKey { get; set; }
        public string? ModelName { get; set; }


        public string? ResolutionKey { get; set; }
        public string? ResolutionName { get; set; }


        public string? NetworkOperatorKey { get; set; }
        public string? NetworkOperatorName { get; set; }


        public string? NotificationUrl { get; set; }


        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }




        public int Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }

    }
    public class OUserDeviceSave
    {
        public long UserId { get; set; }
        public long UserAccountId { get; set; }

        public string? SerialNumber { get; set; }
        public string? OsVersion { get; set; }

        public string? Brand { get; set; }
        public string? Model { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public string? NotificationUrl { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OUserDeviceAppVersion
    {
        public string? ReferenceKey { get; set; }
        public string? DeviceKey { get; set; }
        public string? AppVersionKey { get; set; }
        public string? AppVersionName { get; set; }
        public string? AppKey { get; set; }
        public string? AppName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long Status { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }
    }
    public class OUserDeviceLLocation
    {
        public string? ReferenceKey { get; set; }
        public string? DeviceKey { get; set; }
        public string? Location { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? CreateDate { get; set; }
        public long Status { get; set; }
    }
}
