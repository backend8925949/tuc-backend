//==================================================================================
// FileName: OAppConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using System.Collections.Generic;

namespace HCore.Object
{
    public class OAppConfiguration
    {
        public class Request
        {
            public string? SubTask { get; set; }
            public int AccountType { get; set; }
            #region Helper
            public string? CountryCode { get; set; }
            public string? OperatorName { get; set; }
            public int MCCCode { get; set; }
            public int MNCCode { get; set; }
            #endregion
            #region Device
            public string? DeviceId { get; set; }
            public string? OsName { get; set; }
            public string? OsVersion { get; set; }
            public string? ManufacturerName { get; set; }
            public string? ModelName { get; set; }
            public string? HardwareName { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            #endregion
            #region Location
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            #endregion
            #region App Version
            public string? AppVersion { get; set; }
            #endregion

            internal long PublisherId { get; set; }
            internal long RegionId { get; set; }
            internal long CountryId { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {

            public string? SystemKey { get; set; }
            public string? AccessKey { get; set; }
            public string? CountryIconUrl { get; set; }
            public string? CountryName { get; set; }
            public string? CountryIsd { get; set; }
            public DateTime? AccessValidity { get; set; }
            public object Slider { get; set; }
            public List<OCBanks> Payments { get; set; }


            public List<OAppConfigurationListParameters> RegistrationSources { get; set; }
            public List<OAppConfigurationListParameters> Genders { get; set; }
            public List<OAppConfigurationListParameters> Countries { get; set; }
        }

    }


    public class ICBankResponse
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public List<ICBankList> data { get; set; }
    }
    public class ICBankList
    {
        public string? name { get; set; }
        public string? code { get; set; }
    }
    public class OCBanks
    {
        public string? Name { get; set; }
        public bool IsActive { get; set; }
        public List<OCBank> Banks { get; set; }
    }

    public class OCBank
    {
        public string? Name { get; set; }
        public string? Code { get; set; }
    }

    public class OAppConfigurationListParameters
    {
        public string? SystemName { get; set; }
        public string? Name { get; set; }
        public string? SystemCode { get; set; }
    }




}
