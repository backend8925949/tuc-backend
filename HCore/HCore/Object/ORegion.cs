//==================================================================================
// FileName: ORegion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Object
{
    public class ORegion
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Save
        {
            public string? ReferenceKey { get; set; }
            public string? CountryReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? StatusCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public long? Cities { get; set; }
            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
