//==================================================================================
// FileName: OUserAccountProcess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Object
{

    public class OUserAccountProcesProfile
    {
        public string? AccountTypeKey { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }

        public string? AccessPin { get; set; }

        public string? EmailAddress { get; set; }
        public string? MobileNumber { get; set; }

        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? FullName { get; set; }

        public string? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }



    }
}
