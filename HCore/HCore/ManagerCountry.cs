//==================================================================================
// FileName: ManagerCountry.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManagerCountry
    {
        //FrameworkCountry _FrameworkCountry;
        //public OResponse SaveCountry(OCountry.Save _Request)
        //{
        //    _FrameworkCountry = new FrameworkCountry();
        //    return _FrameworkCountry.SaveCountry(_Request);
        //}

        //public OResponse UpdateCountry(OCountry.Save _Request)
        //{
        //    _FrameworkCountry = new FrameworkCountry();
        //    return _FrameworkCountry.UpdateCountry(_Request);
        //}

        //public OResponse DeleteCountry(OCountry.Save _Request)
        //{
        //    _FrameworkCountry = new FrameworkCountry();
        //    return _FrameworkCountry.DeleteCountry(_Request);
        //}

        //public OResponse GetCountryDetails(OCountry.Request _Request)
        //{
        //    _FrameworkCountry = new FrameworkCountry();
        //    return _FrameworkCountry.GetCountryDetails(_Request);
        //}

        //public OResponse GetCountries(OList.Request _Request)
        //{
        //    _FrameworkCountry = new FrameworkCountry();
        //    return _FrameworkCountry.GetCountries(_Request);
        //}
    }
}
