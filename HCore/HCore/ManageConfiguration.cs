//==================================================================================
// FileName: ManageConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageConfiguration
    {
        #region Declare

        FrameworkConfiguration _FrameworkConfiguration;
        #endregion
        /// <summary>
        /// Description: Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppConfiguration(OAppConfiguration.Request _Request)
        {
            #region Send Response
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetAppConfiguration(_Request);
            #endregion
        }
        #region Manage Operations
        /// <summary>
        /// Description: Gets the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationValue(OConfigurationValue.Request _Request)
        {
            #region Send Response
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfigurationValue(_Request);
            #endregion
        }
        //public string? GetConfigurationValue(string ConfigurationCode, string? CountryKey, OUserReference UserReference)
        //{
        //    #region Send Response
        //    _FrameworkConfiguration = new FrameworkConfiguration();
        //    return _FrameworkConfiguration.GetConfigurationValue(ConfigurationCode, CountryKey, UserReference);
        //    #endregion
        //}
        //public string? GetConfigurationValueByUser(string ConfigurationCode, string? UserAccountKey, OUserReference UserReference)
        //{
        //    #region Send Response
        //    _FrameworkConfiguration = new FrameworkConfiguration();
        //    return _FrameworkConfiguration.GetConfigurationValueByUser(ConfigurationCode, UserAccountKey, UserReference);
        //    #endregion
        //}
        //public string? GetConfigurationValueByUser(string ConfigurationCode, long UserAccountId, OUserReference UserReference)
        //{
        //    #region Send Response
        //    _FrameworkConfiguration = new FrameworkConfiguration();
        //    return _FrameworkConfiguration.GetConfigurationValueByUser(ConfigurationCode, UserAccountId, UserReference);
        //    #endregion
        //}
        #endregion
    }
}
