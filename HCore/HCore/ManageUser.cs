//==================================================================================
// FileName: ManageUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
namespace HCore
{
    public class ManageUser
    {
        FrameworkUser _FrameworkUser;
        /// <summary>
        /// Description: Saves the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.SaveUserParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.UpdateUserParameter(_Request);
        }
        /// <summary>
        /// Description: Deletes the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.DeleteUserParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.GetUserParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the user parameter list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserParameter(OList.Request _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.GetUserParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserActivity(OList.Request _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.GetUserActivity(_Request);
        }

        /// <summary>
        /// Description: Expires the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ExpireUserSession(OUserSession _Request)
        {
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.ExpireUserSession(_Request);
        }

        #region Get
        /// <summary>
        /// Description: Gets the user session list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserSessions(OList.Request _Request)
        {
            #region Send Response
            _FrameworkUser = new FrameworkUser();
            return _FrameworkUser.GetUserSessions(_Request);
            #endregion
        }
        #endregion
    }
}
