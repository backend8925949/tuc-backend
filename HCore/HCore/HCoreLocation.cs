//==================================================================================
// FileName: HCoreLocation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace HCore
{
    public class HCoreLocation
    {
        internal class address_result
        {
            public List<address_components_results> results { get; set; }
            public string? status { get; set; }
        }
        internal class address_components_results
        {
            public List<address_components> address_components { get; set; }
            public string? formatted_address { get; set; }
            public Geometry Geometry { get; set; }
        }
        public class Geometry
        {
            public location location { get; set; }
        }
        public class location
        {
            public string? lat { get; set; }
            public string? lng { get; set; }
        }
        internal class address_components
        {
            public string? long_name { get; set; }
            public string? short_name { get; set; }
            public List<string> types { get; set; }
        }
        public class OGeoLocation
        {
            public string? Status { get; set; }
            public string? Message { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public string? Country { get; set; }
            public string? CountryCode { get; set; }
            public double? CountryLatitude { get; set; }
            public double? CountryLongitude { get; set; }
            public string? State { get; set; }
            public double? StateLatitude { get; set; }
            public double? StateLongitude { get; set; }
            public string? District { get; set; }
            public double? DistrictLatitude { get; set; }
            public double? DistrictLongitude { get; set; }
            public string? City { get; set; }
            public double? CityLatitude { get; set; }
            public double? CityLongitude { get; set; }
            public string? CityArea { get; set; }
            public double? CityAreaLatitude { get; set; }
            public double? CityAreaLongitude { get; set; }
            public string? CitySubArea { get; set; }
            public string? CityStreetName { get; set; }
            public string? CityStreetNumber { get; set; }
            public string? PostCode { get; set; }
        }
        OGeoLocation _OGeoLocation;
        public OGeoLocation GetGeoLocation(string Latitude, string? Longitude)
        {
            _OGeoLocation = new OGeoLocation();
            try
            {
                if (!string.IsNullOrEmpty(Latitude))
                {
                    if (!string.IsNullOrEmpty(Longitude))
                    {
                        #region Call Geolocation
                        string DataResponse = HttpWebRequestProcess(Latitude, Longitude);
                        if (!string.IsNullOrEmpty(DataResponse))
                        {
                            address_result _InternalGeoLocation = JsonConvert.DeserializeObject<address_result>(DataResponse);
                            if (!string.IsNullOrEmpty(_InternalGeoLocation.status))
                            {
                                if (_InternalGeoLocation.status == "OK")
                                {
                                    #region Set Location And Send Response
                                    if (_InternalGeoLocation.results.Count > 0)
                                    {
                                        _OGeoLocation.Status = "OK";
                                        _OGeoLocation.Message = "Location loaded successfully";
                                        _OGeoLocation.Latitude = Convert.ToDouble(Latitude);
                                        _OGeoLocation.Longitude = Convert.ToDouble(Longitude);
                                        string Address = null;
                                        string Country = null;
                                        string CountryCode = null;
                                        double? CountryLatitude = null;
                                        double? CountryLongitude = null;
                                        string State = null;
                                        string StateCode = null;
                                        double? StateLatitude = null;
                                        double? StateLongitude = null;
                                        string District = null;
                                        double? DistrictLatitude = null;
                                        double? DistrictLongitude = null;
                                        string City = null;
                                        double? CityLatitude = null;
                                        double? CityLongitude = null;
                                        string CityArea = null;
                                        double? CityAreaLatitude = null;
                                        double? CityAreaLongitude = null;
                                        string CitySubArea = null;
                                        string CityStreetName = null;
                                        string CityStreetNumber = null;
                                        string PostCode = null;
                                        #region Get Values
                                        address_components_results AddressDetails = _InternalGeoLocation.results[0];
                                        Address = AddressDetails.formatted_address;
                                        foreach (var AddressComponent in AddressDetails.address_components)
                                        {
                                            string long_name = AddressComponent.long_name;
                                            string short_name = AddressComponent.short_name;
                                            foreach (var AddressComponentType in AddressComponent.types)
                                            {
                                                if (AddressComponentType == "country")
                                                {
                                                    Country = long_name;
                                                    CountryCode = short_name;
                                                    foreach (var LocationResult in _InternalGeoLocation.results)
                                                    {
                                                        if (LocationResult.address_components[0].long_name == Country)
                                                        {
                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lat))
                                                            {
                                                                CountryLatitude = Convert.ToDouble(LocationResult.Geometry.location.lat);
                                                            }

                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lng))
                                                            {
                                                                CountryLongitude = Convert.ToDouble(LocationResult.Geometry.location.lng);
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (AddressComponentType == "administrative_area_level_1")
                                                {
                                                    State = long_name;
                                                    StateCode = short_name;
                                                    foreach (var LocationResult in _InternalGeoLocation.results)
                                                    {
                                                        if (LocationResult.address_components[0].long_name == State)
                                                        {
                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lat))
                                                            {
                                                                StateLatitude = Convert.ToDouble(LocationResult.Geometry.location.lat);
                                                            }

                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lng))
                                                            {
                                                                StateLongitude = Convert.ToDouble(LocationResult.Geometry.location.lng);
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (AddressComponentType == "administrative_area_level_2")
                                                {
                                                    District = long_name;
                                                    foreach (var LocationResult in _InternalGeoLocation.results)
                                                    {
                                                        if (LocationResult.address_components[0].long_name == District)
                                                        {
                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lat))
                                                            {
                                                                DistrictLatitude = Convert.ToDouble(LocationResult.Geometry.location.lat);
                                                            }

                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lng))
                                                            {
                                                                DistrictLongitude = Convert.ToDouble(LocationResult.Geometry.location.lng);
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (AddressComponentType == "locality")
                                                {
                                                    City = long_name;
                                                    foreach (var LocationResult in _InternalGeoLocation.results)
                                                    {
                                                        if (LocationResult.address_components[0].long_name == City)
                                                        {
                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lat))
                                                            {
                                                                CityLatitude = Convert.ToDouble(LocationResult.Geometry.location.lat);
                                                            }

                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lng))
                                                            {
                                                                CityLongitude = Convert.ToDouble(LocationResult.Geometry.location.lng);
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (AddressComponentType == "sublocality_level_1")
                                                {
                                                    CityArea = long_name;
                                                    foreach (var LocationResult in _InternalGeoLocation.results)
                                                    {
                                                        if (LocationResult.address_components[0].long_name == CityArea)
                                                        {
                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lat))
                                                            {
                                                                CityAreaLatitude = Convert.ToDouble(LocationResult.Geometry.location.lat);
                                                            }

                                                            if (!string.IsNullOrEmpty(LocationResult.Geometry.location.lng))
                                                            {
                                                                CityAreaLongitude = Convert.ToDouble(LocationResult.Geometry.location.lng);
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (AddressComponentType == "sublocality_level_2")
                                                {
                                                    CitySubArea = long_name;
                                                }
                                                else if (AddressComponentType == "route")
                                                {
                                                    CityStreetName = long_name;
                                                }
                                                else if (AddressComponentType == "street_number")
                                                {
                                                    CityStreetNumber = long_name;
                                                }
                                                else if (AddressComponentType == "postal_code")
                                                {
                                                    PostCode = long_name;
                                                }
                                            }
                                        }
                                        #endregion
                                        _OGeoLocation.Country = Country;
                                        _OGeoLocation.CountryCode = CountryCode;
                                        _OGeoLocation.CountryLatitude = CountryLatitude;
                                        _OGeoLocation.CountryLongitude = CountryLongitude;
                                        _OGeoLocation.State = State;
                                        _OGeoLocation.StateLatitude = StateLatitude;
                                        _OGeoLocation.StateLongitude = StateLongitude;
                                        _OGeoLocation.District = District;
                                        _OGeoLocation.DistrictLatitude = DistrictLatitude;
                                        _OGeoLocation.DistrictLongitude = DistrictLongitude;
                                        _OGeoLocation.City = City;
                                        _OGeoLocation.CityLatitude = CityLatitude;
                                        _OGeoLocation.CityLongitude = CityLongitude;
                                        _OGeoLocation.CityArea = CityArea;
                                        _OGeoLocation.CityAreaLatitude = CityAreaLatitude;
                                        _OGeoLocation.CityAreaLongitude = CityAreaLongitude;
                                        _OGeoLocation.CitySubArea = CitySubArea;
                                        _OGeoLocation.CityStreetName = CityStreetName;
                                        _OGeoLocation.CityStreetNumber = CityStreetNumber;
                                        _OGeoLocation.Address = Address;
                                        _OGeoLocation.PostCode = PostCode;
                                        return _OGeoLocation;
                                    }
                                    else
                                    {
                                        #region Set Location And Send Response
                                        _OGeoLocation.Message = _InternalGeoLocation.status;
                                        _OGeoLocation.Status = "ERROR";
                                        return _OGeoLocation;
                                        #endregion
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Set Location And Send Response
                                    _OGeoLocation.Message = _InternalGeoLocation.status;
                                    _OGeoLocation.Status = "ERROR";
                                    return _OGeoLocation;
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Set Location And Send Response
                                _OGeoLocation.Message = "No Result";
                                _OGeoLocation.Status = "ERROR";
                                return _OGeoLocation;
                                #endregion

                            }
                        }
                        else
                        {
                            #region Set Location And Send Response
                            _OGeoLocation.Message = "No Result";
                            _OGeoLocation.Status = "ERROR";
                            return _OGeoLocation;
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region Set Location And Send Response
                        _OGeoLocation.Message = "Invalid longitude";
                        _OGeoLocation.Status = "ERROR";
                        return _OGeoLocation;
                        #endregion
                    }
                }
                else
                {
                    #region Set Location And Send Response
                    _OGeoLocation.Message = "Invalid latitude";
                    _OGeoLocation.Status = "ERROR";
                    return _OGeoLocation;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                string ErrorMessage = _Exception.Message;
                #region Set Location And Send Response
                _OGeoLocation.Status = "ERROR";
                _OGeoLocation.Message = ErrorMessage;
                return _OGeoLocation;
                #endregion
            }
        }
        private string HttpWebRequestProcess(string Latitude, string? Longitude)
        {
            try
            {
                string EndPoint = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + Latitude + "," + Longitude;

                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(EndPoint);
                    request.Method = "GET";
                    request.ContentType = "application/json";
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream())
                    {
                        if (webStream != null)
                        {
                            using (StreamReader responseReader = new StreamReader(webStream))
                            {
                                return responseReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch
                {
                }
                return "";

                //WebClient webClient = new WebClient();
                //var jsonData = webClient.DownloadData(url);
                //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(address_result));
                //var DDD = ser.ReadObject(new MemoryStream(jsonData));


                //WebClient webClient = new WebClient();
                //var jsonData = webClient.DownloadData(url);
                //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RootObject));
                //var rootObject = ser.ReadObject(new MemoryStream(jsonData));



                ////DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RootObject));
                ////var rootObject = ser.ReadObject(new MemoryStream(jsonData));

                ////HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                ////HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream resStream = jsonData.GetResponseStream();
                //if (resStream != null)
                //{
                //    using (MemoryStream responseReader = new MemoryStream(rootObject))
                //    {
                //        return responseReader.Read();
                //    }
                //}


                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //// request.ContentType = "application/json";
                // WebResponse webResponse = request.GetResponse();
                // using (Stream webStream = webResponse.GetResponseStream())
                // {
                //     if (webStream != null)
                //     {
                //         using (StreamReader responseReader = new StreamReader(webStream))
                //         {
                //             return responseReader.ReadToEnd();
                //         }
                //     }
                // }
            }
            catch
            {
            }
            return "";
        }
    }
}
