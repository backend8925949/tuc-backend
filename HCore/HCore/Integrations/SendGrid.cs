//==================================================================================
// FileName: SendGrid.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace HCore.Integrations
{
    public class SendGrid
    {
        //public SendGrid()
        //{
        //}
        static async Task BroadCastEmail(string ToAddress, string? ToName, object Parameters)
        {
            //var my_object = new
            //{
            //    Name = "Harshal Gandole",
            //    Telephone = "+917588565834",
            //    ID = "#990099"
            //};
            var _SendGridClient = new SendGridClient("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");
            List<object> _ObjectList = new List<object>();
            _ObjectList.Add(Parameters);
            List<EmailAddress> _Email = new List<EmailAddress>();
            _Email.Add(new EmailAddress
            {
                Email = ToAddress,
                Name = ToName
            });
            var _FromAddress = new EmailAddress("noreply@thankucash.com", "Thank U Cash");
            var _BroacastMessage = MailHelper.CreateMultipleTemplateEmailsToMultipleRecipients(_FromAddress, _Email, "d-8e8506e5e1dc4c97a70ba8f39a64283b", _ObjectList);
            var _BroadCastResponse = await _SendGridClient.SendEmailAsync(_BroacastMessage);
        }

    }
}
