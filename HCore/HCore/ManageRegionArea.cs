//==================================================================================
// FileName: ManageRegionArea.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageRegionArea
    {
        //FrameworkRegionArea _FrameworkRegionArea;
        //public OResponse SaveRegionArea(ORegionArea.Save _Request)
        //{
        //    _FrameworkRegionArea = new FrameworkRegionArea();
        //    return _FrameworkRegionArea.SaveRegionArea(_Request);
        //}

        //public OResponse UpdateRegionArea(ORegionArea.Save _Request)
        //{
        //    _FrameworkRegionArea = new FrameworkRegionArea();
        //    return _FrameworkRegionArea.UpdateRegionArea(_Request);
        //}

        //public OResponse DeleteRegionArea(ORegionArea.Save _Request)
        //{
        //    _FrameworkRegionArea = new FrameworkRegionArea();
        //    return _FrameworkRegionArea.DeleteRegionArea(_Request);
        //}

        //public OResponse GetRegionAreaByRegion(OList.Request _Request)
        //{
        //    _FrameworkRegionArea = new FrameworkRegionArea();
        //    return _FrameworkRegionArea.GetRegionAreaByRegion(_Request);
        //}

        //public OResponse GetRegionAreaDetails(ORegionArea.Request _Request)
        //{
        //    _FrameworkRegionArea = new FrameworkRegionArea();
        //    return _FrameworkRegionArea.GetRegionAreaDetails(_Request);
        //}
    }
}
