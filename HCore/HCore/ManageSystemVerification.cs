//==================================================================================
// FileName: ManageSystemVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageSystemVerification
    {
        #region Declare
        
        FrameworkSystemVerification _FrameworkSystemVerification;
        #endregion
        #region Manage Operations
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtp(OSystemVerification.Request _Request)
        {
            #region Send Response
            _FrameworkSystemVerification = new FrameworkSystemVerification();
            return _FrameworkSystemVerification.RequestOtp(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtp(OSystemVerification.RequestVerify _Request)
        {
            #region Send Response
            _FrameworkSystemVerification = new FrameworkSystemVerification();
            return _FrameworkSystemVerification.VerifyOtp(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Expires the verification request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ExpireVerificationRequest(OSystemVerificationDetails _Request)
        {
            #region Send Response
            _FrameworkSystemVerification = new FrameworkSystemVerification();
            return _FrameworkSystemVerification.ExpireVerificationRequest(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the system verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemVerification(OSystemVerificationDetails _Request)
        {
            #region Send Response
            _FrameworkSystemVerification = new FrameworkSystemVerification();
            return _FrameworkSystemVerification.GetSystemVerification(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the system verifications.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemVerifications(OList.Request _Request)
        {
            #region Send Response
            _FrameworkSystemVerification = new FrameworkSystemVerification();
            return _FrameworkSystemVerification.GetSystemVerifications(_Request);
            #endregion
        }
        #endregion
    }
}
