//==================================================================================
// FileName: ManageUserTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore
{
    public class ManageUserTransaction
    {
        FrameworkUserTransaction _FrameworkUserTransaction;

        /// <summary>
        /// Description: Processes the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ProcessTransaction(OProcessTransaction _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.ProcessTransaction(_Request);
        }
        /// <summary>
        /// Description: Rolls the back transaction.
        /// </summary>
        /// <param name="ReferenceId">The reference identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        public void RollBackTransaction(long ReferenceId, OUserReference UserReference)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            _FrameworkUserTransaction.RollBackTransaction(ReferenceId, UserReference);
        }
        /// <summary>
        /// Description: Saves the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OTransactionReference.</returns>
        public OTransactionReference SaveTransaction(OSaveTransaction _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.SaveTransaction(_Request);
        }
        /// <summary>
        /// Description: Saves the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OTransactionReference.</returns>
        public OTransactionReference SaveTransaction(OSaveTransactions _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.SaveTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountBalance(OTransaction.Balance.Request _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetUserAccountBalance(_Request);
        }
        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        public double GetUserAccountBalance(long UserAccountId, int SourceId, OUserReference UserReference)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetUserAccountBalance(UserAccountId, SourceId, UserReference);
        }
        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserBalance(long UserAccountId, OUserReference UserReference)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetAppUserBalance(UserAccountId, UserReference);
        }
        /// <summary>
        /// Description: Gets the application user balance summary.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>ORewardBalance.</returns>
        public ORewardBalance GetAppUserBalanceSummary(long UserAccountId, OUserReference UserReference)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetAppUserBalanceSummary(UserAccountId, UserReference);
        }
        /// <summary>
        /// Description: Gets the application user thank u cash plus balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserThankUCashPlusBalance(long UserAccountId, OUserReference UserReference)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetAppUserThankUCashPlusBalance(UserAccountId, UserReference);
        }
        /// <summary>
        /// Description: Gets the transactions overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactionsOverview(OTransaction.Overview.Request _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetTransactionsOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransaction(OTransaction.Details _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactions(OList.Request _Request)
        {
            _FrameworkUserTransaction = new FrameworkUserTransaction();
            return _FrameworkUserTransaction.GetTransactions(_Request);
        }

    }
}
