//==================================================================================
// FileName: ManageUserDevice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Framework;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore
{
    public class ManageUserDevice
    {
        #region Declare
        FrameworkUserDevice _FrameworkUserDevice;
        #endregion
        #region Operations
        /// <summary>
        /// Description: Updates the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserDevice(OUserDevice _Request)
        {
            #region Send Response
            _FrameworkUserDevice = new FrameworkUserDevice();
            return _FrameworkUserDevice.UpdateUserDevice(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Deletes the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteUserDevice(OUserDevice _Request)
        {
            #region Send Response
            _FrameworkUserDevice = new FrameworkUserDevice();
            return _FrameworkUserDevice.DeleteUserDevice(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserDevice(OUserDevice _Request)
        {
            #region Send Response
            _FrameworkUserDevice = new FrameworkUserDevice();
            return _FrameworkUserDevice.GetUserDevice(_Request);
            #endregion
        }
        /// <summary>
        /// Description: Gets the user device list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserDevices(OList.Request _Request)
        {
            #region Send Response
            _FrameworkUserDevice = new FrameworkUserDevice();
            return _FrameworkUserDevice.GetUserDevices(_Request);
            #endregion
        }



        #endregion
    }
}
