//==================================================================================
// FileName: AppUserSignin.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Object;
using HCore.Operations;
using HCore.Operations.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Background
{
    internal class AppUserSignin : ReceiveActor
    {
        ManageUserTransaction _ManageUserTransaction;
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        CAProductCode _CAProductCode;
        TUCPromoCodeAccount _TUCPromoCodeAccount;
        HCoreContextOperations _HCoreContextOperations;
        public AppUserSignin()
        {
            Receive<OUserAccountAccess.AppProcessRequest>(_Request =>
            {
                try
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                        var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            AccountId = x.Id,
                            AccessPin = x.AccessPin,
                            EmailAddress = x.EmailAddress,
                            MobileNumber = x.MobileNumber,
                            FirstName = x.FirstName,
                            CreateDate = x.CreateDate,
                            AccountTypeId = x.AccountTypeId,
                            CountryIsd = x.Country.Isd,
                            CountryNumberLength = x.Country.MobileNumberLength
                        }).FirstOrDefault();
                        _ManageUserTransaction = new ManageUserTransaction();
                        _Request.UserReference.AccountId = AccountDetails.AccountId;
                        ORewardBalance BalanceDetails = _ManageUserTransaction.GetAppUserBalanceSummary(_Request.AccountId, _Request.UserReference);
                        if (!string.IsNullOrEmpty(AccountDetails.AccessPin))
                        {
                            string DAccessPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, AccountDetails.CountryIsd, AccountDetails.MobileNumber, "Welcome to Thank U Cash: Your Bal: N" + BalanceDetails.Balance + ". Your redeeming PIN is: " + DAccessPin + ".", AccountDetails.AccountId, null);
                            }
                        }
                        if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                        {
                            var _EmailParameters = new
                            {
                                Credit = BalanceDetails.Credit,
                                Debit = BalanceDetails.Debit,
                                Balance = BalanceDetails.Balance,
                                FirstName = AccountDetails.FirstName,
                            };
                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.AppUser_Welcome, AccountDetails.FirstName, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                        }
                        if (string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var CustomerPreRegistration = _HCoreContextOperations.HCOCustomerRegistration
                                .Where(x => x.MobileNumber == AccountDetails.MobileNumber && x.StatusId == 1)
                                .OrderByDescending(x => x.CreateDate)
                                .FirstOrDefault();
                                if (CustomerPreRegistration != null)
                                {
                                    if (!string.IsNullOrEmpty(CustomerPreRegistration.ReferralCode))
                                    {
                                        _Request.ReferralCode = CustomerPreRegistration.ReferralCode;
                                    }
                                    CustomerPreRegistration.AccountId = AccountDetails.AccountId;
                                    CustomerPreRegistration.StatusId = 2;
                                    _HCoreContextOperations.SaveChanges();
                                    _HCoreContextOperations.Dispose();
                                }
                                else
                                {
                                    _HCoreContextOperations.Dispose();
                                }
                            }
                        }
                        if (_Request.UserType == "old" && !string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var _ReferralScheme = _HCoreContext.TUCPromoCode
                                .Where(x => x.Code == _Request.ReferralCode
                                && x.AccountTypeId == AccountDetails.AccountTypeId
                                && x.Condition.SystemName == "appreinstall"
                                && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate)
                                .FirstOrDefault();
                                if (_ReferralScheme != null)
                                {
                                    long UseCount = _HCoreContext.TUCPromoCodeAccount.Count(x => x.PromoCodeId == _ReferralScheme.Id);
                                    if (UseCount <= _ReferralScheme.MaximumLimit)
                                    {
                                        long CustomerUseCount = _HCoreContext.TUCPromoCodeAccount.Count(x => x.AccountId == AccountDetails.AccountId && x.PromoCodeId == _ReferralScheme.Id);
                                        if (CustomerUseCount <= _ReferralScheme.MaximumLimitPerUser)
                                        {
                                            _TUCPromoCodeAccount = new TUCPromoCodeAccount();
                                            _TUCPromoCodeAccount.Guid = HCoreHelper.GenerateGuid();
                                            _TUCPromoCodeAccount.PromoCodeId = _ReferralScheme.Id;
                                            _TUCPromoCodeAccount.AccountId = AccountDetails.AccountId;
                                            _TUCPromoCodeAccount.Value = _ReferralScheme.Value;
                                            _TUCPromoCodeAccount.UseDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.TUCPromoCodeAccount.Add(_TUCPromoCodeAccount);
                                            _HCoreContext.SaveChanges();
                                        }
                                        if (_ReferralScheme.Value > 0)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = 3;
                                            _CoreTransactionRequest.CustomerId = AccountDetails.AccountId;
                                            _CoreTransactionRequest.InvoiceAmount = _ReferralScheme.Value;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _ReferralScheme.Value;
                                            _CoreTransactionRequest.ReferenceAmount = _ReferralScheme.Value;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = 3,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.TUCPromotions.PromotionalCredit,
                                                SourceId = TransactionSource.TUCPromotions,
                                                Amount = _ReferralScheme.Value,
                                                Comission = 0,
                                                TotalAmount = _ReferralScheme.Value,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.TUCPromotions.PromotionalCredit,
                                                SourceId = TransactionSource.TUC,
                                                Amount = _ReferralScheme.Value,
                                                Comission = 0,
                                                TotalAmount = _ReferralScheme.Value,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (_Request.UserType == "new" && !string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var _ReferralScheme = _HCoreContext.TUCPromoCode
                                .Where(x => x.Code == _Request.ReferralCode
                                && x.AccountTypeId == AccountDetails.AccountTypeId
                                && x.Condition.SystemName == "newcustomerregistration"
                                && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate)
                                .FirstOrDefault();
                                if (_ReferralScheme != null)
                                {
                                    long UseCount = _HCoreContext.TUCPromoCodeAccount.Count(x => x.PromoCodeId == _ReferralScheme.Id);
                                    if (UseCount <= _ReferralScheme.MaximumLimit)
                                    {
                                        long CustomerUseCount = _HCoreContext.TUCPromoCodeAccount.Count(x => x.AccountId == AccountDetails.AccountId && x.PromoCodeId == _ReferralScheme.Id);
                                        if (CustomerUseCount <= _ReferralScheme.MaximumLimitPerUser)
                                        {
                                            _TUCPromoCodeAccount = new TUCPromoCodeAccount();
                                            _TUCPromoCodeAccount.Guid = HCoreHelper.GenerateGuid();
                                            _TUCPromoCodeAccount.PromoCodeId = _ReferralScheme.Id;
                                            _TUCPromoCodeAccount.AccountId = AccountDetails.AccountId;
                                            _TUCPromoCodeAccount.Value = _ReferralScheme.Value;
                                            _TUCPromoCodeAccount.UseDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.TUCPromoCodeAccount.Add(_TUCPromoCodeAccount);
                                            _HCoreContext.SaveChanges();
                                        }
                                        if (_ReferralScheme.Value > 0)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = 3;
                                            _CoreTransactionRequest.CustomerId = AccountDetails.AccountId;
                                            _CoreTransactionRequest.InvoiceAmount = _ReferralScheme.Value;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _ReferralScheme.Value;
                                            _CoreTransactionRequest.ReferenceAmount = _ReferralScheme.Value;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = 3,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.TUCPromotions.PromotionalCredit,
                                                SourceId = TransactionSource.TUCPromotions,
                                                Amount = _ReferralScheme.Value,
                                                Comission = 0,
                                                TotalAmount = _ReferralScheme.Value,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.TUCPromotions.PromotionalCredit,
                                                SourceId = TransactionSource.TUC,
                                                Amount = _ReferralScheme.Value,
                                                Comission = 0,
                                                TotalAmount = _ReferralScheme.Value,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (_Request.AddressComponent != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (_Request.AddressComponent != null && _Request.AddressComponent.CityId > 0 && _Request.AddressComponent.StateId > 0)
                            {
                                bool SaveAddress = true;
                                if (_Request.UserType != "new")
                                {
                                    bool AddressCheck = _HCoreContext.HCCoreAddress.Any(x => x.AccountId == _Request.AccountId
                                    && x.Name == _Request.AddressComponent.Name
                                    && x.CityId == _Request.AddressComponent.CityId
                                    && x.StateId == _Request.AddressComponent.StateId
                                    && x.AddressLine1 == _Request.AddressComponent.AddressLine1);
                                    if (AddressCheck)
                                    {
                                        SaveAddress = false;
                                    }
                                }
                                if (SaveAddress)
                                {
                                    HCCoreAddress _HCCoreAddress;
                                    _HCCoreAddress = new HCCoreAddress();
                                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    _HCCoreAddress.AccountId = _Request.AccountId;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    //_HCCoreAddress.DisplayName = "Other";
                                    _HCCoreAddress.LocationTypeId = 800;
                                    if (!string.IsNullOrEmpty(_Request.AddressComponent.Name))
                                    {
                                        _HCCoreAddress.Name = _Request.Name;
                                    }
                                    else
                                    {
                                        _HCCoreAddress.Name = _Request.FirstName + " " + _Request.LastName;
                                    }

                                    if (!string.IsNullOrEmpty(_Request.AddressComponent.MobileNumber))
                                    {
                                        _HCCoreAddress.ContactNumber = _Request.AddressComponent.MobileNumber;
                                    }
                                    else
                                    {
                                        _HCCoreAddress.ContactNumber = _Request.MobileNumber;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                    {
                                        _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                    }
                                    else
                                    {
                                        _HCCoreAddress.EmailAddress = _Request.MobileNumber;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.AddressComponent.AddressLine1))
                                    {
                                        _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                    }
                                    else
                                    {
                                        _HCCoreAddress.AddressLine1 = "";
                                    }
                                    _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                    _HCCoreAddress.ZipCode = _Request.AddressComponent.ZipCode;
                                    _HCCoreAddress.Landmark = _Request.AddressComponent.Landmark;
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                    }
                                    _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                    _HCCoreAddress.CountryId = _Request.CountryId;
                                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreAddress.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    if (_Request.UserReference.AccountId > 0)
                                    {
                                        _HCCoreAddress.CreatedById = _Request.UserReference.AccountId;
                                    }
                                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                }

                            }
                        }
                    }

                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("AppUserSignin", _Exception);
                }
            });
        }
    }
    internal class AppUserSigninProfileManager : ReceiveActor
    {
        ManageUserTransaction _ManageUserTransaction;
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        CAProductCode _CAProductCode;
        TUCPromoCodeAccount _TUCPromoCodeAccount;
        HCoreContextOperations _HCoreContextOperations;
        public AppUserSigninProfileManager()
        {
            Receive<OUserAccountAccess.AppProcessRequest>(_Request =>
            {

            });
        }
    }
}
