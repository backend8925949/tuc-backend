//==================================================================================
// FileName: ManageCityArea.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageCityArea
    {
        //FrameworkCityArea _FrameworkCityArea;
        //public OResponse SaveCityArea(OCityArea.Save _Request)
        //{
        //    _FrameworkCityArea = new FrameworkCityArea();
        //    return _FrameworkCityArea.SaveCityArea(_Request);
        //}

        //public OResponse UpdateCityArea(OCityArea.Save _Request)
        //{
        //    _FrameworkCityArea = new FrameworkCityArea();
        //    return _FrameworkCityArea.UpdateCityArea(_Request);
        //}

        //public OResponse DeleteCityArea(OCityArea.Save _Request)
        //{
        //    _FrameworkCityArea = new FrameworkCityArea();
        //    return _FrameworkCityArea.DeleteCityArea(_Request);
        //}

        //public OResponse GetCityAreaByCity(OList.Request _Request)
        //{
        //    _FrameworkCityArea = new FrameworkCityArea();
        //    return _FrameworkCityArea.GetCityAreaByCity(_Request);
        //}

        //public OResponse GetCityAreaDetails(OCityArea.Request _Request)
        //{
        //    _FrameworkCityArea = new FrameworkCityArea();
        //    return _FrameworkCityArea.GetCityAreaDetails(_Request);
        //}
    }
}
