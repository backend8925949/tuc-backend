//==================================================================================
// FileName: HCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿namespace HCore
{
    public static class CoreConstant
    {

        public const long ThankUMerchantId = 3;
        public const long SmashLabId = 971;
        public static long ThankUAccountId = 3;
        public static long PTSAAccountId = 120;
        public const int DefaultLimit = 10;
        public const int StatusActive = 2;
        public const int StatusInactive = 1;
        public const int StatusBlocked = 3;
        public const int StatusArchived = 4;



        public class RouteUrl
        {
            public class V1
            {
                public const string System = "/api/v1/system/";
                public const string ThankU = "/api/v1/thanku/";
            }
            public class V1Support
            {
                public const string System = "/api/web/v1/system/";
                public const string ThankU = "/api/web/v1/thankuconnect/";
            }
        }

        public static class SendGridEmailTemplateIds
        {

            public const string AuthCodeEmail_Admin = "d-c0062bada1f0482baa93d3d28f9b1dee";
            public const string AppUser_Welcome = "d-1f93a4c971cb445f809809b6770bc7cd";
            public const string ForgotPassword = "d-66312471260d489380adfc7aad3e834e";
            public const string GatewayException = "d-8e8506e5e1dc4c97a70ba8f39a64283b";
            public const string RewardEmail = "d-0e22a726fc3f4fd1aa72cbfee2fee6f1";
            public const string RedeemEmail = "d-1635d8f52c664ed285aae77a553d4046";
            public static class ProductsManager
            {
                public  static class System
                {
                    public const string Order_PendingConfirmation = "d-8d4ea58dd09841b49eb9d2e2af61ed8e";
                    public const string Order_Confirmed = "d-318f9b78c7dc442cabc7b46679099248";
                    public const string Order_Preparing = "d-b81a1bdc93b54ff9acac361f5785e9b6";
                    public const string Order_Ready = "d-7d1192b60e744b92b83bd0ce1bd9bdbc";
                    public const string Order_ReadyToPickup = "d-56c19a241d4847f88a1bbe4229d53985";
                    public const string Order_OutForDelivery = "d-3d25c4466ac44e0d90173bb4c4f2ee0c";
                    public const string Order_DeliveryFailed = "d-d9c63790ac0746f2b67e3a598fc7445b";
                    public const string Order_CancelledByUser = "d-2e2a620e4c654e39ab5e02d57c139455";
                    public const string Order_CancelledBySeller = "d-df2b331d35ac431ba11d137fb05c6002";
                    public const string Order_CancelledBySystem = "d-fc76767c160d41bda2b74d6f83c96628";
                    public const string Order_Delivered = "d-e30abfe18c144430bcc411f074258b86";
                }
            }
        }

        public static class HCoreTask
        {
            public const string Get_AdvertiserOverview = "getadvertiseroverview";
            public const string Get_MerchantOverview = "getmerchantoverview";
            public const string Get_StoreOverview = "getstoreoverview";
            public const string Get_CardUserOverview = "getcarduseroverview";
            public const string Get_AppUserOverview = "getappuseroverview";
            public const string Get_CashierOverview = "getcashieroverview";

            public const string Get_UserAccounts = "getuseraccounts";
            public const string Get_Advertisers = "getadvertisers";
            public const string Get_Merchants = "getmerchants";
            public const string Get_Stores = "getstores";
            public const string Get_CardUsers = "getcardusers";
            public const string Get_AppUsers = "getappusers";
            public const string Get_Cashiers = "getcashiers";

            public const string Get_Merchant = "getmerchant";
            public const string Get_MerchantTransactionOverview = "getmerchanttransactionoverview";
        }
        public class ListType
        {
            public const string System = "system";
            public const string All = "all";
            public const string ReferralUsers = "referralusers";

            public const string Lite = "lite";
            public const string Cashiers = "cashiers";
            public const string CardUsers = "cardusers";
            public const string CardUserAllVisit = "carduserallvisit";
            public const string CardUserStoreVisit = "carduserstorevisit";
            public const string CardUserCashierVisit = "cardusercashiervisit";
            public const string CardUserMerchantVisit = "cardusermerchantvisit";
            public const string Merchants = "merchants";
            public const string Stores = "stores";
            public const string Advertisers = "advertisers";
            public const string AppUsers = "appusers";
            public const string PointsTransfer = "pointstransfer";
            public const string Comissions = "comission";
            public const string MerchantGateways = "merchantgateways";
            public const string MerchantTerminals = "merchantterminals";
            public const string TerminalView = "terminalview";
            public const string AcquirerMerchantView = "acquirermerchantview";
            public const string AcquirerMerchants = "acquirermerchants";
            public const string OwnerView = "ownerview";
            public const string OwnerViewUnique = "ownerviewunique";
            public const string InvoiceTransaction = "invoicetransaction";

            public const string Rewards = "rewards";
            public const string Visitors = "visitors";
            public const string Redeems = "redeems";
            public const string Sale = "sale";
            public const string Payments = "payments";
            public const string Settlements = "settlements";
            public const string Invoices = "invoices";
            public const string ByDate = "bydate";
            public const string Commissions = "commissions";
            public const string AcquirerCommissions = "acquirercommissions";
            public const string CommissionsByDate = "commissionsbydate";

        }
        public class CoreHelperStatus
        {
            public class Default
            {
                public const long Inactive = 1;
                public const long Active = 2;
                public const long Blocked = 3;
                public const long Archived = 4;
            }
            public class Transaction
            {
                public const long Processing = 27;
                public const long Success = 28;
                public const long Pending = 29;
                public const long Failed = 30;
                public const long Cancelled = 31;
                public const long Error = 32;
            }
        }


        public class CoreHelpers
        {
            public class RegistrationSource
            {
                public const int System = 118;
                public const int App = 671;
            }
            public class UserApplicationStatus
            {
                public const int AppInstalled = 195;

                public const int ValidationPending = 199;
                public const int ApprovalPending = 200;
                public const int Approved = 201;
            }
            public class StorageType
            {
                public const string Image = "storagetype.image";
            }
            public class InvoiceType
            {
                public const int Payment = 144;
                public const int RewardInvoice = 263;
                public const int InvoiceItem = 184; 
            }
            public class AccountOperationType
            {
                public const int Online = 173;
                public const int Offline = 174;
                public const int OnlineAndOffline = 175;
            }
            //public class TransactionType
            //{
            //    public const int CashPayment = 147;
            //    public const int OnlineRewardPoints = 148;
            //    public const int RewardCommission = 204;
            //    public const int OnlineGiftPoints = 149;
            //    public const int MerchantCredit = 150;
            //    public const int StoreCredit = 151;
            //    public const int CashierCredit = 152;
            //    public const int CardReward = 153;
            //    public const int CardChange = 154;
            //    public const int CashReward = 155;
            //    public const int CardRedeem = 156;
            //    public const int PendingSettlement = 157;
            //    public const int PointsSettlement = 158;
            //    public const int AtmCardPayment = 159;
            //    public const int BankSettlement = 160;
            //    public const int BankPayment = 161;
            //    public const int CreditCardPayment = 162;
            //    public const int OnlinePayment = 163;
            //    public const int OnlineRedeem = 164;
            //    public const int InvoiceRewards = 165;
            //    public const int ReferralBonus = 202;
            //    public const int ThankUCashPlusCredit = 245;
            //}
            //public class TransactionTypeCategory
            //{
            //    public const int Reward = 207;
            //    public const int Redeem = 208;
            //    public const int Settlement = 209;
            //    public const int PendingSettlement = 210;
            //    public const int PointTransfer = 211;
            //    public const int Payments = 212;
            //}
            //public class TransactionTypeValue
            //{
            //    public const string Reward = "reward";
            //    public const string Redeem = "redeem";
            //    public const string Payments = "payments";
            //}
            //public class TransactionSource
            //{
            //    public const int Merchant = 166;
            //    public const int App = 167;
            //    public const int Card = 168;
            //    public const int Store = 169;
            //    public const int Cashier = 170;
            //    public const int Settlement = 171;
            //    public const int Payments = 172;
            //    public const int ThankUCashPlus = 244;
            //    public const string AppS = "transaction.source.app";
            //    public const string CardS = "transaction.source.card";
            //    public const string SettlementS = "transaction.source.settlement";
            //    public const string PaymentsS = "transaction.source.payments";
            //}
            //public class TransactionMode
            //{
            //    public const int Credit = 145;
            //    public const int Debit = 146;
            //}
            public class LogType
            {
                public const int LogType_Exception = 89;
                public const int LogType_Info = 90;
                public const int LogType_Alert = 91;
                public const int LogType_Security = 92;
            }
            public enum NotificationTypeE
            {
                Email = 133,
                Sms = 134,
                DevicePush = 135,
                InSystem = 136,
            }
            public class NotificationType
            {
                public const long Email = 133;
                public const long Sms = 134;
                public const long DevicePush = 135;
                public const long InSystem = 136;
            }
            public class NotificationGateway
            {
                public const long SMTP = 137;
                public const long MailGun = 138;
                public const long Firbase = 139;
                public const long InSystem = 140;
            }
        }
        //public class UserAccountType
        //{
        //    public const int Controller = 106;
        //    public const int Admin = 107;
        //    public const int Merchant = 108;
        //    public const int Appuser = 109;
        //    public const int Carduser = 110;
        //    public const int MerchantStore = 111;
        //    public const int MerchantCashier = 112;
        //    public const int PgAccount = 113;
        //    public const int PosAccount = 114;
        //    public const int TerminalAccount = 115;
        //    public const int Acquirer = 116;
        //    public const int AcquirerSubAccount = 117;
        //    public const int Agent = 221;
        //}


        public class StatusHelperId
        {
            public const int Default_Inactive = 1;
            public const int Default_Active = 2;
            public const int Default_Blocked = 3;
            public const int Default_Deleted = 4;

            public const int Order_New = 5;
            public const int Order_Dispatched = 6;
            public const int Order_Delivered = 7;
            public const int Order_Cancelled = 8;


            public const int CardOrder_New = 9;
            public const int CardOrder_Approved = 10;
            public const int CardOrder_Rejected = 11;
            public const int CardOrder_Preparing = 12;
            public const int CardOrder_Ready = 13;
            public const int CardOrder_Dispatched = 14;
            public const int CardOrder_Delivered = 15;
            public const int CardOrder_Cancelled = 16;

            public const int Device_InStock = 17;
            public const int Device_MerchantAssigned = 18;
            public const int Device_StoreAssigned = 19;
            public const int Device_Blocked = 20;
            public const int Device_NotWorking = 21;

            public const int Card_NotAssigned = 22;
            public const int Card_Assigned = 23;
            public const int Card_Blocked = 24;
            public const int Card_Lost = 25;
            public const int Card_Damaged = 26;

            public const int Transaction_Processing = 27;
            public const int Transaction_Success = 28;
            public const int Transaction_Pending = 29;
            public const int Transaction_Failed = 30;
            public const long Transaction_Cancelled = 31;
            public const int Transaction_Error = 32;


            public const int RewardInvoice_Pending = 33;
            public const int RewardInvoice_Approved = 34;
            public const int RewardInvoice_Rejected = 35;

            public const int Invoice_Inactive = 36;
            public const int Invoice_Creating = 37;
            public const int Invoice_Pending = 38;
            public const int Invoice_Paid = 39;
            public const int Invoice_Cancelled = 40;
        }
    }
}
