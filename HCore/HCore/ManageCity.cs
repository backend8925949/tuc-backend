//==================================================================================
// FileName: ManageCity.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageCity
    {
        //FrameworkCity _FrameworkCity;
        //public OResponse SaveCity(OCity.Save _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.SaveCity(_Request);
        //}

        //public OResponse UpdateCity(OCity.Save _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.UpdateCity(_Request);
        //}

        //public OResponse DeleteCity(OCity.Save _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.DeleteCity(_Request);
        //}

        //public OResponse GetCityDetailsByRegionAreaId(OList.Request _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.GetCityDetailsByRegionAreaId(_Request);
        //}

        //public OResponse GetCityDetails(OCity.Request _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.GetCityDetails(_Request);
        //}

        //public OResponse GetCityByRegionArea(OList.Request _Request)
        //{
        //    _FrameworkCity = new FrameworkCity();
        //    return _FrameworkCity.GetCityByRegionArea(_Request);
        //}
    }
}
