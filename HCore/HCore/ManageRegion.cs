//==================================================================================
// FileName: ManageRegion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Framework;
using HCore.Helper;
using HCore.Object;

namespace HCore
{
    public class ManageRegion
    {
        //FrameworkRegion _FrameworkRegion;
        //public OResponse SaveRegion(ORegion.Save _Request)
        //{
        //    _FrameworkRegion = new FrameworkRegion();
        //    return _FrameworkRegion.SaveRegion(_Request);
        //}

        //public OResponse UpdateRegion(ORegion.Save _Request)
        //{
        //    _FrameworkRegion = new FrameworkRegion();
        //    return _FrameworkRegion.UpdateRegion(_Request);
        //}

        //public OResponse DeleteRegion(ORegion.Save _Request)
        //{
        //    _FrameworkRegion = new FrameworkRegion();
        //    return _FrameworkRegion.DeleteRegion(_Request);
        //}

        //public OResponse GetRegionsByCountry(OList.Request _Request)
        //{
        //    _FrameworkRegion = new FrameworkRegion();
        //    return _FrameworkRegion.GetRegionsByCountry(_Request);
        //}

        //public OResponse GetRegionDetails(ORegion.Request _Request)
        //{
        //    _FrameworkRegion = new FrameworkRegion();
        //    return _FrameworkRegion.GetRegionDetails(_Request);
        //}
    }
}
