//==================================================================================
// FileName: JobFactory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.ThankU;
//using HCore.ThankU.Operations;
using HCore.ThankUCash;
//using HCore.TUC.Core.Framework.Merchant.Upload;
//using HCore.TUC.Core.Operations.Background;
using HCore.TUC.Core.Operations.Cron;
using HCore.TUC.Core.Operations.CustomerWeb;
using HCore.TUC.Core.Operations.Operations;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.SmsCampaign;
using Quartz;
using Quartz.Impl;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace HCoreX.Api.Jobs
{
    public class HCoreXJobFactory
    {
        public async Task StartJobAsync()
        {

            ISchedulerFactory _ISchedulerFactory = new StdSchedulerFactory();
            IScheduler _IScheduler = await _ISchedulerFactory.GetScheduler();
            await _IScheduler.Start();

            // 1 HOUR
            #region 1 HOUR Job
            IJobDetail _IJobDetail_JOB_1_MIN = JobBuilder.Create<JOB_1_MIN>()
                                    .WithIdentity("JOB_1MIN", "JOB_1_MIN_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_1_MIN = TriggerBuilder.Create()
            .WithIdentity("JOB_1_MIN", "JOB_1_MIN_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_1_MIN)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInMinutes(3).RepeatForever())
            //.WithCronSchedule("0/60 * 0 ? * * *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_1_MIN, _ITrigger_JOB_1_MIN);
            #endregion


            #region 24 HOUR Job
            IJobDetail _IJobDetail_JOB_24_HOUR = JobBuilder.Create<JOB_24_HOUR>()
                                    .WithIdentity("JOB_24_HOUR", "JOB_24_HOUR_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_24_HOUR = TriggerBuilder.Create()
            .WithIdentity("JOB_24_HOUR", "JOB_24_HOUR_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_24_HOUR)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(24).RepeatForever())
            //.WithCronSchedule("0 0 9/24 ? * * *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_24_HOUR, _ITrigger_JOB_24_HOUR);
            #endregion



            #region 23 HOUR Job
            IJobDetail _IJobDetail_JOB_23_HOUR = JobBuilder.Create<JOB_23_HOUR>()
                                    .WithIdentity("JOB_23_HOUR", "JOB_23_HOUR_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_23_HOUR = TriggerBuilder.Create()
            .WithIdentity("JOB_23_HOUR", "JOB_23_HOUR_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_23_HOUR)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(23).RepeatForever())
            //.WithCronSchedule("59 23 * * 1-7")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_23_HOUR, _ITrigger_JOB_23_HOUR);
            #endregion


            #region 1 HOUR Job
            IJobDetail _IJobDetail_JOB_1_HOUR = JobBuilder.Create<JOB_1_HOUR>()
                                    .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_1_HOUR = TriggerBuilder.Create()
            .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_1_HOUR)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(1).RepeatForever())
            //.WithCronSchedule("0 0 0/1 1/1 * ? *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_1_HOUR, _ITrigger_JOB_1_HOUR);
            #endregion


            // LAST DAY OF THE MONTH
            #region 1 HOUR Job
            //IJobDetail _IJobDetail_JOB_30_OF_MONTH = JobBuilder.Create<JOB_30_OF_MON>()
            //                        .WithIdentity("JOB_30_OF_MON", "JOB_30_OF_MON_TRIGGER_Group")
            //                        .Build();
            //ITrigger _ITrigger_JOB_30_OF_MON = TriggerBuilder.Create()
            //.WithIdentity("JOB_30_OF_MON", "JOB_30_OF_MON_TRIGGER_Group")
            //.ForJob(_IJobDetail_JOB_30_OF_MONTH)
            //.StartNow()
            //.WithSimpleSchedule(x => x.WithIntervalInMinutes(30).RepeatForever())
            ////.WithCronSchedule("55 23 30 * *")
            //.Build();
            //await _IScheduler.ScheduleJob(_IJobDetail_JOB_30_OF_MONTH, _ITrigger_JOB_30_OF_MON);
            #endregion
        }
    }
    public class JOB_1_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            HCore.Data.Store.HCoreDataStoreManager.DataStore_Sync();


            //_ManageBot.ProcessTerminalDailyStatus();
            //_ManageBot.UpdateTerminalActivityStatus();

            //HCore.TUC.Plugins.Delivery.Operations.ManageShipment _ManageShipment = new HCore.TUC.Plugins.Delivery.Operations.ManageShipment();
            //_ManageShipment.UpdateOrders();

            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                // Cron Bg Sync
                //CronMerchantCategorySync _CronMerchantCategorySync = new CronMerchantCategorySync();
                //_CronMerchantCategorySync.CoreCronMerchantCategorySync();
                //HCore.TUC.Core.Operations.Merchant.ManageProcessPendingReward _xsd = new TUC.Core.Operations.Merchant.ManageProcessPendingReward();
                //_xsd.ProcessPendingReward();
            }

            // PROCESS SERVER ACTIVITY
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor)
            {

                int CurrentHour = HCoreHelper.GetGMTDateTime().Hour;
                if (CurrentHour == 3)
                {

                    // PROCESS SETTLEMENT INVOICE
                    //ManageSettlement _ManageSettlement = new ManageSettlement();
                    //_ManageSettlement.CreateRewardSettlementInvoices();
                    // PROCESS REWARD SETTLEMENT INVOICE
                    //ManageOverview _ManageOverview = new ManageOverview();
                    //_ManageOverview.CreateRewardSettlementInvoices();
                    // PROCESS BOT OVERVIEW
                    //HCore.Bot.Object.OSyncManager.Request _SynRequest = new HCore.Bot.Object.OSyncManager.Request();
                    //_SynRequest.StartDate = HCoreHelper.GetGMTDate().AddDays(-7);
                    //_SynRequest.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                    //HCore.Bot.ManageSalesBot _ManageSalesBot = new HCore.Bot.ManageSalesBot();
                    //_ManageSalesBot.ProcessSalesHistoryHourly(_SynRequest);
                    //UserOperations.ProcessMerchantOverview();
                }
                //if (CurrentHour == 8)
                //{
                //    HCore.Bot.ManageNotification _ManageNotification = new Bot.ManageNotification();
                //    _ManageNotification.SendSystemSalesReport();
                //}
            }
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {

                //BackgroundCustomerBalanceManager _BackgroundCustomerBalanceManager = new BackgroundCustomerBalanceManager();
                //_BackgroundCustomerBalanceManager.ExpireUserBalance();
                HCore.TUC.Plugins.Vas.ManageBackgroundSync _ManageBackgroundSync = new HCore.TUC.Plugins.Vas.ManageBackgroundSync();
                _ManageBackgroundSync.SyncVasItem();
                //BackgroundActivityStatusManager _BackgroundActivityStatusManager = new BackgroundActivityStatusManager();
                //_BackgroundActivityStatusManager.UpdateAccountActivityStatus();
            }
        }
    }
    public class JOB_1_MIN : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            #region DEALS SYNC CRON
            if (HCoreConstant.HostName == "appconnect.thankucash.com" ||
             HCoreConstant.HostName == "appconnect.thankucash.dev" ||
             HCoreConstant.HostName == "appconnect.thankucash.tech" ||
             HCoreConstant.HostName == "appconnect.thankucash.co" ||
             HCoreConstant.HostName == "testappconnect.thankucash.com" ||
             HCoreConstant.HostName == "testwebconnect.thankucash.com" ||
             HCoreConstant.HostName == "webconnect.thankucash.co")
            {
                HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
                _ManageCore.ProcessSync();
            }
            #endregion         
            //if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
            //    || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local
            //    || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            //{
            //    HCore.TUC.Data.Store.AnxAnalytics _Ana = new TUC.Data.Store.AnxAnalytics();
            //    _Ana.SyncSales();
            //    //_Ana.SyncReward();
            //    //_Ana.SyncRedeem();
            //    //_Ana.SyncCommissionClaim();
            //    //_Ana.SyncRewardClaim();
            //}
            //if (HCoreConstant.HostName == "webconnect.thankucash.com" || HCoreConstant.HostName == "testwebconnect.thankucash.com" || HCoreConstant.HostName == "localhost")
            //{
            //    //ManageSmsBroadcaster _ManageSmsBroadcaster = new ManageSmsBroadcaster();
            //    //_ManageSmsBroadcaster.OpenCampaign();
            //}
            if (
                HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                //HCore.TUC.Plugins.Deals.ManageBackgroundDealCheck _ManageDealStatus = new ManageBackgroundDealCheck();
                //_ManageDealStatus.ManageDealStatus();

                //HCore.Bot.ManageBot _ManageBot = new Bot.ManageBot();
                //_ManageBot.ProcessAllTerminalStatus();

                //AcquirerCampaignStatusUpdate _AcquirerCampaignStatusUpdate = new AcquirerCampaignStatusUpdate();
                //_AcquirerCampaignStatusUpdate.UpdateAcquirerCampaignStatus();

                //HCore.TUC.Core.Operations.Acquirer.ManageOnboarding _AcqOnboarding = new HCore.TUC.Core.Operations.Acquirer.ManageOnboarding();
                //_AcqOnboarding.GetOnboardedCustomerFiles();
            }

            // PROCESS SERVER ACTIVITY
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                //ManageUploadCustomerReward _ManageUploadCustomerReward = new ManageUploadCustomerReward();
                //_ManageUploadCustomerReward.ActorUploadCustomerRewardProcess();
                //HCore.TUC.Core.Operations.Operations.ManageOperations _ManageOperations = new TUC.Core.Operations.Operations.ManageOperations();
                //_ManageOperations.ProcessSales();
                //_ManageOperations.ProcessLoyaltyMerchantCustomer();
                //_ManageOperations.ProcessLoyalty();
                //HCore.TUC.Core.Operations.Acquirer.ManageOnboarding _AcqOnboarding = new HCore.TUC.Core.Operations.Acquirer.ManageOnboarding();
                //_AcqOnboarding.ProcessOnboardMerchants();
                //_AcqOnboarding.ProcessCustomerOnboarding();
            }
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test && HCoreConstant.HostName != "localhost")
                {
                    //HCore.TUC.Core.Operations.Operations.ManageOperations _ManageOperations = new HCore.TUC.Core.Operations.Operations.ManageOperations();
                    //_ManageOperations.AddTestTransaction();
                }
            }
        }
    }
    public class JOB_24_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Delay(0);

            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                //ManageSubscription _ManageSubscription = new ManageSubscription();
                //_ManageSubscription.SubscriptionRemainder();

                //HCore.TUC.Plugins.Loan.ManageDisbursement _ManageDisbursement = new HCore.TUC.Plugins.Loan.ManageDisbursement();
                //_ManageDisbursement.CreditLoanAmount();
                //_ManageDisbursement.UpdateRepaymentStatus();
                //_ManageDisbursement.UpdateRepaymentStatus1();
                //_ManageDisbursement.CheckOverdueRepayments();
            }
        }
    }


    public class JOB_23_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Delay(0);
            //await HCore.Bot.Helper.BotHelper.SaveLogAsync("23HOUR-CRON-HIT", HCoreHelper.GetGMTDateTime().ToString());
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                //HCore.Bot.ManageBot _ManageBot = new HCore.Bot.ManageBot();
                //_ManageBot.UpdateTerminalActivityStatus();
                //_ManageBot.ProcessAllTerminalStatus();
            }
        }
    }

}

