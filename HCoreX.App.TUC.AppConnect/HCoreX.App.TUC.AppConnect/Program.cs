//==================================================================================
// FileName: Program.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCoreX.App.TUC.AppConnect.Core;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

var _WebApplicationBuilder = WebApplication.CreateBuilder(args);
_WebApplicationBuilder.Services.AddMvc(options => options.EnableEndpointRouting = false);
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson(opt =>
{
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson();
_WebApplicationBuilder.Services.Configure<KestrelServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});
_WebApplicationBuilder.Services.Configure<IISServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});

var _IApplicationBuilder = _WebApplicationBuilder.Build();
_IApplicationBuilder.Use(async (context, next) =>
{
    if (string.IsNullOrEmpty(HCoreConstant.HostName))
    {
        HCoreConstant.HostName = context.Request.Host.Host;
        HCoreXConfiguration.LoadConfiguration(context.Request.Host.Host);
        HCoreSystem.DataStore_InitializeSystem();
        HCore.Data.Store.HCoreDataStoreManager.DataStore_Start();
        if (HCoreConstant.HostName == "connect.thankucash.com" || HCoreConstant.HostName == "beta-connect.thankucash.com" || HCoreConstant.HostName == "webconnect.thankucash.com" || HCoreConstant.HostName == "appconnect.thankucash.com"
        || HCoreConstant.HostName == "connect.thankucash.dev" || HCoreConstant.HostName == "connect.thankucash.tech" || HCoreConstant.HostName == "connect.thankucash.co"
        || HCoreConstant.HostName == "webconnect.thankucash.dev" || HCoreConstant.HostName == "webconnect.thankucash.tech" || HCoreConstant.HostName == "webconnect.thankucash.co"
        || HCoreConstant.HostName == "appconnect.thankucash.dev" || HCoreConstant.HostName == "appconnect.thankucash.tech" || HCoreConstant.HostName == "appconnect.thankucash.co")
        {
            HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
            _ManageCore.StartMadDealsStorage();
        }
        if (HCoreConstant.HostEnvironment != HCoreConstant.HostEnvironmentType.Local)
        {
            HCoreXConfiguration.ShedularStartAsync().GetAwaiter().GetResult();
            HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
            _ManageCore.StartMadDealsStorage();
        }
    }
    await next.Invoke();
});
_IApplicationBuilder.UseCors(_Builder => _Builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
_IApplicationBuilder.UseRouting();
_IApplicationBuilder.HCoreXAuth();
_IApplicationBuilder.UseMvc();
_IApplicationBuilder.Run();