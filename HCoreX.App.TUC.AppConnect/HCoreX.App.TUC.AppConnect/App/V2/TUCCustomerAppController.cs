//==================================================================================
// FileName: TUCCustomerAppController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for customer app functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tuccustomer/[action]")]
    public class TUCCustomerAppController
    {
        ManageCustomerApp _ManageCustomerApp;

        /// <summary>
        /// Gets the nearby store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getnearbystores")]
        public object GetNearbyStore([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerApp = new ManageCustomerApp();
            OResponse _Response = _ManageCustomerApp.GetNearbyStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


    }
}
