//==================================================================================
// FileName: TUCPayController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for payment functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucpay/[action]")]
    public class TUCPayController
    {

        ManageTucPay _ManageTucPay;
        /// <summary>
        /// Buy point initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypoint_initizalize")]
        public object BuyPoint_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OTucPay.BuyPoint.Initilize.Request _Request = JsonConvert.DeserializeObject<OTucPay.BuyPoint.Initilize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTucPay = new ManageTucPay();
            OResponse _Response = _ManageTucPay.BuyPoint_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Buy point confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypoint_confirm")]
        public object BuyPoint_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OTucPay.BuyPoint.Confirm.Request _Request = JsonConvert.DeserializeObject<OTucPay.BuyPoint.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTucPay = new ManageTucPay();
            OResponse _Response = _ManageTucPay.BuyPoint_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Buy point cancel.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypoint_cancel")]
        public object BuyPoint_Cancel([FromBody] OAuth.Request _OAuthRequest)
        {
            OTucPay.BuyPoint.Confirm.Request _Request = JsonConvert.DeserializeObject<OTucPay.BuyPoint.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTucPay = new ManageTucPay();
            OResponse _Response = _ManageTucPay.BuyPoint_Cancel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
