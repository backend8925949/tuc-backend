//==================================================================================
// FileName: ThankULeadController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for lead functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucashlead/[action]")]
    public class ThankULeadController : Controller
    {
        ManageLead _ManageLead;

        /// <summary>
        /// Shuffles the leads.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        [HttpPost]
        [ActionName("shuffleleads")]
        public void ShuffleLeads([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            _ManageLead = new ManageLead();
            _ManageLead.ShuffleLeads();
            #endregion
        }

        /// <summary>
        /// Gets the lead overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getleadoverview")]
        public OAuth.Request GetLeadOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLead.Request _Request = JsonConvert.DeserializeObject<OLead.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLeadOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Creates the lead.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("createlead")]
        public OAuth.Request CreateLead([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLead.Request _Request = JsonConvert.DeserializeObject<OLead.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.CreateLead(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the lead status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateleadstatus")]
        public OAuth.Request UpdateLeadStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLead.Request _Request = JsonConvert.DeserializeObject<OLead.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.UpdateLeadStatus(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the lead.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getlead")]
        public OAuth.Request GetLead([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLead.Request _Request = JsonConvert.DeserializeObject<OLead.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLead(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the leads.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getleads")]
        public OAuth.Request GetLeads([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLead(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the future leads.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getfutureleads")]
        public OAuth.Request GetFutureLeads([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetFutureLead(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the lead activity.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getleadactivity")]
        public OAuth.Request GetLeadActivity([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLeadActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the lead overview history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getleadoverviewhistory")]
        public OAuth.Request GetLeadOverviewHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLeadOverviewHistory(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Saves the lead group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveleadgroup")]
        public OAuth.Request SaveLeadGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLeadGroup.Request _Request = JsonConvert.DeserializeObject<OLeadGroup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.SaveLeadGroup(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the lead group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateleadgroup")]
        public OAuth.Request UpdateLeadGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OLeadGroup.Request _Request = JsonConvert.DeserializeObject<OLeadGroup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.UpdateLeadGroup(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the lead group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getleadgroups")]
        public OAuth.Request GetLeadGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLead = new ManageLead();
            OResponse _Response = _ManageLead.GetLeadGroup(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
