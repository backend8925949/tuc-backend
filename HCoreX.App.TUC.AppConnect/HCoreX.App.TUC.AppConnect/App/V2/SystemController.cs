//==================================================================================
// FileName: SystemController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data.Ussd;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static HCore.CoreConstant;

namespace HCore.Api.App.V2
{

    [Produces("application/json")]
    [Route("api/v2/system/[action]")]
    public class SystemController : Controller
    {
        ManageCoreOperations _ManageCoreOperations;
        ManageCoreUserOperations _ManageCoreUserOperations;
        ManageCoreVerification _ManageCoreVerification;
        ManageCoreDevice _ManageCoreDevice;
        Operations.ManageConfiguration _ManageConfiguration;


        /// <summary>
        /// Checks the application version.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("appversioncheck")]
        public OAuth.Request CheckAppVersion([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            AppVersionCheck.Request _Request = JsonConvert.DeserializeObject<AppVersionCheck.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreDevice = new ManageCoreDevice();
            OResponse _Response = _ManageCoreDevice.CheckAppVersion(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Connects the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("connectapp")]
        public OAuth.Request ConnectApp([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ODevice.Request _Request = JsonConvert.DeserializeObject<ODevice.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreDevice = new ManageCoreDevice();
            OResponse _Response = _ManageCoreDevice.ConnectApp(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Connects the device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("connectdevice")]
        public OAuth.Request ConnectDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ODevice.Request _Request = JsonConvert.DeserializeObject<ODevice.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreDevice = new ManageCoreDevice();
            OResponse _Response = _ManageCoreDevice.ConnectDevice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the device notification URL.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatedevicenotificationurl")]
        public OAuth.Request UpdateDeviceNotificationUrl([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ODevice.Request _Request = JsonConvert.DeserializeObject<ODevice.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreDevice = new ManageCoreDevice();
            OResponse _Response = _ManageCoreDevice.UpdateDeviceNotificationUrl(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Requests the otp.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("requestotp")]
        public OAuth.Request RequestOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.Request _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.RequestOtp(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Verifies the otp.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("verifyotp")]
        public OAuth.Request VerifyOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.RequestVerify _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.RequestVerify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.VerifyOtp(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Requests the otp country.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("requestotpcon")]
        public OAuth.Request RequestOtpCountry([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.Request _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.RequestOtpCountry(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Verifies the otp country.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("verifyotpcon")]
        public OAuth.Request VerifyOtpCountry([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.RequestVerify _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.RequestVerify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.VerifyOtpCountry(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Requests the otp country v2.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("requestotpconv2")]
        public OAuth.Request RequestOtpCountryV2([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.Request _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.RequestOtpCountryV2(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Verifies the otp country v2.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("verifyotpconv2")]
        public OAuth.Request VerifyOtpCountryV2([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.RequestVerify _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.RequestVerify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.VerifyOtpCountryV2(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Saves the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecorehelper")]
        public OAuth.Request SaveCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.SaveCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecorecommon")]
        public OAuth.Request SaveCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.SaveCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecoreparameter")]
        public OAuth.Request SaveCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.SaveCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecorehelper")]
        public OAuth.Request UpdateCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.UpdateCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecorecommon")]
        public OAuth.Request UpdateCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.UpdateCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecoreparameter")]
        public OAuth.Request UpdateCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.UpdateCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Deletes the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecorehelper")]
        public OAuth.Request DeleteCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.DeleteCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecorecommon")]
        public OAuth.Request DeleteCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.DeleteCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecoreparameter")]
        public OAuth.Request DeleteCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.DeleteCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorehelper")]
        public OAuth.Request GetCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core helpers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorehelpers")]
        public OAuth.Request GetCoreHelpers([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the status list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstatuslist")]
        public OAuth.Request GetStatusList([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetStatusList(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core helperslite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorehelperslite")]
        public OAuth.Request GetCoreHelperslite([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreHelperLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorecommon")]
        public OAuth.Request GetCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core commons.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorecommons")]
        public OAuth.Request GetCoreCommons([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core common lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorecommonslite")]
        public OAuth.Request GetCoreCommonLite([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreCommonLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreparameter")]
        public OAuth.Request GetCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core parameters.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreparameters")]
        public OAuth.Request GetCoreParameters([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorelog")]
        public OAuth.Request GetCoreLog([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreLog _Request = JsonConvert.DeserializeObject<OCoreLog>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreLog(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core logs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorelogs")]
        public OAuth.Request GetCoreLogs([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreLog(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core usage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreusage")]
        public OAuth.Request GetCoreUsage([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreUsage.Manage _Request = JsonConvert.DeserializeObject<OCoreUsage.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreUsage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core usages.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreusages")]
        public OAuth.Request GetCoreUsages([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreUsage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstorage")]
        public OAuth.Request GetStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OStorage.Request _Request = JsonConvert.DeserializeObject<OStorage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetStorage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storages.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstorages")]
        public OAuth.Request GetStorages([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetStorage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreverification")]
        public OAuth.Request GetCoreVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerification.Request _Request = JsonConvert.DeserializeObject<OCoreVerification.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreVerification(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the core verifications.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreverifications")]
        public OAuth.Request GetCoreVerifications([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetCoreVerification(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccount")]
        public OAuth.Request GetUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccounts")]
        public OAuth.Request GetUserAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user account lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccountslite")]
        public OAuth.Request GetUserAccountLite([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserAccountLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user account owner.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccountowner")]
        public OAuth.Request GetUserAccountOwner([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccountOwner.Manage _Request = JsonConvert.DeserializeObject<OUserAccountOwner.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserAccountOwner(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user account owners.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccountowners")]
        public OAuth.Request GetUserAccountOwners([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserAccountOwner(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransaction")]
        public OAuth.Request GetTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTransaction.Manage _Request = JsonConvert.DeserializeObject<OTransaction.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransactions")]
        public OAuth.Request GetTransactions([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user activity.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseractivity")]
        public OAuth.Request GetUserActivity([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserActivity.Manage _Request = JsonConvert.DeserializeObject<OUserActivity.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user activities.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseractivities")]
        public OAuth.Request GetUserActivities([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserdevice")]
        public OAuth.Request GetUserDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserActivity.Manage _Request = JsonConvert.DeserializeObject<OUserActivity.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user devices.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserdevices")]
        public OAuth.Request GetUserDevices([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user session.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getusersession")]
        public OAuth.Request GetUserSession([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserSession.Manage _Request = JsonConvert.DeserializeObject<OUserSession.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserSession(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user sessions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getusersessions")]
        public OAuth.Request GetUserSessions([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserSession(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserparameter")]
        public OAuth.Request GetUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user parameters.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserparameters")]
        public OAuth.Request GetUserParameters([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user location.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserlocation")]
        public OAuth.Request GetUserLocation([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserLocation.Manage _Request = JsonConvert.DeserializeObject<OUserLocation.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserLocation(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user locations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserlocations")]
        public OAuth.Request GetUserLocations([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserLocation(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserinvoice")]
        public OAuth.Request GetUserInvoice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserInvoice.Manage _Request = JsonConvert.DeserializeObject<OUserInvoice.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the user invoices.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserinvoices")]
        public OAuth.Request GetUserInvoices([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetUserInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Registers the specified o authentication request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("register")]
        public OAuth.Request Register([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.SaveUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveuseraccount")]
        public OAuth.Request SaveUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.SaveUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccount")]
        public OAuth.Request UpdateUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteuseraccount")]
        public OAuth.Request DeleteUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.DeleteUserAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the user password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserpassword")]
        public OAuth.Request UpdateUserPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserPassword(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the user access pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccesspin")]
        public OAuth.Request UpdateUserAccessPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserAccessPin(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Resets the user access pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("resetuseraccesspin")]
        public OAuth.Request ResetUserAccessPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccount.Request _Request = JsonConvert.DeserializeObject<OUserAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.ResetUserAccessPin(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveuserparameter")]
        public OAuth.Request SaveUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.SaveUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserparameter")]
        public OAuth.Request UpdateUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user session.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateusersession")]
        public OAuth.Request UpdateUserSession([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserSession.Manage _Request = JsonConvert.DeserializeObject<OUserSession.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserSession(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserdevice")]
        public OAuth.Request UpdateUserDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserDevice.Manage _Request = JsonConvert.DeserializeObject<OUserDevice.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.UpdateUserDevice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Deletes the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteuserparameter")]
        public OAuth.Request DeleteUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreUserOperations = new ManageCoreUserOperations();
            OResponse _Response = _ManageCoreUserOperations.DeleteUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// The manage core transaction
        /// </summary>
        ManageCoreTransaction _ManageCoreTransaction;

        /// <summary>
        /// Gets the user balance history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserbalancehistory")]
        public OAuth.Request GetUserBalanceHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreTransaction = new ManageCoreTransaction();
            OResponse _Response = _ManageCoreTransaction.GetUserBalanceHistory(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Refreshes the user balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("refreshuserbalance")]
        public OAuth.Request RefreshUserBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OBalance.Request _Request = JsonConvert.DeserializeObject<OBalance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreTransaction = new ManageCoreTransaction();
            OResponse _Response = _ManageCoreTransaction.RefreshUserBalance(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        /// Configuration Manager
        [HttpPost]
        [ActionName("getconfiguration")]
        public OAuth.Request GetConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OSystemConfiguration.Request _Request = JsonConvert.DeserializeObject<OSystemConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Saves the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveconfiguration")]
        public OAuth.Request SaveConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateconfiguration")]
        public OAuth.Request UpdateConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.UpdateConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteconfiguration")]
        public OAuth.Request DeleteConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.DeleteConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the configuration value.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveconfigurationvalue")]
        public OAuth.Request SaveConfigurationValue([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Saves the type of the configuration account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveconfigurationaccounttype")]
        public OAuth.Request SaveConfigurationAccountType([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfigurationAccountType(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the type of the configuration account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteconfigurationaccounttype")]
        public OAuth.Request DeleteConfigurationAccountType([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Save _Request = JsonConvert.DeserializeObject<OConfigurationManager.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.DeleteConfigurationAccountType(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the configuration details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getconfigurationdetails")]
        public OAuth.Request GetConfigurationDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OConfigurationManager.Request _Request = JsonConvert.DeserializeObject<OConfigurationManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the configurations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getconfigurations")]
        public OAuth.Request GetConfigurations([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the configuration values.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getconfigurationvalues")]
        public OAuth.Request GetConfigurationValues([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the configuration account types.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getconfigurationaccountypes")]
        public OAuth.Request GetConfigurationAccountTypes([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfigurationAccountType(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        public class OVerifyRequest
        {
            public class Request
            {
                public string? AppSessionId { get; set; }
                public string? MobileNumber { get; set; }
                public string? DeviceId { get; set; }
            }
        }
        public class OConfirmVerifyRequest
        {
            public class Request
            {
                public string? RequestKey { get; set; }
                public string? MobileNumber { get; set; }
            }
            public class Response
            {
                public bool IsVerified { get; set; }
                public DateTime Verificationtime { get; set; }
            }
        }

        HCoreContextUssd _HCoreContextUssd;
        OResponse _OResponse;
        ussdverification _ussdverification;
        /// <summary>
        /// Requests the verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("requestotpussd")]
        [Produces("application/json")]
        public object RequestVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            _OResponse = new OResponse();
            try
            {
                OVerifyRequest.Request _Request = JsonConvert.DeserializeObject<OVerifyRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
                // _Request
                using (_HCoreContextUssd = new HCoreContextUssd())
                {
                    if (string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _OResponse.Status = "Error";
                        _OResponse.Message = "Invalid mobile number";
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                    if (string.IsNullOrEmpty(_Request.AppSessionId))
                    {
                        _OResponse.Status = "Error";
                        _OResponse.Message = "App session id required";
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                    if (string.IsNullOrEmpty(_Request.DeviceId))
                    {
                        _OResponse.Status = "Error";
                        _OResponse.Message = "Device id required";
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                    else
                    {
                        string FormatMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                        string VerificationCode = HCoreHelper.GenerateRandomNumber(4);
                        string UssdCode = "*347*100*" + VerificationCode + "#";
                        _ussdverification = new ussdverification();
                        _ussdverification.Guid = HCoreHelper.GenerateGuid();
                        _ussdverification.SessionCode = VerificationCode;
                        _ussdverification.MobileNumber = FormatMobileNumber;
                        _ussdverification.CreateDate = DateTime.UtcNow;
                        _ussdverification.StatusId = 1;
                        _ussdverification.AppSessionId = _Request.AppSessionId;
                        _ussdverification.DeviceId = _Request.DeviceId;
                        _HCoreContextUssd.ussdverification.Add(_ussdverification);
                        _HCoreContextUssd.SaveChanges();
                        _OResponse.Status = "Success";
                        _OResponse.Message = "To verify your phone number, please dial " + UssdCode + " on the phone with this number.";
                        _OResponse.Result = new
                        {
                            UssdCode = UssdCode,
                            RequestKey = _ussdverification.Guid,
                            MobileNumber = FormatMobileNumber,
                            SessionCode = VerificationCode,
                        };
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                }
            }
            catch (System.Exception e)
            {
                _OResponse.Status = "Error";
                _OResponse.Message = "Unable to process your request. Please try after some time";
                return HCoreAuth.Auth_Response(_OResponse);
            }

        }
        /// <summary>
        /// Checks the verfication.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifyotpussd")]
        [Produces("application/json")]
        public object CheckVerfication([FromBody] OAuth.Request _OAuthRequest)
        {
            OConfirmVerifyRequest.Request _Request = JsonConvert.DeserializeObject<OConfirmVerifyRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _OResponse = new OResponse();
            try
            {
                using (_HCoreContextUssd = new HCoreContextUssd())
                {
                    if (string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _OResponse.Status = "Error";
                        _OResponse.Message = "Invalid mobile number";
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                    if (string.IsNullOrEmpty(_Request.RequestKey))
                    {
                        _OResponse.Status = "Error";
                        _OResponse.Message = "Request key id required";
                        return HCoreAuth.Auth_Response(_OResponse);
                    }
                    else
                    {

                        string FormatMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                        if (FormatMobileNumber == "2349009009000")
                        {
                            _OResponse.Status = "Success";
                            _OResponse.Message = "Status loaded";
                            _OResponse.Result = new
                            {
                                StatusId = 2
                            };
                            return HCoreAuth.Auth_Response(_OResponse);
                        }
                        var _VRequest = _HCoreContextUssd.ussdverification.Where(x => x.Guid == _Request.RequestKey && x.MobileNumber == FormatMobileNumber).
                            Select(x => new
                            {
                                StatusId = x.StatusId,
                            }).FirstOrDefault();
                        if (_VRequest != null)
                        {
                            if (_VRequest.StatusId == 2)
                            {
                                _OResponse.Status = "Success";
                                _OResponse.Message = "Status loaded";
                                _OResponse.Result = new
                                {
                                    StatusId = _VRequest.StatusId
                                };
                                return HCoreAuth.Auth_Response(_OResponse);
                            }
                            else
                            {
                                _OResponse.Status = "Error";
                                _OResponse.Message = "Mobile number not verified";
                                return HCoreAuth.Auth_Response(_OResponse);
                            }

                        }
                        else
                        {
                            _OResponse.Status = "Error";
                            _OResponse.Message = "Details not found. Please start verification again";
                            return HCoreAuth.Auth_Response(_OResponse);
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                _OResponse.Status = "Error";
                _OResponse.Message = "Unable to process your request. Please try after some time";
                return HCoreAuth.Auth_Response(_OResponse);
            }
        }
        /// <summary>
        /// Gets the role list for user.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrolelistforuser")]
        public OAuth.Request GetRoleListForUser([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreOperations = new ManageCoreOperations();
            OResponse _Response = _ManageCoreOperations.GetRoleListForUser(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Requests the otp over voice call.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("requestotpvoicecall")]
        public OAuth.Request RequestOtpVoiceCall([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreVerificationManager.Request _Request = JsonConvert.DeserializeObject<OCoreVerificationManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreVerification = new ManageCoreVerification();
            OResponse _Response = _ManageCoreVerification.RequestOtpVoiceCall(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
