//==================================================================================
// FileName: ThankUCashRmController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for thank u cash reporting manager functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/rmmanager/[action]")]
    public class ThankUCashRmController
    {
        ManageRm _ManageRm;
        /// <summary>
        /// Assigns the store to rm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("assignstoretorm")]
        public OAuth.Request AssignStoreToRm([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ORmManager.Request _Request = JsonConvert.DeserializeObject<ORmManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.AssignStoreToRm(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Revokes the rm store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("revokermstore")]
        public OAuth.Request RevokeRmStore([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ORmManager.Request _Request = JsonConvert.DeserializeObject<ORmManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.RevokeRmStore(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Gets the rm terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrmterminals")]
        public OAuth.Request GetRmTerminals([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.GetRmTerminals(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the rm stores.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrmstores")]
        public OAuth.Request GetRmStores([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.GetRmStores(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
