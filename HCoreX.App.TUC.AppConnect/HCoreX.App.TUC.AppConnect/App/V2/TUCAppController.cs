//==================================================================================
// FileName: TUCAppController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for app and it's features functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucapp/[action]")]
    public class TUCAppController
    {
        ManageApp _ManageApp;
        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getappconfiguration")]
        public object GetAppConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OApp.OAppConfiguration.Request _Request = JsonConvert.DeserializeObject<OApp.OAppConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetAppConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Confirms the donation.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("confirmdonation")]
        public object ConfirmDonation([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OApp.OPaymentDetails _Request = JsonConvert.DeserializeObject<OApp.OPaymentDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.ConfirmDonation(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the merchant categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantcategories")]
        public object GetMerchantCategories([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetMerchantCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the stores.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstores")]
        public object GetStores([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetStores(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettransactions")]
        public object GetTransactions([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetTransactions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the payments configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpaymentsconfiguration")]
        public object GetPaymentsConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetPaymentsConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Payments the confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("confirmpayment")]
        public object PaymentConfirm([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OApp.OPaymentDetails _Request = JsonConvert.DeserializeObject<OApp.OPaymentDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.PaymentConfirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Payments the account verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("paymentaccountverify")]
        public object PaymentAccountVerify([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OApp.OPaymentDetails _Request = JsonConvert.DeserializeObject<OApp.OPaymentDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.PaymentAccountVerify(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Payments the charge.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("paymentcharge")]
        public object PaymentCharge([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OApp.OPaymentCharge _Request = JsonConvert.DeserializeObject<OApp.OPaymentCharge>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.PaymentCharge(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the billers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbillers")]
        public object GetBillers([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetBillers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the biller packages.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbillerpackages")]
        public object GetBillerPackages([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetBillerPackages(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }



        /// <summary>
        /// Updates the donar configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("udpatedonarconfiguration")]
        public object UpdateDonarConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.RelifDetails _Request = JsonConvert.DeserializeObject<OAppManager.RelifDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.UpdateDonarConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the donar configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdonarconfiguration")]
        public object GetDonarConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.RelifDetails _Request = JsonConvert.DeserializeObject<OAppManager.RelifDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetDonarConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the receiver configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("udpatereceiverconfiguration")]
        public object UpdateReceiverConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.RelifDetails _Request = JsonConvert.DeserializeObject<OAppManager.RelifDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.UpdateReceiverConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the receiver configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreceiverconfiguration")]
        public object GetReceiverConfiguration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.RelifDetails _Request = JsonConvert.DeserializeObject<OAppManager.RelifDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetReceiverConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Saves the donation parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedonationparameter")]
        public object SaveDonationParameter([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.DonationParameter.Request _Request = JsonConvert.DeserializeObject<OAppManager.DonationParameter.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.SaveDonationParameter(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the donation parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedonationparameter")]
        public object UpdateDonationParameter([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.DonationParameter.Request _Request = JsonConvert.DeserializeObject<OAppManager.DonationParameter.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.UpdateDonationParameter(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Deletes the donation parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletedonationparameter")]
        public object DeleteDonationParameter([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OAppManager.DonationParameter.Request _Request = JsonConvert.DeserializeObject<OAppManager.DonationParameter.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.DeleteDonationParameter(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the donation parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdonationparameters")]
        public object GetDonationParameter([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetDonationParameter(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Ges the donations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdonations")]
        public object GeDonations([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GeDonations(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the receivers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreceverrequests")]
        public object GetReceivers([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetReceivers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

    }
}
