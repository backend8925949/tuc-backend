//==================================================================================
// FileName: TUCGiftPointsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for giftpoints functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucgiftpoints/[action]")]
    public class TUCGiftPointsController
    {
        ManageGiftPoints _ManageGiftPoints;
        /// <summary>
        /// Gets the gift points balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getgiftpointsbalance")]
        public OAuth.Request GetGiftPointsBalance([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGiftPoints.BalanceRequest _Request = JsonConvert.DeserializeObject<OGiftPoints.BalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftPoints = new ManageGiftPoints();
            OResponse _Response = _ManageGiftPoints.GetGiftPointsBalance(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Credits the gift points.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("creditgiftpoints")]
        public OAuth.Request CreditGiftPoints([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGiftPoints.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OGiftPoints.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftPoints = new ManageGiftPoints();
            OResponse _Response = _ManageGiftPoints.CreditGiftPoints(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Credits the gift points to customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("creditgiftpointstocustomer")]
        public OAuth.Request CreditGiftPointsToCustomer([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGiftPoints.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OGiftPoints.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftPoints = new ManageGiftPoints();
            OResponse _Response = _ManageGiftPoints.CreditGiftPointsToCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Credits the gift points to customer web.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("creditgiftpointstocustomerweb")]
        public OAuth.Request CreditGiftPointsToCustomerWeb([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGiftPoints.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OGiftPoints.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftPoints = new ManageGiftPoints();
            OResponse _Response = _ManageGiftPoints.CreditGiftPointsToCustomerWeb(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
