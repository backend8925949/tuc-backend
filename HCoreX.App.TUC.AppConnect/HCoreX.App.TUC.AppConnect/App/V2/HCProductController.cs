//==================================================================================
// FileName: HCProductController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for product functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using Newtonsoft.Json;
using HCore.Product;
using HCore.Product.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/hcproduct/[action]")]
    public class HCProductController
    {
        ManageNinja _ManageNinja;
        ManageProduct _ManageProduct;
        ManageCategory _ManageCategory;
        HCore.Product.ManageConfiguration _ManageConfiguration;



        /// <summary>
        /// Updates the ninja reg bank details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateninjaregbankdetails")]
        public object UpdateNinjaRegBankDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.Request _Request = JsonConvert.DeserializeObject<ONinja.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.UpdateNinjaRegBankDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the ninja registration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaregistration")]
        public object GetNinjaRegistration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.Request _Request = JsonConvert.DeserializeObject<ONinja.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.GetNinjaRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the ninja registration profile.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateninjaregistrationprofile")]
        public object UpdateNinjaRegistrationProfile([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.Request _Request = JsonConvert.DeserializeObject<ONinja.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.UpdateNinjaRegistrationProfile(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the ninja balance transfer ninja amount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjabalance")]
        public object GetNinjaBalanceTransferNinjaAmount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.ONinjaTransferRequest _Request = JsonConvert.DeserializeObject<ONinja.ONinjaTransferRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.GetNinjaBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Transfers the ninja amount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("transferninjaamount")]
        public object TransferNinjaAmount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.ONinjaTransferRequest _Request = JsonConvert.DeserializeObject<ONinja.ONinjaTransferRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.TransferNinjaAmount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Saves the ninja registration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveninjaregistration")]
        public object SaveNinjaRegistration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.Request _Request = JsonConvert.DeserializeObject<ONinja.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.SaveNinjaRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the ninja registration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateninjaregistration")]
        public object UpdateNinjaRegistration([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            ONinja.Request _Request = JsonConvert.DeserializeObject<ONinja.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.UpdateNinjaRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the registrations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaregistrations")]
        public object GetRegistrations([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.GetRegistrations(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the ninja transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjatransactions")]
        public object GetNinjaTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.GetNinjaTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the ninja list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaslist")]
        public object GetNinjaList([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageNinja = new ManageNinja();
            OResponse _Response = _ManageNinja.GetNinjaList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the product balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveproduct")]
        public object GetProductBalance([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.SaveProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateproduct")]
        public object UpdateProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.UpdateProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        //[HttpPost]
        //[ActionName("updateproductstock")]
        //public object UpdateProductStock([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Update.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageProduct = new ManageProduct();
        //    OResponse _Response = _ManageProduct.UpdateProductStock(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //    #endregion
        //}
        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteproduct")]
        public object DeleteProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.DeleteProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproduct")]
        public object GetProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproducts")]
        public object GetProducts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the product overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductoverview")]
        public object GetProductOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the product dealer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductdealers")]
        public object GetProductDealer([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductDealer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the product lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductslite")]
        public object GetProductLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductLite(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the store products.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstoreproducts")]
        public object GetStoreProducts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetStoreProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the store product varient.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstoreproductvarient")]
        public object GetStoreProductVarient([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarient.Request _Request = JsonConvert.DeserializeObject<OProductVarient.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetStoreProductVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        #region Category manager

        #endregion

        /// <summary>
        /// Saves the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecategory")]
        public object SaveCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecategory")]
        public object UpdateCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.UpdateCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecategory")]
        public object DeleteCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.DeleteCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategory")]
        public object GetCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }




        /// <summary>
        /// Saves the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savesubcategory")]
        public object SaveSubCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesubcategory")]
        public object UpdateSubCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.UpdateSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Deletes the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletesubcategory")]
        public object DeleteSubCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.DeleteSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubcategory")]
        public object GetSubCategory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the product sub categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubcategories")]
        public object GetProductSubCategories([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }





        /// <summary>
        /// The manage order
        /// </summary>
        ManageOrder _ManageOrder;
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorder")]
        public object GetOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the order overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorderoverview")]
        public object GetOrderOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetOrderOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Upates the order status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateorderstatus")]
        public object UpateOrderStatus([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.UpateOrderStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the order rating.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateorderrating")]
        public object UpdateOrderRating([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Rating _Request = JsonConvert.DeserializeObject<OOrder.Rating>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.UpdateOrderRating(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Cancels the order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("cancelorder")]
        public object CancelOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrderManager.Request _Request = JsonConvert.DeserializeObject<OOrderManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.CancelOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorders")]
        public object GetOrders([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the merchant orders.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantorders")]
        public object GetMerchantOrders([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetMerchantOrders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the merchant order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantorder")]
        public object GetMerchantOrder([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetMerchantOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the order activity.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorderactivities")]
        public object GetOrderActivity([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetOrderActivity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }



        ManageProductVarient _ManageProductVarient;

        /// <summary>
        /// Saves the varient stock.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savevarientstock")]
        public object SaveVarientStock([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarientStock.Request _Request = JsonConvert.DeserializeObject<OProductVarientStock.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.SaveVarientStock(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the varient stock.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatevarientstock")]
        public object UpdateVarientStock([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarientStock.Request _Request = JsonConvert.DeserializeObject<OProductVarientStock.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.UpdateVarientStock(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the varient stock item.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatevarientstockitem")]
        public object UpdateVarientStockItem([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarientStock.Request _Request = JsonConvert.DeserializeObject<OProductVarientStock.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.UpdateVarientStockItem(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Deletes the varient stock.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletevarientstock")]
        public object DeleteVarientStock([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarientStock.Request _Request = JsonConvert.DeserializeObject<OProductVarientStock.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.DeleteVarientStock(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the varient stock.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvarientstockdetails")]
        public object GetVarientStock([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarientStock.Request _Request = JsonConvert.DeserializeObject<OProductVarientStock.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.GetVarientStock(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the varient stocks.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvarientstock")]
        public object GetVarientStocks([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.GetVarientStock(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Saves the varient.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savevarient")]
        public object SaveVarient([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarient.Request _Request = JsonConvert.DeserializeObject<OProductVarient.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.SaveVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the varient.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatevarient")]
        public object UpdateVarient([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarient.Request _Request = JsonConvert.DeserializeObject<OProductVarient.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.UpdateVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Deletes the varient.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletevarient")]
        public object DeleteVarient([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarient.Request _Request = JsonConvert.DeserializeObject<OProductVarient.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.DeleteVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the varient.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvarient")]
        public object GetVarient([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductVarient.Request _Request = JsonConvert.DeserializeObject<OProductVarient.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.GetVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the varients.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvarients")]
        public object GetVarients([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProductVarient = new ManageProductVarient();
            OResponse _Response = _ManageProductVarient.GetVarient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }



        /// <summary>
        /// Initializes the order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("initializeorder")]
        public object InitializeOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrderManager.NewOrder  _Request = JsonConvert.DeserializeObject<OOrderManager.NewOrder>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.InitializeOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Confirms the order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("confirmorder")]
        public object ConfirmOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrderManager.Request _Request = JsonConvert.DeserializeObject<OOrderManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.ConfirmOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the customer order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerorder")]
        public object GetCustomerOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrderManager.Request _Request = JsonConvert.DeserializeObject<OOrderManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetCustomerOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerorders")]
        public object GetCustomerOrders([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetCustomerOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Ninjas reject order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ninjarejectorder")]
        public object NinjaRejectOrder([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.NinjaRejectOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Ninjas order delivery failed.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ninjaorderdeliveryfailed")]
        public object NinjaOrderDeliveryFailed([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.NinjaOrderDeliveryFailed(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Ninjas order delivered.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ninjaorderdelivered")]
        public object NinjaOrderDelivered([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrder.Request _Request = JsonConvert.DeserializeObject<OOrder.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.NinjaOrderDelivered(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the ninja order summary.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaordersummary")]
        public object GetNinjaOrderSummary([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetNinjaOrderSummary(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the ninja order.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaorder")]
        public object GetNinjaOrder([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOrderManager.Request _Request = JsonConvert.DeserializeObject<OOrderManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetNinjaOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Gets the ninja orders.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getninjaorders")]
        public object GetNinjaOrders([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrder = new ManageOrder();
            OResponse _Response = _ManageOrder.GetNinjaOrders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Saves the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveconfiguration")]
        public object SaveConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTUCConfiguration.Request _Request = JsonConvert.DeserializeObject<OTUCConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateconfiguration")]
        public object UpdateConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTUCConfiguration.Request _Request = JsonConvert.DeserializeObject<OTUCConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.UpdateConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }


        /// <summary>
        /// Saves the configuration value.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveconfigurationvalue")]
        public object SaveConfigurationValue([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTUCConfigurationValue.Request _Request = JsonConvert.DeserializeObject<OTUCConfigurationValue.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurations")]
        public object GetConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the configuration history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurationchangehistory")]
        public object GetConfigurationHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfigurationHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Saves the account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveaccountconfiguration")]
        public object SaveAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTUCConfigurationValue.Request _Request = JsonConvert.DeserializeObject<OTUCConfigurationValue.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the dealer configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealerconfigurations")]
        public object GetDealerConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetDealerConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountconfigurations")]
        public object GetAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new Product.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
    }
}