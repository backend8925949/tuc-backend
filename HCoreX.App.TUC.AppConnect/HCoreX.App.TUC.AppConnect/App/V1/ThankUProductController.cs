//==================================================================================
// FileName: ThankUProductController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V1
{
    [Produces("application/json")]
    [Route("api/v1/thankuproduct/[action]")]
    public class ThankUProductController : Controller
    {
        ManageProduct _ManageProduct;

        /// <summary>
        /// Saves the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveproduct")]
        public OAuth.Request SaveProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.SaveProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Saves the product batch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveproducbatch")]
        public OAuth.Request SaveProductBatch([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.SaveProductBatch(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateproduct")]
        public OAuth.Request UpdateProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.UpdateProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the product batch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateproductbatch")]
        public OAuth.Request UpdateProductBatch([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.UpdateProductBatch(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the product status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateproductstatus")]
        public OAuth.Request UpdateProductStatus([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.UpdateProductStatus(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Updates the product batch status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateproductbatchstatus")]
        public OAuth.Request UpdateProductBatchStatus([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.UpdateProductBatchStatus(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteproduct")]
        public OAuth.Request DeleteProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.DeleteProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the product batch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteproductbatch")]
        public OAuth.Request DeleteProductBatch([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.DeleteProductBatch(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }




        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproduct")]
        public OAuth.Request GetProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the product batch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproductbatch")]
        public OAuth.Request GetProductBatch([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the product batches.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproductbatches")]
        public OAuth.Request GetProductBatches([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductBatch(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproducts")]
        public OAuth.Request GetProducts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProduct(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Gets the product code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproductcode")]
        public OAuth.Request GetProductCode([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProductCode.Request _Request = JsonConvert.DeserializeObject<OProductCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductCode(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the product codes.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getproductcodes")]
        public OAuth.Request GetProductCodes([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetProductCode(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the batch codes.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getbatchcodes")]
        public OAuth.Request GetBatchCodes([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OProduct.Request _Request = JsonConvert.DeserializeObject<OProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            OResponse _Response = _ManageProduct.GetBatchCodes(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


    }
}
