//==================================================================================
// FileName: ThankUController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.v1
{
    [Produces("application/json")]
    [Route("api/v1/thanku/[action]")]
    public class ThankUController : Controller
    {
        #region  Reference
        //ManageCard _ManageCard;
        //ManageUserRewardInovice _ManageUserRewardInovice;
        ManageSettlement _ManageSettlement;
        //ManageThankUOperations _ManageThankUOperations;
        ManageAnalytics _ManageAnalytics;
        ManageSlider _ManageSlider;
        HCore.ThankU.ManageApp _ManageApp;
        ManageCashierApp _ManageCashierApp;
        //ManageUserOnboarding _ManageUserOnboarding;
        #endregion
        #region Manage Card
        /// <summary>
        /// Gets the slider images.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsliderimages")]
        public OAuth.Request GetSliderImages([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSlider = new ManageSlider();
            OResponse _Response = _ManageSlider.GetSliderImages(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        //[HttpPost]
        //[Route(RouteUrl.V1.System + "getmerchantcard")]
        //[Route(RouteUrl.V1.System + "getcard")]
        //[ActionName("getcard")]
        //public OAuth.Request GetCard([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCard.Details _Request = JsonConvert.DeserializeObject<OCard.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

        //    _ManageCard = new ManageCard();
        //    OResponse _Response = _ManageCard.GetCard(_Request);

        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[Route(RouteUrl.V1.System + "getmerchantcards")]
        //[Route(RouteUrl.V1.System + "getcards")]
        //[ActionName("getcards")]
        //public OAuth.Request GetCards([FromBody]OAuth.Request _OAuthRequest)
        //{


        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

        //    _ManageCard = new ManageCard();
        //    OResponse _Response = _ManageCard.GetCards(_Request);

        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion

        //}

        //[HttpPost]
        //[ActionName("getcardsoverview")]
        //public OAuth.Request GetCardsOverview([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCard.Overview _Request = JsonConvert.DeserializeObject<OCard.Overview>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCard = new ManageCard();
        //    OResponse _Response = _ManageCard.GetCardsOverview(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion

        //}

        //[HttpPost]
        //[ActionName("getblockedcards")]
        //public OAuth.Request GetBlockedCards([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCard.BlockedCard _Request = JsonConvert.DeserializeObject<OCard.BlockedCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCard = new ManageCard();
        //    OResponse _Response = _ManageCard.GetBlockedCards(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        #endregion

        //[HttpPost]
        //[ActionName("saverewardinvoice")]
        //public OAuth.Request SaveRewardInovice([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OUserRewardInovice _Request = JsonConvert.DeserializeObject<OUserRewardInovice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserRewardInovice = new ManageUserRewardInovice();
        //    OResponse _Response = _ManageUserRewardInovice.SaveUserRewardInovice(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion

        //}
        //[HttpPost]
        //[ActionName("updaterewardinvoice")]
        //public OAuth.Request UpdateUserRewardInovice([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OUserRewardInovice _Request = JsonConvert.DeserializeObject<OUserRewardInovice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserRewardInovice = new ManageUserRewardInovice();
        //    OResponse _Response = _ManageUserRewardInovice.UpdateUserRewardInovice(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion

        //}
        //[HttpPost]
        //[ActionName("getrewardinvoice")]
        //public OAuth.Request GetUserRewardInovice([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OUserRewardInovice _Request = JsonConvert.DeserializeObject<OUserRewardInovice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserRewardInovice = new ManageUserRewardInovice();
        //    OResponse _Response = _ManageUserRewardInovice.GetUserRewardInovice(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion

        //}
        //[HttpPost]
        //[ActionName("getrewardinvoices")]
        //public OAuth.Request GetUserRewardInovices([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserRewardInovice = new ManageUserRewardInovice();
        //    OResponse _Response = _ManageUserRewardInovice.GetUserRewardInovices(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("createsettlementinvoice")]
        //public OAuth.Request CreatePaymentSettlement([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response 
        //    OUserSettlement.Request _Request = JsonConvert.DeserializeObject<OUserSettlement.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageSettlement = new ManageSettlement();
        //    OResponse _Response = _ManageSettlement.CreatePaymentSettlement(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        /// <summary>
        /// Updates the payment invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatesettlementinvoice")]
        public OAuth.Request UpdatePaymentInvoice([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response 
            OUserSettlement.Request _Request = JsonConvert.DeserializeObject<OUserSettlement.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSettlement = new ManageSettlement();
            OResponse _Response = _ManageSettlement.UpdatePaymentInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }

        /// <summary>
        /// Gets the payment invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsettlementinvoice")]
        public OAuth.Request GetPaymentInvoice([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response 
            OUserSettlement.Request _Request = JsonConvert.DeserializeObject<OUserSettlement.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSettlement = new ManageSettlement();
            OResponse _Response = _ManageSettlement.GetPaymentInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }


        /// <summary>
        /// Gets the payment invoices.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsettlementinvoices")]
        public OAuth.Request GetPaymentInvoices([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response 
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSettlement = new ManageSettlement();
            OResponse _Response = _ManageSettlement.GetPaymentInvoices(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }



        /// <summary>
        /// Gets the user account overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.ThankU + "getadvertiseroverview")]
        [Route(RouteUrl.V1.ThankU + "getmerchantoverview")]
        [Route(RouteUrl.V1.ThankU + "getcarduseroverview")]
        [Route(RouteUrl.V1.ThankU + "getappuseroverview")]
        [Route(RouteUrl.V1.ThankU + "getcashieroverview")]
        [Route(RouteUrl.V1.ThankU + "getstoreoverview")]
        [Route(RouteUrl.V1.ThankU + "getuseraccountoverview")]
        [ActionName("getuseraccountoverview")]
        public OAuth.Request GetUserAccountOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            return HCoreAuth.Auth_Response(_ManageAnalytics.GetUserAccountOverview(_Request));
            #endregion
        }


        /// <summary>
        /// Gets the user analytics.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.ThankU + "getadvertiseranalytics")]
        [Route(RouteUrl.V1.ThankU + "getmerchantanalytics")]
        [Route(RouteUrl.V1.ThankU + "getappuseranalytics")]
        [Route(RouteUrl.V1.ThankU + "getstoreanalytics")]
        [Route(RouteUrl.V1.ThankU + "getcashieranalytics")]
        [Route(RouteUrl.V1.ThankU + "getcarduseranalytics")]
        [Route(RouteUrl.V1.ThankU + "getuseranalytics")]
        [ActionName("getuseranalytics")]
        public OAuth.Request GetUserAnalytics([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OChart.Request _Request = JsonConvert.DeserializeObject<OChart.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            return HCoreAuth.Auth_Response(_ManageAnalytics.GetUserAnalytics(_Request));
            #endregion
        }

        //[ActionName("onboarduser")]
        //public OAuth.Request OnBoardUser([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OMerchantOnboarding.Request _Request = JsonConvert.DeserializeObject<OMerchantOnboarding.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserOnboarding = new ManageUserOnboarding();
        //    return HCoreAuth.Auth_Response(_ManageUserOnboarding.OnBoardUser(_Request));
        //    #endregion
        //}
        //[ActionName("getonboarduserdetails")]
        //public OAuth.Request GetOnBoardUserDetails([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OMerchantOnboarding.Request _Request = JsonConvert.DeserializeObject<OMerchantOnboarding.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserOnboarding = new ManageUserOnboarding();
        //    return HCoreAuth.Auth_Response(_ManageUserOnboarding.GetOnBoardUserDetails(_Request));
        //    #endregion
        //}
        //[ActionName("updateonboarduserdetails")]
        //public OAuth.Request UpdateOnBoardUserDetails([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OMerchantOnboarding.Request _Request = JsonConvert.DeserializeObject<OMerchantOnboarding.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserOnboarding = new ManageUserOnboarding();
        //    return HCoreAuth.Auth_Response(_ManageUserOnboarding.UpdateOnBoardUserDetails(_Request));
        //    #endregion
        //}
        /// <summary>
        /// The manage user onboarding
        /// </summary>
        ManageUserOnboarding _ManageUserOnboarding;

        /// <summary>
        /// Saves the account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("saveaccountconfiguration")]
        public OAuth.Request SaveAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OAccountConfiguration _Request = JsonConvert.DeserializeObject<OAccountConfiguration>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserOnboarding = new ManageUserOnboarding();
            return HCoreAuth.Auth_Response(_ManageUserOnboarding.SaveAccountConfiguration(_Request));
            #endregion
        }

        //[ActionName("activateaccount")]
        //public OAuth.Request ActivateAccount([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OAccountConfiguration _Request = JsonConvert.DeserializeObject<OAccountConfiguration>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserOnboarding = new ManageUserOnboarding();
        //    return HCoreAuth.Auth_Response(_ManageUserOnboarding.ActivateAccount(_Request));
        //    #endregion
        //}

        /// <summary>
        /// Gets the user accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("getuseraccounts")]
        public OAuth.Request GetUserAccounts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new HCore.ThankU.ManageApp();
            return HCoreAuth.Auth_Response(_ManageApp.GetUserAccounts(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("getuseraccount")]
        public OAuth.Request GetUserAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OThankUCashApp.Request _Request = JsonConvert.DeserializeObject<OThankUCashApp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new HCore.ThankU.ManageApp();
            return HCoreAuth.Auth_Response(_ManageApp.GetUserAccount(_Request));
            #endregion
        }

        /// <summary>
        /// Connects the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("connectapp")]
        public OAuth.Request ConnectApp([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OThankUCashApp.Request _Request = JsonConvert.DeserializeObject<OThankUCashApp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new HCore.ThankU.ManageApp();
            return HCoreAuth.Auth_Response(_ManageApp.ConnectApp(_Request));
            #endregion
        }
        /// <summary>
        /// Connects the device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("connectdevice")]
        public OAuth.Request ConnectDevice([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OThankUCashApp.Request _Request = JsonConvert.DeserializeObject<OThankUCashApp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new HCore.ThankU.ManageApp();
            return HCoreAuth.Auth_Response(_ManageApp.ConnectDevice(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the settlements overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("getsettlementoverview")]
        public OAuth.Request GetSettlementsOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserSettlement.Request _Request = JsonConvert.DeserializeObject<OUserSettlement.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSettlement = new ManageSettlement();
            return HCoreAuth.Auth_Response(_ManageSettlement.GetSettlementsOverview(_Request));
            #endregion
        }

        /// <summary>
        /// The manage product
        /// </summary>
        ManageProduct _ManageProduct;
        /// <summary>
        /// Redeems the code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [ActionName("redeemproductcode")]
        public OAuth.Request RedeemCode([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OProductCode.Request _Request = JsonConvert.DeserializeObject<OProductCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProduct = new ManageProduct();
            return HCoreAuth.Auth_Response(_ManageProduct.RedeemCode(_Request));
            #endregion
        }


        /// <summary>
        /// Gets the cashier access.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcashieraccess")]
        public OAuth.Request GetCashierAccess([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCashierApp.Request _Request = JsonConvert.DeserializeObject<OCashierApp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashierApp = new ManageCashierApp();
            OResponse _Response = _ManageCashierApp.GetCashierAccess(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

    }
}
