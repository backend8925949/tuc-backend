//==================================================================================
// FileName: TUCMerchantController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for merchant functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.OpenApi;
using HCore.TUC.Core.Operations.OpenApi;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.OpenApi
{
    [Produces("application/json")]
    [Route("api/v3/op/l/[action]")]
    public class TUCMerchantController
    {
        ManageMerchant _ManageMerchant;
        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantlocations")]
        public object GetMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchant = new ManageMerchant();
            OResponse _Response = _ManageMerchant.GetMerchant(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

    }
}
