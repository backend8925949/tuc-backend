//==================================================================================
// FileName: TUCGiftCardController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for giftcard functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================


using HCore.Helper;
using HCore.TUC.Plugins.GiftCards;
using HCore.TUC.Plugins.GiftCards.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;

namespace HCore.Api.App.v3.Tuc.Plugins.GiftCards
{
    [Produces("application/json")]
    [Route("api/v3/plugins/gc/[action]")]
    public class TUCGiftCardController
    {
        ManageGiftCard _ManageGiftCard;
        /// <summary>
        /// Gets the wallet balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("createquickgiftcard")]
        public object GetWalletBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.QuickCard _Request = JsonConvert.DeserializeObject<OGiftCard.QuickCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.CreateGiftCard(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the gift card.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creategiftcard")]
        public object SaveGiftCard([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.CreateGiftCard _Request = JsonConvert.DeserializeObject<OGiftCard.CreateGiftCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.SaveGiftCard(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }
        /// <summary>
        /// Gets the gift card overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgiftcardoverview")]
        public object GetGiftCardOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the gift card list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgiftcards")]
        public object GetGiftCards([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCards(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the gift card purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgiftcardssaleshistory")]
        public object GetGiftCardPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the gift card redeem history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgiftcardsredeemhistory")]
        public object GetGiftCardRedeemHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardRedeemHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the home gift card overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gethomegiftcardoverview")]
        public object GetHomeGiftCardOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetHomeGiftCardOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the home gift card purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gethomegiftcardssaleshistory")]
        public object GetHomeGiftCardPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetHomeGiftCardPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getappuserbalance")]
        public object GetBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.Balance.Request _Request = JsonConvert.DeserializeObject<OGiftCard.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetBalance(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }
    }
}
