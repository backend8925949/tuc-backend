//==================================================================================
// FileName: BNPLMerchantController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI;
// using HCore.Integration.Evolve.AppAPI.Framework;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.Plugins.Loan.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/plugins/merchant/[action]")]
    [ApiController]
    public class BNPLMerchantController : ControllerBase
    {
        ManageMerchantOperation _ManageMerchantOperation;
        // ManageBNPL _ManageBNPL;

        /// <summary>
        /// Gets the loan request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantlist")]
        public object GetLoanRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetMerchantList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the pending loans.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpendingloans")]
        public object GetPendingLoans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetPendingLoans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the redeemed loans.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getredeemedloans")]
        public object GetRedeemedLoans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetRedeemedLoans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the pending loans overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpendingloansoverview")]
        public object GetPendingLoansOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetPendingLoansOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the redeemed loans overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getredeemedloansoverview")]
        public object GetRedeemedLoansOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetRedeemedLoansOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the loan overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloanoverview")]
        public object GetLoanOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx)); 
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetLoanOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the merchant list overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantlistoverview")]
        public object GetMerchantListOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetMerchantListOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the settlement list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsettlementlist")]
        public object GetSettlementList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetSettlementList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the merchant settlement.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantsettlement")]
        public object GetMerchantSettlement([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetMerchantSettlement(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the settlement overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsettlementoverview")]
        public object GetSettlementOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetSettlementOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the settlements lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsettlementslite")]
        public object GetSettlementsLite([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetSettlementsLite(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the settlement overviewlite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsettlementoverviewlite")]
        public object GetSettlementOverviewlite([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.GetSettlementOverviewlite(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Settlements the invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("settlementinvoice")]
        public object SettlementInvoice([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchant.InvoiceEmail _Request = JsonConvert.DeserializeObject<OMerchant.InvoiceEmail>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOperation = new ManageMerchantOperation();
            OResponse _Response = _ManageMerchantOperation.SettlementInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
