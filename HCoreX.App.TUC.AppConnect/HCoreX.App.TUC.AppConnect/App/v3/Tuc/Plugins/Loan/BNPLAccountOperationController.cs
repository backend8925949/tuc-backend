//==================================================================================
// FileName: BNPLAccountOperationController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for account functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Operations;
using Microsoft.AspNetCore.Mvc;
using Mono;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace HCore.Api.App.v3.Tuc.Plugins.Operations
{
    [Produces("application/json")]
    [Route("api/v3/plugins/accountop/[action]")]
    public class BNPLAccountOperationController
    {
        ManageAccountOperations _ManageAccountOperations;


        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccount")]
        public object GetAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.Request _Request = JsonConvert.DeserializeObject<OAccountOperation.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperations = new ManageAccountOperations();
            OResponse _Response = _ManageAccountOperations.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccounts")]
        public object GetAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperations = new ManageAccountOperations();
            OResponse _Response = _ManageAccountOperations.GetAccounts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the account loan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountloan")]
        public object GetAccountLoan([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountLoanOperation.Request _Request = JsonConvert.DeserializeObject<OAccountLoanOperation.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperations = new ManageAccountOperations();
            OResponse _Response = _ManageAccountOperations.GetAccountLoan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the account loans.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountloans")]
        public object GetAccountLoans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperations = new ManageAccountOperations();
            OResponse _Response = _ManageAccountOperations.GetAccountLoans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the account loans overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountloansoverview")]
        public object GetAccountLoansOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountLoanOperation.Overview.Request _Request = JsonConvert.DeserializeObject<OAccountLoanOperation.Overview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperations = new ManageAccountOperations();
            OResponse _Response = _ManageAccountOperations.GetAccountLoansOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}