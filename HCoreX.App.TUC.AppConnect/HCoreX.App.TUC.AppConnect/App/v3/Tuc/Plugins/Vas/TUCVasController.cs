//==================================================================================
// FileName: TUCVasController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Vas functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Vas;
using HCore.TUC.Plugins.Vas.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Vas
{
    [Produces("application/json")]
    [Route("api/v3/plugins/vas/[action]")]
    public class TUCVasController : Controller
    {
        ManageVas _ManageVas;


        /// <summary>
        /// Gets the vas category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvascategories")]
        public object GetVasCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasproducts")]
        public object GetVasProduct([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas product item.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasproductitems")]
        public object GetVasProductItem([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProductItem(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas product purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasproductpurchasehistory")]
        public object GetVasProductPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProductPurchase(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the vas product purchase overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasproductpurchasehistoryoverview")]
        public object GetVasProductPurchaseOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProductPurchaseOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas product purchase.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasproductpurchasedetails")]
        public object GetVasProductPurchase([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProductPurchase(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Updates the vas product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatevasproduct")]
        public object UpdateVasProduct([FromBody] OAuth.Request _OAuthRequest)
        {
            OVas.Product.Request _Request = JsonConvert.DeserializeObject<OVas.Product.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.UpdateVasProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the vas product item.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatevasproductitem")]
        public object UpdateVasProductItem([FromBody] OAuth.Request _OAuthRequest)
        {
            OVas.ProductItem.Request _Request = JsonConvert.DeserializeObject<OVas.ProductItem.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.UpdateVasProductItem(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the vas purchase overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvaspurchaseoverview")]
        public object GetVasPurchaseOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasAnalytics.Request _Request = JsonConvert.DeserializeObject<OVasAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasPurchaseOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas purchase status overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvaspurchasestatusoverview")]
        public object GetVasPurchaseStatusOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasAnalytics.Request _Request = JsonConvert.DeserializeObject<OVasAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasPurchaseStatusOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the vas purchase category overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvaspurchasecategoryoverview")]
        public object GetVasPurchaseCategoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasAnalytics.Request _Request = JsonConvert.DeserializeObject<OVasAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasPurchaseCategoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Vases  payment confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("reversetransaction")]
        public object VasPayment_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasOp.Purchase.Confirm.Request _Request = JsonConvert.DeserializeObject<OVasOp.Purchase.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.VasPayment_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Verifies the vas payment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifyvaspayment")]
        public object VerifyVasPayment([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.VerifyVasPayment(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
