//==================================================================================
// FileName: TUCDealController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Controller for deal related operation of console panel 
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/deals/[action]")]
    public class TUCDealController
    {
        ManageDeals _ManageDeals;
        #region Analytics
        /// <summary>
        /// Gets the deal history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealhistory")]
        public object GetDealHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the deal overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealsoverview")]
        public object GetDealOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the purchase history overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpurchasehistoryoverview")]
        public object GetPurchaseHistoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetPurchaseHistoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        #endregion



        /// <summary>
        /// Gets the merchant deal overview list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantdealoverviewlist")]
        public object GetMerchantDealOverviewList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetMerchantDealOverviewList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the customer deal overview list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerdealoverviewlist")]
        public object GetCustomerDealOverviewList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetCustomerDealOverviewList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the deal purchase overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealspurchaseoverview")]
        public object GetDealPurchaseOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealPurchaseOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Validates the deal title.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("validatedealtitle")]
        public object ValidateDealTitle([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeal.ValidateTitle.Request _Request = JsonConvert.DeserializeObject<ODeal.ValidateTitle.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.ValidateDealTitle(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedeal")]
        public object SaveDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeal.Manage.Request _Request = JsonConvert.DeserializeObject<ODeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.SaveDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedeal")]
        public object UpdateDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeal.Manage.Request _Request = JsonConvert.DeserializeObject<ODeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.UpdateDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Extends the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("extenddeal")]
        public object ExtendDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeal.Manage.Request _Request = JsonConvert.DeserializeObject<ODeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.ExtendDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the deal coupons.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealcoupons")]
        public object UpdateDealCoupons([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeal.Manage.Request _Request = JsonConvert.DeserializeObject<ODeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.UpdateDealCoupons(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Duplicates the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("duplicatedeal")]
        public object DuplicateDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.DuplicateDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Approves the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("approvedeal")]
        public object ApproveDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.ApproveDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Rejects the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("rejectdeal")]
        public object RejectDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.RejectDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the deal status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealstatus")]
        public object UpdateDealStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.UpdateDealStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletedeal")]
        public async Task<object> DeleteDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = await _ManageDeals.DeleteDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the deal image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletedealimage")]
        public object DeleteDealImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.DeleteDealImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeal")]
        public object GetDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpurchasehistory")]
        public object GetPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the purchase details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpurchasedetails")]
        public object GetPurchaseDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetPurchaseDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }




        /// <summary>
        /// Gets the deal review.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealreviews")]
        public object GetDealReview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealReview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the deal rview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealreview")]
        public object UpdateDealRview([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealReview.Update _Request = JsonConvert.DeserializeObject<ODealReview.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.UpdateDealRview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Gets the flash deals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getflashdeals")]
        public object GetFlashDeals([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetFlashDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the flash deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveflashdeal")]
        public object SaveFlashDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OFlashDeal.Manage.Request _Request = JsonConvert.DeserializeObject<OFlashDeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.SaveFlashDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the deal location.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeallocation")]
        public object GetDealLocation([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealLocation(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Removes the flash deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("removeflashdeal")]
        public object RemoveFlashDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OFlashDeal.Manage.Request _Request = JsonConvert.DeserializeObject<OFlashDeal.Manage.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.RemoveFlashDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Sends the deal notification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("senddealnotification")]
        public object SendDealNotification([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.SendDealNotification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deal notifications.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealnotifications")]
        public object GetDealNotifications([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.GetDealNotification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Validates the deal code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("validatedealcode")]
        public object ValidateDealCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.ValidateDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the deal code status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealcodestatus")]
        public object UpdateDealCodeStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            DealCodeStatus _Request = JsonConvert.DeserializeObject<DealCodeStatus>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeals = new ManageDeals();
            OResponse _Response = _ManageDeals.UpdateDealCodeStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
