using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/dealcart/[action]")]
    public class TUCDealsCartController : Controller
    {
        ManageDealCart _ManageDealCart;

        /// <summary>
        /// Description: Saves the cart.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecart")]
        public async Task<object> SaveCart([FromBody] OAuth.Request _OAuthRequest)
        {
            OCart.Save.Request _Request = JsonConvert.DeserializeObject<OCart.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.SaveCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the cart.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecart")]
        public async Task<object> UpdateCart([FromBody] OAuth.Request _OAuthRequest)
        {
            OCart.Update.Request _Request = JsonConvert.DeserializeObject<OCart.Update.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.UpdateCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Deletes the cart.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecart")]
        public async Task<object> DeleteCart([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.DeleteCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cart details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcart")]
        public async Task<object> GetCart([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.GetCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cart list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcarts")]
        public async Task<object> GetCarts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.GetCarts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
