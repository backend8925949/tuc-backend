//==================================================================================
// FileName: TUCDealBookmarkController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controller for bookmark related operation
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.MadDeals;
using HCore.TUC.Plugins.MadDeals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/bookmarks/[action]")]

    public class TUCDealBookmarkController
    {
        ManageDealBookmark _ManageDealBookmark;

        /// <summary>
        /// Saves the book mark.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedealbookmark")]
        public object SaveBookMark([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealBookmark.Save.Request _Request = JsonConvert.DeserializeObject<ODealBookmark.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealBookmark = new ManageDealBookmark();
            OResponse _Response = _ManageDealBookmark.SaveBookMark(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        //[HttpPost]
        //[ActionName("getbookmark")]
        //public object GetBookmark([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageDealBookmark = new ManageDealBookmark();
        //    OResponse _Response = _ManageDealBookmark.GetBookmark(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
    }
}