//==================================================================================
// FileName: TUCDealPromotionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controller for promotional deals operation
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/dealpromotion/[action]")]
    public class TUCDealPromotionController
    {
        ManageDealPromotionOperations _ManageDealPromotionOperations;

        /// <summary>
        /// Saves the deal promotion.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedealpromotion")]
        public object SaveDealPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealPromotion.SaveDealPromotion.Request _Request = JsonConvert.DeserializeObject<ODealPromotion.SaveDealPromotion.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.SaveDealPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the deal promotion.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealpromotion")]
        public object UpdateDealPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealPromotion.UpdateDealPromotion _Request = JsonConvert.DeserializeObject<ODealPromotion.UpdateDealPromotion>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.UpdateDealPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the deal promotion.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletedealpromotion")]
        public object DeleteDealPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.DeleteDealPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deal promotion details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealpromotion")]
        public object GetDealPromotionDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.GetDealPromotionDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deal promotion list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealpromotions")]
        public object GetDealPromotionList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.GetDealPromotionList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Removes the promotion image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("removepromotionimage")]
        public object RemovePromotionImage([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealPromotion.Image _Request = JsonConvert.DeserializeObject<ODealPromotion.Image>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromotionOperations = new ManageDealPromotionOperations();
            OResponse _Response = _ManageDealPromotionOperations.RemovePromotionImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
