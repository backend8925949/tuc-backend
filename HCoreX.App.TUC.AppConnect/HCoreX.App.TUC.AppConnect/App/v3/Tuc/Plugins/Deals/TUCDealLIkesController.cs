//==================================================================================
// FileName: TUCDealLIkesController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controler for deal likes
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/deals/[action]")]
    public class TUCDealLikesController
    {
        ManageDealLikes _ManageDealLikes;

        /// <summary>
        /// Gets the deallikes.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeallikes")]
        public object GetDeallikes([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealLikes = new ManageDealLikes();
            OResponse _Response = _ManageDealLikes.GetDeallikes(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
