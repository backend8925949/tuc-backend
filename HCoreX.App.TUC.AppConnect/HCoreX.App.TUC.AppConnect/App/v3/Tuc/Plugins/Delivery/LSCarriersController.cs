//==================================================================================
// FileName: LSCarriersController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for LS Carriers functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lscarrier/[action]")]
    public class LSCarriersController : Controller
    {
        ManageCarriers _ManageCarriers;

        /// <summary>
        /// Saves the carriers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecarrier")]
        public object SaveCarriers([FromBody] OAuth.Request _OAuthRequest)
        {
            OCarriers.Save.Request _Request = JsonConvert.DeserializeObject<OCarriers.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageCarriers();
            OResponse _Response = _ManageCarriers.SaveCarriers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the carrirer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcarrier")]
        public object GetCarrirer([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageCarriers();
            OResponse _Response = _ManageCarriers.GetCarrirer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the carriers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcarriers")]
        public object GetCarriers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageCarriers();
            OResponse _Response = _ManageCarriers.GetCarriers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

