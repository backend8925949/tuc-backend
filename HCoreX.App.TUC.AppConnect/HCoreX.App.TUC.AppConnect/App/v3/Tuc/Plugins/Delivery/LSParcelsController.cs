//==================================================================================
// FileName: LSParcelsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for LS parcels functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lsparcels/[action]")]

    public class LSParcelsController : Controller
    {
        ManageParcel _ManageParcel;

        /// <summary>
        /// Saves the parcel.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveparcel")]
        public object SaveParcel([FromBody] OAuth.Request _OAuthRequest)
        {
            OParcel.Save _Request = JsonConvert.DeserializeObject<OParcel.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageParcel = new ManageParcel();
            OResponse _Response = _ManageParcel.SaveParcel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the parcel.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getparcel")]
        public object GetParcel([FromBody] OAuth.Request _OAuthRequest)
        {
            OParcel.Details _Request = JsonConvert.DeserializeObject<OParcel.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageParcel = new ManageParcel();
            OResponse _Response = _ManageParcel.GetParcel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the parcels.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getparcels")]
        public object GetParcels([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageParcel = new ManageParcel();
            OResponse _Response = _ManageParcel.GetParcels(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

