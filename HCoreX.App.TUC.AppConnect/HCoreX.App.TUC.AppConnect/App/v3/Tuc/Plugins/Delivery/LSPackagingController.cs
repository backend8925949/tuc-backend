//==================================================================================
// FileName: LSPackagingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for LS packaging functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lspackaging/[action]")]
    public class LSPackagingController : Controller
    {
        ManagePackaging _ManagePackaging;

        /// <summary>
        /// Saves the packaging.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savepackaging")]
        public object SavePackaging([FromBody] OAuth.Request _OAuthRequest)
        {
            OPackaging.Save.Request _Request = JsonConvert.DeserializeObject<OPackaging.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePackaging = new ManagePackaging();
            OResponse _Response = _ManagePackaging.SavePackaging(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the packaging.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpackaging")]
        public object GetPackaging([FromBody] OAuth.Request _OAuthRequest)
        {
            OPackaging.Details _Request = JsonConvert.DeserializeObject<OPackaging.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePackaging = new ManagePackaging();
            OResponse _Response = _ManagePackaging.GetPackaging(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the packagings.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpackagings")]
        public object GetPackagings([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePackaging = new ManagePackaging();
            OResponse _Response = _ManagePackaging.GetPackagings(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the system packaging list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsystempackagings")]
        public object GetSystemPackaging([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePackaging = new ManagePackaging();
            OResponse _Response = _ManagePackaging.GetSystemPackaging(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

