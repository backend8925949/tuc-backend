//==================================================================================
// FileName: TUCBankController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for bank functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Customer;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Customer
{
    [Produces("application/json")]
    [Route("api/v3/cust/account/[action]")]
    public class TUCBankController
    {
        ManageBank? _ManageBank;
        /// <summary>
        /// Description: Gets the bank code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbankcodes")]
        public async Task<object> GetBankCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbankaccounts")]
        public async Task<object> GetBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletebankaccount")]
        public async Task<object> DeleteBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.DeleteBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savebankaccount")]
        public async Task<object> SaveBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.SaveBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        [HttpPost]
        [ActionName("quicksavebankaccount")]
        public async Task<object> QuickSaveBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.QuickSave? request = JsonConvert.DeserializeObject<OBank.QuickSave>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse response = await _ManageBank.QuickSaveBankAccount(request);
            return HCoreAuth.Auth_Response(response, request.UserReference);
        }
    }
}
