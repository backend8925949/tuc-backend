//==================================================================================
// FileName: TUCAppController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined to get app slider and configuration.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Operations.Customer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Customer
{
    [Route("api/v3/cust/app/[action]")]
    public class TUCAppController
    {
        ManageApp _ManageApp;
        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getappconfiguration")]
        public object GetAppConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OApp.Configuration.Request _Request = JsonConvert.DeserializeObject<OApp.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetAppConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the application slider.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getappslider")]
        public object GetAppSlider([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageApp = new ManageApp();
            OResponse _Response = _ManageApp.GetAppSlider(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    } 
}
