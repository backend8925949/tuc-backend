//==================================================================================
// FileName: TUCAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for pin and referral bonus functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Operations.Customer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Operations.CustomerWeb;
namespace HCore.Api.App.v3.Tuc.Customer
{
    [Produces("application/json")]
    [Route("api/v3/cust/account/[action]")]
    public class TUCAccountController
    {
        ManageOperations? _ManageOperations;
        ManageCard? _ManageCard;

        /// <summary>
        /// Gets the cards.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcards")]
        public object GetCards([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCard = new ManageCard();
            OResponse _Response = _ManageCard.GetCard(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the card.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecard")]
        public object DeleteCard([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCard = new ManageCard();
            OResponse _Response = _ManageCard.DeleteCard(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbalance")]
        public object GetBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperation.Balance.Request? _Request = JsonConvert.DeserializeObject<OOperation.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the referral code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatereferralcode")]
        public object UpdateReferralCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperation.ReferralCode.Request? _Request = JsonConvert.DeserializeObject<OOperation.ReferralCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.UpdateReferralCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Validates the referral bonus.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("validatereferralbonus")]
        public object ValidateReferralBonus([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperation.ReferralBonus.Request? _Request = JsonConvert.DeserializeObject<OOperation.ReferralBonus.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.ValidateReferralBonus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepin")]
        public object UpdatePin([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperation.PinManager.Update.Request? _Request = JsonConvert.DeserializeObject<OOperation.PinManager.Update.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.UpdatePin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Resets the pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpin")]
        public object ResetPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperation.PinManager.Update.Request? _Request = JsonConvert.DeserializeObject<OOperation.PinManager.Update.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.ResetPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        ManageUserRegistration? _ManageUserRegistration;

        [HttpPost]
        [ActionName("userlogin")]
        public object UserLogIn([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.UserLogInRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.UserLogInRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserLogIn(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("userregistration")]
        public object UserRegistration([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.UserRegistration? _Request = JsonConvert.DeserializeObject<OUserRegistration.UserRegistration>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("userverification")]
        public object UserVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.UserVerificationRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.UserVerificationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("changepin")]
        public object UpdateAccountPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.ChangeUserAccountPinRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.ChangeUserAccountPinRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UpdateAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("resendpin")]
        public object ResendUserverificationPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.ResendUserVerificationRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.ResendUserVerificationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResendUserverificationPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("resetpin")]
        public object ResetUserAccountPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.ResetUserAccountPinRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.ResetUserAccountPinRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResetUserAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("updatepin")]
        public object ResetAccountPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OUserRegistration.UpdateAccountPinRequest? _Request = JsonConvert.DeserializeObject<OUserRegistration.UpdateAccountPinRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResetAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
