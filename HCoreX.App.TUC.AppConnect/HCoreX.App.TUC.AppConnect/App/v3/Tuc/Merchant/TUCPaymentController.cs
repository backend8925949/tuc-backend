//==================================================================================
// FileName: TUCPaymentController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for cashout functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Merchant
{
    /// <summary>
    /// Class TUCPaymentController.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v3/merchant/payments/[action]")]
    public class TUCPaymentController
    {
        ManageCashout? _ManageCashout;
        /// <summary>
        /// Gets the tuc cash out configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashoutconfiguration")]
        public async Task<object> GetTucCashOutConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Configuration.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashOutConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Tuc cash out initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("cashoutinitialize")]
        public async Task<object> TucCashOutInitialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Initialize.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.TucCashOutInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the tuc cashout.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashouts")]
        public async Task<object> GetTucCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the cashouts overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getredeemwalletoverview")]
        public object GetCashoutsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetCashoutOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the reward wallets.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrewardwallets")]
        public object GetRewardWallets([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetRewardWallets(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Redeems from wallet.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("redeemfromwallet")]
        public object RedeemFromWallet([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Initialize.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.RedeemFromWallet(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the tuc summarize cashout.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsummarizecashouts")]
        public object GetTucSummarizeCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetTucSummarizeCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the tuc summarize cash out configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsummarizecashoutconfiguration")]
        public async Task<object> GetTucSummarizeCashOutConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Configuration.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucSummarizeCashOutConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
