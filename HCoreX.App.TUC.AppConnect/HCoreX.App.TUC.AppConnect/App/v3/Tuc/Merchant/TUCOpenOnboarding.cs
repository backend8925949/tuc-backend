//==================================================================================
// FileName: TUCOpenOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for onboarding functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/onb/merchant/connect/[action]")]
    public class TUCOpenOnboarding : Controller
    {
        ManageOnboarding _ManageMerchantOnboarding;

        /// <summary>
        /// Onboard merchant ST1.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("connectaccount")]
        public object OnboardMerchant_St1([FromBody] OOnboarding.St1.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St1(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Onboard merchant ST2 resend email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resendemail")]
        public object OnboardMerchant_St2_ResendEmail([FromBody] OOnboarding.St2.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St2_ResendEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Onboard merchant ST3 verify email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifyemail")]
        public object OnboardMerchant_St3_VerifyEmail([FromBody] OOnboarding.St3.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St3_VerifyEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Onboard merchant ST4 request mobile verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("requestmobileverification")]
        public object OnboardMerchant_St4_RequestMobileVerification([FromBody] OOnboarding.St4.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St4_RequestMobileVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Onboard merchant ST5 verify mobile number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifymobile")]
        public object OnboardMerchant_St5_VerifyMobileNumber([FromBody] OOnboarding.St5.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St5_VerifyMobileNumber(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Onboard merchant ST6 complete setup.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant")]
        public object OnboardMerchant_St6_CompleteSetup([FromBody] OOnboarding.St6.Request _Request)
        {
            _ManageMerchantOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St6_CompleteSetup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
