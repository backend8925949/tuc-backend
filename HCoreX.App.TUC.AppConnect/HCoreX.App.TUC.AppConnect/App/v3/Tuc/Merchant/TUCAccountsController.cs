//==================================================================================
// FileName: TUCAccountsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for account functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/accounts/[action]")]
    public class TUCAccountsController : Controller
    {
        ManageAccounts _ManageAccounts;
        /// <summary>
        /// Onboard Merchant
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchantv4")]
        public object OnboardMerchantV4([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.MerchantOnboardingV4.Request _Request = JsonConvert.DeserializeObject<OAccounts.MerchantOnboardingV4.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.OnboardMerchantV4(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Onboard merchant change number verification
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchantchangenumber")]
        public object OnboardMerchantChangeVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.MerchantOnboarding.ChangeNumber _Request = JsonConvert.DeserializeObject<OAccounts.MerchantOnboarding.ChangeNumber>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.OnboardMerchantChangeVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Onboard merchant request verification
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchantrequestverification")]
        public object OnboardMerchantRequestVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.MerchantOnboarding.VerifyOtp _Request = JsonConvert.DeserializeObject<OAccounts.MerchantOnboarding.VerifyOtp>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.OnboardMerchantRequestVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Onboard merchant verify mobile number
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [ActionName("onboardmerchantverifynumber")]
        public object OnboardMerchantVerifyNumber([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.MerchantOnboarding.VerifyOtp _Request = JsonConvert.DeserializeObject<OAccounts.MerchantOnboarding.VerifyOtp>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.OnboardMerchantVerifyNumber(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Onboard merchant
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant")]
        public object OnboardMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.MerchantOnboarding.MerchantDetails _Request = JsonConvert.DeserializeObject<OAccounts.MerchantOnboarding.MerchantDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.OnboardMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecustomer")]
        public object SaveCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Customer.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Customer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.SaveCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savestore")]
        public object SaveStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Store.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.SaveStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecashier")]
        public object SaveCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.SaveCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveterminal")]
        public object SaveTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.SaveTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the sub account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savesubaccount")]
        public object SaveSubAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.SaveSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Resets the sub account password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetsubaccountpassword")]
        public object ResetSubAccountPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.ResetSubAccountPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatemerchantdetails")]
        public object UpdateMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Merchant.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.UpdateMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatestore")]
        public object UpdateStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Store.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.UpdateStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Resets the merchant password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetmerchantpassword")]
        public object ResetMerchantPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Merchant.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.ResetMerchantPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Resets the cashier password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetcashierpassword")]
        public object ResetCashierPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.ResetCashierPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecashier")]
        public object UpdateCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.UpdateCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the sub account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesubaccount")]
        public object UpdateSubAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.UpdateSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        //[HttpPost]
        //[ActionName("resetaubaccountpassword")]
        //public object ResetSubAccountPassword([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAccounts = new ManageAccounts();
        //    OResponse _Response = _ManageAccounts.ResetSubAccountPassword(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}




        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchant")]
        public object GetMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Merchant.Request _Request = JsonConvert.DeserializeObject<OAccounts.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the cashiers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashiers")]
        public object GetCashiers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCashiers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashier")]
        public object GetCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccounts.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the cashiers overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashiersoverview")]
        public object GetCashiersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCashiersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the sub accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubaccounts")]
        public object GetSubAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the sub account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubaccount")]
        public object GetSubAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccounts.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomer")]
        public object GetCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Customer.Request _Request = JsonConvert.DeserializeObject<OAccounts.Customer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the stores.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstores")]
        public object GetStores([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetStores(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstore")]
        public object GetStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Store.Request _Request = JsonConvert.DeserializeObject<OAccounts.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminals")]
        public object GetTerminals([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetTerminals(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the terminals overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalsoverview")]
        public object GetTerminalsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetTerminalsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminal")]
        public object GetTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccounts.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Enables the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("enableterminal")]
        public object EnableTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.EnableTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Disables the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("disableterminal")]
        public object DisableTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.DisableTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Enables the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("enablestore")]
        public object EnableStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Store.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.EnableStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Disables the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("disablestore")]
        public object DisableStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Store.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.DisableStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Enables the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("enablecashier")]
        public object EnableCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.EnableCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Disables the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("disablecashier")]
        public object DisableCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.DisableCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Enables the subaccount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("enablesubaccount")]
        public object EnableSubaccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.EnableSubaccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Disables the subaccount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("disablesubaccount")]
        public object DisableSubaccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperations.SubAccount.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.SubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.DisableSubaccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomers")]
        public object GetCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the customer overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomersoverview")]
        public object GetCustomerOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCustomerOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        //[HttpPost]
        //[ActionName("disableterminal")]
        //public object EnableCashier([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OAccountOperations.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAccounts = new ManageAccounts();
        //    OResponse _Response = _ManageAccounts.EnableCashier(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        //[HttpPost]
        //[ActionName("disablecashier")]
        //public object DisableCashier([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OAccountOperations.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccountOperations.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAccounts = new ManageAccounts();
        //    OResponse _Response = _ManageAccounts.DisableCashier(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}


    }
}
