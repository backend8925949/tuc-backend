//==================================================================================
// FileName: TUCSubscriptionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for subscription functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/subscription/[action]")]
    public class TUCSubscriptionController : Controller
    {
        ManageSubscription _ManageSubscription;
        /// <summary>
        /// Gets the account balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountbalance")]
        public object GetAccountBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.Balance.Request _Request = JsonConvert.DeserializeObject<OSubscription.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Topups the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("topupaccount")]
        public object TopupAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.Topup.Request _Request = JsonConvert.DeserializeObject<OSubscription.Topup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.TopupAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the topup history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopuphistory")]
        public object GetTopupHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetTopupHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the top up overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopupoverview")]
        public object GetTopUpOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetTopUpOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
