//==================================================================================
// FileName: TUCMerchantCategoryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for merchant category functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Merchant;
using HCore.TUC.Core.Merchant.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/merchantscat/[action]")]
    public class TUCMerchantCategoryController
    {
        ManageCategory _ManageCategory;


        /// <summary>
        /// Saves the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecategory")]
        public object SaveCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Save.Request _Request = JsonConvert.DeserializeObject<OCategory.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecategory")]
        public object UpdateCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Update _Request = JsonConvert.DeserializeObject<OCategory.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.UpdateCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecategory")]
        public object DeleteCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.DeleteCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategory")]
        public object GetCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets all category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getallcategories")]
        public object GetAllCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetAllCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
