//==================================================================================
// FileName: TUCOperationsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for operations.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant.BulkRewards;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/operations/[action]")]
    public class TUCOperationsController : Controller
    {
        ManageOperations _ManageOperations;
        /// <summary>
        /// Saves the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveconfiguration")]
        public object SaveConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Configuration.Request _Request = JsonConvert.DeserializeObject<OOperations.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.SaveConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the configurations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveconfigurations")]
        public object SaveConfigurations([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Configuration.BulkUpdate _Request = JsonConvert.DeserializeObject<OOperations.Configuration.BulkUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.SaveConfigurations(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfiguration")]
        public object GetConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Configuration.Config _Request = JsonConvert.DeserializeObject<OOperations.Configuration.Config>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the loyalty configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloyaltyconfiguration")]
        public object GetLoyaltyConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Configuration.Config _Request = JsonConvert.DeserializeObject<OOperations.Configuration.Config>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetLoyaltyConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the configurations.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurations")]
        public object GetConfigurations([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the configuration history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurationhistory")]
        public object GetConfigurationHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetConfigurationHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// The manage upload customer reward
        /// </summary>
        ManageBulkRewardUpload _ManageBulkRewardUpload;
        //ManageUploadCustomerReward _ManageUploadCustomerReward;
        /// <summary>
        /// Rewards the customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("uploadcustomerrewards")]
        public object UploadBulkCustomerRewards([FromBody] OAuth.Request _OAuthRequest)
        {
            OBulkRewardUpload.Request _Request = JsonConvert.DeserializeObject<OBulkRewardUpload.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBulkRewardUpload = new ManageBulkRewardUpload();
            OResponse _Response = _ManageBulkRewardUpload.UploadBulkRewardCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageBulkRewards? _ManageBulkRewards;
        /// <summary>
        /// Description: Gets the bulk customer rewards.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbulkcustomerrewards")]
        public object GetBulkRewardCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBulkRewards = new ManageBulkRewards();
            OResponse _Response = _ManageBulkRewards.GetBulkRewardCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
