//==================================================================================
// FileName: TUCOnboardingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for onboarding functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Operations.Partner;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Partner
{
    [Produces("application/json")]
    [Route("api/v3/partner/onboarding/[action]")]
    public class TUCOnboardingController : Controller
    {
        ManageOnboarding _ManageOnboarding;
        /// <summary>
        /// Onboard merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchants")]
        public object OnboardMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboarding.Merchant.Request _Request = JsonConvert.DeserializeObject<OOnboarding.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.OnboardMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the onboarded merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getonboardedmerchants")]
        public object GetOnboardedMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        HCore.TUC.Core.Operations.Acquirer.ManageOnboarding _ManageOnboardingAcq;

        /// <summary>
        /// Onboard customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardcustomers")]
        public object OnboardCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
         HCore.TUC.Core.Object.Acquirer.OOnboarding.Customer.Request _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Acquirer.OOnboarding.Customer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingAcq = new TUC.Core.Operations.Acquirer.ManageOnboarding();
            OResponse _Response = _ManageOnboardingAcq.OnboardCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the onboarded customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getonboardedcustomers")]
        public object GetOnboardedCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingAcq = new TUC.Core.Operations.Acquirer.ManageOnboarding();
            OResponse _Response = _ManageOnboardingAcq.GetOnboardedCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the onboarded customer files.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getonboardedcustomerfiles")]
        public object GetOnboardedCustomerFiles([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingAcq = new TUC.Core.Operations.Acquirer.ManageOnboarding();
            OResponse _Response = _ManageOnboardingAcq.GetOnboardedCustomerFiles(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
