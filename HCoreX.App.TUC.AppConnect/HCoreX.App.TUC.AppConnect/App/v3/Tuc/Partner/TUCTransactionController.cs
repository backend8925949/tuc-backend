//==================================================================================
// FileName: TUCTransactionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Api.App.v3.Tuc.Partner
{
    public class TUCTransactionController
    {
        public TUCTransactionController()
        {
        }
    }
}
