//==================================================================================
// FileName: TUCBranchController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for branch functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Operations.Partner;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Partner
{
    [Produces("application/json")]
    [Route("api/v3/partner/branch/[action]")]
    public class TUCBranchController : Controller
    {
        ManageBranch _ManageBranch;
        /// <summary>
        /// Saves the branch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savebranch")]
        public object SaveBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OBranch.Request _Request = JsonConvert.DeserializeObject<OBranch.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.SaveBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the branch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatebranch")]
        public object UpdateBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OBranch.Request _Request = JsonConvert.DeserializeObject<OBranch.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.UpdateBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the branch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbranch")]
        public object GetBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the branchs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbranches")]
        public object GetBranchs([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the manager.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savemanager")]
        public object SaveManager([FromBody]OAuth.Request _OAuthRequest)
        {
            OManager.Request _Request = JsonConvert.DeserializeObject<OManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.SaveManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the manager.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatemanager")]
        public object UpdateManager([FromBody]OAuth.Request _OAuthRequest)
        {
            OManager.Request _Request = JsonConvert.DeserializeObject<OManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.UpdateManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the b get managerranch.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmanager")]
        public object GetBGetManagerranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the managers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmanagers")]
        public object GetManagers([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
