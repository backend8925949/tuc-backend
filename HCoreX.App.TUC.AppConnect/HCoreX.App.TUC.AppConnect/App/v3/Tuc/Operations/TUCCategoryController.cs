//==================================================================================
// FileName: TUCCategoryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Category Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Operations
{
    [Route("api/v3/tuc/categories/[action]")]
    public class TUCCategoryController 
    {
        ManageCategory _ManageCategory;
        /// <summary>
        /// Saves the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecategory")]
        public object SaveCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecategory")]
        public object UpdateCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.UpdateCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecategory")]
        public object DeleteCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.DeleteCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategory")]
        public object GetCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets all category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getallcatgories")]
        public object GetAllCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetAllCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Saves the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savesubcategory")]
        public object SaveSubCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the sub categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savesubcategories")]
        public object SaveSubCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.SaveSubCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesubcategory")]
        public object UpdateSubCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.UpdateSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Deletes the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletesubcategory")]
        public object DeleteSubCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.DeleteSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the sub category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubcategory")]
        public object GetSubCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OCategory.Request _Request = JsonConvert.DeserializeObject<OCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the sub categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubcategories")]
        public object GetSubCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCategory = new ManageCategory();
            OResponse _Response = _ManageCategory.GetSubCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
