//==================================================================================
// FileName: TUCBankController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Bank Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/bankop/[action]")]
    public class TUCBankController : Controller
    {
        ManageBank? _ManageBank;

        /// <summary>
        /// Description: Method defined to get list of bank codes
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbankcodes")]
        public async Task<object> GetBankCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of bank accounts
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbankaccounts")]
        public async Task<object> GetBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to delete bank account
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deletebankaccount")]
        public async Task<object> DeleteBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.DeleteBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savebankaccount")]
        public async Task<object> SaveBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.SaveBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatebankaccount")]
        public async Task<object> UpdateBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.UpdateBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbankaccount")]
        public async Task<object> GetBankAccountDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
