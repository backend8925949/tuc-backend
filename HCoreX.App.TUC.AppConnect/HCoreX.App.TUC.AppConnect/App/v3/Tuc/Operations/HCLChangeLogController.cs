//==================================================================================
// FileName: HCLChangeLogController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Logging Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/hclchangelog/[action]")]

    public class HCLChangeLogController : Controller
    {
        ManageChangeLog _ManageAppPromotion;


        /// <summary>
        /// Saves the change log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savechangelog")]
        public object SaveChangeLog([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreChangeLog.Save.Request _Request = JsonConvert.DeserializeObject<OCoreChangeLog.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageChangeLog();
            OResponse _Response = _ManageAppPromotion.SaveChangeLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the change log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatechangelog")]
        public object UpdateChangeLog([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreChangeLog.Update _Request = JsonConvert.DeserializeObject<OCoreChangeLog.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageChangeLog();
            OResponse _Response = _ManageAppPromotion.UpdateChangeLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the change log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletechangelog")]
        public object DeleteChangeLog([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageChangeLog();
            OResponse _Response = _ManageAppPromotion.DeleteChangeLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the change log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getchangelog")]
        public object GetChangeLog([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageChangeLog();
            OResponse _Response = _ManageAppPromotion.GetChangeLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the change logs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getchangelogs")]
        public object GetChangeLogs([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageChangeLog();
            OResponse _Response = _ManageAppPromotion.GetChangeLogs(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
