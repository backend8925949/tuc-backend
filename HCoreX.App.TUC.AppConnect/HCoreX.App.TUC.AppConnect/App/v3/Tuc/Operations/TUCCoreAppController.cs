//==================================================================================
// FileName: TUCCoreAppController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for core app and its version functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/coreappmanager/[action]")]
    public class TUCCoreAppController : Controller
    {
        ManageCoreApp _ManageCoreApp;

        /// <summary>
        /// Saves the core application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecoreapp")]
        public object SaveCoreApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreAppManager.Save.Request _Request = JsonConvert.DeserializeObject<OCoreAppManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.SaveCoreApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the core application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecoreapp")]
        public object UpdateCoreApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreAppManager.Update _Request = JsonConvert.DeserializeObject<OCoreAppManager.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.UpdateCoreApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the core application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecoreapp")]
        public object DeleteCoreApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.DeleteCoreApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the core application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcoreapp")]
        public object GetCoreApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.GetCoreApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the core apps.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcoreapps")]
        public object GetCoreApps([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.GetCoreApps(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Saves the core application version.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecoreappversion")]
        public object SaveCoreAppVersion([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreAppVersionManager.Save.Request _Request = JsonConvert.DeserializeObject<OCoreAppVersionManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.SaveCoreAppVersion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the core application version status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecoreappversionstatus")]
        public object UpdateCoreAppVersionStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreAppVersionManager.UpdateStatus _Request = JsonConvert.DeserializeObject<OCoreAppVersionManager.UpdateStatus>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.UpdateCoreAppVersionStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the core application version.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecoreappversion")]
        public object DeleteCoreAppVersion([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.DeleteCoreAppVersion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the core application version.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcoreappversion")]
        public object GetCoreAppVersion([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.GetCoreAppVersion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the core application versions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcoreappversions")]
        public object GetCoreAppVersions([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreApp = new ManageCoreApp();
            OResponse _Response = _ManageCoreApp.GetCoreAppVersions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}