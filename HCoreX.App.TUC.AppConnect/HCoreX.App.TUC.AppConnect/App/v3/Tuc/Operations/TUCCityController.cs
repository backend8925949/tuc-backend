//==================================================================================
// FileName: TUCCityController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for city functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/city/[action]")]
    public class TUCCityController : Controller
    {
        ManageCityManager _ManageCityManager;

        /// <summary>
        /// Saves the city.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecity")]
        public object SaveCity([FromBody] OAuth.Request _OAuthRequest)
        {
            OCityManager.Save.Request _Request = JsonConvert.DeserializeObject<OCityManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.SaveCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the city.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecity")]
        public object UpdateCity([FromBody] OAuth.Request _OAuthRequest)
        {
            OCityManager.Update _Request = JsonConvert.DeserializeObject<OCityManager.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.UpdateCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the city.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecity")]
        public object DeleteCity([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.DeleteCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcity")]
        public object GetCity([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the cities.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCities([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Saves the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecityarea")]
        public object SaveCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            OCityAreaManager.Save.Request _Request = JsonConvert.DeserializeObject<OCityAreaManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.SaveCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecityarea")]
        public object UpdateCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            OCityAreaManager.Update _Request = JsonConvert.DeserializeObject<OCityAreaManager.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.UpdateCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecityarea")]
        public object DeleteCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.DeleteCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcityarea")]
        public object GetCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the city areas.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcityareas")]
        public object GetCityAreas([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}