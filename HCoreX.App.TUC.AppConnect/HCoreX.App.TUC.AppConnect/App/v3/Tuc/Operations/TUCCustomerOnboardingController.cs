//==================================================================================
// FileName: TUCCustomerOnboardingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for customer onboarding functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace HCore.Api.App.v3.Tuc.Operations
{

    [Produces("application/json")]
    [Route("api/v3/tuc/cop/[action]")]
    public class TUCCustomerOnboardingController : Controller
    {
        ManageCustomerOnboarding _ManageCustomerOnboarding;
        /// <summary>
        /// Onboard Customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardcustomer")]
        public object OnboardCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOboarding.Request _Request = JsonConvert.DeserializeObject<OCustomerOboarding.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOnboarding = new ManageCustomerOnboarding();
            OResponse _Response = _ManageCustomerOnboarding.OnboardCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
