//==================================================================================
// FileName: TimeZoneController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Timezone Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/timezone/[action]")]

    public class TimeZoneController: Controller
    {
        ManageTimeZone _ManageTimeZone;

        /// <summary>
        /// Saves the time zones.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savetimezone")]
        public object SaveTimeZones([FromBody] OAuth.Request _OAuthRequest)
        {
            _ManageTimeZone = new ManageTimeZone();
            OResponse _Response = _ManageTimeZone.SaveTimeZones();
            return HCoreAuth.Auth_Response(_Response);
        }

        /// <summary>
        /// Gets the time zones.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettimezone")]
        public object GetTimeZones([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTimeZone = new ManageTimeZone();
            OResponse _Response = _ManageTimeZone.GetTimeZones(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

