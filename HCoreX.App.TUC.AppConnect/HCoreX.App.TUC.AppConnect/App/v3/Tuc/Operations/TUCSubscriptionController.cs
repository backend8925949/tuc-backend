//==================================================================================
// FileName: TUCSubscriptionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for getting details about subscription
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Operations
{
    /// <summary>
    /// Class TUCSubscriptionController.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v3/tuc/subscriptions/[action]")]
    public class TUCSubscriptionController
    {
        ManageSubscription _ManageSubscription;
        /// <summary>
        /// Updates the subscription.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesubscription")]
        public object UpdateSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.SubscriptionUpdate.Request _Request = JsonConvert.DeserializeObject<OSubscription.SubscriptionUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.UpdateSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the subscription.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsubscriptions")]
        public object GetSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the account subscription.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountsubscriptions")]
        public object GetAccountSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the account subscription overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountsubscriptionsoverview")]
        public object GetAccountSubscriptionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountSubscriptionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get amount paid to paystack for subscription monthly.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getamountmonthly")]
        public object GetAmountMonthly([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAmountMonthly(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
