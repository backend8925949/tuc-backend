//==================================================================================
// FileName: TUCOperationsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for operations realated to storage,configuration and categories
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/op/[action]")]
    public class TUCOperationsController : Controller
    {
        HCore.TUC.Core.Operations.Operations.ManageStorage _ManageStorage;
        ManageOperations _ManageOperations;
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcityareas")]
        public object GetCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstorage")]
        public object GetStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new TUC.Core.Operations.Operations.ManageStorage();
            OResponse _Response = _ManageStorage.GetStorage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savestorage")]
        public object SaveStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            OStorageManager.Request _Request = JsonConvert.DeserializeObject<OStorageManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new TUC.Core.Operations.Operations.ManageStorage();
            OResponse _Response = _ManageStorage.SaveStorage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletestorage")]
        public object DeleteStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new TUC.Core.Operations.Operations.ManageStorage();
            OResponse _Response = _ManageStorage.DeleteStorage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the download.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdownloads")]
        public object GetDownload([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetDownload(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Gets the account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountconfigurations")]
        public object GetAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetDownload(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Saves the account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveaccountconfiguration")]
        public object SaveAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetDownload(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
