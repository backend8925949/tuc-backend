//==================================================================================
// FileName: TUCSmsCampaignController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for smscampaign functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using Newtonsoft.Json;
using HCore.TUC.SmsCampaign;
using HCore.TUC.SmsCampaign.Object;

namespace HCore.Api.App.v3.Tuc.SmsCampaign
{
    [Produces("application/json")]
    [Route("api/v3/campaigns/sms/[action]")]
    public class TUCSmsCampaignController
    {
        ManageAccount _ManageAccount;
        ManageWallet _ManageWallet;
        ManageGroup _ManageGroup;
        ManageAnalytics _ManageAnalytics;
        ManageCampaign _ManageCampaign;
        ManageCampaignHelper _ManageCampaignHelper;

        /// <summary>
        /// Gets the account list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccounts")]
        public object GetAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccounts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbalance")]
        public object GetBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OWallet.Topup.Request _Request = JsonConvert.DeserializeObject<OWallet.Topup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.GetBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Topups the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("topupwallet")]
        public object TopupAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OWallet.Topup.Request _Request = JsonConvert.DeserializeObject<OWallet.Topup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.TopupAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the topup history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopuphistory")]
        public object GetTopupHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.GetTopupHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the sender ids.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsenderids")]
        public object GetSenderIds([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaignHelper = new ManageCampaignHelper();
            OResponse _Response = _ManageCampaignHelper.GetSenderId(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomers")]
        public object GetCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaignHelper = new ManageCampaignHelper();
            OResponse _Response = _ManageCampaignHelper.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the customer count.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomercount")]
        public object GetCustomerCount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaignHelper = new ManageCampaignHelper();
            OResponse _Response = _ManageCampaignHelper.GetCustomerCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the account overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountsoverview")]
        public object GetAccountOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetAccountOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the SMS overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsmsoverview")]
        public object GetSmsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
           OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetSmsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the campaign overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcampaignoverview")]
        public object GetCampaignOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetCampaignOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }



        /// <summary>
        /// Saves the group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savegroup")]
        public object SaveGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGroup.Save.Request _Request = JsonConvert.DeserializeObject<OGroup.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.SaveGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Updates the group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updategroup")]
        public object UpdateGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGroup.Save.Request _Request = JsonConvert.DeserializeObject<OGroup.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.UpdateGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Deletes the group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletegroup")]
        public object DeleteGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGroup.Save.Request _Request = JsonConvert.DeserializeObject<OGroup.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.DeleteGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Deletes the group items.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletegroupitems")]
        public object DeleteGroupItems([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGroupItem.Delete.BulkDelete _Request = JsonConvert.DeserializeObject<OGroupItem.Delete.BulkDelete>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.DeleteGroupItems(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the group.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgroup")]
        public object GetGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OGroup.Reference _Request = JsonConvert.DeserializeObject<OGroup.Reference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.GetGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgroups")]
        public object GetGroups([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.GetGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the group item.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgroupitems")]
        public object GetGroupItem([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGroup = new ManageGroup();
            OResponse _Response = _ManageGroup.GetGroupItem(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecampaign")]
        public object SaveCampaign([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Save.Request _Request = JsonConvert.DeserializeObject<OCampaign.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.SaveCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecampaign")]
        public object UpdateCampaign([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Save.Request _Request = JsonConvert.DeserializeObject<OCampaign.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.UpdateCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcampaign")]
        public object GetCampaign([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Reference _Request = JsonConvert.DeserializeObject<OCampaign.Reference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.GetCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the campaigns.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcampaigns")]
        public object GetCampaigns([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.GetCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the campaign item.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcampaignitems")]
        public object GetCampaignItem([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.GetCampaignItem(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecampaign")]
        public object DeleteCampaign([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Save.Request _Request = JsonConvert.DeserializeObject<OCampaign.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.DeleteCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Duplicates the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("duplicatecampaign")]
        public object DuplicateCampaign([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Duplicate.Request _Request = JsonConvert.DeserializeObject<OCampaign.Duplicate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.DuplicateCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the campaign status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecampaignstatus")]
        public object UpdateCampaignStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Reference _Request = JsonConvert.DeserializeObject<OCampaign.Reference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.UpdateCampaignStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
    }
}
