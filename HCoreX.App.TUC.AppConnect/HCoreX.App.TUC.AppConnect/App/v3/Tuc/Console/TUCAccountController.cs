//==================================================================================
// FileName: TUCAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined to save and get customer,acquirer,pssp,merchant functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/accounts/[action]")]
    public class TUCAccountController
    {       
        ManageAccount _ManageAccount;
        /// <summary>
        /// Description: Updates the account status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaccountstatus")]
        public object UpdateAccountStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccount.ManageStatus.Request _Request = JsonConvert.DeserializeObject<OAccount.ManageStatus.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateAccountStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecustomer")]
        public object UpdateCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomer.Request _Request = JsonConvert.DeserializeObject<OCustomer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Saves the partner.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savepartner")]
        public object SavePartner([FromBody] OAuth.Request _OAuthRequest)
        {
            OPartner.Request _Request = JsonConvert.DeserializeObject<OPartner.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SavePartner(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the acquirer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveacquirer")]
        public object SaveAcquirer([FromBody] OAuth.Request _OAuthRequest)
        {
            OAcquirer.Request _Request = JsonConvert.DeserializeObject<OAcquirer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveAcquirer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Savepssps the specified o authentication request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savepssp")]
        public object Savepssp([FromBody] OAuth.Request _OAuthRequest)
        {
            OPssp.Request _Request = JsonConvert.DeserializeObject<OPssp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.Savepssp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the PTSP.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveptsp")]
        public object SavePtsp([FromBody] OAuth.Request _OAuthRequest)
        {
            OPtsp.Request _Request = JsonConvert.DeserializeObject<OPtsp.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SavePtsp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savemerchant")]
        public object SaveMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchant.Request _Request = JsonConvert.DeserializeObject<OMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccounts")]
        public object GetAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the partners.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpartners")]
        public object GetPartners([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPartner(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the partner.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpartner")]
        public object GetPartner([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPartner(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the acquirers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getacquirers")]
        public object GetAcquirers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAcquirer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getacquirer")]
        public object GetAcquirer([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAcquirer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cashiers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashiers")]
        public object GetCashiers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCashiers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashiersoverview")]
        public object GetCashiersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCashiersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cashier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashier")]
        public object GetCashier([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Cashier.Request _Request = JsonConvert.DeserializeObject<OAccounts.Cashier.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCashier(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the PTSPS.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getptsps")]
        public object GetPtsps([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPtsp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the PTSP.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getptsp")]
        public object GetPtsp([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPtsp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the PSSPS.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpssps")]
        public object GetPssps([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPssp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the PSSP.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpssp")]
        public object GetPssp([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetPssp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantsoverview")]
        public object GetMerchantOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetMerchantOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Description: Gets the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstores")]
        public object GetStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstoreoverview")]
        public object GetStoreOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetStoreOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the store location.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstorelocations")]
        public object GetStoreLocation([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetStoreLocation(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminals")]
        public object GetTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the terminals overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalsoverview")]
        public object GetTerminalsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetTerminalsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomers")]
        public object GetCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
