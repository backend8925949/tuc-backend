//==================================================================================
// FileName: TUCCustomerController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for tracking customer realated activities
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using HCore.TUC.Core.Operations.Console;
using HCore.TUC.Core.Operations.Console.Analytics.Customer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console.Analytics
{
    [Produces("application/json")]
    [Route("api/v3/con/canalytics/[action]")]
    public class TUCCustomerController
    {
        AnalyticsCustomer _AnalyticsCustomer;
        /// <summary>
        /// Description: Method defined to get status count of customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcuststatuscounts")]
        public object GetCustomerStatusCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomerStatusCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get payment modes of customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustpaymentmodes")]
        public object GetCustomersPaymentMode([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersPaymentMode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get referral count of customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustreferralcounts")]
        public object GetCustomerReferrerCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomerReferrerCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get acitvity status count of customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustactivitystatuscounts")]
        public object GetCustomerActivityStatusCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomerActivityStatusCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get activity status count of customer app
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustappactivitystatuscounts")]
        public object GetCustomerAppActivityStatusCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomerAppActivityStatusCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of top senders customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcusttopspenders")]
        public object GetCustomersTopSpenders([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersTopSpenders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of top category customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcusttopcategory")]
        public object GetCustomersTopCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersTopCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of top visitors customer
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustomerstopvisitors")]
        public object GetCustomersTopVisitors([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersTopVisitors(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get  customer gender
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustgenders")]
        public object GetCustomersGender([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersGender(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get  customer age groups
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustagegroups")]
        public object GetCustomersAgeGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersAgeGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get  customer age groups by gender
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustagegroupsbygender")]
        public object GetCustomersAgeGroupGender([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersAgeGroupGender(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to get customers spend range
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustspendrange")]
        public object GetCustomersSpendRange([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetCustomersSpendRange(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get  customers sales range
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustsalesrange")]
        public object GetSaleRange([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetSaleRange(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get customers reward range
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustrewardrange")]
        public object GetRewardRange([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsCustomer = new AnalyticsCustomer();
            OResponse _Response = _AnalyticsCustomer.GetRewardRange(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
