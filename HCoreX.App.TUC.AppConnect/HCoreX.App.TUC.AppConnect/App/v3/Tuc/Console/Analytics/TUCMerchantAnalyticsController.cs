//==================================================================================
// FileName: TUCMerchantAnalyticsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined to track merchant analytics.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Merchant;
using HCore.TUC.Core.Operations.Console;
using HCore.TUC.Core.Operations.Console.Analytics.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console.Analytics
{   
    [Produces("application/json")]
    [Route("api/v3/con/manalytics/[action]")]
    public class TUCMerchantAnalyticsController
    {
       
        AnalyticsMerchant _AnalyticsMerchant;
        /// <summary>
        /// Description: Gets the status count.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstatuscount")]
        public object GetStatusCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetStatusCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the activity status count.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getactivitystatuscount")]
        public object GetActivityStatusCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetActivityStatusCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the top rewards.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettoprewards")]
        public object GetTopRewards([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetTopRewards(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the top visits.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopvisits")]
        public object GetTopVisits([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetTopVisits(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the top sale.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopsale")]
        public object GetTopSale([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetTopSale(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the top category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("gettopcategory")]
        public object GetTopCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetTopCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the sale range.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsalerange")]
        public object GetSaleRange([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetSaleRange(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the reward range.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrewardrange")]
        public object GetRewardRange([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaMerchant.Request _Request = JsonConvert.DeserializeObject<OAnaMerchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _AnalyticsMerchant = new AnalyticsMerchant();
            OResponse _Response = _AnalyticsMerchant.GetRewardRange(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
