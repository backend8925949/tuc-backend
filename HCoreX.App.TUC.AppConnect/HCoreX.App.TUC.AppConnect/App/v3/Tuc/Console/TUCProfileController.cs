//==================================================================================
// FileName: TUCProfileController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for account related functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/profile/[action]")]
    public class TUCProfileController
    {
        ManageProfile _ManageProfile;
        //[HttpPost]
        //[ActionName("saveadmin")]
        //public object SaveAdmin([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OProfile.Request _Request = JsonConvert.DeserializeObject<OProfile.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageProfile = new ManageProfile();
        //    OResponse _Response = _ManageProfile.SaveAdmin(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        /// <summary>
        /// Gets the profile.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getprofile")]
        public object GetProfile([FromBody] OAuth.Request _OAuthRequest)
        {
            OProfile.Details.Request _Request = JsonConvert.DeserializeObject<OProfile.Details.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProfile = new ManageProfile();
            OResponse _Response = _ManageProfile.GetProfile(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the admin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedetails")]
        public object UpdateAdmin([FromBody] OAuth.Request _OAuthRequest)
        {
            OProfile.Request _Request = JsonConvert.DeserializeObject<OProfile.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProfile = new ManageProfile();
            OResponse _Response = _ManageProfile.UpdateProfile(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepassword")]
        public object UpdatePassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OProfile.Password.Request _Request = JsonConvert.DeserializeObject<OProfile.Password.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProfile = new ManageProfile();
            OResponse _Response = _ManageProfile.UpdatePassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Removes the profile image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("removeprofileimage")]
        public object RemoveProfileImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OProfile.Profile _Request = JsonConvert.DeserializeObject<OProfile.Profile>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProfile = new ManageProfile();
            OResponse _Response = _ManageProfile.RemoveProfileImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
