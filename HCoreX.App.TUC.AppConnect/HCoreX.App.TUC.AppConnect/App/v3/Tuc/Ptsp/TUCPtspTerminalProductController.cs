//==================================================================================
// FileName: TUCPtspTerminalProductController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Ptsp terminal product functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.TUC.Core.Operations.Ptsp;
using HCore.Helper;
using Newtonsoft.Json;
using HCore.TUC.Core.Object.Ptsp;

namespace HCore.Api.App.v3.Tuc.Ptsp
{
    [Produces("application/json")]
    [Route("api/v3/ptsp/terminalproduct/[action]")]
    public class TUCPtspTerminalProductController
    {
        ManageTerminalProduct _ManageTerminalProduct;
        /// <summary>
        /// Saves the terminal product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveterminalproduct")]
        public object SaveTerminalProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTerminalProduct.Request _Request = JsonConvert.DeserializeObject<OTerminalProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTerminalProduct = new ManageTerminalProduct();
            OResponse _Response = _ManageTerminalProduct.SaveTerminalProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the terminal product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateterminalproduct")]
        public object UpdateTerminalProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTerminalProduct.Request _Request = JsonConvert.DeserializeObject<OTerminalProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTerminalProduct = new ManageTerminalProduct();
            OResponse _Response = _ManageTerminalProduct.UpdateTerminalProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Deletes the terminal product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteterminalproduct")]
        public object DeleteTerminalProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTerminalProduct.Request _Request = JsonConvert.DeserializeObject<OTerminalProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTerminalProduct = new ManageTerminalProduct();
            OResponse _Response = _ManageTerminalProduct.DeleteTerminalProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the terminal product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalproduct")]
        public object GetTerminalProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OTerminalProduct.Request _Request = JsonConvert.DeserializeObject<OTerminalProduct.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTerminalProduct = new ManageTerminalProduct();
            OResponse _Response = _ManageTerminalProduct.GetTerminalProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the terminal products.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalproducts")]
        public object GetTerminalProducts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTerminalProduct = new ManageTerminalProduct();
            OResponse _Response = _ManageTerminalProduct.GetTerminalProduct(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
    }
}
