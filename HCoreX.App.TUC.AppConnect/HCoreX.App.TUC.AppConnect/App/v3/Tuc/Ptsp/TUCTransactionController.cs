//==================================================================================
// FileName: TUCTransactionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for transactions.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.TUC.Core.Operations.Ptsp;
using HCore.Helper;
using Newtonsoft.Json;
using HCore.TUC.Core.Object.Ptsp;

namespace HCore.Api.App.v3.Tuc.Ptsp
{
    [Produces("application/json")]
    [Route("api/v3/ptsp/transaction/[action]")]
    public class TUCTransactionController
    {
        ManageTransactions _ManageTransactions;
        /// <summary>
        /// Gets the commission history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcommissionhistory")]
        public object GetCommissionHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetCommissionHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the commission history overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcommissionhistoryoverview")]
        public object GetCommissionHistoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetCommissionHistoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }

    }
}
