//==================================================================================
// FileName: TUCController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined related to verification process and account functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Operations.CustomerWeb;
using Microsoft.AspNetCore.Mvc;
namespace HCore.Api.App.v3.Tuc.CustomerWeb
{
    [Produces("application/json")]
    [Route("api/v3/w/acc/connect/[action]")]
    public class TUCController
    {
        ManageVerification _ManageVerification;

        /// <summary>
        /// Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("requestotp")]
        public object RequestOtp([FromBody] OCoreVerificationManager.Request _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.RequestOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifyotp")]
        public object VerifyOtp([FromBody] OCoreVerificationManager.RequestVerify _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.VerifyOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageAccount _ManageAccount;
        /// <summary>
        /// Validates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("validatepin")]
        public object ValidatePin([FromBody] OAccount.ValidatePin.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.ValidatePin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Updates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaccount")]
        public object UpdateAccount([FromBody] OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccount")]
        public object GetAccount([FromBody] OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountbalance")]
        public object GetAccountBalance([FromBody] OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccountBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Gets the account balance open.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccbalops")]
        public object GetAccountBalanceOpen([FromBody] OAccount.OCustReq.Req _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccountBalanceOpen(_Request);
            return _Response;
        }
        /// <summary>
        /// Registers the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("registeraccops")]
        public object RegisterAccount([FromBody] OAccount.OCustRegReq.Req _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.RegisterAccount(_Request);
            return _Response;
        }

    }
}
