//==================================================================================
// FileName: TUCDealController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Operations.CustomerWeb;
using Microsoft.AspNetCore.Mvc;

namespace HCore.Api.App.v3.Tuc.CustomerWeb
{
    [Produces("application/json")]
    [Route("api/v3/w/deals/connect/[action]")]
    public class TUCDealController
    {
        ManageDeal _ManageDeal;
        /// <summary>
        /// Saves the deal view.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedealview")]
        public object SaveDealView([FromBody] ODeal.View _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.SaveDealView(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Buys the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buydeal")]
        public object BuyDeal([FromBody] ODeal.Purchase.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.BuyDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealpurchasehistory")]
        public object GetPurchaseHistory([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealcode")]
        public object GetDealCode([FromBody] ODeal.Purchase.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeal")]
        public object GetDeal([FromBody] ODeal.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] ODeal.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDeals(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Shares the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("sharedealcode")]
        public object ShareDealCode([FromBody] ODeal.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.ShareDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
