//==================================================================================
// FileName: HCSystemController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for system actions.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Operations.Acquirer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.System
{
    [Produces("application/json")]
    [Route("api/v3/system/manage/[action]")]
    public class HCSystemController
    {

        ManageSystem _ManageSystem;
        /// <summary>
        /// Saves the state of the list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveliststate")]
        public object SaveListState([FromBody] OAuth.Request _OAuthRequest)
        {
            OListState _Request = JsonConvert.DeserializeObject<OListState>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystem = new ManageSystem();
            OResponse _Response = _ManageSystem.SaveListState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Deletes the state of the list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteliststate")]
        public object DeleteListState([FromBody] OAuth.Request _OAuthRequest)
        {
            OListState _Request = JsonConvert.DeserializeObject<OListState>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystem = new ManageSystem();
            OResponse _Response = _ManageSystem.DeleteListState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the state of the list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getliststates")]
        public object GetListState([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystem = new ManageSystem();
            OResponse _Response = _ManageSystem.GetListState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
