//==================================================================================
// FileName: HCFaqController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for faq functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using Newtonsoft.Json;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.v3.System
{
    [Produces("application/json")]
    [Route("api/v3/sys/faq/[action]")]
    public class HCFaqController
    {
        ManageFaq _ManageFaq;

        /// <summary>
        /// Saves the FAQ category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savefaqcategory")]
        public object SaveFaqCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaqCategory.Manage _Request = JsonConvert.DeserializeObject<OFaqCategory.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.SaveFaqCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the FAQ category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatefaqcategory")]
        public object UpdateFaqCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaqCategory.Manage _Request = JsonConvert.DeserializeObject<OFaqCategory.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.UpdateFaqCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Deletes the FAQ category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletefaqcategory")]
        public object DeleteFaqCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaqCategory.Manage _Request = JsonConvert.DeserializeObject<OFaqCategory.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.DeleteFaqCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ category.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqcategory")]
        public object GetFaqCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaqCategory.Manage _Request = JsonConvert.DeserializeObject<OFaqCategory.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ category list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqcategories")]
        public object GetFaqCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ category client.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqcategoryclient")]
        public object GetFaqCategoryClient([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaqCategory.Manage _Request = JsonConvert.DeserializeObject<OFaqCategory.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqCategoryClient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ category clients.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqcategoriesclient")]
        public object GetFaqCategoryClients([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqCategoryClient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }




        /// <summary>
        /// Saves the FAQ.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savefaq")]
        public object SaveFaq([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaq.Manage _Request = JsonConvert.DeserializeObject<OFaq.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.SaveFaq(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Updates the FAQ.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatefaq")]
        public object UpdateFaq([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaq.Manage _Request = JsonConvert.DeserializeObject<OFaq.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.UpdateFaq(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Deletes the FAQ.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletefaq")]
        public object DeleteFaq([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaq.Manage _Request = JsonConvert.DeserializeObject<OFaq.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.DeleteFaq(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaq")]
        public object GetFaq([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaq.Manage _Request = JsonConvert.DeserializeObject<OFaq.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaq(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the faqs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqs")]
        public object GetFaqs([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaq(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ client.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqclient")]
        public object GetFaqClient([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OFaq.Manage _Request = JsonConvert.DeserializeObject<OFaq.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqClient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Gets the FAQ clients.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqsclient")]
        public object GetFaqClients([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqClient(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
    }
}
