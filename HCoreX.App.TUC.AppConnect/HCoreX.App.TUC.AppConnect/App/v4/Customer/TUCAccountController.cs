//==================================================================================
// FileName: TUCAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for accounts functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Operations.Console;
using HCore.TUC.Core.Operations.Customer;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.AppConnect.App.v4.Customer
{
    [Produces("application/json")]
    [Route("api/v4/customer/account/[action]")]
    public class TUCAccountController
    {
        ManageOperations _ManageOperations;
        /// <summary>
        /// Updates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepin")]
        public object UpdatePin([FromBody] OOperation.PinManager.Update.Request _Request)
        {
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.UpdatePin(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Resets the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpin")]
        public object ResetPin([FromBody] OOperation.PinManager.Update.Request _Request)
        {
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.ResetPin(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the App Logo
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getapplogo")]
        public object GetAppLogo([FromBody] OAdminUser.AuthUpdate.LogoRequest _Request)
        {

            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetAppLogo(_Request);
            return HCoreAuth.Auth_ResponseClear(_Response, _Request.UserReference);
        }

    }
}

