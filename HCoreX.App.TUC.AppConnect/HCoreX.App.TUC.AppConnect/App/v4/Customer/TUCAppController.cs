//==================================================================================
// FileName: TUCAppController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
using Microsoft.AspNetCore.Mvc;
using static HCore.ThankU.Object.OThankUCashApp.Response;

namespace HCoreX.App.TUC.AppConnect.App.v4.Customer
{
    public class OApp
    {
        public string? Version { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    [Produces("application/json")]
    [Route("api/v4/customer/app/[action]")]
    public class TUCAppController
    {
        /// <summary>
        /// Pings the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ping")]
        public object Ping([FromBody] OApp _Request)
        {
            var _Response = new OResponse
            {
                Status = "success",
                Message = "App connected successfully",
                Result = new
                {
                    Version = "2.0.2"
                },
                Mode = "test",
                ResponseCode = "200"
            };
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
        }

        ManageFaq _ManageFaq;
        /// <summary>
        /// Gets the FAQ category client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaqcategories")]
        public object GetFaqCategoryClient([FromBody] OList.Request _Request)
        {
            #region Build Response
            _Request.TypeId = HCoreConstant.Helpers.UserAccountType.Appuser;
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqCategoryClient(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the FAQ client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfaq")]
        public object GetFaqClient([FromBody] OList.Request _Request)
        {
            #region Build Response
            _ManageFaq = new ManageFaq();
            OResponse _Response = _ManageFaq.GetFaqClient(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
            #endregion
        }
    }
}
