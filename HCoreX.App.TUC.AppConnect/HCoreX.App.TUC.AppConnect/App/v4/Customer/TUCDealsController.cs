//==================================================================================
// FileName: TUCDealsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for deals functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash;
using HCore.TUC.Plugins.MadDeals;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.AppConnect.App.v4.Customer
{
    [Produces("application/json")]
    [Route("api/v4/customer/deals/[action]")]
    public class TUCDealsController
    {
        ManageDealNG _ManageDealNG;
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OList.Request _Request)
        {
            #region Build Response
            _ManageDealNG = new ManageDealNG();
            OResponse _Response = _ManageDealNG.GetCategories(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] HCore.TUC.Plugins.MadDeals.Object.OMadDealsNG.Deals.List.Request _Request)
        {
            #region Build Response
            _ManageDealNG = new ManageDealNG();
            OResponse _Response = _ManageDealNG.GetDeal(_Request);
            return HCoreAuth.Auth_ResponseJ(_Response, _Request.UserReference);
            #endregion
        }
    }
}
