﻿using System;
using System.Collections.Generic;
using HCore.Data.Logging.Models;
using Microsoft.EntityFrameworkCore;

namespace HCore.Data.Logging;

public partial class HCoreContextLogging : DbContext
{
    private readonly string ConnectionString;
    public HCoreContextLogging()
    {
        this.ConnectionString = Helper.HostHelper.HostLogging;
    }
    public HCoreContextLogging(string ConnectionString) : base()
    {
        this.ConnectionString = ConnectionString;
    }
    public HCoreContextLogging(DbContextOptions<HCoreContextLogging> options) : base(options)
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseNpgsql(ConnectionString);
        }
    }

    public virtual DbSet<HCLChangeLog> HCLChangeLog { get; set; }

    public virtual DbSet<HCLCoreLog> HCLCoreLog { get; set; }

    public virtual DbSet<HCLCoreNotification> HCLCoreNotification { get; set; }

    public virtual DbSet<HCLCoreUsage> HCLCoreUsage { get; set; }

    public virtual DbSet<HCLCoreUsageBnpl> HCLCoreUsageBnpl { get; set; }

    public virtual DbSet<HCLCron> HCLCron { get; set; }

    public virtual DbSet<HCLDBEvent> HCLDBEvent { get; set; }

    public virtual DbSet<HCLPushNotification> HCLPushNotification { get; set; }

    public virtual DbSet<HCLSmsNotification> HCLSmsNotification { get; set; }

    public virtual DbSet<HCXLCoreAppVersionUsage> HCXLCoreAppVersionUsage { get; set; }

    public virtual DbSet<hcl_app_version_token> hcl_app_version_token { get; set; }

    public virtual DbSet<hcl_coreusage_terminal> hcl_coreusage_terminal { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<HCLChangeLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165454_PRIMARY");

            entity.ToTable("HCLChangeLog", "tuc_logging");

            entity.HasIndex(e => e.Guid, "idx_165454_Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "idx_165454_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LocationName).HasMaxLength(254);
            entity.Property(e => e.Log).HasMaxLength(512);
            entity.Property(e => e.Title).HasMaxLength(215);
            entity.Property(e => e.VersionName).HasMaxLength(120);
        });

        modelBuilder.Entity<HCLCoreLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165461_PRIMARY");

            entity.ToTable("HCLCoreLog", "tuc_logging");

            entity.Property(e => e.HostName).HasMaxLength(128);
            entity.Property(e => e.Message).HasMaxLength(1024);
            entity.Property(e => e.Title).HasMaxLength(512);
        });

        modelBuilder.Entity<HCLCoreNotification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165468_PRIMARY");

            entity.ToTable("HCLCoreNotification", "tuc_logging");

            entity.Property(e => e.Attempts).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.BccAddress).HasMaxLength(2048);
            entity.Property(e => e.CcAddress).HasMaxLength(2048);
            entity.Property(e => e.Description).HasMaxLength(2048);
            entity.Property(e => e.FromAddress).HasMaxLength(512);
            entity.Property(e => e.FromName).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Priority).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(256);
            entity.Property(e => e.ReplyToAddress).HasMaxLength(512);
            entity.Property(e => e.ReplyToName).HasMaxLength(256);
            entity.Property(e => e.Subject).HasMaxLength(2048);
            entity.Property(e => e.ToAddress).HasMaxLength(2048);
            entity.Property(e => e.ToName).HasMaxLength(256);
        });

        modelBuilder.Entity<HCLCoreUsage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165477_PRIMARY");

            entity.ToTable("HCLCoreUsage", "tuc_logging");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IpAddress).HasMaxLength(64);
        });

        modelBuilder.Entity<HCLCoreUsageBnpl>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165484_PRIMARY");

            entity.ToTable("HCLCoreUsageBnpl", "tuc_logging");

            entity.Property(e => e.ApiName).HasMaxLength(128);
            entity.Property(e => e.ApiUrl).HasMaxLength(512);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Provider).HasMaxLength(64);
        });

        modelBuilder.Entity<HCLCron>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165491_PRIMARY");

            entity.ToTable("HCLCron", "tuc_logging");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Title).HasMaxLength(128);
        });

        modelBuilder.Entity<HCLDBEvent>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165496_PRIMARY");

            entity.ToTable("HCLDBEvent", "tuc_logging");

            entity.HasIndex(e => e.Id, "idx_165496_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Title).HasMaxLength(64);
        });

        modelBuilder.Entity<HCLPushNotification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165501_PRIMARY");

            entity.ToTable("HCLPushNotification", "tuc_logging");

            entity.Property(e => e.DestinationUrl).HasMaxLength(2048);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.Task).HasMaxLength(128);
            entity.Property(e => e.Title).HasMaxLength(256);
        });

        modelBuilder.Entity<HCLSmsNotification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165508_PRIMARY");

            entity.ToTable("HCLSmsNotification", "tuc_logging");

            entity.HasIndex(e => e.Id, "idx_165508_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.ReferenceGroupName).HasMaxLength(128);
            entity.Property(e => e.ReferenceMessage).HasMaxLength(512);
            entity.Property(e => e.ReferenceName).HasMaxLength(128);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.Source).HasMaxLength(64);
            entity.Property(e => e.StatusCode).HasMaxLength(64);
            entity.Property(e => e.SystemReference).HasMaxLength(64);
        });

        modelBuilder.Entity<HCXLCoreAppVersionUsage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165515_PRIMARY");

            entity.ToTable("HCXLCoreAppVersionUsage", "tuc_logging");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IpAddress).HasMaxLength(16);
            entity.Property(e => e.StatusMessage).HasMaxLength(512);
        });

        modelBuilder.Entity<hcl_app_version_token>(entity =>
        {
            entity.HasKey(e => e.id).HasName("hcl_app_version_token_pkey");

            entity.ToTable("hcl_app_version_token", "tuc_logging");

            entity.Property(e => e.token).HasMaxLength(512);
            entity.Property(e => e.token_padding).HasMaxLength(128);
        });

        modelBuilder.Entity<hcl_coreusage_terminal>(entity =>
        {
            entity.HasKey(e => e.id).HasName("hcl_coreusage_terminal_pkey");

            entity.ToTable("hcl_coreusage_terminal", "tuc_logging");

            entity.Property(e => e.id).ValueGeneratedNever();
            entity.Property(e => e.guid).HasMaxLength(64);
            entity.Property(e => e.ip_address).HasMaxLength(32);
            entity.Property(e => e.status_message).HasMaxLength(256);
            entity.Property(e => e.terminal_id).HasMaxLength(16);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
