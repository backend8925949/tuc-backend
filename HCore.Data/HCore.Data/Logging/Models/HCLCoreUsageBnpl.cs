﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLCoreUsageBnpl
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string? Provider { get; set; }

    public string? ApiName { get; set; }

    public string? ApiUrl { get; set; }

    public string? Request { get; set; }

    public string? Response { get; set; }

    public DateTime? RequestTime { get; set; }
}
