﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLDBEvent
{
    public long Id { get; set; }

    public string? Title { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public long? StatusId { get; set; }

    public long? Count { get; set; }
}
