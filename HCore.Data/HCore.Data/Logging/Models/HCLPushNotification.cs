﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLPushNotification
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long TypeId { get; set; }

    public string? DestinationUrl { get; set; }

    public string? MobileNumber { get; set; }

    public string? Task { get; set; }

    public string? Title { get; set; }

    public string? Message { get; set; }

    public string? Content { get; set; }

    public long? CreatedById { get; set; }

    public long StatusId { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? ResponseContent { get; set; }

    public DateTime CreateDate { get; set; }
}
