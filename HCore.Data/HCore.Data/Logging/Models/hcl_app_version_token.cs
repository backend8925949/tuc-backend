﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class hcl_app_version_token
{
    public long id { get; set; }

    public long? app_id { get; set; }

    public long account_id { get; set; }

    public long? appversion_id { get; set; }

    public string token { get; set; } = null!;

    public string? token_padding { get; set; }

    public DateTime create_date { get; set; }

    public DateTime? expiary_date { get; set; }

    public DateTime? expired_on { get; set; }

    public DateTime? last_request_date { get; set; }

    public int status_id { get; set; }
}
