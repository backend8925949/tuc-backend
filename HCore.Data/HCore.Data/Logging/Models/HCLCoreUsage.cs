﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLCoreUsage
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AppVersionId { get; set; }

    public long? ApiId { get; set; }

    public long? FeatureId { get; set; }

    public long? AccountId { get; set; }

    public long? SessionId { get; set; }

    public string? IpAddress { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public string? Request { get; set; }

    public string? Response { get; set; }

    public DateTime RequestTime { get; set; }

    public DateTime? ResponseTime { get; set; }

    public double? TimeDifference { get; set; }

    public long? StatusId { get; set; }
}
