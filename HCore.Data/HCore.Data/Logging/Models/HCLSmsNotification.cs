﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLSmsNotification
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string Source { get; set; } = null!;

    public string MobileNumber { get; set; } = null!;

    public string? Message { get; set; }

    public string? ReferenceNumber { get; set; }

    public long? ReferenceGroupId { get; set; }

    public string? ReferenceGroupName { get; set; }

    public string? ReferenceName { get; set; }

    public string? ReferenceMessage { get; set; }

    public DateTime? SendDate { get; set; }

    public DateTime? DeliveryDate { get; set; }

    public string? StatusCode { get; set; }

    public string? Data { get; set; }

    public DateTime? CreateDate { get; set; }

    public double? Amount { get; set; }

    public string? DeliveryResponse { get; set; }

    public string? SystemReference { get; set; }

    public long? TransactionId { get; set; }
}
