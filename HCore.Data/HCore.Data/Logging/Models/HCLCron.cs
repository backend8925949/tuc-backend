﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLCron
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? SiteId { get; set; }

    public string Title { get; set; } = null!;

    public DateTime StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public string? Comment { get; set; }

    public long StatusId { get; set; }
}
