﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLChangeLog
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Title { get; set; }

    public string? LocationName { get; set; }

    public string? Log { get; set; }

    public string? Comment { get; set; }

    public DateTime? ReleaseDate { get; set; }

    public string? VersionName { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long? StatusId { get; set; }
}
