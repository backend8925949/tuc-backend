﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class HCLCoreNotification
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? TypeId { get; set; }

    public long? ParentId { get; set; }

    public long? GatewayId { get; set; }

    public long? CategoryId { get; set; }

    public long? TemplateId { get; set; }

    public long? AccountId { get; set; }

    public long? FromUserAccountId { get; set; }

    public long? FromDeviceId { get; set; }

    public long? ToUserAccountId { get; set; }

    public long? ToUserDeviceId { get; set; }

    public long? Priority { get; set; }

    public string? ToName { get; set; }

    public string? ToAddress { get; set; }

    public string? FromName { get; set; }

    public string? FromAddress { get; set; }

    public string? ReplyToName { get; set; }

    public string? ReplyToAddress { get; set; }

    public string? CcAddress { get; set; }

    public string? BccAddress { get; set; }

    public string? Subject { get; set; }

    public string Message { get; set; } = null!;

    public DateTime SendDate { get; set; }

    public DateTime? ActualSendDate { get; set; }

    public DateTime? OpenDate { get; set; }

    public long Attempts { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? Description { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long StatusId { get; set; }
}
