﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class hcl_coreusage_terminal
{
    public long id { get; set; }

    public string guid { get; set; } = null!;

    public string? terminal_id { get; set; }

    public long? terminal_reference_id { get; set; }

    public long? appversion_id { get; set; }

    public long? api_id { get; set; }

    public string? ip_address { get; set; }

    public string? request { get; set; }

    public string? response { get; set; }

    public DateTime? request_time { get; set; }

    public DateTime? response_time { get; set; }

    public long? status_id { get; set; }

    public string? status_message { get; set; }
}
