//==================================================================================
// FileName: TUCWemaCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HCore.Data.Integration
{
    public partial class TUCWemaCustomer
    {
        public long Id { get; set; }
        public long? AccountId { get; set; }
        public int? IsSmsSent { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Gender { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime SyncTime { get; set; }
        public string SMSMessageId { get; set; }
        public string SMSStatusId { get; set; }
        public string SMSStatusName { get; set; }
        public string SMSReceiver { get; set; }
        public string SMSGroupId { get; set; }
        public string SMSStatusDescription { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string NotificationResponse { get; set; }
        public int? IsAlreadyExist { get; set; }
    }
}
