//==================================================================================
// FileName: HCoreContextIntegration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HCore.Data.Integration
{
    public partial class HCoreContextIntegration : DbContext
    {
        public HCoreContextIntegration()
        {
        }

        public HCoreContextIntegration(DbContextOptions<HCoreContextIntegration> options)
            : base(options)
        {
        }

        public virtual DbSet<TUCWemaCustomer> TUCWemaCustomer { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=tuc-m-core.cd6dp6shun6m.eu-west-2.rds.amazonaws.com;user id=tucsync;password=ThankUCash@75885658342021;database=live_hc_tuc_integration", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.23-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_0900_ai_ci");

            modelBuilder.Entity<TUCWemaCustomer>(entity =>
            {
                entity.Property(e => e.EmailAddress).HasMaxLength(512);

                entity.Property(e => e.FirstName).HasMaxLength(128);

                entity.Property(e => e.Gender).HasMaxLength(16);

                entity.Property(e => e.IsAlreadyExist).HasDefaultValueSql("'0'");

                entity.Property(e => e.IsSmsSent).HasDefaultValueSql("'0'");

                entity.Property(e => e.LastName).HasMaxLength(128);

                entity.Property(e => e.MobileNumber).HasMaxLength(128);

                entity.Property(e => e.RegistrationDate).HasColumnType("datetime");

                entity.Property(e => e.SMSGroupId).HasMaxLength(128);

                entity.Property(e => e.SMSMessageId).HasMaxLength(128);

                entity.Property(e => e.SMSReceiver).HasMaxLength(128);

                entity.Property(e => e.SMSStatusDescription).HasMaxLength(256);

                entity.Property(e => e.SMSStatusId).HasMaxLength(128);

                entity.Property(e => e.SMSStatusName).HasMaxLength(128);

                entity.Property(e => e.SyncTime)
                    .HasColumnType("timestamp")
                    .ValueGeneratedOnAddOrUpdate()
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
