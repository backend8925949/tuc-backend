﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOMerchantUpload
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OwnerId { get; set; }

    public string? DisplayName { get; set; }

    public string? Name { get; set; }

    public string? ContactNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? Address { get; set; }

    public string? CityAreaName { get; set; }

    public string? CityName { get; set; }

    public string? StateName { get; set; }

    public string? CountryName { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public long? AccountId { get; set; }

    public string? CategoryName { get; set; }

    public string? ContactPersonName { get; set; }

    public string? ContactPersonMobileNumber { get; set; }

    public string? ContactPersonEmailAddress { get; set; }

    public string? ReferenceNumber { get; set; }

    public double? RewardPercentage { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long? StatusId { get; set; }

    public string? StatusMessage { get; set; }
}
