﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOUploadTerminal
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? AccountId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? AcquirerId { get; set; }

    public long? ProviderId { get; set; }

    public long? CashierId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long? StatusId { get; set; }

    public long? ImportStatusId { get; set; }

    public string? MerchantName { get; set; }

    public string? StoreName { get; set; }

    public string? AcquirerName { get; set; }

    public string? ProviderName { get; set; }

    public string? CashierName { get; set; }

    public string? Comment { get; set; }
}
