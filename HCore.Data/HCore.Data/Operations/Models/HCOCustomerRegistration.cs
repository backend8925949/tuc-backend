﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOCustomerRegistration
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long CountryId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? EmailAddress { get; set; }

    public string? MobileNumber { get; set; }

    public string? Source { get; set; }

    public string? ReferralCode { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? StatusId { get; set; }

    public string? DeviceType { get; set; }

    public string? Browser { get; set; }

    public string? BrowserVersion { get; set; }

    public string? Device { get; set; }

    public string? Os { get; set; }

    public string? OsVersion { get; set; }

    public string? UserAgent { get; set; }

    public string? IpAddress { get; set; }

    public long? AccountId { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public string? App { get; set; }
}
