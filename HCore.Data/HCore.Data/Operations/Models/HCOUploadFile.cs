﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOUploadFile
{
    public long Id { get; set; }

    public long TypeId { get; set; }

    public long OwnerId { get; set; }

    public string Name { get; set; } = null!;

    public string? FilePath { get; set; }

    public long TotalRecord { get; set; }

    public long Pending { get; set; }

    public long Processing { get; set; }

    public long Completed { get; set; }

    public long Success { get; set; }

    public long Error { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long? StatusId { get; set; }
}
