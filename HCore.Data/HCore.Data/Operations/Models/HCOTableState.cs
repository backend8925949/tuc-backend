﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOTableState
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccessTypeId { get; set; }

    public long? AccountId { get; set; }

    public string? AccountKey { get; set; }

    public string? SystemName { get; set; }

    public string? Data { get; set; }

    public DateTime? CreateDate { get; set; }
}
