﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOUploadCustomerReward
{
    public long Id { get; set; }

    public long MerchantId { get; set; }

    public long? MerchantCountryId { get; set; }

    public string? AccountNumber { get; set; }

    public string? Name { get; set; }

    public string? FirstName { get; set; }

    public string? MiddleName { get; set; }

    public string? LastName { get; set; }

    public string? MobileNumber { get; set; }

    public string? FormattedMobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? Gender { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? Comment { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public long? StatusId { get; set; }

    public string? StatusMessage { get; set; }

    public long? AccountId { get; set; }

    public long? AccountCountryId { get; set; }

    public string? AccountCountryIsd { get; set; }

    public double? InvoiceAmount { get; set; }

    public double? RewardAmount { get; set; }

    public long? TransactionId { get; set; }

    public string? TransactionReference { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public long? TerminalId { get; set; }

    public long? FileId { get; set; }

    public string? MerchantDisplayName { get; set; }

    public long? MerchantCategoryId { get; set; }

    public double? MerchantCategoryComission { get; set; }

    public long? IsNotificationSent { get; set; }

    public int? IsSmsSent { get; set; }

    public int? IsEmailSent { get; set; }

    public double? TucComissionAmount { get; set; }

    public double? TucComissionAmountPercentage { get; set; }

    public double? CustomerRewardAmount { get; set; }

    public double? CustomerRewardPercentage { get; set; }

    public string? TucComissionSource { get; set; }

    public double? MerchantTransactionTypeId { get; set; }

    public double? MerchantTransactionSourceId { get; set; }

    public double? CustomerTransactionTypeId { get; set; }

    public double? CustomerTransactionSourceId { get; set; }

    public int? TransactionStatusId { get; set; }
}
