﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCTMerchantOnboarding
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? EmailAddress { get; set; }

    public string? Password { get; set; }

    public string? EmailOtp { get; set; }

    public short? IsEmailVerified { get; set; }

    public string? EmailCode { get; set; }

    public DateTime? EmailVerificationDate { get; set; }

    public string? DisplayName { get; set; }

    public string? Categories { get; set; }

    public long? CountryId { get; set; }

    public string? MobileNumber { get; set; }

    public string? MobileOtp { get; set; }

    public short? IsMobileVerified { get; set; }

    public string? MobileVerificationToken { get; set; }

    public DateTime? MobileVerificationDate { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public string? MapAddress { get; set; }

    public string? Address { get; set; }

    public long? SubscriptionId { get; set; }

    public DateTime? CreateDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? StatusId { get; set; }

    public string? SubscriptionKey { get; set; }

    public long? AccountId { get; set; }

    public string? Source { get; set; }

    public string? Host { get; set; }

    public string? CountryIsd { get; set; }

    public string? ReferralCode { get; set; }

    public string? Name { get; set; }
}
