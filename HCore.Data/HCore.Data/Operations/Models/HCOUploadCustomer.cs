﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Operations.Models;

public partial class HCOUploadCustomer
{
    public long Id { get; set; }

    public long OwnerId { get; set; }

    public string? Name { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? MobileNumber { get; set; }

    public string? FormattedMobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? Gender { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime? CreateDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public string? Comment { get; set; }

    public long? StatusId { get; set; }

    public string? StatusMessage { get; set; }

    public long? CreatedById { get; set; }

    public long? ModifyById { get; set; }

    public long? FileId { get; set; }

    public long? AccountId { get; set; }
}
