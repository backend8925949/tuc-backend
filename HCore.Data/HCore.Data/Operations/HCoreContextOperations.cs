﻿using System;
using System.Collections.Generic;
using HCore.Data.Operations.Models;
using Microsoft.EntityFrameworkCore;

namespace HCore.Data.Operations;

public partial class HCoreContextOperations : DbContext
{
    private readonly string ConnectionString;
    public HCoreContextOperations()
    {
        this.ConnectionString = Helper.HostHelper.HostOperations;
    }
    public HCoreContextOperations(string ConnectionString) : base()
    {
        this.ConnectionString = ConnectionString;
    }
    public HCoreContextOperations(DbContextOptions<HCoreContextOperations> options) : base(options)
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseNpgsql(ConnectionString);
        }
    }

    public virtual DbSet<HCOCustomerRegistration> HCOCustomerRegistration { get; set; }

    public virtual DbSet<HCOMerchantUpload> HCOMerchantUpload { get; set; }

    public virtual DbSet<HCOTableState> HCOTableState { get; set; }

    public virtual DbSet<HCOUploadCustomer> HCOUploadCustomer { get; set; }

    public virtual DbSet<HCOUploadCustomerReward> HCOUploadCustomerReward { get; set; }

    public virtual DbSet<HCOUploadFile> HCOUploadFile { get; set; }

    public virtual DbSet<HCOUploadTerminal> HCOUploadTerminal { get; set; }

    public virtual DbSet<HCTMerchantOnboarding> HCTMerchantOnboarding { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<HCOCustomerRegistration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165071_PRIMARY");

            entity.ToTable("HCOCustomerRegistration", "tuc_operations");

            entity.HasIndex(e => e.Guid, "idx_165071_Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "idx_165071_Id_UNIQUE").IsUnique();

            entity.Property(e => e.App).HasMaxLength(128);
            entity.Property(e => e.Browser).HasMaxLength(128);
            entity.Property(e => e.BrowserVersion).HasMaxLength(128);
            entity.Property(e => e.Device).HasMaxLength(128);
            entity.Property(e => e.DeviceType).HasMaxLength(128);
            entity.Property(e => e.EmailAddress).HasMaxLength(512);
            entity.Property(e => e.FirstName).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IpAddress).HasMaxLength(64);
            entity.Property(e => e.LastName).HasMaxLength(256);
            entity.Property(e => e.MobileNumber).HasMaxLength(32);
            entity.Property(e => e.Os).HasMaxLength(128);
            entity.Property(e => e.OsVersion).HasMaxLength(128);
            entity.Property(e => e.ReferralCode).HasMaxLength(128);
            entity.Property(e => e.Source).HasMaxLength(128);
            entity.Property(e => e.UserAgent).HasMaxLength(512);
        });

        modelBuilder.Entity<HCOMerchantUpload>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165110_PRIMARY");

            entity.ToTable("HCOMerchantUpload", "tuc_operations");

            entity.HasIndex(e => e.Guid, "idx_165110_Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "idx_165110_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Address).HasMaxLength(256);
            entity.Property(e => e.CategoryName).HasMaxLength(128);
            entity.Property(e => e.CityAreaName).HasMaxLength(256);
            entity.Property(e => e.CityName).HasMaxLength(256);
            entity.Property(e => e.ContactNumber).HasMaxLength(256);
            entity.Property(e => e.ContactPersonEmailAddress).HasMaxLength(256);
            entity.Property(e => e.ContactPersonMobileNumber).HasMaxLength(256);
            entity.Property(e => e.ContactPersonName).HasMaxLength(256);
            entity.Property(e => e.CountryName).HasMaxLength(256);
            entity.Property(e => e.DisplayName).HasMaxLength(30);
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.StateName).HasMaxLength(256);
            entity.Property(e => e.StatusMessage).HasMaxLength(128);
        });

        modelBuilder.Entity<HCOTableState>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165141_PRIMARY");

            entity.ToTable("HCOTableState", "tuc_operations");

            entity.HasIndex(e => e.Id, "idx_155252_Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "idx_165141_Id_UNIQUE").IsUnique();

            entity.Property(e => e.AccountKey).HasMaxLength(64);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.SystemName).HasMaxLength(128);
        });

        modelBuilder.Entity<HCOUploadCustomer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165148_PRIMARY");

            entity.ToTable("HCOUploadCustomer", "tuc_operations");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.FirstName).HasMaxLength(64);
            entity.Property(e => e.FormattedMobileNumber).HasMaxLength(16);
            entity.Property(e => e.Gender).HasMaxLength(32);
            entity.Property(e => e.LastName).HasMaxLength(64);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.StatusMessage).HasMaxLength(128);
        });

        modelBuilder.Entity<HCOUploadCustomerReward>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165155_PRIMARY");

            entity.ToTable("HCOUploadCustomerReward", "tuc_operations");

            entity.Property(e => e.AccountCountryIsd).HasMaxLength(16);
            entity.Property(e => e.AccountNumber).HasMaxLength(64);
            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.FirstName).HasMaxLength(64);
            entity.Property(e => e.FormattedMobileNumber).HasMaxLength(16);
            entity.Property(e => e.Gender).HasMaxLength(32);
            entity.Property(e => e.LastName).HasMaxLength(64);
            entity.Property(e => e.MerchantDisplayName).HasMaxLength(128);
            entity.Property(e => e.MiddleName).HasMaxLength(64);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.StatusMessage).HasMaxLength(128);
            entity.Property(e => e.TransactionReference).HasMaxLength(64);
            entity.Property(e => e.TucComissionSource).HasMaxLength(128);
        });

        modelBuilder.Entity<HCOUploadFile>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165162_PRIMARY");

            entity.ToTable("HCOUploadFile", "tuc_operations");

            entity.HasIndex(e => e.Id, "idx_165162_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Completed).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.Error).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.FilePath).HasMaxLength(1024);
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.Pending).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.Processing).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.Success).HasDefaultValueSql("'0'::bigint");
            entity.Property(e => e.TotalRecord).HasDefaultValueSql("'0'::bigint");
        });

        modelBuilder.Entity<HCOUploadTerminal>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165175_PRIMARY");

            entity.ToTable("HCOUploadTerminal", "tuc_operations");

            entity.Property(e => e.AcquirerName).HasMaxLength(256);
            entity.Property(e => e.CashierName).HasMaxLength(256);
            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MerchantName).HasMaxLength(256);
            entity.Property(e => e.ProviderName).HasMaxLength(256);
            entity.Property(e => e.StoreName).HasMaxLength(256);
        });

        modelBuilder.Entity<HCTMerchantOnboarding>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("idx_165182_PRIMARY");

            entity.ToTable("HCTMerchantOnboarding", "tuc_operations");

            entity.HasIndex(e => e.Id, "idx_165182_Id_UNIQUE").IsUnique();

            entity.Property(e => e.Address).HasMaxLength(128);
            entity.Property(e => e.Categories).HasMaxLength(256);
            entity.Property(e => e.CountryIsd).HasMaxLength(32);
            entity.Property(e => e.DisplayName).HasMaxLength(256);
            entity.Property(e => e.EmailAddress).HasMaxLength(128);
            entity.Property(e => e.EmailCode).HasMaxLength(64);
            entity.Property(e => e.EmailOtp).HasMaxLength(16);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Host).HasMaxLength(512);
            entity.Property(e => e.MapAddress).HasMaxLength(128);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.MobileOtp).HasMaxLength(6);
            entity.Property(e => e.MobileVerificationToken).HasMaxLength(64);
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.Password).HasMaxLength(128);
            entity.Property(e => e.ReferralCode).HasMaxLength(128);
            entity.Property(e => e.Source).HasMaxLength(128);
            entity.Property(e => e.SubscriptionKey).HasMaxLength(64);
        });


        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
