//==================================================================================
// FileName: ussdverification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HCore.Data.Ussd
{
    public partial class ussdverification
    {
        public long Id { get; set; }
        public string Guid { get; set; }
        public string SessionId { get; set; }
        public string SessionCode { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public int StatusId { get; set; }
        public DateTime? VerificationDate { get; set; }
        public string AppSessionId { get; set; }
        public string DeviceId { get; set; }
    }
}
