//==================================================================================
// FileName: operation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HCore.Data.Ussd
{
    public partial class operation
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
