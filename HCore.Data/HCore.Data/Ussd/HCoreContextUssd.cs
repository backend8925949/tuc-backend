//==================================================================================
// FileName: HCoreContextUssd.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HCore.Data.Ussd
{
    public partial class HCoreContextUssd : DbContext
    {
        public HCoreContextUssd()
        {
        }

        public HCoreContextUssd(DbContextOptions<HCoreContextUssd> options)
            : base(options)
        {
        }

        public virtual DbSet<audit> audit { get; set; }
        public virtual DbSet<operation> operation { get; set; }
        public virtual DbSet<sessionstage> sessionstage { get; set; }
        public virtual DbSet<sessionstate> sessionstate { get; set; }
        public virtual DbSet<ussdex> ussdex { get; set; }
        public virtual DbSet<ussdsessions> ussdsessions { get; set; }
        public virtual DbSet<ussdverification> ussdverification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=tuc-m-core.cd6dp6shun6m.eu-west-2.rds.amazonaws.com;user id=tucsync;password=ThankUCash@75885658342021;database=tuc_ussd_live;sslmode=none;treattinyasboolean=true", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.23-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_0900_ai_ci");

            modelBuilder.Entity<audit>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("audit");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.DataString)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasMaxLength(70)
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.NetworkCode)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.Operation)
                    .IsRequired()
                    .HasMaxLength(45)
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.ServiceCode)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.SessionEndAt)
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnType("text")
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.Stage)
                    .IsRequired()
                    .HasMaxLength(45)
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(45)
                    .UseCollation("latin1_swedish_ci")
                    .HasCharSet("latin1");
            });

            modelBuilder.Entity<operation>(entity =>
            {
                entity.HasCharSet("latin1")
                    .UseCollation("latin1_swedish_ci");

                entity.HasIndex(e => e.Code, "Code_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "Name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<sessionstage>(entity =>
            {
                entity.HasCharSet("latin1")
                    .UseCollation("latin1_swedish_ci");

                entity.HasIndex(e => e.Code, "Code_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "Name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<sessionstate>(entity =>
            {
                entity.HasCharSet("latin1")
                    .UseCollation("latin1_swedish_ci");

                entity.HasIndex(e => e.Code, "Code_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "Name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<ussdex>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(1024);
            });

            modelBuilder.Entity<ussdsessions>(entity =>
            {
                entity.HasCharSet("latin1")
                    .UseCollation("latin1_swedish_ci");

                entity.HasIndex(e => e.Guid, "Guid_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Id_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.CreatedAt).HasColumnType("text");

                entity.Property(e => e.DataString).HasColumnType("text");

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.ModifiedAt).HasColumnType("text");

                entity.Property(e => e.ModifiedBy).HasColumnType("text");

                entity.Property(e => e.NetworkCode).HasColumnType("text");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.ServiceCode).HasColumnType("text");

                entity.Property(e => e.SessionEndAt).HasColumnType("text");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<ussdverification>(entity =>
            {
                entity.HasIndex(e => e.CreateDate, "in_createdate");

                entity.HasIndex(e => e.Id, "in_id");

                entity.HasIndex(e => e.MobileNumber, "in_mobilenumber");

                entity.HasIndex(e => e.StatusId, "in_statusid");

                entity.HasIndex(e => e.VerificationDate, "in_verificationdate");

                entity.Property(e => e.AppSessionId).HasMaxLength(64);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeviceId).HasMaxLength(64);

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.MobileNumber)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.Property(e => e.SessionCode)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.Property(e => e.SessionId).HasMaxLength(128);

                entity.Property(e => e.StatusId).HasDefaultValueSql("'1'");

                entity.Property(e => e.VerificationDate).HasColumnType("datetime");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
