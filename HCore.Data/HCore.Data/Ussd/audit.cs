//==================================================================================
// FileName: audit.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HCore.Data.Ussd
{
    public partial class audit
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Phone { get; set; }
        public string SessionId { get; set; }
        public string ServiceCode { get; set; }
        public string NetworkCode { get; set; }
        public string DataString { get; set; }
        public string Status { get; set; }
        public string Stage { get; set; }
        public int SyncFailCount { get; set; }
        public string CreatedAt { get; set; }
        public string ModifiedAt { get; set; }
        public string ModifiedBy { get; set; }
        public string SessionEndAt { get; set; }
        public string Operation { get; set; }
    }
}
