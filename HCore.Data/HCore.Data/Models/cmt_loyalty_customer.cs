﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class cmt_loyalty_customer
{
    public long id { get; set; }

    public string guid { get; set; } = null!;

    public long? program_id { get; set; }

    public long owner_id { get; set; }

    public long customer_id { get; set; }

    public int source_id { get; set; }

    public double credit { get; set; }

    public double debit { get; set; }

    public double balance { get; set; }

    public double invoice_amount { get; set; }

    public long? total_transactions { get; set; }

    public DateTime? last_transaction_date { get; set; }

    public DateTime create_date { get; set; }

    public DateTime? modify_date { get; set; }

    public int status_id { get; set; }

    public DateTime sync_time { get; set; }

    public virtual HCUAccount customer { get; set; } = null!;

    public virtual HCUAccount owner { get; set; } = null!;

    public virtual HCCore source { get; set; } = null!;

    public virtual HCCore status { get; set; } = null!;
}
