﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSPackages
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? AccountId { get; set; }

    public long? DealId { get; set; }

    public double? Height { get; set; }

    public double? Length { get; set; }

    public string? ProductName { get; set; }

    public string? SizeUnit { get; set; }

    public string? Type { get; set; }

    public double? Weight { get; set; }

    public string? WeightUnit { get; set; }

    public double? Width { get; set; }

    public string? PackagingId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public string? TId { get; set; }

    public int? StatusId { get; set; }

    public string? Description { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual MDDeal? Deal { get; set; }

   public virtual ICollection<LSParcel> LSParcel { get; set; } = new List<LSParcel>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }
}
