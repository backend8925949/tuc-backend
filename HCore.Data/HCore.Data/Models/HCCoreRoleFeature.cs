﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreRoleFeature
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? RoleId { get; set; }

    public long? FeatureId { get; set; }

    public bool? isAdd { get; set; }

    public bool? isView { get; set; }

    public bool? isDelete { get; set; }

    public bool? isUpdate { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public bool? isViewAll { get; set; }

    public virtual HCCoreFeature? Feature { get; set; }

    public virtual HCCoreRole? Role { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
