﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealPromotionClick
{
    public long Id { get; set; }

    public long? AccountId { get; set; }

    public long PromotionId { get; set; }

    public DateTime CreateDate { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual MDDealPromotion Promotion { get; set; } = null!;
}
