﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountSession
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? PlatformId { get; set; }

    public long? AccountId { get; set; }

    public long? DeviceId { get; set; }

    public long AppVersionId { get; set; }

    public DateTime LoginDate { get; set; }

    public DateTime LastActivityDate { get; set; }

    public DateTime? LogoutDate { get; set; }

    public string? IPAddress { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public string? NotificationUrl { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccountDevice? Device { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Platform { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
