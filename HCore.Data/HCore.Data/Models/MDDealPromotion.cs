﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealPromotion
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ImageStorageId { get; set; }

    public long? DealId { get; set; }

    public string? Url { get; set; }

    public string? Location { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public int? TypeId { get; set; }

    public int? CountryId { get; set; }

    public virtual HCCoreCountry? Country { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual MDDeal? Deal { get; set; }

    public virtual HCCoreStorage? ImageStorage { get; set; }

   public virtual ICollection<MDDealPromotionClick> MDDealPromotionClick { get; set; } = new List<MDDealPromotionClick>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? Type { get; set; }
}
