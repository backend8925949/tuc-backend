﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCBranchRmTarget
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? BranchId { get; set; }

    public long ManagerId { get; set; }

    public long RmId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public double? TotalTarget { get; set; }

    public double? AchievedTarget { get; set; }

    public double? RemainingTarget { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public virtual TUCBranch? Branch { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount Manager { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount Rm { get; set; } = null!;

    public virtual HCCore? Status { get; set; }
}
