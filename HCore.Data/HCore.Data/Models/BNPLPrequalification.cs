﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLPrequalification
{
    public int Id { get; set; }

    public string? Guid { get; set; }

    public long? AccountId { get; set; }

    public long? LoanId { get; set; }

    public string? BankCode { get; set; }

    public string? BankName { get; set; }

    public string? BankType { get; set; }

    public string? BankAccountNumber { get; set; }

    public long? Yearly_Income { get; set; }

    public long? Monthly_Income { get; set; }

    public long? Average_Income { get; set; }

    public int? Income_Sources { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyByid { get; set; }

    public int? StatusId { get; set; }

    public virtual BNPLAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual BNPLLoanProcess? Loan { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }
}
