﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountSubscription
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long AccountId { get; set; }

    public long SubscriptionId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public DateTime RenewDate { get; set; }

    public double? ActualPrice { get; set; }

    public double? SellingPrice { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public string? PaymentReference { get; set; }

    public long? TransactionId { get; set; }

    public long? Value { get; set; }

    public string? AuthorizationCode { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeature { get; set; } = new List<HCUAccountSubscriptionFeature>();

   public virtual ICollection<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPayment { get; set; } = new List<HCUAccountSubscriptionPayment>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCSubscription Subscription { get; set; } = null!;

    public virtual HCCore Type { get; set; } = null!;
}
