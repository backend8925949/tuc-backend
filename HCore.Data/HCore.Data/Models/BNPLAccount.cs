﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

/// <summary>
/// This table manages bnpl activated accounts
/// </summary>
public partial class BNPLAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public double? CreditLimit { get; set; }

    public long VenderUserId { get; set; }

    public string? ProviderReference { get; set; }

    public DateTime? CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? EmailAddress { get; set; }

    public string? WorkEmailAddress { get; set; }

    public string? MobileNumber { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public string? BvnNumber { get; set; }

    public double? Salary { get; set; }

    public int? SalaryDay { get; set; }

    public string? Address { get; set; }

    public string? StateName { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public string? LgaName { get; set; }

    public string? MonoReferenceNumber { get; set; }

    public double? MonoReferenceBalance { get; set; }

    public string? BankName { get; set; }

    public string? BankCode { get; set; }

    public string? BankType { get; set; }

    public string? BankAccountName { get; set; }

    public string? BankAccountNumber { get; set; }

    public double? AvailableCreditLimit { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoan { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLPrequalification> BNPLPrequalification { get; set; } = new List<BNPLPrequalification>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
