﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCPromoCode
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int CountryId { get; set; }

    public int AccountTypeId { get; set; }

    public int ConditionId { get; set; }

    public int? TypeId { get; set; }

    public string? Title { get; set; }

    public string? Description { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public string Code { get; set; } = null!;

    public double Value { get; set; }

    public double Budget { get; set; }

    public long MaximumLimit { get; set; }

    public int MaximumLimitPerUser { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCCore AccountType { get; set; } = null!;

    public virtual TUCPromoCodeCondition Condition { get; set; } = null!;

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCPromoCodeAccount> TUCPromoCodeAccount { get; set; } = new List<TUCPromoCodeAccount>();

    public virtual HCCore? Type { get; set; }
}
