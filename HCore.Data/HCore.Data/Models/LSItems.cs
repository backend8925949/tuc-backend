﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSItems
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? ParcelId { get; set; }

    public string? Description { get; set; }

    public string? Name { get; set; }

    public string? Currency { get; set; }

    public long? Price { get; set; }

    public long? Quantity { get; set; }

    public double? Weight { get; set; }

    public virtual LSParcel? Parcel { get; set; }
}
