﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCampaignAudience
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long CampaignId { get; set; }

    public long? UserAccountId { get; set; }

    public long? CardTypeId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual TUCampaign Campaign { get; set; } = null!;

    public virtual HCCoreParameter? CardType { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore Type { get; set; } = null!;

    public virtual HCUAccount? UserAccount { get; set; }
}
