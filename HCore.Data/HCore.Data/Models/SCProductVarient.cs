﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCProductVarient
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long ProductId { get; set; }

    public string Sku { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public long MinimumQuantity { get; set; }

    public long MaximumQuantity { get; set; }

    public double ActualPrice { get; set; }

    public double SellingPrice { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? BarcodeNumber { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual SCProduct Product { get; set; } = null!;

   public virtual ICollection<SCOrderItem> SCOrderItem { get; set; } = new List<SCOrderItem>();

   public virtual ICollection<SCProductVarientStock> SCProductVarientStock { get; set; } = new List<SCProductVarientStock>();

    public virtual HCCore Status { get; set; } = null!;
}
