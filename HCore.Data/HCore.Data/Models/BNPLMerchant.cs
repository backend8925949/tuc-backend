﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLMerchant
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public int SettelmentTypeId { get; set; }

    public long? SettelmentDays { get; set; }

    public sbyte? IsMerchantSettelment { get; set; }

    public int TransactionSourceId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifiyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoan { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlement { get; set; } = new List<BNPLMerchantSettlement>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifiyBy { get; set; }

    public virtual HCCore SettelmentType { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore TransactionSource { get; set; } = null!;
}
