﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSGroupCondition
{
    public long Id { get; set; }

    public long GroupId { get; set; }

    public string Name { get; set; } = null!;

    public string Value { get; set; } = null!;

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual SMSGroup Group { get; set; } = null!;
}
