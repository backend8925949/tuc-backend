﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLAccountLoan
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long? ProviderId { get; set; }

    public int PlanId { get; set; }

    public long? VenderLoanId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double TotalAmount { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string LoanCode { get; set; } = null!;

    public string LoanPin { get; set; } = null!;

    public sbyte IsRedeemed { get; set; }

    public DateTime? RedeemDate { get; set; }

    public long? RedeemLocationId { get; set; }

    public long? TerminalId { get; set; }

    public long? CashierId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long MerchantId { get; set; }

    public long? CreditTransactionId { get; set; }

    public long? RedeemTransactionId { get; set; }

    public virtual BNPLAccount Account { get; set; } = null!;

   public virtual ICollection<BNPLAccountLoanActivity> BNPLAccountLoanActivity { get; set; } = new List<BNPLAccountLoanActivity>();

   public virtual ICollection<BNPLAccountLoanPayment> BNPLAccountLoanPayment { get; set; } = new List<BNPLAccountLoanPayment>();

   public virtual ICollection<BNPLLoanProcess> BNPLLoanProcess { get; set; } = new List<BNPLLoanProcess>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlement { get; set; } = new List<BNPLMerchantSettlement>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual BNPLMerchant Merchant { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual BNPLPlan Plan { get; set; } = null!;

    public virtual HCUAccount? Provider { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
