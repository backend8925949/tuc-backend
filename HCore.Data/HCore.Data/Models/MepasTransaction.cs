﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MepasTransaction
{
    public int Id { get; set; }

    public string? TransactionType { get; set; }

    public string? TransTime { get; set; }

    public string? TransAmount { get; set; }

    public string? BusinessShortCode { get; set; }

    public string? BillRefNumber { get; set; }

    public string? InvoiceNumber { get; set; }

    public string? OrgAccountBalance { get; set; }

    public string? ThirdPartTransID { get; set; }

    public string? MSISDN { get; set; }

    public string? FirstName { get; set; }

    public string? TransID { get; set; }
}
