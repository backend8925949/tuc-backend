﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLConfiguration
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Policy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
