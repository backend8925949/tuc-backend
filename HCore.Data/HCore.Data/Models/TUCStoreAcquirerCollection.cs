﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCStoreAcquirerCollection
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public DateTime Date { get; set; }

    public long StoreId { get; set; }

    public long AcquirerId { get; set; }

    public long Terminals { get; set; }

    public double InvoiceAmount { get; set; }

    public double NibsPercentage { get; set; }

    public double NibsAmount { get; set; }

    public double ExpectedAmount { get; set; }

    public double? ReceivedAmount { get; set; }

    public double? AmountDifference { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Acquirer { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount Store { get; set; } = null!;
}
