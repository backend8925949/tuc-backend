﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSCampaignGroupItem
{
    public long Id { get; set; }

    public long CampaignId { get; set; }

    public long? GroupId { get; set; }

    public string? Name { get; set; }

    public string MobileNumber { get; set; } = null!;

    public long? AccountId { get; set; }

    public DateTime? ModifyDate { get; set; }

    public int StatusId { get; set; }

    public sbyte? IsSent { get; set; }

    public DateTime? SendDate { get; set; }

    public string? ExternalId { get; set; }

    public DateTime? DeliveryDate { get; set; }

    public string? ExStatus { get; set; }

    public string? ExStatusCode { get; set; }

    public string? CountryName { get; set; }

    public string? Operator { get; set; }

    public int? SmsLength { get; set; }

    public int? PageCount { get; set; }

    public double? SystemCost { get; set; }

    public DateTime CreateDate { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual SMSCampaign Campaign { get; set; } = null!;

    public virtual SMSCampaignGroup? Group { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
