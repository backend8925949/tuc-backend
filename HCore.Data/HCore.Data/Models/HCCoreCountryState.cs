﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreCountryState
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int CountryId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? IsoCode { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? OIdName { get; set; }

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCCoreAddress> HCCoreAddress { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreCountryStateCity> HCCoreCountryStateCity { get; set; } = new List<HCCoreCountryStateCity>();

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCBranch> TUCBranch { get; set; } = new List<TUCBranch>();
}
