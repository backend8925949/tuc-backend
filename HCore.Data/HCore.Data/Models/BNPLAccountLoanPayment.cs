﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLAccountLoanPayment
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long LoanId { get; set; }

    public long? VendorLoanRepaymentId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double TotalAmount { get; set; }

    public double TotalOutStanding { get; set; }

    public double PrincipalAmount { get; set; }

    public double InterestAmount { get; set; }

    public double SystemAmount { get; set; }

    public double ProviderAmount { get; set; }

    public string? PaymentReference { get; set; }

    public DateTime? PaymentDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual BNPLAccountLoan Loan { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
