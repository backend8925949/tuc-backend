﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCPayOut
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long? BankId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double CommissionAmount { get; set; }

    public double TotalAmount { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string? ExtReferenceNumber { get; set; }

    public string? ExtReferenceId { get; set; }

    public int? SourceId { get; set; }

    public string? Request { get; set; }

    public string? Response { get; set; }

    public string? UserComment { get; set; }

    public string? SystemComment { get; set; }

    public string? Description { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long? RedeemFrom { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccountBank? Bank { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Source { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
