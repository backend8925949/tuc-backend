﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealGallery
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long DealId { get; set; }

    public long ImageStorageId { get; set; }

    public int IsDefault { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual HCCoreStorage ImageStorage { get; set; } = null!;
}
