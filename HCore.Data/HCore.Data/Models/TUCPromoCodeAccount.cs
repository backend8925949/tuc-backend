﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCPromoCodeAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long PromoCodeId { get; set; }

    public long AccountId { get; set; }

    public double Value { get; set; }

    public DateTime UseDate { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual TUCPromoCode PromoCode { get; set; } = null!;
}
