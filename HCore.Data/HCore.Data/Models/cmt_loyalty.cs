﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class cmt_loyalty
{
    public long id { get; set; }

    public string guid { get; set; } = null!;

    public long? transaction_id { get; set; }

    public int loyalty_type_id { get; set; }

    public DateTime transaction_date { get; set; }

    public int payment_mode_id { get; set; }

    public int source_id { get; set; }

    public int type_id { get; set; }

    public double amount { get; set; }

    public double charge { get; set; }

    public double commission_amount { get; set; }

    public double total_amount { get; set; }

    public double invoice_amount { get; set; }

    public double loyalty_invoice_amount { get; set; }

    public long? from_account_id { get; set; }

    public double from_account_balance { get; set; }

    public long? to_account_id { get; set; }

    public double to_account_balance { get; set; }

    public double system_amount { get; set; }

    public long? customer_id { get; set; }

    public double customer_amount { get; set; }

    public long? merchant_id { get; set; }

    public double merchant_amount { get; set; }

    public long? store_id { get; set; }

    public double? store_amount { get; set; }

    public long? acquirer_id { get; set; }

    public double acquirer_amount { get; set; }

    public double provider_amount { get; set; }

    public long? provider_id { get; set; }

    public long? cashier_id { get; set; }

    public double cashier_amount { get; set; }

    public long? terminal_id { get; set; }

    public long? program_id { get; set; }

    public string? invoice_number { get; set; }

    public string? account_number { get; set; }

    public string? bin_number { get; set; }

    public int? bin_number_id { get; set; }

    public string? reference_number { get; set; }

    public DateTime create_date { get; set; }

    public long? created_by_id { get; set; }

    public DateTime? modify_date { get; set; }

    public long? modify_by_id { get; set; }

    public int status_id { get; set; }

    public string? status_message { get; set; }

    public string? comment { get; set; }

    public virtual HCUAccount? acquirer { get; set; }

    public virtual HCCoreBinNumber? bin_numberNavigation { get; set; }

    public virtual HCUAccount? cashier { get; set; }

    public virtual HCUAccount? created_by { get; set; }

    public virtual HCUAccount? customer { get; set; }

    public virtual HCUAccount? from_account { get; set; }

    public virtual HCCore loyalty_type { get; set; } = null!;

    public virtual HCUAccount? merchant { get; set; }

    public virtual HCUAccount? modify_by { get; set; }

    public virtual HCCore payment_mode { get; set; } = null!;

    public virtual HCUAccount? provider { get; set; }

    public virtual HCCore source { get; set; } = null!;

    public virtual HCCore status { get; set; } = null!;

    public virtual HCUAccount? store { get; set; }

    public virtual TUCTerminal? terminal { get; set; }

    public virtual HCUAccount? to_account { get; set; }

    public virtual HCUAccountTransaction? transaction { get; set; }

    public virtual HCCore type { get; set; } = null!;
}
