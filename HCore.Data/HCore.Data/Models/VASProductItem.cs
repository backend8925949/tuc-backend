﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class VASProductItem
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long ProductId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public long? ReferenceId { get; set; }

    public string? ReferenceKey { get; set; }

    public sbyte IsFixedAmount { get; set; }

    public double? Amount { get; set; }

    public double? RewardPercentage { get; set; }

    public double? UserPercentage { get; set; }

    public string? Validity { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual VASProduct Product { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<VasPayment> VasPayment { get; set; } = new List<VasPayment>();
}
