﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCSale
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? TransactionId { get; set; }

    public DateTime TransactionDate { get; set; }

    public int? PaymentModeId { get; set; }

    public long? CustomerId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public long? TerminalId { get; set; }

    public int SourceId { get; set; }

    public int TypeId { get; set; }

    public double InvoiceAmount { get; set; }

    public string? InvoiceNumber { get; set; }

    public string? AccountNumber { get; set; }

    public string? BinNumber { get; set; }

    public long? BinNumberId { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? StatusMessage { get; set; }

    public DateTime SyncDate { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? Customer { get; set; }

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? PaymentMode { get; set; }

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }

    public virtual TUCTerminal? Terminal { get; set; }

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore Type { get; set; } = null!;
}
