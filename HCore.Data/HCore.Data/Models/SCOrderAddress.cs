﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCOrderAddress
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long OrderId { get; set; }

    public string Name { get; set; } = null!;

    public string MobileNumber { get; set; } = null!;

    public string? AlternateMobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? AddressLine1 { get; set; }

    public string? AddressLine2 { get; set; }

    public string? Landmark { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public string? MapAddress { get; set; }

    public long AreaId { get; set; }

    public long CityId { get; set; }

    public long StateId { get; set; }

    public long CountryId { get; set; }

    public string? ZipCode { get; set; }

    public string? Instructions { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCCoreParameter Area { get; set; } = null!;

    public virtual HCCoreParameter City { get; set; } = null!;

    public virtual HCCoreCommon Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual SCOrder Order { get; set; } = null!;

    public virtual HCCoreParameter State { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore Type { get; set; } = null!;
}
