﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreBinNumber
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Bin { get; set; } = null!;

    public string? BinEnd { get; set; }

    public string? BinNumber { get; set; }

    public long? TypeId { get; set; }

    public long? BrandId { get; set; }

    public long? SubBrandId { get; set; }

    public long? BankId { get; set; }

    public int? CountryId { get; set; }

    public DateTime? CreateDate { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public int? TCountryId { get; set; }

    public virtual HCCoreCommon? Bank { get; set; }

    public virtual HCCoreCommon? Brand { get; set; }

    public virtual HCCoreCountry? Country { get; set; }

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransaction { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCoreCommon? SubBrand { get; set; }

   public virtual ICollection<TUCLoyalty> TUCLoyalty { get; set; } = new List<TUCLoyalty>();

    public virtual HCCoreCommon? Type { get; set; }

   public virtual ICollection<cmt_loyalty> cmt_loyalty { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_sale> cmt_sale { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failed { get; set; } = new List<cmt_sale_failed>();
}
