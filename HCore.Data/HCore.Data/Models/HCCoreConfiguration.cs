﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreConfiguration
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public string Value { get; set; } = null!;

    public int? ValueHelperId { get; set; }

    public int DataTypeId { get; set; }

    public int? AccountTypeId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public long? ParameterId { get; set; }

    public virtual HCCore? AccountType { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCCore DataType { get; set; } = null!;

   public virtual ICollection<HCCoreConfigurationHistory> HCCoreConfigurationHistory { get; set; } = new List<HCCoreConfigurationHistory>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfiguration { get; set; } = new List<HCUAccountConfiguration>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? ValueHelper { get; set; }
}
