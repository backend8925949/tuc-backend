﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreVerification
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int VerificationTypeId { get; set; }

    public string? CountryIsd { get; set; }

    public string? MobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string AccessKey { get; set; } = null!;

    public string AccessCode { get; set; } = null!;

    public string? AccessCodeStart { get; set; }

    public string? EmailMessage { get; set; }

    public string? MobileMessage { get; set; }

    public DateTime? ExpiaryDate { get; set; }

    public DateTime? VerifyDate { get; set; }

    public string? RequestIpAddress { get; set; }

    public double? RequestLatitude { get; set; }

    public double? RequestLongitude { get; set; }

    public string? RequestLocation { get; set; }

    public int? VerifyAttemptCount { get; set; }

    public string? VerifyIpAddress { get; set; }

    public double? VerifyLatitude { get; set; }

    public double? VerifyLongitude { get; set; }

    public string? VerifyAddress { get; set; }

    public string? ReferenceKey { get; set; }

    public DateTime CreateDate { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

   public virtual ICollection<HCUAccountParameter> HCUAccountParameter { get; set; } = new List<HCUAccountParameter>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore VerificationType { get; set; } = null!;
}
