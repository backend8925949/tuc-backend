﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCOrderActivity
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrderId { get; set; }

    public long? OrderItemId { get; set; }

    public string Title { get; set; } = null!;

    public string Description { get; set; } = null!;

    public string? SystemDescription { get; set; }

    public string? Comment { get; set; }

    public int? OrderStatusId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual SCOrder Order { get; set; } = null!;

    public virtual SCOrderItem? OrderItem { get; set; }

    public virtual HCCore? OrderStatus { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
