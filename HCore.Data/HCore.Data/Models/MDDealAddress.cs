﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealAddress
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? AddressId { get; set; }

    public string? TId { get; set; }

    public string? AddressReference { get; set; }

    public long? AccountId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreAddress? Address { get; set; }
}
