﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealWishlist
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long DealId { get; set; }

    public DateTime CreateDate { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;
}
