﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreParameter
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public int? TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public int? HelperId { get; set; }

    public long? AccountId { get; set; }

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public long? CommonId { get; set; }

    public long? SubCommonId { get; set; }

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public string? Value { get; set; }

    public string? SubValue { get; set; }

    public string? Data { get; set; }

    public string? Description { get; set; }

    public long? Sequence { get; set; }

    public long? Count { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreCommon? Common { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceModel { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceNetworkOperator { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceOsVersion { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceResolution { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocationCity { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocationCityArea { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocationRegion { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocationRegionArea { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameter { get; set; } = new List<HCUAccountParameter>();

    public virtual HCCore? Helper { get; set; }

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<HCCoreParameter> InverseParent { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> InverseSubParent { get; set; } = new List<HCCoreParameter>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreParameter? Parent { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

   public virtual ICollection<SCOrderAddress> SCOrderAddressArea { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressCity { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressState { get; set; } = new List<SCOrderAddress>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCoreCommon? SubCommon { get; set; }

    public virtual HCCoreParameter? SubParent { get; set; }

    public virtual HCCore? SubType { get; set; }

   public virtual ICollection<TUCBranchAccount> TUCBranchAccount { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistration { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudience { get; set; } = new List<TUCampaignAudience>();

    public virtual HCCore? Type { get; set; }
}
