﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProductShedule
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long ProductId { get; set; }

    public long? SubProductId { get; set; }

    public long? TypeId { get; set; }

    public int? StartHour { get; set; }

    public int? EndHour { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public int? DayOfWeek { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual CAProduct Product { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual CAProduct? SubProduct { get; set; }

    public virtual CAProduct? Type { get; set; }
}
