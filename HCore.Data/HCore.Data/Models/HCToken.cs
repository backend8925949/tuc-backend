﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCToken
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public DateTime CreateDate { get; set; }

    public DateTime? ExpiaryDate { get; set; }

    public DateTime? LastRequestDate { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
