﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCLoyaltyMerchantCustomer
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? LoyaltyId { get; set; }

    public long? MerchantId { get; set; }

    public long? CustomerId { get; set; }

    public int? SourceId { get; set; }

    public double? Credit { get; set; }

    public double? Debit { get; set; }

    public double? Balance { get; set; }

    public double? InvoiceAmount { get; set; }

    public long? Transactions { get; set; }

    public DateTime? LastTransactionDate { get; set; }

    public DateTime? CreateDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ProgramId { get; set; }

    public int? StatusId { get; set; }

    public virtual HCUAccount? Customer { get; set; }

    public virtual HCUAccount? Merchant { get; set; }

    public virtual TUCLProgram? Program { get; set; }

    public virtual HCCore? Source { get; set; }
}
