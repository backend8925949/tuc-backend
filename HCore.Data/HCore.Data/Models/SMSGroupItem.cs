﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSGroupItem
{
    public long Id { get; set; }

    public long GroupId { get; set; }

    public string? Name { get; set; }

    public string MobileNumber { get; set; } = null!;

    public long? AccountId { get; set; }

    public DateTime CreateDate { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual SMSGroup Group { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
