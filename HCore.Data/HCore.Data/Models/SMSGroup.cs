﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSGroup
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public int SourceId { get; set; }

    public string Title { get; set; } = null!;

    public long TotalItem { get; set; }

    public string? Condition { get; set; }

    public string? FileName { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroup { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSGroupCondition> SMSGroupCondition { get; set; } = new List<SMSGroupCondition>();

   public virtual ICollection<SMSGroupItem> SMSGroupItem { get; set; } = new List<SMSGroupItem>();

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
