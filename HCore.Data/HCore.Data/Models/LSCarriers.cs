﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSCarriers
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public string? Name { get; set; }

    public string? EmailAddress { get; set; }

    public string? MobileNumber { get; set; }

    public sbyte? Active { get; set; }

    public sbyte? Domestic { get; set; }

    public sbyte? International { get; set; }

    public string? IconUrl { get; set; }

    public sbyte? Regional { get; set; }

    public sbyte? RequiresInvoice { get; set; }

    public sbyte? RequiresWaybill { get; set; }

    public string? Slug { get; set; }

    public string? CarrierId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<LSShipmentRate> LSShipmentRate { get; set; } = new List<LSShipmentRate>();

   public virtual ICollection<LSShipments> LSShipments { get; set; } = new List<LSShipments>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }
}
