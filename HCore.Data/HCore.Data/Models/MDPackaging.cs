﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDPackaging
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public double Height { get; set; }

    public double Length { get; set; }

    public string? Name { get; set; }

    public string SizeUnit { get; set; } = null!;

    public string Type { get; set; } = null!;

    public double Weight { get; set; }

    public string WeightUnit { get; set; } = null!;

    public double Width { get; set; }

    public string? PackagingId { get; set; }

    public string? TId { get; set; }

    public long? MerchantId { get; set; }

    public double? MinimunWeight { get; set; }

    public double? MaximumWeight { get; set; }

    public int? CategoryId { get; set; }

    public virtual TUCCategory? Category { get; set; }

    public virtual HCUAccount? Merchant { get; set; }
}
