﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountOwner
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public int AccountTypeId { get; set; }

    public long OwnerId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCCore AccountType { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount Owner { get; set; } = null!;

    public virtual HCCore? Status { get; set; }
}
