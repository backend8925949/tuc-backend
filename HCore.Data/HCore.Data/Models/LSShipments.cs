﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSShipments
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? CustomerId { get; set; }

    public long? DeliveryPartnerId { get; set; }

    public long? ToAddressId { get; set; }

    public long? FromAddressId { get; set; }

    public long? ReturnAddressId { get; set; }

    public long? ParcelId { get; set; }

    public long? CarrierId { get; set; }

    public DateTime? PickUpDate { get; set; }

    public string? UserId { get; set; }

    public string? ShipmentId { get; set; }

    public double? RateAmount { get; set; }

    public string? TId { get; set; }

    public DateTime? DeliveryDate { get; set; }

    public string? RateId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public string? TrackingNumber { get; set; }

    public long? BillId { get; set; }

    public long? DealId { get; set; }

    public double? DealPrice { get; set; }

    public double? TotalAmount { get; set; }

    public string? OrderReference { get; set; }

    public string? CancelPickUpUrl { get; set; }

    public string? TrackingUrl { get; set; }

    public string? TransactionReference { get; set; }

    public virtual HCCoreStorage? Bill { get; set; }

    public virtual LSCarriers? Carrier { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? Customer { get; set; }

    public virtual MDDeal? Deal { get; set; }

    public virtual HCUAccount? DeliveryPartner { get; set; }

    public virtual HCCoreAddress? FromAddress { get; set; }

   public virtual ICollection<LSOperations> LSOperations { get; set; } = new List<LSOperations>();

   public virtual ICollection<LSShipmentRate> LSShipmentRate { get; set; } = new List<LSShipmentRate>();

   public virtual ICollection<MDDealCode> MDDealCode { get; set; } = new List<MDDealCode>();

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual LSParcel? Parcel { get; set; }

    public virtual HCCoreAddress? ReturnAddress { get; set; }

    public virtual HCCore? Status { get; set; }

    public virtual HCUAccount? Store { get; set; }

    public virtual HCCoreAddress? ToAddress { get; set; }
}
