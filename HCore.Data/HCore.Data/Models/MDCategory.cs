﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDCategory
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? RootCategoryId { get; set; }

    public double Commission { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<MDDealPromoCode> MDDealPromoCode { get; set; } = new List<MDDealPromoCode>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual TUCCategory? RootCategory { get; set; }

    public virtual HCCore? Status { get; set; }
}
