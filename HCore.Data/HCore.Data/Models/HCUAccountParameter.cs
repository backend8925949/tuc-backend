﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountParameter
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public long? AccountId { get; set; }

    public long? DeviceId { get; set; }

    public long? CommonId { get; set; }

    public long? ParameterId { get; set; }

    public int? HelperId { get; set; }

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public string? Value { get; set; }

    public string? SubValue { get; set; }

    public string? Description { get; set; }

    public string? Data { get; set; }

    public long? VerificationId { get; set; }

    public long? CountValue { get; set; }

    public double? AverageValue { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public long? TransactionId { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public string? RequestKey { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreCommon? Common { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccountDevice? Device { get; set; }

    public virtual HCCore? Helper { get; set; }

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<HCUAccountParameter> InverseParent { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> InverseSubParent { get; set; } = new List<HCUAccountParameter>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreParameter? Parameter { get; set; }

    public virtual HCUAccountParameter? Parent { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccountParameter? SubParent { get; set; }

    public virtual HCCore? SubType { get; set; }

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore? Type { get; set; }

    public virtual HCCoreVerification? Verification { get; set; }
}
