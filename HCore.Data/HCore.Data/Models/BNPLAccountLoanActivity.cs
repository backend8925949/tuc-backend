﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLAccountLoanActivity
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long LoanId { get; set; }

    public string Description { get; set; } = null!;

    public sbyte IsSystemActivity { get; set; }

    public DateTime CreateDate { get; set; }

    public int StatusId { get; set; }

    public virtual BNPLAccountLoan Loan { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
