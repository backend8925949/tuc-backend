﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProductLocation
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long ProductId { get; set; }

    public long? SubProductId { get; set; }

    public long AccountId { get; set; }

    public long? SubAccountId { get; set; }

    public long? ProductCodeUseCount { get; set; }

    public DateTime? ProductLastUseDate { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

   public virtual ICollection<CAProduct> CAProduct { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProductCode> CAProductCode { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistory { get; set; } = new List<CAProductUseHistory>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual CAProduct Product { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? SubAccount { get; set; }

    public virtual CAProduct? SubProduct { get; set; }
}
