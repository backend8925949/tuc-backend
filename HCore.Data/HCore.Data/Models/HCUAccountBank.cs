﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountBank
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string AccountNumber { get; set; } = null!;

    public string? BankName { get; set; }

    public string? BankCode { get; set; }

    public long? BankId { get; set; }

    public string? ReferenceCode { get; set; }

    public string? ReferenceKey { get; set; }

    public string? BvnNumber { get; set; }

    public string? Comment { get; set; }

    public sbyte? IsPrimary { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCPayOut> TUCPayOut { get; set; } = new List<TUCPayOut>();
}
