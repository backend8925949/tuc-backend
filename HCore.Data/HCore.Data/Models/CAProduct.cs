﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProduct
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public long? ParentId { get; set; }

    public long AccountId { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public string? Terms { get; set; }

    public int UsageTypeId { get; set; }

    public string? UsageInformation { get; set; }

    public int? IsPinRequired { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? CodeValidityStartDate { get; set; }

    public DateTime? CodeValidityEndDate { get; set; }

    public double? UnitPrice { get; set; }

    public double? SellingPrice { get; set; }

    public double? DiscountAmount { get; set; }

    public double? DiscountPercentage { get; set; }

    public double? Amount { get; set; }

    public double? Charge { get; set; }

    public double? CommissionAmount { get; set; }

    public double? TotalAmount { get; set; }

    public long? MaximumUnitSale { get; set; }

    public long? TotalUnitSale { get; set; }

    public long? TotalUsed { get; set; }

    public double? TotalSaleAmount { get; set; }

    public double? TotalUsedAmount { get; set; }

    public DateTime? LastUseDate { get; set; }

    public long? LastUseLocationId { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public long? Views { get; set; }

    public string? Message { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? AccountNumber { get; set; }

    public string? PaymentReference { get; set; }

    public string? PaymentSource { get; set; }

    public long? TransactionId { get; set; }

    public string? CompanyName { get; set; }

    public bool? IsIndividual { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupProduct { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupSubProduct { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductCode> CAProductCodeProduct { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductCode> CAProductCodeSubProduct { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductLocation> CAProductLocationProduct { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductLocation> CAProductLocationSubProduct { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductShedule> CAProductSheduleProduct { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProductShedule> CAProductSheduleSubProduct { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProductShedule> CAProductSheduleType { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistoryProduct { get; set; } = new List<CAProductUseHistory>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistorySubProduct { get; set; } = new List<CAProductUseHistory>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<CAProduct> InverseParent { get; set; } = new List<CAProduct>();

    public virtual CAProductLocation? LastUseLocation { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual CAProduct? Parent { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? SubType { get; set; }

    public virtual HCCore Type { get; set; } = null!;

    public virtual HCCore UsageType { get; set; } = null!;
}
