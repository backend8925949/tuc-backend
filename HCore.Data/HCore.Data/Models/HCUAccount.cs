﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int AccountTypeId { get; set; }

    public int? AccountOperationTypeId { get; set; }

    public long UserId { get; set; }

    public long? OwnerId { get; set; }

    public long? SubOwnerId { get; set; }

    public string? AccessPin { get; set; }

    public string? AccountCode { get; set; }

    public string DisplayName { get; set; } = null!;

    public string? Name { get; set; }

    public string? FirstName { get; set; }

    public string? MiddleName { get; set; }

    public string? LastName { get; set; }

    public string? MobileNumber { get; set; }

    public string? ContactNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? SecondaryEmailAddress { get; set; }

    public int? GenderId { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public string? Address { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public int? CountryId { get; set; }

    public long? StateId { get; set; }

    public long? CityId { get; set; }

    public long? CityAreaId { get; set; }

    public string? WebsiteUrl { get; set; }

    public sbyte EmailVerificationStatus { get; set; }

    public DateTime? EmailVerificationStatusDate { get; set; }

    public sbyte NumberVerificationStatus { get; set; }

    public DateTime? NumberVerificationStatusDate { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public string? ReferralCode { get; set; }

    public string? ReferralUrl { get; set; }

    public string? Description { get; set; }

    public int? RegistrationSourceId { get; set; }

    public long? RoleId { get; set; }

    public long? AppVersionId { get; set; }

    public long? CardId { get; set; }

    public int? ApplicationStatusId { get; set; }

    public int? AccountLevel { get; set; }

    public long? SubscriptionId { get; set; }

    public long? BankId { get; set; }

    public DateTime? LastLoginDate { get; set; }

    public long? CountValue { get; set; }

    public double? AverageValue { get; set; }

    public string? RequestKey { get; set; }

    public DateTime? LastActivityDate { get; set; }

    public DateTime? LastTransactionDate { get; set; }

    public double? AccountPercentage { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? CpName { get; set; }

    public string? CpFirstName { get; set; }

    public string? CpLastName { get; set; }

    public string? CpMobileNumber { get; set; }

    public string? CpContactNumber { get; set; }

    public string? CpEmailAddress { get; set; }

    public DateTime? Date { get; set; }

    public long? AddressId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public int? PrimaryCategoryId { get; set; }

    public int? SecondaryCategoryId { get; set; }

    public int? ActivityStatusId { get; set; }

    public DateTime? SyncTime { get; set; }

    public long? TimeZoneId { get; set; }

    public sbyte? DocumentVerificationStatus { get; set; }

    public DateTime? DocumentVerificationStatusDate { get; set; }

    public bool? IsTempPin { get; set; }

    public string? ShortCode { get; set; }

    public string? OperatorID { get; set; }

    public string? ReferrerId { get; set; }

    public virtual HCCore? AccountLevelNavigation { get; set; }

    public virtual HCCore? AccountOperationType { get; set; }

    public virtual HCCore AccountType { get; set; } = null!;

    public virtual HCCore? ActivityStatus { get; set; }

    public virtual HCCoreAddress? AddressNavigation { get; set; }

    public virtual HCCoreCommon? AppVersion { get; set; }

    public virtual HCCore? ApplicationStatus { get; set; }

   public virtual ICollection<BNPLAccount> BNPLAccountAccount { get; set; } = new List<BNPLAccount>();

   public virtual ICollection<BNPLAccount> BNPLAccountCreatedBy { get; set; } = new List<BNPLAccount>();

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoanCreatedBy { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoanModifyBy { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLAccountLoanPayment> BNPLAccountLoanPaymentCreatedBy { get; set; } = new List<BNPLAccountLoanPayment>();

   public virtual ICollection<BNPLAccountLoanPayment> BNPLAccountLoanPaymentModifyBy { get; set; } = new List<BNPLAccountLoanPayment>();

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoanProvider { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLAccount> BNPLAccountModifyBy { get; set; } = new List<BNPLAccount>();

   public virtual ICollection<BNPLConfiguration> BNPLConfiguration { get; set; } = new List<BNPLConfiguration>();

   public virtual ICollection<BNPLLoanProcess> BNPLLoanProcessCreated_By { get; set; } = new List<BNPLLoanProcess>();

   public virtual ICollection<BNPLLoanProcess> BNPLLoanProcessModify_By { get; set; } = new List<BNPLLoanProcess>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantAccount { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantCreatedBy { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantModifiyBy { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlementCreatedBy { get; set; } = new List<BNPLMerchantSettlement>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlementModifyBy { get; set; } = new List<BNPLMerchantSettlement>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlementProvider { get; set; } = new List<BNPLMerchantSettlement>();

   public virtual ICollection<BNPLPlan> BNPLPlanCreatedBy { get; set; } = new List<BNPLPlan>();

   public virtual ICollection<BNPLPlan> BNPLPlanModifyBy { get; set; } = new List<BNPLPlan>();

   public virtual ICollection<BNPLPrequalification> BNPLPrequalificationCreatedBy { get; set; } = new List<BNPLPrequalification>();

   public virtual ICollection<BNPLPrequalification> BNPLPrequalificationModifyBy { get; set; } = new List<BNPLPrequalification>();

    public virtual HCUAccount? Bank { get; set; }

   public virtual ICollection<CAProduct> CAProductAccount { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupAccount { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupCreatedBy { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupModifyBy { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupOwner { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductCode> CAProductCodeAccount { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductCode> CAProductCodeCreatedBy { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductCode> CAProductCodeModifyBy { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProduct> CAProductCreatedBy { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProductLocation> CAProductLocationAccount { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductLocation> CAProductLocationCreatedBy { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductLocation> CAProductLocationModifyBy { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductLocation> CAProductLocationSubAccount { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProduct> CAProductModifyBy { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProductShedule> CAProductSheduleCreatedBy { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProductShedule> CAProductSheduleModifyBy { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistoryAccount { get; set; } = new List<CAProductUseHistory>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistoryCreatedBy { get; set; } = new List<CAProductUseHistory>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistoryModifyBy { get; set; } = new List<CAProductUseHistory>();

    public virtual TUCard? Card { get; set; }

    public virtual HCCoreCountryStateCity? City { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea { get; set; }

    public virtual HCCoreCountry? Country { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCCore? Gender { get; set; }

   public virtual ICollection<HCCoreAddress> HCCoreAddressAccount { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreAddress> HCCoreAddressCreatedBy { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreAddress> HCCoreAddressModifyBy { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreApp> HCCoreAppAccount { get; set; } = new List<HCCoreApp>();

   public virtual ICollection<HCCoreApp> HCCoreAppCreatedBy { get; set; } = new List<HCCoreApp>();

   public virtual ICollection<HCCoreApp> HCCoreAppModifyBy { get; set; } = new List<HCCoreApp>();

   public virtual ICollection<HCCoreAppVersion> HCCoreAppVersionCreatedBy { get; set; } = new List<HCCoreAppVersion>();

   public virtual ICollection<HCCoreAppVersion> HCCoreAppVersionModifyBy { get; set; } = new List<HCCoreAppVersion>();

   public virtual ICollection<HCCoreAppVersionToken> HCCoreAppVersionToken { get; set; } = new List<HCCoreAppVersionToken>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonAccount { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonCreatedBy { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonModifyBy { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationCreatedBy { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreConfigurationHistory> HCCoreConfigurationHistoryCreatedBy { get; set; } = new List<HCCoreConfigurationHistory>();

   public virtual ICollection<HCCoreConfigurationHistory> HCCoreConfigurationHistoryModifyBy { get; set; } = new List<HCCoreConfigurationHistory>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationModifyBy { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreCountry> HCCoreCountryCreatedBy { get; set; } = new List<HCCoreCountry>();

   public virtual ICollection<HCCoreCountry> HCCoreCountryModifyBy { get; set; } = new List<HCCoreCountry>();

   public virtual ICollection<HCCoreCountryStateCityArea> HCCoreCountryStateCityAreaCreatedBy { get; set; } = new List<HCCoreCountryStateCityArea>();

   public virtual ICollection<HCCoreCountryStateCityArea> HCCoreCountryStateCityAreaModifyBy { get; set; } = new List<HCCoreCountryStateCityArea>();

   public virtual ICollection<HCCoreCountryStateCity> HCCoreCountryStateCityCreatedBy { get; set; } = new List<HCCoreCountryStateCity>();

   public virtual ICollection<HCCoreCountryStateCity> HCCoreCountryStateCityModifyBy { get; set; } = new List<HCCoreCountryStateCity>();

   public virtual ICollection<HCCoreCountryState> HCCoreCountryStateCreatedBy { get; set; } = new List<HCCoreCountryState>();

   public virtual ICollection<HCCoreCountryState> HCCoreCountryStateModifyBy { get; set; } = new List<HCCoreCountryState>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterAccount { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterCreatedBy { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterModifyBy { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreRole> HCCoreRoleCreatedBy { get; set; } = new List<HCCoreRole>();

   public virtual ICollection<HCCoreRole> HCCoreRoleModifyBy { get; set; } = new List<HCCoreRole>();

   public virtual ICollection<HCCoreStorage> HCCoreStorage { get; set; } = new List<HCCoreStorage>();

   public virtual ICollection<HCCoreStorageFolder> HCCoreStorageFolderAccount { get; set; } = new List<HCCoreStorageFolder>();

   public virtual ICollection<HCCoreStorageFolder> HCCoreStorageFolderCreatedBy { get; set; } = new List<HCCoreStorageFolder>();

   public virtual ICollection<HCCoreStorageFolder> HCCoreStorageFolderModifyBy { get; set; } = new List<HCCoreStorageFolder>();

   public virtual ICollection<HCCoreStorageTag> HCCoreStorageTagCreatedBy { get; set; } = new List<HCCoreStorageTag>();

   public virtual ICollection<HCCoreStorageTag> HCCoreStorageTagModifyBy { get; set; } = new List<HCCoreStorageTag>();

   public virtual ICollection<HCSubscription> HCSubscriptionCreatedBy { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCSubscriptionFeature> HCSubscriptionFeatureCreatedBy { get; set; } = new List<HCSubscriptionFeature>();

   public virtual ICollection<HCSubscriptionFeatureItem> HCSubscriptionFeatureItemCreatedBy { get; set; } = new List<HCSubscriptionFeatureItem>();

   public virtual ICollection<HCSubscriptionFeatureItem> HCSubscriptionFeatureItemModifyBy { get; set; } = new List<HCSubscriptionFeatureItem>();

   public virtual ICollection<HCSubscriptionFeature> HCSubscriptionFeatureModifyBy { get; set; } = new List<HCSubscriptionFeature>();

   public virtual ICollection<HCSubscription> HCSubscriptionModifyBy { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCToken> HCToken { get; set; } = new List<HCToken>();

   public virtual ICollection<HCTransaction> HCTransactionCreatedBy { get; set; } = new List<HCTransaction>();

   public virtual ICollection<HCTransaction> HCTransactionModifyBy { get; set; } = new List<HCTransaction>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterBank { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterCashier { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterCreatedBy { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterCustomer { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterMerchant { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterModifyBy { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterProvider { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterStore { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransactionParameter> HCTransactionParameterTerminal { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCTransaction> HCTransactionUserAccount { get; set; } = new List<HCTransaction>();

   public virtual ICollection<HCUAccountActivity> HCUAccountActivity { get; set; } = new List<HCUAccountActivity>();

   public virtual ICollection<HCUAccountAuth> HCUAccountAuthCreatedBy { get; set; } = new List<HCUAccountAuth>();

   public virtual ICollection<HCUAccountAuth> HCUAccountAuthModifyBy { get; set; } = new List<HCUAccountAuth>();

   public virtual ICollection<HCUAccountBalance> HCUAccountBalanceAccount { get; set; } = new List<HCUAccountBalance>();

   public virtual ICollection<HCUAccountBalance> HCUAccountBalanceParent { get; set; } = new List<HCUAccountBalance>();

   public virtual ICollection<HCUAccountBank> HCUAccountBankAccount { get; set; } = new List<HCUAccountBank>();

   public virtual ICollection<HCUAccountBank> HCUAccountBankCreatedBy { get; set; } = new List<HCUAccountBank>();

   public virtual ICollection<HCUAccountBank> HCUAccountBankModifyBy { get; set; } = new List<HCUAccountBank>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfigurationAccount { get; set; } = new List<HCUAccountConfiguration>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfigurationCreatedBy { get; set; } = new List<HCUAccountConfiguration>();

   public virtual ICollection<HCUAccountConfigurationHistory> HCUAccountConfigurationHistoryCreatedBy { get; set; } = new List<HCUAccountConfigurationHistory>();

   public virtual ICollection<HCUAccountConfigurationHistory> HCUAccountConfigurationHistoryModifyBy { get; set; } = new List<HCUAccountConfigurationHistory>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfigurationModifyBy { get; set; } = new List<HCUAccountConfiguration>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceAccount { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceCreatedBy { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceModifyBy { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoiceAccount { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoiceCreatedBy { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoiceModifyBy { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoicePaymentApprover { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocation { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerAccount { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerCreatedBy { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerModifyBy { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerOwner { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterAccount { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterCreatedBy { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterModifyBy { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountSession> HCUAccountSessionAccount { get; set; } = new List<HCUAccountSession>();

   public virtual ICollection<HCUAccountSession> HCUAccountSessionModifyBy { get; set; } = new List<HCUAccountSession>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscriptionAccount { get; set; } = new List<HCUAccountSubscription>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscriptionCreatedBy { get; set; } = new List<HCUAccountSubscription>();

   public virtual ICollection<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeatureCreatedBy { get; set; } = new List<HCUAccountSubscriptionFeature>();

   public virtual ICollection<HCUAccountSubscriptionFeatureItem> HCUAccountSubscriptionFeatureItemCreatedBy { get; set; } = new List<HCUAccountSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccountSubscriptionFeatureItem> HCUAccountSubscriptionFeatureItemModifyBy { get; set; } = new List<HCUAccountSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeatureModifyBy { get; set; } = new List<HCUAccountSubscriptionFeature>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscriptionModifyBy { get; set; } = new List<HCUAccountSubscription>();

   public virtual ICollection<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPaymentAccount { get; set; } = new List<HCUAccountSubscriptionPayment>();

   public virtual ICollection<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPaymentCreatedBy { get; set; } = new List<HCUAccountSubscriptionPayment>();

   public virtual ICollection<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPaymentModifyBy { get; set; } = new List<HCUAccountSubscriptionPayment>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionAccount { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionBank { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCashier { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCreatedBy { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCustomer { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionModifyBy { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionParent { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionProvider { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionSubParent { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<HCUAccount> InverseBank { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> InverseCreatedBy { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> InverseModifyBy { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> InverseOwner { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> InverseSubOwner { get; set; } = new List<HCUAccount>();

   public virtual ICollection<LSCarriers> LSCarriersCreatedBy { get; set; } = new List<LSCarriers>();

   public virtual ICollection<LSCarriers> LSCarriersModifyBy { get; set; } = new List<LSCarriers>();

   public virtual ICollection<LSOperations> LSOperations { get; set; } = new List<LSOperations>();

   public virtual ICollection<LSPackages> LSPackagesAccount { get; set; } = new List<LSPackages>();

   public virtual ICollection<LSPackages> LSPackagesCreatedBy { get; set; } = new List<LSPackages>();

   public virtual ICollection<LSPackages> LSPackagesModifyBy { get; set; } = new List<LSPackages>();

   public virtual ICollection<LSParcel> LSParcelAccount { get; set; } = new List<LSParcel>();

   public virtual ICollection<LSParcel> LSParcelCreatedBy { get; set; } = new List<LSParcel>();

   public virtual ICollection<LSParcel> LSParcelModifyBy { get; set; } = new List<LSParcel>();

   public virtual ICollection<LSShipments> LSShipmentsCreatedBy { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsCustomer { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsDeliveryPartner { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsMerchant { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsModifyBy { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsStore { get; set; } = new List<LSShipments>();

   public virtual ICollection<MDCategory> MDCategoryCreatedBy { get; set; } = new List<MDCategory>();

   public virtual ICollection<MDCategory> MDCategoryModifyBy { get; set; } = new List<MDCategory>();

   public virtual ICollection<MDDeal> MDDealAccount { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealAddress> MDDealAddress { get; set; } = new List<MDDealAddress>();

   public virtual ICollection<MDDeal> MDDealApprover { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealBookmark> MDDealBookmark { get; set; } = new List<MDDealBookmark>();

   public virtual ICollection<MDDealCart> MDDealCartAccount { get; set; } = new List<MDDealCart>();

   public virtual ICollection<MDDealCart> MDDealCartCreatedBy { get; set; } = new List<MDDealCart>();

   public virtual ICollection<MDDealCart> MDDealCartModifyBy { get; set; } = new List<MDDealCart>();

   public virtual ICollection<MDDealCode> MDDealCodeAccount { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeCashier { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeCreatedBy { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeLastUseLocation { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeModifyBy { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodePartner { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeSecondaryAccount { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeTerminal { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDeal> MDDealCreatedBy { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealGallery> MDDealGallery { get; set; } = new List<MDDealGallery>();

   public virtual ICollection<MDDealLike> MDDealLike { get; set; } = new List<MDDealLike>();

   public virtual ICollection<MDDealLocation> MDDealLocationCreatedBy { get; set; } = new List<MDDealLocation>();

   public virtual ICollection<MDDealLocation> MDDealLocationLocation { get; set; } = new List<MDDealLocation>();

   public virtual ICollection<MDDeal> MDDealModifyBy { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealNotification> MDDealNotification { get; set; } = new List<MDDealNotification>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeAccount { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromoCodeAccount> MDDealPromoCodeAccountNavigation { get; set; } = new List<MDDealPromoCodeAccount>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeCreatedBy { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeModifyBy { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromotionClick> MDDealPromotionClick { get; set; } = new List<MDDealPromotionClick>();

   public virtual ICollection<MDDealPromotion> MDDealPromotionCreatedBy { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<MDDealPromotion> MDDealPromotionModifyBy { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<MDDealReview> MDDealReviewCreatedBy { get; set; } = new List<MDDealReview>();

   public virtual ICollection<MDDealReview> MDDealReviewModifyBy { get; set; } = new List<MDDealReview>();

   public virtual ICollection<MDDealSearch> MDDealSearchAccount { get; set; } = new List<MDDealSearch>();

   public virtual ICollection<MDDealSearch> MDDealSearchCreatedBy { get; set; } = new List<MDDealSearch>();

   public virtual ICollection<MDDealShedule> MDDealSheduleCreatedBy { get; set; } = new List<MDDealShedule>();

   public virtual ICollection<MDDealShedule> MDDealSheduleModifyBy { get; set; } = new List<MDDealShedule>();

   public virtual ICollection<MDDealView> MDDealView { get; set; } = new List<MDDealView>();

   public virtual ICollection<MDDealWishlist> MDDealWishlist { get; set; } = new List<MDDealWishlist>();

   public virtual ICollection<MDFlashDeal> MDFlashDealCreatedBy { get; set; } = new List<MDFlashDeal>();

   public virtual ICollection<MDFlashDeal> MDFlashDealModifyBy { get; set; } = new List<MDFlashDeal>();

   public virtual ICollection<MDPackaging> MDPackaging { get; set; } = new List<MDPackaging>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Owner { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

    public virtual TUCCategory? PrimaryCategory { get; set; }

    public virtual HCCore? RegistrationSource { get; set; }

    public virtual HCCoreRole? Role { get; set; }

   public virtual ICollection<SCCategory> SCCategoryCreatedBy { get; set; } = new List<SCCategory>();

   public virtual ICollection<SCCategory> SCCategoryModifyBy { get; set; } = new List<SCCategory>();

   public virtual ICollection<SCConfiguration> SCConfigurationAccount { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCConfiguration> SCConfigurationCreatedBy { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCConfiguration> SCConfigurationModifyBy { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCOrderActivity> SCOrderActivity { get; set; } = new List<SCOrderActivity>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressCreatedBy { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressModifyBy { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrder> SCOrderCreatedBy { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderCustomer { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderDealer { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderDealerLocation { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrderItem> SCOrderItemCreatedBy { get; set; } = new List<SCOrderItem>();

   public virtual ICollection<SCOrderItem> SCOrderItemModifyBy { get; set; } = new List<SCOrderItem>();

   public virtual ICollection<SCOrder> SCOrderModifyBy { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderRider { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCProduct> SCProductAccount { get; set; } = new List<SCProduct>();

   public virtual ICollection<SCProduct> SCProductCreatedBy { get; set; } = new List<SCProduct>();

   public virtual ICollection<SCProduct> SCProductModifyBy { get; set; } = new List<SCProduct>();

   public virtual ICollection<SCProductVarient> SCProductVarientCreatedBy { get; set; } = new List<SCProductVarient>();

   public virtual ICollection<SCProductVarient> SCProductVarientModifyBy { get; set; } = new List<SCProductVarient>();

   public virtual ICollection<SCProductVarientStock> SCProductVarientStockCreatedBy { get; set; } = new List<SCProductVarientStock>();

   public virtual ICollection<SCProductVarientStock> SCProductVarientStockModifyBy { get; set; } = new List<SCProductVarientStock>();

   public virtual ICollection<SCProductVarientStock> SCProductVarientStockSubAccount { get; set; } = new List<SCProductVarientStock>();

   public virtual ICollection<SMSCampaign> SMSCampaignAccount { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSCampaign> SMSCampaignCreatedBy { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSCampaignGroupCondition> SMSCampaignGroupCondition { get; set; } = new List<SMSCampaignGroupCondition>();

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroupCreatedBy { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSCampaignGroupItem> SMSCampaignGroupItem { get; set; } = new List<SMSCampaignGroupItem>();

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroupModifyBy { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSCampaign> SMSCampaignModifyBy { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSCampaign> SMSCampaignReviewer { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSGroup> SMSGroupAccount { get; set; } = new List<SMSGroup>();

   public virtual ICollection<SMSGroupCondition> SMSGroupCondition { get; set; } = new List<SMSGroupCondition>();

   public virtual ICollection<SMSGroup> SMSGroupCreatedBy { get; set; } = new List<SMSGroup>();

   public virtual ICollection<SMSGroupItem> SMSGroupItem { get; set; } = new List<SMSGroupItem>();

   public virtual ICollection<SMSGroup> SMSGroupModifyBy { get; set; } = new List<SMSGroup>();

    public virtual TUCCategory? SecondaryCategory { get; set; }

    public virtual HCCoreCountryState? State { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? SubOwner { get; set; }

    public virtual HCUAccountSubscription? Subscription { get; set; }

   public virtual ICollection<TUCAppPromotion> TUCAppPromotionCreatedBy { get; set; } = new List<TUCAppPromotion>();

   public virtual ICollection<TUCAppPromotion> TUCAppPromotionModifyBy { get; set; } = new List<TUCAppPromotion>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountAccount { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountCreatedBy { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountManager { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountMerchant { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountModifyBy { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountOwner { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccountStore { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranch> TUCBranchCreatedBy { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCBranch> TUCBranchManager { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCBranch> TUCBranchModifyBy { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCBranch> TUCBranchOwner { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTargetCreatedBy { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTargetManager { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTargetModifyBy { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTargetRm { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCCategoryAccount> TUCCategoryAccountAccount { get; set; } = new List<TUCCategoryAccount>();

   public virtual ICollection<TUCCategoryAccount> TUCCategoryAccountCreatedBy { get; set; } = new List<TUCCategoryAccount>();

   public virtual ICollection<TUCCategoryAccount> TUCCategoryAccountModifyBy { get; set; } = new List<TUCCategoryAccount>();

   public virtual ICollection<TUCCategory> TUCCategoryCreatedBy { get; set; } = new List<TUCCategory>();

   public virtual ICollection<TUCCategory> TUCCategoryModifyBy { get; set; } = new List<TUCCategory>();

   public virtual ICollection<TUCLProgram> TUCLProgramAccount { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLProgram> TUCLProgramCreatedBy { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLProgramGroup> TUCLProgramGroupAccount { get; set; } = new List<TUCLProgramGroup>();

   public virtual ICollection<TUCLProgramGroup> TUCLProgramGroupCreatedBy { get; set; } = new List<TUCLProgramGroup>();

   public virtual ICollection<TUCLProgramGroup> TUCLProgramGroupModifyBy { get; set; } = new List<TUCLProgramGroup>();

   public virtual ICollection<TUCLProgram> TUCLProgramModifyBy { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyAcquirer { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyCashier { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyCreatedBy { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyCustomer { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyFromAccount { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyFromReferrer { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyMerchant { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyaltyMerchantCustomer> TUCLoyaltyMerchantCustomerCustomer { get; set; } = new List<TUCLoyaltyMerchantCustomer>();

   public virtual ICollection<TUCLoyaltyMerchantCustomer> TUCLoyaltyMerchantCustomerMerchant { get; set; } = new List<TUCLoyaltyMerchantCustomer>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyModifyBy { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingAcquirer { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingCashier { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingCreatedBy { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingCustomer { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingFromAccount { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingMerchant { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingModifyBy { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingProvider { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingStore { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingToAccount { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyProvider { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyStore { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyToAccount { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyToReferrer { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCMerchantCategory> TUCMerchantCategoryCreatedBy { get; set; } = new List<TUCMerchantCategory>();

   public virtual ICollection<TUCMerchantCategory> TUCMerchantCategoryModifyBy { get; set; } = new List<TUCMerchantCategory>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationAccount { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationCreatedBy { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationModifyBy { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCPayOut> TUCPayOutAccount { get; set; } = new List<TUCPayOut>();

   public virtual ICollection<TUCPayOut> TUCPayOutCreatedBy { get; set; } = new List<TUCPayOut>();

   public virtual ICollection<TUCPayOut> TUCPayOutModifyBy { get; set; } = new List<TUCPayOut>();

   public virtual ICollection<TUCPromoCodeAccount> TUCPromoCodeAccount { get; set; } = new List<TUCPromoCodeAccount>();

   public virtual ICollection<TUCPromoCode> TUCPromoCodeCreatedBy { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCPromoCode> TUCPromoCodeModifyBy { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanCashier { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanCreatedByNavigation { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanMerchant { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanModifyByNavigation { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanStore { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScanUserAccount { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCSale> TUCSaleCashier { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleCreatedBy { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleCustomer { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleMerchant { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleModifyBy { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleStore { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCStoreAcquirerCollection> TUCStoreAcquirerCollectionAcquirer { get; set; } = new List<TUCStoreAcquirerCollection>();

   public virtual ICollection<TUCStoreAcquirerCollection> TUCStoreAcquirerCollectionCreatedBy { get; set; } = new List<TUCStoreAcquirerCollection>();

   public virtual ICollection<TUCStoreAcquirerCollection> TUCStoreAcquirerCollectionModifyBy { get; set; } = new List<TUCStoreAcquirerCollection>();

   public virtual ICollection<TUCStoreAcquirerCollection> TUCStoreAcquirerCollectionStore { get; set; } = new List<TUCStoreAcquirerCollection>();

   public virtual ICollection<TUCTerminal> TUCTerminalAccount { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalAcquirer { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalAcquirerRm { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalCashier { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalCreatedBy { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalMerchant { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalMerchantRm { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalModifyBy { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminalProduct> TUCTerminalProductAccount { get; set; } = new List<TUCTerminalProduct>();

   public virtual ICollection<TUCTerminalProduct> TUCTerminalProductCreatedBy { get; set; } = new List<TUCTerminalProduct>();

   public virtual ICollection<TUCTerminalProduct> TUCTerminalProductModifyBy { get; set; } = new List<TUCTerminalProduct>();

   public virtual ICollection<TUCTerminal> TUCTerminalProvider { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalProviderRm { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminalStatus> TUCTerminalStatus { get; set; } = new List<TUCTerminalStatus>();

   public virtual ICollection<TUCTerminal> TUCTerminalStore { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCampaign> TUCampaignAccount { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudienceCreatedBy { get; set; } = new List<TUCampaignAudience>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudienceModifyBy { get; set; } = new List<TUCampaignAudience>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudienceUserAccount { get; set; } = new List<TUCampaignAudience>();

   public virtual ICollection<TUCampaign> TUCampaignCreatedBy { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCampaign> TUCampaignManager { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCampaign> TUCampaignModifyBy { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCard> TUCardActiveMerchant { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardActiveUserAccount { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardCashier { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardStore { get; set; } = new List<TUCard>();

   public virtual ICollection<TULead> TULeadAccount { get; set; } = new List<TULead>();

   public virtual ICollection<TULeadActivity> TULeadActivityCreatedBy { get; set; } = new List<TULeadActivity>();

   public virtual ICollection<TULeadActivity> TULeadActivityModifyBy { get; set; } = new List<TULeadActivity>();

   public virtual ICollection<TULead> TULeadAgent { get; set; } = new List<TULead>();

   public virtual ICollection<TULead> TULeadCreatedBy { get; set; } = new List<TULead>();

   public virtual ICollection<TULeadGroup> TULeadGroupAgent { get; set; } = new List<TULeadGroup>();

   public virtual ICollection<TULeadGroup> TULeadGroupCreatedBy { get; set; } = new List<TULeadGroup>();

   public virtual ICollection<TULeadGroup> TULeadGroupMerchant { get; set; } = new List<TULeadGroup>();

   public virtual ICollection<TULeadGroup> TULeadGroupModifyBy { get; set; } = new List<TULeadGroup>();

   public virtual ICollection<TULead> TULeadModifyBy { get; set; } = new List<TULead>();

   public virtual ICollection<TULead> TULeadOwner { get; set; } = new List<TULead>();

   public virtual ICollection<TULeadOwner> TULeadOwnerAgent { get; set; } = new List<TULeadOwner>();

   public virtual ICollection<TULeadOwner> TULeadOwnerCreatedBy { get; set; } = new List<TULeadOwner>();

   public virtual ICollection<TULeadOwner> TULeadOwnerModifyBy { get; set; } = new List<TULeadOwner>();

   public virtual ICollection<TUProduct> TUProductAccount { get; set; } = new List<TUProduct>();

   public virtual ICollection<TUProductCode> TUProductCodeAccount { get; set; } = new List<TUProductCode>();

   public virtual ICollection<TUProductCode> TUProductCodeCreatedBy { get; set; } = new List<TUProductCode>();

   public virtual ICollection<TUProductCode> TUProductCodeModifyBy { get; set; } = new List<TUProductCode>();

   public virtual ICollection<TUProduct> TUProductCreatedBy { get; set; } = new List<TUProduct>();

   public virtual ICollection<TUProduct> TUProductModifyBy { get; set; } = new List<TUProduct>();

    public virtual HCCoreCommon? TimeZone { get; set; }

    public virtual HCUAccountAuth User { get; set; } = null!;

   public virtual ICollection<VASCategory> VASCategoryCreatedBy { get; set; } = new List<VASCategory>();

   public virtual ICollection<VASCategory> VASCategoryModifyBy { get; set; } = new List<VASCategory>();

   public virtual ICollection<VASProduct> VASProductCreatedBy { get; set; } = new List<VASProduct>();

   public virtual ICollection<VASProductItem> VASProductItemCreatedBy { get; set; } = new List<VASProductItem>();

   public virtual ICollection<VASProductItem> VASProductItemModifyBy { get; set; } = new List<VASProductItem>();

   public virtual ICollection<VASProduct> VASProductModifyBy { get; set; } = new List<VASProduct>();

   public virtual ICollection<VasPayment> VasPaymentAccount { get; set; } = new List<VasPayment>();

   public virtual ICollection<VasPayment> VasPaymentCreatedBy { get; set; } = new List<VasPayment>();

   public virtual ICollection<VasPayment> VasPaymentModifyBy { get; set; } = new List<VasPayment>();

   public virtual ICollection<cmt_loyalty_customer> cmt_loyalty_customercustomer { get; set; } = new List<cmt_loyalty_customer>();

   public virtual ICollection<cmt_loyalty_customer> cmt_loyalty_customerowner { get; set; } = new List<cmt_loyalty_customer>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltyacquirer { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltycashier { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltycreated_by { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltycustomer { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltyfrom_account { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltymerchant { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltymodify_by { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltyprovider { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltystore { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltyto_account { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedacquirer { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedcashier { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedcreated_by { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedcustomer { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedmerchant { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedmodify_by { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedprovider { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedstore { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale> cmt_saleacquirer { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salecashier { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salecreated_by { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salecustomer { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salemerchant { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salemodify_by { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_saleprovider { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salestore { get; set; } = new List<cmt_sale>();
}
