﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCCategory
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? ParentCategoryId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCUAccount> HCUAccountPrimaryCategory { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> HCUAccountSecondaryCategory { get; set; } = new List<HCUAccount>();

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<TUCCategory> InverseParentCategory { get; set; } = new List<TUCCategory>();

   public virtual ICollection<MDCategory> MDCategory { get; set; } = new List<MDCategory>();

   public virtual ICollection<MDDeal> MDDealCategory { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDeal> MDDealSubcategory { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDPackaging> MDPackaging { get; set; } = new List<MDPackaging>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual TUCCategory? ParentCategory { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCCategoryAccount> TUCCategoryAccount { get; set; } = new List<TUCCategoryAccount>();

   public virtual ICollection<TUCMerchantCategory> TUCMerchantCategory { get; set; } = new List<TUCMerchantCategory>();
}
