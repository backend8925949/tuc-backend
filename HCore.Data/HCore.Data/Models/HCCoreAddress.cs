﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreAddress
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? DisplayName { get; set; }

    public int? LocationTypeId { get; set; }

    public sbyte? IsPrimaryAddress { get; set; }

    public long? AccountId { get; set; }

    public string? Name { get; set; }

    public string? ContactNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string AddressLine1 { get; set; } = null!;

    public string? AddressLine2 { get; set; }

    public string? ZipCode { get; set; }

    public string? Landmark { get; set; }

    public long? CityId { get; set; }

    public long? CityAreaId { get; set; }

    public long? StateId { get; set; }

    public int CountryId { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public string? MapAddress { get; set; }

    public string? Instructions { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public string? AdditionalContactNumber { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreCountryStateCity? City { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea { get; set; }

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

   public virtual ICollection<LSShipments> LSShipmentsFromAddress { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsReturnAddress { get; set; } = new List<LSShipments>();

   public virtual ICollection<LSShipments> LSShipmentsToAddress { get; set; } = new List<LSShipments>();

    public virtual HCCore? LocationType { get; set; }

   public virtual ICollection<MDDealAddress> MDDealAddress { get; set; } = new List<MDDealAddress>();

   public virtual ICollection<MDDealCode> MDDealCodeFromAddress { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeToAddress { get; set; } = new List<MDDealCode>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreCountryState? State { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
