﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCSubscription
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public int AccountTypeId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public double? ActualPrice { get; set; }

    public double? SellingPrice { get; set; }

    public int Days { get; set; }

    public int AdditionalDays { get; set; }

    public int TotalDays { get; set; }

    public string? Policy { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public int CountryId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCCore AccountType { get; set; } = null!;

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCSubscriptionFeature> HCSubscriptionFeature { get; set; } = new List<HCSubscriptionFeature>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscription { get; set; } = new List<HCUAccountSubscription>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCLProgram> TUCLProgram { get; set; } = new List<TUCLProgram>();

    public virtual HCCore Type { get; set; } = null!;
}
