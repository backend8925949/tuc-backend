﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUProduct
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long? ParentId { get; set; }

    public long? AccountId { get; set; }

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public string? Description { get; set; }

    public double? UnitPrice { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public long? Quantity { get; set; }

    public int? RewardTypeId { get; set; }

    public double? RewardValue { get; set; }

    public long? IconStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<TUProduct> InverseParent { get; set; } = new List<TUProduct>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual TUProduct? Parent { get; set; }

    public virtual HCCore? RewardType { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUProductCode> TUProductCode { get; set; } = new List<TUProductCode>();

    public virtual HCCore Type { get; set; } = null!;
}
