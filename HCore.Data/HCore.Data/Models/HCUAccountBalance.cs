﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountBalance
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long? ParentId { get; set; }

    public int SourceId { get; set; }

    public double Credit { get; set; }

    public long? CreditTransaction { get; set; }

    public double Debit { get; set; }

    public long? DebitTransaction { get; set; }

    public double Balance { get; set; }

    public long Transactions { get; set; }

    public DateTime? LastTransactionDate { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public long? ProgramId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount? Parent { get; set; }

    public virtual TUCLProgram? Program { get; set; }

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
