﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCCardRewardProcessing
{
    public long Id { get; set; }

    public string? FileName { get; set; }

    public string? DumpProvider { get; set; }

    public string? FilePath { get; set; }

    public DateTime? DateObtained { get; set; }

    public DateTime? DateProcessed { get; set; }

    public DateTime? ProcessEndDate { get; set; }

    public long? TotalEntries { get; set; }

    public long? ParentId { get; set; }

    public long? ProcessedEntries { get; set; }

    public long? FailedEntries { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }
}
