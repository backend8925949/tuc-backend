﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCNinjaRegistration
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? UserAccountId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? MobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? Address { get; set; }

    public long? ImageStorageId { get; set; }

    public long? ProofStorageId { get; set; }

    public long? UtilityBillStorageId { get; set; }

    public string? VehicleNumber { get; set; }

    public string? Reference1Name { get; set; }

    public string? Reference1ContactNumber { get; set; }

    public string? Reference2Name { get; set; }

    public string? Reference2ContactNumber { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public long? AccountId { get; set; }

    public string? Comment { get; set; }

    public string? FacebookUrl { get; set; }

    public string? TwitterUrl { get; set; }

    public string? InstagramUrl { get; set; }

    public string? LinkedInUrl { get; set; }

    public int? CountryId { get; set; }

    public long? RegionId { get; set; }

    public long? CityId { get; set; }

    public long? CityArea1Id { get; set; }

    public long? CityArea2Id { get; set; }

    public long? CityArea3Id { get; set; }

    public long? CityArea4Id { get; set; }

    public string? BankName { get; set; }

    public string? BankAccountNumber { get; set; }

    public string? BankAccountName { get; set; }

    public string? BankCode { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreCountryStateCity? City { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea1 { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea2 { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea3 { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea4 { get; set; }

    public virtual HCCoreCountry? Country { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCCoreStorage? ImageStorage { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreStorage? ProofStorage { get; set; }

    public virtual HCCoreParameter? Region { get; set; }

    public virtual HCCore? Status { get; set; }

    public virtual HCCoreStorage? UtilityBillStorage { get; set; }
}
