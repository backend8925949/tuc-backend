﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDeal
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long AccountId { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public string? Terms { get; set; }

    public int UsageTypeId { get; set; }

    public string? UsageInformation { get; set; }

    public int? IsPinRequired { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public long? CodeValidtyDays { get; set; }

    public DateTime? CodeValidityStartDate { get; set; }

    public DateTime? CodeValidityEndDate { get; set; }

    public double? ActualPrice { get; set; }

    public double? SellingPrice { get; set; }

    public int? DiscountTypeId { get; set; }

    public double? DiscountAmount { get; set; }

    public double? DiscountPercentage { get; set; }

    public double? Amount { get; set; }

    public double? Charge { get; set; }

    public double? CommissionAmount { get; set; }

    public double? CommissionPercentage { get; set; }

    public double? TotalAmount { get; set; }

    public long? MaximumUnitSale { get; set; }

    public long? MaximumUnitSalePerDay { get; set; }

    public long? MaximumUnitSalePerPerson { get; set; }

    public long? PosterStorageId { get; set; }

    public long Views { get; set; }

    public long Like { get; set; }

    public long DisLike { get; set; }

    public string? Message { get; set; }

    public int? CategoryId { get; set; }

    public int? SettlementTypeId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long? TView { get; set; }

    public long? TLike { get; set; }

    public long? TDislike { get; set; }

    public long? TPurchase { get; set; }

    public long? ApproverId { get; set; }

    public string? ApproverComment { get; set; }

    public DateTime? ApprovalDate { get; set; }

    public int? SubcategoryId { get; set; }

    public int? VisiblityStatusId { get; set; }

    public string? TitleContent { get; set; }

    public int? TitleTypeId { get; set; }

    public DateTime? SyncTime { get; set; }

    public int? BuyerTypeId { get; set; }

    public int? DealTypeId { get; set; }

    public int? DeliveryTypeId { get; set; }

    public string? Slug { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount? Approver { get; set; }

    public virtual TUCCategory? Category { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCCore? DealType { get; set; }

    public virtual HCCore? DeliveryType { get; set; }

    public virtual HCCore? DiscountType { get; set; }

   public virtual ICollection<LSOperations> LSOperations { get; set; } = new List<LSOperations>();

   public virtual ICollection<LSPackages> LSPackages { get; set; } = new List<LSPackages>();

   public virtual ICollection<LSParcel> LSParcel { get; set; } = new List<LSParcel>();

   public virtual ICollection<LSShipments> LSShipments { get; set; } = new List<LSShipments>();

   public virtual ICollection<MDDealBookmark> MDDealBookmark { get; set; } = new List<MDDealBookmark>();

   public virtual ICollection<MDDealCart> MDDealCart { get; set; } = new List<MDDealCart>();

   public virtual ICollection<MDDealCode> MDDealCode { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealGallery> MDDealGallery { get; set; } = new List<MDDealGallery>();

   public virtual ICollection<MDDealLike> MDDealLike { get; set; } = new List<MDDealLike>();

   public virtual ICollection<MDDealLocation> MDDealLocation { get; set; } = new List<MDDealLocation>();

   public virtual ICollection<MDDealNotification> MDDealNotification { get; set; } = new List<MDDealNotification>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCode { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromotion> MDDealPromotion { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<MDDealReview> MDDealReview { get; set; } = new List<MDDealReview>();

   public virtual ICollection<MDDealShedule> MDDealShedule { get; set; } = new List<MDDealShedule>();

   public virtual ICollection<MDDealView> MDDealView { get; set; } = new List<MDDealView>();

   public virtual ICollection<MDDealWishlist> MDDealWishlist { get; set; } = new List<MDDealWishlist>();

   public virtual ICollection<MDFlashDeal> MDFlashDeal { get; set; } = new List<MDFlashDeal>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

    public virtual HCCore? SettlementType { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual TUCCategory? Subcategory { get; set; }

    public virtual HCCore Type { get; set; } = null!;

    public virtual HCCore UsageType { get; set; } = null!;
}
