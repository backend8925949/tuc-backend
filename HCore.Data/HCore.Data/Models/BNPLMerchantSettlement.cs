﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLMerchantSettlement
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long MerchantId { get; set; }

    public long LoanId { get; set; }

    public long ProviderId { get; set; }

    public long? TransactionReference { get; set; }

    public DateTime? LoanRedeenDate { get; set; }

    public DateTime? SettlementDate { get; set; }

    public double? LoanAmount { get; set; }

    public double? TucFees { get; set; }

    public double? SettlementAmount { get; set; }

    public string? InvoiceNumber { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyByid { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual BNPLAccountLoan Loan { get; set; } = null!;

    public virtual BNPLMerchant Merchant { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount Provider { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccountTransaction? TransactionReferenceNavigation { get; set; }
}
