﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountInvoice
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long? ParentId { get; set; }

    public long? AccountId { get; set; }

    public string? InoviceNumber { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public string? FromName { get; set; }

    public string? FromAddress { get; set; }

    public string? FromContactNumber { get; set; }

    public string? FromEmailAddress { get; set; }

    public string? FromFax { get; set; }

    public string? ToName { get; set; }

    public string? ToAddress { get; set; }

    public string? ToContactNumber { get; set; }

    public string? ToEmailAddress { get; set; }

    public string? ToFax { get; set; }

    public long? TotalItem { get; set; }

    public double? UnitCost { get; set; }

    public double Amount { get; set; }

    public double ChargePercentage { get; set; }

    public double Charge { get; set; }

    public double DiscountPercentage { get; set; }

    public double DiscountAmount { get; set; }

    public double ComissionPercentage { get; set; }

    public double? ComissionAmount { get; set; }

    public double? TotalAmount { get; set; }

    public DateTime? InoviceDate { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime? PaymentDate { get; set; }

    public string? PaymentReference { get; set; }

    public int? PaymentModeId { get; set; }

    public long? PaymentProofId { get; set; }

    public long? PaymentApproverId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCTransaction> HCTransaction { get; set; } = new List<HCTransaction>();

   public virtual ICollection<HCUAccountInvoice> InverseParent { get; set; } = new List<HCUAccountInvoice>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccountInvoice? Parent { get; set; }

    public virtual HCUAccount? PaymentApprover { get; set; }

    public virtual HCCore? PaymentMode { get; set; }

    public virtual HCCoreStorage? PaymentProof { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore Type { get; set; } = null!;
}
