﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCTerminalStatus
{
    public long Id { get; set; }

    public long? TerminalId { get; set; }

    public DateTime Date { get; set; }

    public long? ActivityStatusId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? BankId { get; set; }

    public long? PtspId { get; set; }

    public long? RmId { get; set; }

    public long? ManagerId { get; set; }

    public long? StatusId { get; set; }

    public virtual HCUAccount? Terminal { get; set; }
}
