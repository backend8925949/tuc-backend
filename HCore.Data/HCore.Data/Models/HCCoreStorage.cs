﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreStorage
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? FolderId { get; set; }

    public long? AccountId { get; set; }

    public int StorageTypeId { get; set; }

    public int SourceId { get; set; }

    public string Name { get; set; } = null!;

    public string Path { get; set; } = null!;

    public string? ThumbnailPath { get; set; }

    public string? Extension { get; set; }

    public double? Size { get; set; }

    public int? HelperId { get; set; }

    public DateTime CreateDate { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public int? Width { get; set; }

    public int? Height { get; set; }

    public string? Small { get; set; }

    public string? Medium { get; set; }

    public string? Lasrge { get; set; }

    public virtual HCUAccount? Account { get; set; }

   public virtual ICollection<CAProduct> CAProductIconStorage { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProduct> CAProductPosterStorage { get; set; } = new List<CAProduct>();

    public virtual HCCoreStorageFolder? Folder { get; set; }

   public virtual ICollection<HCCoreCommon> HCCoreCommonIconStorage { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonPosterStorage { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterIconStorage { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterPosterStorage { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreStorageTag> HCCoreStorageTag { get; set; } = new List<HCCoreStorageTag>();

   public virtual ICollection<HCUAccount> HCUAccountIconStorage { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoice { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterIconStorage { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterPosterStorage { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccount> HCUAccountPosterStorage { get; set; } = new List<HCUAccount>();

    public virtual HCCore? Helper { get; set; }

   public virtual ICollection<LSShipments> LSShipments { get; set; } = new List<LSShipments>();

   public virtual ICollection<MDDeal> MDDeal { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealGallery> MDDealGallery { get; set; } = new List<MDDealGallery>();

   public virtual ICollection<MDDealPromotion> MDDealPromotion { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<SCCategory> SCCategoryPrimaryStorage { get; set; } = new List<SCCategory>();

   public virtual ICollection<SCCategory> SCCategorySecondaryStorage { get; set; } = new List<SCCategory>();

   public virtual ICollection<SCConfiguration> SCConfiguration { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCProduct> SCProduct { get; set; } = new List<SCProduct>();

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore StorageType { get; set; } = null!;

   public virtual ICollection<TUCAppPromotion> TUCAppPromotion { get; set; } = new List<TUCAppPromotion>();

   public virtual ICollection<TUCCategory> TUCCategoryIconStorage { get; set; } = new List<TUCCategory>();

   public virtual ICollection<TUCCategory> TUCCategoryPosterStorage { get; set; } = new List<TUCCategory>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationImageStorage { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationProofStorage { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistrationUtilityBillStorage { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScan { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCTerminal> TUCTerminal { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUProduct> TUProduct { get; set; } = new List<TUProduct>();

   public virtual ICollection<VASProduct> VASProduct { get; set; } = new List<VASProduct>();
}
