﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TULead
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? OwnerId { get; set; }

    public long? AgentId { get; set; }

    public long? AccountId { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? Agent { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Owner { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TULeadActivity> TULeadActivity { get; set; } = new List<TULeadActivity>();

   public virtual ICollection<TULeadOwner> TULeadOwner { get; set; } = new List<TULeadOwner>();
}
