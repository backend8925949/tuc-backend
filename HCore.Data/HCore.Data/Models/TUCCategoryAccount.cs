﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCCategoryAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public int CategoryId { get; set; }

    public int? TypeId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual TUCCategory Category { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
