﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreStorageFolder
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string Path { get; set; } = null!;

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCCoreStorage> HCCoreStorage { get; set; } = new List<HCCoreStorage>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
