﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSCampaign
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public string Title { get; set; } = null!;

    public DateTime SendDate { get; set; }

    public int SenderId { get; set; }

    public string? Message { get; set; }

    public int? RecipientTypeId { get; set; }

    public int? RecipientSubTypeId { get; set; }

    public double? Budget { get; set; }

    public double? PerItemCost { get; set; }

    public int ItemLimit { get; set; }

    public int Total { get; set; }

    public int PriorityId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? ReviewerComment { get; set; }

    public long? ReviewerId { get; set; }

    public long? Unsent { get; set; }

    public long? Sent { get; set; }

    public long? Delivered { get; set; }

    public long? Pending { get; set; }

    public long? Failed { get; set; }

    public long? TransactionId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? RecipientSubType { get; set; }

    public virtual HCCore? RecipientType { get; set; }

    public virtual HCUAccount? Reviewer { get; set; }

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroup { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSCampaignGroupItem> SMSCampaignGroupItem { get; set; } = new List<SMSCampaignGroupItem>();

    public virtual SMSSenderProvider Sender { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
