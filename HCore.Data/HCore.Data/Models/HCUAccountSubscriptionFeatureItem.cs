﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountSubscriptionFeatureItem
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountSubscriptionFeatureId { get; set; }

    public long SubscriptionFeatureId { get; set; }

    public long SystemFeatureId { get; set; }

    public long? MinimumLimit { get; set; }

    public long? MaximumLimit { get; set; }

    public double? MinimumAmount { get; set; }

    public double? MaximumAmount { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccountSubscriptionFeature AccountSubscriptionFeature { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCSubscriptionFeatureItem SubscriptionFeature { get; set; } = null!;

    public virtual HCCoreCommon SystemFeature { get; set; } = null!;
}
