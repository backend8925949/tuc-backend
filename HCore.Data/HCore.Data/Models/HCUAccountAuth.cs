﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountAuth
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string? SecondaryPassword { get; set; }

    public string SystemPassword { get; set; } = null!;

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? ShortCode { get; set; }

    public string? OperatorID { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
