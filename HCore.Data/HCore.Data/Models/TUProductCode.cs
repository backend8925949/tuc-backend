﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUProductCode
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public long BatchId { get; set; }

    public string ItemCode { get; set; } = null!;

    public long? AccountId { get; set; }

    public DateTime? UseDate { get; set; }

    public long? TransactionId { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual TUProduct Batch { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore Type { get; set; } = null!;
}
