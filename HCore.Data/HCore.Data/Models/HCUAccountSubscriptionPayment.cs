﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountSubscriptionPayment
{
    public long Id { get; set; }

    public long AccountId { get; set; }

    public long AccountSubscriptionId { get; set; }

    public DateTime? TransactionDate { get; set; }

    public double? Amount { get; set; }

    public double? Charge { get; set; }

    public double? CommissionAmount { get; set; }

    public double? TotalAmount { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccountSubscription AccountSubscription { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
