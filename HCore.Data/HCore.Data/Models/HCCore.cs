﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCore
{
    public int Id { get; set; }

    public string? Guid { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public int? ParentId { get; set; }

    public int? SubParentId { get; set; }

    public int? Sequence { get; set; }

    public string? Value { get; set; }

    public string? TypeName { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

   public virtual ICollection<BNPLAccount> BNPLAccount { get; set; } = new List<BNPLAccount>();

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoan { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLAccountLoanActivity> BNPLAccountLoanActivity { get; set; } = new List<BNPLAccountLoanActivity>();

   public virtual ICollection<BNPLAccountLoanPayment> BNPLAccountLoanPayment { get; set; } = new List<BNPLAccountLoanPayment>();

   public virtual ICollection<BNPLConfiguration> BNPLConfiguration { get; set; } = new List<BNPLConfiguration>();

   public virtual ICollection<BNPLLoanProcess> BNPLLoanProcess { get; set; } = new List<BNPLLoanProcess>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantSettelmentType { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlement { get; set; } = new List<BNPLMerchantSettlement>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantStatus { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLMerchant> BNPLMerchantTransactionSource { get; set; } = new List<BNPLMerchant>();

   public virtual ICollection<BNPLPlan> BNPLPlan { get; set; } = new List<BNPLPlan>();

   public virtual ICollection<BNPLPrequalification> BNPLPrequalification { get; set; } = new List<BNPLPrequalification>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupGender { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupStatus { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupSubType { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductAccountGroup> CAProductAccountGroupType { get; set; } = new List<CAProductAccountGroup>();

   public virtual ICollection<CAProductCode> CAProductCodeStatus { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductCode> CAProductCodeSubType { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductCode> CAProductCodeType { get; set; } = new List<CAProductCode>();

   public virtual ICollection<CAProductLocation> CAProductLocation { get; set; } = new List<CAProductLocation>();

   public virtual ICollection<CAProductShedule> CAProductShedule { get; set; } = new List<CAProductShedule>();

   public virtual ICollection<CAProduct> CAProductStatus { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProduct> CAProductSubType { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProduct> CAProductType { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProduct> CAProductUsageType { get; set; } = new List<CAProduct>();

   public virtual ICollection<CAProductUseHistory> CAProductUseHistory { get; set; } = new List<CAProductUseHistory>();

   public virtual ICollection<HCCoreAddress> HCCoreAddressLocationType { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreAddress> HCCoreAddressStatus { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreApp> HCCoreApp { get; set; } = new List<HCCoreApp>();

   public virtual ICollection<HCCoreAppVersion> HCCoreAppVersion { get; set; } = new List<HCCoreAppVersion>();

   public virtual ICollection<HCCoreAppVersionToken> HCCoreAppVersionToken { get; set; } = new List<HCCoreAppVersionToken>();

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumber { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonHelper { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonStatus { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonSubType { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> HCCoreCommonType { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationAccountType { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationDataType { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreConfigurationHistory> HCCoreConfigurationHistoryStatus { get; set; } = new List<HCCoreConfigurationHistory>();

   public virtual ICollection<HCCoreConfigurationHistory> HCCoreConfigurationHistoryValueHelper { get; set; } = new List<HCCoreConfigurationHistory>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationStatus { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreConfiguration> HCCoreConfigurationValueHelper { get; set; } = new List<HCCoreConfiguration>();

   public virtual ICollection<HCCoreCountry> HCCoreCountry { get; set; } = new List<HCCoreCountry>();

   public virtual ICollection<HCCoreCountryState> HCCoreCountryState { get; set; } = new List<HCCoreCountryState>();

   public virtual ICollection<HCCoreCountryStateCity> HCCoreCountryStateCity { get; set; } = new List<HCCoreCountryStateCity>();

   public virtual ICollection<HCCoreCountryStateCityArea> HCCoreCountryStateCityArea { get; set; } = new List<HCCoreCountryStateCityArea>();

   public virtual ICollection<HCCoreFeature> HCCoreFeatureHelper { get; set; } = new List<HCCoreFeature>();

   public virtual ICollection<HCCoreFeature> HCCoreFeatureStatus { get; set; } = new List<HCCoreFeature>();

   public virtual ICollection<HCCoreFeature> HCCoreFeatureType { get; set; } = new List<HCCoreFeature>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterHelper { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterStatus { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterSubType { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterType { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreRoleFeature> HCCoreRoleFeature { get; set; } = new List<HCCoreRoleFeature>();

   public virtual ICollection<HCCoreRole> HCCoreRoleHelper { get; set; } = new List<HCCoreRole>();

   public virtual ICollection<HCCoreRole> HCCoreRoleStatus { get; set; } = new List<HCCoreRole>();

   public virtual ICollection<HCCoreStorageFolder> HCCoreStorageFolder { get; set; } = new List<HCCoreStorageFolder>();

   public virtual ICollection<HCCoreStorage> HCCoreStorageHelper { get; set; } = new List<HCCoreStorage>();

   public virtual ICollection<HCCoreStorage> HCCoreStorageSource { get; set; } = new List<HCCoreStorage>();

   public virtual ICollection<HCCoreStorage> HCCoreStorageStatus { get; set; } = new List<HCCoreStorage>();

   public virtual ICollection<HCCoreStorage> HCCoreStorageStorageType { get; set; } = new List<HCCoreStorage>();

   public virtual ICollection<HCCoreVerification> HCCoreVerificationStatus { get; set; } = new List<HCCoreVerification>();

   public virtual ICollection<HCCoreVerification> HCCoreVerificationVerificationType { get; set; } = new List<HCCoreVerification>();

   public virtual ICollection<HCSubscription> HCSubscriptionAccountType { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCSubscriptionFeature> HCSubscriptionFeature { get; set; } = new List<HCSubscriptionFeature>();

   public virtual ICollection<HCSubscriptionFeatureItem> HCSubscriptionFeatureItem { get; set; } = new List<HCSubscriptionFeatureItem>();

   public virtual ICollection<HCSubscription> HCSubscriptionStatus { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCSubscription> HCSubscriptionType { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCToken> HCToken { get; set; } = new List<HCToken>();

   public virtual ICollection<HCUAccount> HCUAccountAccountLevelNavigation { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> HCUAccountAccountOperationType { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccount> HCUAccountAccountType { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountActivity> HCUAccountActivityActivityType { get; set; } = new List<HCUAccountActivity>();

   public virtual ICollection<HCUAccount> HCUAccountActivityStatus { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountActivity> HCUAccountActivityStatusNavigation { get; set; } = new List<HCUAccountActivity>();

   public virtual ICollection<HCUAccount> HCUAccountApplicationStatus { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountAuth> HCUAccountAuth { get; set; } = new List<HCUAccountAuth>();

   public virtual ICollection<HCUAccountBalance> HCUAccountBalanceSource { get; set; } = new List<HCUAccountBalance>();

   public virtual ICollection<HCUAccountBalance> HCUAccountBalanceStatus { get; set; } = new List<HCUAccountBalance>();

   public virtual ICollection<HCUAccountBank> HCUAccountBank { get; set; } = new List<HCUAccountBank>();

   public virtual ICollection<HCUAccountConfigurationHistory> HCUAccountConfigurationHistoryStatus { get; set; } = new List<HCUAccountConfigurationHistory>();

   public virtual ICollection<HCUAccountConfigurationHistory> HCUAccountConfigurationHistoryValueHelper { get; set; } = new List<HCUAccountConfigurationHistory>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfigurationStatus { get; set; } = new List<HCUAccountConfiguration>();

   public virtual ICollection<HCUAccountConfiguration> HCUAccountConfigurationValueHelper { get; set; } = new List<HCUAccountConfiguration>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceStatus { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccountDevice> HCUAccountDeviceType { get; set; } = new List<HCUAccountDevice>();

   public virtual ICollection<HCUAccount> HCUAccountGender { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoicePaymentMode { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoiceStatus { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountInvoice> HCUAccountInvoiceType { get; set; } = new List<HCUAccountInvoice>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerAccountType { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountOwner> HCUAccountOwnerStatus { get; set; } = new List<HCUAccountOwner>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterHelper { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterStatus { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterSubType { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameterType { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccount> HCUAccountRegistrationSource { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountSession> HCUAccountSessionPlatform { get; set; } = new List<HCUAccountSession>();

   public virtual ICollection<HCUAccountSession> HCUAccountSessionStatus { get; set; } = new List<HCUAccountSession>();

   public virtual ICollection<HCUAccount> HCUAccountStatus { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeature { get; set; } = new List<HCUAccountSubscriptionFeature>();

   public virtual ICollection<HCUAccountSubscriptionFeatureItem> HCUAccountSubscriptionFeatureItem { get; set; } = new List<HCUAccountSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPayment { get; set; } = new List<HCUAccountSubscriptionPayment>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscriptionStatus { get; set; } = new List<HCUAccountSubscription>();

   public virtual ICollection<HCUAccountSubscription> HCUAccountSubscriptionType { get; set; } = new List<HCUAccountSubscription>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionMode { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionPaymentMethod { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionSource { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionStatus { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionType { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCCore> InverseParent { get; set; } = new List<HCCore>();

   public virtual ICollection<HCCore> InverseStatus { get; set; } = new List<HCCore>();

   public virtual ICollection<HCCore> InverseSubParent { get; set; } = new List<HCCore>();

   public virtual ICollection<LSCarriers> LSCarriers { get; set; } = new List<LSCarriers>();

   public virtual ICollection<LSPackages> LSPackages { get; set; } = new List<LSPackages>();

   public virtual ICollection<LSParcel> LSParcel { get; set; } = new List<LSParcel>();

   public virtual ICollection<LSShipments> LSShipments { get; set; } = new List<LSShipments>();

   public virtual ICollection<MDCategory> MDCategory { get; set; } = new List<MDCategory>();

   public virtual ICollection<MDDealBookmark> MDDealBookmark { get; set; } = new List<MDDealBookmark>();

   public virtual ICollection<MDDealCart> MDDealCart { get; set; } = new List<MDDealCart>();

   public virtual ICollection<MDDealCode> MDDealCodeLastUseSourceNavigation { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDealCode> MDDealCodeStatus { get; set; } = new List<MDDealCode>();

   public virtual ICollection<MDDeal> MDDealDealType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDeal> MDDealDeliveryType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDeal> MDDealDiscountType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeStatus { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeType { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromoCode> MDDealPromoCodeValueType { get; set; } = new List<MDDealPromoCode>();

   public virtual ICollection<MDDealPromotion> MDDealPromotionStatus { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<MDDealPromotion> MDDealPromotionType { get; set; } = new List<MDDealPromotion>();

   public virtual ICollection<MDDealReview> MDDealReview { get; set; } = new List<MDDealReview>();

   public virtual ICollection<MDDeal> MDDealSettlementType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealShedule> MDDealSheduleStatus { get; set; } = new List<MDDealShedule>();

   public virtual ICollection<MDDealShedule> MDDealSheduleType { get; set; } = new List<MDDealShedule>();

   public virtual ICollection<MDDeal> MDDealStatus { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDeal> MDDealType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDeal> MDDealUsageType { get; set; } = new List<MDDeal>();

   public virtual ICollection<MDDealView> MDDealView { get; set; } = new List<MDDealView>();

   public virtual ICollection<MDFlashDeal> MDFlashDeal { get; set; } = new List<MDFlashDeal>();

    public virtual HCCore? Parent { get; set; }

   public virtual ICollection<SCCategory> SCCategory { get; set; } = new List<SCCategory>();

   public virtual ICollection<SCConfiguration> SCConfigurationDataType { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCConfiguration> SCConfigurationStatus { get; set; } = new List<SCConfiguration>();

   public virtual ICollection<SCOrderActivity> SCOrderActivityOrderStatus { get; set; } = new List<SCOrderActivity>();

   public virtual ICollection<SCOrderActivity> SCOrderActivityStatus { get; set; } = new List<SCOrderActivity>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressStatus { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrderAddress> SCOrderAddressType { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrder> SCOrderDeliveryMode { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderDeliveryType { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrderItem> SCOrderItem { get; set; } = new List<SCOrderItem>();

   public virtual ICollection<SCOrder> SCOrderPaymentMode { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderPaymentStatus { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderPriority { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCOrder> SCOrderStatus { get; set; } = new List<SCOrder>();

   public virtual ICollection<SCProduct> SCProduct { get; set; } = new List<SCProduct>();

   public virtual ICollection<SCProductVarient> SCProductVarient { get; set; } = new List<SCProductVarient>();

   public virtual ICollection<SCProductVarientStock> SCProductVarientStock { get; set; } = new List<SCProductVarientStock>();

   public virtual ICollection<SMSCampaignGroupItem> SMSCampaignGroupItem { get; set; } = new List<SMSCampaignGroupItem>();

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroupSource { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSCampaignGroup> SMSCampaignGroupStatus { get; set; } = new List<SMSCampaignGroup>();

   public virtual ICollection<SMSCampaign> SMSCampaignRecipientSubType { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSCampaign> SMSCampaignRecipientType { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSCampaign> SMSCampaignStatus { get; set; } = new List<SMSCampaign>();

   public virtual ICollection<SMSGroupItem> SMSGroupItem { get; set; } = new List<SMSGroupItem>();

   public virtual ICollection<SMSGroup> SMSGroupSource { get; set; } = new List<SMSGroup>();

   public virtual ICollection<SMSGroup> SMSGroupStatus { get; set; } = new List<SMSGroup>();

   public virtual ICollection<SMSSenderProvider> SMSSenderProvider { get; set; } = new List<SMSSenderProvider>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? SubParent { get; set; }

   public virtual ICollection<TUCAppPromotion> TUCAppPromotion { get; set; } = new List<TUCAppPromotion>();

   public virtual ICollection<TUCBranch> TUCBranch { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCBranchAccount> TUCBranchAccount { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTarget { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCCategory> TUCCategory { get; set; } = new List<TUCCategory>();

   public virtual ICollection<TUCCategoryAccount> TUCCategoryAccount { get; set; } = new List<TUCCategoryAccount>();

   public virtual ICollection<TUCLProgram> TUCLProgramComissionType { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLProgramGroup> TUCLProgramGroup { get; set; } = new List<TUCLProgramGroup>();

   public virtual ICollection<TUCLProgram> TUCLProgramProgramType { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLProgram> TUCLProgramStatus { get; set; } = new List<TUCLProgram>();

   public virtual ICollection<TUCLoyaltyMerchantCustomer> TUCLoyaltyMerchantCustomer { get; set; } = new List<TUCLoyaltyMerchantCustomer>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyMode { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyPaymentMethod { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingLoyaltyType { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingMode { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingSource { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingStatus { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPendingType { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltySource { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyStatus { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyalty> TUCLoyaltyType { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCMerchantCategory> TUCMerchantCategory { get; set; } = new List<TUCMerchantCategory>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistration { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCPayOut> TUCPayOutSource { get; set; } = new List<TUCPayOut>();

   public virtual ICollection<TUCPayOut> TUCPayOutStatus { get; set; } = new List<TUCPayOut>();

   public virtual ICollection<TUCPromoCode> TUCPromoCodeAccountType { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCPromoCodeCondition> TUCPromoCodeConditionAccountType { get; set; } = new List<TUCPromoCodeCondition>();

   public virtual ICollection<TUCPromoCodeCondition> TUCPromoCodeConditionHelper { get; set; } = new List<TUCPromoCodeCondition>();

   public virtual ICollection<TUCPromoCodeCondition> TUCPromoCodeConditionStatus { get; set; } = new List<TUCPromoCodeCondition>();

   public virtual ICollection<TUCPromoCode> TUCPromoCodeStatus { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCPromoCode> TUCPromoCodeType { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCReceiptScan> TUCReceiptScan { get; set; } = new List<TUCReceiptScan>();

   public virtual ICollection<TUCSale> TUCSalePaymentMode { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleSource { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleStatus { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCSale> TUCSaleType { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUCStoreAcquirerCollection> TUCStoreAcquirerCollection { get; set; } = new List<TUCStoreAcquirerCollection>();

   public virtual ICollection<TUCTerminal> TUCTerminalActivityStatus { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminalProduct> TUCTerminalProduct { get; set; } = new List<TUCTerminalProduct>();

   public virtual ICollection<TUCTerminal> TUCTerminalStatus { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCTerminal> TUCTerminalType { get; set; } = new List<TUCTerminal>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudienceStatus { get; set; } = new List<TUCampaignAudience>();

   public virtual ICollection<TUCampaignAudience> TUCampaignAudienceType { get; set; } = new List<TUCampaignAudience>();

   public virtual ICollection<TUCampaign> TUCampaignStatus { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCampaign> TUCampaignSubType { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCampaign> TUCampaignType { get; set; } = new List<TUCampaign>();

   public virtual ICollection<TUCard> TUCardCardTagType { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardCardType { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardPrepaidCardType { get; set; } = new List<TUCard>();

   public virtual ICollection<TUCard> TUCardStatus { get; set; } = new List<TUCard>();

   public virtual ICollection<TULead> TULead { get; set; } = new List<TULead>();

   public virtual ICollection<TULeadActivity> TULeadActivity { get; set; } = new List<TULeadActivity>();

   public virtual ICollection<TULeadGroup> TULeadGroup { get; set; } = new List<TULeadGroup>();

   public virtual ICollection<TULeadOwner> TULeadOwner { get; set; } = new List<TULeadOwner>();

   public virtual ICollection<TUProductCode> TUProductCodeStatus { get; set; } = new List<TUProductCode>();

   public virtual ICollection<TUProductCode> TUProductCodeType { get; set; } = new List<TUProductCode>();

   public virtual ICollection<TUProduct> TUProductRewardType { get; set; } = new List<TUProduct>();

   public virtual ICollection<TUProduct> TUProductStatus { get; set; } = new List<TUProduct>();

   public virtual ICollection<TUProduct> TUProductType { get; set; } = new List<TUProduct>();

   public virtual ICollection<VASCategory> VASCategory { get; set; } = new List<VASCategory>();

   public virtual ICollection<VASProduct> VASProduct { get; set; } = new List<VASProduct>();

   public virtual ICollection<VASProductItem> VASProductItem { get; set; } = new List<VASProductItem>();

   public virtual ICollection<VasPayment> VasPaymentStatus { get; set; } = new List<VasPayment>();

   public virtual ICollection<VasPayment> VasPaymentType { get; set; } = new List<VasPayment>();

   public virtual ICollection<cmt_loyalty_customer> cmt_loyalty_customersource { get; set; } = new List<cmt_loyalty_customer>();

   public virtual ICollection<cmt_loyalty_customer> cmt_loyalty_customerstatus { get; set; } = new List<cmt_loyalty_customer>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltyloyalty_type { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltypayment_mode { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltysource { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltystatus { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_loyalty> cmt_loyaltytype { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedpayment_mode { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedsource { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedstatus { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failedtype { get; set; } = new List<cmt_sale_failed>();

   public virtual ICollection<cmt_sale> cmt_salepayment_mode { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salesource { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_salestatus { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale> cmt_saletype { get; set; } = new List<cmt_sale>();
}
