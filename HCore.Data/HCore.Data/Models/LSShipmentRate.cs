﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSShipmentRate
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public string? RateId { get; set; }

    public double? Amount { get; set; }

    public long? CarrierId { get; set; }

    public string? Currency { get; set; }

    public DateTime? DeliveryDate { get; set; }

    public string? DeliveryTime { get; set; }

    public double? InsuranceFee { get; set; }

    public string? PickUpTime { get; set; }

    public string? PickUpAddress { get; set; }

    public string? DeliveryAddress { get; set; }

    public long? ShipmentId { get; set; }

    public DateTime? CreateDate { get; set; }

    public virtual LSCarriers? Carrier { get; set; }

    public virtual LSShipments? Shipment { get; set; }
}
