﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealBookmark
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long DealId { get; set; }

    public long AccountId { get; set; }

    public DateTime CreateDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
