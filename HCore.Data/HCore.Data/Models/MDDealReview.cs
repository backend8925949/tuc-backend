﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealReview
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long DealId { get; set; }

    public long? DealCodeId { get; set; }

    public int? Rating { get; set; }

    public int? IsWorking { get; set; }

    public string? Review { get; set; }

    public string? SystemComment { get; set; }

    public string? Comment { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual MDDealCode? DealCode { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
