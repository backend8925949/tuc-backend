﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreStorageTag
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long StorageId { get; set; }

    public string? Key { get; set; }

    public string? Value { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreStorage Storage { get; set; } = null!;
}
