﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSOperations
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long AccountId { get; set; }

    public long DealId { get; set; }

    public string? RateId { get; set; }

    public long? ShipmentId { get; set; }

    public DateTime? CreateDate { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual LSShipments? Shipment { get; set; }
}
