﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCCategory
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ParentId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public double? PlatformCharge { get; set; }

    public long? PrimaryStorageId { get; set; }

    public long? SecondaryStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<SCCategory> InverseParent { get; set; } = new List<SCCategory>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual SCCategory? Parent { get; set; }

    public virtual HCCoreStorage? PrimaryStorage { get; set; }

   public virtual ICollection<SCProduct> SCProduct { get; set; } = new List<SCProduct>();

    public virtual HCCoreStorage? SecondaryStorage { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
