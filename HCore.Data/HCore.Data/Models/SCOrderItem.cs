﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCOrderItem
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrderId { get; set; }

    public long VarientId { get; set; }

    public double UnitPrice { get; set; }

    public int Quantity { get; set; }

    public double? Amount { get; set; }

    public double? DiscountAmount { get; set; }

    public double TotalAmount { get; set; }

    public double RewardAmount { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual SCOrder Order { get; set; } = null!;

   public virtual ICollection<SCOrderActivity> SCOrderActivity { get; set; } = new List<SCOrderActivity>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual SCProductVarient Varient { get; set; } = null!;
}
