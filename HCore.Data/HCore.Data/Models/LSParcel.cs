﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class LSParcel
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? PackagingId { get; set; }

    public string? Description { get; set; }

    public double? TotalWeight { get; set; }

    public string? WeightUnit { get; set; }

    public string? ParcelId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public string? TId { get; set; }

    public int? StatusId { get; set; }

    public long? DealId { get; set; }

    public long? AccountId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual MDDeal? Deal { get; set; }

   public virtual ICollection<LSItems> LSItems { get; set; } = new List<LSItems>();

   public virtual ICollection<LSShipments> LSShipments { get; set; } = new List<LSShipments>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual LSPackages? Packaging { get; set; }

    public virtual HCCore? Status { get; set; }
}
