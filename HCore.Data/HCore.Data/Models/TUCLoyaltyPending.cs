﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCLoyaltyPending
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public DateTime TransactionDate { get; set; }

    public long? TransactionId { get; set; }

    public int LoyaltyTypeId { get; set; }

    public int TypeId { get; set; }

    public int ModeId { get; set; }

    public int SourceId { get; set; }

    public long FromAccountId { get; set; }

    public long ToAccountId { get; set; }

    public double Amount { get; set; }

    public double CommissionAmount { get; set; }

    public double TotalAmount { get; set; }

    public double InvoiceAmount { get; set; }

    public double LoyaltyInvoiceAmount { get; set; }

    public double Balance { get; set; }

    public long CustomerId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public long? TerminalId { get; set; }

    public long? ProviderId { get; set; }

    public long? AcquirerId { get; set; }

    public string? InvoiceNumber { get; set; }

    public string? AccountNumber { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime? SyncDate { get; set; }

    public virtual HCUAccount? Acquirer { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount Customer { get; set; } = null!;

    public virtual HCUAccount FromAccount { get; set; } = null!;

    public virtual HCCore LoyaltyType { get; set; } = null!;

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCCore Mode { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Provider { get; set; }

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }

    public virtual TUCTerminal? Terminal { get; set; }

    public virtual HCUAccount ToAccount { get; set; } = null!;

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore Type { get; set; } = null!;
}
