﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCLProgram
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Name { get; set; } = null!;

    public long AccountId { get; set; }

    public int? ProgramTypeId { get; set; }

    public long? SubscriptionId { get; set; }

    public int ComissionTypeId { get; set; }

    public double Comission { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public double? RewardPercentage { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public double? MinInvoiceAmount { get; set; }

    public double? MaxInvoiceAmount { get; set; }

    public double? MaxRewards { get; set; }

    public double? MaxComission { get; set; }

    public double CashierCommission { get; set; }

    public double CashierMaxReward { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCCore ComissionType { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCUAccountBalance> HCUAccountBalance { get; set; } = new List<HCUAccountBalance>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransaction { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? ProgramType { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCSubscription? Subscription { get; set; }

   public virtual ICollection<TUCLProgramGroup> TUCLProgramGroup { get; set; } = new List<TUCLProgramGroup>();

   public virtual ICollection<TUCLoyaltyMerchantCustomer> TUCLoyaltyMerchantCustomer { get; set; } = new List<TUCLoyaltyMerchantCustomer>();
}
