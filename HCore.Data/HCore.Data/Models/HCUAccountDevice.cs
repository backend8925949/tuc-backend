﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountDevice
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? TypeId { get; set; }

    public long AccountId { get; set; }

    public string SerialNumber { get; set; } = null!;

    public long? OsVersionId { get; set; }

    public long AppVersionId { get; set; }

    public long? ModelId { get; set; }

    public long? ResolutionId { get; set; }

    public long? NetworkOperatorId { get; set; }

    public string? NotificationUrl { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCUAccountActivity> HCUAccountActivity { get; set; } = new List<HCUAccountActivity>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocation { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameter { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountSession> HCUAccountSession { get; set; } = new List<HCUAccountSession>();

    public virtual HCCoreParameter? Model { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreParameter? NetworkOperator { get; set; }

    public virtual HCCoreParameter? OsVersion { get; set; }

    public virtual HCCoreParameter? Resolution { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? Type { get; set; }
}
