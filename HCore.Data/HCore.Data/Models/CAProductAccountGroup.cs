﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProductAccountGroup
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public long? ParentId { get; set; }

    public long? ProductId { get; set; }

    public long? SubProductId { get; set; }

    public long? OwnerId { get; set; }

    public long? AccountId { get; set; }

    public string? DisplayName { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? MobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public int? GenderId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCCore? Gender { get; set; }

   public virtual ICollection<CAProductAccountGroup> InverseParent { get; set; } = new List<CAProductAccountGroup>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Owner { get; set; }

    public virtual CAProductAccountGroup? Parent { get; set; }

    public virtual CAProduct? Product { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual CAProduct? SubProduct { get; set; }

    public virtual HCCore? SubType { get; set; }

    public virtual HCCore Type { get; set; } = null!;
}
