﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class VasPayment
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ProductItemId { get; set; }

    public long AccountId { get; set; }

    public string AccountNumber { get; set; } = null!;

    public double? Amount { get; set; }

    public double? RewardAmount { get; set; }

    public double? UserRewardAmount { get; set; }

    public double? CommissionAmount { get; set; }

    public string? PaymentReference { get; set; }

    public string? PaymentSource { get; set; }

    public string? PaymentToken { get; set; }

    public string? BillerName { get; set; }

    public string? PackageName { get; set; }

    public string? Request { get; set; }

    public string? Response { get; set; }

    public string? ResponseMessage { get; set; }

    public string? OrderId { get; set; }

    public string? Comment { get; set; }

    public int? TypeId { get; set; }

    public long? TransactionId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public long? ProductId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual VASProduct? Product { get; set; }

    public virtual VASProductItem? ProductItem { get; set; }

    public virtual HCCore? Status { get; set; }

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore? Type { get; set; }
}
