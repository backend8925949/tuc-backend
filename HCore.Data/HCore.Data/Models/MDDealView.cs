﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealView
{
    public long Id { get; set; }

    public long DealId { get; set; }

    public long CustomerId { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public DateTime CreateDate { get; set; }

    public int? PlatformId { get; set; }

    public virtual HCUAccount Customer { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual HCCore? Platform { get; set; }
}
