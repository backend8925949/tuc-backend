﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class ReferralSettings
{
    public int Id { get; set; }

    public decimal? Amount { get; set; }

    public string? SetBy { get; set; }

    public int? Status { get; set; }

    public DateTime? DateSet { get; set; }
}
