﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCConfiguration
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? DataTypeId { get; set; }

    public string? Name { get; set; }

    public string Value { get; set; } = null!;

    public long? AccountId { get; set; }

    public long? ParentId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string? Description { get; set; }

    public string? Comment { get; set; }

    public long? LevelId { get; set; }

    public long? ImageStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCCore? DataType { get; set; }

    public virtual HCCoreStorage? ImageStorage { get; set; }

   public virtual ICollection<SCConfiguration> InverseParent { get; set; } = new List<SCConfiguration>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual SCConfiguration? Parent { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
