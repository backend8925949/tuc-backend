﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreAppVersion
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AppId { get; set; }

    public string VersionId { get; set; } = null!;

    public string ApiKey { get; set; } = null!;

    public string PublicKey { get; set; } = null!;

    public string PrivateKey { get; set; } = null!;

    public string SystemPublicKey { get; set; } = null!;

    public string SystemPrivateKey { get; set; } = null!;

    public long RequestCount { get; set; }

    public DateTime? LastRequestTime { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCCoreApp App { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCCoreAppVersionToken> HCCoreAppVersionToken { get; set; } = new List<HCCoreAppVersionToken>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
