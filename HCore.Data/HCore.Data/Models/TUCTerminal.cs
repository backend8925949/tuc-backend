﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCTerminal
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string IdentificationNumber { get; set; } = null!;

    public string? SerialNumber { get; set; }

    public string? DisplayName { get; set; }

    public long? AccountId { get; set; }

    public long? MerchantId { get; set; }

    public long? MerchantRmId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public long? ProviderId { get; set; }

    public long? ProviderRmId { get; set; }

    public long? AcquirerId { get; set; }

    public long? AcquirerBranchId { get; set; }

    public long? AcquirerRmId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? LastActivityDate { get; set; }

    public DateTime? LastTransactionDate { get; set; }

    public int? ActivityStatusId { get; set; }

    public long? ReceiptStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public int? TypeId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? Acquirer { get; set; }

    public virtual TUCBranch? AcquirerBranch { get; set; }

    public virtual HCUAccount? AcquirerRm { get; set; }

    public virtual HCCore? ActivityStatus { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransaction { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? MerchantRm { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Provider { get; set; }

    public virtual HCUAccount? ProviderRm { get; set; }

    public virtual HCCoreStorage? ReceiptStorage { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }

   public virtual ICollection<TUCLoyalty> TUCLoyalty { get; set; } = new List<TUCLoyalty>();

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPending { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCSale> TUCSale { get; set; } = new List<TUCSale>();

    public virtual HCCore? Type { get; set; }

   public virtual ICollection<cmt_loyalty> cmt_loyalty { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_sale> cmt_sale { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failed { get; set; } = new List<cmt_sale_failed>();
}
