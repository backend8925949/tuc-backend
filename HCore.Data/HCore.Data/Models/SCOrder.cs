﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCOrder
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string OrderId { get; set; } = null!;

    public DateTime? OpenDate { get; set; }

    public DateTime? CloseDate { get; set; }

    public long? CustomerId { get; set; }

    public int TotalItem { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double OtherCharge { get; set; }

    public double DeliveryCharge { get; set; }

    public double DiscountAmount { get; set; }

    public double? Commission { get; set; }

    public double TotalAmount { get; set; }

    public int? PaymentModeId { get; set; }

    public int? PaymentStatusId { get; set; }

    public string? PaymentReference { get; set; }

    public double RewardAmount { get; set; }

    public double? RewardUserAmount { get; set; }

    public double? RewardSystemAmount { get; set; }

    public long? DealerId { get; set; }

    public long? DealerLocationId { get; set; }

    public int? DealerLocationRating { get; set; }

    public string? DealerLocationReview { get; set; }

    public DateTime? ExpectedDeliveryDate { get; set; }

    public DateTime? ActualDeliveryDate { get; set; }

    public int? DeliveryTypeId { get; set; }

    public int? DeliveryModeId { get; set; }

    public int? PriorityId { get; set; }

    public int? CustomerRating { get; set; }

    public string? CustomerReview { get; set; }

    public long? RiderId { get; set; }

    public int? RiderRating { get; set; }

    public string? RiderReview { get; set; }

    public double? RiderAmount { get; set; }

    public double? RiderCommissionAmount { get; set; }

    public double? RiderTotalAmount { get; set; }

    public double? OrderLatitude { get; set; }

    public double? OrderLongitude { get; set; }

    public string? OrderDeliveryCode { get; set; }

    public int? EPTime { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? Customer { get; set; }

    public virtual HCUAccount? Dealer { get; set; }

    public virtual HCUAccount? DealerLocation { get; set; }

    public virtual HCCore? DeliveryMode { get; set; }

    public virtual HCCore? DeliveryType { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? PaymentMode { get; set; }

    public virtual HCCore? PaymentStatus { get; set; }

    public virtual HCCore? Priority { get; set; }

    public virtual HCUAccount? Rider { get; set; }

   public virtual ICollection<SCOrderActivity> SCOrderActivity { get; set; } = new List<SCOrderActivity>();

   public virtual ICollection<SCOrderAddress> SCOrderAddress { get; set; } = new List<SCOrderAddress>();

   public virtual ICollection<SCOrderItem> SCOrderItem { get; set; } = new List<SCOrderItem>();

    public virtual HCCore Status { get; set; } = null!;
}
