﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountLocation
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long? DeviceId { get; set; }

    public string? Location { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public long? CountryId { get; set; }

    public long? RegionId { get; set; }

    public long? RegionAreaId { get; set; }

    public long? CityId { get; set; }

    public long? CityAreaId { get; set; }

    public DateTime? CreateDate { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCCoreParameter? City { get; set; }

    public virtual HCCoreParameter? CityArea { get; set; }

    public virtual HCCoreCommon? Country { get; set; }

    public virtual HCUAccountDevice? Device { get; set; }

    public virtual HCCoreParameter? Region { get; set; }

    public virtual HCCoreParameter? RegionArea { get; set; }
}
