﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreFeature
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Name { get; set; }

    public int TypeId { get; set; }

    public int HelperId { get; set; }

    public long? ParentId { get; set; }

    public string? SystemName { get; set; }

    public string? Add { get; set; }

    public string? Edit { get; set; }

    public string? Delete { get; set; }

    public string? View { get; set; }

    public bool? isTab { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public string? ViewAll { get; set; }

   public virtual ICollection<HCCoreRoleFeature> HCCoreRoleFeature { get; set; } = new List<HCCoreRoleFeature>();

    public virtual HCCore Helper { get; set; } = null!;

   public virtual ICollection<HCCoreFeature> InverseParent { get; set; } = new List<HCCoreFeature>();

    public virtual HCCoreFeature? Parent { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore Type { get; set; } = null!;
}
