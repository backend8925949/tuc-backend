﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCSubscriptionFeature
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long SubscriptionId { get; set; }

    public string Name { get; set; } = null!;

    public string? ShortDescription { get; set; }

    public string? Description { get; set; }

    public string? Policy { get; set; }

    public long? MinimumLimit { get; set; }

    public long? MaximumLimit { get; set; }

    public double? MinimumAmount { get; set; }

    public double? MaximumAmount { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCSubscriptionFeatureItem> HCSubscriptionFeatureItem { get; set; } = new List<HCSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeature { get; set; } = new List<HCUAccountSubscriptionFeature>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCSubscription Subscription { get; set; } = null!;
}
