﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCoreCustomerObj
{
    public long id { get; set; }

    public string? accountStatus { get; set; }

    public string? emailAddress { get; set; }

    public string? phoneNumber { get; set; }

    public string? accountName { get; set; }

    public string? accountNumber { get; set; }

    public string? cbaCustomerID { get; set; }

    public string? responseMessage { get; set; }

    public string? availableBalance { get; set; }

    public string? responseCode { get; set; }
}
