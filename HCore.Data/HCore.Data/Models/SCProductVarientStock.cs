﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCProductVarientStock
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? SubAccountId { get; set; }

    public long VarientId { get; set; }

    public long Quantity { get; set; }

    public long MinimumQuantity { get; set; }

    public long MaximumQuantity { get; set; }

    public double ActualPrice { get; set; }

    public double SellingPrice { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? SubAccount { get; set; }

    public virtual SCProductVarient Varient { get; set; } = null!;
}
