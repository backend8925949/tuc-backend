﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSCampaignGroup
{
    public long Id { get; set; }

    public long CampaignId { get; set; }

    public long? GroupId { get; set; }

    public int SourceId { get; set; }

    public string? Title { get; set; }

    public string? Condition { get; set; }

    public string? FileName { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual SMSCampaign Campaign { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual SMSGroup? Group { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

   public virtual ICollection<SMSCampaignGroupCondition> SMSCampaignGroupCondition { get; set; } = new List<SMSCampaignGroupCondition>();

   public virtual ICollection<SMSCampaignGroupItem> SMSCampaignGroupItem { get; set; } = new List<SMSCampaignGroupItem>();

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
