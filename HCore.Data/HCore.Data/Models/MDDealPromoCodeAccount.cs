﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealPromoCodeAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long PromoCodeId { get; set; }

    public long AccountId { get; set; }

    public DateTime UseDate { get; set; }

    public DateTime? SyncTime { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

   public virtual ICollection<MDDealCode> MDDealCode { get; set; } = new List<MDDealCode>();

    public virtual MDDealPromoCode PromoCode { get; set; } = null!;
}
