﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCTransaction
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long UserAccountId { get; set; }

    public long ModeId { get; set; }

    public long TypeId { get; set; }

    public long SourceId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double ComissionAmount { get; set; }

    public double TotalAmount { get; set; }

    public double Balance { get; set; }

    public string Comment { get; set; } = null!;

    public string? ReferenceNumber { get; set; }

    public long? InvoiceItemId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public string? StatusMessage { get; set; }

    public long StatusId { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccountInvoice? InvoiceItem { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount UserAccount { get; set; } = null!;
}
