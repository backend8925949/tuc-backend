﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLPlan
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public string? Policy { get; set; }

    public int Tenture { get; set; }

    public double MinimumLoanAmount { get; set; }

    public double CustomerInterestRate { get; set; }

    public double ProviderInterestRate { get; set; }

    public double SystemInterestRate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long? PlanGracePeriod { get; set; }

    public sbyte? ExcludeWeekends { get; set; }

    public string? TenuredIn { get; set; }

    public string? Frequency { get; set; }

   public virtual ICollection<BNPLAccountLoan> BNPLAccountLoan { get; set; } = new List<BNPLAccountLoan>();

   public virtual ICollection<BNPLLoanProcess> BNPLLoanProcess { get; set; } = new List<BNPLLoanProcess>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
