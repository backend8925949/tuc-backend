﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCTerminalProduct
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Sku { get; set; } = null!;

    public string ReferenceNumber { get; set; } = null!;

    public long AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public string? CategoryName { get; set; }

    public string? SubCategoryName { get; set; }

    public double ActualPrice { get; set; }

    public double SellingPrice { get; set; }

    public long TotalStock { get; set; }

    public double RewardPercentage { get; set; }

    public long AvailableStock { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
