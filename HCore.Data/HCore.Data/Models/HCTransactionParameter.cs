﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCTransactionParameter
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public DateTime TransactionDate { get; set; }

    public long TypeId { get; set; }

    public long SourceId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double CommissionAmount { get; set; }

    public double TotalAmount { get; set; }

    public long TotalAmountPercentage { get; set; }

    public double InvoiceAmount { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? AccountNumber { get; set; }

    public string? TCode { get; set; }

    public string? Comment { get; set; }

    public string? RequestKey { get; set; }

    public long? TucCardId { get; set; }

    public long? BinNumberId { get; set; }

    public long? CampaignId { get; set; }

    public long? CustomerId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public long? TerminalId { get; set; }

    public long? ProviderId { get; set; }

    public long? BankId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreateDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public string? StatusMessage { get; set; }

    public long StatusId { get; set; }

    public virtual HCUAccount? Bank { get; set; }

    public virtual TUCampaign? Campaign { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? Customer { get; set; }

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Provider { get; set; }

    public virtual HCUAccount? Store { get; set; }

    public virtual HCUAccount? Terminal { get; set; }
}
