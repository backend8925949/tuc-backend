﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreCountry
{
    public int Id { get; set; }

    public string? Guid { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string Isd { get; set; } = null!;

    public string Iso { get; set; } = null!;

    public string? CurrencyName { get; set; }

    public string? CurrencyNotation { get; set; }

    public string? CurrencySymbol { get; set; }

    public string? CurrencyHex { get; set; }

    public string? CapitalName { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public int MobileNumberLength { get; set; }

    public string? TimeZoneName { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCCoreAddress> HCCoreAddress { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumber { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreCommon> HCCoreCommon { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCountryState> HCCoreCountryState { get; set; } = new List<HCCoreCountryState>();

   public virtual ICollection<HCSubscription> HCSubscription { get; set; } = new List<HCSubscription>();

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

   public virtual ICollection<MDDealPromotion> MDDealPromotion { get; set; } = new List<MDDealPromotion>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCAppPromotion> TUCAppPromotion { get; set; } = new List<TUCAppPromotion>();

   public virtual ICollection<TUCBranch> TUCBranch { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistration { get; set; } = new List<TUCNinjaRegistration>();

   public virtual ICollection<TUCPromoCode> TUCPromoCode { get; set; } = new List<TUCPromoCode>();

   public virtual ICollection<TUCPromoCodeCondition> TUCPromoCodeCondition { get; set; } = new List<TUCPromoCodeCondition>();
}
