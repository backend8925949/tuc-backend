﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProductCode
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public long ProductId { get; set; }

    public long? SubProductId { get; set; }

    public long? AccountId { get; set; }

    public string ItemCode { get; set; } = null!;

    public double? ItemAmount { get; set; }

    public double? AvailableAmount { get; set; }

    public string? ItemPin { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public long? UseCount { get; set; }

    public DateTime? LastUseDate { get; set; }

    public long? LastUseLocationId { get; set; }

    public int? UseAttempts { get; set; }

    public string? Comment { get; set; }

    public string? Message { get; set; }

    public long? TransactionId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

   public virtual ICollection<CAProductUseHistory> CAProductUseHistory { get; set; } = new List<CAProductUseHistory>();

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual CAProductLocation? LastUseLocation { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual CAProduct Product { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual CAProduct? SubProduct { get; set; }

    public virtual HCCore? SubType { get; set; }

    public virtual HCUAccountTransaction? Transaction { get; set; }

    public virtual HCCore Type { get; set; } = null!;
}
