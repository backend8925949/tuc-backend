﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountTransaction
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public long? ParentTransactionId { get; set; }

    public long? AccountId { get; set; }

    public int ModeId { get; set; }

    public int TypeId { get; set; }

    public int SourceId { get; set; }

    public double Amount { get; set; }

    public double Charge { get; set; }

    public double ComissionAmount { get; set; }

    public double TotalAmount { get; set; }

    public double TotalAmountPercentage { get; set; }

    public double Balance { get; set; }

    public double PurchaseAmount { get; set; }

    public double? ReferenceAmount { get; set; }

    public double? ReferenceInvoiceAmount { get; set; }

    public DateTime TransactionDate { get; set; }

    public string? InoviceNumber { get; set; }

    public string? AccountNumber { get; set; }

    public string? ReferenceNumber { get; set; }

    public string? Comment { get; set; }

    public long? BankId { get; set; }

    public long? ProviderId { get; set; }

    public long? CardBrandId { get; set; }

    public long? CardSubBrandId { get; set; }

    public long? CardTypeId { get; set; }

    public long? CardBankId { get; set; }

    public string? TCode { get; set; }

    public string? RequestKey { get; set; }

    public string? GroupKey { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long? CustomerId { get; set; }

    public long? CampaignId { get; set; }

    public long? CashierId { get; set; }

    public int? BinNumberId { get; set; }

    public int? PaymentMethodId { get; set; }

    public long? TerminalId { get; set; }

    public long? RedeemFrom { get; set; }

    public long? ProgramId { get; set; }

    public double? CashierReward { get; set; }

    public string? TransHash { get; set; }

    public int? RedeemMethod { get; set; }

    public virtual HCUAccount? Account { get; set; }

   public virtual ICollection<BNPLMerchantSettlement> BNPLMerchantSettlement { get; set; } = new List<BNPLMerchantSettlement>();

    public virtual HCUAccount? Bank { get; set; }

    public virtual HCCoreBinNumber? BinNumber { get; set; }

   public virtual ICollection<CAProductCode> CAProductCode { get; set; } = new List<CAProductCode>();

    public virtual TUCampaign? Campaign { get; set; }

    public virtual HCCoreCommon? CardBank { get; set; }

    public virtual HCCoreCommon? CardBrand { get; set; }

    public virtual HCCoreCommon? CardSubBrand { get; set; }

    public virtual HCCoreCommon? CardType { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? Customer { get; set; }

   public virtual ICollection<HCUAccountParameter> HCUAccountParameter { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountTransaction> InverseParentTransaction { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<MDDealCode> MDDealCode { get; set; } = new List<MDDealCode>();

    public virtual HCCore Mode { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Parent { get; set; }

    public virtual HCUAccountTransaction? ParentTransaction { get; set; }

    public virtual HCCore? PaymentMethod { get; set; }

    public virtual TUCLProgram? Program { get; set; }

    public virtual HCUAccount? Provider { get; set; }

    public virtual HCCore Source { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? SubParent { get; set; }

   public virtual ICollection<TUCLoyaltyPending> TUCLoyaltyPending { get; set; } = new List<TUCLoyaltyPending>();

   public virtual ICollection<TUCSale> TUCSale { get; set; } = new List<TUCSale>();

   public virtual ICollection<TUProductCode> TUProductCode { get; set; } = new List<TUProductCode>();

    public virtual TUCTerminal? Terminal { get; set; }

    public virtual HCCore Type { get; set; } = null!;

   public virtual ICollection<VasPayment> VasPayment { get; set; } = new List<VasPayment>();

   public virtual ICollection<cmt_loyalty> cmt_loyalty { get; set; } = new List<cmt_loyalty>();

   public virtual ICollection<cmt_sale> cmt_sale { get; set; } = new List<cmt_sale>();

   public virtual ICollection<cmt_sale_failed> cmt_sale_failed { get; set; } = new List<cmt_sale_failed>();
}
