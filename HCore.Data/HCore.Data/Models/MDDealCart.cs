﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealCart
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long DealId { get; set; }

    public long? AccountId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public int? ItemCount { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }
}
