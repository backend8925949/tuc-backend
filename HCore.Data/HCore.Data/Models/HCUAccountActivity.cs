﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCUAccountActivity
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public long? DeviceId { get; set; }

    public long? FeatureId { get; set; }

    public int ActivityTypeId { get; set; }

    public string? ReferenceKey { get; set; }

    public string? SubReferenceKey { get; set; }

    public string? EntityName { get; set; }

    public string? FieldName { get; set; }

    public string? OldValue { get; set; }

    public string? NewValue { get; set; }

    public string? Description { get; set; }

    public DateTime CreateDate { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCore ActivityType { get; set; } = null!;

    public virtual HCUAccountDevice? Device { get; set; }

    public virtual HCCoreCommon? Feature { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
