﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreRole
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public int? HelperId { get; set; }

    public long? CommonId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual HCCoreCommon? Common { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCCoreRoleFeature> HCCoreRoleFeature { get; set; } = new List<HCCoreRoleFeature>();

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

    public virtual HCCore? Helper { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;
}
