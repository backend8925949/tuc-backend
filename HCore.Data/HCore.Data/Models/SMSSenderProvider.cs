﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSSenderProvider
{
    public int Id { get; set; }

    public string SenderId { get; set; } = null!;

    public string ProviderName { get; set; } = null!;

    public int StatusId { get; set; }

   public virtual ICollection<SMSCampaign> SMSCampaign { get; set; } = new List<SMSCampaign>();

    public virtual HCCore Status { get; set; } = null!;
}
