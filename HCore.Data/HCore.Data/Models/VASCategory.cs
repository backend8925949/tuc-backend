﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class VASCategory
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public long? ReferenceId { get; set; }

    public string? ReferenceKey { get; set; }

    public long? IconStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public long CountryId { get; set; }

    public virtual HCCoreCommon Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<VASProduct> VASProduct { get; set; } = new List<VASProduct>();
}
