﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SCProduct
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public long? CategoryId { get; set; }

    public int? IsInventoryEnabled { get; set; }

    public long? PrimaryStorageId { get; set; }

    public double? RewardPercentage { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual SCCategory? Category { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreStorage? PrimaryStorage { get; set; }

   public virtual ICollection<SCProductVarient> SCProductVarient { get; set; } = new List<SCProductVarient>();

    public virtual HCCore Status { get; set; } = null!;
}
