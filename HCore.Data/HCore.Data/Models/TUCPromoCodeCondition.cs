﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCPromoCodeCondition
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public int CountryId { get; set; }

    public int? AccountTypeId { get; set; }

    public string Name { get; set; } = null!;

    public string? SystemName { get; set; }

    public string? Description { get; set; }

    public double Value { get; set; }

    public double MinimumValue { get; set; }

    public double MaximumValue { get; set; }

    public int? HelperId { get; set; }

    public int StatusId { get; set; }

    public virtual HCCore? AccountType { get; set; }

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCCore? Helper { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCPromoCode> TUCPromoCode { get; set; } = new List<TUCPromoCode>();
}
