﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCampaign
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public double? SubTypeValue { get; set; }

    public long AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public int? SubTypeId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public double? MinimumInvoiceAmount { get; set; }

    public double? MaximumInvoiceAmount { get; set; }

    public double? MinimumRewardAmount { get; set; }

    public double? MaximumRewardAmount { get; set; }

    public int? CustomAudience { get; set; }

    public double? Credit { get; set; }

    public double? Debit { get; set; }

    public double? Balance { get; set; }

    public long? ManagerId { get; set; }

    public string? SmsText { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCTransactionParameter> HCTransactionParameter { get; set; } = new List<HCTransactionParameter>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransaction { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCUAccount? Manager { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? SubType { get; set; }

   public virtual ICollection<TUCampaignAudience> TUCampaignAudience { get; set; } = new List<TUCampaignAudience>();

    public virtual HCCore Type { get; set; } = null!;
}
