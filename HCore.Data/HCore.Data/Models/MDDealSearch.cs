﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealSearch
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string SearchText { get; set; } = null!;

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }
}
