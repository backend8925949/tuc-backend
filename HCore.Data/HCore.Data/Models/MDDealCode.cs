﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealCode
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long DealId { get; set; }

    public long? AccountId { get; set; }

    public string ItemCode { get; set; } = null!;

    public string? ItemPin { get; set; }

    public double? ItemAmount { get; set; }

    public double? AvailableAmount { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public long? UseCount { get; set; }

    public DateTime? LastUseDate { get; set; }

    public long? LastUseLocationId { get; set; }

    public int? LastUseSource { get; set; }

    public int? UseAttempts { get; set; }

    public string? Message { get; set; }

    public string? Comment { get; set; }

    public long? TransactionId { get; set; }

    public long? CashierId { get; set; }

    public long? TerminalId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public long? SecondaryAccountId { get; set; }

    public string? SecondaryCode { get; set; }

    public string? SecondaryMessage { get; set; }

    public double? Amount { get; set; }

    public double? Charge { get; set; }

    public double? DeliveryCharge { get; set; }

    public int? ItemCount { get; set; }

    public double? TotalAmount { get; set; }

    public double? ComissionAmount { get; set; }

    public double? PartnerComissionAmount { get; set; }

    public long? FromAddressId { get; set; }

    public long? ToAddressId { get; set; }

    public string? ReferenceNumber { get; set; }

    public long? PromoCodeId { get; set; }

    public long? OrderId { get; set; }

    public long? PartnerId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual MDDeal Deal { get; set; } = null!;

    public virtual HCCoreAddress? FromAddress { get; set; }

    public virtual HCUAccount? LastUseLocation { get; set; }

    public virtual HCCore? LastUseSourceNavigation { get; set; }

   public virtual ICollection<MDDealReview> MDDealReview { get; set; } = new List<MDDealReview>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual LSShipments? Order { get; set; }

    public virtual HCUAccount? Partner { get; set; }

    public virtual MDDealPromoCodeAccount? PromoCode { get; set; }

    public virtual HCUAccount? SecondaryAccount { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Terminal { get; set; }

    public virtual HCCoreAddress? ToAddress { get; set; }

    public virtual HCUAccountTransaction? Transaction { get; set; }
}
