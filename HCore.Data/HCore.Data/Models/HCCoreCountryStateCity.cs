﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreCountryStateCity
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long StateId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? IsoCode { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public string? OldName { get; set; }

    public int? IsExternal { get; set; }

    public virtual HCUAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<HCCoreAddress> HCCoreAddress { get; set; } = new List<HCCoreAddress>();

   public virtual ICollection<HCCoreCountryStateCityArea> HCCoreCountryStateCityArea { get; set; } = new List<HCCoreCountryStateCityArea>();

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreCountryState State { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCBranch> TUCBranch { get; set; } = new List<TUCBranch>();

   public virtual ICollection<TUCNinjaRegistration> TUCNinjaRegistration { get; set; } = new List<TUCNinjaRegistration>();
}
