﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCard
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string CardNumber { get; set; } = null!;

    public string SerialNumber { get; set; } = null!;

    public int CardTypeId { get; set; }

    public int CardTagTypeId { get; set; }

    public int PrepaidCardTypeId { get; set; }

    public double PrepaidPoints { get; set; }

    public long ActiveMerchantId { get; set; }

    public long? ActiveUserAccountId { get; set; }

    public string? ReferenceNumber { get; set; }

    public DateTime? ExpiaryDate { get; set; }

    public DateTime? LastActivity { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyBy { get; set; }

    public int StatusId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public virtual HCUAccount ActiveMerchant { get; set; } = null!;

    public virtual HCUAccount? ActiveUserAccount { get; set; }

    public virtual HCCore CardTagType { get; set; } = null!;

    public virtual HCCore CardType { get; set; } = null!;

    public virtual HCUAccount? Cashier { get; set; }

   public virtual ICollection<HCUAccount> HCUAccount { get; set; } = new List<HCUAccount>();

    public virtual HCCore PrepaidCardType { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }
}
