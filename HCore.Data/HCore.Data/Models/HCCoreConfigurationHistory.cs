﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreConfigurationHistory
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int ConfigurationId { get; set; }

    public string Value { get; set; } = null!;

    public int? ValueHelperId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCCoreConfiguration Configuration { get; set; } = null!;

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCore? ValueHelper { get; set; }
}
