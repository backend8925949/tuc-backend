﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class SMSCampaignGroupCondition
{
    public long Id { get; set; }

    public long CampaignGroupId { get; set; }

    public string Name { get; set; } = null!;

    public string Value { get; set; } = null!;

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public virtual SMSCampaignGroup CampaignGroup { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;
}
