﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreCommon
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int TypeId { get; set; }

    public int? SubTypeId { get; set; }

    public int? HelperId { get; set; }

    public long? AccountId { get; set; }

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public string? Value { get; set; }

    public string? SubValue { get; set; }

    public string? Data { get; set; }

    public string? Description { get; set; }

    public long? Sequence { get; set; }

    public long? Count { get; set; }

    public long? IconStorageId { get; set; }

    public long? PosterStorageId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public int? CountryId { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual HCCoreCountry? Country { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumberBank { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumberBrand { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumberSubBrand { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreBinNumber> HCCoreBinNumberType { get; set; } = new List<HCCoreBinNumber>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterCommon { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreParameter> HCCoreParameterSubCommon { get; set; } = new List<HCCoreParameter>();

   public virtual ICollection<HCCoreRole> HCCoreRole { get; set; } = new List<HCCoreRole>();

   public virtual ICollection<HCSubscriptionFeatureItem> HCSubscriptionFeatureItem { get; set; } = new List<HCSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccountActivity> HCUAccountActivity { get; set; } = new List<HCUAccountActivity>();

   public virtual ICollection<HCUAccount> HCUAccountAppVersion { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountLocation> HCUAccountLocation { get; set; } = new List<HCUAccountLocation>();

   public virtual ICollection<HCUAccountParameter> HCUAccountParameter { get; set; } = new List<HCUAccountParameter>();

   public virtual ICollection<HCUAccountSubscriptionFeatureItem> HCUAccountSubscriptionFeatureItem { get; set; } = new List<HCUAccountSubscriptionFeatureItem>();

   public virtual ICollection<HCUAccount> HCUAccountTimeZone { get; set; } = new List<HCUAccount>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCardBank { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCardBrand { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCardSubBrand { get; set; } = new List<HCUAccountTransaction>();

   public virtual ICollection<HCUAccountTransaction> HCUAccountTransactionCardType { get; set; } = new List<HCUAccountTransaction>();

    public virtual HCCore? Helper { get; set; }

    public virtual HCCoreStorage? IconStorage { get; set; }

   public virtual ICollection<HCCoreCommon> InverseParent { get; set; } = new List<HCCoreCommon>();

   public virtual ICollection<HCCoreCommon> InverseSubParent { get; set; } = new List<HCCoreCommon>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCoreCommon? Parent { get; set; }

    public virtual HCCoreStorage? PosterStorage { get; set; }

   public virtual ICollection<SCOrderAddress> SCOrderAddress { get; set; } = new List<SCOrderAddress>();

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCCoreCommon? SubParent { get; set; }

    public virtual HCCore? SubType { get; set; }

    public virtual HCCore Type { get; set; } = null!;

   public virtual ICollection<VASCategory> VASCategory { get; set; } = new List<VASCategory>();
}
