﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCReceiptScan
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long UserAccountId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long? CashierId { get; set; }

    public string? InoviceNumber { get; set; }

    public double? InoviceAmount { get; set; }

    public long? InoviceStorageId { get; set; }

    public double? ApprovedAmount { get; set; }

    public double? UserAmount { get; set; }

    public double? CommissionAmount { get; set; }

    public double? TotalAmount { get; set; }

    public string? Comment { get; set; }

    public DateTime CreateDate { get; set; }

    public long? CreatedBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyBy { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount? Cashier { get; set; }

    public virtual HCUAccount? CreatedByNavigation { get; set; }

    public virtual HCCoreStorage? InoviceStorage { get; set; }

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? ModifyByNavigation { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }

    public virtual HCUAccount UserAccount { get; set; } = null!;
}
