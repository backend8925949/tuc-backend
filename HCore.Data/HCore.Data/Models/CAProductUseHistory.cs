﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class CAProductUseHistory
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long ProductId { get; set; }

    public long? SubProductId { get; set; }

    public long AccountId { get; set; }

    public long ProductCodeId { get; set; }

    public double? Amount { get; set; }

    public long? LocationId { get; set; }

    public string? AccountComment { get; set; }

    public string? ProductManagerComment { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual CAProductLocation? Location { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual CAProduct Product { get; set; } = null!;

    public virtual CAProductCode ProductCode { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;

    public virtual CAProduct? SubProduct { get; set; }
}
