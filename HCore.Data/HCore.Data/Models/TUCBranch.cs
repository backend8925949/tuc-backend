﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCBranch
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OwnerId { get; set; }

    public long? ManagerId { get; set; }

    public string Name { get; set; } = null!;

    public string DisplayName { get; set; } = null!;

    public string? BranchCode { get; set; }

    public string? PhoneNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string? Address { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }

    public int CountryId { get; set; }

    public long? StateId { get; set; }

    public long? CityId { get; set; }

    public long? CityAreaId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCCoreCountryStateCity? City { get; set; }

    public virtual HCCoreCountryStateCityArea? CityArea { get; set; }

    public virtual HCCoreCountry Country { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? Manager { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount Owner { get; set; } = null!;

    public virtual HCCoreCountryState? State { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<TUCBranchAccount> TUCBranchAccount { get; set; } = new List<TUCBranchAccount>();

   public virtual ICollection<TUCBranchRmTarget> TUCBranchRmTarget { get; set; } = new List<TUCBranchRmTarget>();

   public virtual ICollection<TUCTerminal> TUCTerminal { get; set; } = new List<TUCTerminal>();
}
