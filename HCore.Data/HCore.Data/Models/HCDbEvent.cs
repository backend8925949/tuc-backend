﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCDbEvent
{
    public long Id { get; set; }

    public string? Title { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public int? StatusId { get; set; }

    public long? Count { get; set; }
}
