﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TULeadOwner
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? LeadId { get; set; }

    public long? AgentId { get; set; }

    public string? Comment { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int? StatusId { get; set; }

    public virtual HCUAccount? Agent { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual TULead? Lead { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }
}
