﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class TUCBranchAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long BranchId { get; set; }

    public long? ManagerId { get; set; }

    public long AccountId { get; set; }

    public long? OwnerId { get; set; }

    public long? MerchantId { get; set; }

    public long? StoreId { get; set; }

    public long AccountLevelId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCCoreParameter AccountLevel { get; set; } = null!;

    public virtual TUCBranch Branch { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCUAccount? Manager { get; set; }

    public virtual HCUAccount? Merchant { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCUAccount? Owner { get; set; }

    public virtual HCCore Status { get; set; } = null!;

    public virtual HCUAccount? Store { get; set; }
}
