﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class HCCoreAppVersionToken
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public long AppversionId { get; set; }

    public string Token { get; set; } = null!;

    public string? TokenEnc { get; set; }

    public DateTime CreateDate { get; set; }

    public DateTime? ExpiaryDate { get; set; }

    public DateTime? ExpiredOn { get; set; }

    public DateTime? LastRequestDate { get; set; }

    public int StatusId { get; set; }

    public virtual HCUAccount Account { get; set; } = null!;

    public virtual HCCoreAppVersion Appversion { get; set; } = null!;

    public virtual HCCore Status { get; set; } = null!;
}
