﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class BNPLLoanProcess
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long LoanId { get; set; }

    public long? UserId { get; set; }

    public long? Loan_Product_Id { get; set; }

    public int? Loan_Plan_Id { get; set; }

    public string? Vendor_Loan_Reference { get; set; }

    public long? Vendor_Loan_Amount { get; set; }

    public string? Vendor_Loan_Purpose { get; set; }

    public string? Vendor_Loan_Status { get; set; }

    public string? Lender_Name { get; set; }

    public string? Lender_Status { get; set; }

    public long? Organization_Id { get; set; }

    public string? Debit_Mandate_Next_Step { get; set; }

    public DateTime? Loan_Declined_Date { get; set; }

    public long? Loan_Declined_By_Id { get; set; }

    public DateTime? Loan_Approved_Date { get; set; }

    public long? Loan_Approved_By_Id { get; set; }

    public DateTime? Loan_Submite_Date { get; set; }

    public sbyte? Credit_Score_Check_Done { get; set; }

    public string? Merchant_Id { get; set; }

    public string? Service_Type_Id { get; set; }

    public string? Repayment_Amount { get; set; }

    public DateTime? Mandate_Start_Date { get; set; }

    public DateTime? Mandate_End_Date { get; set; }

    public string? Mandate_Type { get; set; }

    public string? Max_No_Of_Debits { get; set; }

    public string? MonoId { get; set; }

    public DateTime? Create_Date { get; set; }

    public long? Created_By_Id { get; set; }

    public DateTime? Modify_Date { get; set; }

    public long? Modify_By_Id { get; set; }

    public int? Status_Id { get; set; }

    public int? DebitMandateStatusId { get; set; }

   public virtual ICollection<BNPLPrequalification> BNPLPrequalification { get; set; } = new List<BNPLPrequalification>();

    public virtual HCUAccount? Created_By { get; set; }

    public virtual BNPLAccountLoan Loan { get; set; } = null!;

    public virtual BNPLPlan? Loan_Plan { get; set; }

    public virtual HCUAccount? Modify_By { get; set; }

    public virtual HCCore? Status { get; set; }
}
