﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class MDDealPromoCode
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int ValueTypeId { get; set; }

    public int? TypeId { get; set; }

    public long? DealId { get; set; }

    public int? CategoryId { get; set; }

    public long? AccountId { get; set; }

    public sbyte? IsAllDeals { get; set; }

    public string Code { get; set; } = null!;

    public double Value { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public int? StatusId { get; set; }

    public DateTime? CreateDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public virtual HCUAccount? Account { get; set; }

    public virtual MDCategory? Category { get; set; }

    public virtual HCUAccount? CreatedBy { get; set; }

    public virtual MDDeal? Deal { get; set; }

   public virtual ICollection<MDDealPromoCodeAccount> MDDealPromoCodeAccount { get; set; } = new List<MDDealPromoCodeAccount>();

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore? Status { get; set; }

    public virtual HCCore? Type { get; set; }

    public virtual HCCore ValueType { get; set; } = null!;
}
