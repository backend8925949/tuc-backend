﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class VASProduct
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long CategoryId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public long? ReferenceId { get; set; }

    public string? ReferenceKey { get; set; }

    public sbyte IsFixedAmount { get; set; }

    public double? Amount { get; set; }

    public double? RewardPercentage { get; set; }

    public double? UserPercentage { get; set; }

    public long? IconStorageId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifyById { get; set; }

    public int StatusId { get; set; }

    public DateTime SyncTime { get; set; }

    public virtual VASCategory Category { get; set; } = null!;

    public virtual HCUAccount CreatedBy { get; set; } = null!;

    public virtual HCCoreStorage? IconStorage { get; set; }

    public virtual HCUAccount? ModifyBy { get; set; }

    public virtual HCCore Status { get; set; } = null!;

   public virtual ICollection<VASProductItem> VASProductItem { get; set; } = new List<VASProductItem>();

   public virtual ICollection<VasPayment> VasPayment { get; set; } = new List<VasPayment>();
}
