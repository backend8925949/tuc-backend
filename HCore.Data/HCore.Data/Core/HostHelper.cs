//==================================================================================
// FileName: HostHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HCore.Data.Helper
{
    public class HostHelper
    {
        public static string Host = "";
        public static string HostLogging = "";
        public static string HostOperations = "";
        public static string HostIntegration = "";

    }
}

#region Master
// MASTER
// dotnet ef dbcontext scaffold Server="tuc-m-core.cd6dp6shun6m.eu-west-2.rds.amazonaws.com;User Id=tucsync;Password=ThankUCash@75885658342021;Database=HC_ThankUCash" "Pomelo.EntityFrameworkCore.MySql" -c HCoreContext -f --use-database-names   --no-pluralize -o "Models"  --no-onconfiguring --context-dir ""

// STAGING
// dotnet ef dbcontext scaffold Server="tuc-cicd-dev-qa.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=admin;Password=tgrtgtDDEF34DFF;Database=hcs_tuc_m" "Pomelo.EntityFrameworkCore.MySql" -c HCoreContext -f --use-database-names   --no-pluralize -o "Models"  --no-onconfiguring --context-dir ""


//private readonly string ConnectionString;
//public HCoreContext()
//{
//    this.ConnectionString = Helper.HostHelper.Host;
//}
//public HCoreContext(string ConnectionString) : base()
//        {
//    this.ConnectionString = ConnectionString;
//}
//public HCoreContext(DbContextOptions<HCoreContext> options) : base(options)
//        {
//}
//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//{
//    if (!optionsBuilder.IsConfigured)
//    {
//              optionsBuilder.UseMySql(ConnectionString, Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.23-mysql"));
//    }
//}
#endregion

#region Logging
// LOGGING
// Staging
//dotnet ef dbcontext scaffold "Server=tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_logging_staging" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names  --no-pluralize -o  "Logging/Models"  --no-onconfiguring --context-dir "Logging"

// LocalHost
// sudo dotnet ef dbcontext scaffold Server="localhost;User Id=root;Password=123456789;Database=tuc_operations_local;Scaffold:Views=off" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names   --no-pluralize --no-onconfiguring -o "Logging/Models"  --no-onconfiguring --context-dir "Logging"

// QA
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_logging_qa;Scaffold:Views=off" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names   --no-pluralize --no-onconfiguring -o "Logging/Models"  --no-onconfiguring --context-dir "Logging"

// DEV
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_logging_dev;Scaffold:Views=off" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names   --no-pluralize --no-onconfiguring -o "Logging/Models"  --no-onconfiguring --context-dir "Logging"

// STAGING
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_logging_staging;Scaffold:Views=off" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names   --no-pluralize --no-onconfiguring -o "Logging/Models"  --no-onconfiguring --context-dir "Logging"

// MASTER
// sudo dotnet ef dbcontext scaffold Server="tuc-master-operations.cd6dp6shun6m.eu-west-2.rds.amazonaws.com;User Id=tucmasterops;Password=a0_32755_RlNpKZHZo8_k;Database=tuc_master_operations;Scaffold:Views=off" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextLogging -f --use-database-names   --no-pluralize --no-onconfiguring -o "Logging/Models"  --no-onconfiguring --context-dir "Logging"



//private readonly string ConnectionString;
//public HCoreContextLogging()
//{
//    this.ConnectionString = Helper.HostHelper.HostLogging;
//}
//public HCoreContextLogging(string ConnectionString) : base()
//    {
//    this.ConnectionString = ConnectionString;
//}
//public HCoreContextLogging(DbContextOptions<HCoreContextLogging> options) : base(options)
//    {
//}
//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//{
//    if (!optionsBuilder.IsConfigured)
//    {
//        optionsBuilder.UseNpgsql(ConnectionString);
//    }
//}
#endregion




#region Operations
// OPERATIONS

// LocalHost
// sudo dotnet ef dbcontext scaffold Server="localhost;User Id=root;Password=123456789;Database=tuc_operations_local" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextOperations -f --use-database-names   --no-pluralize --no-onconfiguring -o "Operations/Models"  --no-onconfiguring --context-dir "Operations"

// QA
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_operations_qa" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextOperations -f --use-database-names   --no-pluralize --no-onconfiguring -o "Operations/Models"  --no-onconfiguring --context-dir "Operations"

// DEV
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_operations_dev" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextOperations -f --use-database-names   --no-pluralize --no-onconfiguring -o "Operations/Models"  --no-onconfiguring --context-dir "Operations"

// STAGING
// sudo dotnet ef dbcontext scaffold Server="tuc-staging-operations.cb5ml7rsm9wb.us-east-1.rds.amazonaws.com;User Id=tucstagingopscor;Password=mW8y_c4_3u52hv6RH8g_8;Database=tuc_operations_staging" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextOperations -f --use-database-names   --no-pluralize --no-onconfiguring -o "Operations/Models"  --no-onconfiguring --context-dir "Operations"

// MASTER
// sudo dotnet ef dbcontext scaffold Server="tuc-master-operations.cd6dp6shun6m.eu-west-2.rds.amazonaws.com;User Id=tucmasterops;Password=a0_32755_RlNpKZHZo8_k;Database=tuc_master_operations" "Npgsql.EntityFrameworkCore.PostgreSQL" -c HCoreContextOperations -f --use-database-names   --no-pluralize --no-onconfiguring -o "Operations/Models"  --no-onconfiguring --context-dir "Operations"


//private readonly string ConnectionString;
//public HCoreContextOperations()
//{
//    this.ConnectionString = Helper.HostHelper.HostOperations;
//}
//public HCoreContextOperations(string ConnectionString) : base()
//    {
//    this.ConnectionString = ConnectionString;
//}
//public HCoreContextOperations(DbContextOptions<HCoreContextOperations> options) : base(options)
//    {
//}
//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//{
//    if (!optionsBuilder.IsConfigured)
//    {
//        optionsBuilder.UseNpgsql(ConnectionString);
//    }
//}
#endregion


