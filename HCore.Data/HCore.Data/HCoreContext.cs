﻿using System;
using System.Collections.Generic;
using HCore.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace HCore.Data;

public partial class HCoreContext : DbContext
{

    private readonly string ConnectionString;
    public HCoreContext()
    {
        this.ConnectionString = Helper.HostHelper.Host;
    }
    public HCoreContext(string ConnectionString) : base()
    {
        this.ConnectionString = ConnectionString;
    }
    public HCoreContext(DbContextOptions<HCoreContext> options) : base(options)
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseMySql(ConnectionString, Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.23-mysql"));
        }
    }

    public virtual DbSet<BNPLAccount> BNPLAccount { get; set; }

    public virtual DbSet<BNPLAccountLoan> BNPLAccountLoan { get; set; }

    public virtual DbSet<BNPLAccountLoanActivity> BNPLAccountLoanActivity { get; set; }

    public virtual DbSet<BNPLAccountLoanPayment> BNPLAccountLoanPayment { get; set; }

    public virtual DbSet<BNPLConfiguration> BNPLConfiguration { get; set; }

    public virtual DbSet<BNPLLoanProcess> BNPLLoanProcess { get; set; }

    public virtual DbSet<BNPLMerchant> BNPLMerchant { get; set; }

    public virtual DbSet<BNPLMerchantSettlement> BNPLMerchantSettlement { get; set; }

    public virtual DbSet<BNPLPlan> BNPLPlan { get; set; }

    public virtual DbSet<BNPLPrequalification> BNPLPrequalification { get; set; }

    public virtual DbSet<CAProduct> CAProduct { get; set; }

    public virtual DbSet<CAProductAccountGroup> CAProductAccountGroup { get; set; }

    public virtual DbSet<CAProductCode> CAProductCode { get; set; }

    public virtual DbSet<CAProductLocation> CAProductLocation { get; set; }

    public virtual DbSet<CAProductShedule> CAProductShedule { get; set; }

    public virtual DbSet<CAProductUseHistory> CAProductUseHistory { get; set; }

    public virtual DbSet<HCCore> HCCore { get; set; }

    public virtual DbSet<HCCoreAddress> HCCoreAddress { get; set; }

    public virtual DbSet<HCCoreApp> HCCoreApp { get; set; }

    public virtual DbSet<HCCoreAppVersion> HCCoreAppVersion { get; set; }

    public virtual DbSet<HCCoreAppVersionToken> HCCoreAppVersionToken { get; set; }

    public virtual DbSet<HCCoreBinNumber> HCCoreBinNumber { get; set; }

    public virtual DbSet<HCCoreCommon> HCCoreCommon { get; set; }

    public virtual DbSet<HCCoreConfiguration> HCCoreConfiguration { get; set; }

    public virtual DbSet<HCCoreConfigurationHistory> HCCoreConfigurationHistory { get; set; }

    public virtual DbSet<HCCoreCountry> HCCoreCountry { get; set; }

    public virtual DbSet<HCCoreCountryState> HCCoreCountryState { get; set; }

    public virtual DbSet<HCCoreCountryStateCity> HCCoreCountryStateCity { get; set; }

    public virtual DbSet<HCCoreCountryStateCityArea> HCCoreCountryStateCityArea { get; set; }

    public virtual DbSet<HCCoreFeature> HCCoreFeature { get; set; }

    public virtual DbSet<HCCoreParameter> HCCoreParameter { get; set; }

    public virtual DbSet<HCCoreRole> HCCoreRole { get; set; }

    public virtual DbSet<HCCoreRoleFeature> HCCoreRoleFeature { get; set; }

    public virtual DbSet<HCCoreStorage> HCCoreStorage { get; set; }

    public virtual DbSet<HCCoreStorageFolder> HCCoreStorageFolder { get; set; }

    public virtual DbSet<HCCoreStorageTag> HCCoreStorageTag { get; set; }

    public virtual DbSet<HCCoreVerification> HCCoreVerification { get; set; }

    public virtual DbSet<HCDbEvent> HCDbEvent { get; set; }

    public virtual DbSet<HCSubscription> HCSubscription { get; set; }

    public virtual DbSet<HCSubscriptionFeature> HCSubscriptionFeature { get; set; }

    public virtual DbSet<HCSubscriptionFeatureItem> HCSubscriptionFeatureItem { get; set; }

    public virtual DbSet<HCToken> HCToken { get; set; }

    public virtual DbSet<HCTransaction> HCTransaction { get; set; }

    public virtual DbSet<HCTransactionParameter> HCTransactionParameter { get; set; }

    public virtual DbSet<HCUAccount> HCUAccount { get; set; }

    public virtual DbSet<HCUAccountActivity> HCUAccountActivity { get; set; }

    public virtual DbSet<HCUAccountAuth> HCUAccountAuth { get; set; }

    public virtual DbSet<HCUAccountBalance> HCUAccountBalance { get; set; }

    public virtual DbSet<HCUAccountBank> HCUAccountBank { get; set; }

    public virtual DbSet<HCUAccountConfiguration> HCUAccountConfiguration { get; set; }

    public virtual DbSet<HCUAccountConfigurationHistory> HCUAccountConfigurationHistory { get; set; }

    public virtual DbSet<HCUAccountDevice> HCUAccountDevice { get; set; }

    public virtual DbSet<HCUAccountInvoice> HCUAccountInvoice { get; set; }

    public virtual DbSet<HCUAccountLocation> HCUAccountLocation { get; set; }

    public virtual DbSet<HCUAccountOwner> HCUAccountOwner { get; set; }

    public virtual DbSet<HCUAccountParameter> HCUAccountParameter { get; set; }

    public virtual DbSet<HCUAccountSession> HCUAccountSession { get; set; }

    public virtual DbSet<HCUAccountSubscription> HCUAccountSubscription { get; set; }

    public virtual DbSet<HCUAccountSubscriptionFeature> HCUAccountSubscriptionFeature { get; set; }

    public virtual DbSet<HCUAccountSubscriptionFeatureItem> HCUAccountSubscriptionFeatureItem { get; set; }

    public virtual DbSet<HCUAccountSubscriptionPayment> HCUAccountSubscriptionPayment { get; set; }

    public virtual DbSet<HCUAccountTransaction> HCUAccountTransaction { get; set; }

    public virtual DbSet<HCoreCustomerObj> HCoreCustomerObj { get; set; }

    public virtual DbSet<LSCarriers> LSCarriers { get; set; }

    public virtual DbSet<LSItems> LSItems { get; set; }

    public virtual DbSet<LSOperations> LSOperations { get; set; }

    public virtual DbSet<LSPackages> LSPackages { get; set; }

    public virtual DbSet<LSParcel> LSParcel { get; set; }

    public virtual DbSet<LSShipmentRate> LSShipmentRate { get; set; }

    public virtual DbSet<LSShipments> LSShipments { get; set; }

    public virtual DbSet<MDCategory> MDCategory { get; set; }

    public virtual DbSet<MDDeal> MDDeal { get; set; }

    public virtual DbSet<MDDealAddress> MDDealAddress { get; set; }

    public virtual DbSet<MDDealBookmark> MDDealBookmark { get; set; }

    public virtual DbSet<MDDealCart> MDDealCart { get; set; }

    public virtual DbSet<MDDealCode> MDDealCode { get; set; }

    public virtual DbSet<MDDealGallery> MDDealGallery { get; set; }

    public virtual DbSet<MDDealLike> MDDealLike { get; set; }

    public virtual DbSet<MDDealLocation> MDDealLocation { get; set; }

    public virtual DbSet<MDDealNotification> MDDealNotification { get; set; }

    public virtual DbSet<MDDealPromoCode> MDDealPromoCode { get; set; }

    public virtual DbSet<MDDealPromoCodeAccount> MDDealPromoCodeAccount { get; set; }

    public virtual DbSet<MDDealPromotion> MDDealPromotion { get; set; }

    public virtual DbSet<MDDealPromotionClick> MDDealPromotionClick { get; set; }

    public virtual DbSet<MDDealReview> MDDealReview { get; set; }

    public virtual DbSet<MDDealSearch> MDDealSearch { get; set; }

    public virtual DbSet<MDDealShedule> MDDealShedule { get; set; }

    public virtual DbSet<MDDealView> MDDealView { get; set; }

    public virtual DbSet<MDDealWishlist> MDDealWishlist { get; set; }

    public virtual DbSet<MDFlashDeal> MDFlashDeal { get; set; }

    public virtual DbSet<MDPackaging> MDPackaging { get; set; }

    public virtual DbSet<MepasTransaction> MepasTransaction { get; set; }

    public virtual DbSet<ReferralSettings> ReferralSettings { get; set; }

    public virtual DbSet<SCCategory> SCCategory { get; set; }

    public virtual DbSet<SCConfiguration> SCConfiguration { get; set; }

    public virtual DbSet<SCOrder> SCOrder { get; set; }

    public virtual DbSet<SCOrderActivity> SCOrderActivity { get; set; }

    public virtual DbSet<SCOrderAddress> SCOrderAddress { get; set; }

    public virtual DbSet<SCOrderItem> SCOrderItem { get; set; }

    public virtual DbSet<SCProduct> SCProduct { get; set; }

    public virtual DbSet<SCProductVarient> SCProductVarient { get; set; }

    public virtual DbSet<SCProductVarientStock> SCProductVarientStock { get; set; }

    public virtual DbSet<SMSCampaign> SMSCampaign { get; set; }

    public virtual DbSet<SMSCampaignGroup> SMSCampaignGroup { get; set; }

    public virtual DbSet<SMSCampaignGroupCondition> SMSCampaignGroupCondition { get; set; }

    public virtual DbSet<SMSCampaignGroupItem> SMSCampaignGroupItem { get; set; }

    public virtual DbSet<SMSGroup> SMSGroup { get; set; }

    public virtual DbSet<SMSGroupCondition> SMSGroupCondition { get; set; }

    public virtual DbSet<SMSGroupItem> SMSGroupItem { get; set; }

    public virtual DbSet<SMSSenderProvider> SMSSenderProvider { get; set; }

    public virtual DbSet<TUCAppPromotion> TUCAppPromotion { get; set; }

    public virtual DbSet<TUCBranch> TUCBranch { get; set; }

    public virtual DbSet<TUCBranchAccount> TUCBranchAccount { get; set; }

    public virtual DbSet<TUCBranchRmTarget> TUCBranchRmTarget { get; set; }

    public virtual DbSet<TUCCardRewardProcessing> TUCCardRewardProcessing { get; set; }

    public virtual DbSet<TUCCategory> TUCCategory { get; set; }

    public virtual DbSet<TUCCategoryAccount> TUCCategoryAccount { get; set; }

    public virtual DbSet<TUCLProgram> TUCLProgram { get; set; }

    public virtual DbSet<TUCLProgramGroup> TUCLProgramGroup { get; set; }

    public virtual DbSet<TUCLoyalty> TUCLoyalty { get; set; }

    public virtual DbSet<TUCLoyaltyMerchantCustomer> TUCLoyaltyMerchantCustomer { get; set; }

    public virtual DbSet<TUCLoyaltyPending> TUCLoyaltyPending { get; set; }

    public virtual DbSet<TUCMerchantCategory> TUCMerchantCategory { get; set; }

    public virtual DbSet<TUCNinjaRegistration> TUCNinjaRegistration { get; set; }

    public virtual DbSet<TUCPayOut> TUCPayOut { get; set; }

    public virtual DbSet<TUCPromoCode> TUCPromoCode { get; set; }

    public virtual DbSet<TUCPromoCodeAccount> TUCPromoCodeAccount { get; set; }

    public virtual DbSet<TUCPromoCodeCondition> TUCPromoCodeCondition { get; set; }

    public virtual DbSet<TUCReceiptScan> TUCReceiptScan { get; set; }

    public virtual DbSet<TUCSale> TUCSale { get; set; }

    public virtual DbSet<TUCStoreAcquirerCollection> TUCStoreAcquirerCollection { get; set; }

    public virtual DbSet<TUCTerminal> TUCTerminal { get; set; }

    public virtual DbSet<TUCTerminalProduct> TUCTerminalProduct { get; set; }

    public virtual DbSet<TUCTerminalStatus> TUCTerminalStatus { get; set; }

    public virtual DbSet<TUCampaign> TUCampaign { get; set; }

    public virtual DbSet<TUCampaignAudience> TUCampaignAudience { get; set; }

    public virtual DbSet<TUCard> TUCard { get; set; }

    public virtual DbSet<TULead> TULead { get; set; }

    public virtual DbSet<TULeadActivity> TULeadActivity { get; set; }

    public virtual DbSet<TULeadGroup> TULeadGroup { get; set; }

    public virtual DbSet<TULeadOwner> TULeadOwner { get; set; }

    public virtual DbSet<TUProduct> TUProduct { get; set; }

    public virtual DbSet<TUProductCode> TUProductCode { get; set; }

    public virtual DbSet<VASCategory> VASCategory { get; set; }

    public virtual DbSet<VASProduct> VASProduct { get; set; }

    public virtual DbSet<VASProductItem> VASProductItem { get; set; }

    public virtual DbSet<VasPayment> VasPayment { get; set; }

    public virtual DbSet<cmt_loyalty> cmt_loyalty { get; set; }

    public virtual DbSet<cmt_loyalty_customer> cmt_loyalty_customer { get; set; }

    public virtual DbSet<cmt_sale> cmt_sale { get; set; }

    public virtual DbSet<cmt_sale_failed> cmt_sale_failed { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<BNPLAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable(tb => tb.HasComment("This table manages bnpl activated accounts"));

            entity.HasIndex(e => e.AccountId, "fk_bnpl_acc_accid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_acc_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bnpl_acc_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_acc_statusid_core_idx");

            entity.Property(e => e.Address).HasMaxLength(64);
            entity.Property(e => e.BankAccountName).HasMaxLength(128);
            entity.Property(e => e.BankAccountNumber).HasMaxLength(64);
            entity.Property(e => e.BankCode).HasMaxLength(32);
            entity.Property(e => e.BankName).HasMaxLength(128);
            entity.Property(e => e.BankType).HasMaxLength(128);
            entity.Property(e => e.BvnNumber).HasMaxLength(64);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DateOfBirth).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress).HasMaxLength(128);
            entity.Property(e => e.FirstName).HasMaxLength(64);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastName).HasMaxLength(64);
            entity.Property(e => e.LgaName).HasMaxLength(64);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.MonoReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.ProviderReference).HasMaxLength(128);
            entity.Property(e => e.StateName).HasMaxLength(64);
            entity.Property(e => e.WorkEmailAddress).HasMaxLength(128);

            entity.HasOne(d => d.Account).WithMany(p => p.BNPLAccountAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_acc_accid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLAccountCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_acc_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLAccountModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bnpl_acc_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLAccount)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_acc_statusid_core");
        });

        modelBuilder.Entity<BNPLAccountLoan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_bnpl_accid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_createdbyid_acc_idx");

            entity.HasIndex(e => e.MerchantId, "fk_bnpl_merchantid_bnplacc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bnpl_modifybyid_acc_idx");

            entity.HasIndex(e => e.PlanId, "fk_bnpl_planid_plan_idx");

            entity.HasIndex(e => e.ProviderId, "fk_bnpl_providerid_hcuacc_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LoanCode).HasMaxLength(16);
            entity.Property(e => e.LoanPin).HasMaxLength(32);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.RedeemDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.BNPLAccountLoan)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_accid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLAccountLoanCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_createdbyid_acc");

            entity.HasOne(d => d.Merchant).WithMany(p => p.BNPLAccountLoan)
                .HasForeignKey(d => d.MerchantId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchantid_merchant");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLAccountLoanModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bnpl_modifybyid_acc");

            entity.HasOne(d => d.Plan).WithMany(p => p.BNPLAccountLoan)
                .HasForeignKey(d => d.PlanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_planid_plan");

            entity.HasOne(d => d.Provider).WithMany(p => p.BNPLAccountLoanProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("fk_bnpl_providerid_hcuacc");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLAccountLoan)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_statusid_core");
        });

        modelBuilder.Entity<BNPLAccountLoanActivity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.LoanId, "fk_bnpl_accloanact_loanid_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_accstatusid_status_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(128);
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.Loan).WithMany(p => p.BNPLAccountLoanActivity)
                .HasForeignKey(d => d.LoanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_accloanact_loanid");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLAccountLoanActivity)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_accstatusid_status");
        });

        modelBuilder.Entity<BNPLAccountLoanPayment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_loanpay_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bnpl_loanpay_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_loanpay_statusid_helper_idx");

            entity.HasIndex(e => e.LoanId, "fk_bnpl_loanpayment_loanid_loan_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentReference).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLAccountLoanPaymentCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_loanpay_createdbyid_acc");

            entity.HasOne(d => d.Loan).WithMany(p => p.BNPLAccountLoanPayment)
                .HasForeignKey(d => d.LoanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_loanpayment_loanid_loan");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLAccountLoanPaymentModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bnpl_loanpay_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLAccountLoanPayment)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_loanpay_statusid_helper");
        });

        modelBuilder.Entity<BNPLConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.ModifyById, "fk_bnpl_config_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_config_statusid_core_idx");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Policy).HasMaxLength(2000);

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLConfiguration)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bnpl_config_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLConfiguration)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_config_statusid_core");
        });

        modelBuilder.Entity<BNPLLoanProcess>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Created_By_Id, "FK_BNPLLoanProcess_CreatedById_HCUA_Id_idx");

            entity.HasIndex(e => e.Loan_Plan_Id, "FK_BNPLLoanProcess_LoanPlanId_BNPLPlan_Id_idx");

            entity.HasIndex(e => e.Modify_By_Id, "FK_BNPLLoanProcess_ModifyById_HCUA_Id_idx");

            entity.HasIndex(e => e.Status_Id, "FK_BNPLLoanProcess_StatusId_HCore_Id_idx");

            entity.HasIndex(e => e.LoanId, "FK_BNPLLoanProcess_loanId_BNPLAccountLoan_Id_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.Create_Date).HasColumnType("datetime");
            entity.Property(e => e.Credit_Score_Check_Done).HasDefaultValueSql("'0'");
            entity.Property(e => e.Debit_Mandate_Next_Step).HasMaxLength(45);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Lender_Name).HasMaxLength(250);
            entity.Property(e => e.Lender_Status).HasMaxLength(45);
            entity.Property(e => e.Loan_Approved_Date).HasColumnType("datetime");
            entity.Property(e => e.Loan_Declined_Date).HasColumnType("datetime");
            entity.Property(e => e.Loan_Submite_Date).HasColumnType("datetime");
            entity.Property(e => e.Mandate_End_Date).HasColumnType("datetime");
            entity.Property(e => e.Mandate_Start_Date).HasColumnType("datetime");
            entity.Property(e => e.Mandate_Type).HasMaxLength(45);
            entity.Property(e => e.Max_No_Of_Debits).HasMaxLength(45);
            entity.Property(e => e.Merchant_Id).HasMaxLength(45);
            entity.Property(e => e.Modify_Date).HasColumnType("datetime");
            entity.Property(e => e.MonoId).HasMaxLength(45);
            entity.Property(e => e.Repayment_Amount).HasMaxLength(45);
            entity.Property(e => e.Service_Type_Id).HasMaxLength(45);
            entity.Property(e => e.Vendor_Loan_Purpose).HasMaxLength(250);
            entity.Property(e => e.Vendor_Loan_Reference).HasMaxLength(45);
            entity.Property(e => e.Vendor_Loan_Status).HasMaxLength(45);

            entity.HasOne(d => d.Created_By).WithMany(p => p.BNPLLoanProcessCreated_By)
                .HasForeignKey(d => d.Created_By_Id)
                .HasConstraintName("FK_BNPLLoanProcess_CreatedById_HCUA_Id");

            entity.HasOne(d => d.Loan).WithMany(p => p.BNPLLoanProcess)
                .HasForeignKey(d => d.LoanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_BNPLLoanProcess_loanId_BNPLAccountLoan_Id");

            entity.HasOne(d => d.Loan_Plan).WithMany(p => p.BNPLLoanProcess)
                .HasForeignKey(d => d.Loan_Plan_Id)
                .HasConstraintName("FK_BNPLLoanProcess_LoanPlanId_BNPLPlan_Id");

            entity.HasOne(d => d.Modify_By).WithMany(p => p.BNPLLoanProcessModify_By)
                .HasForeignKey(d => d.Modify_By_Id)
                .HasConstraintName("FK_BNPLLoanProcess_ModifyById_HCUA_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLLoanProcess)
                .HasForeignKey(d => d.Status_Id)
                .HasConstraintName("FK_BNPLLoanProcess_StatusId_HCore_Id");
        });

        modelBuilder.Entity<BNPLMerchant>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_bnpl_merchant_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_merchant_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifiyById, "fk_bnpl_merchant_modifybyid_acc_idx");

            entity.HasIndex(e => e.SettelmentTypeId, "fk_bnpl_merchant_settelmenttypeid_helper");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_merchant_statusid_core_idx");

            entity.HasIndex(e => e.TransactionSourceId, "fk_bnpl_merchant_transctionsourceid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsMerchantSettelment).HasDefaultValueSql("'1'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.BNPLMerchantAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchant_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLMerchantCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchant_createdbyid_acc");

            entity.HasOne(d => d.ModifiyBy).WithMany(p => p.BNPLMerchantModifiyBy)
                .HasForeignKey(d => d.ModifiyById)
                .HasConstraintName("fk_bnpl_merchant_modifybyid_acc");

            entity.HasOne(d => d.SettelmentType).WithMany(p => p.BNPLMerchantSettelmentType)
                .HasForeignKey(d => d.SettelmentTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchant_settementtypeid_helper");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLMerchantStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchant_statusid_core");

            entity.HasOne(d => d.TransactionSource).WithMany(p => p.BNPLMerchantTransactionSource)
                .HasForeignKey(d => d.TransactionSourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchant_transctionsourceid_helper");
        });

        modelBuilder.Entity<BNPLMerchantSettlement>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_createdbyid_hcua_id_idx");

            entity.HasIndex(e => e.LoanId, "fk_bnpl_loanid_bnplaccloan_id_idx");

            entity.HasIndex(e => e.MerchantId, "fk_bnpl_merchantid_bnplmerchant_id_idx");

            entity.HasIndex(e => e.ModifyByid, "fk_bnpl_modifybyid_hcua_id_idx");

            entity.HasIndex(e => e.ProviderId, "fk_bnpl_providerid_hcua_id_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_statusid_hcore_id_idx");

            entity.HasIndex(e => e.TransactionReference, "fk_bnpl_transactionreference_hcuacctrans_id_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InvoiceNumber).HasMaxLength(30);
            entity.Property(e => e.LoanRedeenDate).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SettlementDate).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLMerchantSettlementCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_bnpl_createdbyid_hcua_id");

            entity.HasOne(d => d.Loan).WithMany(p => p.BNPLMerchantSettlement)
                .HasForeignKey(d => d.LoanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_loanid_bnplaccloan_id");

            entity.HasOne(d => d.Merchant).WithMany(p => p.BNPLMerchantSettlement)
                .HasForeignKey(d => d.MerchantId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_merchantid_bnplmerchant_id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLMerchantSettlementModifyBy)
                .HasForeignKey(d => d.ModifyByid)
                .HasConstraintName("fk_bnpl_modifybyid_hcua_id");

            entity.HasOne(d => d.Provider).WithMany(p => p.BNPLMerchantSettlementProvider)
                .HasForeignKey(d => d.ProviderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_providerid_hcua_id");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLMerchantSettlement)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_statusid_hcore_id");

            entity.HasOne(d => d.TransactionReferenceNavigation).WithMany(p => p.BNPLMerchantSettlement)
                .HasForeignKey(d => d.TransactionReference)
                .HasConstraintName("fk_bnpl_transactionreference_hcuacctrans_id");
        });

        modelBuilder.Entity<BNPLPlan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CreatedById, "fk_bnpl_plan_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bnpl_plan_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_bnpl_plan_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.ExcludeWeekends).HasDefaultValueSql("'0'");
            entity.Property(e => e.Frequency).HasMaxLength(45);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.Policy).HasMaxLength(1024);
            entity.Property(e => e.TenuredIn).HasMaxLength(45);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLPlanCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_plan_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLPlanModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bnpl_plan_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLPlan)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bnpl_plan_statusid_core");
        });

        modelBuilder.Entity<BNPLPrequalification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "FK_BNPLPreq_BNPLAccNum_BNPLAcc_Id_idx");

            entity.HasIndex(e => e.CreatedById, "FK_BNPLPreq_CreatedById_HCUA_Id_idx");

            entity.HasIndex(e => e.LoanId, "FK_BNPLPreq_LoanId_HCUA_Id_idx");

            entity.HasIndex(e => e.ModifyByid, "FK_BNPLPreq_ModifyById_HCUA_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_BNPLPreq_StatusId_HCore_Id_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.BankAccountNumber).HasMaxLength(45);
            entity.Property(e => e.BankCode).HasMaxLength(45);
            entity.Property(e => e.BankName).HasMaxLength(250);
            entity.Property(e => e.BankType).HasMaxLength(250);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.BNPLPrequalification)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_BNPLPreq_BNPLAccNum_BNPLAcc_Id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.BNPLPrequalificationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_BNPLPreq_CreatedById_HCUA_Id");

            entity.HasOne(d => d.Loan).WithMany(p => p.BNPLPrequalification)
                .HasForeignKey(d => d.LoanId)
                .HasConstraintName("FK_BNPLPreq_LoanId_HCUA_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.BNPLPrequalificationModifyBy)
                .HasForeignKey(d => d.ModifyByid)
                .HasConstraintName("FK_BNPLPreq_ModifyById_HCUA_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.BNPLPrequalification)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_BNPLPreq_StatusId_HCore_Id");
        });

        modelBuilder.Entity<CAProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_caproduct_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_caproduct_createdbyid_account_idx");

            entity.HasIndex(e => e.IconStorageId, "fk_caproduct_iconstorageid_storage_idx");

            entity.HasIndex(e => e.LastUseLocationId, "fk_caproduct_lastuselocationid_caprodloca_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caproduct_modifybyid_account_idx");

            entity.HasIndex(e => e.ParentId, "fk_caproduct_parentid_caproduct_idx");

            entity.HasIndex(e => e.PosterStorageId, "fk_caproduct_posterstorageid_storage_idx");

            entity.HasIndex(e => e.StatusId, "fk_caproduct_statusid_corehelper_idx");

            entity.HasIndex(e => e.SubTypeId, "fk_caproduct_subtypeid_corehelper_idx");

            entity.HasIndex(e => e.TypeId, "fk_caproduct_typeid_corehelper_idx");

            entity.HasIndex(e => e.UsageTypeId, "fk_caproduct_usagetypeid_corehelper_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(128);
            entity.Property(e => e.Amount).HasDefaultValueSql("'0'");
            entity.Property(e => e.Charge).HasDefaultValueSql("'0'");
            entity.Property(e => e.CodeValidityEndDate).HasColumnType("datetime");
            entity.Property(e => e.CodeValidityStartDate).HasColumnType("datetime");
            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CommissionAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.CompanyName).HasMaxLength(200);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.DiscountAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.DiscountPercentage).HasDefaultValueSql("'0'");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsIndividual).HasDefaultValueSql("'0'");
            entity.Property(e => e.IsPinRequired).HasDefaultValueSql("'0'");
            entity.Property(e => e.LastUseDate).HasColumnType("datetime");
            entity.Property(e => e.MaximumUnitSale).HasDefaultValueSql("'0'");
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentReference).HasMaxLength(128);
            entity.Property(e => e.PaymentSource).HasMaxLength(64);
            entity.Property(e => e.SellingPrice).HasDefaultValueSql("'0'");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(256);
            entity.Property(e => e.TotalAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.TotalSaleAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.TotalUnitSale).HasDefaultValueSql("'0'");
            entity.Property(e => e.TotalUsed).HasDefaultValueSql("'0'");
            entity.Property(e => e.TotalUsedAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.UnitPrice).HasDefaultValueSql("'0'");
            entity.Property(e => e.Views).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Account).WithMany(p => p.CAProductAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproduct_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproduct_createdbyid_account");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.CAProductIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("fk_caproduct_iconstorageid_storage");

            entity.HasOne(d => d.LastUseLocation).WithMany(p => p.CAProduct)
                .HasForeignKey(d => d.LastUseLocationId)
                .HasConstraintName("fk_caproduct_lastuselocationid_caprodloca");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caproduct_modifybyid_account");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("fk_caproduct_parentid_caproduct");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.CAProductPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("fk_caproduct_posterstorageid_storage");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproduct_statusid_corehelper");

            entity.HasOne(d => d.SubType).WithMany(p => p.CAProductSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("fk_caproduct_subtypeid_corehelper");

            entity.HasOne(d => d.Type).WithMany(p => p.CAProductType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproduct_typeid_corehelper");

            entity.HasOne(d => d.UsageType).WithMany(p => p.CAProductUsageType)
                .HasForeignKey(d => d.UsageTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproduct_usagetypeid_corehelper");
        });

        modelBuilder.Entity<CAProductAccountGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_prodaccgroup_accountid_uacc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_prodaccgroup_createdby_uacc_idx");

            entity.HasIndex(e => e.GenderId, "fk_prodaccgroup_genderid_corehelper_idx");

            entity.HasIndex(e => e.ModifyById, "fk_prodaccgroup_modifybyid_uacc_idx");

            entity.HasIndex(e => e.OwnerId, "fk_prodaccgroup_ownerid_uacc_idx");

            entity.HasIndex(e => e.ParentId, "fk_prodaccgroup_parentid_prodaccgroup_idx");

            entity.HasIndex(e => e.ProductId, "fk_prodaccgroup_productid_caproduct_idx");

            entity.HasIndex(e => e.StatusId, "fk_prodaccgroup_statusid_corehelper_idx");

            entity.HasIndex(e => e.SubProductId, "fk_prodaccgroup_subproductid_idx");

            entity.HasIndex(e => e.SubTypeId, "fk_prodaccgroup_subtypeid_helper_idx");

            entity.HasIndex(e => e.TypeId, "fk_prodaccgroup_typeid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(30);
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.FirstName)
                .HasMaxLength(256)
                .HasDefaultValueSql("'`'");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastName).HasMaxLength(256);
            entity.Property(e => e.MobileNumber).HasMaxLength(32);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.CAProductAccountGroupAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_prodaccgroup_accountid_uacc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductAccountGroupCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_prodaccgroup_createdby_uacc");

            entity.HasOne(d => d.Gender).WithMany(p => p.CAProductAccountGroupGender)
                .HasForeignKey(d => d.GenderId)
                .HasConstraintName("fk_prodaccgroup_genderid_corehelper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductAccountGroupModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_prodaccgroup_modifybyid_uacc");

            entity.HasOne(d => d.Owner).WithMany(p => p.CAProductAccountGroupOwner)
                .HasForeignKey(d => d.OwnerId)
                .HasConstraintName("fk_prodaccgroup_ownerid_uacc");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("fk_prodaccgroup_parentid_prodaccgroup");

            entity.HasOne(d => d.Product).WithMany(p => p.CAProductAccountGroupProduct)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("fk_prodaccgroup_productid_caproduct");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductAccountGroupStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_prodaccgroup_statusid_corehelper");

            entity.HasOne(d => d.SubProduct).WithMany(p => p.CAProductAccountGroupSubProduct)
                .HasForeignKey(d => d.SubProductId)
                .HasConstraintName("fk_prodaccgroup_subproductid");

            entity.HasOne(d => d.SubType).WithMany(p => p.CAProductAccountGroupSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("fk_prodaccgroup_subtypeid_helper");

            entity.HasOne(d => d.Type).WithMany(p => p.CAProductAccountGroupType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_prodaccgroup_typeid_helper");
        });

        modelBuilder.Entity<CAProductCode>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_caproductcode_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_caproductcode_createdbyid_account_idx");

            entity.HasIndex(e => e.LastUseLocationId, "fk_caproductcode_lastuselocationid_caprodloc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caproductcode_modifybyid_account_idx");

            entity.HasIndex(e => e.ProductId, "fk_caproductcode_productid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_caproductcode_statusid_corehelper_idx");

            entity.HasIndex(e => e.SubProductId, "fk_caproductcode_subproductid_product_idx");

            entity.HasIndex(e => e.SubTypeId, "fk_caproductcode_subtypeid_corehelper_idx");

            entity.HasIndex(e => e.TransactionId, "fk_caproductcode_transactionid_transaction_idx");

            entity.HasIndex(e => e.TypeId, "fk_caproductcode_typeid_corehelper_idx");

            entity.Property(e => e.AvailableAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ItemAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ItemCode).HasMaxLength(1024);
            entity.Property(e => e.ItemPin).HasMaxLength(256);
            entity.Property(e => e.LastUseDate).HasColumnType("datetime");
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.UseAttempts).HasDefaultValueSql("'0'");
            entity.Property(e => e.UseCount).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Account).WithMany(p => p.CAProductCodeAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_caproductcode_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductCodeCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductcode_createdbyid_account");

            entity.HasOne(d => d.LastUseLocation).WithMany(p => p.CAProductCode)
                .HasForeignKey(d => d.LastUseLocationId)
                .HasConstraintName("fk_caproductcode_lastuselocationid_caprodloc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductCodeModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caproductcode_modifybyid_account");

            entity.HasOne(d => d.Product).WithMany(p => p.CAProductCodeProduct)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductcode_productid_product");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductCodeStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductcode_statusid_corehelper");

            entity.HasOne(d => d.SubProduct).WithMany(p => p.CAProductCodeSubProduct)
                .HasForeignKey(d => d.SubProductId)
                .HasConstraintName("fk_caproductcode_subproductid_product");

            entity.HasOne(d => d.SubType).WithMany(p => p.CAProductCodeSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("fk_caproductcode_subtypeid_corehelper");

            entity.HasOne(d => d.Transaction).WithMany(p => p.CAProductCode)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("fk_caproductcode_transactionid_transaction");

            entity.HasOne(d => d.Type).WithMany(p => p.CAProductCodeType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductcode_typeid_corehelper");
        });

        modelBuilder.Entity<CAProductLocation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_caproductlocation_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_caproductlocation_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caproductlocation_modifybyid_account_idx");

            entity.HasIndex(e => e.ProductId, "fk_caproductlocation_productid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_caproductlocation_statusid_corehelper_idx");

            entity.HasIndex(e => e.SubAccountId, "fk_caproductlocation_subaccountid_account_idx");

            entity.HasIndex(e => e.SubProductId, "fk_caproductlocation_subproductid_product_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ProductCodeUseCount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ProductLastUseDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.CAProductLocationAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductlocation_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductLocationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductlocation_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductLocationModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caproductlocation_modifybyid_account");

            entity.HasOne(d => d.Product).WithMany(p => p.CAProductLocationProduct)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductlocation_productid_product");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductLocation)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductlocation_statusid_corehelper");

            entity.HasOne(d => d.SubAccount).WithMany(p => p.CAProductLocationSubAccount)
                .HasForeignKey(d => d.SubAccountId)
                .HasConstraintName("fk_caproductlocation_subaccountid_account");

            entity.HasOne(d => d.SubProduct).WithMany(p => p.CAProductLocationSubProduct)
                .HasForeignKey(d => d.SubProductId)
                .HasConstraintName("fk_caproductlocation_subproductid_product");
        });

        modelBuilder.Entity<CAProductShedule>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_caproductshedule_createdbyd_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caproductshedule_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_caproductshedule_statusid_corehelper_idx");

            entity.HasIndex(e => e.TypeId, "fk_caproductshedule_typeid_idx");

            entity.HasIndex(e => e.ProductId, "fk_csproductshedule_productid_caproduct_idx");

            entity.HasIndex(e => e.SubProductId, "fk_csproductshedule_subproductid_product_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartTime).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductSheduleCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductshedule_createdbyd");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductSheduleModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caproductshedule_modifybyid_account");

            entity.HasOne(d => d.Product).WithMany(p => p.CAProductSheduleProduct)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductshedule_productid_caproduct");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductShedule)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductshedule_statusid_corehelper");

            entity.HasOne(d => d.SubProduct).WithMany(p => p.CAProductSheduleSubProduct)
                .HasForeignKey(d => d.SubProductId)
                .HasConstraintName("fk_caproductshedule_subproductid_product");

            entity.HasOne(d => d.Type).WithMany(p => p.CAProductSheduleType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_caproductshedule_typeid");
        });

        modelBuilder.Entity<CAProductUseHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.ProductCodeId, "fk_caproductusehistory_codeid_productcode_idx");

            entity.HasIndex(e => e.CreatedById, "fk_caproductusehistory_createdbyid_account_idx");

            entity.HasIndex(e => e.LocationId, "fk_caproductusehistory_locationid_productlocation_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caproductusehistory_modifybyid_idx");

            entity.HasIndex(e => e.ProductId, "fk_caproductusehistory_productid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_caproductusehistory_statusid_corehelper_idx");

            entity.HasIndex(e => e.AccountId, "fk_caproductusehitosty_accountid_account_idx");

            entity.HasIndex(e => e.SubProductId, "fk_caproductusehitosty_subproductid_product_idx");

            entity.Property(e => e.AccountComment).HasMaxLength(512);
            entity.Property(e => e.Amount).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ProductManagerComment).HasMaxLength(512);

            entity.HasOne(d => d.Account).WithMany(p => p.CAProductUseHistoryAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductusehitosty_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.CAProductUseHistoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductusehistory_createdbyid_account");

            entity.HasOne(d => d.Location).WithMany(p => p.CAProductUseHistory)
                .HasForeignKey(d => d.LocationId)
                .HasConstraintName("fk_caproductusehistory_locationid_productlocation");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.CAProductUseHistoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caproductusehistory_modifybyid");

            entity.HasOne(d => d.ProductCode).WithMany(p => p.CAProductUseHistory)
                .HasForeignKey(d => d.ProductCodeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductusehistory_codeid_productcode");

            entity.HasOne(d => d.Product).WithMany(p => p.CAProductUseHistoryProduct)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductusehistory_productid_product");

            entity.HasOne(d => d.Status).WithMany(p => p.CAProductUseHistory)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caproductusehistory_statusid_corehelper");

            entity.HasOne(d => d.SubProduct).WithMany(p => p.CAProductUseHistorySubProduct)
                .HasForeignKey(d => d.SubProductId)
                .HasConstraintName("fk_caproductusehitosty_subproductid_product");
        });

        modelBuilder.Entity<HCCore>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.ParentId, "fk_core_parentid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_core_statusid_core_idx");

            entity.HasIndex(e => e.SubParentId, "fk_core_subparentid_core_idx");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Name).HasMaxLength(64);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(64);
            entity.Property(e => e.TypeName).HasMaxLength(64);
            entity.Property(e => e.Value).HasMaxLength(64);

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("fk_core_parentid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.InverseStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_core_statusid_core");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("fk_core_subparentid_core");
        });

        modelBuilder.Entity<HCCoreAddress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_caddress_accountid_account_idx");

            entity.HasIndex(e => e.CityAreaId, "fk_caddress_cityareaid_cityarea_idx");

            entity.HasIndex(e => e.CityId, "fk_caddress_cityid_city_idx");

            entity.HasIndex(e => e.CountryId, "fk_caddress_country_countryid_idx");

            entity.HasIndex(e => e.LocationTypeId, "fk_caddress_locationtypeid_helper_idx");

            entity.HasIndex(e => e.ModifyById, "fk_caddress_modifybyid_account_idx");

            entity.HasIndex(e => e.StateId, "fk_caddress_stateid_state_idx");

            entity.HasIndex(e => e.StatusId, "fk_caddress_statusid_helper_idx");

            entity.HasIndex(e => e.CreatedById, "fl_caddress_createdbyid_account_idx");

            entity.Property(e => e.AdditionalContactNumber).HasMaxLength(15);
            entity.Property(e => e.AddressLine1).HasMaxLength(128);
            entity.Property(e => e.AddressLine2).HasMaxLength(128);
            entity.Property(e => e.ContactNumber).HasMaxLength(16);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(64);
            entity.Property(e => e.EmailAddress).HasMaxLength(128);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Instructions).HasMaxLength(128);
            entity.Property(e => e.Landmark).HasMaxLength(64);
            entity.Property(e => e.MapAddress).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.ZipCode).HasMaxLength(16);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreAddressAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_caddress_accountid_account");

            entity.HasOne(d => d.CityArea).WithMany(p => p.HCCoreAddress)
                .HasForeignKey(d => d.CityAreaId)
                .HasConstraintName("fk_caddress_cityareaid_cityarea");

            entity.HasOne(d => d.City).WithMany(p => p.HCCoreAddress)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("fk_caddress_cityid_city");

            entity.HasOne(d => d.Country).WithMany(p => p.HCCoreAddress)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caddress_country_countryid");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreAddressCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caddress_createdbyid_account");

            entity.HasOne(d => d.LocationType).WithMany(p => p.HCCoreAddressLocationType)
                .HasForeignKey(d => d.LocationTypeId)
                .HasConstraintName("fk_caddress_locationtypeid_helper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreAddressModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_caddress_modifybyid_account");

            entity.HasOne(d => d.State).WithMany(p => p.HCCoreAddress)
                .HasForeignKey(d => d.StateId)
                .HasConstraintName("fk_caddress_stateid_state");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreAddressStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_caddress_statusid_helper");
        });

        modelBuilder.Entity<HCCoreApp>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_app_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_app_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_app_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_app_statusid_core_idx");

            entity.Property(e => e.AllowLogging).HasDefaultValueSql("'1'");
            entity.Property(e => e.AppKey).HasMaxLength(64);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IpAddress).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreAppAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_app_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreAppCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_app_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreAppModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_app_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreApp)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_app_statusid_core");
        });

        modelBuilder.Entity<HCCoreAppVersion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AppId, "fk_appv_appid_app_idx");

            entity.HasIndex(e => e.CreatedById, "fk_appv_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_appv_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_appv_statusid_core_idx");

            entity.Property(e => e.ApiKey).HasMaxLength(128);
            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastRequestTime).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.VersionId).HasMaxLength(8);

            entity.HasOne(d => d.App).WithMany(p => p.HCCoreAppVersion)
                .HasForeignKey(d => d.AppId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_appid_app");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreAppVersionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreAppVersionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_appv_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreAppVersion)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_statusid_core");
        });

        modelBuilder.Entity<HCCoreAppVersionToken>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_token_accountid_account_idx");

            entity.HasIndex(e => e.AppversionId, "fk_token_appversionid_appversion_idx");

            entity.HasIndex(e => e.StatusId, "fk_token_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.ExpiaryDate).HasColumnType("datetime");
            entity.Property(e => e.ExpiredOn).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastRequestDate).HasColumnType("datetime");
            entity.Property(e => e.Token).HasMaxLength(256);
            entity.Property(e => e.TokenEnc).HasMaxLength(2048);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreAppVersionToken)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_apvtoken_accountid_account");

            entity.HasOne(d => d.Appversion).WithMany(p => p.HCCoreAppVersionToken)
                .HasForeignKey(d => d.AppversionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_apvtoken_appversionid_appversion");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreAppVersionToken)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_apvtoken_statusid_core");
        });

        modelBuilder.Entity<HCCoreBinNumber>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.BankId, "FK_CoreBinNo_BankId_CoreCommon_idx");

            entity.HasIndex(e => e.BrandId, "FK_CoreBinNo_BrandId_CoreCommon_idx");

            entity.HasIndex(e => e.StatusId, "FK_CoreBinNo_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.SubBrandId, "FK_CoreBinNo_SubBrandId_CoreCommon_idx");

            entity.HasIndex(e => e.TypeId, "FK_CoreBinNo_TypeId_CoreCommon_idx");

            entity.HasIndex(e => e.CountryId, "FK_CoreCountryId_Country_idx");

            entity.Property(e => e.Bin)
                .HasMaxLength(64)
                .UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");
            entity.Property(e => e.BinEnd)
                .HasMaxLength(64)
                .UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");
            entity.Property(e => e.BinNumber)
                .HasMaxLength(128)
                .UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Bank).WithMany(p => p.HCCoreBinNumberBank)
                .HasForeignKey(d => d.BankId)
                .HasConstraintName("FK_CoreBinNo_BankId_CoreCommon");

            entity.HasOne(d => d.Brand).WithMany(p => p.HCCoreBinNumberBrand)
                .HasForeignKey(d => d.BrandId)
                .HasConstraintName("FK_CoreBinNo_BrandId_CoreCommon");

            entity.HasOne(d => d.Country).WithMany(p => p.HCCoreBinNumber)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("FK_CoreCountryId_Country");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreBinNumber)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreBinNo_StatusId_CoreHelper");

            entity.HasOne(d => d.SubBrand).WithMany(p => p.HCCoreBinNumberSubBrand)
                .HasForeignKey(d => d.SubBrandId)
                .HasConstraintName("FK_CoreBinNo_SubBrandId_CoreCommon");

            entity.HasOne(d => d.Type).WithMany(p => p.HCCoreBinNumberType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK_CoreBinNo_TypeId_CoreCommon");
        });

        modelBuilder.Entity<HCCoreCommon>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CountryId, "FK_CoreCommon_CountryId_Country_idx");

            entity.HasIndex(e => e.CreatedById, "FK_CoreCommon_CreatedBy_UserAccount_idx");

            entity.HasIndex(e => e.IconStorageId, "FK_CoreCommon_IconStorageId_Storage_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_CoreCommon_ModifyBy_UserAccount_Id_idx");

            entity.HasIndex(e => e.ParentId, "FK_CoreCommon_ParentId_CoreCommon_Id_idx");

            entity.HasIndex(e => e.PosterStorageId, "FK_CoreCommon_PosterStorageId_Storage_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_CoreCommon_StatusId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.SubParentId, "FK_CoreCommon_SubParentId_CoreCommon_Id_idx");

            entity.HasIndex(e => e.SubTypeId, "FK_CoreCommon_SubTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.TypeId, "FK_CoreCommon_TypeId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.AccountId, "FK_CoreCommon_UserAccountId_UserAccount_Id_idx");

            entity.HasIndex(e => e.HelperId, "Fk_CoreCommon_HelperId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SubValue).HasMaxLength(2048);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(256);
            entity.Property(e => e.Value).HasMaxLength(2048);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreCommonAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_CoreCommon_UserAccountId_UserAccount_Id");

            entity.HasOne(d => d.Country).WithMany(p => p.HCCoreCommon)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("FK_CoreCommon_CountryId_Country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreCommonCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreCommon_CreatedBy_UserAccount");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCCoreCommonHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("Fk_CoreCommon_HelperId_CoreHelper_Id");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.HCCoreCommonIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("FK_CoreCommon_IconStorageId_Storage_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreCommonModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_CoreCommon_ModifyBy_UserAccount_Id");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_CoreCommon_ParentId_CoreCommon_Id");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.HCCoreCommonPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("FK_CoreCommon_PosterStorageId_Storage_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreCommonStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreCommon_StatusId_CoreHelper_Id");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_CoreCommon_SubParentId_CoreCommon_Id");

            entity.HasOne(d => d.SubType).WithMany(p => p.HCCoreCommonSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("FK_CoreCommon_SubTypeId_CoreHelper");

            entity.HasOne(d => d.Type).WithMany(p => p.HCCoreCommonType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreCommon_TypeId_CoreHelper_Id");
        });

        modelBuilder.Entity<HCCoreConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountTypeId, "fk_config_acctypeid_core_idx");

            entity.HasIndex(e => e.CreatedById, "fk_config_createdbyid_account_idx");

            entity.HasIndex(e => e.DataTypeId, "fk_config_datatypeid_core_idx");

            entity.HasIndex(e => e.ModifyById, "fk_config_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_config_statusid_core_idx");

            entity.HasIndex(e => e.ValueHelperId, "fk_config_valuehelperid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.SystemName).HasMaxLength(128);
            entity.Property(e => e.Value).HasMaxLength(1024);

            entity.HasOne(d => d.AccountType).WithMany(p => p.HCCoreConfigurationAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .HasConstraintName("fk_config_acctypeid_core");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreConfigurationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_config_createdbyid_account");

            entity.HasOne(d => d.DataType).WithMany(p => p.HCCoreConfigurationDataType)
                .HasForeignKey(d => d.DataTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_config_datatypeid_core");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreConfigurationModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_config_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreConfigurationStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_config_statusid_core");

            entity.HasOne(d => d.ValueHelper).WithMany(p => p.HCCoreConfigurationValueHelper)
                .HasForeignKey(d => d.ValueHelperId)
                .HasConstraintName("fk_config_valuehelperid_core");
        });

        modelBuilder.Entity<HCCoreConfigurationHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.ConfigurationId, "fk_confighistory_configid_config_idx");

            entity.HasIndex(e => e.CreatedById, "fk_confighistory_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_confighistory_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_confighistory_statusid_core_idx");

            entity.HasIndex(e => e.ValueHelperId, "fk_confighistory_valuehelper_core_idx");

            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Value).HasMaxLength(1024);

            entity.HasOne(d => d.Configuration).WithMany(p => p.HCCoreConfigurationHistory)
                .HasForeignKey(d => d.ConfigurationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_confighistory_configid_config");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreConfigurationHistoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_confighistory_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreConfigurationHistoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_confighistory_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreConfigurationHistoryStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_confighistory_statusid_core");

            entity.HasOne(d => d.ValueHelper).WithMany(p => p.HCCoreConfigurationHistoryValueHelper)
                .HasForeignKey(d => d.ValueHelperId)
                .HasConstraintName("fk_confighistory_valuehelper_core");
        });

        modelBuilder.Entity<HCCoreCountry>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CreatedById, "fk_country_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_country_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_country_statusid_core_idx");

            entity.Property(e => e.CapitalName).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.CurrencyHex).HasMaxLength(32);
            entity.Property(e => e.CurrencyName).HasMaxLength(32);
            entity.Property(e => e.CurrencyNotation).HasMaxLength(32);
            entity.Property(e => e.CurrencySymbol).HasMaxLength(32);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Isd).HasMaxLength(8);
            entity.Property(e => e.Iso).HasMaxLength(8);
            entity.Property(e => e.MobileNumberLength).HasDefaultValueSql("'10'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(128);
            entity.Property(e => e.TimeZoneName).HasMaxLength(128);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreCountryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_country_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreCountryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_country_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreCountry)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_country_statusid_core");
        });

        modelBuilder.Entity<HCCoreCountryState>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CountryId, "fk_state_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_state_createdbyid_account_id_idx");

            entity.HasIndex(e => e.ModifyById, "fk_state_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_state_statusid_core_id_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsoCode).HasMaxLength(8);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.OIdName).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.Country).WithMany(p => p.HCCoreCountryState)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_state_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreCountryStateCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_state_createdbyid_account_id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreCountryStateModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_state_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreCountryState)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_state_statusid_core_id");
        });

        modelBuilder.Entity<HCCoreCountryStateCity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_city_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_city_modifybyid_account_idx");

            entity.HasIndex(e => e.StateId, "fk_city_stateid_state_idx");

            entity.HasIndex(e => e.StatusId, "fk_city_statusid_status_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsoCode).HasMaxLength(8);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.OldName).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreCountryStateCityCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_city_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreCountryStateCityModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_city_modifybyid_account");

            entity.HasOne(d => d.State).WithMany(p => p.HCCoreCountryStateCity)
                .HasForeignKey(d => d.StateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_city_stateid_state");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreCountryStateCity)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_city_statusid_status");
        });

        modelBuilder.Entity<HCCoreCountryStateCityArea>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CityId, "fk_cityarea_cityid_city_idx");

            entity.HasIndex(e => e.CreatedById, "fk_cityarea_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_cityarea_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_cityarea_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.City).WithMany(p => p.HCCoreCountryStateCityArea)
                .HasForeignKey(d => d.CityId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cityarea_cityid_city");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreCountryStateCityAreaCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cityarea_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreCountryStateCityAreaModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_cityarea_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreCountryStateCityArea)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cityarea_statusid_core");
        });

        modelBuilder.Entity<HCCoreFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.ParentId, "FK_CoreFeature_ParentId_CoreFeature_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_CoreFeature_StatusId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.TypeId, "FK_CoreFeature_TypeId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.HelperId, "Fk_CoreFeature_HelperId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.Add).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Delete).HasMaxLength(256);
            entity.Property(e => e.Edit).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(256);
            entity.Property(e => e.View).HasMaxLength(256);
            entity.Property(e => e.ViewAll).HasMaxLength(256);
            entity.Property(e => e.isTab).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCCoreFeatureHelper)
                .HasForeignKey(d => d.HelperId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Fk_CoreFeature_HelperId_CoreHelper_Id");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_CoreFeature_ParentId_CoreFeature_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreFeatureStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreFeature_StatusId_CoreHelper_Id");

            entity.HasOne(d => d.Type).WithMany(p => p.HCCoreFeatureType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreFeature_TypeId_CoreHelper_Id");
        });

        modelBuilder.Entity<HCCoreParameter>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CommonId, "FK_CoreParameter_CommonId_CoreCommon_idx");

            entity.HasIndex(e => e.CreatedById, "FK_CoreParameter_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.HelperId, "FK_CoreParameter_HelperId_CoreHelper_idx");

            entity.HasIndex(e => e.IconStorageId, "FK_CoreParameter_IconStorageId_Storage_idx");

            entity.HasIndex(e => e.ModifyById, "FK_CoreParameter_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.ParentId, "FK_CoreParameter_ParentId_CoreParameter_idx");

            entity.HasIndex(e => e.PosterStorageId, "FK_CoreParameter_PosterStorageId_Storage_idx");

            entity.HasIndex(e => e.StatusId, "FK_CoreParameter_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.SubCommonId, "FK_CoreParameter_SubCommonId_CoreCommon_idx");

            entity.HasIndex(e => e.SubParentId, "FK_CoreParameter_SubParentId_CoreParameter_idx");

            entity.HasIndex(e => e.SubTypeId, "FK_CoreParameter_SubTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.TypeId, "FK_CoreParameter_TypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_CoreParameter_UserAccountId_UserAccount_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SubValue).HasMaxLength(2048);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(256);
            entity.Property(e => e.Value).HasMaxLength(2048);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreParameterAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_CoreParameter_UserAccountId_UserAccount");

            entity.HasOne(d => d.Common).WithMany(p => p.HCCoreParameterCommon)
                .HasForeignKey(d => d.CommonId)
                .HasConstraintName("FK_CoreParameter_CommonId_CoreCommon");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreParameterCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreParameter_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCCoreParameterHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("FK_CoreParameter_HelperId_CoreHelper");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.HCCoreParameterIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("FK_CoreParameter_IconStorageId_Storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreParameterModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_CoreParameter_ModifyById_UserAccount");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_CoreParameter_ParentId_CoreParameter");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.HCCoreParameterPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("FK_CoreParameter_PosterStorageId_Storage");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreParameterStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreParameter_StatusId_CoreHelper");

            entity.HasOne(d => d.SubCommon).WithMany(p => p.HCCoreParameterSubCommon)
                .HasForeignKey(d => d.SubCommonId)
                .HasConstraintName("FK_CoreParameter_SubCommonId_CoreCommon");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_CoreParameter_SubParentId_CoreParameter");

            entity.HasOne(d => d.SubType).WithMany(p => p.HCCoreParameterSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("FK_CoreParameter_SubTypeId_CoreHelper");

            entity.HasOne(d => d.Type).WithMany(p => p.HCCoreParameterType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK_CoreParameter_TypeId_CoreHelper");
        });

        modelBuilder.Entity<HCCoreRole>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CommonId, "FK_CoreRole_CommonId_CoreCommon_idx");

            entity.HasIndex(e => e.CreatedById, "FK_CoreRole_CreatedById_UserAccount_Id");

            entity.HasIndex(e => e.HelperId, "FK_CoreRole_HelperId_CoreHelper_idx");

            entity.HasIndex(e => e.ModifyById, "FK_CoreRole_ModifyById_UserAccount");

            entity.HasIndex(e => e.StatusId, "FK_CoreRole_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(256);

            entity.HasOne(d => d.Common).WithMany(p => p.HCCoreRole)
                .HasForeignKey(d => d.CommonId)
                .HasConstraintName("FK_CoreRole_CommonId_CoreCommon");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreRoleCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreRole_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCCoreRoleHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("FK_CoreRole_HelperId_CoreHelper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreRoleModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_CoreRole_ModifyById_UserAccount");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreRoleStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreRole_StatusId_CoreHelper");
        });

        modelBuilder.Entity<HCCoreRoleFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.FeatureId, "FK_CoreRoleFeature_FeatureId_CoreFeature_idx");

            entity.HasIndex(e => e.RoleId, "FK_CoreRoleFeature_RoleId_CoreRole_idx");

            entity.HasIndex(e => e.StatusId, "FK_CoreRoleFeature_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.isAdd).HasDefaultValueSql("'0'");
            entity.Property(e => e.isDelete).HasDefaultValueSql("'0'");
            entity.Property(e => e.isUpdate).HasDefaultValueSql("'0'");
            entity.Property(e => e.isView).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Feature).WithMany(p => p.HCCoreRoleFeature)
                .HasForeignKey(d => d.FeatureId)
                .HasConstraintName("FK_CoreRoleFeature_FeatureId_CoreFeature");

            entity.HasOne(d => d.Role).WithMany(p => p.HCCoreRoleFeature)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK_CoreRoleFeature_RoleId_CoreRole");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreRoleFeature)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreRoleFeature_StatusId_CoreHelper");
        });

        modelBuilder.Entity<HCCoreStorage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.FolderId, "FK_HCST_FolderId_idx");

            entity.HasIndex(e => e.HelperId, "FK_HCST_HelperId_CoreHelper_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCST_StatusId_Core_idx");

            entity.HasIndex(e => e.SourceId, "FK_HCST_StorageSource_CoreHelper_idx");

            entity.HasIndex(e => e.StorageTypeId, "FK_HCST_StorageTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_HCST_UserAccountId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Extension)
                .HasMaxLength(50)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.Lasrge).HasMaxLength(1024);
            entity.Property(e => e.Medium).HasMaxLength(1024);
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.Path)
                .HasMaxLength(1024)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.Small).HasMaxLength(1024);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.ThumbnailPath).HasMaxLength(1024);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreStorage)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_HCST_UserAccountId_HCUserAccount_Id");

            entity.HasOne(d => d.Folder).WithMany(p => p.HCCoreStorage)
                .HasForeignKey(d => d.FolderId)
                .HasConstraintName("FK_HCST_FolderId");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCCoreStorageHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("FK_HCST_HelperId_CoreHelper");

            entity.HasOne(d => d.Source).WithMany(p => p.HCCoreStorageSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCST_StorageSource_CoreHelper");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreStorageStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCST_StatusId_Core");

            entity.HasOne(d => d.StorageType).WithMany(p => p.HCCoreStorageStorageType)
                .HasForeignKey(d => d.StorageTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCST_StorageTypeId_CoreHelper");
        });

        modelBuilder.Entity<HCCoreStorageFolder>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_storagefolder_accountid_idx");

            entity.HasIndex(e => e.CreatedById, "fk_storagefolder_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_storagefolder_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_storagefolder_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.Path).HasMaxLength(512);

            entity.HasOne(d => d.Account).WithMany(p => p.HCCoreStorageFolderAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_storagefolder_accountid");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreStorageFolderCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_storagefolder_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreStorageFolderModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_storagefolder_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreStorageFolder)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_storagefolder_statusid_core");
        });

        modelBuilder.Entity<HCCoreStorageTag>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_storagetag_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_storagetag_modifybyid_account_idx");

            entity.HasIndex(e => e.StorageId, "fk_storagetag_storageid_storage_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Key).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Value).HasMaxLength(256);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCCoreStorageTagCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_storagetag_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCCoreStorageTagModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_storagetag_modifybyid_account");

            entity.HasOne(d => d.Storage).WithMany(p => p.HCCoreStorageTag)
                .HasForeignKey(d => d.StorageId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_storagetag_storageid_storage");
        });

        modelBuilder.Entity<HCCoreVerification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.StatusId, "FK_HCSV_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.VerificationTypeId, "FK_HCSV_TypeId_CoreHelper_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.Property(e => e.AccessCode).HasMaxLength(45);
            entity.Property(e => e.AccessCodeStart).HasMaxLength(45);
            entity.Property(e => e.AccessKey).HasMaxLength(45);
            entity.Property(e => e.CountryIsd).HasMaxLength(10);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.EmailMessage).HasMaxLength(1024);
            entity.Property(e => e.ExpiaryDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.MobileMessage).HasMaxLength(256);
            entity.Property(e => e.MobileNumber).HasMaxLength(45);
            entity.Property(e => e.ReferenceKey).HasMaxLength(256);
            entity.Property(e => e.RequestIpAddress).HasMaxLength(64);
            entity.Property(e => e.RequestLocation).HasMaxLength(512);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.VerifyAddress).HasMaxLength(512);
            entity.Property(e => e.VerifyDate).HasColumnType("datetime");
            entity.Property(e => e.VerifyIpAddress).HasMaxLength(64);

            entity.HasOne(d => d.Status).WithMany(p => p.HCCoreVerificationStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCSV_StatusId_CoreHelper");

            entity.HasOne(d => d.VerificationType).WithMany(p => p.HCCoreVerificationVerificationType)
                .HasForeignKey(d => d.VerificationTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCSV_TypeId_CoreHelper");
        });

        modelBuilder.Entity<HCDbEvent>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.StartTime).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(64);
        });

        modelBuilder.Entity<HCSubscription>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountTypeId, "fk_subscription_accounttypeid_helper_idx");

            entity.HasIndex(e => e.CountryId, "fk_subscription_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_subscription_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_subscription_modifybyid_idx");

            entity.HasIndex(e => e.StatusId, "fk_subscription_statusid_helper_idx");

            entity.HasIndex(e => e.TypeId, "fk_subscription_typeid_helper_idx");

            entity.Property(e => e.CountryId).HasDefaultValueSql("'1'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.AccountType).WithMany(p => p.HCSubscriptionAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscription_accounttypeid_helper");

            entity.HasOne(d => d.Country).WithMany(p => p.HCSubscription)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscription_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCSubscriptionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscription_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCSubscriptionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_subscription_modifybyid");

            entity.HasOne(d => d.Status).WithMany(p => p.HCSubscriptionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscription_statusid_helper");

            entity.HasOne(d => d.Type).WithMany(p => p.HCSubscriptionType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscription_typeid_helper");
        });

        modelBuilder.Entity<HCSubscriptionFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.CreatedById, "fk_subscfeature_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_subscfeature_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_subscfeature_statusid_helper_idx");

            entity.HasIndex(e => e.SubscriptionId, "fk_subscfeature_subscriptionid_subscription_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MaximumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MaximumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ShortDescription).HasMaxLength(256);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCSubscriptionFeatureCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfeature_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCSubscriptionFeatureModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_subscfeature_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCSubscriptionFeature)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfeature_statusid_helper");

            entity.HasOne(d => d.Subscription).WithMany(p => p.HCSubscriptionFeature)
                .HasForeignKey(d => d.SubscriptionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfeature_subscriptionid_subscription");
        });

        modelBuilder.Entity<HCSubscriptionFeatureItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.CreatedById, "fk_subscfitem_createdbyid_account_idx");

            entity.HasIndex(e => e.SubscriptionFeatureId, "fk_subscfitem_featureid_subscfeatureid_idx");

            entity.HasIndex(e => e.ModifyById, "fk_subscfitem_modifybyid_accountid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_subscfitem_statusid_helper_idx");

            entity.HasIndex(e => e.SystemFeatureId, "fk_subscfitem_sysfeatureid_common_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MaximumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MaximumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCSubscriptionFeatureItemCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfitem_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCSubscriptionFeatureItemModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_subscfitem_modifybyid_accountid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCSubscriptionFeatureItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfitem_statusid_helper");

            entity.HasOne(d => d.SubscriptionFeature).WithMany(p => p.HCSubscriptionFeatureItem)
                .HasForeignKey(d => d.SubscriptionFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfitem_featureid_subscfeatureid");

            entity.HasOne(d => d.SystemFeature).WithMany(p => p.HCSubscriptionFeatureItem)
                .HasForeignKey(d => d.SystemFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subscfitem_sysfeatureid_common");
        });

        modelBuilder.Entity<HCToken>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_token_accountid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_token_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.ExpiaryDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastRequestDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.HCToken)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_token_accountid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCToken)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_token_statusid_core");
        });

        modelBuilder.Entity<HCTransaction>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_tr_createdbyid_useraccount_idx");

            entity.HasIndex(e => e.InvoiceItemId, "fk_tr_invoiceitemid_invoice_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tr_modifybyid_useraccount_idx");

            entity.HasIndex(e => e.UserAccountId, "fk_tr_useraccountud_useraccount_idx");

            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.StatusMessage).HasMaxLength(128);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCTransactionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tr_createdbyid_useraccount");

            entity.HasOne(d => d.InvoiceItem).WithMany(p => p.HCTransaction)
                .HasForeignKey(d => d.InvoiceItemId)
                .HasConstraintName("fk_tr_invoiceitemid_invoice");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCTransactionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tr_modifybyid_useraccount");

            entity.HasOne(d => d.UserAccount).WithMany(p => p.HCTransactionUserAccount)
                .HasForeignKey(d => d.UserAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tr_useraccountud_useraccount");
        });

        modelBuilder.Entity<HCTransactionParameter>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.BankId, "fk_trp_bankid_useraccount_idx");

            entity.HasIndex(e => e.CampaignId, "fk_trp_campaignid_campaign_idx");

            entity.HasIndex(e => e.CashierId, "fk_trp_cashierid_helper_idx");

            entity.HasIndex(e => e.CreatedById, "fk_trp_createdbyid_useraccount_idx");

            entity.HasIndex(e => e.CustomerId, "fk_trp_customerid_useraccount_idx");

            entity.HasIndex(e => e.MerchantId, "fk_trp_merchantid_useraccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_trp_modifybyid_useraccount_idx");

            entity.HasIndex(e => e.ProviderId, "fk_trp_providerid_useraccount_idx");

            entity.HasIndex(e => e.StoreId, "fk_trp_storeid_useraccount_idx");

            entity.HasIndex(e => e.TerminalId, "fk_trp_terminalid_useraccount_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(64);
            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.RequestKey).HasMaxLength(64);
            entity.Property(e => e.StatusMessage).HasMaxLength(128);
            entity.Property(e => e.TCode).HasMaxLength(16);
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Bank).WithMany(p => p.HCTransactionParameterBank)
                .HasForeignKey(d => d.BankId)
                .HasConstraintName("fk_trp_bankid_useraccount");

            entity.HasOne(d => d.Campaign).WithMany(p => p.HCTransactionParameter)
                .HasForeignKey(d => d.CampaignId)
                .HasConstraintName("fk_trp_campaignid_campaign");

            entity.HasOne(d => d.Cashier).WithMany(p => p.HCTransactionParameterCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_trp_cashierid_useraccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCTransactionParameterCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_trp_createdbyid_useraccount");

            entity.HasOne(d => d.Customer).WithMany(p => p.HCTransactionParameterCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("fk_trp_customerid_useraccount");

            entity.HasOne(d => d.Merchant).WithMany(p => p.HCTransactionParameterMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_trp_merchantid_useraccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCTransactionParameterModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_trp_modifybyid_useraccount");

            entity.HasOne(d => d.Provider).WithMany(p => p.HCTransactionParameterProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("fk_trp_providerid_useraccount");

            entity.HasOne(d => d.Store).WithMany(p => p.HCTransactionParameterStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_trp_storeid_useraccount");

            entity.HasOne(d => d.Terminal).WithMany(p => p.HCTransactionParameterTerminal)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_trp_terminalid_useraccount");
        });

        modelBuilder.Entity<HCUAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.AccountLevel, "FK_HCUA_AccountLevel_Helper_Id_idx");

            entity.HasIndex(e => e.AccountOperationTypeId, "FK_HCUA_AccountOperationTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountTypeId, "FK_HCUA_AccountTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.ActivityStatusId, "FK_HCUA_ActStatusId_Helper_idx");

            entity.HasIndex(e => e.AddressId, "FK_HCUA_AddressId_CoreAddress_idx");

            entity.HasIndex(e => e.TimeZoneId, "FK_HCUA_AddressId_TzId_idx");

            entity.HasIndex(e => e.AppVersionId, "FK_HCUA_AppVersionId_CoreCommon_idx");

            entity.HasIndex(e => e.ApplicationStatusId, "FK_HCUA_ApplicationStatusId_CoreHelper_idx");

            entity.HasIndex(e => e.BankId, "FK_HCUA_BankId_HCUserAccount_idx");

            entity.HasIndex(e => e.CardId, "FK_HCUA_CardId_TUCard_Id_idx");

            entity.HasIndex(e => e.CityAreaId, "FK_HCUA_CityAreaId_CityArea_idx");

            entity.HasIndex(e => e.CityId, "FK_HCUA_CityId_City_idx");

            entity.HasIndex(e => e.CountryId, "FK_HCUA_CountryId_Country_idx");

            entity.HasIndex(e => e.CreatedById, "FK_HCUA_CreatedBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.GenderId, "FK_HCUA_GenderId_Helper_idx");

            entity.HasIndex(e => e.IconStorageId, "FK_HCUA_IconStorageId_HCStorage_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_HCUA_ModifyBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.OwnerId, "FK_HCUA_OwnerId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.PosterStorageId, "FK_HCUA_PosterStorageId_HCStorage_Id_idx");

            entity.HasIndex(e => e.PrimaryCategoryId, "FK_HCUA_PriCat_Cat_idx");

            entity.HasIndex(e => e.RegistrationSourceId, "FK_HCUA_RegSource_Core_idx");

            entity.HasIndex(e => e.RoleId, "FK_HCUA_RoleId_CoreRole_Id_idx");

            entity.HasIndex(e => e.SecondaryCategoryId, "FK_HCUA_SecCat_Cat_idx");

            entity.HasIndex(e => e.StateId, "FK_HCUA_StateId_State_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCUA_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.SubOwnerId, "FK_HCUA_SubOwnerId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.SubscriptionId, "FK_HCUA_SubscriptionId_Sub_idx");

            entity.HasIndex(e => e.UserId, "FK_HCUA_UserId_HCUser_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreateDate, "IN_CreateDate");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.AccessPin).HasMaxLength(256);
            entity.Property(e => e.AccountCode).HasMaxLength(45);
            entity.Property(e => e.Address).HasMaxLength(256);
            entity.Property(e => e.ContactNumber)
                .HasMaxLength(64)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.CpContactNumber).HasMaxLength(64);
            entity.Property(e => e.CpEmailAddress).HasMaxLength(512);
            entity.Property(e => e.CpFirstName).HasMaxLength(128);
            entity.Property(e => e.CpLastName).HasMaxLength(128);
            entity.Property(e => e.CpMobileNumber).HasMaxLength(64);
            entity.Property(e => e.CpName).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Date).HasColumnType("datetime");
            entity.Property(e => e.DateOfBirth).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.DisplayName).HasMaxLength(30);
            entity.Property(e => e.DocumentVerificationStatusDate).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(500)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.EmailVerificationStatusDate).HasColumnType("datetime");
            entity.Property(e => e.FirstName)
                .HasMaxLength(250)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.IsTempPin).HasDefaultValueSql("'0'");
            entity.Property(e => e.LastActivityDate).HasColumnType("datetime");
            entity.Property(e => e.LastLoginDate).HasColumnType("datetime");
            entity.Property(e => e.LastName)
                .HasMaxLength(250)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.LastTransactionDate).HasColumnType("datetime");
            entity.Property(e => e.MiddleName).HasMaxLength(128);
            entity.Property(e => e.MobileNumber).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasMaxLength(6);
            entity.Property(e => e.Name)
                .HasMaxLength(512)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.NumberVerificationStatusDate).HasColumnType("datetime");
            entity.Property(e => e.OperatorID).HasMaxLength(45);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(256);
            entity.Property(e => e.ReferralCode)
                .HasMaxLength(250)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.ReferralUrl)
                .HasMaxLength(1024)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.ReferrerId).HasMaxLength(100);
            entity.Property(e => e.RequestKey).HasMaxLength(64);
            entity.Property(e => e.SecondaryEmailAddress).HasMaxLength(512);
            entity.Property(e => e.ShortCode).HasMaxLength(45);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.WebsiteUrl).HasMaxLength(1024);

            entity.HasOne(d => d.AccountLevelNavigation).WithMany(p => p.HCUAccountAccountLevelNavigation)
                .HasForeignKey(d => d.AccountLevel)
                .HasConstraintName("FK_HCUA_AccountLevel_Helper_Id");

            entity.HasOne(d => d.AccountOperationType).WithMany(p => p.HCUAccountAccountOperationType)
                .HasForeignKey(d => d.AccountOperationTypeId)
                .HasConstraintName("FK_HCUA_AccountOperationTypeId_CoreHelper");

            entity.HasOne(d => d.AccountType).WithMany(p => p.HCUAccountAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUA_AccountTypeId_CoreHelper");

            entity.HasOne(d => d.ActivityStatus).WithMany(p => p.HCUAccountActivityStatus)
                .HasForeignKey(d => d.ActivityStatusId)
                .HasConstraintName("FK_HCUA_ActStatusId_Helper");

            entity.HasOne(d => d.AddressNavigation).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.AddressId)
                .HasConstraintName("FK_HCUA_AddressId_CoreAddress");

            entity.HasOne(d => d.AppVersion).WithMany(p => p.HCUAccountAppVersion)
                .HasForeignKey(d => d.AppVersionId)
                .HasConstraintName("FK_HCUA_AppVersionId_CoreCommon");

            entity.HasOne(d => d.ApplicationStatus).WithMany(p => p.HCUAccountApplicationStatus)
                .HasForeignKey(d => d.ApplicationStatusId)
                .HasConstraintName("FK_HCUA_ApplicationStatusId_CoreHelper");

            entity.HasOne(d => d.Bank).WithMany(p => p.InverseBank)
                .HasForeignKey(d => d.BankId)
                .HasConstraintName("FK_HCUA_BankId_HCUserAccount");

            entity.HasOne(d => d.Card).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.CardId)
                .HasConstraintName("FK_HCUA_CardId_TUCard_Id");

            entity.HasOne(d => d.CityArea).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.CityAreaId)
                .HasConstraintName("FK_HCUA_CityAreaId_CityArea");

            entity.HasOne(d => d.City).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("FK_HCUA_CityId_City");

            entity.HasOne(d => d.Country).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("FK_HCUA_CountryId_Country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.InverseCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_HCUA_CreatedBy_HCUserAccount_Id");

            entity.HasOne(d => d.Gender).WithMany(p => p.HCUAccountGender)
                .HasForeignKey(d => d.GenderId)
                .HasConstraintName("FK_HCUA_GenderId_Helper");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.HCUAccountIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("FK_HCUA_IconStorageId_HCStorage_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.InverseModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_HCUA_ModifyBy_HCUserAccount_Id");

            entity.HasOne(d => d.Owner).WithMany(p => p.InverseOwner)
                .HasForeignKey(d => d.OwnerId)
                .HasConstraintName("FK_HCUA_OwnerId_HCUserAccount_Id");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.HCUAccountPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("FK_HCUA_PosterStorageId_HCStorage_Id");

            entity.HasOne(d => d.PrimaryCategory).WithMany(p => p.HCUAccountPrimaryCategory)
                .HasForeignKey(d => d.PrimaryCategoryId)
                .HasConstraintName("FK_HCUA_PriCat_Cat");

            entity.HasOne(d => d.RegistrationSource).WithMany(p => p.HCUAccountRegistrationSource)
                .HasForeignKey(d => d.RegistrationSourceId)
                .HasConstraintName("FK_HCUA_RegSource_Core");

            entity.HasOne(d => d.Role).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK_HCUA_RoleId_CoreRole_Id");

            entity.HasOne(d => d.SecondaryCategory).WithMany(p => p.HCUAccountSecondaryCategory)
                .HasForeignKey(d => d.SecondaryCategoryId)
                .HasConstraintName("FK_HCUA_SecCat_Cat");

            entity.HasOne(d => d.State).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.StateId)
                .HasConstraintName("FK_HCUA_StateId_State");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUA_StatusId_CoreHelper");

            entity.HasOne(d => d.SubOwner).WithMany(p => p.InverseSubOwner)
                .HasForeignKey(d => d.SubOwnerId)
                .HasConstraintName("FK_HCUA_SubOwnerId_HCUserAccount_Id");

            entity.HasOne(d => d.Subscription).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.SubscriptionId)
                .HasConstraintName("FK_HCUA_SubscriptionId_Sub");

            entity.HasOne(d => d.TimeZone).WithMany(p => p.HCUAccountTimeZone)
                .HasForeignKey(d => d.TimeZoneId)
                .HasConstraintName("FK_HCUA_AddressId_TzId");

            entity.HasOne(d => d.User).WithMany(p => p.HCUAccount)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUA_UserId_HCUser_Id");
        });

        modelBuilder.Entity<HCUAccountActivity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.ActivityTypeId, "FK_HCUAA_ActivityTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.DeviceId, "FK_HCUAA_DeviceId_HCUserDevice_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCUAA_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.FeatureId, "FK_HCUAA_SystemPermissionId_HCSystemPermission_Id_idx");

            entity.HasIndex(e => e.AccountId, "FK_HCUAA_UserAccountId_HCUserAccount_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EntityName).HasMaxLength(256);
            entity.Property(e => e.FieldName).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.ReferenceKey).HasMaxLength(45);
            entity.Property(e => e.SubReferenceKey).HasMaxLength(45);

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountActivity)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_HCUAA_UserAccountId_HCUserAccount");

            entity.HasOne(d => d.ActivityType).WithMany(p => p.HCUAccountActivityActivityType)
                .HasForeignKey(d => d.ActivityTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUAA_ActivityTypeId_CoreHelper");

            entity.HasOne(d => d.Device).WithMany(p => p.HCUAccountActivity)
                .HasForeignKey(d => d.DeviceId)
                .HasConstraintName("FK_HCUAA_DeviceId_HCUserDevice_Id");

            entity.HasOne(d => d.Feature).WithMany(p => p.HCUAccountActivity)
                .HasForeignKey(d => d.FeatureId)
                .HasConstraintName("FK_HCUAA_SystemPermissionId_HCSystemPermission_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountActivityStatusNavigation)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUAA_StatusId_CoreHelper");
        });

        modelBuilder.Entity<HCUAccountAuth>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CreatedById, "FK_HCU_CreatedBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_HCU_ModifyBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCU_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasMaxLength(6);
            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.ModifyDate).HasMaxLength(6);
            entity.Property(e => e.OperatorID).HasMaxLength(45);
            entity.Property(e => e.Password)
                .HasMaxLength(500)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.SecondaryPassword).HasMaxLength(256);
            entity.Property(e => e.ShortCode).HasMaxLength(45);
            entity.Property(e => e.SystemPassword).HasMaxLength(500);
            entity.Property(e => e.Username)
                .HasMaxLength(500)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountAuthCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_HCU_CreatedBy_HCUserAccount_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountAuthModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_HCU_ModifyBy_HCUserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountAuth)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCU_StatusId_CoreHelper");
        });

        modelBuilder.Entity<HCUAccountBalance>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.ParentId, "FK_UAB_ParentId_UserAccount_idx");

            entity.HasIndex(e => e.ProgramId, "FK_UAB_ProgramId_Program_idx");

            entity.HasIndex(e => e.SourceId, "FK_UAB_SourceId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_UAB_StatusId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.AccountId, "FK_UAB_UserAccountId_UserAccount_Id_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreditTransaction).HasDefaultValueSql("'0'");
            entity.Property(e => e.DebitTransaction).HasDefaultValueSql("'0'");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.LastTransactionDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountBalanceAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UAB_UserAccountId_UserAccount_Id");

            entity.HasOne(d => d.Parent).WithMany(p => p.HCUAccountBalanceParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_UAB_ParentId_UserAccount");

            entity.HasOne(d => d.Program).WithMany(p => p.HCUAccountBalance)
                .HasForeignKey(d => d.ProgramId)
                .HasConstraintName("FK_UAB_ProgramId_Program");

            entity.HasOne(d => d.Source).WithMany(p => p.HCUAccountBalanceSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UAB_SourceId_CoreHelper_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountBalanceStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UAB_StatusId_CoreHelper_Id");
        });

        modelBuilder.Entity<HCUAccountBank>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_bank_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_bank_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bank_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_bank_statusid_helper_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(32);
            entity.Property(e => e.BankCode).HasMaxLength(32);
            entity.Property(e => e.BankName).HasMaxLength(128);
            entity.Property(e => e.BvnNumber).HasMaxLength(64);
            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsPrimary).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceCode).HasMaxLength(64);
            entity.Property(e => e.ReferenceKey).HasMaxLength(64);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountBankAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bank_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountBankCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bank_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountBankModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bank_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountBank)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bank_statusid_helper");
        });

        modelBuilder.Entity<HCUAccountConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_accconfig_accid_account_idx");

            entity.HasIndex(e => e.ConfigurationId, "fk_accconfig_configid_config_idx");

            entity.HasIndex(e => e.CreatedById, "fk_accconfig_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_accconfig_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_accconfig_statusid_core_idx");

            entity.HasIndex(e => e.ValueHelperId, "fk_accconfig_valuehelperid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.Value).HasMaxLength(1024);

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountConfigurationAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accconfig_accid_account");

            entity.HasOne(d => d.Configuration).WithMany(p => p.HCUAccountConfiguration)
                .HasForeignKey(d => d.ConfigurationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accconfig_configid_config");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountConfigurationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_accconfig_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountConfigurationModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_accconfig_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountConfigurationStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accconfig_statusid_core");

            entity.HasOne(d => d.ValueHelper).WithMany(p => p.HCUAccountConfigurationValueHelper)
                .HasForeignKey(d => d.ValueHelperId)
                .HasConstraintName("fk_accconfig_valuehelperid_core");
        });

        modelBuilder.Entity<HCUAccountConfigurationHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountConfigurationId, "fk_accconfigh_configid_accconfig_idx");

            entity.HasIndex(e => e.CreatedById, "fk_accconfigh_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_accconfigh_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_accconfigh_statusid_core_idx");

            entity.HasIndex(e => e.ValueHelperId, "fk_accconfigh_vhelperid_core_idx");

            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Value).HasMaxLength(1024);

            entity.HasOne(d => d.AccountConfiguration).WithMany(p => p.HCUAccountConfigurationHistory)
                .HasForeignKey(d => d.AccountConfigurationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accconfigh_configid_accconfig");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountConfigurationHistoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_accconfigh_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountConfigurationHistoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_accconfigh_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountConfigurationHistoryStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accconfigh_statusid_core");

            entity.HasOne(d => d.ValueHelper).WithMany(p => p.HCUAccountConfigurationHistoryValueHelper)
                .HasForeignKey(d => d.ValueHelperId)
                .HasConstraintName("fk_accconfigh_vhelperid_core");
        });

        modelBuilder.Entity<HCUAccountDevice>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CreatedById, "FK_HCUD_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.ModelId, "FK_HCUD_ModelId_HCDeviceModel_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_HCUD_ModifyById_UserAccount_Id_idx");

            entity.HasIndex(e => e.NetworkOperatorId, "FK_HCUD_NetworkOperatodId_CoreParameter_idx");

            entity.HasIndex(e => e.OsVersionId, "FK_HCUD_OsVersionId_HCOsVersion_Id_idx");

            entity.HasIndex(e => e.ResolutionId, "FK_HCUD_ResolutionId_FK_HCDeviceResoloution_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCUD_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.TypeId, "FK_HCUD_TypeId_Helper_idx");

            entity.HasIndex(e => e.AccountId, "FK_HCUD_UserAccountId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.NotificationUrl).HasMaxLength(4000);
            entity.Property(e => e.SerialNumber).HasMaxLength(1024);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountDeviceAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUD_UserAccountId_HCUserAccount_Id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountDeviceCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_HCUD_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Model).WithMany(p => p.HCUAccountDeviceModel)
                .HasForeignKey(d => d.ModelId)
                .HasConstraintName("FK_HCUD_ModelId_HCDeviceModel_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountDeviceModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_HCUD_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.NetworkOperator).WithMany(p => p.HCUAccountDeviceNetworkOperator)
                .HasForeignKey(d => d.NetworkOperatorId)
                .HasConstraintName("FK_HCUD_NetworkOperatodId_CoreParameter");

            entity.HasOne(d => d.OsVersion).WithMany(p => p.HCUAccountDeviceOsVersion)
                .HasForeignKey(d => d.OsVersionId)
                .HasConstraintName("FK_HCUD_OsVersionId_HCOsVersion_Id");

            entity.HasOne(d => d.Resolution).WithMany(p => p.HCUAccountDeviceResolution)
                .HasForeignKey(d => d.ResolutionId)
                .HasConstraintName("FK_HCUD_ResolutionId_FK_HCDeviceResoloution_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountDeviceStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUD_StatusId_CoreHelper");

            entity.HasOne(d => d.Type).WithMany(p => p.HCUAccountDeviceType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK_HCUD_TypeId_Helper");
        });

        modelBuilder.Entity<HCUAccountInvoice>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CreatedById, "FK_UserInvoice_CreatedById_UserAccount_idx");

            entity.HasIndex(e => e.ModifyById, "FK_UserInvoice_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.ParentId, "FK_UserInvoice_ParentId_UserInvoice_idx");

            entity.HasIndex(e => e.PaymentApproverId, "FK_UserInvoice_PaymentApproverId_UserAccount_idx");

            entity.HasIndex(e => e.PaymentModeId, "FK_UserInvoice_PaymentModeId_idx");

            entity.HasIndex(e => e.PaymentProofId, "FK_UserInvoice_PaymentProofId_Storage_idx");

            entity.HasIndex(e => e.StatusId, "FK_UserInvoice_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.TypeId, "FK_UserInvoice_TypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_UserInvoice_UserAccountId_UserAccount_idx");

            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.FromAddress).HasMaxLength(512);
            entity.Property(e => e.FromContactNumber).HasMaxLength(20);
            entity.Property(e => e.FromEmailAddress).HasMaxLength(256);
            entity.Property(e => e.FromFax).HasMaxLength(20);
            entity.Property(e => e.FromName).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InoviceDate).HasColumnType("datetime");
            entity.Property(e => e.InoviceNumber).HasMaxLength(45);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.PaymentDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentReference).HasMaxLength(256);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.ToAddress).HasMaxLength(512);
            entity.Property(e => e.ToContactNumber).HasMaxLength(20);
            entity.Property(e => e.ToEmailAddress).HasMaxLength(256);
            entity.Property(e => e.ToFax).HasMaxLength(20);
            entity.Property(e => e.ToName).HasMaxLength(256);

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountInvoiceAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_UserInvoice_UserAccountId_UserAccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountInvoiceCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_UserInvoice_CreatedById_UserAccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountInvoiceModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_UserInvoice_ModifyById_UserAccount");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_UserInvoice_ParentId_UserInvoice");

            entity.HasOne(d => d.PaymentApprover).WithMany(p => p.HCUAccountInvoicePaymentApprover)
                .HasForeignKey(d => d.PaymentApproverId)
                .HasConstraintName("FK_UserInvoice_PaymentApproverId_UserAccount");

            entity.HasOne(d => d.PaymentMode).WithMany(p => p.HCUAccountInvoicePaymentMode)
                .HasForeignKey(d => d.PaymentModeId)
                .HasConstraintName("FK_UserInvoice_PaymentModeId");

            entity.HasOne(d => d.PaymentProof).WithMany(p => p.HCUAccountInvoice)
                .HasForeignKey(d => d.PaymentProofId)
                .HasConstraintName("FK_UserInvoice_PaymentProofId_Storage");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountInvoiceStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserInvoice_StatusId_CoreHelper");

            entity.HasOne(d => d.Type).WithMany(p => p.HCUAccountInvoiceType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserInvoice_TypeId_CoreHelper");
        });

        modelBuilder.Entity<HCUAccountLocation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CityAreaId, "fk_userlocation_cityareaid_coreparameter_idx");

            entity.HasIndex(e => e.CityId, "fk_userlocation_cityid_coreparameter_idx");

            entity.HasIndex(e => e.CountryId, "fk_userlocation_countryid_corecommon_idx");

            entity.HasIndex(e => e.DeviceId, "fk_userlocation_deviceid_userdevice_idx");

            entity.HasIndex(e => e.RegionAreaId, "fk_userlocation_regionareaid_coreparameter_idx");

            entity.HasIndex(e => e.RegionId, "fk_userlocation_regionid_coreparameter_idx");

            entity.HasIndex(e => e.AccountId, "fk_userlocation_useraccountid_useraccount_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Location).HasMaxLength(512);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountLocation)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_userlocation_useraccountid_useraccount");

            entity.HasOne(d => d.CityArea).WithMany(p => p.HCUAccountLocationCityArea)
                .HasForeignKey(d => d.CityAreaId)
                .HasConstraintName("fk_userlocation_cityareaid_coreparameter");

            entity.HasOne(d => d.City).WithMany(p => p.HCUAccountLocationCity)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("fk_userlocation_cityid_coreparameter");

            entity.HasOne(d => d.Country).WithMany(p => p.HCUAccountLocation)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("fk_userlocation_countryid_corecommon");

            entity.HasOne(d => d.Device).WithMany(p => p.HCUAccountLocation)
                .HasForeignKey(d => d.DeviceId)
                .HasConstraintName("fk_userlocation_deviceid_userdevice");

            entity.HasOne(d => d.RegionArea).WithMany(p => p.HCUAccountLocationRegionArea)
                .HasForeignKey(d => d.RegionAreaId)
                .HasConstraintName("fk_userlocation_regionareaid_coreparameter");

            entity.HasOne(d => d.Region).WithMany(p => p.HCUAccountLocationRegion)
                .HasForeignKey(d => d.RegionId)
                .HasConstraintName("fk_userlocation_regionid_coreparameter");
        });

        modelBuilder.Entity<HCUAccountOwner>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountTypeId, "FK_HCUAO_AccountTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.CreatedById, "FK_HCUAO_CreatedBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_HCUAO_ModifyBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.OwnerId, "FK_HCUAO_OwnerId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCUAO_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_HCUAO_UserAccountId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountOwnerAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUAO_UserAccountId_HCUserAccount_Id");

            entity.HasOne(d => d.AccountType).WithMany(p => p.HCUAccountOwnerAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUAO_AccountTypeId_CoreHelper");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountOwnerCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_HCUAO_CreatedBy_HCUserAccount_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountOwnerModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_HCUAO_ModifyBy_HCUserAccount_Id");

            entity.HasOne(d => d.Owner).WithMany(p => p.HCUAccountOwnerOwner)
                .HasForeignKey(d => d.OwnerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCUAO_OwnerId_HCUserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountOwnerStatus)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_HCUAO_StatusId_CoreHelper");
        });

        modelBuilder.Entity<HCUAccountParameter>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CommonId, "FK_UserParameter_CommonId_CoreCommon_idx");

            entity.HasIndex(e => e.CreatedById, "FK_UserParameter_CreatedById_UserAccount_idx");

            entity.HasIndex(e => e.DeviceId, "FK_UserParameter_DeviceId_UserDevice_idx");

            entity.HasIndex(e => e.HelperId, "FK_UserParameter_HelperId_CoreHelper_idx");

            entity.HasIndex(e => e.IconStorageId, "FK_UserParameter_IconUrl_Storage_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_UserParameter_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.ParameterId, "FK_UserParameter_ParameterId_CoreParameter_idx");

            entity.HasIndex(e => e.ParentId, "FK_UserParameter_ParentId_UserParameter_idx");

            entity.HasIndex(e => e.PosterStorageId, "FK_UserParameter_PosterUrl_Storage_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_UserParameter_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.SubParentId, "FK_UserParameter_SubParentId_UserParameter_idx");

            entity.HasIndex(e => e.SubTypeId, "FK_UserParameter_SubTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.TransactionId, "FK_UserParameter_TransactionId_UserTransaction_idx");

            entity.HasIndex(e => e.TypeId, "FK_UserParameter_TypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_UserParameter_UserAccountId_UserAccount_idx");

            entity.HasIndex(e => e.VerificationId, "FK_UserParameter_VerificationId_CoreVerification_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(2048);
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.RequestKey).HasMaxLength(64);
            entity.Property(e => e.StartTime).HasColumnType("datetime");
            entity.Property(e => e.SubValue).HasMaxLength(2048);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(256);
            entity.Property(e => e.Value).HasMaxLength(2048);

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountParameterAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_UserParameter_UserAccountId_UserAccount");

            entity.HasOne(d => d.Common).WithMany(p => p.HCUAccountParameter)
                .HasForeignKey(d => d.CommonId)
                .HasConstraintName("FK_UserParameter_CommonId_CoreCommon");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountParameterCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_UserParameter_CreatedById_UserAccount");

            entity.HasOne(d => d.Device).WithMany(p => p.HCUAccountParameter)
                .HasForeignKey(d => d.DeviceId)
                .HasConstraintName("FK_UserParameter_DeviceId_UserDevice");

            entity.HasOne(d => d.Helper).WithMany(p => p.HCUAccountParameterHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("FK_UserParameter_HelperId_CoreHelper");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.HCUAccountParameterIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("FK_UserParameter_IconUrl_Storage_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountParameterModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_UserParameter_ModifyById_UserAccount");

            entity.HasOne(d => d.Parameter).WithMany(p => p.HCUAccountParameter)
                .HasForeignKey(d => d.ParameterId)
                .HasConstraintName("FK_UserParameter_ParameterId_CoreParameter");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_UserParameter_ParentId_UserParameter");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.HCUAccountParameterPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("FK_UserParameter_PosterUrl_Storage_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountParameterStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserParameter_StatusId_CoreHelper");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_UserParameter_SubParentId_UserParameter");

            entity.HasOne(d => d.SubType).WithMany(p => p.HCUAccountParameterSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("FK_UserParameter_SubTypeId_CoreHelper");

            entity.HasOne(d => d.Transaction).WithMany(p => p.HCUAccountParameter)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("FK_UserParameter_TransactionId_UserTransaction");

            entity.HasOne(d => d.Type).WithMany(p => p.HCUAccountParameterType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK_UserParameter_TypeId_CoreHelper");

            entity.HasOne(d => d.Verification).WithMany(p => p.HCUAccountParameter)
                .HasForeignKey(d => d.VerificationId)
                .HasConstraintName("FK_UserParameter_VerificationId_CoreVerification");
        });

        modelBuilder.Entity<HCUAccountSession>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.ModifyById, "FK_HCSUS_ModifyById_UserAccount_Id_idx");

            entity.HasIndex(e => e.PlatformId, "FK_HCSUS_PlatformId_Core_idx");

            entity.HasIndex(e => e.StatusId, "FK_HCSUS_StatusId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.AccountId, "FK_HCSUS_UserAccountId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.DeviceId, "FK_HCSUS_UserDeviceId_HCUserDevice_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");
            entity.Property(e => e.IPAddress).HasMaxLength(50);
            entity.Property(e => e.LastActivityDate).HasMaxLength(6);
            entity.Property(e => e.LoginDate).HasMaxLength(6);
            entity.Property(e => e.LogoutDate).HasMaxLength(6);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.NotificationUrl).HasMaxLength(2048);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountSessionAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_HCSUS_UserAccountId_HCUserAccount_Id");

            entity.HasOne(d => d.Device).WithMany(p => p.HCUAccountSession)
                .HasForeignKey(d => d.DeviceId)
                .HasConstraintName("FK_HCSUS_UserDeviceId_HCUserDevice_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountSessionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_HCSUS_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Platform).WithMany(p => p.HCUAccountSessionPlatform)
                .HasForeignKey(d => d.PlatformId)
                .HasConstraintName("FK_HCSUS_PlatformId_Core");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountSessionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCSUS_StatusId_CoreHelper_Id");
        });

        modelBuilder.Entity<HCUAccountSubscription>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountId, "fk_accsub_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_accsub_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_accsub_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_accsub_statusid_helper_idx");

            entity.HasIndex(e => e.SubscriptionId, "fk_accsub_subscriptionid_subs_idx");

            entity.HasIndex(e => e.TypeId, "fk_accsub_typeid_helper_idx");

            entity.Property(e => e.AuthorizationCode).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentReference).HasMaxLength(128);
            entity.Property(e => e.RenewDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountSubscriptionAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsub_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountSubscriptionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_accsub_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountSubscriptionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_accsub_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountSubscriptionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsub_statusid_helper");

            entity.HasOne(d => d.Subscription).WithMany(p => p.HCUAccountSubscription)
                .HasForeignKey(d => d.SubscriptionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsub_subscriptionid_subs");

            entity.HasOne(d => d.Type).WithMany(p => p.HCUAccountSubscriptionType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsub_typeid_helper");
        });

        modelBuilder.Entity<HCUAccountSubscriptionFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountSubscriptionId, "fk_accsubfeature_accsubfeid_accsubf_idx");

            entity.HasIndex(e => e.CreatedById, "fk_accsubfeature_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_accsubfeature_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_accsubfeature_statusid_helper_idx");

            entity.HasIndex(e => e.SubscriptionFeatureId, "fk_accsubfeature_subfid_subf_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MaximumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MaximumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.AccountSubscription).WithMany(p => p.HCUAccountSubscriptionFeature)
                .HasForeignKey(d => d.AccountSubscriptionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsubfeature_accsubfeid_accsubf");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountSubscriptionFeatureCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsubfeature_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountSubscriptionFeatureModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_accsubfeature_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountSubscriptionFeature)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsubfeature_statusid_helper");

            entity.HasOne(d => d.SubscriptionFeature).WithMany(p => p.HCUAccountSubscriptionFeature)
                .HasForeignKey(d => d.SubscriptionFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_accsubfeature_subfid_subf");
        });

        modelBuilder.Entity<HCUAccountSubscriptionFeatureItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountSubscriptionFeatureId, "fk_acc_su_f_item_asubfid_asubfitem_idx");

            entity.HasIndex(e => e.CreatedById, "fk_acc_su_f_item_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_acc_su_f_item_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_acc_su_f_item_statusid_helper_idx");

            entity.HasIndex(e => e.SubscriptionFeatureId, "fk_acc_su_f_item_subfid_sfitem_idx");

            entity.HasIndex(e => e.SystemFeatureId, "fk_acc_su_f_item_sysffid_common_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MaximumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MaximumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.MinimumLimit).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.AccountSubscriptionFeature).WithMany(p => p.HCUAccountSubscriptionFeatureItem)
                .HasForeignKey(d => d.AccountSubscriptionFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_acc_su_f_item_asubfid_asubfitem");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountSubscriptionFeatureItemCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_acc_su_f_item_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountSubscriptionFeatureItemModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_acc_su_f_item_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountSubscriptionFeatureItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_acc_su_f_item_statusid_helper");

            entity.HasOne(d => d.SubscriptionFeature).WithMany(p => p.HCUAccountSubscriptionFeatureItem)
                .HasForeignKey(d => d.SubscriptionFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_acc_su_f_item_subfid_sfitem");

            entity.HasOne(d => d.SystemFeature).WithMany(p => p.HCUAccountSubscriptionFeatureItem)
                .HasForeignKey(d => d.SystemFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_acc_su_f_item_sysffid_common");
        });

        modelBuilder.Entity<HCUAccountSubscriptionPayment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AccountId, "fk_subpayment_accountid_account_idx");

            entity.HasIndex(e => e.AccountSubscriptionId, "fk_subpayment_accsubid_accsub_idx");

            entity.HasIndex(e => e.CreatedById, "fk_subpayment_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_subpayment_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_subpayment_statusid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountSubscriptionPaymentAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subpayment_accountid_account");

            entity.HasOne(d => d.AccountSubscription).WithMany(p => p.HCUAccountSubscriptionPayment)
                .HasForeignKey(d => d.AccountSubscriptionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subpayment_accsubid_accsub");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountSubscriptionPaymentCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_subpayment_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountSubscriptionPaymentModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_subpayment_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountSubscriptionPayment)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_subpayment_statusid_helper");
        });

        modelBuilder.Entity<HCUAccountTransaction>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "HCTran_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.ModeId, "HCTran_ModeId_Core_idx");

            entity.HasIndex(e => e.ModifyById, "HCTran_ModifyById_UserAccount_Id_idx");

            entity.HasIndex(e => e.ParentId, "HCTran_ParentId_UserAccount_Id_idx");

            entity.HasIndex(e => e.ParentTransactionId, "HCTran_ParentTransactionId_Tran_Id_idx");

            entity.HasIndex(e => e.PaymentMethodId, "HCTran_PaymentMethodId_Core_idx");

            entity.HasIndex(e => e.ProgramId, "HCTran_ProgramId_Program_idx");

            entity.HasIndex(e => e.SourceId, "HCTran_SourceId_Core_idx");

            entity.HasIndex(e => e.StatusId, "HCTran_StatusId_Core_idx");

            entity.HasIndex(e => e.SubParentId, "HCTran_SubParentId_UserAccount_Id_idx");

            entity.HasIndex(e => e.TypeId, "HCTran_TypeId_Core_idx");

            entity.HasIndex(e => e.AccountId, "HCTran_UserAccountId_UserAccount_Id_idx");

            entity.HasIndex(e => e.BankId, "HC_Tran_AcquirerId_UserAccount_idx");

            entity.HasIndex(e => e.BinNumberId, "HC_Tran_BinNumberId_HCCoreBinNumber_idx");

            entity.HasIndex(e => e.CampaignId, "HC_Tran_CampaignId_Campaign_idx");

            entity.HasIndex(e => e.CardBankId, "HC_Tran_CardBankId_CoreCommon_idx");

            entity.HasIndex(e => e.CardBrandId, "HC_Tran_CardBrandId_CoreCommon_Id_idx");

            entity.HasIndex(e => e.CardSubBrandId, "HC_Tran_CardSubBrandId_CoreCommon_idx");

            entity.HasIndex(e => e.CardTypeId, "HC_Tran_CardTypeId_CoreCommon_idx");

            entity.HasIndex(e => e.CashierId, "HC_Tran_CashierId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.CustomerId, "HC_Tran_CustomerId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.ProviderId, "HC_Tran_ProviderId_HCUserAccount_idx");

            entity.HasIndex(e => e.TerminalId, "HC_Tran_TerminalId_Terminal_idx");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.TransactionDate, "trdate_ndx");

            entity.Property(e => e.AccountNumber).HasMaxLength(128);
            entity.Property(e => e.CashierReward).HasDefaultValueSql("'0'");
            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.GroupKey).HasMaxLength(45);
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.InoviceNumber).HasMaxLength(45);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.RequestKey).HasMaxLength(64);
            entity.Property(e => e.TCode).HasMaxLength(16);
            entity.Property(e => e.TransHash).HasMaxLength(150);
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.HCUAccountTransactionAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("HCTran_UserAccountId_UserAccount_Id");

            entity.HasOne(d => d.Bank).WithMany(p => p.HCUAccountTransactionBank)
                .HasForeignKey(d => d.BankId)
                .HasConstraintName("HC_Tran_BankId_AcquirerId_UserAccount");

            entity.HasOne(d => d.BinNumber).WithMany(p => p.HCUAccountTransaction)
                .HasForeignKey(d => d.BinNumberId)
                .HasConstraintName("HC_Tran_BinNumberId_HCCoreBinNumber");

            entity.HasOne(d => d.Campaign).WithMany(p => p.HCUAccountTransaction)
                .HasForeignKey(d => d.CampaignId)
                .HasConstraintName("HC_Tran_CampaignId_Campaign");

            entity.HasOne(d => d.CardBank).WithMany(p => p.HCUAccountTransactionCardBank)
                .HasForeignKey(d => d.CardBankId)
                .HasConstraintName("HC_Tran_CardBankId_CoreCommon");

            entity.HasOne(d => d.CardBrand).WithMany(p => p.HCUAccountTransactionCardBrand)
                .HasForeignKey(d => d.CardBrandId)
                .HasConstraintName("HC_Tran_CardBrandId_CoreCommon_Id");

            entity.HasOne(d => d.CardSubBrand).WithMany(p => p.HCUAccountTransactionCardSubBrand)
                .HasForeignKey(d => d.CardSubBrandId)
                .HasConstraintName("HC_Tran_CardSubBrandId_CoreCommon");

            entity.HasOne(d => d.CardType).WithMany(p => p.HCUAccountTransactionCardType)
                .HasForeignKey(d => d.CardTypeId)
                .HasConstraintName("HC_Tran_CardTypeId_CoreCommon");

            entity.HasOne(d => d.Cashier).WithMany(p => p.HCUAccountTransactionCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("HC_Tran_CashierId_HCUserAccount_Id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.HCUAccountTransactionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("HCTran_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Customer).WithMany(p => p.HCUAccountTransactionCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("HC_Tran_CustomerId_HCUserAccount_Id");

            entity.HasOne(d => d.Mode).WithMany(p => p.HCUAccountTransactionMode)
                .HasForeignKey(d => d.ModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HCTran_ModeId_Core");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.HCUAccountTransactionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("HCTran_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Parent).WithMany(p => p.HCUAccountTransactionParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("HCTran_ParentId_UserAccount_Id");

            entity.HasOne(d => d.ParentTransaction).WithMany(p => p.InverseParentTransaction)
                .HasForeignKey(d => d.ParentTransactionId)
                .HasConstraintName("HCTran_ParentTransactionId_Tran_Id");

            entity.HasOne(d => d.PaymentMethod).WithMany(p => p.HCUAccountTransactionPaymentMethod)
                .HasForeignKey(d => d.PaymentMethodId)
                .HasConstraintName("HCTran_PaymentMethodId_Core");

            entity.HasOne(d => d.Program).WithMany(p => p.HCUAccountTransaction)
                .HasForeignKey(d => d.ProgramId)
                .HasConstraintName("HCTran_ProgramId_Program");

            entity.HasOne(d => d.Provider).WithMany(p => p.HCUAccountTransactionProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("HC_Tran_ProviderId_HCUserAccount");

            entity.HasOne(d => d.Source).WithMany(p => p.HCUAccountTransactionSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HCTran_SourceId_Core");

            entity.HasOne(d => d.Status).WithMany(p => p.HCUAccountTransactionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HCTran_StatusId_Core");

            entity.HasOne(d => d.SubParent).WithMany(p => p.HCUAccountTransactionSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("HCTran_SubParentId_UserAccount_Id");

            entity.HasOne(d => d.Terminal).WithMany(p => p.HCUAccountTransaction)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("HC_Tran_TerminalId_Terminal");

            entity.HasOne(d => d.Type).WithMany(p => p.HCUAccountTransactionType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HCTran_TypeId_Core");
        });

        modelBuilder.Entity<HCoreCustomerObj>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasIndex(e => e.id, "id_UNIQUE").IsUnique();

            entity.Property(e => e.accountName).HasMaxLength(255);
            entity.Property(e => e.accountNumber).HasMaxLength(255);
            entity.Property(e => e.accountStatus).HasMaxLength(255);
            entity.Property(e => e.availableBalance).HasMaxLength(255);
            entity.Property(e => e.cbaCustomerID).HasMaxLength(255);
            entity.Property(e => e.emailAddress).HasMaxLength(50);
            entity.Property(e => e.phoneNumber).HasMaxLength(255);
            entity.Property(e => e.responseCode).HasMaxLength(255);
            entity.Property(e => e.responseMessage).HasMaxLength(255);
        });

        modelBuilder.Entity<LSCarriers>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_carrier_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_carrier_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_carrier_statusid_core_idx");

            entity.Property(e => e.CarrierId).HasMaxLength(45);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress).HasMaxLength(284);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IconUrl).HasMaxLength(300);
            entity.Property(e => e.MobileNumber).HasMaxLength(284);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(284);
            entity.Property(e => e.Slug).HasMaxLength(45);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.LSCarriersCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_carrier_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.LSCarriersModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_carrier_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.LSCarriers)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_carrier_statusid_core");
        });

        modelBuilder.Entity<LSItems>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.ParcelId, "fk_item_parcelid_parcel_idx");

            entity.Property(e => e.Currency).HasMaxLength(45);
            entity.Property(e => e.Description).HasMaxLength(300);
            entity.Property(e => e.Guid).HasMaxLength(80);
            entity.Property(e => e.Name).HasMaxLength(80);

            entity.HasOne(d => d.Parcel).WithMany(p => p.LSItems)
                .HasForeignKey(d => d.ParcelId)
                .HasConstraintName("fk_item_parcelid_parcel");
        });

        modelBuilder.Entity<LSOperations>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_operations_accid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_operations_dealid_deal_idx");

            entity.HasIndex(e => e.ShipmentId, "fk_operations_shipmentid_shipment_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.RateId).HasMaxLength(64);

            entity.HasOne(d => d.Account).WithMany(p => p.LSOperations)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_operations_accid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.LSOperations)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_operations_dealid_deal");

            entity.HasOne(d => d.Shipment).WithMany(p => p.LSOperations)
                .HasForeignKey(d => d.ShipmentId)
                .HasConstraintName("fk_operations_shipmentid_shipment");
        });

        modelBuilder.Entity<LSPackages>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_package_accountid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_package_createdbyid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_package_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_package_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_package_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(100);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.PackagingId).HasMaxLength(45);
            entity.Property(e => e.ProductName).HasMaxLength(264);
            entity.Property(e => e.SizeUnit).HasMaxLength(45);
            entity.Property(e => e.TId).HasMaxLength(80);
            entity.Property(e => e.Type).HasMaxLength(45);
            entity.Property(e => e.WeightUnit).HasMaxLength(45);

            entity.HasOne(d => d.Account).WithMany(p => p.LSPackagesAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_package_accountid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.LSPackagesCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_package_createdbyid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.LSPackages)
                .HasForeignKey(d => d.DealId)
                .HasConstraintName("fk_package_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.LSPackagesModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_package_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.LSPackages)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_package_statusid_core");
        });

        modelBuilder.Entity<LSParcel>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_parcel_accid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_parcel_createdbyid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_parcel_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_parcel_modifybyid_acc_idx");

            entity.HasIndex(e => e.PackagingId, "fk_parcel_packagingid_packaging_idx");

            entity.HasIndex(e => e.StatusId, "fk_parcel_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(300);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ParcelId).HasMaxLength(45);
            entity.Property(e => e.TId).HasMaxLength(80);
            entity.Property(e => e.WeightUnit).HasMaxLength(45);

            entity.HasOne(d => d.Account).WithMany(p => p.LSParcelAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_parcel_accid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.LSParcelCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_parcel_createdbyid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.LSParcel)
                .HasForeignKey(d => d.DealId)
                .HasConstraintName("fk_parcel_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.LSParcelModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_parcel_modifybyid_acc");

            entity.HasOne(d => d.Packaging).WithMany(p => p.LSParcel)
                .HasForeignKey(d => d.PackagingId)
                .HasConstraintName("fk_parcel_packagingid_packaging");

            entity.HasOne(d => d.Status).WithMany(p => p.LSParcel)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_parcel_statusid_core");
        });

        modelBuilder.Entity<LSShipmentRate>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CarrierId, "fk_rate_carrierid_carrier_idx");

            entity.HasIndex(e => e.ShipmentId, "fk_rate_shipmentid_shipment_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Currency).HasMaxLength(84);
            entity.Property(e => e.DeliveryAddress).HasMaxLength(84);
            entity.Property(e => e.DeliveryDate).HasColumnType("datetime");
            entity.Property(e => e.DeliveryTime).HasMaxLength(84);
            entity.Property(e => e.Guid).HasMaxLength(84);
            entity.Property(e => e.PickUpAddress).HasMaxLength(84);
            entity.Property(e => e.PickUpTime).HasMaxLength(84);
            entity.Property(e => e.RateId).HasMaxLength(84);

            entity.HasOne(d => d.Carrier).WithMany(p => p.LSShipmentRate)
                .HasForeignKey(d => d.CarrierId)
                .HasConstraintName("fk_rate_carrierid_carrier");

            entity.HasOne(d => d.Shipment).WithMany(p => p.LSShipmentRate)
                .HasForeignKey(d => d.ShipmentId)
                .HasConstraintName("fk_rate_shipmentid_shipment");
        });

        modelBuilder.Entity<LSShipments>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.BillId, "fk_shipment_billid_corestorage_idx");

            entity.HasIndex(e => e.CarrierId, "fk_shipment_carrierid_carrier_idx");

            entity.HasIndex(e => e.CreatedById, "fk_shipment_createdbyid_acc_idx");

            entity.HasIndex(e => e.CustomerId, "fk_shipment_customrtid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_shipment_dealid_deal_idx");

            entity.HasIndex(e => e.DeliveryPartnerId, "fk_shipment_deliverypartner_acc_idx");

            entity.HasIndex(e => e.FromAddressId, "fk_shipment_fromaddressid_address_idx");

            entity.HasIndex(e => e.MerchantId, "fk_shipment_merchantid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_shipment_modifybyid_acc_idx");

            entity.HasIndex(e => e.ParcelId, "fk_shipment_parcelid_parcel_idx");

            entity.HasIndex(e => e.ReturnAddressId, "fk_shipment_returnaddressid_address_idx");

            entity.HasIndex(e => e.StatusId, "fk_shipment_statusid_core_idx");

            entity.HasIndex(e => e.StoreId, "fk_shipment_storeid_acc_idx");

            entity.HasIndex(e => e.ToAddressId, "fk_shipment_toaddressid_address_idx");

            entity.Property(e => e.CancelPickUpUrl).HasMaxLength(300);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DeliveryDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.OrderReference).HasMaxLength(80);
            entity.Property(e => e.PickUpDate).HasColumnType("datetime");
            entity.Property(e => e.RateId).HasMaxLength(150);
            entity.Property(e => e.ShipmentId).HasMaxLength(45);
            entity.Property(e => e.TId).HasMaxLength(80);
            entity.Property(e => e.TrackingNumber).HasMaxLength(256);
            entity.Property(e => e.TrackingUrl).HasMaxLength(300);
            entity.Property(e => e.TransactionReference).HasMaxLength(84);
            entity.Property(e => e.UserId).HasMaxLength(45);

            entity.HasOne(d => d.Bill).WithMany(p => p.LSShipments)
                .HasForeignKey(d => d.BillId)
                .HasConstraintName("fk_shipment_billid_corestorage");

            entity.HasOne(d => d.Carrier).WithMany(p => p.LSShipments)
                .HasForeignKey(d => d.CarrierId)
                .HasConstraintName("fk_shipment_carrierid_carrier");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.LSShipmentsCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_shipment_createdbyid_acc");

            entity.HasOne(d => d.Customer).WithMany(p => p.LSShipmentsCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("fk_shipment_customrtid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.LSShipments)
                .HasForeignKey(d => d.DealId)
                .HasConstraintName("fk_shipment_dealid_deal");

            entity.HasOne(d => d.DeliveryPartner).WithMany(p => p.LSShipmentsDeliveryPartner)
                .HasForeignKey(d => d.DeliveryPartnerId)
                .HasConstraintName("fk_shipment_deliverypartner_acc");

            entity.HasOne(d => d.FromAddress).WithMany(p => p.LSShipmentsFromAddress)
                .HasForeignKey(d => d.FromAddressId)
                .HasConstraintName("fk_shipment_fromaddressid_address");

            entity.HasOne(d => d.Merchant).WithMany(p => p.LSShipmentsMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_shipment_merchantid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.LSShipmentsModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_shipment_modifybyid_acc");

            entity.HasOne(d => d.Parcel).WithMany(p => p.LSShipments)
                .HasForeignKey(d => d.ParcelId)
                .HasConstraintName("fk_shipment_parcelid_parcel");

            entity.HasOne(d => d.ReturnAddress).WithMany(p => p.LSShipmentsReturnAddress)
                .HasForeignKey(d => d.ReturnAddressId)
                .HasConstraintName("fk_shipment_returnaddressid_address");

            entity.HasOne(d => d.Status).WithMany(p => p.LSShipments)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_shipment_statusid_core");

            entity.HasOne(d => d.Store).WithMany(p => p.LSShipmentsStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_shipment_storeid_acc");

            entity.HasOne(d => d.ToAddress).WithMany(p => p.LSShipmentsToAddress)
                .HasForeignKey(d => d.ToAddressId)
                .HasConstraintName("fk_shipment_toaddressid_address");
        });

        modelBuilder.Entity<MDCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CreatedById, "fk_deal_cat_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_deal_cat_modifybyid_acc_idx");

            entity.HasIndex(e => e.RootCategoryId, "fk_deal_cat_rootcat_tuccat_idx");

            entity.HasIndex(e => e.StatusId, "fk_deal_cat_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDCategoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_deal_cat_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDCategoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_deal_cat_modifybyid_acc");

            entity.HasOne(d => d.RootCategory).WithMany(p => p.MDCategory)
                .HasForeignKey(d => d.RootCategoryId)
                .HasConstraintName("fk_deal_cat_rootcat_tuccat");

            entity.HasOne(d => d.Status).WithMany(p => p.MDCategory)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_deal_cat_statusid_status");
        });

        modelBuilder.Entity<MDDeal>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_deal_accountid_account_idx");

            entity.HasIndex(e => e.ApproverId, "fk_deal_approverid_account_idx");

            entity.HasIndex(e => e.CategoryId, "fk_deal_cat_tuccat_idx");

            entity.HasIndex(e => e.CreatedById, "fk_deal_createdbyid_account_idx");

            entity.HasIndex(e => e.DealTypeId, "fk_deal_dealtypeid_core_idx");

            entity.HasIndex(e => e.DeliveryTypeId, "fk_deal_deliverytype_core_idx");

            entity.HasIndex(e => e.DiscountTypeId, "fk_deal_discounttypeid_helper_idx");

            entity.HasIndex(e => e.PosterStorageId, "fk_deal_imagestorageid_storage_idx");

            entity.HasIndex(e => e.ModifyById, "fk_deal_modifybyid_account_idx");

            entity.HasIndex(e => e.SettlementTypeId, "fk_deal_settlementtypeid_helper_idx");

            entity.HasIndex(e => e.StatusId, "fk_deal_statusid_helper_idx");

            entity.HasIndex(e => e.SubcategoryId, "fk_deal_subcategoryid_category_idx");

            entity.HasIndex(e => e.TypeId, "fk_deal_typeid_helper_idx");

            entity.HasIndex(e => e.UsageTypeId, "fk_deal_usagetypeid_helper_idx");

            entity.Property(e => e.ActualPrice).HasDefaultValueSql("'0'");
            entity.Property(e => e.Amount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ApprovalDate).HasColumnType("datetime");
            entity.Property(e => e.ApproverComment).HasMaxLength(256);
            entity.Property(e => e.BuyerTypeId).HasDefaultValueSql("'1'");
            entity.Property(e => e.Charge).HasDefaultValueSql("'0'");
            entity.Property(e => e.CodeValidityEndDate).HasColumnType("datetime");
            entity.Property(e => e.CodeValidityStartDate).HasColumnType("datetime");
            entity.Property(e => e.CommissionAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.DiscountAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.DiscountPercentage).HasDefaultValueSql("'0'");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IsPinRequired).HasDefaultValueSql("'0'");
            entity.Property(e => e.MaximumUnitSale).HasDefaultValueSql("'0'");
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SellingPrice).HasDefaultValueSql("'0'");
            entity.Property(e => e.Slug).HasMaxLength(256);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.TDislike).HasDefaultValueSql("'0'");
            entity.Property(e => e.TLike).HasDefaultValueSql("'0'");
            entity.Property(e => e.TPurchase).HasDefaultValueSql("'0'");
            entity.Property(e => e.TView).HasDefaultValueSql("'0'");
            entity.Property(e => e.Title).HasMaxLength(256);
            entity.Property(e => e.TitleContent).HasMaxLength(128);
            entity.Property(e => e.TotalAmount).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deal_accountid_account");

            entity.HasOne(d => d.Approver).WithMany(p => p.MDDealApprover)
                .HasForeignKey(d => d.ApproverId)
                .HasConstraintName("fk_deal_approverid_account");

            entity.HasOne(d => d.Category).WithMany(p => p.MDDealCategory)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("fk_deal_cat_tuccat");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deal_createdbyid_account");

            entity.HasOne(d => d.DealType).WithMany(p => p.MDDealDealType)
                .HasForeignKey(d => d.DealTypeId)
                .HasConstraintName("fk_deal_dealtypeid_core");

            entity.HasOne(d => d.DeliveryType).WithMany(p => p.MDDealDeliveryType)
                .HasForeignKey(d => d.DeliveryTypeId)
                .HasConstraintName("fk_deal_deliverytype_core");

            entity.HasOne(d => d.DiscountType).WithMany(p => p.MDDealDiscountType)
                .HasForeignKey(d => d.DiscountTypeId)
                .HasConstraintName("fk_deal_discounttypeid_helper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_deal_modifybyid_account");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.MDDeal)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("fk_deal_imagestorageid_storage");

            entity.HasOne(d => d.SettlementType).WithMany(p => p.MDDealSettlementType)
                .HasForeignKey(d => d.SettlementTypeId)
                .HasConstraintName("fk_deal_settlementtypeid_helper");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deal_statusid_helper");

            entity.HasOne(d => d.Subcategory).WithMany(p => p.MDDealSubcategory)
                .HasForeignKey(d => d.SubcategoryId)
                .HasConstraintName("fk_deal_subcategoryid_category");

            entity.HasOne(d => d.Type).WithMany(p => p.MDDealType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deal_typeid_helper");

            entity.HasOne(d => d.UsageType).WithMany(p => p.MDDealUsageType)
                .HasForeignKey(d => d.UsageTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deal_usagetypeid_helper");
        });

        modelBuilder.Entity<MDDealAddress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_dealaddress_addressid_acc_idx");

            entity.HasIndex(e => e.AddressId, "fk_dealaddress_addressid_address_idx");

            entity.Property(e => e.AddressReference).HasMaxLength(84);
            entity.Property(e => e.Guid).HasMaxLength(84);
            entity.Property(e => e.TId).HasMaxLength(84);

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealAddress)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_dealaddress_addressid_acc");

            entity.HasOne(d => d.Address).WithMany(p => p.MDDealAddress)
                .HasForeignKey(d => d.AddressId)
                .HasConstraintName("fk_dealaddress_addressid_address");
        });

        modelBuilder.Entity<MDDealBookmark>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.StatusId, "fk_dbmak_statusid_idx");

            entity.HasIndex(e => e.AccountId, "fk_dbmark_accid_idx");

            entity.HasIndex(e => e.DealId, "fk_dbmark_dealid_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealBookmark)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dbmark_accid");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealBookmark)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dbmark_dealid");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealBookmark)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dbmak_statusid");
        });

        modelBuilder.Entity<MDDealCart>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_cart_accountid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_cart_createdby_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_cart_dealid_deal");

            entity.HasIndex(e => e.ModifyById, "fk_cart_modifyby_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_cart_status_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(84);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealCartAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_cart_accountid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealCartCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_cart_createdby_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealCart)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cart_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealCartModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_cart_modifyby_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealCart)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_cart_status_core");
        });

        modelBuilder.Entity<MDDealCode>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_dealcode_accountid_account_idx");

            entity.HasIndex(e => e.CashierId, "fk_dealcode_cashierid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_dealcode_createdbyid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_dealcode_dealid_deal_idx");

            entity.HasIndex(e => e.FromAddressId, "fk_dealcode_faddressid_address_idx");

            entity.HasIndex(e => e.LastUseSource, "fk_dealcode_lastusesource_helper_idx");

            entity.HasIndex(e => e.LastUseLocationId, "fk_dealcode_locationid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_dealcode_modifybyid_account_idx");

            entity.HasIndex(e => e.OrderId, "fk_dealcode_orderid_lsshipments_idx");

            entity.HasIndex(e => e.PartnerId, "fk_dealcode_partnerid_account_idx");

            entity.HasIndex(e => e.PromoCodeId, "fk_dealcode_promocodeid_promocodeacc_idx");

            entity.HasIndex(e => e.SecondaryAccountId, "fk_dealcode_secaccountid_account_idx");

            entity.HasIndex(e => e.ToAddressId, "fk_dealcode_taddressid_address_idx");

            entity.HasIndex(e => e.TerminalId, "fk_dealcode_terminalid_account_idx");

            entity.HasIndex(e => e.TransactionId, "fk_dealcode_transactionid_transaction_idx");

            entity.HasIndex(e => e.StatusId, "fl_dealcode_statusid_helper_idx");

            entity.Property(e => e.AvailableAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ComissionAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ItemAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ItemCode).HasMaxLength(1024);
            entity.Property(e => e.ItemPin).HasMaxLength(256);
            entity.Property(e => e.LastUseDate).HasColumnType("datetime");
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.PartnerComissionAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.SecondaryCode).HasMaxLength(64);
            entity.Property(e => e.SecondaryMessage).HasMaxLength(128);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.UseAttempts).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealCodeAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_dealcode_accountid_account");

            entity.HasOne(d => d.Cashier).WithMany(p => p.MDDealCodeCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_dealcode_cashierid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealCodeCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealcode_createdbyid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealCode)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealcode_dealid_deal");

            entity.HasOne(d => d.FromAddress).WithMany(p => p.MDDealCodeFromAddress)
                .HasForeignKey(d => d.FromAddressId)
                .HasConstraintName("fk_dealcode_faddressid_address");

            entity.HasOne(d => d.LastUseLocation).WithMany(p => p.MDDealCodeLastUseLocation)
                .HasForeignKey(d => d.LastUseLocationId)
                .HasConstraintName("fk_dealcode_locationid_account");

            entity.HasOne(d => d.LastUseSourceNavigation).WithMany(p => p.MDDealCodeLastUseSourceNavigation)
                .HasForeignKey(d => d.LastUseSource)
                .HasConstraintName("fk_dealcode_lastusesource_helper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealCodeModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_dealcode_modifybyid_account");

            entity.HasOne(d => d.Order).WithMany(p => p.MDDealCode)
                .HasForeignKey(d => d.OrderId)
                .HasConstraintName("fk_dealcode_orderid_lsshipments");

            entity.HasOne(d => d.Partner).WithMany(p => p.MDDealCodePartner)
                .HasForeignKey(d => d.PartnerId)
                .HasConstraintName("fk_dealcode_partnerid_account");

            entity.HasOne(d => d.PromoCode).WithMany(p => p.MDDealCode)
                .HasForeignKey(d => d.PromoCodeId)
                .HasConstraintName("fk_dealcode_promocodeid_promocodeacc");

            entity.HasOne(d => d.SecondaryAccount).WithMany(p => p.MDDealCodeSecondaryAccount)
                .HasForeignKey(d => d.SecondaryAccountId)
                .HasConstraintName("fk_dealcode_secaccountid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealCodeStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fl_dealcode_statusid_helper");

            entity.HasOne(d => d.Terminal).WithMany(p => p.MDDealCodeTerminal)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_dealcode_terminalid_account");

            entity.HasOne(d => d.ToAddress).WithMany(p => p.MDDealCodeToAddress)
                .HasForeignKey(d => d.ToAddressId)
                .HasConstraintName("fk_dealcode_taddressid_address");

            entity.HasOne(d => d.Transaction).WithMany(p => p.MDDealCode)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("fk_dealcode_transactionid_transaction");
        });

        modelBuilder.Entity<MDDealGallery>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_dealgallary_createdbyid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_dealgallary_dealid_deal_idx");

            entity.HasIndex(e => e.ImageStorageId, "fk_dealgallary_storageid_storage_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealGallery)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealgallary_createdbyid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealGallery)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealgallary_dealid_deal");

            entity.HasOne(d => d.ImageStorage).WithMany(p => p.MDDealGallery)
                .HasForeignKey(d => d.ImageStorageId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealgallary_storageid_storage");
        });

        modelBuilder.Entity<MDDealLike>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_deallike_accountid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_deallike_deal_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealLike)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deallike_accountid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealLike)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_deallike_deal");
        });

        modelBuilder.Entity<MDDealLocation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_dealloc_createdbyid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_dealloc_dealid_deal_idx");

            entity.HasIndex(e => e.LocationId, "fk_dealloc_deallocid_account_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealLocationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealloc_createdbyid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealLocation)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealloc_dealid_deal");

            entity.HasOne(d => d.Location).WithMany(p => p.MDDealLocationLocation)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealloc_deallocid_account");
        });

        modelBuilder.Entity<MDDealNotification>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.CreatedById, "fk_dealnot_createdbyid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_dealnot_dealid_deal_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealNotification)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealnot_createdbyid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealNotification)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealnot_dealid_deal");
        });

        modelBuilder.Entity<MDDealPromoCode>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_dealpromocode_accountid_acc_idx");

            entity.HasIndex(e => e.CategoryId, "fk_dealpromocode_categoryid_dealcategory_idx");

            entity.HasIndex(e => e.CreatedById, "fk_dealpromocode_createdby_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_dealpromocode_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_dealpromocode_modifyby_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_dealpromocode_status_core_idx");

            entity.HasIndex(e => e.TypeId, "fk_dealpromocode_type_core_idx");

            entity.HasIndex(e => e.ValueTypeId, "fk_dealpromocode_valuetype_core_idx");

            entity.Property(e => e.Code).HasMaxLength(84);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(85);
            entity.Property(e => e.IsAllDeals).HasDefaultValueSql("'0'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealPromoCodeAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_dealpromocode_accountid_acc");

            entity.HasOne(d => d.Category).WithMany(p => p.MDDealPromoCode)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("fk_dealpromocode_categoryid_dealcategory");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealPromoCodeCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_dealpromocode_createdby_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealPromoCode)
                .HasForeignKey(d => d.DealId)
                .HasConstraintName("fk_dealpromocode_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealPromoCodeModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_dealpromocode_modifyby_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealPromoCodeStatus)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_dealpromocode_status_core");

            entity.HasOne(d => d.Type).WithMany(p => p.MDDealPromoCodeType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_dealpromocode_type_core");

            entity.HasOne(d => d.ValueType).WithMany(p => p.MDDealPromoCodeValueType)
                .HasForeignKey(d => d.ValueTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealpromocode_valuetype_core");
        });

        modelBuilder.Entity<MDDealPromoCodeAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_promocodeacc_accountid_acc_idx");

            entity.HasIndex(e => e.PromoCodeId, "fk_promocodeacc_promocodeid_promocode_idx");

            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.UseDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealPromoCodeAccountNavigation)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promocodeacc_accountid_acc");

            entity.HasOne(d => d.PromoCode).WithMany(p => p.MDDealPromoCodeAccount)
                .HasForeignKey(d => d.PromoCodeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promocodeacc_promocodeid_promocode");
        });

        modelBuilder.Entity<MDDealPromotion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CountryId, "fk_countryid_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_dealpromo_createdbyid_acc_idx");

            entity.HasIndex(e => e.DealId, "fk_dealpromo_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_dealpromo_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_dealpromo_statusid_core_idx");

            entity.HasIndex(e => e.ImageStorageId, "fk_dealpromo_storage_idx");

            entity.HasIndex(e => e.TypeId, "fk_dealpromo_typeid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Location).HasMaxLength(256);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.Url).HasMaxLength(1024);

            entity.HasOne(d => d.Country).WithMany(p => p.MDDealPromotion)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("fk_countryid_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealPromotionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealpromo_createdbyid_acc");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealPromotion)
                .HasForeignKey(d => d.DealId)
                .HasConstraintName("fk_dealpromo_dealid_deal");

            entity.HasOne(d => d.ImageStorage).WithMany(p => p.MDDealPromotion)
                .HasForeignKey(d => d.ImageStorageId)
                .HasConstraintName("fk_dealpromo_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealPromotionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_dealpromo_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealPromotionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealpromo_statusid_core");

            entity.HasOne(d => d.Type).WithMany(p => p.MDDealPromotionType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_dealpromo_typeid_helper");
        });

        modelBuilder.Entity<MDDealPromotionClick>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_dpromocl_accid_acc_idx");

            entity.HasIndex(e => e.PromotionId, "fk_dpromocl_promoid_promo_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealPromotionClick)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_dpromocl_accid_acc");

            entity.HasOne(d => d.Promotion).WithMany(p => p.MDDealPromotionClick)
                .HasForeignKey(d => d.PromotionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dpromocl_promoid_promo");
        });

        modelBuilder.Entity<MDDealReview>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_dealreview_createdbyid_account_idx");

            entity.HasIndex(e => e.DealCodeId, "fk_dealreview_dealcodeid_dealcode_idx");

            entity.HasIndex(e => e.DealId, "fk_dealreview_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_dealreview_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_dealreview_statusid_helper_idx");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Review).HasMaxLength(256);
            entity.Property(e => e.SystemComment).HasMaxLength(128);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealReviewCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_dealreview_createdbyid_account");

            entity.HasOne(d => d.DealCode).WithMany(p => p.MDDealReview)
                .HasForeignKey(d => d.DealCodeId)
                .HasConstraintName("fk_dealreview_dealcodeid_dealcode");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealReview)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealreview_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealReviewModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_dealreview_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealReview)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealreview_statusid_helper");
        });

        modelBuilder.Entity<MDDealSearch>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_dealsearch_accid_acc_idx");

            entity.HasIndex(e => e.CreatedById, "fk_dealsearch_createdby_acc_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(84);
            entity.Property(e => e.SearchText).HasMaxLength(256);

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealSearchAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_dealsearch_accid_acc");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealSearchCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_dealsearch_createdby_acc");
        });

        modelBuilder.Entity<MDDealShedule>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_dealshedule_createdbyid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_dealshedule_dealid_deal_idx");

            entity.HasIndex(e => e.ModifyById, "fk_dealshedule_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_dealshedule_statusid_helper_idx");

            entity.HasIndex(e => e.TypeId, "fk_dealshedule_typeid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartTime).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDDealSheduleCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealshedule_createdbyid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealShedule)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealshedule_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDDealSheduleModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_dealshedule_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.MDDealSheduleStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealshedule_statusid_helper");

            entity.HasOne(d => d.Type).WithMany(p => p.MDDealSheduleType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_dealshedule_typeid_helper");
        });

        modelBuilder.Entity<MDDealView>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CustomerId, "fk_dealview_customerid_deal_idx");

            entity.HasIndex(e => e.DealId, "fk_dealview_dealid_deal_idx");

            entity.HasIndex(e => e.PlatformId, "fk_dealview_pltfrmid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");

            entity.HasOne(d => d.Customer).WithMany(p => p.MDDealView)
                .HasForeignKey(d => d.CustomerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealview_customerid_deal");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealView)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dealview_dealid_deal");

            entity.HasOne(d => d.Platform).WithMany(p => p.MDDealView)
                .HasForeignKey(d => d.PlatformId)
                .HasConstraintName("fk_dealview_pltfrmid_core");
        });

        modelBuilder.Entity<MDDealWishlist>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_dwishlist_accountid_idx");

            entity.HasIndex(e => e.DealId, "fk_dwishlist_dealid_deal_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);

            entity.HasOne(d => d.Account).WithMany(p => p.MDDealWishlist)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dwishlist_accountid");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDDealWishlist)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_dwishlist_dealid_deal");
        });

        modelBuilder.Entity<MDFlashDeal>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_flashdeal_createdbyid_account_idx");

            entity.HasIndex(e => e.DealId, "fk_flashdeal_dealid_deal");

            entity.HasIndex(e => e.ModifyById, "fk_flashdeal_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_flashdeal_statusid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.MDFlashDealCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_flashdeal_createdbyid_account");

            entity.HasOne(d => d.Deal).WithMany(p => p.MDFlashDeal)
                .HasForeignKey(d => d.DealId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_flashdeal_dealid_deal");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.MDFlashDealModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_flashdeal_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.MDFlashDeal)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_flashdeal_statusid_helper");
        });

        modelBuilder.Entity<MDPackaging>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CategoryId, "fk_mdpackaging_categoryid_cat_idx");

            entity.HasIndex(e => e.MerchantId, "fk_mdpackaging_merchantid_acc_idx");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Name).HasMaxLength(300);
            entity.Property(e => e.PackagingId).HasMaxLength(64);
            entity.Property(e => e.SizeUnit).HasMaxLength(64);
            entity.Property(e => e.TId).HasMaxLength(64);
            entity.Property(e => e.Type).HasMaxLength(64);
            entity.Property(e => e.WeightUnit).HasMaxLength(64);

            entity.HasOne(d => d.Category).WithMany(p => p.MDPackaging)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("fk_mdpackaging_categoryid_cat");

            entity.HasOne(d => d.Merchant).WithMany(p => p.MDPackaging)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_mdpackaging_merchantid_acc");
        });

        modelBuilder.Entity<MepasTransaction>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.BillRefNumber).HasMaxLength(255);
            entity.Property(e => e.BusinessShortCode).HasMaxLength(255);
            entity.Property(e => e.FirstName).HasMaxLength(255);
            entity.Property(e => e.InvoiceNumber).HasMaxLength(255);
            entity.Property(e => e.MSISDN).HasMaxLength(255);
            entity.Property(e => e.OrgAccountBalance).HasMaxLength(255);
            entity.Property(e => e.ThirdPartTransID).HasMaxLength(255);
            entity.Property(e => e.TransAmount).HasMaxLength(255);
            entity.Property(e => e.TransID).HasMaxLength(255);
            entity.Property(e => e.TransTime).HasMaxLength(255);
            entity.Property(e => e.TransactionType).HasMaxLength(255);
        });

        modelBuilder.Entity<ReferralSettings>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.Property(e => e.Amount).HasPrecision(18, 2);
            entity.Property(e => e.DateSet).HasColumnType("datetime");
            entity.Property(e => e.SetBy).HasMaxLength(100);
        });

        modelBuilder.Entity<SCCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.PrimaryStorageId, "fk_hcpprodcar_primarystorageid_storage_idx");

            entity.HasIndex(e => e.CreatedById, "fk_hcpprodcat_createdbyid_hcaccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcpprodcat_modifybyid_hcaccount_idx");

            entity.HasIndex(e => e.ParentId, "fk_hcpprodcat_parentcat_cat_idx");

            entity.HasIndex(e => e.SecondaryStorageId, "fk_hcpprodcat_secondarystorageid_storage_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcpprodcat_statusid_corehelper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SystemName).HasMaxLength(256);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCCategoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpprodcat_createdbyid_hcaccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCCategoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcpprodcat_modifybyid_hcaccount");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("fk_hcpprodcat_parentcat_cat");

            entity.HasOne(d => d.PrimaryStorage).WithMany(p => p.SCCategoryPrimaryStorage)
                .HasForeignKey(d => d.PrimaryStorageId)
                .HasConstraintName("fk_hcpprodcar_primarystorageid_storage");

            entity.HasOne(d => d.SecondaryStorage).WithMany(p => p.SCCategorySecondaryStorage)
                .HasForeignKey(d => d.SecondaryStorageId)
                .HasConstraintName("fk_hcpprodcat_secondarystorageid_storage");

            entity.HasOne(d => d.Status).WithMany(p => p.SCCategory)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpprodcat_statusid_corehelper");
        });

        modelBuilder.Entity<SCConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.StatusId, "fk_Scconfig_statusid_helper_idx");

            entity.HasIndex(e => e.AccountId, "fk_scconfig_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_scconfig_createdbyid_account_idx");

            entity.HasIndex(e => e.ImageStorageId, "fk_scconfig_imagestorageid_storage_idx");

            entity.HasIndex(e => e.ModifyById, "fk_scconfig_modifybyid_account_idx");

            entity.HasIndex(e => e.ParentId, "fk_scconfig_parentid_scconfig_idx");

            entity.HasIndex(e => e.DataTypeId, "fk_scconfig_typeid_helper_idx");

            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(256);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Value).HasMaxLength(2048);

            entity.HasOne(d => d.Account).WithMany(p => p.SCConfigurationAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_scconfig_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCConfigurationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_scconfig_createdbyid_account");

            entity.HasOne(d => d.DataType).WithMany(p => p.SCConfigurationDataType)
                .HasForeignKey(d => d.DataTypeId)
                .HasConstraintName("fk_scconfig_typeid_helper");

            entity.HasOne(d => d.ImageStorage).WithMany(p => p.SCConfiguration)
                .HasForeignKey(d => d.ImageStorageId)
                .HasConstraintName("fk_scconfig_imagestorageid_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCConfigurationModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_scconfig_modifybyid_account");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("fk_scconfig_parentid_scconfig");

            entity.HasOne(d => d.Status).WithMany(p => p.SCConfigurationStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_Scconfig_statusid_helper");
        });

        modelBuilder.Entity<SCOrder>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.OrderId, "OrderId_UNIQUE").IsUnique();

            entity.HasIndex(e => e.DealerLocationId, "fk_hcpoprder_dealerlocationid_account_idx");

            entity.HasIndex(e => e.CustomerId, "fk_hcporder_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_hcporder_createdbyid_account_idx");

            entity.HasIndex(e => e.DealerId, "fk_hcporder_dealerid_account_idx");

            entity.HasIndex(e => e.DeliveryTypeId, "fk_hcporder_deliverytypeid_helper_idx");

            entity.HasIndex(e => e.DeliveryModeId, "fk_hcporder_modeid_helper_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcporder_modifybyid_account_idx");

            entity.HasIndex(e => e.PaymentModeId, "fk_hcporder_paymentmodeid_helper_idx");

            entity.HasIndex(e => e.PaymentStatusId, "fk_hcporder_paymentstatusid_helper_idx");

            entity.HasIndex(e => e.PriorityId, "fk_hcporder_priorityid_helper_idx");

            entity.HasIndex(e => e.RiderId, "fk_hcporder_riderid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcporder_statudid_helper_idx");

            entity.Property(e => e.ActualDeliveryDate).HasColumnType("datetime");
            entity.Property(e => e.CloseDate).HasColumnType("datetime");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.CustomerRating).HasDefaultValueSql("'0'");
            entity.Property(e => e.CustomerReview).HasMaxLength(256);
            entity.Property(e => e.DealerLocationReview).HasMaxLength(256);
            entity.Property(e => e.ExpectedDeliveryDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.OpenDate).HasColumnType("datetime");
            entity.Property(e => e.OrderDeliveryCode).HasMaxLength(16);
            entity.Property(e => e.OrderId).HasMaxLength(16);
            entity.Property(e => e.PaymentReference).HasMaxLength(128);
            entity.Property(e => e.RiderReview).HasMaxLength(256);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCOrderCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporder_createdbyid_account");

            entity.HasOne(d => d.Customer).WithMany(p => p.SCOrderCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("fk_hcporder_accountid_account");

            entity.HasOne(d => d.Dealer).WithMany(p => p.SCOrderDealer)
                .HasForeignKey(d => d.DealerId)
                .HasConstraintName("fk_hcporder_dealerid_account");

            entity.HasOne(d => d.DealerLocation).WithMany(p => p.SCOrderDealerLocation)
                .HasForeignKey(d => d.DealerLocationId)
                .HasConstraintName("fk_hcpoprder_dealerlocationid_account");

            entity.HasOne(d => d.DeliveryMode).WithMany(p => p.SCOrderDeliveryMode)
                .HasForeignKey(d => d.DeliveryModeId)
                .HasConstraintName("fk_hcporder_deliverymodeid_helper");

            entity.HasOne(d => d.DeliveryType).WithMany(p => p.SCOrderDeliveryType)
                .HasForeignKey(d => d.DeliveryTypeId)
                .HasConstraintName("fk_hcporder_deliverytypeid_helper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCOrderModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcporder_modifybyid_account");

            entity.HasOne(d => d.PaymentMode).WithMany(p => p.SCOrderPaymentMode)
                .HasForeignKey(d => d.PaymentModeId)
                .HasConstraintName("fk_hcporder_paymentmodeid_helper");

            entity.HasOne(d => d.PaymentStatus).WithMany(p => p.SCOrderPaymentStatus)
                .HasForeignKey(d => d.PaymentStatusId)
                .HasConstraintName("fk_hcporder_paymentstatusid_helper");

            entity.HasOne(d => d.Priority).WithMany(p => p.SCOrderPriority)
                .HasForeignKey(d => d.PriorityId)
                .HasConstraintName("fk_hcporder_priorityid_helper");

            entity.HasOne(d => d.Rider).WithMany(p => p.SCOrderRider)
                .HasForeignKey(d => d.RiderId)
                .HasConstraintName("fk_hcporder_riderid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.SCOrderStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporder_statudid_helper");
        });

        modelBuilder.Entity<SCOrderActivity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_hcporactivity_createdbyid_account_idx");

            entity.HasIndex(e => e.OrderId, "fk_oractivity_orderid_order_idx");

            entity.HasIndex(e => e.OrderItemId, "fk_oractivity_orderitemid_orderitem_idx");

            entity.HasIndex(e => e.OrderStatusId, "fk_oractivity_orderstatusid_helper_idx");

            entity.HasIndex(e => e.StatusId, "fk_oractivity_statusid_helper_idx");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(128);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.SystemDescription).HasMaxLength(256);
            entity.Property(e => e.Title).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCOrderActivity)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_oractivity_createdbyid_account");

            entity.HasOne(d => d.Order).WithMany(p => p.SCOrderActivity)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_oractivity_orderid_order");

            entity.HasOne(d => d.OrderItem).WithMany(p => p.SCOrderActivity)
                .HasForeignKey(d => d.OrderItemId)
                .HasConstraintName("fk_oractivity_orderitemid_orderitem");

            entity.HasOne(d => d.OrderStatus).WithMany(p => p.SCOrderActivityOrderStatus)
                .HasForeignKey(d => d.OrderStatusId)
                .HasConstraintName("fk_oractivity_orderstatusid_helper");

            entity.HasOne(d => d.Status).WithMany(p => p.SCOrderActivityStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_oractivity_statusid_helper");
        });

        modelBuilder.Entity<SCOrderAddress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AreaId, "fk_hcpoaddress_areaid_order_idx");

            entity.HasIndex(e => e.CityId, "fk_hcpoaddress_cityid_parameter_idx");

            entity.HasIndex(e => e.CountryId, "fk_hcpoaddress_countryid_parameter_idx");

            entity.HasIndex(e => e.CreatedById, "fk_hcpoaddress_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcpoaddress_modifybyid_account_idx");

            entity.HasIndex(e => e.OrderId, "fk_hcpoaddress_orderid_order_idx");

            entity.HasIndex(e => e.StateId, "fk_hcpoaddress_stateid_parameter_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcpoaddress_statusid_helper_idx");

            entity.HasIndex(e => e.TypeId, "fk_hcpoaddress_typeid_helper_idx");

            entity.Property(e => e.AddressLine1).HasMaxLength(128);
            entity.Property(e => e.AddressLine2).HasMaxLength(128);
            entity.Property(e => e.AlternateMobileNumber).HasMaxLength(16);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Instructions).HasMaxLength(128);
            entity.Property(e => e.Landmark).HasMaxLength(64);
            entity.Property(e => e.MapAddress).HasMaxLength(128);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ZipCode).HasMaxLength(16);

            entity.HasOne(d => d.Area).WithMany(p => p.SCOrderAddressArea)
                .HasForeignKey(d => d.AreaId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_areaid_parameter");

            entity.HasOne(d => d.City).WithMany(p => p.SCOrderAddressCity)
                .HasForeignKey(d => d.CityId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_cityid_parameter");

            entity.HasOne(d => d.Country).WithMany(p => p.SCOrderAddress)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_countryid_common");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCOrderAddressCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCOrderAddressModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcpoaddress_modifybyid_account");

            entity.HasOne(d => d.Order).WithMany(p => p.SCOrderAddress)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_orderid_order");

            entity.HasOne(d => d.State).WithMany(p => p.SCOrderAddressState)
                .HasForeignKey(d => d.StateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_stateid_parameter");

            entity.HasOne(d => d.Status).WithMany(p => p.SCOrderAddressStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_statusid_helper");

            entity.HasOne(d => d.Type).WithMany(p => p.SCOrderAddressType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpoaddress_typeid_helper");
        });

        modelBuilder.Entity<SCOrderItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_hcporderitem_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcporderitem_modifybyid_account_idx");

            entity.HasIndex(e => e.OrderId, "fk_hcporderitem_orderid_order_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcporderitem_statusid_helper_idx");

            entity.HasIndex(e => e.VarientId, "fk_hcporderitem_varientid_pvarient_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCOrderItemCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporderitem_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCOrderItemModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcporderitem_modifybyid_account");

            entity.HasOne(d => d.Order).WithMany(p => p.SCOrderItem)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporderitem_orderid_order");

            entity.HasOne(d => d.Status).WithMany(p => p.SCOrderItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporderitem_statusid_helper");

            entity.HasOne(d => d.Varient).WithMany(p => p.SCOrderItem)
                .HasForeignKey(d => d.VarientId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcporderitem_varientid_pvarient");
        });

        modelBuilder.Entity<SCProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_hcpproduct_accountid_hcaccount_idx");

            entity.HasIndex(e => e.CategoryId, "fk_hcpproduct_categoryid_hcproductcategory_idx");

            entity.HasIndex(e => e.CreatedById, "fk_hcpproduct_createdbyid_hcaccount_idx");

            entity.HasIndex(e => e.PrimaryStorageId, "fk_hcpproduct_iconstorageid_storage_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcpproduct_modifybyid_hcaccount_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcpproduct_statusid_corehelper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SystemName).HasMaxLength(256);

            entity.HasOne(d => d.Account).WithMany(p => p.SCProductAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproduct_accountid_hcaccount");

            entity.HasOne(d => d.Category).WithMany(p => p.SCProduct)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("fk_hcpproduct_categoryid_hcproductcategory");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCProductCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproduct_createdbyid_hcaccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCProductModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcpproduct_modifybyid_hcaccount");

            entity.HasOne(d => d.PrimaryStorage).WithMany(p => p.SCProduct)
                .HasForeignKey(d => d.PrimaryStorageId)
                .HasConstraintName("fk_hcpproduct_primarystorageid_storage");

            entity.HasOne(d => d.Status).WithMany(p => p.SCProduct)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproduct_statusid_corehelper");
        });

        modelBuilder.Entity<SCProductVarient>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_hcpproductvarient_productid_createdbyid_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcpproductvarient_productid_modifybyid_idx");

            entity.HasIndex(e => e.ProductId, "fk_hcpproductvarient_productid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcpproductvarient_productid_statusid_idx");

            entity.Property(e => e.BarcodeNumber).HasMaxLength(32);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(256);
            entity.Property(e => e.Sku).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(256);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCProductVarientCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproductvarient_productid_createdbyid");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCProductVarientModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcpproductvarient_productid_modifybyid");

            entity.HasOne(d => d.Product).WithMany(p => p.SCProductVarient)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproductvarient_productid_product");

            entity.HasOne(d => d.Status).WithMany(p => p.SCProductVarient)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcpproductvarient_productid_statusid");
        });

        modelBuilder.Entity<SCProductVarientStock>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_hcproductstock_createdbyid_hcaccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_hcproductstock_modifybyid_idx");

            entity.HasIndex(e => e.VarientId, "fk_hcproductstock_productvarientid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_hcproductstock_statusid_hccorehelper_idx");

            entity.HasIndex(e => e.SubAccountId, "fk_hcproductstock_subaccountid_hcaccount_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SCProductVarientStockCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcproductstock_createdbyid_hcaccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SCProductVarientStockModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_hcproductstock_modifybyid_hcaccount");

            entity.HasOne(d => d.Status).WithMany(p => p.SCProductVarientStock)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcproductstock_statusid_hccorehelper");

            entity.HasOne(d => d.SubAccount).WithMany(p => p.SCProductVarientStockSubAccount)
                .HasForeignKey(d => d.SubAccountId)
                .HasConstraintName("fk_hcproductstock_subaccountid_hcaccount");

            entity.HasOne(d => d.Varient).WithMany(p => p.SCProductVarientStock)
                .HasForeignKey(d => d.VarientId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_hcproductstock_productvarientid_product");
        });

        modelBuilder.Entity<SMSCampaign>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_smscamp_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_smscamp_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_smscamp_modifybyid_account_idx");

            entity.HasIndex(e => e.RecipientSubTypeId, "fk_smscamp_recepientsubtypeid_core_idx");

            entity.HasIndex(e => e.RecipientTypeId, "fk_smscamp_recepienttypeid_core_idx");

            entity.HasIndex(e => e.ReviewerId, "fk_smscamp_reviewerid_account_idx");

            entity.HasIndex(e => e.SenderId, "fk_smscamp_senderid_senderprovider_idx");

            entity.HasIndex(e => e.StatusId, "fk_smscamp_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Message).HasMaxLength(512);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReviewerComment).HasMaxLength(512);
            entity.Property(e => e.SendDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(64);

            entity.HasOne(d => d.Account).WithMany(p => p.SMSCampaignAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscamp_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SMSCampaignCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscamp_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SMSCampaignModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_smscamp_modifybyid_account");

            entity.HasOne(d => d.RecipientSubType).WithMany(p => p.SMSCampaignRecipientSubType)
                .HasForeignKey(d => d.RecipientSubTypeId)
                .HasConstraintName("fk_smscamp_recepientsubtypeid_core");

            entity.HasOne(d => d.RecipientType).WithMany(p => p.SMSCampaignRecipientType)
                .HasForeignKey(d => d.RecipientTypeId)
                .HasConstraintName("fk_smscamp_recepienttypeid_core");

            entity.HasOne(d => d.Reviewer).WithMany(p => p.SMSCampaignReviewer)
                .HasForeignKey(d => d.ReviewerId)
                .HasConstraintName("fk_smscamp_reviewerid_account");

            entity.HasOne(d => d.Sender).WithMany(p => p.SMSCampaign)
                .HasForeignKey(d => d.SenderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscamp_senderid_senderprovider");

            entity.HasOne(d => d.Status).WithMany(p => p.SMSCampaignStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscamp_statusid_core");
        });

        modelBuilder.Entity<SMSCampaignGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CampaignId, "fk_smscampgrp_campid_camp_idx");

            entity.HasIndex(e => e.CreatedById, "fk_smscampgrp_createdbyid_account_idx");

            entity.HasIndex(e => e.GroupId, "fk_smscampgrp_groupid_grp_idx");

            entity.HasIndex(e => e.ModifyById, "fk_smscampgrp_modifybyid_account_idx");

            entity.HasIndex(e => e.SourceId, "fk_smscampgrp_sourceid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_smscampgrp_statusid_core_idx");

            entity.Property(e => e.Condition).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.FileName).HasMaxLength(256);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(128);

            entity.HasOne(d => d.Campaign).WithMany(p => p.SMSCampaignGroup)
                .HasForeignKey(d => d.CampaignId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgrp_campid_camp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SMSCampaignGroupCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgrp_createdbyid_account");

            entity.HasOne(d => d.Group).WithMany(p => p.SMSCampaignGroup)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("fk_smscampgrp_groupid_grp");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SMSCampaignGroupModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_smscampgrp_modifybyid_account");

            entity.HasOne(d => d.Source).WithMany(p => p.SMSCampaignGroupSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgrp_sourceid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.SMSCampaignGroupStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgrp_statusid_core");
        });

        modelBuilder.Entity<SMSCampaignGroupCondition>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CampaignGroupId, "fk_campgrpid_campgrp_idx");

            entity.HasIndex(e => e.CreatedById, "fk_campgrpid_createdbyid_acc_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(64);
            entity.Property(e => e.Value).HasMaxLength(64);

            entity.HasOne(d => d.CampaignGroup).WithMany(p => p.SMSCampaignGroupCondition)
                .HasForeignKey(d => d.CampaignGroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campgrpid_campgrp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SMSCampaignGroupCondition)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campgrpid_createdbyid_acc");
        });

        modelBuilder.Entity<SMSCampaignGroupItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_smscampgitem_accid_account_idx");

            entity.HasIndex(e => e.CampaignId, "fk_smscampgitem_campaignid_camp_idx");

            entity.HasIndex(e => e.GroupId, "fk_smscampgitem_groupid_campgrp_idx");

            entity.HasIndex(e => e.StatusId, "fk_smscampgitem_status_core_idx");

            entity.Property(e => e.CountryName).HasMaxLength(64);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DeliveryDate).HasColumnType("datetime");
            entity.Property(e => e.ExStatus).HasMaxLength(32);
            entity.Property(e => e.ExStatusCode).HasMaxLength(16);
            entity.Property(e => e.ExternalId).HasMaxLength(512);
            entity.Property(e => e.MobileNumber).HasMaxLength(32);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.Operator).HasMaxLength(64);
            entity.Property(e => e.SendDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.SMSCampaignGroupItem)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_smscampgitem_accid_account");

            entity.HasOne(d => d.Campaign).WithMany(p => p.SMSCampaignGroupItem)
                .HasForeignKey(d => d.CampaignId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgitem_campaignid_camp");

            entity.HasOne(d => d.Group).WithMany(p => p.SMSCampaignGroupItem)
                .HasForeignKey(d => d.GroupId)
                .HasConstraintName("fk_smscampgitem_groupid_campgrp");

            entity.HasOne(d => d.Status).WithMany(p => p.SMSCampaignGroupItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smscampgitem_status_core");
        });

        modelBuilder.Entity<SMSGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_smsgroup_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_smsgroup_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_smsgroup_modifybyid_account_idx");

            entity.HasIndex(e => e.SourceId, "fk_smsgroup_sourceid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_smsgroup_statusid_core_idx");

            entity.Property(e => e.Condition).HasMaxLength(512);
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.FileName).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(128);

            entity.HasOne(d => d.Account).WithMany(p => p.SMSGroupAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroup_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SMSGroupCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroup_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.SMSGroupModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_smsgroup_modifybyid_account");

            entity.HasOne(d => d.Source).WithMany(p => p.SMSGroupSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroup_sourceid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.SMSGroupStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroup_statusid_core");
        });

        modelBuilder.Entity<SMSGroupCondition>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_smsgroupcon_createdbyid_account_idx");

            entity.HasIndex(e => e.GroupId, "fk_smsgroupcon_groupid_group_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(64);
            entity.Property(e => e.Value).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.SMSGroupCondition)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroupcon_createdbyid_account");

            entity.HasOne(d => d.Group).WithMany(p => p.SMSGroupCondition)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroupcon_groupid_group");
        });

        modelBuilder.Entity<SMSGroupItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_smsgroupi_accountid_account_idx");

            entity.HasIndex(e => e.GroupId, "fk_smsgroupi_groupid_group_idx");

            entity.HasIndex(e => e.StatusId, "fk_smsgroupi_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.MobileNumber).HasMaxLength(32);
            entity.Property(e => e.Name).HasMaxLength(128);

            entity.HasOne(d => d.Account).WithMany(p => p.SMSGroupItem)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_smsgroupi_accountid_account");

            entity.HasOne(d => d.Group).WithMany(p => p.SMSGroupItem)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroupi_groupid_group");

            entity.HasOne(d => d.Status).WithMany(p => p.SMSGroupItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smsgroupi_statusid_core");
        });

        modelBuilder.Entity<SMSSenderProvider>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.StatusId, "fk_smssenderprovider_core_idx");

            entity.Property(e => e.ProviderName).HasMaxLength(64);
            entity.Property(e => e.SenderId).HasMaxLength(16);

            entity.HasOne(d => d.Status).WithMany(p => p.SMSSenderProvider)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_smssenderprovider_core");
        });

        modelBuilder.Entity<TUCAppPromotion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CountryId, "fk_apppromo_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_apppromo_createdbyid_acc_idx");

            entity.HasIndex(e => e.ImageStorageId, "fk_apppromo_imgstorageid_storage_idx");

            entity.HasIndex(e => e.ModifyById, "fk_apppromo_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_apppromo_statusid_helper_idx");

            entity.Property(e => e.AppId).HasDefaultValueSql("'1'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.NavigateUrl).HasMaxLength(1024);
            entity.Property(e => e.NavigationType).HasMaxLength(64);
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Country).WithMany(p => p.TUCAppPromotion)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("fk_apppromo_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCAppPromotionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_apppromo_createdbyid_acc");

            entity.HasOne(d => d.ImageStorage).WithMany(p => p.TUCAppPromotion)
                .HasForeignKey(d => d.ImageStorageId)
                .HasConstraintName("fk_apppromo_imgstorageid_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCAppPromotionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_apppromo_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCAppPromotion)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_apppromo_statusid_helper");
        });

        modelBuilder.Entity<TUCBranch>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CityAreaId, "fk_branch_cityareaid_cityarea_idx");

            entity.HasIndex(e => e.CityId, "fk_branch_cityid_city_idx");

            entity.HasIndex(e => e.CountryId, "fk_branch_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_branch_createdby_account_idx");

            entity.HasIndex(e => e.ManagerId, "fk_branch_managerid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_branch_modifiedbyid_account_idx");

            entity.HasIndex(e => e.OwnerId, "fk_branch_ownerid_account_idx");

            entity.HasIndex(e => e.StateId, "fk_branch_stateid_state_idx");

            entity.HasIndex(e => e.StatusId, "fk_branch_statusid_helper_idx");

            entity.Property(e => e.Address).HasMaxLength(128);
            entity.Property(e => e.BranchCode).HasMaxLength(16);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(32);
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.PhoneNumber).HasMaxLength(16);

            entity.HasOne(d => d.CityArea).WithMany(p => p.TUCBranch)
                .HasForeignKey(d => d.CityAreaId)
                .HasConstraintName("fk_branch_cityareaid_cityarea");

            entity.HasOne(d => d.City).WithMany(p => p.TUCBranch)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("fk_branch_cityid_city");

            entity.HasOne(d => d.Country).WithMany(p => p.TUCBranch)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branch_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCBranchCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branch_createdby_account");

            entity.HasOne(d => d.Manager).WithMany(p => p.TUCBranchManager)
                .HasForeignKey(d => d.ManagerId)
                .HasConstraintName("fk_branch_managerid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCBranchModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_branch_modifiedbyid_account");

            entity.HasOne(d => d.Owner).WithMany(p => p.TUCBranchOwner)
                .HasForeignKey(d => d.OwnerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branch_ownerid_account");

            entity.HasOne(d => d.State).WithMany(p => p.TUCBranch)
                .HasForeignKey(d => d.StateId)
                .HasConstraintName("fk_branch_stateid_state");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCBranch)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branch_statusid_helper");
        });

        modelBuilder.Entity<TUCBranchAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_branchacc_accountid_idx");

            entity.HasIndex(e => e.AccountLevelId, "fk_branchacc_accountlevelid_parameter_idx");

            entity.HasIndex(e => e.BranchId, "fk_branchacc_branch_idx");

            entity.HasIndex(e => e.CreatedById, "fk_branchacc_createdbyid_acc_idx");

            entity.HasIndex(e => e.ManagerId, "fk_branchacc_managerid_acc_idx");

            entity.HasIndex(e => e.MerchantId, "fk_branchacc_merchantid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_branchacc_modifuedbyid_acc_idx");

            entity.HasIndex(e => e.OwnerId, "fk_branchacc_ownerid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_branchacc_statusid_helper_idx");

            entity.HasIndex(e => e.StoreId, "fk_branchacc_storeid_acc_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCBranchAccountAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branchacc_accountid_account");

            entity.HasOne(d => d.AccountLevel).WithMany(p => p.TUCBranchAccount)
                .HasForeignKey(d => d.AccountLevelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branchacc_accountlevelid_parameter");

            entity.HasOne(d => d.Branch).WithMany(p => p.TUCBranchAccount)
                .HasForeignKey(d => d.BranchId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branchacc_branchid_branch");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCBranchAccountCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branchacc_createdbyid_acc");

            entity.HasOne(d => d.Manager).WithMany(p => p.TUCBranchAccountManager)
                .HasForeignKey(d => d.ManagerId)
                .HasConstraintName("fk_branchacc_managerid_acc");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCBranchAccountMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_branchacc_merchantid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCBranchAccountModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_branchacc_modifiedbyid_acc");

            entity.HasOne(d => d.Owner).WithMany(p => p.TUCBranchAccountOwner)
                .HasForeignKey(d => d.OwnerId)
                .HasConstraintName("fk_branchacc_ownerid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCBranchAccount)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_branchacc_statusid_helper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCBranchAccountStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_branchacc_storeid_acc");
        });

        modelBuilder.Entity<TUCBranchRmTarget>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.BranchId, "fk_rmtarget_branch_branchid_idx");

            entity.HasIndex(e => e.CreatedById, "fk_rmtarget_createdby_account_idx");

            entity.HasIndex(e => e.ManagerId, "fk_rmtarget_managerod_manager_idx");

            entity.HasIndex(e => e.ModifyById, "fk_rmtarget_modifyby_account_idx");

            entity.HasIndex(e => e.RmId, "fk_rmtarget_rmid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_rmtarget_statusid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Branch).WithMany(p => p.TUCBranchRmTarget)
                .HasForeignKey(d => d.BranchId)
                .HasConstraintName("fk_rmtarget_branch_branchid");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCBranchRmTargetCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_rmtarget_createdby_account");

            entity.HasOne(d => d.Manager).WithMany(p => p.TUCBranchRmTargetManager)
                .HasForeignKey(d => d.ManagerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_rmtarget_managerod_manager");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCBranchRmTargetModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_rmtarget_modifyby_account");

            entity.HasOne(d => d.Rm).WithMany(p => p.TUCBranchRmTargetRm)
                .HasForeignKey(d => d.RmId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_rmtarget_rmid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCBranchRmTarget)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_rmtarget_statusid_helper");
        });

        modelBuilder.Entity<TUCCardRewardProcessing>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.DateObtained).HasColumnType("datetime");
            entity.Property(e => e.DateProcessed).HasColumnType("datetime");
            entity.Property(e => e.DumpProvider).HasMaxLength(255);
            entity.Property(e => e.FileName).HasMaxLength(255);
            entity.Property(e => e.FilePath).HasMaxLength(255);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ProcessEndDate).HasColumnType("datetime");
        });

        modelBuilder.Entity<TUCCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.CreatedById, "fk_tcat_createdbyid_acc_idx");

            entity.HasIndex(e => e.IconStorageId, "fk_tcat_iconstid_storage_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tcat_modifybyid_acc_idx");

            entity.HasIndex(e => e.ParentCategoryId, "fk_tcat_parentid_tuccat_idx");

            entity.HasIndex(e => e.PosterStorageId, "fk_tcat_poststid_storage_idx");

            entity.HasIndex(e => e.StatusId, "fk_tcat_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCCategoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tcat_createdbyid_acc");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.TUCCategoryIconStorage)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("fk_tcat_iconstid_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCCategoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tcat_modifybyid_acc");

            entity.HasOne(d => d.ParentCategory).WithMany(p => p.InverseParentCategory)
                .HasForeignKey(d => d.ParentCategoryId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("fk_tcat_parentid_tuccat");

            entity.HasOne(d => d.PosterStorage).WithMany(p => p.TUCCategoryPosterStorage)
                .HasForeignKey(d => d.PosterStorageId)
                .HasConstraintName("fk_tcat_poststid_storage");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCCategory)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tcat_statusid_core");
        });

        modelBuilder.Entity<TUCCategoryAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_catacc_accid_acc_idx");

            entity.HasIndex(e => e.CategoryId, "fk_catacc_catid_tuccat_idx");

            entity.HasIndex(e => e.CreatedById, "fk_catacc_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_catacc_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_catacc_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCCategoryAccountAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_catacc_accid_acc");

            entity.HasOne(d => d.Category).WithMany(p => p.TUCCategoryAccount)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_catacc_catid_tuccat");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCCategoryAccountCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_catacc_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCCategoryAccountModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_catacc_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCCategoryAccount)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_catacc_statusid_core");
        });

        modelBuilder.Entity<TUCLProgram>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_tuclprogram_accountid_acc_idx");

            entity.HasIndex(e => e.ComissionTypeId, "fk_tuclprogram_comtypeid_core_idx");

            entity.HasIndex(e => e.CreatedById, "fk_tuclprogram_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tuclprogram_modifybyid_account_idx");

            entity.HasIndex(e => e.ProgramTypeId, "fk_tuclprogram_programtypeid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_tuclprogram_statusid_status_idx");

            entity.HasIndex(e => e.SubscriptionId, "fk_tuclprogram_subscriptionid_subs_idx");

            entity.Property(e => e.CashierCommission).HasDefaultValueSql("'0.5'");
            entity.Property(e => e.CashierMaxReward).HasDefaultValueSql("'1000'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCLProgramAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tuclprogram_accountid_acc");

            entity.HasOne(d => d.ComissionType).WithMany(p => p.TUCLProgramComissionType)
                .HasForeignKey(d => d.ComissionTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tuclprogram_comtypeid_core");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCLProgramCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_tuclprogram_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCLProgramModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tuclprogram_modifybyid_account");

            entity.HasOne(d => d.ProgramType).WithMany(p => p.TUCLProgramProgramType)
                .HasForeignKey(d => d.ProgramTypeId)
                .HasConstraintName("fk_tuclprogram_programtypeid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCLProgramStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tuclprogram_statusid_status");

            entity.HasOne(d => d.Subscription).WithMany(p => p.TUCLProgram)
                .HasForeignKey(d => d.SubscriptionId)
                .HasConstraintName("fk_tuclprogram_subscriptionid_subs");
        });

        modelBuilder.Entity<TUCLProgramGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_pgr_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_pgr_createbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_pgr_modifybyid_acc_idx");

            entity.HasIndex(e => e.ProgramId, "fk_pgr_programid_program_idx");

            entity.HasIndex(e => e.StatusId, "fk_pgr_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCLProgramGroupAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pgr_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCLProgramGroupCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_pgr_createbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCLProgramGroupModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_pgr_modifybyid_acc");

            entity.HasOne(d => d.Program).WithMany(p => p.TUCLProgramGroup)
                .HasForeignKey(d => d.ProgramId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pgr_programid_program");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCLProgramGroup)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pgr_statusid_core");
        });

        modelBuilder.Entity<TUCLoyalty>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AcquirerId, "fk_loyalty_acquirerid_account_idx");

            entity.HasIndex(e => e.BinNumberId, "fk_loyalty_binid_bin_idx");

            entity.HasIndex(e => e.CashierId, "fk_loyalty_cashierid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_loyalty_createdbyid_account_idx");

            entity.HasIndex(e => e.CustomerId, "fk_loyalty_customerid_account_idx");

            entity.HasIndex(e => e.FromAccountId, "fk_loyalty_fromaccountid_account_idx");

            entity.HasIndex(e => e.FromReferrerId, "fk_loyalty_fromrefid_account_idx");

            entity.HasIndex(e => e.MerchantId, "fk_loyalty_merchantid_account_idx");

            entity.HasIndex(e => e.ModeId, "fk_loyalty_modeid_core_idx");

            entity.HasIndex(e => e.ModifyById, "fk_loyalty_modifybyid_account_idx");

            entity.HasIndex(e => e.PaymentMethodId, "fk_loyalty_pmethodid_core_idx");

            entity.HasIndex(e => e.ProviderId, "fk_loyalty_providerid_account_idx");

            entity.HasIndex(e => e.SourceId, "fk_loyalty_sourceid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_loyalty_statusid_core_idx");

            entity.HasIndex(e => e.StoreId, "fk_loyalty_storeid_account_idx");

            entity.HasIndex(e => e.TerminalId, "fk_loyalty_terminalid_terminal_idx");

            entity.HasIndex(e => e.ToAccountId, "fk_loyalty_toaccountid_account_idx");

            entity.HasIndex(e => e.ToReferrerId, "fk_loyalty_torefid_account_idx");

            entity.HasIndex(e => e.TypeId, "fk_loyalty_typeid_core_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(128);
            entity.Property(e => e.BinNumber).HasMaxLength(64);
            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InvoiceNumber).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Acquirer).WithMany(p => p.TUCLoyaltyAcquirer)
                .HasForeignKey(d => d.AcquirerId)
                .HasConstraintName("fk_loyalty_acquirerid_account");

            entity.HasOne(d => d.BinNumberNavigation).WithMany(p => p.TUCLoyalty)
                .HasForeignKey(d => d.BinNumberId)
                .HasConstraintName("fk_loyalty_binid_bin");

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCLoyaltyCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_loyalty_cashierid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCLoyaltyCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_loyalty_createdbyid_account");

            entity.HasOne(d => d.Customer).WithMany(p => p.TUCLoyaltyCustomer)
                .HasForeignKey(d => d.CustomerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_customerid_account");

            entity.HasOne(d => d.FromAccount).WithMany(p => p.TUCLoyaltyFromAccount)
                .HasForeignKey(d => d.FromAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_fromaccountid_account");

            entity.HasOne(d => d.FromReferrer).WithMany(p => p.TUCLoyaltyFromReferrer)
                .HasForeignKey(d => d.FromReferrerId)
                .HasConstraintName("fk_loyalty_fromrefid_account");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCLoyaltyMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_loyalty_merchantid_account");

            entity.HasOne(d => d.Mode).WithMany(p => p.TUCLoyaltyMode)
                .HasForeignKey(d => d.ModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_modeid_core");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCLoyaltyModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_loyalty_modifybyid_account");

            entity.HasOne(d => d.PaymentMethod).WithMany(p => p.TUCLoyaltyPaymentMethod)
                .HasForeignKey(d => d.PaymentMethodId)
                .HasConstraintName("fk_loyalty_pmethodid_core");

            entity.HasOne(d => d.Provider).WithMany(p => p.TUCLoyaltyProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("fk_loyalty_providerid_account");

            entity.HasOne(d => d.Source).WithMany(p => p.TUCLoyaltySource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_sourceid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCLoyaltyStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_statusid_core");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCLoyaltyStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_loyalty_storeid_account");

            entity.HasOne(d => d.Terminal).WithMany(p => p.TUCLoyalty)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_loyalty_terminalid_terminal");

            entity.HasOne(d => d.ToAccount).WithMany(p => p.TUCLoyaltyToAccount)
                .HasForeignKey(d => d.ToAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_toaccountid_account");

            entity.HasOne(d => d.ToReferrer).WithMany(p => p.TUCLoyaltyToReferrer)
                .HasForeignKey(d => d.ToReferrerId)
                .HasConstraintName("fk_loyalty_torefid_account");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCLoyaltyType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_loyalty_typeid_core");
        });

        modelBuilder.Entity<TUCLoyaltyMerchantCustomer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.SourceId, "fk_tuclmercccust_sourceid_idx");

            entity.HasIndex(e => e.CustomerId, "fk_tuclmerccust_customerid_account_idx");

            entity.HasIndex(e => e.MerchantId, "fk_tuclmerccust_merchantid_account_idx");

            entity.HasIndex(e => e.ProgramId, "fk_tuclmerccust_programid_idx");

            entity.Property(e => e.Balance).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Credit).HasDefaultValueSql("'0'");
            entity.Property(e => e.Debit).HasDefaultValueSql("'0'");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InvoiceAmount).HasDefaultValueSql("'0'");
            entity.Property(e => e.LastTransactionDate).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Customer).WithMany(p => p.TUCLoyaltyMerchantCustomerCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("fk_tuclmerccust_customerid_account");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCLoyaltyMerchantCustomerMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_tuclmerccust_merchantid_account");

            entity.HasOne(d => d.Program).WithMany(p => p.TUCLoyaltyMerchantCustomer)
                .HasForeignKey(d => d.ProgramId)
                .HasConstraintName("fk_tuclmerccust_programid");

            entity.HasOne(d => d.Source).WithMany(p => p.TUCLoyaltyMerchantCustomer)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("fk_tuclmercccust_sourceid");
        });

        modelBuilder.Entity<TUCLoyaltyPending>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AcquirerId, "fk_ployalty_acquirerid_account_idx");

            entity.HasIndex(e => e.CashierId, "fk_ployalty_cashierid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_ployalty_createdbyid_account_idx");

            entity.HasIndex(e => e.CustomerId, "fk_ployalty_customerid_account_idx");

            entity.HasIndex(e => e.FromAccountId, "fk_ployalty_fromaccountid_account_idx");

            entity.HasIndex(e => e.LoyaltyTypeId, "fk_ployalty_loyaltytypeid_helper_idx");

            entity.HasIndex(e => e.MerchantId, "fk_ployalty_merchantid_account_idx");

            entity.HasIndex(e => e.ModeId, "fk_ployalty_modeid_helper_idx");

            entity.HasIndex(e => e.ModifyById, "fk_ployalty_modifybyid_account_idx");

            entity.HasIndex(e => e.ProviderId, "fk_ployalty_providerid_account_idx");

            entity.HasIndex(e => e.SourceId, "fk_ployalty_sourceid_helper_idx");

            entity.HasIndex(e => e.StatusId, "fk_ployalty_statusid_helper_idx");

            entity.HasIndex(e => e.StoreId, "fk_ployalty_storeid_account_idx");

            entity.HasIndex(e => e.TerminalId, "fk_ployalty_terminalid_terminal_idx");

            entity.HasIndex(e => e.ToAccountId, "fk_ployalty_toacountid_account_idx");

            entity.HasIndex(e => e.TransactionId, "fk_ployalty_transactionid_transaction_idx");

            entity.HasIndex(e => e.TypeId, "fk_ployalty_typeid_helper_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InvoiceNumber).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.SyncDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Acquirer).WithMany(p => p.TUCLoyaltyPendingAcquirer)
                .HasForeignKey(d => d.AcquirerId)
                .HasConstraintName("fk_ployalty_acquirerid_account");

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCLoyaltyPendingCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_ployalty_cashierid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCLoyaltyPendingCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_ployalty_createdbyid_account");

            entity.HasOne(d => d.Customer).WithMany(p => p.TUCLoyaltyPendingCustomer)
                .HasForeignKey(d => d.CustomerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_customerid_account");

            entity.HasOne(d => d.FromAccount).WithMany(p => p.TUCLoyaltyPendingFromAccount)
                .HasForeignKey(d => d.FromAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_fromaccountid_account");

            entity.HasOne(d => d.LoyaltyType).WithMany(p => p.TUCLoyaltyPendingLoyaltyType)
                .HasForeignKey(d => d.LoyaltyTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_loyaltytypeid_helper");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCLoyaltyPendingMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_ployalty_merchantid_account");

            entity.HasOne(d => d.Mode).WithMany(p => p.TUCLoyaltyPendingMode)
                .HasForeignKey(d => d.ModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_modeid_helper");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCLoyaltyPendingModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_ployalty_modifybyid_account");

            entity.HasOne(d => d.Provider).WithMany(p => p.TUCLoyaltyPendingProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("fk_ployalty_providerid_account");

            entity.HasOne(d => d.Source).WithMany(p => p.TUCLoyaltyPendingSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_sourceid_helper");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCLoyaltyPendingStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_statusid_helper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCLoyaltyPendingStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_ployalty_storeid_account");

            entity.HasOne(d => d.Terminal).WithMany(p => p.TUCLoyaltyPending)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_ployalty_terminalid_terminal");

            entity.HasOne(d => d.ToAccount).WithMany(p => p.TUCLoyaltyPendingToAccount)
                .HasForeignKey(d => d.ToAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_toacountid_account");

            entity.HasOne(d => d.Transaction).WithMany(p => p.TUCLoyaltyPending)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("fk_ployalty_transactionid_transaction");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCLoyaltyPendingType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ployalty_typeid_helper");
        });

        modelBuilder.Entity<TUCMerchantCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.RootCategoryId, "fk_mcat_catid_tuccat_idx");

            entity.HasIndex(e => e.CreatedById, "fk_mcat_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_mcat_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_mcat_statusid_core_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCMerchantCategoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_mcat_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCMerchantCategoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_mcat_modifybyid_acc");

            entity.HasOne(d => d.RootCategory).WithMany(p => p.TUCMerchantCategory)
                .HasForeignKey(d => d.RootCategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_mcat_catid_tuccat");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCMerchantCategory)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_mcat_statusid_core");
        });

        modelBuilder.Entity<TUCNinjaRegistration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.StatusId, "fk_tucnreg_statusid_helper_idx");

            entity.HasIndex(e => e.ImageStorageId, "fk_tunreg_imagestorageid_storage_idx");

            entity.HasIndex(e => e.AccountId, "fk_tureg_accountid_account_idx");

            entity.HasIndex(e => e.UserAccountId, "fk_tureg_accountid_account_idx1");

            entity.HasIndex(e => e.CityArea1Id, "fk_tureg_cityareaid1_parameter_idx");

            entity.HasIndex(e => e.CityArea2Id, "fk_tureg_cityareaid2_parameter_idx");

            entity.HasIndex(e => e.CityArea3Id, "fk_tureg_cityareaid3_parameter_idx");

            entity.HasIndex(e => e.CityArea4Id, "fk_tureg_cityareaid4_parameter_idx");

            entity.HasIndex(e => e.CityId, "fk_tureg_cityid_parameter_idx");

            entity.HasIndex(e => e.CountryId, "fk_tureg_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_tureg_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tureg_modifybyid_account_idx");

            entity.HasIndex(e => e.ProofStorageId, "fk_tureg_proofstorageid_storage_idx");

            entity.HasIndex(e => e.RegionId, "fk_tureg_regionid_parameter_idx");

            entity.HasIndex(e => e.UtilityBillStorageId, "fk_tureg_utilitystorageid_storage_idx");

            entity.Property(e => e.Address).HasMaxLength(512);
            entity.Property(e => e.BankAccountName).HasMaxLength(128);
            entity.Property(e => e.BankAccountNumber).HasMaxLength(64);
            entity.Property(e => e.BankCode).HasMaxLength(28);
            entity.Property(e => e.BankName).HasMaxLength(128);
            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EmailAddress).HasMaxLength(256);
            entity.Property(e => e.FacebookUrl).HasMaxLength(1024);
            entity.Property(e => e.FirstName).HasMaxLength(128);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InstagramUrl).HasMaxLength(1024);
            entity.Property(e => e.LastName).HasMaxLength(128);
            entity.Property(e => e.LinkedInUrl).HasMaxLength(1024);
            entity.Property(e => e.MobileNumber).HasMaxLength(16);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Reference1ContactNumber).HasMaxLength(16);
            entity.Property(e => e.Reference1Name).HasMaxLength(256);
            entity.Property(e => e.Reference2ContactNumber).HasMaxLength(16);
            entity.Property(e => e.Reference2Name).HasMaxLength(256);
            entity.Property(e => e.TwitterUrl).HasMaxLength(1024);
            entity.Property(e => e.VehicleNumber).HasMaxLength(64);

            entity.HasOne(d => d.Account).WithMany(p => p.TUCNinjaRegistrationAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_tureg_accountid_account");

            entity.HasOne(d => d.CityArea1).WithMany(p => p.TUCNinjaRegistrationCityArea1)
                .HasForeignKey(d => d.CityArea1Id)
                .HasConstraintName("fk_tureg_cityareaid1_parameter");

            entity.HasOne(d => d.CityArea2).WithMany(p => p.TUCNinjaRegistrationCityArea2)
                .HasForeignKey(d => d.CityArea2Id)
                .HasConstraintName("fk_tureg_cityareaid2_parameter");

            entity.HasOne(d => d.CityArea3).WithMany(p => p.TUCNinjaRegistrationCityArea3)
                .HasForeignKey(d => d.CityArea3Id)
                .HasConstraintName("fk_tureg_cityareaid3_parameter");

            entity.HasOne(d => d.CityArea4).WithMany(p => p.TUCNinjaRegistrationCityArea4)
                .HasForeignKey(d => d.CityArea4Id)
                .HasConstraintName("fk_tureg_cityareaid4_parameter");

            entity.HasOne(d => d.City).WithMany(p => p.TUCNinjaRegistration)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("fk_tureg_cityid_parameter");

            entity.HasOne(d => d.Country).WithMany(p => p.TUCNinjaRegistration)
                .HasForeignKey(d => d.CountryId)
                .HasConstraintName("fk_tureg_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCNinjaRegistrationCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_tureg_createdbyid_account");

            entity.HasOne(d => d.ImageStorage).WithMany(p => p.TUCNinjaRegistrationImageStorage)
                .HasForeignKey(d => d.ImageStorageId)
                .HasConstraintName("fk_tunreg_imagestorageid_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCNinjaRegistrationModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tureg_modifybyid_account");

            entity.HasOne(d => d.ProofStorage).WithMany(p => p.TUCNinjaRegistrationProofStorage)
                .HasForeignKey(d => d.ProofStorageId)
                .HasConstraintName("fk_tureg_proofstorageid_storage");

            entity.HasOne(d => d.Region).WithMany(p => p.TUCNinjaRegistration)
                .HasForeignKey(d => d.RegionId)
                .HasConstraintName("fk_tureg_regionid_parameter");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCNinjaRegistration)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_tucnreg_statusid_helper");

            entity.HasOne(d => d.UtilityBillStorage).WithMany(p => p.TUCNinjaRegistrationUtilityBillStorage)
                .HasForeignKey(d => d.UtilityBillStorageId)
                .HasConstraintName("fk_tureg_utilitystorageid_storage");
        });

        modelBuilder.Entity<TUCPayOut>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.ModifyById, "fk_payour_modifybyid_account_idx");

            entity.HasIndex(e => e.AccountId, "fk_payout_accountid_account_idx");

            entity.HasIndex(e => e.BankId, "fk_payout_bankid_bank_idx");

            entity.HasIndex(e => e.CreatedById, "fk_payout_createdbyid_account_idx");

            entity.HasIndex(e => e.SourceId, "fk_payout_sourceid_helper_idx");

            entity.HasIndex(e => e.StatusId, "fk_payout_statusid_helper_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(128);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.ExtReferenceId).HasMaxLength(64);
            entity.Property(e => e.ExtReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SystemComment).HasMaxLength(128);
            entity.Property(e => e.UserComment).HasMaxLength(128);

            entity.HasOne(d => d.Account).WithMany(p => p.TUCPayOutAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_payout_accountid_account");

            entity.HasOne(d => d.Bank).WithMany(p => p.TUCPayOut)
                .HasForeignKey(d => d.BankId)
                .HasConstraintName("fk_payout_bankid_bank");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCPayOutCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_payout_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCPayOutModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_payout_modifybyid_account");

            entity.HasOne(d => d.Source).WithMany(p => p.TUCPayOutSource)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("fk_payout_sourceid_helper");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCPayOutStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_payout_statusid_helper");
        });

        modelBuilder.Entity<TUCPromoCode>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountTypeId, "fk_promo_acctypeid_core_idx");

            entity.HasIndex(e => e.ConditionId, "fk_promo_cond_condition_idx");

            entity.HasIndex(e => e.CountryId, "fk_promo_countryid_country_idx");

            entity.HasIndex(e => e.CreatedById, "fk_promo_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_promo_modifybyid_acc_idx");

            entity.HasIndex(e => e.StatusId, "fk_promo_statusid_core_idx");

            entity.HasIndex(e => e.TypeId, "fk_promo_typeid_core_idx");

            entity.Property(e => e.Code).HasMaxLength(64);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(256);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.MaximumLimitPerUser).HasDefaultValueSql("'1'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(128);

            entity.HasOne(d => d.AccountType).WithMany(p => p.TUCPromoCodeAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promo_acctypeid_core");

            entity.HasOne(d => d.Condition).WithMany(p => p.TUCPromoCode)
                .HasForeignKey(d => d.ConditionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promo_cond_condition");

            entity.HasOne(d => d.Country).WithMany(p => p.TUCPromoCode)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promo_countryid_country");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCPromoCodeCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promo_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCPromoCodeModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_promo_modifybyid_acc");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCPromoCodeStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promo_statusid_core");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCPromoCodeType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_promo_typeid_core");
        });

        modelBuilder.Entity<TUCPromoCodeAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountId, "fk_pcode_acc_acc_idx");

            entity.HasIndex(e => e.PromoCodeId, "fk_pcode_acc_pcode_idx");

            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.UseDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCPromoCodeAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pcode_acc_acc");

            entity.HasOne(d => d.PromoCode).WithMany(p => p.TUCPromoCodeAccount)
                .HasForeignKey(d => d.PromoCodeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pcode_acc_pcode");
        });

        modelBuilder.Entity<TUCPromoCodeCondition>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.AccountTypeId, "fk_promocon_acctypeid_core_idx");

            entity.HasIndex(e => e.CountryId, "fk_promocon_countryid_idx");

            entity.HasIndex(e => e.HelperId, "fk_promocon_helperid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_promocon_statusid_core_idx");

            entity.Property(e => e.Description).HasMaxLength(256);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.AccountType).WithMany(p => p.TUCPromoCodeConditionAccountType)
                .HasForeignKey(d => d.AccountTypeId)
                .HasConstraintName("fk_promocon_acctypeid_core");

            entity.HasOne(d => d.Country).WithMany(p => p.TUCPromoCodeCondition)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promocon_countryid");

            entity.HasOne(d => d.Helper).WithMany(p => p.TUCPromoCodeConditionHelper)
                .HasForeignKey(d => d.HelperId)
                .HasConstraintName("fk_promocon_helperid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCPromoCodeConditionStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_promocon_statusid_core");
        });

        modelBuilder.Entity<TUCReceiptScan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CashierId, "FK_CashierId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.CreatedBy, "FK_CreatedBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.InoviceStorageId, "FK_InoviceStorageId_HCStorage_idx");

            entity.HasIndex(e => e.MerchantId, "FK_MerchantId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.ModifyBy, "FK_ModifyBy_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.StoreId, "FK_StoreId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_URI_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.UserAccountId, "FK_UserAccountId_HCUserAccount_Id_idx");

            entity.Property(e => e.Comment).HasMaxLength(1024);
            entity.Property(e => e.CreateDate).HasMaxLength(6);
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.InoviceNumber).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasMaxLength(6);

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCReceiptScanCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("FK_CashierId_HCUserAccount_Id");

            entity.HasOne(d => d.CreatedByNavigation).WithMany(p => p.TUCReceiptScanCreatedByNavigation)
                .HasForeignKey(d => d.CreatedBy)
                .HasConstraintName("FK_CreatedBy_HCUserAccount_Id");

            entity.HasOne(d => d.InoviceStorage).WithMany(p => p.TUCReceiptScan)
                .HasForeignKey(d => d.InoviceStorageId)
                .HasConstraintName("FK_InoviceStorageId_HCStorage");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCReceiptScanMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("FK_MerchantId_HCUserAccount_Id");

            entity.HasOne(d => d.ModifyByNavigation).WithMany(p => p.TUCReceiptScanModifyByNavigation)
                .HasForeignKey(d => d.ModifyBy)
                .HasConstraintName("FK_ModifyBy_HCUserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCReceiptScan)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_URI_StatusId_CoreHelper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCReceiptScanStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("FK_StoreId_HCUserAccount_Id");

            entity.HasOne(d => d.UserAccount).WithMany(p => p.TUCReceiptScanUserAccount)
                .HasForeignKey(d => d.UserAccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserAccountId_HCUserAccount_Id");
        });

        modelBuilder.Entity<TUCSale>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_Index").IsUnique();

            entity.HasIndex(e => e.TransactionDate, "TransactionDate_Index");

            entity.HasIndex(e => e.CustomerId, "fk_tucsale_accountid_account_idx");

            entity.HasIndex(e => e.BinNumberId, "fk_tucsale_binid_bin_idx");

            entity.HasIndex(e => e.CashierId, "fk_tucsale_cashierid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_tucsale_createdbyid_account_idx");

            entity.HasIndex(e => e.MerchantId, "fk_tucsale_merchantid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tucsale_modifybyid_account_idx");

            entity.HasIndex(e => e.PaymentModeId, "fk_tucsale_paymentmodeid_core_idx");

            entity.HasIndex(e => e.SourceId, "fk_tucsale_sourceid_core_idx");

            entity.HasIndex(e => e.StatusId, "fk_tucsale_statusid_core_idx");

            entity.HasIndex(e => e.StoreId, "fk_tucsale_storeid_account_idx");

            entity.HasIndex(e => e.TerminalId, "fk_tucsale_terminalid_terminal_idx");

            entity.HasIndex(e => e.TransactionId, "fk_tucsale_transactionid_transaction_idx");

            entity.HasIndex(e => e.TypeId, "fk_tucsale_typeid_core_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(64);
            entity.Property(e => e.BinNumber).HasMaxLength(32);
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.InvoiceNumber).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(64);
            entity.Property(e => e.StatusMessage).HasMaxLength(256);
            entity.Property(e => e.SyncDate)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.TransactionDate).HasColumnType("datetime");

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCSaleCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_tucsale_cashierid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCSaleCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_tucsale_createdbyid_account");

            entity.HasOne(d => d.Customer).WithMany(p => p.TUCSaleCustomer)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("fk_tucsale_accountid_account");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCSaleMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_tucsale_merchantid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCSaleModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tucsale_modifybyid_account");

            entity.HasOne(d => d.PaymentMode).WithMany(p => p.TUCSalePaymentMode)
                .HasForeignKey(d => d.PaymentModeId)
                .HasConstraintName("fk_tucsale_paymentmodeid_core");

            entity.HasOne(d => d.Source).WithMany(p => p.TUCSaleSource)
                .HasForeignKey(d => d.SourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucsale_sourceid_core");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCSaleStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucsale_statusid_core");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCSaleStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_tucsale_storeid_account");

            entity.HasOne(d => d.Terminal).WithMany(p => p.TUCSale)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_tucsale_terminalid_terminal");

            entity.HasOne(d => d.Transaction).WithMany(p => p.TUCSale)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("fk_tucsale_transactionid_transaction");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCSaleType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucsale_typeid_core");
        });

        modelBuilder.Entity<TUCStoreAcquirerCollection>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.AcquirerId, "fk_stcoll_acquirerid_useraccount_idx");

            entity.HasIndex(e => e.CreatedById, "fk_stcoll_createdbyid_useraccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_stcoll_modifybyud_useraccount_idx");

            entity.HasIndex(e => e.StatusId, "fk_stcoll_statusid_corehelper_idx");

            entity.HasIndex(e => e.StoreId, "fk_stcoll_storeid_useraccount_idx");

            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Date).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Acquirer).WithMany(p => p.TUCStoreAcquirerCollectionAcquirer)
                .HasForeignKey(d => d.AcquirerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_stcoll_acquirerid_useraccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCStoreAcquirerCollectionCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_stcoll_createdbyid_useraccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCStoreAcquirerCollectionModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_stcoll_modifybyud_useraccount");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCStoreAcquirerCollection)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_stcoll_statusid_corehelper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCStoreAcquirerCollectionStore)
                .HasForeignKey(d => d.StoreId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_stcoll_storeid_useraccount");
        });

        modelBuilder.Entity<TUCTerminal>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.MerchantRmId, "fk_Terminal_merchantrmid_account_idx");

            entity.HasIndex(e => e.AccountId, "fk_terminal_accountid_account_idx");

            entity.HasIndex(e => e.AcquirerBranchId, "fk_terminal_acquirerbranchid_branch_idx");

            entity.HasIndex(e => e.AcquirerId, "fk_terminal_acquirerid_account_idx");

            entity.HasIndex(e => e.AcquirerRmId, "fk_terminal_acquirerrmid_account_idx");

            entity.HasIndex(e => e.ActivityStatusId, "fk_terminal_activitystatusid_helper_idx");

            entity.HasIndex(e => e.CashierId, "fk_terminal_cashierid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_terminal_createdbyid_account_idx");

            entity.HasIndex(e => e.MerchantId, "fk_terminal_merchantid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_terminal_modifiedbyid_account_idx");

            entity.HasIndex(e => e.ProviderId, "fk_terminal_providerid_account_idx");

            entity.HasIndex(e => e.ProviderRmId, "fk_terminal_providerrmid_account_idx");

            entity.HasIndex(e => e.ReceiptStorageId, "fk_terminal_receiptimage_storage_idx");

            entity.HasIndex(e => e.StatusId, "fk_terminal_statusid_helper_idx");

            entity.HasIndex(e => e.StoreId, "fk_terminal_storeid_account_idx");

            entity.HasIndex(e => e.TypeId, "fk_terminal_typeid_helper_id_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(32);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.IdentificationNumber).HasMaxLength(32);
            entity.Property(e => e.LastActivityDate).HasColumnType("datetime");
            entity.Property(e => e.LastTransactionDate).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.SerialNumber).HasMaxLength(32);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCTerminalAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_terminal_accountid_account");

            entity.HasOne(d => d.AcquirerBranch).WithMany(p => p.TUCTerminal)
                .HasForeignKey(d => d.AcquirerBranchId)
                .HasConstraintName("fk_terminal_acquirerbranchid_branch");

            entity.HasOne(d => d.Acquirer).WithMany(p => p.TUCTerminalAcquirer)
                .HasForeignKey(d => d.AcquirerId)
                .HasConstraintName("fk_terminal_acquirerid_account");

            entity.HasOne(d => d.AcquirerRm).WithMany(p => p.TUCTerminalAcquirerRm)
                .HasForeignKey(d => d.AcquirerRmId)
                .HasConstraintName("fk_terminal_acquirerrmid_account");

            entity.HasOne(d => d.ActivityStatus).WithMany(p => p.TUCTerminalActivityStatus)
                .HasForeignKey(d => d.ActivityStatusId)
                .HasConstraintName("fk_terminal_activitystatusid_helper");

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCTerminalCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("fk_terminal_cashierid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCTerminalCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_terminal_createdbyid_account");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TUCTerminalMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("fk_terminal_merchantid_account");

            entity.HasOne(d => d.MerchantRm).WithMany(p => p.TUCTerminalMerchantRm)
                .HasForeignKey(d => d.MerchantRmId)
                .HasConstraintName("fk_terminal_merchantrmid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCTerminalModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_terminal_modifiedbyid_account");

            entity.HasOne(d => d.Provider).WithMany(p => p.TUCTerminalProvider)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("fk_terminal_providerid_account");

            entity.HasOne(d => d.ProviderRm).WithMany(p => p.TUCTerminalProviderRm)
                .HasForeignKey(d => d.ProviderRmId)
                .HasConstraintName("fk_terminal_providerrmid_account");

            entity.HasOne(d => d.ReceiptStorage).WithMany(p => p.TUCTerminal)
                .HasForeignKey(d => d.ReceiptStorageId)
                .HasConstraintName("fk_terminal_receiptimage_storage");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCTerminalStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_terminal_statusid_helper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCTerminalStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("fk_terminal_storeid_account");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCTerminalType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_terminal_typeid_helper");
        });

        modelBuilder.Entity<TUCTerminalProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_tproduct_AccountId_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_tproduct_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tproduct_modifybyid_Account_idx");

            entity.HasIndex(e => e.StatusId, "fk_tproduct_statusid_helper_idx");

            entity.Property(e => e.CategoryName).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.ReferenceNumber).HasMaxLength(128);
            entity.Property(e => e.Sku).HasMaxLength(128);
            entity.Property(e => e.SubCategoryName).HasMaxLength(128);
            entity.Property(e => e.SystemName).HasMaxLength(256);

            entity.HasOne(d => d.Account).WithMany(p => p.TUCTerminalProductAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tproduct_AccountId_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCTerminalProductCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tproduct_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCTerminalProductModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tproduct_modifybyid_Account");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCTerminalProduct)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tproduct_statusid_helper");
        });

        modelBuilder.Entity<TUCTerminalStatus>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.TerminalId, "fk_tuterminalstatus_terminalid_useraccount_idx");

            entity.Property(e => e.Date).HasColumnType("datetime");

            entity.HasOne(d => d.Terminal).WithMany(p => p.TUCTerminalStatus)
                .HasForeignKey(d => d.TerminalId)
                .HasConstraintName("fk_tuterminalstatus_terminalid_useraccount");
        });

        modelBuilder.Entity<TUCampaign>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CreatedById, "fk_tucampaign_createdbyid_useraccount_idx");

            entity.HasIndex(e => e.ManagerId, "fk_tucampaign_managerid_useraccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_tucampaign_modifybyid_useraccount_idx");

            entity.HasIndex(e => e.StatusId, "fk_tucampaign_statusid_corehelper_idx");

            entity.HasIndex(e => e.SubTypeId, "fk_tucampaign_subtypeid_corehelper_idx");

            entity.HasIndex(e => e.TypeId, "fk_tucampaign_typeid_corehelper_idx");

            entity.HasIndex(e => e.AccountId, "fk_tucampaign_useraccountid_useraccount_idx");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.SmsText).HasMaxLength(256);
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUCampaignAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucampaign_useraccountid_useraccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCampaignCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucampaign_createdbyid_useraccount");

            entity.HasOne(d => d.Manager).WithMany(p => p.TUCampaignManager)
                .HasForeignKey(d => d.ManagerId)
                .HasConstraintName("fk_tucampaign_managerid_useraccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCampaignModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_tucampaign_modifybyid_useraccount");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCampaignStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucampaign_statusid_corehelper");

            entity.HasOne(d => d.SubType).WithMany(p => p.TUCampaignSubType)
                .HasForeignKey(d => d.SubTypeId)
                .HasConstraintName("fk_tucampaign_subtypeid_corehelper");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCampaignType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_tucampaign_typeid_corehelper");
        });

        modelBuilder.Entity<TUCampaignAudience>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CampaignId, "fk_campaud_campaignid_campaign_idx");

            entity.HasIndex(e => e.CardTypeId, "fk_campaudience_cardtypeid_coreparameter_idx");

            entity.HasIndex(e => e.CreatedById, "fk_campaudience_createdbyid_useraccount_idx");

            entity.HasIndex(e => e.ModifyById, "fk_campaudience_modifybyid_useraccount_idx");

            entity.HasIndex(e => e.StatusId, "fk_campaudience_statusid_idx");

            entity.HasIndex(e => e.TypeId, "fk_campaudience_typeid_corehelper_idx");

            entity.HasIndex(e => e.UserAccountId, "fk_campaudience_useraccountid_useraccount_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Campaign).WithMany(p => p.TUCampaignAudience)
                .HasForeignKey(d => d.CampaignId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campaudience_campaignid_campaign");

            entity.HasOne(d => d.CardType).WithMany(p => p.TUCampaignAudience)
                .HasForeignKey(d => d.CardTypeId)
                .HasConstraintName("fk_campaudience_cardtypeid_coreparameter");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUCampaignAudienceCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campaudience_createdbyid_useraccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUCampaignAudienceModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_campaudience_modifybyid_useraccount");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCampaignAudienceStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campaudience_statusid");

            entity.HasOne(d => d.Type).WithMany(p => p.TUCampaignAudienceType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_campaudience_typeid_corehelper");

            entity.HasOne(d => d.UserAccount).WithMany(p => p.TUCampaignAudienceUserAccount)
                .HasForeignKey(d => d.UserAccountId)
                .HasConstraintName("fk_campaudience_useraccountid_useraccount");
        });

        modelBuilder.Entity<TUCard>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CardTypeId, "FK_TUCA_CardTagTypeId_CoreHelper");

            entity.HasIndex(e => e.CardTagTypeId, "FK_TUCA_CardTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.CashierId, "FK_TUCA_CashierId_Account_idx");

            entity.HasIndex(e => e.PrepaidCardTypeId, "FK_TUCA_PrepaidCardType_CoreHelper_idx");

            entity.HasIndex(e => e.StatusId, "FK_TUCA_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.StoreId, "FK_TUCA_StoreId_Account_idx");

            entity.HasIndex(e => e.ActiveMerchantId, "FK_TUCard_MerchantId_HCUserAccount_Id_idx");

            entity.HasIndex(e => e.ActiveUserAccountId, "FK_TUCard_UserAccountId_HCUAcc_Id_idx");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.Property(e => e.CardNumber).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.ExpiaryDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(45);
            entity.Property(e => e.LastActivity).HasColumnType("datetime");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.ReferenceNumber).HasMaxLength(256);
            entity.Property(e => e.SerialNumber).HasMaxLength(128);

            entity.HasOne(d => d.ActiveMerchant).WithMany(p => p.TUCardActiveMerchant)
                .HasForeignKey(d => d.ActiveMerchantId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUCA_MerchantId_HCUAcc_Id");

            entity.HasOne(d => d.ActiveUserAccount).WithMany(p => p.TUCardActiveUserAccount)
                .HasForeignKey(d => d.ActiveUserAccountId)
                .HasConstraintName("FK_TUCA_UserAccountId_HCUAcc_Id");

            entity.HasOne(d => d.CardTagType).WithMany(p => p.TUCardCardTagType)
                .HasForeignKey(d => d.CardTagTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUCA_CardTypeId_CoreHelper");

            entity.HasOne(d => d.CardType).WithMany(p => p.TUCardCardType)
                .HasForeignKey(d => d.CardTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUCA_CardTagTypeId_CoreHelper");

            entity.HasOne(d => d.Cashier).WithMany(p => p.TUCardCashier)
                .HasForeignKey(d => d.CashierId)
                .HasConstraintName("FK_TUCA_CashierId_Account");

            entity.HasOne(d => d.PrepaidCardType).WithMany(p => p.TUCardPrepaidCardType)
                .HasForeignKey(d => d.PrepaidCardTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUCA_PrepaidCardType_CoreHelper");

            entity.HasOne(d => d.Status).WithMany(p => p.TUCardStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUCA_StatusId_CoreHelper");

            entity.HasOne(d => d.Store).WithMany(p => p.TUCardStore)
                .HasForeignKey(d => d.StoreId)
                .HasConstraintName("FK_TUCA_StoreId_Account");
        });

        modelBuilder.Entity<TULead>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.AccountId, "FK_Lead_AccountId_UserAccount_Id_idx");

            entity.HasIndex(e => e.AgentId, "FK_Lead_AgentId_UserAccount_Id_idx");

            entity.HasIndex(e => e.CreatedById, "FK_Lead_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_Lead_ModifyById_UserAccount_Id_idx");

            entity.HasIndex(e => e.OwnerId, "FK_Lead_OwnerId_UserAccount_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_Lead_StatusId_CoreHelper_Id_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyById).HasDefaultValueSql("'2'");
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartTime).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TULeadAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_Lead_AccountId_UserAccount_Id");

            entity.HasOne(d => d.Agent).WithMany(p => p.TULeadAgent)
                .HasForeignKey(d => d.AgentId)
                .HasConstraintName("FK_Lead_AgentId_UserAccount_Id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TULeadCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_Lead_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TULeadModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_Lead_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Owner).WithMany(p => p.TULeadOwner)
                .HasForeignKey(d => d.OwnerId)
                .HasConstraintName("FK_Lead_OwnerId_UserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.TULead)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Lead_StatusId_CoreHelper_Id");
        });

        modelBuilder.Entity<TULeadActivity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.StatusId, "FK_LeadStatusId_Helper_Id_idx");

            entity.HasIndex(e => e.CreatedById, "FK_LeadStatus_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.LeadId, "FK_LeadStatus_LeadId_Lead_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_LeadStatus_ModifyById_UserAccount_Id_idx");

            entity.Property(e => e.Comment).HasMaxLength(512);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Title).HasMaxLength(256);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TULeadActivityCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_LeadStatus_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Lead).WithMany(p => p.TULeadActivity)
                .HasForeignKey(d => d.LeadId)
                .HasConstraintName("FK_LeadStatus_LeadId_Lead_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TULeadActivityModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_LeadStatus_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.TULeadActivity)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_LeadStatusId_Helper_Id");
        });

        modelBuilder.Entity<TULeadGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.AgentId, "FK_TULG_AgentId_UserAccount_idx");

            entity.HasIndex(e => e.CreatedById, "FK_TULG_CreatedById_UserAccount_idx");

            entity.HasIndex(e => e.MerchantId, "FK_TULG_MerchantId_UserAccount_idx");

            entity.HasIndex(e => e.ModifyById, "FK_TULG_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.StatusId, "FK_TULG_StatusId_CoreHelper_idx");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(256);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Agent).WithMany(p => p.TULeadGroupAgent)
                .HasForeignKey(d => d.AgentId)
                .HasConstraintName("FK_TULG_AgentId_UserAccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TULeadGroupCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_TULG_CreatedById_UserAccount");

            entity.HasOne(d => d.Merchant).WithMany(p => p.TULeadGroupMerchant)
                .HasForeignKey(d => d.MerchantId)
                .HasConstraintName("FK_TULG_MerchantId_UserAccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TULeadGroupModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_TULG_ModifyById_UserAccount");

            entity.HasOne(d => d.Status).WithMany(p => p.TULeadGroup)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_TULG_StatusId_CoreHelper");
        });

        modelBuilder.Entity<TULeadOwner>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.AgentId, "FK_LeadOwner_AgentId_UserAccount_Id_idx");

            entity.HasIndex(e => e.CreatedById, "FK_LeadOwner_CreatedById_UserAccount_Id_idx");

            entity.HasIndex(e => e.LeadId, "FK_LeadOwner_LeadId_Lead_Id_idx");

            entity.HasIndex(e => e.ModifyById, "FK_LeadOwner_ModifyById_UserAccount_Id_idx");

            entity.HasIndex(e => e.StatusId, "FK_LeadOwner_StatusId_Helper_Id_idx");

            entity.Property(e => e.Comment).HasMaxLength(256);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(256);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.Agent).WithMany(p => p.TULeadOwnerAgent)
                .HasForeignKey(d => d.AgentId)
                .HasConstraintName("FK_LeadOwner_AgentId_UserAccount_Id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TULeadOwnerCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_LeadOwner_CreatedById_UserAccount_Id");

            entity.HasOne(d => d.Lead).WithMany(p => p.TULeadOwner)
                .HasForeignKey(d => d.LeadId)
                .HasConstraintName("FK_LeadOwner_LeadId_Lead_Id");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TULeadOwnerModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_LeadOwner_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.TULeadOwner)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_LeadOwner_StatusId_Helper_Id");
        });

        modelBuilder.Entity<TUProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.CreatedById, "FK_TUProduct_CreatedById_UserAccount_idx");

            entity.HasIndex(e => e.IconStorageId, "FK_TUProduct_IconStorageId_CoreStorage_idx");

            entity.HasIndex(e => e.ModifyById, "FK_TUProduct_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.ParentId, "FK_TUProduct_ParentId_Product_idx");

            entity.HasIndex(e => e.RewardTypeId, "FK_TUProduct_RewardTypeId_CoreHelper_idx");

            entity.HasIndex(e => e.StatusId, "FK_TUProduct_StatusId_CoreHelper_idx");

            entity.HasIndex(e => e.TypeId, "FK_TUProduct_TypeId_CoreHelper_Id_idx");

            entity.HasIndex(e => e.AccountId, "FK_TUProduct_UserAccountId_UserAccount_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(512);
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.Quantity).HasDefaultValueSql("'0'");
            entity.Property(e => e.RewardValue).HasDefaultValueSql("'0'");
            entity.Property(e => e.StartTime).HasColumnType("datetime");
            entity.Property(e => e.SystemName).HasMaxLength(256);
            entity.Property(e => e.UnitPrice).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Account).WithMany(p => p.TUProductAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_TUProduct_UserAccountId_UserAccount");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUProductCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_TUProduct_CreatedById_UserAccount");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.TUProduct)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("FK_TUProduct_IconStorageId_CoreStorage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUProductModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_TUProduct_ModifyById_UserAccount");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_TUProduct_ParentId_Product");

            entity.HasOne(d => d.RewardType).WithMany(p => p.TUProductRewardType)
                .HasForeignKey(d => d.RewardTypeId)
                .HasConstraintName("FK_TUProduct_RewardTypeId_CoreHelper");

            entity.HasOne(d => d.Status).WithMany(p => p.TUProductStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUProduct_StatusId_CoreHelper");

            entity.HasOne(d => d.Type).WithMany(p => p.TUProductType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUProduct_TypeId_CoreHelper_Id");
        });

        modelBuilder.Entity<TUProductCode>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.HasIndex(e => e.BatchId, "FK_TUProductCode_BatchId_TUProduct_idx");

            entity.HasIndex(e => e.CreatedById, "FK_TUProductCode_CreatedById_UserAccount_idx");

            entity.HasIndex(e => e.ModifyById, "FK_TUProductCode_ModifyById_UserAccount_idx");

            entity.HasIndex(e => e.StatusId, "FK_TUProductCode_StatusId_UserAccount_idx");

            entity.HasIndex(e => e.TransactionId, "FK_TUProductCode_Transaction_Id_idx");

            entity.HasIndex(e => e.TypeId, "FK_TUProductCode_TypeId_CoreHelper_idx");

            entity.HasIndex(e => e.AccountId, "FK_TUProductCode_UserAccountId_UserAccount_idx");

            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ItemCode).HasMaxLength(128);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.UseDate).HasColumnType("datetime");

            entity.HasOne(d => d.Account).WithMany(p => p.TUProductCodeAccount)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_TUProductCode_UserAccountId_UserAccount");

            entity.HasOne(d => d.Batch).WithMany(p => p.TUProductCode)
                .HasForeignKey(d => d.BatchId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUProductCode_BatchId_TUProduct");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.TUProductCodeCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_TUProductCode_CreatedById_UserAccount");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.TUProductCodeModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("FK_TUProductCode_ModifyById_UserAccount");

            entity.HasOne(d => d.Status).WithMany(p => p.TUProductCodeStatus)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUProductCode_StatusId_UserAccount");

            entity.HasOne(d => d.Transaction).WithMany(p => p.TUProductCode)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("FK_TUProductCode_Transaction_Id");

            entity.HasOne(d => d.Type).WithMany(p => p.TUProductCodeType)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TUProductCode_TypeId_CoreHelper");
        });

        modelBuilder.Entity<VASCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_vascategor_createdbyid_account_idx");

            entity.HasIndex(e => e.CountryId, "fk_vascategory_countryid_common_idx");

            entity.HasIndex(e => e.ModifyById, "fk_vascategory_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_vascategory_statusid_helper_idx");

            entity.Property(e => e.CountryId).HasDefaultValueSql("'1'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceKey).HasMaxLength(128);
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(128);

            entity.HasOne(d => d.Country).WithMany(p => p.VASCategory)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vascategory_countryid_common");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.VASCategoryCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vascategor_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.VASCategoryModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_vascategory_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.VASCategory)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vascategory_statusid_helper");
        });

        modelBuilder.Entity<VASProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CategoryId, "fk_categoryid_vascategory_idx");

            entity.HasIndex(e => e.CreatedById, "fk_vasproduct_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_vasproduct_modifybyid_account_idx");

            entity.HasIndex(e => e.StatusId, "fk_vasproduct_statusid_helper_idx");

            entity.HasIndex(e => e.IconStorageId, "fk_vasproduct_storageid_storage_idx");

            entity.Property(e => e.Amount).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceKey).HasMaxLength(128);
            entity.Property(e => e.RewardPercentage).HasDefaultValueSql("'0'");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(128);
            entity.Property(e => e.UserPercentage).HasDefaultValueSql("'0'");

            entity.HasOne(d => d.Category).WithMany(p => p.VASProduct)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_categoryid_vascategory");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.VASProductCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vasproduct_createdbyid_account");

            entity.HasOne(d => d.IconStorage).WithMany(p => p.VASProduct)
                .HasForeignKey(d => d.IconStorageId)
                .HasConstraintName("fk_vasproduct_storageid_storage");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.VASProductModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_vasproduct_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.VASProduct)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vasproduct_statusid_helper");
        });

        modelBuilder.Entity<VASProductItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.CreatedById, "fk_vasproductitem_createdbyid_account_idx");

            entity.HasIndex(e => e.ModifyById, "fk_vasproductitem_modifybyid_account_idx");

            entity.HasIndex(e => e.ProductId, "fk_vasproductitem_productid_product_idx");

            entity.HasIndex(e => e.StatusId, "fk_vasproductitem_statusid_helper_idx");

            entity.Property(e => e.Amount).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(128);
            entity.Property(e => e.ReferenceKey).HasMaxLength(128);
            entity.Property(e => e.RewardPercentage).HasDefaultValueSql("'0'");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");
            entity.Property(e => e.SystemName).HasMaxLength(128);
            entity.Property(e => e.UserPercentage).HasDefaultValueSql("'0'");
            entity.Property(e => e.Validity).HasMaxLength(64);

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.VASProductItemCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vasproductitem_createdbyid_account");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.VASProductItemModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_vasproductitem_modifybyid_account");

            entity.HasOne(d => d.Product).WithMany(p => p.VASProductItem)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vasproductitem_productid_product");

            entity.HasOne(d => d.Status).WithMany(p => p.VASProductItem)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_vasproductitem_statusid_helper");
        });

        modelBuilder.Entity<VasPayment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .HasCharSet("utf8mb3")
                .UseCollation("utf8_general_ci");

            entity.HasIndex(e => e.Guid, "Guid_UNIQUE").IsUnique();

            entity.HasIndex(e => e.Id, "Id_UNIQUE").IsUnique();

            entity.HasIndex(e => e.AccountId, "fk_bpayment_accountid_account_idx");

            entity.HasIndex(e => e.CreatedById, "fk_bpayment_createdbyid_acc_idx");

            entity.HasIndex(e => e.ModifyById, "fk_bpayment_modifybyid_account_idx");

            entity.HasIndex(e => e.TransactionId, "fk_bpayment_trid_tran_idx");

            entity.HasIndex(e => e.ProductId, "fk_bppayment_productid_vprod_idx");

            entity.HasIndex(e => e.ProductItemId, "fk_bppayment_productitemid_proditem_idx");

            entity.HasIndex(e => e.StatusId, "fk_bppayment_statusid_helper_idx");

            entity.HasIndex(e => e.TypeId, "fk_bppayment_typeid_helper_idx");

            entity.Property(e => e.AccountNumber).HasMaxLength(128);
            entity.Property(e => e.BillerName).HasMaxLength(128);
            entity.Property(e => e.Comment).HasMaxLength(128);
            entity.Property(e => e.CreateDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Guid).HasMaxLength(64);
            entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            entity.Property(e => e.OrderId).HasMaxLength(32);
            entity.Property(e => e.PackageName).HasMaxLength(128);
            entity.Property(e => e.PaymentReference).HasMaxLength(128);
            entity.Property(e => e.PaymentSource).HasMaxLength(64);
            entity.Property(e => e.PaymentToken).HasMaxLength(128);
            entity.Property(e => e.ResponseMessage).HasMaxLength(128);
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.SyncTime)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("timestamp");

            entity.HasOne(d => d.Account).WithMany(p => p.VasPaymentAccount)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_bpayment_accountid_account");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.VasPaymentCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("fk_bpayment_createdbyid_acc");

            entity.HasOne(d => d.ModifyBy).WithMany(p => p.VasPaymentModifyBy)
                .HasForeignKey(d => d.ModifyById)
                .HasConstraintName("fk_bpayment_modifybyid_account");

            entity.HasOne(d => d.Product).WithMany(p => p.VasPayment)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("fk_bppayment_productid_vprod");

            entity.HasOne(d => d.ProductItem).WithMany(p => p.VasPayment)
                .HasForeignKey(d => d.ProductItemId)
                .HasConstraintName("fk_bppayment_productitemid_proditem");

            entity.HasOne(d => d.Status).WithMany(p => p.VasPaymentStatus)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("fk_bppayment_statusid_helper");

            entity.HasOne(d => d.Transaction).WithMany(p => p.VasPayment)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("fk_bpayment_trid_tran");

            entity.HasOne(d => d.Type).WithMany(p => p.VasPaymentType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("fk_bppayment_typeid_helper");
        });

        modelBuilder.Entity<cmt_loyalty>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasIndex(e => e.acquirer_id, "fk_cmtloyalty_acquirerid_acc_idx");

            entity.HasIndex(e => e.bin_number_id, "fk_cmtloyalty_binnumberid_bin_idx");

            entity.HasIndex(e => e.cashier_id, "fk_cmtloyalty_cashierid_acc_idx");

            entity.HasIndex(e => e.created_by_id, "fk_cmtloyalty_createdbyid_acc_idx");

            entity.HasIndex(e => e.customer_id, "fk_cmtloyalty_customerid_acc_idx");

            entity.HasIndex(e => e.from_account_id, "fk_cmtloyalty_fromaccountid_acc_idx");

            entity.HasIndex(e => e.loyalty_type_id, "fk_cmtloyalty_loyaltytypeid_core_idx");

            entity.HasIndex(e => e.merchant_id, "fk_cmtloyalty_merchantid_acc_idx");

            entity.HasIndex(e => e.modify_by_id, "fk_cmtloyalty_modifybyid_acc_idx");

            entity.HasIndex(e => e.payment_mode_id, "fk_cmtloyalty_paymentmodeid_core_idx");

            entity.HasIndex(e => e.provider_id, "fk_cmtloyalty_providerid_acc_idx");

            entity.HasIndex(e => e.source_id, "fk_cmtloyalty_sourceid_core_idx");

            entity.HasIndex(e => e.status_id, "fk_cmtloyalty_statusid_core_idx");

            entity.HasIndex(e => e.store_id, "fk_cmtloyalty_storeid_acc_idx");

            entity.HasIndex(e => e.terminal_id, "fk_cmtloyalty_terminalid_terminal_idx");

            entity.HasIndex(e => e.to_account_id, "fk_cmtloyalty_toaccountid_acc_idx");

            entity.HasIndex(e => e.transaction_id, "fk_cmtloyalty_transactionid_tran_idx");

            entity.HasIndex(e => e.type_id, "fk_cmtloyalty_typeid_core_idx");

            entity.Property(e => e.account_number).HasMaxLength(64);
            entity.Property(e => e.bin_number).HasMaxLength(64);
            entity.Property(e => e.comment).HasMaxLength(512);
            entity.Property(e => e.create_date).HasColumnType("datetime");
            entity.Property(e => e.guid).HasMaxLength(64);
            entity.Property(e => e.invoice_number).HasMaxLength(64);
            entity.Property(e => e.modify_date).HasColumnType("datetime");
            entity.Property(e => e.reference_number).HasMaxLength(128);
            entity.Property(e => e.status_message).HasMaxLength(128);
            entity.Property(e => e.transaction_date).HasColumnType("datetime");

            entity.HasOne(d => d.acquirer).WithMany(p => p.cmt_loyaltyacquirer)
                .HasForeignKey(d => d.acquirer_id)
                .HasConstraintName("fk_cmtloyalty_acquirerid_acc");

            entity.HasOne(d => d.bin_numberNavigation).WithMany(p => p.cmt_loyalty)
                .HasForeignKey(d => d.bin_number_id)
                .HasConstraintName("fk_cmtloyalty_binnumberid_bin");

            entity.HasOne(d => d.cashier).WithMany(p => p.cmt_loyaltycashier)
                .HasForeignKey(d => d.cashier_id)
                .HasConstraintName("fk_cmtloyalty_cashierid_acc");

            entity.HasOne(d => d.created_by).WithMany(p => p.cmt_loyaltycreated_by)
                .HasForeignKey(d => d.created_by_id)
                .HasConstraintName("fk_cmtloyalty_createdbyid_acc");

            entity.HasOne(d => d.customer).WithMany(p => p.cmt_loyaltycustomer)
                .HasForeignKey(d => d.customer_id)
                .HasConstraintName("fk_cmtloyalty_customerid_acc");

            entity.HasOne(d => d.from_account).WithMany(p => p.cmt_loyaltyfrom_account)
                .HasForeignKey(d => d.from_account_id)
                .HasConstraintName("fk_cmtloyalty_fromaccountid_acc");

            entity.HasOne(d => d.loyalty_type).WithMany(p => p.cmt_loyaltyloyalty_type)
                .HasForeignKey(d => d.loyalty_type_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtloyalty_loyaltytypeid_core");

            entity.HasOne(d => d.merchant).WithMany(p => p.cmt_loyaltymerchant)
                .HasForeignKey(d => d.merchant_id)
                .HasConstraintName("fk_cmtloyalty_merchantid_acc");

            entity.HasOne(d => d.modify_by).WithMany(p => p.cmt_loyaltymodify_by)
                .HasForeignKey(d => d.modify_by_id)
                .HasConstraintName("fk_cmtloyalty_modifybyid_acc");

            entity.HasOne(d => d.payment_mode).WithMany(p => p.cmt_loyaltypayment_mode)
                .HasForeignKey(d => d.payment_mode_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtloyalty_paymentmodeid_core");

            entity.HasOne(d => d.provider).WithMany(p => p.cmt_loyaltyprovider)
                .HasForeignKey(d => d.provider_id)
                .HasConstraintName("fk_cmtloyalty_providerid_acc");

            entity.HasOne(d => d.source).WithMany(p => p.cmt_loyaltysource)
                .HasForeignKey(d => d.source_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtloyalty_sourceid_core");

            entity.HasOne(d => d.status).WithMany(p => p.cmt_loyaltystatus)
                .HasForeignKey(d => d.status_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtloyalty_statusid_core");

            entity.HasOne(d => d.store).WithMany(p => p.cmt_loyaltystore)
                .HasForeignKey(d => d.store_id)
                .HasConstraintName("fk_cmtloyalty_storeid_acc");

            entity.HasOne(d => d.terminal).WithMany(p => p.cmt_loyalty)
                .HasForeignKey(d => d.terminal_id)
                .HasConstraintName("fk_cmtloyalty_terminalid_terminal");

            entity.HasOne(d => d.to_account).WithMany(p => p.cmt_loyaltyto_account)
                .HasForeignKey(d => d.to_account_id)
                .HasConstraintName("fk_cmtloyalty_toaccountid_acc");

            entity.HasOne(d => d.transaction).WithMany(p => p.cmt_loyalty)
                .HasForeignKey(d => d.transaction_id)
                .HasConstraintName("fk_cmtloyalty_transactionid_tran");

            entity.HasOne(d => d.type).WithMany(p => p.cmt_loyaltytype)
                .HasForeignKey(d => d.type_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtloyalty_typeid_core");
        });

        modelBuilder.Entity<cmt_loyalty_customer>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasIndex(e => e.customer_id, "fk_cmtlcustomer_customerid_acc_idx");

            entity.HasIndex(e => e.owner_id, "fk_cmtlcustomer_ownerid_acc_idx");

            entity.HasIndex(e => e.source_id, "fk_cmtlcustomer_sourceid_core_idx");

            entity.HasIndex(e => e.status_id, "fk_cmtlcustomer_statusid_core_idx");

            entity.Property(e => e.create_date).HasColumnType("datetime");
            entity.Property(e => e.guid).HasMaxLength(64);
            entity.Property(e => e.last_transaction_date).HasColumnType("datetime");
            entity.Property(e => e.modify_date).HasColumnType("datetime");
            entity.Property(e => e.sync_time)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");

            entity.HasOne(d => d.customer).WithMany(p => p.cmt_loyalty_customercustomer)
                .HasForeignKey(d => d.customer_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtlcustomer_customerid_acc");

            entity.HasOne(d => d.owner).WithMany(p => p.cmt_loyalty_customerowner)
                .HasForeignKey(d => d.owner_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtlcustomer_ownerid_acc");

            entity.HasOne(d => d.source).WithMany(p => p.cmt_loyalty_customersource)
                .HasForeignKey(d => d.source_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtlcustomer_sourceid_core");

            entity.HasOne(d => d.status).WithMany(p => p.cmt_loyalty_customerstatus)
                .HasForeignKey(d => d.status_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtlcustomer_statusid_core");
        });

        modelBuilder.Entity<cmt_sale>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasIndex(e => e.acquirer_id, "fk_cmtsale_acquirerid_acc_idx");

            entity.HasIndex(e => e.bin_number_id, "fk_cmtsale_binnumber_bin_idx");

            entity.HasIndex(e => e.cashier_id, "fk_cmtsale_cashierid_acc_idx");

            entity.HasIndex(e => e.created_by_id, "fk_cmtsale_createdbyid_acc_idx");

            entity.HasIndex(e => e.customer_id, "fk_cmtsale_custid_acc_idx");

            entity.HasIndex(e => e.merchant_id, "fk_cmtsale_merchantid_acc_idx");

            entity.HasIndex(e => e.modify_by_id, "fk_cmtsale_modifybyid_acc_idx");

            entity.HasIndex(e => e.payment_mode_id, "fk_cmtsale_pmodeid_core_idx");

            entity.HasIndex(e => e.provider_id, "fk_cmtsale_providerid_acc_idx");

            entity.HasIndex(e => e.source_id, "fk_cmtsale_sourceid_core_idx");

            entity.HasIndex(e => e.status_id, "fk_cmtsale_statusid_core_idx");

            entity.HasIndex(e => e.store_id, "fk_cmtsale_storeid_acc_idx");

            entity.HasIndex(e => e.terminal_id, "fk_cmtsale_terminalid_acc_idx");

            entity.HasIndex(e => e.transaction_id, "fk_cmtsale_tranid_tran_idx");

            entity.HasIndex(e => e.type_id, "fk_cmtsale_typeid_core_idx");

            entity.Property(e => e.account_number).HasMaxLength(64);
            entity.Property(e => e.bin_number).HasMaxLength(45);
            entity.Property(e => e.create_date).HasColumnType("datetime");
            entity.Property(e => e.guid).HasMaxLength(64);
            entity.Property(e => e.invoice_number).HasMaxLength(64);
            entity.Property(e => e.modify_date).HasColumnType("datetime");
            entity.Property(e => e.reference_number).HasMaxLength(45);
            entity.Property(e => e.status_message).HasMaxLength(128);
            entity.Property(e => e.sync_time)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.transaction_date).HasColumnType("datetime");

            entity.HasOne(d => d.acquirer).WithMany(p => p.cmt_saleacquirer)
                .HasForeignKey(d => d.acquirer_id)
                .HasConstraintName("fk_cmtsale_acquirerid_acc");

            entity.HasOne(d => d.bin_numberNavigation).WithMany(p => p.cmt_sale)
                .HasForeignKey(d => d.bin_number_id)
                .HasConstraintName("fk_cmtsale_binnumber_bin");

            entity.HasOne(d => d.cashier).WithMany(p => p.cmt_salecashier)
                .HasForeignKey(d => d.cashier_id)
                .HasConstraintName("fk_cmtsale_cashierid_acc");

            entity.HasOne(d => d.created_by).WithMany(p => p.cmt_salecreated_by)
                .HasForeignKey(d => d.created_by_id)
                .HasConstraintName("fk_cmtsale_createdbyid_acc");

            entity.HasOne(d => d.customer).WithMany(p => p.cmt_salecustomer)
                .HasForeignKey(d => d.customer_id)
                .HasConstraintName("fk_cmtsale_customerid_acc");

            entity.HasOne(d => d.merchant).WithMany(p => p.cmt_salemerchant)
                .HasForeignKey(d => d.merchant_id)
                .HasConstraintName("fk_cmtsale_merchantid_acc");

            entity.HasOne(d => d.modify_by).WithMany(p => p.cmt_salemodify_by)
                .HasForeignKey(d => d.modify_by_id)
                .HasConstraintName("fk_cmtsale_modifybyid_acc");

            entity.HasOne(d => d.payment_mode).WithMany(p => p.cmt_salepayment_mode)
                .HasForeignKey(d => d.payment_mode_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsale_pmodeid_core");

            entity.HasOne(d => d.provider).WithMany(p => p.cmt_saleprovider)
                .HasForeignKey(d => d.provider_id)
                .HasConstraintName("fk_cmtsale_providerid_acc");

            entity.HasOne(d => d.source).WithMany(p => p.cmt_salesource)
                .HasForeignKey(d => d.source_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsale_sourceid_core");

            entity.HasOne(d => d.status).WithMany(p => p.cmt_salestatus)
                .HasForeignKey(d => d.status_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsale_statusid_core");

            entity.HasOne(d => d.store).WithMany(p => p.cmt_salestore)
                .HasForeignKey(d => d.store_id)
                .HasConstraintName("fk_cmtsale_storeid_acc");

            entity.HasOne(d => d.terminal).WithMany(p => p.cmt_sale)
                .HasForeignKey(d => d.terminal_id)
                .HasConstraintName("fk_cmtsale_terminalid_acc");

            entity.HasOne(d => d.transaction).WithMany(p => p.cmt_sale)
                .HasForeignKey(d => d.transaction_id)
                .HasConstraintName("fk_cmtsale_tranid_tran");

            entity.HasOne(d => d.type).WithMany(p => p.cmt_saletype)
                .HasForeignKey(d => d.type_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsale_typeid_core");
        });

        modelBuilder.Entity<cmt_sale_failed>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasIndex(e => e.acquirer_id, "fk_cmtsalefailed_acquirerid_acc_idx");

            entity.HasIndex(e => e.bin_number_id, "fk_cmtsalefailed_binnumber_bin_idx");

            entity.HasIndex(e => e.cashier_id, "fk_cmtsalefailed_cashierid_acc_idx");

            entity.HasIndex(e => e.created_by_id, "fk_cmtsalefailed_createdbyid_acc_idx");

            entity.HasIndex(e => e.customer_id, "fk_cmtsalefailed_custid_acc_idx");

            entity.HasIndex(e => e.merchant_id, "fk_cmtsalefailed_merchantid_acc_idx");

            entity.HasIndex(e => e.modify_by_id, "fk_cmtsalefailed_modifybyid_acc_idx");

            entity.HasIndex(e => e.payment_mode_id, "fk_cmtsalefailed_pmodeid_core_idx");

            entity.HasIndex(e => e.provider_id, "fk_cmtsalefailed_providerid_acc_idx");

            entity.HasIndex(e => e.source_id, "fk_cmtsalefailed_sourceid_core_idx");

            entity.HasIndex(e => e.status_id, "fk_cmtsalefailed_statusid_core_idx");

            entity.HasIndex(e => e.store_id, "fk_cmtsalefailed_storeid_acc_idx");

            entity.HasIndex(e => e.terminal_id, "fk_cmtsalefailed_terminalid_acc_idx");

            entity.HasIndex(e => e.transaction_id, "fk_cmtsalefailed_tranid_core_idx");

            entity.HasIndex(e => e.type_id, "fk_cmtsalefailed_typeid_core_idx");

            entity.Property(e => e.account_number).HasMaxLength(64);
            entity.Property(e => e.bin_number).HasMaxLength(45);
            entity.Property(e => e.create_date).HasColumnType("datetime");
            entity.Property(e => e.guid).HasMaxLength(64);
            entity.Property(e => e.invoice_number).HasMaxLength(64);
            entity.Property(e => e.modify_date).HasColumnType("datetime");
            entity.Property(e => e.reference_number).HasMaxLength(45);
            entity.Property(e => e.status_message).HasMaxLength(128);
            entity.Property(e => e.sync_time)
                .ValueGeneratedOnAddOrUpdate()
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.transaction_date).HasColumnType("datetime");

            entity.HasOne(d => d.acquirer).WithMany(p => p.cmt_sale_failedacquirer)
                .HasForeignKey(d => d.acquirer_id)
                .HasConstraintName("fk_cmtsalefailed_acquirerid_acc");

            entity.HasOne(d => d.bin_numberNavigation).WithMany(p => p.cmt_sale_failed)
                .HasForeignKey(d => d.bin_number_id)
                .HasConstraintName("fk_cmtsalefailed_binnumber_bin");

            entity.HasOne(d => d.cashier).WithMany(p => p.cmt_sale_failedcashier)
                .HasForeignKey(d => d.cashier_id)
                .HasConstraintName("fk_cmtsalefailed_cashierid_acc");

            entity.HasOne(d => d.created_by).WithMany(p => p.cmt_sale_failedcreated_by)
                .HasForeignKey(d => d.created_by_id)
                .HasConstraintName("fk_cmtsalefailed_createdbyid_acc");

            entity.HasOne(d => d.customer).WithMany(p => p.cmt_sale_failedcustomer)
                .HasForeignKey(d => d.customer_id)
                .HasConstraintName("fk_cmtsalefailed_customerid_acc");

            entity.HasOne(d => d.merchant).WithMany(p => p.cmt_sale_failedmerchant)
                .HasForeignKey(d => d.merchant_id)
                .HasConstraintName("fk_cmtsalefailed_merchantid_acc");

            entity.HasOne(d => d.modify_by).WithMany(p => p.cmt_sale_failedmodify_by)
                .HasForeignKey(d => d.modify_by_id)
                .HasConstraintName("fk_cmtsalefailed_modifybyid_acc");

            entity.HasOne(d => d.payment_mode).WithMany(p => p.cmt_sale_failedpayment_mode)
                .HasForeignKey(d => d.payment_mode_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsalefailed_pmodeid_core");

            entity.HasOne(d => d.provider).WithMany(p => p.cmt_sale_failedprovider)
                .HasForeignKey(d => d.provider_id)
                .HasConstraintName("fk_cmtsalefailed_providerid_acc");

            entity.HasOne(d => d.source).WithMany(p => p.cmt_sale_failedsource)
                .HasForeignKey(d => d.source_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsalefailed_sourceid_core");

            entity.HasOne(d => d.status).WithMany(p => p.cmt_sale_failedstatus)
                .HasForeignKey(d => d.status_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsalefailed_statusid_core");

            entity.HasOne(d => d.store).WithMany(p => p.cmt_sale_failedstore)
                .HasForeignKey(d => d.store_id)
                .HasConstraintName("fk_cmtsalefailed_storeid_acc");

            entity.HasOne(d => d.terminal).WithMany(p => p.cmt_sale_failed)
                .HasForeignKey(d => d.terminal_id)
                .HasConstraintName("fk_cmtsalefailed_terminalid_acc");

            entity.HasOne(d => d.transaction).WithMany(p => p.cmt_sale_failed)
                .HasForeignKey(d => d.transaction_id)
                .HasConstraintName("fk_cmtsalefailed_tranid_core");

            entity.HasOne(d => d.type).WithMany(p => p.cmt_sale_failedtype)
                .HasForeignKey(d => d.type_id)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_cmtsalefailed_typeid_core");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
