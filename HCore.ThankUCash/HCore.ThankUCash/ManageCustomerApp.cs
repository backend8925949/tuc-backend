//==================================================================================
// FileName: ManageCustomerApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.FrameworkCustomerApp;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash
{
    public class ManageCustomerApp
    {
        FrameworkCAppMerchant _FrameworkCAppMerchant;
        /// <summary>
        /// Gets the nearby store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNearbyStore(OList.Request _Request)
        {
            _FrameworkCAppMerchant = new FrameworkCAppMerchant();
            return _FrameworkCAppMerchant.GetNearbyStore(_Request);
        }
    }
}
