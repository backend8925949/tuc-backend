//==================================================================================
// FileName: FrameworkTucPayResource.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.FrameworkTucPay
{
    public class TucPayResource
    {
        public const string TUCP500 = "TUCP500";
        public const string TUCP500M = "Unable to process request. Please try after some time";
        public const string TUCP401 = "TUCP401";
        public const string TUCP401M = "Your account is blocked or suspended. Please contact support";


        // Series 400
        #region Series 400
        public const string TUCP402 = "TUCP402";
        public const string TUCP402M = "Unable to verify your payment.";
        public const string TUCP403 = "TUCP403";
        public const string TUCP403M = "Unable to process your request. Amount will be credited to your account within 24 hours";
        public const string TUCP404 = "TUCP404";
        public const string TUCP404M = "Unable to find payment details";
        #endregion
        // Series 200
        #region Series 200
        public const string TUCP200 = "TUCP200";
        public const string TUCP200M = "Payment successful. Points credited to your account";
        public const string TUCP201 = "TUCP201";
        public const string TUCP201M = "Payment cancelled successfully";
        #endregion

        //public const string TUCP403 = "TUCP403";
        //public const string TUCP403M = "Unable to verify your payment";
    }
}
