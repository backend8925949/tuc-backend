//==================================================================================
// FileName: FrameworkBuyPoints.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to buypoints
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using RestSharp;
using HCore.Operations.Object;
using HCore.Operations;
using System.Security.Cryptography;
using HCore.Integration.Paystack;

namespace HCore.ThankUCash.FrameworkTucPay
{
    public class FrameworkBuyPoints
    {
        //internal const int UserCard = 465;

        internal class TransactionTypeReward
        {
            internal const int PointPurchase = 567;
            //internal const int BillPayment = 568;
            //internal const int LCCTopup = 569;
        }
        //internal class TransactionTypeRedeem
        //{
        //    internal const int BillPayment = 479;
        //    internal const int LccTopup = 480;
        //}


        //internal const long TransactionSourcePayment = 471;
        private const int PaymentTypePointPurchase = 564;
        //private const long PaymentTypeBillPayment = 565;
        //private const long PaymentTypeLccTopup = 566;
        VasPayment _VasPayment;
        HCoreContext _HCoreContext;
        OTucPay.BuyPoint.Initilize.Response _InitializeResponse;
        OTucPay.BuyPoint.Confirm.Response _ConfirmResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        /// <summary>
        /// Buys the point initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPoint_Initialize(OTucPay.BuyPoint.Initilize.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TucPayResource.TUCP401, TucPayResource.TUCP401M);
                    }
                    _VasPayment = new VasPayment();
                    _VasPayment.Guid = "TUCBP" + _Request.UserReference.AccountId + "O" + HCoreHelper.GenerateDateString();
                    _VasPayment.ProductItemId = 1;
                    _VasPayment.TypeId = PaymentTypePointPurchase;
                    _VasPayment.AccountId = _Request.UserReference.AccountId;
                    _VasPayment.AccountNumber = _Request.UserReference.AccountCode;
                    _VasPayment.Amount = _Request.Amount;
                    _VasPayment.RewardAmount = 0;
                    _VasPayment.PaymentSource = "payment";
                    _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                    _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                    _VasPayment.CreatedById = _Request.UserReference.AccountId;
                    _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                    _HCoreContext.VasPayment.Add(_VasPayment);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    _InitializeResponse = new OTucPay.BuyPoint.Initilize.Response();
                    _InitializeResponse.ReferenceId = _VasPayment.Id;
                    _InitializeResponse.ReferenceKey = _VasPayment.Guid;
                    _InitializeResponse.Amount = _Request.Amount;
                    _InitializeResponse.InitializeDate = _VasPayment.CreateDate;
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _InitializeResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BuyPoint_Initialize", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, TucPayResource.TUCP500, TucPayResource.TUCP500M);
            }
            #endregion
        }
        /// <summary>
        /// Buys the point confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPoint_Confirm(OTucPay.BuyPoint.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TucPayResource.TUCP401, TucPayResource.TUCP401M);
                    }
                    _HCoreContext.Dispose();
                    OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.ReferenceKey, _AppConfig.PaystackPrivateKey);
                    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                    {
                        #region Operation
                        using (_HCoreContext = new HCoreContext())
                        {
                            var BillPayment = _HCoreContext.VasPayment.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            if (BillPayment != null)
                            {
                                BillPayment.ModifyDate = HCoreHelper.GetGMTDateTime();
                                BillPayment.ModifyById = _Request.UserReference.AccountId;
                                BillPayment.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                _HCoreContext.SaveChanges();
                            }
                            _HCoreContext.Dispose();
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.CustomerId = BillPayment.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = 3;
                            _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.amount / 100;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.amount / 100;
                            if (_PayStackResponseData.authorization != null)
                            {
                                _CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                            }
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _PayStackResponseData.amount / 100;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = BillPayment.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeReward.PointPurchase,
                                SourceId = TransactionSource.TUC,
                                Amount = _PayStackResponseData.amount / 100,
                                Charge = 0,
                                TotalAmount = _PayStackResponseData.amount / 100,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BillPaymentUpdate = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                    BillPaymentUpdate.TransactionId = TransactionResponse.ReferenceId;
                                    BillPaymentUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentUpdate.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentUpdate.StatusId = HelperStatus.BillPaymentStatus.Success;
                                    BillPaymentUpdate.EndDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    _ConfirmResponse = new OTucPay.BuyPoint.Confirm.Response();
                                    _ConfirmResponse.ReferenceId = BillPaymentUpdate.Id;
                                    _ConfirmResponse.ReferenceKey = BillPaymentUpdate.Guid;
                                    _ConfirmResponse.Status = "Success";
                                    _ConfirmResponse.Amount = (double)BillPaymentUpdate.Amount;
                                    _ConfirmResponse.InitializeDate = BillPaymentUpdate.CreateDate;
                                    _ConfirmResponse.ConfirmDate = BillPaymentUpdate.ModifyDate;
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ConfirmResponse, TucPayResource.TUCP200, TucPayResource.TUCP200M);
                                    #endregion
                                }
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BillPaymentUpdate = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                    if (TransactionResponse.ReferenceId != 0)
                                    {
                                        BillPayment.TransactionId = TransactionResponse.ReferenceId;
                                    }
                                    BillPaymentUpdate.EndDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentUpdate.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentUpdate.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                    _HCoreContext.SaveChanges();
                                    _ConfirmResponse = new OTucPay.BuyPoint.Confirm.Response();
                                    _ConfirmResponse.ReferenceId = BillPaymentUpdate.Id;
                                    _ConfirmResponse.ReferenceKey = BillPaymentUpdate.Guid;
                                    _ConfirmResponse.Status = "Success";
                                    _ConfirmResponse.Amount = (double)BillPaymentUpdate.Amount;
                                    _ConfirmResponse.InitializeDate = BillPaymentUpdate.CreateDate;
                                    _ConfirmResponse.ConfirmDate = BillPaymentUpdate.ModifyDate;
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _ConfirmResponse, TucPayResource.TUCP403, TucPayResource.TUCP403M);
                                    #endregion
                                }

                            }
                        }
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TucPayResource.TUCP402, TucPayResource.TUCP402M);
                    }

                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BuyPoint_Confirm", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, TucPayResource.TUCP500, TucPayResource.TUCP500M);
            }
            #endregion
        }
        /// <summary>
        /// Buys the point cancel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPoint_Cancel(OTucPay.BuyPoint.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TucPayResource.TUCP401, TucPayResource.TUCP401M);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var BillPaymentUpdate = _HCoreContext.VasPayment.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                        if (BillPaymentUpdate != null)
                        {
                            BillPaymentUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                            BillPaymentUpdate.ModifyById = _Request.UserReference.AccountId;
                            BillPaymentUpdate.StatusId = HelperStatus.BillPaymentStatus.Cancelled;
                            BillPaymentUpdate.EndDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();

                            _ConfirmResponse = new OTucPay.BuyPoint.Confirm.Response();
                            _ConfirmResponse.ReferenceId = BillPaymentUpdate.Id;
                            _ConfirmResponse.ReferenceKey = BillPaymentUpdate.Guid;
                            _ConfirmResponse.Status = "Cancelled";
                            _ConfirmResponse.Amount = (double)BillPaymentUpdate.Amount;
                            _ConfirmResponse.InitializeDate = BillPaymentUpdate.CreateDate;
                            _ConfirmResponse.ConfirmDate = BillPaymentUpdate.ModifyDate;
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _ConfirmResponse, TucPayResource.TUCP201, TucPayResource.TUCP201M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _ConfirmResponse, TucPayResource.TUCP404, TucPayResource.TUCP404M);
                        }
                    }


                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BuyPoint_Confirm", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, TucPayResource.TUCP500, TucPayResource.TUCP500M);
            }
            #endregion
        }

    }
}
