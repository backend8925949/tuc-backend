//==================================================================================
// FileName: FrameworkProductManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to Product Manager
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using HCore.Object;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Operations;
using HCore.Operations.Object;

namespace HCore.ThankUCash.CAProductManager
{
    public class FrameworkProductManager
    {
        HCoreContext _HCoreContext;
        CAProduct _CAProduct;
        CAProductCode _CAProductCode;
        List<CAProductCode> _CAProductCodes;
        //OProduct.ProductCodeProcess _ProductCodeProcess;
        //TUProduct _TUProduct;
        //ManageUserTransaction _ManageUserTransaction;
        OCAProduct.BalanceResponse _BalanceResponse;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCore.Operations.Object.OUserAccount.Request _UserAccountCreateRequest;
        /// <summary>
        /// Description: Gets the product balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductBalance(OCAProduct.BalanceRequest _Request)
        {
            _BalanceResponse = new OCAProduct.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                long TransactionSourceId = 0;
                if (_Request.SourceCode == TransactionSource.GiftCardsS)
                {
                    TransactionSourceId = TransactionSource.GiftCards;
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Invalid transaction source");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.AccountId == _Request.UserAccountId
                       && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                       && x.ModeId == TransactionMode.Credit)
                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                      .Where(x => x.AccountId == _Request.UserAccountId
                      && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                      && x.ModeId == TransactionMode.Debit)
                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetProductBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Credits the product account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditProductAccount(OCAProduct.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OCAProduct.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            AccountTypeId = x.AccountTypeId,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                double CommissionPercentage = 0;
                                double SystemAmount = 0;
                                double UserAmount = 0;
                                int TransactionSourceId = 0;
                                int TransactionTypeId = 0;
                                string ConvinenceChargeConfiguration = "giftcardsconvinencecharges";
                                if (_Request.SourceCode == TransactionSource.GiftCardsS)
                                {
                                    TransactionSourceId = TransactionSource.GiftCards;
                                    TransactionTypeId = TransactionType.GiftCard;
                                    ConvinenceChargeConfiguration = "giftcardsconvinencecharges";
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Invalid transaction source");
                                    #endregion
                                }
                                string TConvincenceCharges = HCoreHelper.GetConfiguration(ConvinenceChargeConfiguration, AccountDetails.Id);
                                if (!string.IsNullOrEmpty(TConvincenceCharges))
                                {
                                    CommissionPercentage = Convert.ToDouble(TConvincenceCharges);
                                    SystemAmount = HCoreHelper.GetPercentage(_Request.Amount, CommissionPercentage);
                                    UserAmount = _Request.Amount - SystemAmount;
                                }
                                else
                                {
                                    SystemAmount = 0;
                                    UserAmount = _Request.Amount;
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSourceId,
                                    Comment = _Request.Comment,
                                    Amount = _Request.Amount,
                                    Comission = SystemAmount,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                           .Where(x => x.AccountId == _Request.UserAccountId
                                           && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                           && x.ModeId == TransactionMode.Credit)
                                           .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.UserAccountId
                                          && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Amount credited to account");
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "This feature is only available for merchants");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                        }

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditGiftPoints", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveProduct(OCAProduct.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                int? SubTypeId = HCoreHelper.GetSystemHelperId(_Request.SubTypeCode, _Request.UserReference);
                long? AccountId = HCoreHelper.GetUserAccountId(_Request.AccountKey, _Request.UserReference);
                int? UsageTypeId = HCoreHelper.GetSystemHelperId(_Request.UsageTypeCode, _Request.UserReference);
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long? ParentId = null;
                    if (!string.IsNullOrEmpty(_Request.ParentKey))
                    {
                        ParentId = _HCoreContext.CAProduct.Where(x => x.Guid == _Request.ParentKey).Select(x => x.Id).FirstOrDefault();
                    }
                    _CAProduct = new CAProduct();
                    _CAProduct.Guid = HCoreHelper.GenerateGuid();
                    _CAProduct.TypeId = (int)TypeId;
                    _CAProduct.SubTypeId = SubTypeId;
                    _CAProduct.ParentId = ParentId;
                    _CAProduct.AccountId = (long)AccountId;
                    _CAProduct.Title = _Request.Title;
                    _CAProduct.Description = _Request.Description;
                    _CAProduct.Terms = _Request.Terms;
                    _CAProduct.UsageTypeId = (int)UsageTypeId;
                    _CAProduct.Terms = _Request.Terms;
                    _CAProduct.UsageInformation = _Request.UsageInformation;
                    _CAProduct.IsPinRequired = _Request.IsPinRequired;
                    _CAProduct.StartDate = _Request.StartDate;
                    _CAProduct.EndDate = _Request.EndDate;
                    _CAProduct.CodeValidityStartDate = _Request.CodeValidityStartDate;
                    _CAProduct.CodeValidityEndDate = _Request.CodeValidityEndDate;
                    _CAProduct.UnitPrice = _Request.UnitPrice;
                    _CAProduct.SellingPrice = _Request.SellingPrice;
                    _CAProduct.DiscountAmount = _Request.DiscountAmount;
                    _CAProduct.DiscountPercentage = _Request.DiscountPercentage;
                    _CAProduct.Amount = _Request.Amount;
                    _CAProduct.Charge = _Request.Charge;
                    _CAProduct.CommissionAmount = _Request.CommissionAmount;
                    _CAProduct.TotalAmount = _Request.TotalAmount;
                    _CAProduct.MaximumUnitSale = _Request.MaximumUnitSale;
                    _CAProduct.TotalUnitSale = _Request.TotalUnitSale;
                    _CAProduct.TotalUsed = _Request.TotalUsed;
                    _CAProduct.Views = 0;
                    _CAProduct.StatusId = (int)StatusId;
                    _CAProduct.CreateDate = HCoreHelper.GetGMTDate();
                    _CAProduct.CreatedById = _Request.UserReference.AccountId;
                    _HCoreContext.CAProduct.Add(_CAProduct);
                    _HCoreContext.SaveChanges();
                    long ProductId = _CAProduct.Id;
                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ProductDetails = _HCoreContext.CAProduct.Where(x => x.Id == ProductId).FirstOrDefault();
                            if (ProductDetails != null)
                            {
                                ProductDetails.IconStorageId = IconStorageId;
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                    long? PosterStorageId = null;
                    if (_Request.PosterStorage != null && !string.IsNullOrEmpty(_Request.PosterStorage.Content))
                    {
                        PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterStorage.Name, _Request.PosterStorage.Extension, _Request.PosterStorage.Content, null, _Request.UserReference);
                    }
                    if (PosterStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ProductDetails = _HCoreContext.CAProduct.Where(x => x.Id == ProductId).FirstOrDefault();
                            if (ProductDetails != null)
                            {
                                ProductDetails.PosterStorageId = PosterStorageId;
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CAProduct.Guid, "TUP108");
                    #endregion
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Creates the quick gift card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreateQuickGiftCard(OCAProduct.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OCAProduct.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be greater than 0");
                }
                else if (_Request.Amount > 100000)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be upto 100000 ");
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer mobile number is required");
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            })
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                long AccountId = 0;
                                _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                                var CustomerInformation = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             AccountTypeId = x.AccountTypeId,
                                             StatusId = x.StatusId,
                                         })
                                         .FirstOrDefault();
                                if (CustomerInformation != null)
                                {
                                    AccountId = CustomerInformation.Id;
                                }
                                else
                                {
                                    _UserAccountCreateRequest = new Operations.Object.OUserAccount.Request();
                                    _UserAccountCreateRequest.AccountTypeCode = HCoreConstant.Helpers.UserAccountType.AppUserS;
                                    _UserAccountCreateRequest.AccountOperationTypeCode = HCoreConstant.Helpers.AccountOperationType.OnlineAndOfflineS;
                                    _UserAccountCreateRequest.RegistrationSourceCode = Helpers.RegistrationSource.SystemS;
                                    _UserAccountCreateRequest.Name = _Request.FirstName + " " + _Request.LastName;
                                    _UserAccountCreateRequest.DisplayName = _Request.FirstName;
                                    _UserAccountCreateRequest.FirstName = _Request.FirstName;
                                    _UserAccountCreateRequest.LastName = _Request.LastName;
                                    _UserAccountCreateRequest.EmailAddress = _Request.EmailAddress;
                                    _UserAccountCreateRequest.MobileNumber = _Request.MobileNumber;
                                    _UserAccountCreateRequest.GenderCode = _Request.GenderCode;
                                    _UserAccountCreateRequest.StatusCode = HelperStatus.Default.ActiveS;
                                    _UserAccountCreateRequest.UserReference = _Request.UserReference;
                                    HCore.Operations.ManageCoreUserOperations _ManageCoreUserOperations = new ManageCoreUserOperations();
                                    HCore.Operations.Object.OUserAccount.Request _Response = _ManageCoreUserOperations.SaveUserAccountParameter(_UserAccountCreateRequest);
                                    if (_Response != null)
                                    {
                                        AccountId = (long)_Response.ReferenceId;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Unable to create user account. Please contact support");
                                    }
                                }
                                var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             AccountTypeId = x.AccountTypeId,
                                             StatusId = x.StatusId,
                                         })
                                         .FirstOrDefault();
                                if (CustomerDetails != null)
                                {
                                    if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.AccountId == _Request.UserAccountId
                                         && x.SourceId == TransactionSource.GiftCards
                                         && x.StatusId == HelperStatus.Transaction.Success
                                         && x.ModeId == TransactionMode.Credit)
                                         .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.UserAccountId
                                         && x.StatusId == HelperStatus.Transaction.Success
                                          && x.SourceId == TransactionSource.GiftPoints
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                        if (_Request.Amount < (_BalanceResponse.Balance + 1))
                                        {
                                            _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                            _CoreTransactionRequest.CustomerId = CustomerDetails.Id;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserAccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Comment = _Request.Comment,
                                                Amount = _Request.Amount,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = CustomerDetails.Id,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Amount = _Request.Amount,
                                                Comment = _Request.Comment,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    DateTime CreateDate = HCoreHelper.GetGMTDate();
                                                    DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                                    DateTime EndDate = HCoreHelper.GetGMTDateTime().AddYears(1);
                                                    _CAProductCodes = new List<CAProductCode>();
                                                    _CAProductCodes.Add(new CAProductCode
                                                    {
                                                        Guid = HCoreHelper.GenerateGuid(),
                                                        AccountId = AccountId,
                                                        TypeId = Helpers.Product.QuickGiftCard,
                                                        ItemCode = HCoreEncrypt.EncryptHash("314" + HCoreHelper.GenerateRandomNumber(8)),
                                                        ItemAmount = _Request.Amount,
                                                        AvailableAmount = _Request.Amount,
                                                        ItemPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4)),
                                                        StartDate = StartDate,
                                                        EndDate = EndDate,
                                                        UseCount = 0,
                                                        UseAttempts = 0,
                                                        CreateDate = CreateDate,
                                                        CreatedById = _Request.UserReference.AccountId,
                                                        StatusId = HelperStatus.ProdutCode.Unused
                                                    });
                                                    _CAProduct = new CAProduct();
                                                    _CAProduct.Guid = HCoreHelper.GenerateGuid();
                                                    _CAProduct.TypeId = Helpers.Product.QuickGiftCard;
                                                    _CAProduct.AccountId = AccountDetails.Id;
                                                    _CAProduct.Title = "Instant Gift Card";
                                                    _CAProduct.Description = "Instant Gift Card";
                                                    _CAProduct.Terms = _Request.Terms;
                                                    _CAProduct.UsageTypeId = HelperType.ProductUsageType.FullValue;
                                                    _CAProduct.UsageInformation = _Request.UsageInformation;
                                                    _CAProduct.IsPinRequired = 0;
                                                    _CAProduct.StartDate = StartDate;
                                                    _CAProduct.EndDate = EndDate;
                                                    _CAProduct.CodeValidityStartDate = _CAProduct.StartDate;
                                                    _CAProduct.CodeValidityEndDate = _CAProduct.EndDate;
                                                    _CAProduct.UnitPrice = _Request.Amount;
                                                    _CAProduct.SellingPrice = _Request.Amount;
                                                    _CAProduct.DiscountAmount = 0;
                                                    _CAProduct.DiscountPercentage = 0;
                                                    _CAProduct.Amount = _Request.Amount;
                                                    _CAProduct.Charge = 0;
                                                    _CAProduct.CommissionAmount = 0;
                                                    _CAProduct.TotalAmount = _Request.Amount;
                                                    _CAProduct.MaximumUnitSale = 1;
                                                    _CAProduct.TotalUnitSale = 0;
                                                    _CAProduct.TotalUsed = 0;
                                                    _CAProduct.TotalUsedAmount = 0;
                                                    _CAProduct.TotalSaleAmount = _Request.Amount;
                                                    _CAProduct.Views = 0;
                                                    _CAProduct.Message = _Request.Message;
                                                    _CAProduct.Comment = _Request.Comment;
                                                    _CAProduct.StatusId = HelperStatus.Product.Active;
                                                    _CAProduct.CreateDate = CreateDate;
                                                    _CAProduct.CreatedById = _Request.UserReference.AccountId;
                                                    _CAProduct.CAProductCodeProduct = _CAProductCodes;
                                                    _HCoreContext.CAProduct.Add(_CAProduct);
                                                    _HCoreContext.SaveChanges();
                                                    //string Message = "";
                                                    //if (!string.IsNullOrEmpty(_Request.Comment))
                                                    //{
                                                    //    Message = _Request.Comment;
                                                    //}
                                                    //else
                                                    //{
                                                    //    Message = HCoreHelper.GetConfigurationValueByUserAccount("customergiftpointcreditalert", AccountDetails.Id, _Request.UserReference);
                                                    //}
                                                    if (!string.IsNullOrEmpty(_Request.Message))
                                                    {
                                                        _Request.Message = _Request.Message.Replace("[AMOUNT]", _Request.Amount.ToString());
                                                        _Request.Message = _Request.Message.Replace("[DISPLAYNAME]", AccountDetails.DisplayName);
                                                        if (_Request.Message.Length > 120)
                                                        {
                                                            _Request.Message = _Request.Message.Substring(0, 120);
                                                        }
                                                        _Request.Message = _Request.Message + ". Download TUC App: https://bit.ly/tuc-app";
                                                        HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.Message, CustomerDetails.Id, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    }
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Gift card created for customer worth N" + _Request.Amount.ToString());
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Gift card wallet balance is low. Credit wallet to giveout rewards");
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer account is not active");
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer not registered. Download app or do transactions at partnered merchant stores");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateQuickGiftCard", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Creates the gift card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreateGiftCard(OCAProduct.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OCAProduct.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be greater than 0");
                }
                else if (_Request.Amount > 100000)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be upto 100000 ");
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            })
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == _Request.UserAccountId
                                            && x.SourceId == TransactionSource.GiftCards
                                            && x.StatusId == HelperStatus.Transaction.Success
                                            && x.ModeId == TransactionMode.Credit)
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                                _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                  .Where(x => x.AccountId == _Request.UserAccountId
                                 && x.StatusId == HelperStatus.Transaction.Success
                                  && x.SourceId == TransactionSource.GiftPoints
                                  && x.ModeId == TransactionMode.Debit)
                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                if (_Request.Amount < (_BalanceResponse.Balance + 1))
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        DateTime CreateDate = HCoreHelper.GetGMTDate();
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        DateTime EndDate = HCoreHelper.GetGMTDateTime().AddYears(1);
                                        _CAProduct = new CAProduct();
                                        _CAProduct.Guid = HCoreHelper.GenerateGuid();
                                        _CAProduct.TypeId = Product.GiftCard;
                                        _CAProduct.AccountId = AccountDetails.Id;
                                        _CAProduct.Title = "Gift Card";
                                        _CAProduct.Description = "Gift Card";
                                        _CAProduct.Terms = _Request.Terms;
                                        _CAProduct.UsageTypeId = HelperType.ProductUsageType.FullValue;
                                        _CAProduct.UsageInformation = _Request.UsageInformation;
                                        _CAProduct.IsPinRequired = 0;
                                        _CAProduct.StartDate = StartDate;
                                        _CAProduct.EndDate = EndDate;
                                        _CAProduct.CodeValidityStartDate = _CAProduct.StartDate;
                                        _CAProduct.CodeValidityEndDate = _CAProduct.EndDate;
                                        _CAProduct.UnitPrice = _Request.Amount;
                                        _CAProduct.SellingPrice = _Request.Amount;
                                        _CAProduct.DiscountAmount = 0;
                                        _CAProduct.DiscountPercentage = 0;
                                        _CAProduct.Amount = _Request.Amount;
                                        _CAProduct.Charge = 0;
                                        _CAProduct.CommissionAmount = 0;
                                        _CAProduct.TotalAmount = _Request.Amount;
                                        _CAProduct.MaximumUnitSale = 0;
                                        _CAProduct.TotalUnitSale = 0;
                                        _CAProduct.TotalUsed = 0;
                                        _CAProduct.TotalSaleAmount = 0;
                                        _CAProduct.TotalUsedAmount = 0;
                                        _CAProduct.Views = 0;
                                        _CAProduct.Message = _Request.Message;
                                        _CAProduct.Comment = _Request.Comment;
                                        _CAProduct.StatusId = HelperStatus.Product.Active;
                                        _CAProduct.CreateDate = CreateDate;
                                        _CAProduct.CreatedById = _Request.UserReference.AccountId;
                                        _CAProduct.CAProductCodeProduct = _CAProductCodes;
                                        _HCoreContext.CAProduct.Add(_CAProduct);
                                        _HCoreContext.SaveChanges();
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Gift card created worth N" + _Request.Amount.ToString());
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Gift card wallet balance is low. Credit wallet to giveout rewards");
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateQuickGiftCard", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Creates the gift card code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreateGiftCardCode(OCAProduct.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OCAProduct.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (string.IsNullOrEmpty(_Request.ProductKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Product code required");
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer mobile number is required");
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ProductDetails = _HCoreContext.CAProduct
                                                          .Where(x => x.TypeId == Helpers.Product.GiftCard
                                                                    && x.Guid == _Request.ProductKey)
                                                          .Select(x => new
                                                          {
                                                              ReferenceId = x.Id,
                                                              SellingPrice = x.SellingPrice,
                                                              Message = x.Message,
                                                          })
                                                          .FirstOrDefault();
                        if (ProductDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Product details not found");
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            })
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                long AccountId = 0;
                                _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                                var CustomerInformation = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             AccountTypeId = x.AccountTypeId,
                                             StatusId = x.StatusId,
                                         })
                                         .FirstOrDefault();
                                if (CustomerInformation != null)
                                {
                                    AccountId = CustomerInformation.Id;
                                }
                                else
                                {
                                    _UserAccountCreateRequest = new Operations.Object.OUserAccount.Request();
                                    _UserAccountCreateRequest.AccountTypeCode = HCoreConstant.Helpers.UserAccountType.AppUserS;
                                    _UserAccountCreateRequest.AccountOperationTypeCode = HCoreConstant.Helpers.AccountOperationType.OnlineAndOfflineS;
                                    _UserAccountCreateRequest.RegistrationSourceCode = Helpers.RegistrationSource.SystemS;
                                    _UserAccountCreateRequest.FirstName = _Request.FirstName;
                                    _UserAccountCreateRequest.LastName = _Request.LastName;
                                    _UserAccountCreateRequest.EmailAddress = _Request.EmailAddress;
                                    _UserAccountCreateRequest.MobileNumber = _Request.MobileNumber;
                                    _UserAccountCreateRequest.GenderCode = _Request.GenderCode;
                                    _UserAccountCreateRequest.StatusCode = HelperStatus.Default.ActiveS;
                                    HCore.Operations.ManageCoreUserOperations _ManageCoreUserOperations = new ManageCoreUserOperations();
                                    HCore.Operations.Object.OUserAccount.Request _Response = _ManageCoreUserOperations.SaveUserAccountParameter(_UserAccountCreateRequest);
                                    if (_Response != null)
                                    {
                                        AccountId = (long)_Response.ReferenceId;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Unable to create user account. Please contact support");
                                    }
                                }
                                var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             AccountTypeId = x.AccountTypeId,
                                             StatusId = x.StatusId,
                                         })
                                         .FirstOrDefault();
                                if (CustomerDetails != null)
                                {
                                    if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.AccountId == _Request.UserAccountId
                                         && x.SourceId == TransactionSource.GiftCards
                                         && x.StatusId == HelperStatus.Transaction.Success
                                         && x.ModeId == TransactionMode.Credit)
                                         .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.UserAccountId
                                         && x.StatusId == HelperStatus.Transaction.Success
                                          && x.SourceId == TransactionSource.GiftPoints
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                        if (_Request.Amount < (_BalanceResponse.Balance + 1))
                                        {
                                            _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                            _CoreTransactionRequest.CustomerId = CustomerDetails.Id;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserAccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Comment = _Request.Comment,
                                                Amount = _Request.Amount,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = CustomerDetails.Id,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Amount = _Request.Amount,
                                                Comment = _Request.Comment,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var ProductInfoForUpdate = _HCoreContext.CAProduct.Where(x => x.Id == ProductDetails.ReferenceId).FirstOrDefault();
                                                    ProductInfoForUpdate.MaximumUnitSale += 1;
                                                    ProductInfoForUpdate.TotalUnitSale += 1;
                                                    ProductInfoForUpdate.TotalSaleAmount += ProductInfoForUpdate.SellingPrice;
                                                    DateTime CreateDate = HCoreHelper.GetGMTDate();
                                                    DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                                    DateTime EndDate = HCoreHelper.GetGMTDateTime().AddYears(1);
                                                    _CAProductCode = new CAProductCode();
                                                    _CAProductCode.Guid = HCoreHelper.GenerateGuid();
                                                    _CAProductCode.ProductId = ProductDetails.ReferenceId;
                                                    _CAProductCode.AccountId = AccountId;
                                                    _CAProductCode.TypeId = Helpers.Product.GiftCard;
                                                    _CAProductCode.ItemCode = HCoreEncrypt.EncryptHash("314" + HCoreHelper.GenerateRandomNumber(8));
                                                    _CAProductCode.ItemAmount = ProductDetails.SellingPrice;
                                                    _CAProductCode.AvailableAmount = ProductDetails.SellingPrice;
                                                    _CAProductCode.ItemPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                                    _CAProductCode.StartDate = StartDate;
                                                    _CAProductCode.EndDate = EndDate;
                                                    _CAProductCode.UseCount = 0;
                                                    _CAProductCode.UseAttempts = 0;
                                                    _CAProductCode.CreateDate = CreateDate;
                                                    _CAProductCode.CreatedById = _Request.UserReference.AccountId;
                                                    _CAProductCode.StatusId = HelperStatus.ProdutCode.Unused;
                                                    _HCoreContext.CAProductCode.Add(_CAProductCode);
                                                    _HCoreContext.SaveChanges();
                                                    if (!string.IsNullOrEmpty(ProductDetails.Message))
                                                    {
                                                        _Request.Message = ProductDetails.Message;
                                                        _Request.Message = _Request.Message.Replace("[AMOUNT]", ProductDetails.SellingPrice.ToString());
                                                        _Request.Message = _Request.Message.Replace("[DISPLAYNAME]", AccountDetails.DisplayName);
                                                        if (_Request.Message.Length > 120)
                                                        {
                                                            _Request.Message = _Request.Message.Substring(0, 120);
                                                        }
                                                        _Request.Message += ". Download TUC App: https://bit.ly/tuc-app";
                                                        HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.Message, CustomerDetails.Id, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    }
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Gift card created for customer worth N" + ProductDetails.SellingPrice.ToString());
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Gift card wallet balance is low. Credit wallet to giveout rewards");
                                        }


                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer account is not active");
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer not registered. Download app or do transactions at partnered merchant stores");
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateQuickGiftCard", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductCode(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.CAProductCode
                                                 select new OCAProduct.ProductCode
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     ProductOwnerId = x.Product.AccountId,
                                                     ProductOwnerKey = x.Product.Account.Guid,

                                                     TypeId = x.TypeId,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,

                                                     SubTypeId = x.SubTypeId,
                                                     SubTypeCode = x.SubType.SystemName,
                                                     SubTypeName = x.SubType.Name,

                                                     ProductId = x.ProductId,
                                                     ProductKey = x.Product.Guid,

                                                     SubProductId = x.SubProductId,
                                                     SubProductKey = x.SubProduct.Guid,

                                                     AccountId = x.AccountId,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountMobileNumber = x.Account.MobileNumber,

                                                     MerchantId = x.Account.OwnerId,
                                                     MerchantKey = x.Account.Owner.Guid,
                                                     MerchantDisplayName = x.Account.Owner.DisplayName,

                                                     ItemAmount = x.ItemAmount,
                                                     AvailableAmount = x.AvailableAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     UseCount = x.UseCount,
                                                     LastUseDate = x.LastUseDate,

                                                     LastUseLocationId = x.LastUseLocation.AccountId,
                                                     LastUseLocationName = x.LastUseLocation.Account.DisplayName,
                                                     LastUseLocaionKey = x.LastUseLocation.Account.Guid,

                                                     LastUseSubLocationId = x.LastUseLocation.SubAccountId,
                                                     LastUseSubLocationName = x.LastUseLocation.SubAccount.DisplayName,
                                                     LastUseSubLocationKey = x.LastUseLocation.SubAccount.Guid,

                                                     UseAttempts = x.UseAttempts,
                                                     Comment = x.Comment,
                                                     Message = x.Message,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByKey = x.CreatedBy.Guid,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByKey = x.ModifyBy.Guid,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCAProduct.ProductCode> Data = (from x in _HCoreContext.CAProductCode
                                                         select new OCAProduct.ProductCode
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             ProductOwnerId = x.Product.AccountId,
                                                             ProductOwnerKey = x.Product.Account.Guid,

                                                             TypeId = x.TypeId,
                                                             TypeCode = x.Type.SystemName,
                                                             TypeName = x.Type.Name,

                                                             SubTypeId = x.SubTypeId,
                                                             SubTypeCode = x.SubType.SystemName,
                                                             SubTypeName = x.SubType.Name,

                                                             ProductId = x.ProductId,
                                                             ProductKey = x.Product.Guid,

                                                             SubProductId = x.SubProductId,
                                                             SubProductKey = x.SubProduct.Guid,

                                                             AccountId = x.AccountId,
                                                             AccountKey = x.Account.Guid,
                                                             AccountDisplayName = x.Account.DisplayName,
                                                             AccountMobileNumber = x.Account.MobileNumber,

                                                             MerchantId = x.Account.OwnerId,
                                                             MerchantKey = x.Account.Owner.Guid,
                                                             MerchantDisplayName = x.Account.Owner.DisplayName,

                                                             ItemAmount = x.ItemAmount,
                                                             AvailableAmount = x.AvailableAmount,
                                                             StartDate = x.StartDate,
                                                             EndDate = x.EndDate,
                                                             UseCount = x.UseCount,
                                                             LastUseDate = x.LastUseDate,

                                                             LastUseLocationId = x.LastUseLocation.AccountId,
                                                             LastUseLocationName = x.LastUseLocation.Account.DisplayName,
                                                             LastUseLocaionKey = x.LastUseLocation.Account.Guid,

                                                             LastUseSubLocationId = x.LastUseLocation.SubAccountId,
                                                             LastUseSubLocationName = x.LastUseLocation.SubAccount.DisplayName,
                                                             LastUseSubLocationKey = x.LastUseLocation.SubAccount.Guid,

                                                             UseAttempts = x.UseAttempts,
                                                             Comment = x.Comment,
                                                             Message = x.Message,

                                                             CreateDate = x.CreateDate,
                                                             CreatedById = x.CreatedById,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                             CreatedByKey = x.CreatedBy.Guid,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyById = x.ModifyById,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                             ModifyByKey = x.ModifyBy.Guid,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    //foreach (var Details in Data)
                    //{
                    //    if (!string.IsNullOrEmpty(Details.IconUrl))
                    //    {
                    //        Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        Details.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProductCode", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProduct(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.CAProduct
                                                 select new OCAProduct.List
                                                 {

                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,
                                                     SubTypeCode = x.SubType.SystemName,
                                                     SubTypeName = x.SubType.Name,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     Tite = x.Title,
                                                     UsageTypeCode = x.UsageType.SystemName,
                                                     UsageTypeName = x.UsageType.Name,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     TotalUnitSale = x.TotalUnitSale,
                                                     TotalUsed = x.TotalUsed,
                                                     TotalSaleAmount = x.TotalSaleAmount,
                                                     TotalUsedAmount = x.TotalUsedAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     SellingPrice = x.SellingPrice,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     LastUseDate = x.LastUseDate,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCAProduct.List> Data = (from x in _HCoreContext.CAProduct
                                                  select new OCAProduct.List
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,
                                                      TypeCode = x.Type.SystemName,
                                                      TypeName = x.Type.Name,
                                                      SubTypeCode = x.SubType.SystemName,
                                                      SubTypeName = x.SubType.Name,
                                                      AccountKey = x.Account.Guid,
                                                      AccountDisplayName = x.Account.DisplayName,
                                                      Tite = x.Title,
                                                      UsageTypeCode = x.UsageType.SystemName,
                                                      UsageTypeName = x.UsageType.Name,
                                                      MaximumUnitSale = x.MaximumUnitSale,
                                                      TotalUnitSale = x.TotalUnitSale,
                                                      TotalUsed = x.TotalUsed,
                                                      TotalSaleAmount = x.TotalSaleAmount,
                                                      TotalUsedAmount = x.TotalUsedAmount,
                                                      StartDate = x.StartDate,
                                                      EndDate = x.EndDate,
                                                      LastUseDate = x.LastUseDate,
                                                      SellingPrice = x.SellingPrice,
                                                      CreateDate = x.CreateDate,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                      CreatedByKey = x.CreatedBy.Guid,
                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    //foreach (var Details in Data)
                    //{
                    //    if (!string.IsNullOrEmpty(Details.IconUrl))
                    //    {
                    //        Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        Details.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward product list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardProductList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCProduct
                                                 where x.StatusId == HelperStatus.Default.Active
                                                 select new OCAProduct.RewardProduct
                                                 {

                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     AccountId = x.AccountId,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     CategoryKey = x.Category.Guid,
                                                     CategoryName = x.Category.Name,
                                                     Name = x.Name,
                                                     SystemName = x.SystemName,
                                                     Description = x.Description,
                                                     IconUrl = x.PrimaryStorage.Path,
                                                     //MinimumAmount = x.MinimumAmount,
                                                     //MaximumAmount = x.MaximumAmount,
                                                     //MinimumQuantity = x.MinimumQuantity,
                                                     //MaximumQuantity = x.MaximumQuantity,
                                                     //ActualPrice = x.ActualPrice,
                                                     //SellingPrice = x.SellingPrice,
                                                     //AllowMultiple = x.AllowMultiple,
                                                     //RewardPercentage = x.RewardPercentage,
                                                     //MaximumRewardAmount = x.MaximumRewardAmount,
                                                     //TotalStock = x.SCProductVarient.,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCAProduct.RewardProduct> Data = (from x in _HCoreContext.SCProduct
                                                           where x.StatusId == HelperStatus.Default.Active
                                                           select new OCAProduct.RewardProduct
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               AccountId = x.AccountId,
                                                               AccountKey = x.Account.Guid,
                                                               AccountDisplayName = x.Account.DisplayName,
                                                               CategoryKey = x.Category.Guid,
                                                               CategoryName = x.Category.Name,
                                                               Name = x.Name,
                                                               SystemName = x.SystemName,
                                                               Description = x.Description,
                                                               IconUrl = x.PrimaryStorage.Path,
                                                               //MinimumAmount = x.MinimumAmount,
                                                               //MaximumAmount = x.MaximumAmount,
                                                               //MinimumQuantity = x.MinimumQuantity,
                                                               //MaximumQuantity = x.MaximumQuantity,
                                                               //ActualPrice = x.ActualPrice,
                                                               //SellingPrice = x.SellingPrice,
                                                               //AllowMultiple = x.AllowMultiple,
                                                               //RewardPercentage = x.RewardPercentage,
                                                               //MaximumRewardAmount = x.MaximumRewardAmount,
                                                               //TotalStock = x.TotalStock,
                                                               CreateDate = x.CreateDate,
                                                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                               CreatedByKey = x.CreatedBy.Guid,
                                                               StatusId = x.StatusId,
                                                               StatusCode = x.Status.SystemName,
                                                               StatusName = x.Status.Name
                                                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetRewardProductList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the prduct reward percentage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePrductRewardPercentage(OCAProduct.RewardProductUpdate _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                using (_HCoreContext = new HCoreContext())
                {
                    var Product = _HCoreContext.SCProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Product != null)
                    {
                        //Product.RewardPercentage = _Request.RewardPercentage;
                        Product.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Product.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "TUP108");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP108");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}