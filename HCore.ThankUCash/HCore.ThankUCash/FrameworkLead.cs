//==================================================================================
// FileName: FrameworkLead.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to lead
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkLead
    {
        HCoreContext _HCoreContext;
        TULead _TULead;
        TULeadOwner _TULeadOwner;
        TULeadActivity _TULeadActivity;
        UserAccountsList _UserAccountsList;
        TULeadGroup _TULeadGroup;
        public class UserAccountsList
        {
            public long UserAccountId;
            public DateTime CreateDate;
        }
        /// <summary>
        /// Shuffles the leads.
        /// </summary>
        internal void ShuffleLeads()
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var SupportUsers = _HCoreContext.HCUAccount
                  .Where(x => x.Role.SystemName == "customersupport" && x.StatusId == HCoreConstant.HelperStatus.Default.Active && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Admin)
                  .Select(x => new
                  {
                      UserAccountId = x.Id,
                      DisplayName = x.DisplayName,
                  }).ToList();

                    // int LeadLimit = SupportUsers.Count() * 120;
                    //int PerUserLoad = 120;
                    //int Skip = 0;
                    DateTime StartTime = HCoreHelper.GetGMTDate();
                    DateTime EndTime = StartTime.AddDays(1).AddSeconds(-1);
                    foreach (var SupportUser in SupportUsers)
                    {
                        List<UserAccountsList> _UserAccountsList = new List<UserAccountsList>();
                        using (_HCoreContext = new HCoreContext())
                        {
                            int SupportLoad = _HCoreContext.TULead.Where(x => x.AgentId == SupportUser.UserAccountId && x.CreateDate > StartTime && x.CreateDate < EndTime).Select(x => x.Id).Count();
                            int LeadOffset = 120;
                            LeadOffset = LeadOffset - SupportLoad;
                            if (LeadOffset > 0)
                            {
                                // Group 1 users who have only mobile number registered and have been rewarded points before 
                                _UserAccountsList = _HCoreContext.HCUAccount
                                .Where(x => x.DisplayName.StartsWith("234")
                                    && _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.Account.UserId == x.UserId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                (m.SourceId == TransactionSource.TUC)
                                                                  ).Count() > 0
                                    && x.TULeadAccount.Count == 0
                                    && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser)
                                .Select(x => new UserAccountsList
                                {
                                    UserAccountId = x.Id,
                                    CreateDate = x.CreateDate,
                                }).OrderByDescending(x => x.CreateDate).Skip(0).Take(LeadOffset).ToList();

                                if (_UserAccountsList.Count < 120)
                                {
                                    int TOffset = 120 - _UserAccountsList.Count;
                                    var _UserAccountsListC = _HCoreContext.HCUAccount
                                  .Where(x => x.DisplayName.StartsWith("234")
                                   && x.OwnerId != null
                                      && _UserAccountsList.Where(m => m.UserAccountId == x.Id).Count() == 0
                                && x.TULeadAccount.Count == 0
                                      && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser)
                                  .Select(x => new UserAccountsList
                                  {
                                      UserAccountId = x.Id,
                                  }).OrderByDescending(x => x.CreateDate).Skip(0).Take(TOffset).ToList();
                                    _UserAccountsList.AddRange(_UserAccountsListC);
                                }
                                // Group 2  update profile with zero balance
                                if (_UserAccountsList.Count < 120)
                                {
                                    int TOffset = 120 - _UserAccountsList.Count;
                                    var _UserAccountsListC = _HCoreContext.HCUAccount
                                     .Where(x =>
                                     (_HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.Account.UserId == x.UserId &&
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                                                   m.SourceId == TransactionSource.TUC)
                                                                 .Sum(m => (double?)m.TotalAmount) ?? 0) -
                                                                       (_HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.Account.UserId == x.UserId &&
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                   m.SourceId == TransactionSource.TUC)
                                                                 .Sum(m => (double?)m.TotalAmount) ?? 0) == 0
                                      && x.TULeadAccount.Count == 0
                                       && x.OwnerId != null
                                    && _UserAccountsList.Where(m => m.UserAccountId == x.Id).Count() == 0
                                     && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                     && (x.DisplayName == null || x.FirstName == null || x.LastName == null || x.GenderId == null || x.DateOfBirth == null || x.EmailAddress == null)
                                     )
                                     .Select(x => new UserAccountsList
                                     {
                                         UserAccountId = x.Id,
                                     }).OrderByDescending(x => x.CreateDate).Skip(0).Take(TOffset).ToList();
                                    _UserAccountsList.AddRange(_UserAccountsListC);

                                }

                                // Group 3 Redeem customer  
                                if (_UserAccountsList.Count < 120)
                                {
                                    int TOffset = 120 - _UserAccountsList.Count;
                                    var _UserAccountsListC = _HCoreContext.HCUAccount
                                     .Where(x =>
                                          x.TULeadAccount.Count == 0
                                  && x.HCUAccountTransactionAccount.Where(m => m.ModeId == Helpers.TransactionMode.Debit && m.SourceId == TransactionSource.TUC).Count() > 0
                                    && _UserAccountsList.Where(m => m.UserAccountId == x.Id).Count() == 0
                                     && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                      && x.OwnerId != null
                                     && (x.DisplayName == null || x.FirstName == null || x.LastName == null || x.GenderId == null || x.DateOfBirth == null || x.EmailAddress == null)
                                     )
                                     .Select(x => new UserAccountsList
                                     {
                                         UserAccountId = x.Id,
                                     }).OrderByDescending(x => x.CreateDate).Skip(0).Take(TOffset).ToList();
                                    _UserAccountsList.AddRange(_UserAccountsListC);
                                }
                                foreach (var User in _UserAccountsList)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _TULead = new TULead();
                                        _TULead.Guid = HCoreHelper.GenerateGuid();
                                        _TULead.AgentId = SupportUser.UserAccountId;
                                        _TULead.AccountId = User.UserAccountId;
                                        _TULead.StartTime = CurrentTime;
                                        _TULead.CreateDate = CurrentTime;
                                        _TULead.CreatedById = 1;
                                        _TULead.StatusId = TULeadStatus.New;
                                        _HCoreContext.TULead.Add(_TULead);
                                        _HCoreContext.SaveChanges();
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _TULeadOwner = new TULeadOwner();
                                            _TULeadOwner.LeadId = _TULead.Id;
                                            _TULeadOwner.AgentId = SupportUser.UserAccountId;
                                            _TULeadOwner.StartDate = CurrentTime;
                                            _TULeadOwner.CreatedById = 1;
                                            _TULeadOwner.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                            _HCoreContext.TULeadOwner.Add(_TULeadOwner);
                                            _HCoreContext.SaveChanges();
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _TULeadActivity = new TULeadActivity();
                                            _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                                            _TULeadActivity.LeadId = _TULead.Id;
                                            _TULeadActivity.Title = "New Lead Created";
                                            _TULeadActivity.StartDate = CurrentTime;
                                            _TULeadActivity.CreateDate = CurrentTime;
                                            _TULeadActivity.CreatedById = 1;
                                            _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                            _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                                            _HCoreContext.SaveChanges();
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _TULeadActivity = new TULeadActivity();
                                            _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                                            _TULeadActivity.LeadId = _TULead.Id;
                                            _TULeadActivity.Title = "Lead assigned to " + SupportUser.DisplayName;
                                            _TULeadActivity.StartDate = HCoreHelper.GetGMTDateTime();
                                            _TULeadActivity.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _TULeadActivity.CreatedById = 1;
                                            _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                            _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                                            _HCoreContext.SaveChanges();
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("ShuffleLeads", _Exception);
                #endregion
            }
            #endregion

        }
        /// <summary>
        /// Creates the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreateLead(OLead.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var SupportUser = _HCoreContext.HCUAccount
                                        .Where(x => x.Id == _Request.UserReference.AccountId)
                                        .Select(x => new
                                        {
                                            UserAccountId = x.Id,
                                            DisplayName = x.DisplayName,
                                        }).FirstOrDefault();
                    var SupportUserMerchantGroup = _HCoreContext.TULeadGroup.Where(x => x.AgentId == SupportUser.UserAccountId
                    && x.StatusId == HCoreConstant.HelperStatus.Default.Active).Select(x => x.MerchantId).ToList();
                    foreach (var MerchantId in SupportUserMerchantGroup)
                    {
                        if (_UserAccountsList == null)
                        {
                            _UserAccountsList = (from n in _HCoreContext.HCUAccount
                                                 where n.OwnerId == MerchantId
                                                     && n.AccountTypeId == UserAccountType.Appuser
                                                     && !n.TULeadAccount.Any()
                                                 orderby n.CreateDate descending
                                                 select new UserAccountsList
                                                 {
                                                     UserAccountId = n.Id,
                                                     CreateDate = n.CreateDate,
                                                 }).FirstOrDefault();
                        }
                    }
                    if (_UserAccountsList == null)
                    {
                        _UserAccountsList = (from n in _HCoreContext.HCUAccount
                                             where
                                                    n.OwnerId != null
                                                 && n.AccountTypeId == UserAccountType.Appuser
                                                 && !n.TULeadAccount.Any()
                                                 && n.HCUAccountTransactionAccount.Any(a => a.BankId != null)
                                                 && _HCoreContext.TULeadGroup
                                                 .Where(x =>
                                                 x.AgentId == SupportUser.UserAccountId
                                                 && x.MerchantId == n.OwnerId
                                                 && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                                 ).Count() > 0
                                             orderby n.CreateDate descending
                                             select new UserAccountsList
                                             {
                                                 UserAccountId = n.Id,
                                                 CreateDate = n.CreateDate,
                                             }).FirstOrDefault();
                    }

                    //_UserAccountsList = new List<UserAccountsList>();
                    #region Get Lead
                    if (_UserAccountsList == null)
                    {
                        _UserAccountsList = (from n in _HCoreContext.HCUAccount
                                             where (n.OwnerId != 6967 && n.OwnerId != 7260)
                                                 && n.OwnerId != null
                                                 && n.HCUAccountTransactionAccount.Any(a => a.BankId != null)
                                                 && n.AccountTypeId == UserAccountType.Appuser
                                                 && !n.TULeadAccount.Any()
                                             orderby n.CreateDate descending
                                             select new UserAccountsList
                                             {
                                                 UserAccountId = n.Id,
                                                 CreateDate = n.CreateDate,
                                             }).FirstOrDefault();
                    }

                    //                                                 && (n.DisplayName.StartsWith("234") || n.DisplayName == null || n.FirstName == null || n.LastName == null || n.GenderId == null)
                    //_UserAccountsList = (from n in _HCoreContext.HCUAccount
                    //where (n.OwnerId != 6967 && n.OwnerId != 7260)
                    //    && n.AccountTypeId == UserAccountType.Appuser
                    //    && _HCoreContext.TULead.Where(x => x.AccountId == n.Id).Count() == 0
                    //    && (n.DisplayName.StartsWith("234") || n.DisplayName == null || n.FirstName == null || n.LastName == null || n.GenderId == null)
                    //orderby n.CreateDate descending
                    //select new UserAccountsList
                    //{
                    //    UserAccountId = n.Id,
                    //    CreateDate = n.CreateDate,
                    //}).FirstOrDefault();

                    #endregion
                    if (_UserAccountsList != null)
                    {

                        using (_HCoreContext = new HCoreContext())
                        {
                            _TULead = new TULead();
                            _TULead.Guid = HCoreHelper.GenerateGuid();
                            _TULead.AgentId = SupportUser.UserAccountId;
                            _TULead.AccountId = _UserAccountsList.UserAccountId;
                            _TULead.StartTime = CurrentTime;
                            _TULead.CreateDate = CurrentTime;
                            _TULead.ModifyDate = CurrentTime;
                            _TULead.CreatedById = 1;
                            _TULead.StatusId = TULeadStatus.Calling;
                            _HCoreContext.TULead.Add(_TULead);
                            _HCoreContext.SaveChanges();
                            _Request.ReferenceKey = _TULead.Guid;
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TULeadOwner = new TULeadOwner();
                                _TULeadOwner.LeadId = _TULead.Id;
                                _TULeadOwner.AgentId = SupportUser.UserAccountId;
                                _TULeadOwner.StartDate = CurrentTime;
                                _TULeadOwner.CreatedById = 1;
                                _TULeadOwner.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                _HCoreContext.TULeadOwner.Add(_TULeadOwner);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TULeadActivity = new TULeadActivity();
                                _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                                _TULeadActivity.LeadId = _TULead.Id;
                                _TULeadActivity.Title = "New Lead Created";
                                _TULeadActivity.StartDate = CurrentTime;
                                _TULeadActivity.CreateDate = CurrentTime;
                                _TULeadActivity.CreatedById = 1;
                                _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TULeadActivity = new TULeadActivity();
                                _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                                _TULeadActivity.LeadId = _TULead.Id;
                                _TULeadActivity.Title = "Lead assigned to " + SupportUser.DisplayName;
                                _TULeadActivity.StartDate = HCoreHelper.GetGMTDateTime();
                                _TULeadActivity.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TULeadActivity.CreatedById = 1;
                                _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TULeadActivity = new TULeadActivity();
                                _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                                _TULeadActivity.LeadId = _TULead.Id;
                                _TULeadActivity.Title = "Lead status updated to calling";
                                _TULeadActivity.StartDate = HCoreHelper.GetGMTDateTime();
                                _TULeadActivity.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TULeadActivity.CreatedById = 1;
                                _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                                _HCoreContext.SaveChanges();
                            }
                        }
                        return GetLead(_Request);
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "New leads not found");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("CreateLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion

        }
        /// <summary>
        /// Saves the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveLead(OLead.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                long? OwnerId = HCoreHelper.GetUserAccountId(_Request.OwnerKey, _Request.UserReference);
                long? AgentId = HCoreHelper.GetUserAccountId(_Request.AgentKey, _Request.UserReference);
                long? AccountId = HCoreHelper.GetUserAccountId(_Request.AccountKey, _Request.UserReference);
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long DetailsCheck = _HCoreContext.TULead.Where(x => x.AccountId == AccountId).Select(x => x.Id).FirstOrDefault();
                    if (DetailsCheck == 0)
                    {
                        _TULead = new TULead();
                        _TULead.Guid = HCoreHelper.GenerateGuid();
                        _TULead.OwnerId = OwnerId;
                        _TULead.AgentId = AgentId;
                        _TULead.AccountId = AccountId;
                        _TULead.CreateDate = CurrentTime;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _TULead.CreatedById = _Request.UserReference.AccountId;
                        }
                        _TULead.StatusId = TULeadStatus.New;
                        _HCoreContext.TULead.Add(_TULead);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _TULead.Guid;
                        using (_HCoreContext = new HCoreContext())
                        {
                            _TULeadOwner = new TULeadOwner();
                            _TULeadOwner.LeadId = _TULead.Id;
                            _TULeadOwner.AgentId = AgentId;
                            _TULeadOwner.Comment = _Request.Comment;
                            _TULeadOwner.StartDate = CurrentTime;
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _TULeadOwner.CreatedById = _Request.UserReference.AccountId;
                            }
                            _TULeadOwner.StatusId = HCoreConstant.HelperStatus.Default.Active;
                            _HCoreContext.TULeadOwner.Add(_TULeadOwner);
                            _HCoreContext.SaveChanges();
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            _TULeadActivity = new TULeadActivity();
                            _TULeadActivity.LeadId = _TULead.Id;
                            _TULeadActivity.Title = "New lead assgined";
                            _TULeadActivity.StartDate = CurrentTime;
                            _TULeadActivity.CreateDate = CurrentTime;
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _TULeadActivity.CreatedById = _Request.UserReference.AccountId;
                            }
                            _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                            _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                            _HCoreContext.SaveChanges();
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the lead status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateLeadStatus(OLead.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var StatusDetails = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode)
                        .Select(x => new
                        {
                            Id = x.Id,
                            Name = x.Name,
                        }).FirstOrDefault();

                    var LeadDetails = _HCoreContext.TULead.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (LeadDetails != null)
                    {
                        LeadDetails.ModifyDate = CurrentTime;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            LeadDetails.ModifyById = _Request.UserReference.AccountId;
                        }
                        else
                        {
                            LeadDetails.ModifyById = 1;
                        }
                        LeadDetails.StatusId = StatusDetails.Id;
                        if (StatusDetails.Id == TULeadStatus.Closed)
                        {
                            LeadDetails.EndTime = CurrentTime;
                        }
                        if (StatusDetails.Id == TULeadStatus.Calling)
                        {
                            if (LeadDetails.StartTime != null)
                            {
                                LeadDetails.StartTime = CurrentTime;
                            }
                        }
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();


                        using (_HCoreContext = new HCoreContext())
                        {
                            _TULeadActivity = new TULeadActivity();
                            _TULeadActivity.Guid = HCoreHelper.GenerateGuid();
                            _TULeadActivity.LeadId = LeadDetails.Id;
                            _TULeadActivity.Title = "Lead status updated to " + StatusDetails.Name;
                            _TULeadActivity.Comment = _Request.Comment;
                            _TULeadActivity.Description = _Request.Description;
                            _TULeadActivity.StartDate = CurrentTime;
                            _TULeadActivity.CreateDate = CurrentTime;
                            if (_Request.EndTime != null)
                            {
                                _TULeadActivity.EndDate = _Request.EndTime;
                            }
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _TULeadActivity.CreatedById = _Request.UserReference.AccountId;
                            }
                            _TULeadActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
                            _HCoreContext.TULeadActivity.Add(_TULeadActivity);
                            _HCoreContext.SaveChanges();
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1000");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateLeadStatus", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLead(OLead.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OLead.Response Data = (from x in _HCoreContext.TULead
                                           where x.Guid == _Request.ReferenceKey
                                           select new OLead.Response
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               OwnerKey = x.Owner.Guid,
                                               OwnerDisplayName = x.Owner.DisplayName,

                                               AgentKey = x.Agent.Guid,
                                               AgentDisplayName = x.Agent.DisplayName,

                                               AccountKey = x.Account.Guid,
                                               AccountDisplayName = x.Account.DisplayName,

                                               StartTime = x.StartTime,
                                               EndTime = x.EndTime,

                                               CreateDate = x.CreateDate,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifyDate,
                                               ModifyByKey = x.ModifyBy.Guid,
                                               ModifyByDisplayName = x.ModifyBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name
                                           })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetLeads", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the lead list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLead(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TULead
                                        select new OLead.Response
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            OwnerKey = x.Owner.Guid,
                                            OwnerDisplayName = x.Owner.DisplayName,

                                            AgentKey = x.Agent.Guid,
                                            AgentDisplayName = x.Agent.DisplayName,

                                            AccountKey = x.Account.Guid,
                                            AccountDisplayName = x.Account.DisplayName,
                                            AccountMobileNumber = x.Account.MobileNumber,
                                            AccountEmailAddress = x.Account.EmailAddress,


                                            AccountOwnerKey = x.Account.Owner.Guid,
                                            AccountOwnerDisplayName = x.Account.Owner.DisplayName,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,

                                            StartTime = x.StartTime,
                                            EndTime = x.EndTime,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OLead.Response> Data = (from x in _HCoreContext.TULead
                                                 select new OLead.Response
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     OwnerKey = x.Owner.Guid,
                                                     OwnerDisplayName = x.Owner.DisplayName,

                                                     AgentKey = x.Agent.Guid,
                                                     AgentDisplayName = x.Agent.DisplayName,

                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountMobileNumber = x.Account.MobileNumber,
                                                     AccountEmailAddress = x.Account.EmailAddress,

                                                     AccountOwnerKey = x.Account.Owner.Guid,
                                                     AccountOwnerDisplayName = x.Account.Owner.DisplayName,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     StartTime = x.StartTime,
                                                     EndTime = x.EndTime,

                                                     ModifyDate = x.ModifyDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {

                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetLeads", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the future lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFutureLead(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccount
                                        where x.OwnerId != null && x.AccountTypeId == UserAccountType.Appuser && x.OwnerId != 6967 && x.OwnerId != 7260
                                        select new OLead.Response
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            AgentKey = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Agent.Guid).FirstOrDefault(),
                                            AgentDisplayName = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Agent.DisplayName).FirstOrDefault(),
                                            LeadStatusName = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Status.Name).FirstOrDefault(),

                                            AccountKey = x.Guid,
                                            AccountDisplayName = x.DisplayName,
                                            AccountMobileNumber = x.MobileNumber,
                                            AccountEmailAddress = x.EmailAddress,

                                            AccountOwnerKey = x.Owner.Guid,
                                            AccountOwnerDisplayName = x.Owner.DisplayName,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OLead.Response> Data = (from x in _HCoreContext.HCUAccount
                                                 where x.OwnerId != null && x.AccountTypeId == UserAccountType.Appuser && x.OwnerId != 6967 && x.OwnerId != 7260
                                                 select new OLead.Response
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AgentKey = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Agent.Guid).FirstOrDefault(),
                                                     AgentDisplayName = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Agent.DisplayName).FirstOrDefault(),
                                                     LeadStatusName = _HCoreContext.TULead.Where(m => m.AccountId == x.Id).Select(m => m.Status.Name).FirstOrDefault(),

                                                     AccountKey = x.Guid,
                                                     AccountDisplayName = x.DisplayName,
                                                     AccountMobileNumber = x.MobileNumber,
                                                     AccountEmailAddress = x.EmailAddress,

                                                     AccountOwnerKey = x.Owner.Guid,
                                                     AccountOwnerDisplayName = x.Owner.DisplayName,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {

                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetLeads", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the lead activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLeadActivity(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TULeadActivity
                                        select new OLeadActivity.Response
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            LeadKey = x.Lead.Guid,

                                            Title = x.Title,
                                            Description = x.Description,

                                            Comment = x.Comment,
                                            StartDate = x.StartDate,
                                            EndDate = x.EndDate,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OLeadActivity.Response> Data = (from x in _HCoreContext.TULeadActivity
                                                         select new OLeadActivity.Response
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             LeadKey = x.Lead.Guid,

                                                             Title = x.Title,
                                                             Description = x.Description,

                                                             Comment = x.Comment,
                                                             StartDate = x.StartDate,
                                                             EndDate = x.EndDate,

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {

                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetLeadActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// The over view response
        /// </summary>
        OLead.OverviewResponse _OverViewResponse;
        /// <summary>
        /// Gets the lead overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLeadOverview(OLead.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                DateTime StartTime = HCoreHelper.GetGMTDate();
                DateTime EndTime = StartTime.AddDays(1).AddSeconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    int TotalLeads = _HCoreContext.TULead
                    .Where(x => x.Agent.Guid == _Request.ReferenceKey
                        && (x.StatusId == TULeadStatus.Calling
                    || x.StatusId == TULeadStatus.CallFailed
                    || x.StatusId == TULeadStatus.New
                        || x.StatusId == TULeadStatus.CallLater
                        )).Select(X => X.Id).Count();
                    int PreviousLeads = _HCoreContext.TULead
                    .Where(x => x.Agent.Guid == _Request.ReferenceKey
                    && x.CreateDate < StartTime
                        && (x.StatusId == TULeadStatus.Calling
                    || x.StatusId == TULeadStatus.CallFailed
                    || x.StatusId == TULeadStatus.New
                        || x.StatusId == TULeadStatus.CallLater
                        )).Select(X => X.Id).Count();


                    _OverViewResponse = new OLead.OverviewResponse();

                    _OverViewResponse.Total = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey).Select(X => X.Id).Count();
                    _OverViewResponse.TotalNew = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.New).Select(X => X.Id).Count();
                    _OverViewResponse.TotalCalling = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.Calling).Select(X => X.Id).Count();
                    _OverViewResponse.TotalCallFailed = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.CallFailed).Select(X => X.Id).Count();
                    _OverViewResponse.TotalCallLater = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.CallLater).Select(X => X.Id).Count();
                    _OverViewResponse.TotalNotInterested = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.NotInterested).Select(X => X.Id).Count();
                    _OverViewResponse.TotalInvalidNumber = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.InvalidNumber).Select(X => X.Id).Count();
                    _OverViewResponse.TotalClosed = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.Closed).Select(X => X.Id).Count();

                    _OverViewResponse.TodayTotal = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime).Select(X => X.Id).Count();
                    _OverViewResponse.TodayNew = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.New).Select(X => X.Id).Count();
                    _OverViewResponse.TodayCalling = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.Calling).Select(X => X.Id).Count();
                    _OverViewResponse.TodayCallFailed = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.CallFailed).Select(X => X.Id).Count();
                    _OverViewResponse.TodayCallLater = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.CallLater).Select(X => X.Id).Count();
                    _OverViewResponse.TodayNotInterested = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.NotInterested).Select(X => X.Id).Count();
                    _OverViewResponse.TodayInvalidNumber = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.InvalidNumber).Select(X => X.Id).Count();
                    _OverViewResponse.TodayClosed = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.CreateDate > StartTime && x.CreateDate < EndTime && x.StatusId == TULeadStatus.Closed).Select(X => X.Id).Count();


                    _OverViewResponse.New = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.New).Select(X => X.Id).Count();
                    _OverViewResponse.Closed = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.Closed).Select(X => X.Id).Count();
                    _OverViewResponse.Invalid = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && x.StatusId == TULeadStatus.InvalidNumber).Select(X => X.Id).Count();
                    _OverViewResponse.Processing = _HCoreContext.TULead.Where(x => x.Agent.Guid == _Request.ReferenceKey && (x.StatusId == TULeadStatus.Calling || x.StatusId == TULeadStatus.CallFailed || x.StatusId == TULeadStatus.CallLater || x.StatusId == TULeadStatus.NotInterested)).Select(X => X.Id).Count();
                    _OverViewResponse.BackLog = TotalLeads - PreviousLeads;
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OverViewResponse, "HC0001");
                    #endregion
                }

                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the lead overview history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLeadOverviewHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                int TotalRecords = 0;
                List<OLead.ListOverview> Data = new List<OLead.ListOverview>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "Total", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Date", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    switch (_Request.Type)
                    {
                        case ListType.All:
                            #region Total Records
                            TotalRecords = _HCoreContext.TULead
                                .Where(x => x.AgentId == _Request.UserReference.AccountId)
                                .GroupBy(x => x.CreateDate.Value.Date)
                                .Select(x => new OLead.ListOverview
                                {
                                    Date = x.Key,
                                    Total = x.Count(),
                                    CallFailed = x.Count(m => m.StatusId == TULeadStatus.CallFailed),
                                    CallLater = x.Count(m => m.StatusId == TULeadStatus.CallLater),
                                    NotInterested = x.Count(m => m.StatusId == TULeadStatus.NotInterested),
                                    InvalidNumber = x.Count(m => m.StatusId == TULeadStatus.InvalidNumber),
                                    Closed = x.Count(m => m.StatusId == TULeadStatus.Closed),
                                })
                           .Where(_Request.SearchCondition)
                           .Count();
                            #endregion
                            #region Get Data
                            Data = _HCoreContext.TULead
                                .Where(x => x.AgentId == _Request.UserReference.AccountId)
                                .GroupBy(x => x.CreateDate.Value.Date)
                                .Select(x => new OLead.ListOverview
                                {
                                    Date = x.Key,
                                    Total = x.Count(),
                                    CallFailed = x.Count(m => m.StatusId == TULeadStatus.CallFailed),
                                    CallLater = x.Count(m => m.StatusId == TULeadStatus.CallLater),
                                    NotInterested = x.Count(m => m.StatusId == TULeadStatus.NotInterested),
                                    InvalidNumber = x.Count(m => m.StatusId == TULeadStatus.InvalidNumber),
                                    Closed = x.Count(m => m.StatusId == TULeadStatus.Closed),
                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            break;
                        default:
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLeadOverviewHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Saves the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveLeadGroup(OLeadGroup.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long AgentId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.AgentKey).Select(x => x.Id).FirstOrDefault();
                    long MerchantId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.MerchantKey).Select(x => x.Id).FirstOrDefault();
                    long CheckGroup = _HCoreContext.TULeadGroup.Where(x => x.AgentId == AgentId && x.MerchantId == MerchantId && x.StatusId == HCoreConstant.HelperStatus.Default.Active).Select(x => x.Id).FirstOrDefault();
                    if (CheckGroup == 0)
                    {
                        _TULeadGroup = new TULeadGroup();
                        _TULeadGroup.Guid = HCoreHelper.GenerateGuid();
                        _TULeadGroup.MerchantId = MerchantId;
                        _TULeadGroup.AgentId = AgentId;
                        _TULeadGroup.StartDate = HCoreHelper.GetGMTDateTime();
                        _TULeadGroup.CreatedById = _Request.UserReference.AccountId;
                        _TULeadGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TULeadGroup.StatusId = HCoreConstant.HelperStatus.Default.Active;
                        _HCoreContext.TULeadGroup.Add(_TULeadGroup);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1000", "Merchant assigned to agent");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Merchant already assigned to agent");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateLeadStatus", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateLeadGroup(OLeadGroup.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var GroupDetails = _HCoreContext.TULeadGroup.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (GroupDetails != null)
                    {
                        GroupDetails.EndDate = HCoreHelper.GetGMTDateTime();
                        GroupDetails.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                        GroupDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        GroupDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1000", "Merchant removed from agent");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Group details not found");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateLeadStatus", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLeadGroup(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TULeadGroup
                                        select new OLeadGroup.Response
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            AgentKey = x.Agent.Guid,
                                            AgentDisplayName = x.Agent.DisplayName,
                                            MerchantKey = x.Merchant.Guid,
                                            MerchantDisplayName = x.Merchant.DisplayName,

                                            StartDate = x.StartDate,
                                            EndDate = x.EndDate,


                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,


                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OLeadGroup.Response> Data = (from x in _HCoreContext.TULeadGroup
                                                      select new OLeadGroup.Response
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          AgentKey = x.Agent.Guid,
                                                          AgentDisplayName = x.Agent.DisplayName,
                                                          MerchantKey = x.Merchant.Guid,
                                                          MerchantDisplayName = x.Merchant.DisplayName,

                                                          StartDate = x.StartDate,
                                                          EndDate = x.EndDate,


                                                          CreateDate = x.CreateDate,
                                                          CreatedByKey = x.CreatedBy.Guid,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,


                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {

                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetLeadActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
