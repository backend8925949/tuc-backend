//==================================================================================
// FileName: ManageAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;

namespace HCore.ThankUCash
{
    public class ManageAccounts
    {
        AppUsersList _AppUsersList;
        FrameworkAccounts _FrameworkAccounts;
        FrameworkAccountsAppUsers _FrameworkAccountsAppUsers;

        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStore(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }
        /// <summary>
        /// Description: Gets the store lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreLite(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStoreLite(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirer(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAcquirer(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerMerchants(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAcquirerMerchants(_Request);
        }

        /// <summary>
        /// Description: Gets the pg account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPgAccount(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPgAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the position account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPosAccount(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPosAccount(_Request);
        }

        /// <summary>
        /// Description: Gets the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashier(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashier(_Request);
        }
        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminal(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }


        /// <summary>
        /// Description: Gets the application user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUser(OList.Request _Request)
        {
            _AppUsersList = new AppUsersList();
            return _AppUsersList.GetAppUser(_Request);
        }
        /// <summary>
        /// Description: Gets the application user lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUserLite(OList.Request _Request)
        {
            _FrameworkAccountsAppUsers = new FrameworkAccountsAppUsers();
            return _FrameworkAccountsAppUsers.GetAppUserLite(_Request);
        }
        /// <summary>
        /// Description: Gets the application users visit.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUsersVisit(OList.Request _Request)
        {
            _FrameworkAccountsAppUsers = new FrameworkAccountsAppUsers();
            return _FrameworkAccountsAppUsers.GetAppUsersVisit(_Request);
        }
        /// <summary>
        /// Description: Gets the relationship manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRelationshipManager(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetRelationshipManager(_Request);
        }

        /// <summary>
        /// Description: Gets the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubAccount(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetSubAccount(_Request);
        }

        /// <summary>
        /// Description: Gets the application user cards.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUserCards(OList.Request _Request)
        {
            _FrameworkAccountsAppUsers = new FrameworkAccountsAppUsers();
            return _FrameworkAccountsAppUsers.GetAppUserCards(_Request);
        }
        
    }
}
