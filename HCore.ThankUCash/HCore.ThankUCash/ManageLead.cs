//==================================================================================
// FileName: ManageLead.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageLead
    {
        FrameworkLead _FrameworkLead;
        /// <summary>
        /// Shuffles the leads.
        /// </summary>
        public void ShuffleLeads()
        {
            _FrameworkLead = new FrameworkLead();
            _FrameworkLead.ShuffleLeads();
        }
        //public OResponse LoadLeads(OLead.Request _Request)
        //{
        //    _FrameworkLead = new FrameworkLead();
        //    return _FrameworkLead.LoadLeads(_Request);
        //}
        /// <summary>
        /// Creates the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreateLead(OLead.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.CreateLead(_Request);
        }
        /// <summary>
        /// Gets the lead overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLeadOverview(OLead.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLeadOverview(_Request);
        }
        /// <summary>
        /// Saves the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveLead(OLead.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.SaveLead(_Request);
        }
        /// <summary>
        /// Updates the lead status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateLeadStatus(OLead.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.UpdateLeadStatus(_Request);
        }
        /// <summary>
        /// Gets the lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLead(OLead.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLead(_Request);
        }
        /// <summary>
        /// Gets the lead list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLead(OList.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLead(_Request);
        }
        /// <summary>
        /// Gets the future lead.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFutureLead(OList.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetFutureLead(_Request);
        }
        /// <summary>
        /// Gets the lead activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLeadActivity(OList.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLeadActivity(_Request);
        }
        /// <summary>
        /// Gets the lead overview history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLeadOverviewHistory(OList.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLeadOverviewHistory(_Request);
        }

        /// <summary>
        /// Saves the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveLeadGroup(OLeadGroup.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.SaveLeadGroup(_Request);
        }
        /// <summary>
        /// Updates the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateLeadGroup(OLeadGroup.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.UpdateLeadGroup(_Request);
        }
        /// <summary>
        /// Gets the lead group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLeadGroup(OList.Request _Request)
        {
            _FrameworkLead = new FrameworkLead();
            return _FrameworkLead.GetLeadGroup(_Request);
        }
    }
}
