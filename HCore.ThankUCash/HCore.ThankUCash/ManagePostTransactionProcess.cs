//==================================================================================
// FileName: ManagePostTransactionProcess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using System;
using HCore.ThankUCash.Actors;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;
using HCore.Operations.Object;

namespace HCore.ThankUCash
{
    public class ManagePostTransactionProcess
    {
        OResponse _OResponse;
        /// <summary>
        /// Description: Processes the transaction details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ProcessTransactionDetails(OCoreTransaction.Request _Request)
        {
            #region TransactionPostProcess
            var _TransactionPostProcessActor = ActorSystem.Create("TransactionPostProcessManagerActor");
            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<TransactionPostProcessManagerActor>("TransactionPostProcessManagerActor");
            _TransactionPostProcessActorNotify.Tell(_Request);
            #endregion
            _OResponse = new OResponse();
            _OResponse.Status = "Success";
            _OResponse.ResponseCode = "200";
            _OResponse.Message = "Received";
            return _OResponse;
        }
    }
}
