//==================================================================================
// FileName: ManageAgent.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageAgent
    {
        //FrameworkAgents _FrameworkAgents;
        //public OResponse GetOwnerPurchaseSummaryByDate(OList.Request _Request)
        //{
        //    _FrameworkAgents = new FrameworkAgents();
        //    return _FrameworkAgents.GetOwnerPurchaseSummaryByDate(_Request);
        //}
        //public OResponse GetMerchantsSale(OList.Request _Request)
        //{
        //    _FrameworkAgents = new FrameworkAgents();
        //    return _FrameworkAgents.GetMerchantsSale(_Request);
        //}
    }
}
