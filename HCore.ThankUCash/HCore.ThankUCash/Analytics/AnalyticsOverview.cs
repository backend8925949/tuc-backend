//==================================================================================
// FileName: AnalyticsOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankUCash.Object;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using HCore.Data.Store;

namespace HCore.ThankUCash.Analytics
{
    public class AnalyticsOverview
    {
        HCoreContext _HCoreContext;
        OOverview.Overview _OOverviewResponse;
        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Appuser)
                                {
                                    #region Get  Data
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                   && m.AccountId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                    && m.AccountId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    #endregion
                                }
                                else if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Acquirer)
                                {
                                    #region Load Overview
                                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.MerchantId == AccountDetails.UserAccountId && x.AcquirerId == SubUserAccountId).Count();
                                    _OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == AccountDetails.UserAccountId
                                        && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                        && x.TUCTerminalStore.Any(m => m.AcquirerId == SubUserAccountId)
                                        ).Count();
                                    #region Get  Data
                                    _OOverviewResponse.Commission = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m =>
                                                                      m.AccountId == SubUserAccountId &&
                                                                      m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                         m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.Settlement
                                                                        ).Sum(m => (double?)m.TotalAmount) ?? 0;
                                    #endregion

                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                         && m.ModeId == Helpers.TransactionMode.Credit
                                                                         && m.TransactionDate > TDayStart
                                                                         && m.TransactionDate < TDayEnd
                                                                         && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                         && m.ModeId == TransactionMode.Credit
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                          ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.IdleTerminals = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > T7DayStart
                                                                        && m.TransactionDate < T7DayEnd
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.DeadTerminals = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                      && m.ModeId == Helpers.TransactionMode.Credit
                                                                      && m.TransactionDate < TDeadDayEnd
                                                                      && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                      && m.ModeId == TransactionMode.Credit
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Select(x => x.CreatedById).Distinct().Count();


                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == SubUserAccountId
                                                       && m.ParentId == AccountDetails.UserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                             m.BankId == SubUserAccountId
                                                                               && m.ParentId == AccountDetails.UserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }


                                    _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                                  x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                                  && (_HCoreContext.HCUAccountOwner.Where(m => m.OwnerId == AccountDetails.UserAccountId && m.Account.OwnerId == x.Id && m.Account.BankId == SubUserAccountId).Count() > 0)
                                  ).Select(x => new OOverview.PosOverview
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      DisplayName = x.DisplayName,
                                  }).Skip(0).Take(50).ToList();
                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        PosAcc.Terminals = _HCoreContext.TUCTerminal.Count(x =>
                                            x.AcquirerId == SubUserAccountId
                                            && x.ProviderId == AccountDetails.UserAccountId);
                                        var LastTransaction = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m =>
                                                                             m.BankId == SubUserAccountId
                                                                           && m.ParentId == AccountDetails.UserAccountId
                                                                           && m.CreatedBy.OwnerId == PosAcc.ReferenceId
                                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && m.TransactionDate > _Request.StartTime
                                                                           && m.TransactionDate < _Request.EndTime
                                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                            )
                                                                           .OrderByDescending(m => m.TransactionDate)
                                                                           .Select(m => new
                                                                           {
                                                                               MerchantDisplayName = m.Parent.DisplayName,
                                                                               StoreDisplayName = m.SubParent.DisplayName,
                                                                               TransactionDate = m.TransactionDate
                                                                           })
                                                                           .FirstOrDefault();

                                        if (LastTransaction != null)
                                        {
                                            PosAcc.MerchantDisplayName = LastTransaction.MerchantDisplayName;
                                            PosAcc.StoreDisplayName = LastTransaction.StoreDisplayName;
                                            PosAcc.LastTransactionDate = LastTransaction.TransactionDate;
                                        }
                                    }
                                    #endregion


                                }
                                else
                                {
                                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime
                               && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));
                                    _OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == AccountDetails.UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == AccountDetails.UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == AccountDetails.UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    _OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == AccountDetails.UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    _OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == AccountDetails.UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    _OOverviewResponse.RewardCardsUsed = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == AccountDetails.UserAccountId && x.StatusId != TUStatusHelper.Card.NotAssigned && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();

                                    _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                            .Where(m =>
                                                            m.ParentId == AccountDetails.UserAccountId &&
                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                             m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                            m.ParentId == AccountDetails.UserAccountId &&
                                                             m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                            .Sum(m => (double?)m.TotalAmount) ?? 0);
                                    #region Get  Data
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                         m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.Settlement
                                                                        ).Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.SettlementCredit = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                          m.TransactionDate > _Request.StartTime &&
                                                        m.TransactionDate < _Request.EndTime &&
                                                         m.SourceId == TransactionSource.Settlement
                                                         ).Sum(m => (double?)m.TotalAmount) ?? 0;


                                    _OOverviewResponse.SettlementDebit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;

                                    #endregion
                                    List<OOverview.AcquirerCollection> Acquirers = _HCoreContext.HCUAccount
                     .Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.StatusId == HelperStatus.Default.Active)
                     .Select(x => new OOverview.AcquirerCollection
                     {
                         ReferenceId = x.Id,
                         ReferenceKey = x.Guid,
                         DisplayName = x.DisplayName,
                         IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                         PurchaseAmount = 0,
                         Terminals = 0,
                     }).ToList();

                                    var Terminals = _HCoreContext.TUCTerminal
                                       .Where(x => x.ProviderId == AccountDetails.UserAccountId)
                                       .Select(x => new
                                       {
                                           TerminalId = x.Id,
                                           AcquirerId = x.AcquirerId,
                                       })
                                       .ToList();
                                    foreach (var Terminal in Terminals)
                                    {
                                        //   long TerminalAcquirer = _HCoreContext.HCUAccountOwner
                                        //.Where(x => x.Owner.AccountTypeId == Helpers.UserAccountType.Acquirer
                                        //&& x.UserAccountId == TerminalId
                                        //&& x.AccountTypeId == Helpers.UserAccountType.TerminalAccount)
                                        //.Select(x => x.OwnerId).FirstOrDefault();

                                        double PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TerminalId == Terminal.TerminalId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                       && m.SourceId == TransactionSource.ThankUCashPlus
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                        if (Acquirers.FirstOrDefault(x => x.ReferenceId == Terminal.AcquirerId) != null)
                                        {
                                            Acquirers.FirstOrDefault(x => x.ReferenceId == Terminal.AcquirerId).PurchaseAmount = Acquirers.FirstOrDefault(x => x.ReferenceId == Terminal.AcquirerId).PurchaseAmount + PurchaseAmount;
                                            Acquirers.FirstOrDefault(x => x.ReferenceId == Terminal.AcquirerId).Terminals = Acquirers.FirstOrDefault(x => x.ReferenceId == Terminal.AcquirerId).Terminals + 1;
                                        }
                                    }
                                    _OOverviewResponse.AcquirerAmountDistribution = Acquirers.Where(x => x.PurchaseAmount > 0).ToList();


                                    _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId));
                                    _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.UserAccountId));
                                    if (_OOverviewResponse.ThankUCashPlus > 0)
                                    {
                                        _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.UserAccountId));
                                        _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.UserAccountId));
                                        _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.UserAccountId));
                                    }
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Appuser)
                                {
                                    #region Get     Data
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                   && m.AccountId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                    && m.AccountId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    #endregion

                                }
                                else if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Acquirer)
                                {
                                    #region Load Overview
                                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.MerchantId == AccountDetails.UserAccountId && x.AcquirerId == SubUserAccountId).Count();
                                    #region Get  Data
                                    _OOverviewResponse.Commission = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m =>
                                                                      m.AccountId == SubUserAccountId &&
                                                                      m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                         m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.Settlement
                                                                        ).Sum(m => (double?)m.TotalAmount) ?? 0;


                                    #endregion

                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                         && m.ModeId == Helpers.TransactionMode.Credit
                                                                         && m.TransactionDate > TDayStart
                                                                         && m.TransactionDate < TDayEnd
                                                                         && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                         && m.ModeId == TransactionMode.Credit
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                          ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.IdleTerminals = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > T7DayStart
                                                                        && m.TransactionDate < T7DayEnd
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.DeadTerminals = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == AccountDetails.UserAccountId && a.Account.BankId == SubUserAccountId).Count() > 0
                                                                      && m.ModeId == Helpers.TransactionMode.Credit
                                                                      && m.TransactionDate < TDeadDayEnd
                                                                      && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                      && m.ModeId == TransactionMode.Credit
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Select(x => x.CreatedById).Distinct().Count();


                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == SubUserAccountId
                                                       && m.SubParentId == AccountDetails.UserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                             m.BankId == SubUserAccountId
                                                                               && m.SubParentId == AccountDetails.UserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }


                                    _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                                  x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                                  && (_HCoreContext.HCUAccountOwner.Where(m => m.OwnerId == AccountDetails.UserAccountId && m.Account.OwnerId == x.Id && m.Account.BankId == SubUserAccountId).Count() > 0)
                                  ).Select(x => new OOverview.PosOverview
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      DisplayName = x.DisplayName,
                                  }).Skip(0).Take(50).ToList();
                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        PosAcc.Terminals = _HCoreContext.TUCTerminal.Count(x =>
                                            x.AcquirerId == SubUserAccountId
                                            && x.ProviderId == AccountDetails.UserAccountId);
                                        var LastTransaction = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m =>
                                                                             m.BankId == SubUserAccountId
                                                                           && m.SubParentId == AccountDetails.UserAccountId
                                                                           && m.CreatedBy.OwnerId == PosAcc.ReferenceId
                                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && m.TransactionDate > _Request.StartTime
                                                                           && m.TransactionDate < _Request.EndTime
                                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                            )
                                                                           .OrderByDescending(m => m.TransactionDate)
                                                                           .Select(m => new
                                                                           {
                                                                               MerchantDisplayName = m.Parent.DisplayName,
                                                                               StoreDisplayName = m.SubParent.DisplayName,
                                                                               TransactionDate = m.TransactionDate
                                                                           })
                                                                           .FirstOrDefault();

                                        if (LastTransaction != null)
                                        {
                                            PosAcc.MerchantDisplayName = LastTransaction.MerchantDisplayName;
                                            PosAcc.StoreDisplayName = LastTransaction.StoreDisplayName;
                                            PosAcc.LastTransactionDate = LastTransaction.TransactionDate;
                                        }
                                    }
                                    #endregion


                                }
                                else
                                {
                                    _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.OwnerId));
                                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                    && x.TransactionDate > _Request.StartTime
                                    && x.TransactionDate < _Request.EndTime)
                                    .Select(x => x.AccountId)
                                    .Distinct()
                                    .Count();
                                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == AccountDetails.UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                                    #region Get     Data
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    #endregion


                                    _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.OwnerId));
                                    if (_OOverviewResponse.ThankUCashPlus > 0)
                                    {
                                        //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                                        _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.OwnerId));
                                        _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.OwnerId));
                                        _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.OwnerId));
                                    }
                                }


                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                long MerchantId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.OwnerId).FirstOrDefault();
                                long StoreId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.OwnerId).FirstOrDefault();
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", MerchantId));
                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();

                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                              && x.Account.AccountTypeId == UserAccountType.Appuser
                              && x.Account.OwnerId == MerchantId
                              && x.TransactionDate > _Request.StartTime
                              && x.TransactionDate < _Request.EndTime)
                              .Select(x => x.AccountId)
                              .Distinct()
                              .Count();

                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                             && x.Account.AccountTypeId == UserAccountType.Appuser
                             && x.Account.OwnerId != MerchantId
                             && x.TransactionDate > _Request.StartTime
                             && x.TransactionDate < _Request.EndTime)
                             .Select(x => x.AccountId)
                             .Distinct()
                             .Count();
                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.HCUAccountTransactionAccount.Where(m => m.CreatedById == AccountDetails.UserAccountId
                                && m.AccountId == x.Id
                                && m.TransactionDate > _Request.StartTime
                                && m.TransactionDate < _Request.EndTime)
                                .Count() > 1)
                                .Distinct()
                                .Count();

                                #region Get     Data
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;


                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }

                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                            && m.ModeId == TransactionMode.Credit
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                     && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                         (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && m.SourceId == TransactionSource.TUC
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();


                                #endregion


                                _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.OwnerId));
                                if (_OOverviewResponse.ThankUCashPlus > 0)
                                {
                                    //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.OwnerId));
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data

                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Count();
                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                           .Count();

                                _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.ThankUCashPlus)
                                                                         .Count();



                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                              m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0);


                                _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0);



                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                            m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == TransactionMode.Credit
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                     && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                                #endregion

                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Acquirer)
                            {
                                #region Load Overview
                                _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.StatusId == HelperStatus.Default.Active).Count();
                                _OOverviewResponse.Ptsp = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.ProviderId).Distinct().Count();
                                _OOverviewResponse.Stores = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.StoreId).Distinct().Count();
                                _OOverviewResponse.Merchants = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                _OOverviewResponse.ReferredAppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.OwnerId == AccountDetails.UserAccountId);
                                _OOverviewResponse.ReferredMerchants = _HCoreContext.HCUAccount.Where(x => x.OwnerId == AccountDetails.UserAccountId && x.AccountTypeId == Helpers.UserAccountType.Merchant).Count();
                                //_OOverviewResponse.ReferredReferredStores = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == AccountDetails.UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore).Count();
                                #region Get  Data
                                //_OOverviewResponse.ReferredAppUsersVisit = _HCoreContext.HCUAccountTransaction
                                //                             .Where(m => m.Account.OwnerId == AccountDetails.UserAccountId
                                //                              && m.TransactionDate > _Request.StartTime
                                //                              && m.TransactionDate < _Request.EndTime
                                //                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                              && m.StatusId == HelperStatus.Transaction.Success
                                //                            ).Count();
                                //_OOverviewResponse.ReferredAppUsersPurchase = _HCoreContext.HCUAccountTransaction
                                //                             .Where(m => m.Account.OwnerId == AccountDetails.UserAccountId
                                //                              && m.TransactionDate > _Request.StartTime
                                //                              && m.TransactionDate < _Request.EndTime
                                //                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                              && m.StatusId == HelperStatus.Transaction.Success
                                //                            ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                //_OOverviewResponse.ReferredMerchantVisits = _HCoreContext.HCUAccountTransaction
                                //                            .Where(m => m.Parent.OwnerId == AccountDetails.UserAccountId
                                //                             && m.TransactionDate > _Request.StartTime
                                //                             && m.TransactionDate < _Request.EndTime
                                //                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                             && m.StatusId == HelperStatus.Transaction.Success
                                //                           ).Count();
                                //_OOverviewResponse.ReferredMerchantSale = _HCoreContext.HCUAccountTransaction
                                // .Where(m => m.Parent.OwnerId == AccountDetails.UserAccountId
                                //  && m.TransactionDate > _Request.StartTime
                                //  && m.TransactionDate < _Request.EndTime
                                //  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //  && m.StatusId == HelperStatus.Transaction.Success
                                //).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                                //_OOverviewResponse.Commission = _HCoreContext.HCUAccountTransaction
                                //.Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                // m.StatusId == HelperStatus.Transaction.Success &&
                                // m.ModeId == Helpers.TransactionMode.Credit &&
                                //  m.TransactionDate > _Request.StartTime &&
                                //m.TransactionDate < _Request.EndTime &&
                                //m.SourceId == TransactionSource.Settlement
                                //).Sum(m => (double?)m.TotalAmount) ?? 0;


                                #endregion
                                _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                               .Where(m =>
                                                m.BankId == AccountDetails.UserAccountId
                                                && m.Account.AccountTypeId == UserAccountType.Appuser
                                                && m.TransactionDate > _Request.StartTime
                                                && m.TransactionDate < _Request.EndTime
                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                && m.StatusId == HelperStatus.Transaction.Success
                                                 ).Count();
                                _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                 .Where(m =>
                                                  m.BankId == AccountDetails.UserAccountId
                                                  && m.Account.AccountTypeId == UserAccountType.Appuser
                                                  && m.TransactionDate > _Request.StartTime
                                                  && m.TransactionDate < _Request.EndTime
                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                  && m.StatusId == HelperStatus.Transaction.Failed
                                                   ).Count();
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                  .Where(m =>
                                                   m.BankId == AccountDetails.UserAccountId
                                                   && m.Account.AccountTypeId == UserAccountType.Appuser
                                                   && m.TransactionDate > _Request.StartTime
                                                   && m.TransactionDate < _Request.EndTime
                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                   && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                    ).Count();
                                if (_OOverviewResponse.Transactions > 0)
                                {
                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                          m.BankId == AccountDetails.UserAccountId
                                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                DateTime TDayStart = HCoreHelper.GetGMTDate();
                                DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                              x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                              && x.TUCTerminalProvider.Any(m => m.AcquirerId == AccountDetails.UserAccountId)
                              ).Select(x => new OOverview.PosOverview
                              {
                                  ReferenceId = x.Id,
                                  ReferenceKey = x.Guid,
                                  DisplayName = x.DisplayName,
                                  IconUrl = x.IconStorage.Path
                              }).Skip(0).Take(50).ToList();
                                foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                {
                                    if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                    {
                                        PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                    }
                                    PosAcc.Terminals = _HCoreContext.TUCTerminal.Count(x =>
                                        x.AcquirerId == AccountDetails.UserAccountId
                                        && x.ProviderId == PosAcc.ReferenceId);


                                    PosAcc.UnusedTerminals = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Count() == 0).Count();
                                    PosAcc.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                       && m.CreatedBy.OwnerId == PosAcc.ReferenceId && m.TransactionDate > TDayStart
                                                                         && m.TransactionDate < TDayEnd
                                                                         ).Select(x => x.CreatedById).Distinct().Count();
                                    PosAcc.IdleTerminals = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                      && m.CreatedBy.OwnerId == PosAcc.ReferenceId && m.TransactionDate > T7DayStart
                                                                       && m.TransactionDate < T7DayEnd
                                                                        ).Select(x => x.CreatedById).Distinct().Count();

                                    PosAcc.DeadTerminals = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                         && m.CreatedBy.OwnerId == PosAcc.ReferenceId && m.TransactionDate < TDeadDayEnd
                                                                          ).Select(x => x.CreatedById).Distinct().Count();

                                    var LastTransaction = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m =>
                                                                         m.BankId == AccountDetails.UserAccountId
                                                                       && m.CreatedBy.OwnerId == PosAcc.ReferenceId
                                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        )
                                                                       .OrderByDescending(m => m.TransactionDate)
                                                                       .Select(m => new
                                                                       {
                                                                           MerchantDisplayName = m.Parent.DisplayName,
                                                                           StoreDisplayName = m.SubParent.DisplayName,
                                                                           TransactionDate = m.TransactionDate
                                                                       })
                                                                       .FirstOrDefault();
                                    if (LastTransaction != null)
                                    {
                                        PosAcc.MerchantDisplayName = LastTransaction.MerchantDisplayName;
                                        PosAcc.StoreDisplayName = LastTransaction.StoreDisplayName;
                                        PosAcc.LastTransactionDate = LastTransaction.TransactionDate;
                                    }
                                }
                                #endregion
                                #region Terminals Overview
                                _OOverviewResponse.UnusedTerminals = _HCoreContext.TUCTerminal.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Count() == 0).Count();
                                _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > TDayStart
                                                                     && m.TransactionDate < TDayEnd
                                                                     ).Select(x => x.CreatedById).Distinct().Count();
                                _OOverviewResponse.IdleTerminals = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > T7DayStart
                                                                   && m.TransactionDate < T7DayEnd
                                                                    ).Select(x => x.CreatedById).Distinct().Count();

                                _OOverviewResponse.DeadTerminals = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                      && m.TransactionDate < TDeadDayEnd
                                                                      ).Select(x => x.CreatedById).Distinct().Count();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.RelationshipManager)
                            {
                                #region Load Overview


                                _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.RmId == AccountDetails.UserAccountId && x.StatusId == HelperStatus.Default.Active).Select(x => x.MerchantId).Distinct().Count();
                                _OOverviewResponse.Ptsp = HCoreDataStore.Terminals.Where(x => x.RmId == AccountDetails.UserAccountId && x.StatusId == HelperStatus.Default.Active).Select(x => x.ProviderId).Distinct().Count();
                                _OOverviewResponse.Stores = HCoreDataStore.Terminals.Where(x => x.RmId == AccountDetails.UserAccountId && x.StatusId == HelperStatus.Default.Active).Select(x => x.StoreId).Distinct().Count();
                                _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Where(x => x.RmId == AccountDetails.UserAccountId && x.StatusId == HelperStatus.Default.Active).Select(x => x.ReferenceId).Count();

                                DateTime TDayStart = HCoreHelper.GetGMTDate();
                                DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                                #region Terminals Overview
                                _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate == null && x.StatusId == HelperStatus.Default.Active);
                                _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd && x.StatusId == HelperStatus.Default.Active);
                                _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd && x.StatusId == HelperStatus.Default.Active);
                                _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd && x.StatusId == HelperStatus.Default.Active);
                                #endregion


                                //_OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                //               .Where(m =>
                                //                m.BankId == AccountDetails.UserAccountId
                                //                && m.Account.AccountTypeId == UserAccountType.Appuser
                                //                && m.TransactionDate > _Request.StartTime
                                //                && m.TransactionDate < _Request.EndTime
                                //                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.App)
                                //                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                && m.StatusId == HelperStatus.Transaction.Success
                                //                 ).Count();
                                //_OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                //                 .Where(m =>
                                //                  m.BankId == AccountDetails.UserAccountId
                                //                  && m.Account.AccountTypeId == UserAccountType.Appuser
                                //                  && m.TransactionDate > _Request.StartTime
                                //                  && m.TransactionDate < _Request.EndTime
                                //                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.App)
                                //                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                  && m.StatusId == HelperStatus.Transaction.Failed
                                //                   ).Count();
                                //if (_OOverviewResponse.Transactions > 0)
                                //{
                                //    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                //                                         .Where(m =>
                                //                                          m.BankId == AccountDetails.UserAccountId
                                //                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                //                                          && m.TransactionDate > _Request.StartTime
                                //                                          && m.TransactionDate < _Request.EndTime
                                //                                          && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.App)
                                //                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                                          && m.StatusId == HelperStatus.Transaction.Success
                                //                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                //}

                                //  _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                                //x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                                //&& x.InverseOwner.Any(m => m.BankId == AccountDetails.OwnerId && m.AccountTypeId == UserAccountType.TerminalAccount)
                                //).Select(x => new OOverview.PosOverview
                                //{
                                //    ReferenceId = x.Id,
                                //    ReferenceKey = x.Guid,
                                //    DisplayName = x.DisplayName,
                                //    IconUrl = x.IconStorage.Path
                                //}).Skip(0).Take(50).ToList();
                                //foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                //{
                                //    if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                //    {
                                //        PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                //    }
                                //    #region Terminals Overview
                                //    PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.OwnerId && x.ProviderId == PosAcc.ReferenceId && x.RmId == AccountDetails.UserAccountId);
                                //    PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x =>  x.ProviderId == PosAcc.ReferenceId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                //    PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                //    PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                //    PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x =>   x.ProviderId == PosAcc.ReferenceId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                //    #endregion
                                //}
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                          && x.CreateDate > _Request.StartTime
                          && x.CreateDate < _Request.EndTime)
                          .Count();

                        _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                         x.AccountTypeId == UserAccountType.Appuser
                         && x.HCUAccountTransactionAccount.Where(m => m.AccountId == x.Id
                         && m.TransactionDate > _Request.StartTime
                         && m.TransactionDate < _Request.EndTime)
                         .Count() > 1)
                         .Distinct()
                         .Count();

                        _OOverviewResponse.PurchaseAmount = 0;
                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Count();
                        if (_OOverviewResponse.Transactions > 0)
                        {
                            _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }

                        _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0);


                        _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.TotalAmount) ?? 0);



                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the account overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverviewLite(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                #region Get  Data
                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.CreateDate > _Request.StartTime
                            && x.CreateDate < _Request.EndTime
                            && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));

                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.CreateDate > _Request.StartTime
                                && x.CreateDate < _Request.EndTime
                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == AccountDetails.UserAccountId
                                && m.AccountId == x.Id) > 1)
                                .Distinct()
                                .Count();

                                _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime
                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == AccountDetails.UserAccountId
                               && m.AccountId == x.Id) == 1)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                   m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.Settlement
                                                                    ).Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                           && m.TypeId == TransactionType.CashReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > _Request.StartTime
                                                                           && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                                               && m.TypeId == TransactionType.CardReward
                                                                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.TransactionDate > _Request.StartTime
                                                                                                               && m.TransactionDate < _Request.EndTime
                                                                                                               && m.ModeId == TransactionMode.Credit
                                                                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                           m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                               m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.TUC)
                                                                         .Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                           m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                           m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                           m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.TUC)
                                                                          .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;


                                _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                   m.TransactionDate < _Request.EndTime &&
                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                             .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                                m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                  m.TransactionDate > _Request.StartTime &&
                                                                                   m.TransactionDate < _Request.EndTime &&
                                                                                m.SourceId == TransactionSource.ThankUCashPlus)
                                                                          .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.TUCPlusRewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                m.StatusId == HelperStatus.Transaction.Success &&
                                                                m.ModeId == Helpers.TransactionMode.Credit &&
                                                                m.TransactionDate > _Request.StartTime &&
                                                                m.TransactionDate < _Request.EndTime &&
                                                                m.SourceId == TransactionSource.ThankUCashPlus)
                                                          .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                if (_Request.Type != "lite")
                                {
                                    _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                                  && x.StatusId == HelperStatus.Default.Active)
                                                  .Select(x => new OOverview.PosOverview
                                                  {
                                                      DisplayName = x.DisplayName,
                                                      ReferenceKey = x.Guid,
                                                      ReferenceId = x.Id,
                                                      Terminals = _HCoreContext.TUCTerminal
                                                           .Count(a => a.StatusId == HelperStatus.Default.Active
                                                               && a.AcquirerId == AccountDetails.UserAccountId),
                                                  }).ToList();

                                    foreach (var PosOverview in _OOverviewResponse.PosOverview)
                                    {
                                        PosOverview.ActiveTerminals = _HCoreContext.TUCTerminal
                                  .Count(a =>
                                       a.StatusId == HelperStatus.Default.Active
                                      && a.ProviderId == PosOverview.ReferenceId
                                      && a.AcquirerId == AccountDetails.UserAccountId
                                      && a.HCUAccountTransaction.Count(m => m.ParentId == AccountDetails.UserAccountId && m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime) > 0);

                                        PosOverview.IdleTerminals = _HCoreContext.TUCTerminal
                                        .Count(a => a.Provider.StatusId == HelperStatus.Default.Active
                                            && a.ProviderId == PosOverview.ReferenceId
                                            && a.AccountId == AccountDetails.UserAccountId
                                            && a.HCUAccountTransaction.Count(m => m.ParentId == AccountDetails.UserAccountId && m.TransactionDate > _Request.StartTime &&
                                                                                       m.TransactionDate < _Request.EndTime) == 0);

                                        PosOverview.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId
                                       && a.ParentId == AccountDetails.UserAccountId).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                                        PosOverview.LastTransactionLocation = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId
                                       && a.ParentId == AccountDetails.UserAccountId).OrderByDescending(x => x.TransactionDate).Select(x => x.SubParent.DisplayName).FirstOrDefault();

                                    }

                                }

                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Acquirer)
                            {
                                DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                                DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                                if (SubUserAccountTypeId == UserAccountType.Terminal)
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.CreatedById == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.CreatedById == SubUserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                               && m.CreatedById == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                              && m.CreatedById == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.CreatedById == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                 && m.CreatedById == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                && m.CreatedById == SubUserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                               && m.CreatedById == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                 && m.CreatedById == SubUserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.CreatedById == SubUserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.CreatedById == SubUserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();
                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.CreatedById == SubUserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.CreatedById == SubUserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.CreatedById == SubUserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                else if (SubUserAccountTypeId == UserAccountType.Merchant)
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                               && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                              && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                 && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                && m.ParentId == SubUserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                               && m.ParentId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                 && m.ParentId == SubUserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.ParentId == SubUserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();
                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.ParentId == SubUserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                else
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveMerchants = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                          && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.ParentId).Distinct().Count();

                                    _OOverviewResponse.ActiveMerchantsDiff = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                       && m.TransactionDate > DiffStartTime
                                                               && m.TransactionDate < DiffEndTime
                                                                        ).Select(x => x.ParentId).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();

                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }


                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.PosAccount)
                            {
                                DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                                DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                                if (SubUserAccountTypeId == UserAccountType.Terminal)
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.TerminalId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.TerminalId == SubUserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.TerminalId).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                               && m.TerminalId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                              && m.TerminalId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.TerminalId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                 && m.TerminalId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                && m.TerminalId == SubUserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                               && m.TerminalId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                 && m.TerminalId == SubUserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.TerminalId == SubUserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.TerminalId == SubUserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();
                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.TerminalId == SubUserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.TerminalId == SubUserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.TerminalId == SubUserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                else if (SubUserAccountTypeId == UserAccountType.Merchant)
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                               && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                              && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                 && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                && m.ParentId == SubUserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                               && m.ParentId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                 && m.ParentId == SubUserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.ParentId == SubUserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();
                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.ParentId == SubUserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                else
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveMerchants = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                          && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.ParentId).Distinct().Count();

                                    _OOverviewResponse.ActiveMerchantsDiff = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                       && m.TransactionDate > DiffStartTime
                                                               && m.TransactionDate < DiffEndTime
                                                                        ).Select(x => x.ParentId).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;




                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }

                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data

                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Count();
                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                //_OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                // m.StatusId == HelperStatus.Transaction.Success &&
                                // m.ModeId == Helpers.TransactionMode.Credit &&
                                //   m.TransactionDate > _Request.StartTime &&
                                // m.TransactionDate < _Request.EndTime &&
                                // m.SourceId == TransactionSource.ThankUCashPlus)
                                //.Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                  m.StatusId == HelperStatus.Transaction.Success &&
                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                    m.TransactionDate > _Request.StartTime &&
                                  m.TransactionDate < _Request.EndTime &&
                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                .Count();

                                _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                  m.StatusId == HelperStatus.Transaction.Success &&
                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                    m.TransactionDate > _Request.StartTime &&
                                    m.TransactionDate < _Request.EndTime &&
                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                .Count();



                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                          m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                //_OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                //  .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                //    m.StatusId == HelperStatus.Transaction.Success &&
                                //    m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                //    m.ModeId == Helpers.TransactionMode.Credit &&
                                //m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                //    m.TransactionDate > _Request.StartTime &&
                                // m.TransactionDate < _Request.EndTime &&
                                //  m.SourceId == TransactionSource.TUC)
                                //.Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                 .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                   m.StatusId == HelperStatus.Transaction.Success &&
                                   m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                    m.TransactionDate > _Request.StartTime &&
                                  m.TransactionDate < _Request.EndTime &&
                                  m.SourceId == TransactionSource.TUC)
                                .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                //_OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                //.Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                //  m.StatusId == HelperStatus.Transaction.Success &&
                                //  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                //  m.ModeId == Helpers.TransactionMode.Debit &&
                                //    m.TransactionDate > _Request.StartTime &&
                                // m.TransactionDate < _Request.EndTime &&
                                //  m.SourceId == TransactionSource.TUC)
                                //.Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                  m.StatusId == HelperStatus.Transaction.Success &&
                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                    m.TransactionDate > _Request.StartTime &&
                                  m.TransactionDate < _Request.EndTime &&
                                  m.SourceId == TransactionSource.TUC)
                                .Count();


                                _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0);


                                _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0);

                                _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success)
                                                               .Select(x => new OOverview.MiniTransaction
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   TypeName = x.Type.Name,
                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   MerchantName = x.Parent.DisplayName,
                                                                   RewardAmount = x.TotalAmount,
                                                                   TransactionDate = x.TransactionDate,
                                                                   StoreAddress = x.SubParent.Address,
                                                                   StoreDisplayName = x.SubParent.DisplayName,
                                                               })
                                                               .FirstOrDefault();
                                // Card Reward
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                                #endregion

                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                                DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                               && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || m.SourceId == TransactionSource.ThankUCashPlus)
                                                           ).Count();

                                _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && m.TransactionDate > DiffStartTime
                                                              && m.TransactionDate < DiffEndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                              && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || m.SourceId == TransactionSource.ThankUCashPlus)
                                                          ).Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                              && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                               && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || m.SourceId == TransactionSource.ThankUCashPlus)
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && m.TransactionDate > DiffStartTime
                                                              && m.TransactionDate < DiffEndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                              && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || m.SourceId == TransactionSource.ThankUCashPlus)
                                                            ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                         && m.TypeId == TransactionType.CashReward
                                                                         && m.ModeId == Helpers.TransactionMode.Credit
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.ModeId == TransactionMode.Credit
                                                                         && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                              ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > DiffStartTime
                                                                        && m.TransactionDate < DiffEndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                          && m.TypeId == TransactionType.CardReward
                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && m.ModeId == TransactionMode.Credit
                                                                          && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > DiffStartTime
                                                                       && m.TransactionDate < DiffEndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                        .Where(m =>
                                         m.TerminalId == AccountDetails.UserAccountId
                                         && m.Account.AccountTypeId == UserAccountType.Appuser
                                         && m.TransactionDate > _Request.StartTime
                                         && m.TransactionDate < _Request.EndTime
                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                         && m.StatusId == HelperStatus.Transaction.Success
                                          ).Count();
                                _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                 .Where(m =>
                                                  m.TerminalId == AccountDetails.UserAccountId
                                                  && m.Account.AccountTypeId == UserAccountType.Appuser
                                                  && m.TransactionDate > _Request.StartTime
                                                  && m.TransactionDate < _Request.EndTime
                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                  && m.StatusId == HelperStatus.Transaction.Failed
                                                   ).Count();
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                  .Where(m =>
                                                   m.TerminalId == AccountDetails.UserAccountId
                                                   && m.Account.AccountTypeId == UserAccountType.Appuser
                                                   && m.TransactionDate > _Request.StartTime
                                                   && m.TransactionDate < _Request.EndTime
                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                   && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                    ).Count();
                                if (_OOverviewResponse.Transactions > 0)
                                {
                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                          m.TerminalId == AccountDetails.UserAccountId
                                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ReferenceId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.RelationshipManager)
                            {
                                DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                                DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                                if (SubUserAccountTypeId == UserAccountType.Terminal)
                                {
                                    #region Build Data
                                    _OOverviewResponse.Stores = _HCoreContext.HCUAccountOwner
                                                                .Count(x => x.AccountId == AccountDetails.UserAccountId
                                                                && x.Owner.AccountTypeId == UserAccountType.MerchantStore
                                                                && x.StatusId == HelperStatus.Default.Active);
                                    _OOverviewResponse.Merchants = _HCoreContext.HCUAccountOwner
                                                               .Where(x => x.AccountId == AccountDetails.UserAccountId
                                                               && x.Owner.AccountTypeId == UserAccountType.MerchantStore
                                                               && x.StatusId == HelperStatus.Default.Active).Select(x => x.Owner.OwnerId).Count();
                                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal
                                                               .Count(x => x.AcquirerId == AccountDetails.OwnerId
                                                               && x.Merchant.HCUAccountOwnerOwner.Any(m => m.AccountId == AccountDetails.UserAccountId));

                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                                    #region Terminals Overview
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.OwnerId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.OwnerId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.OwnerId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.OwnerId && x.RmId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    #endregion
                                    #endregion

                                }
                                else if (SubUserAccountTypeId == UserAccountType.Merchant)
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId && m.ParentId == SubUserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                               && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                              && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.ParentId == SubUserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                 && m.ParentId == SubUserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                && m.ParentId == SubUserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                               && m.ParentId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                                 && m.ParentId == SubUserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.ParentId == SubUserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();
                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.ParentId == SubUserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();
                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.ParentId == SubUserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.MerchantId == SubUserAccountId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.MerchantId == SubUserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }
                                else
                                {
                                    #region Build Data

                                    _OOverviewResponse.ActiveMerchants = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                          && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.ParentId).Distinct().Count();

                                    _OOverviewResponse.ActiveMerchantsDiff = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                       && m.TransactionDate > DiffStartTime
                                                               && m.TransactionDate < DiffEndTime
                                                                        ).Select(x => x.ParentId).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                          ).Select(x => x.CreatedById).Distinct().Count();
                                    _OOverviewResponse.ActiveTerminalsDiff = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                        && m.TransactionDate > DiffStartTime
                                                                && m.TransactionDate < DiffEndTime
                                                                         ).Select(x => x.CreatedById).Distinct().Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                               ).Count();

                                    _OOverviewResponse.TransactionsDiff = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                              ).Count();

                                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.PurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                  && m.TransactionDate > DiffStartTime
                                                                  && m.TransactionDate < DiffEndTime
                                                                  && m.ModeId == TransactionMode.Credit
                                                                  && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || m.SourceId == TransactionSource.ThankUCashPlus)
                                                                ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                             && m.TypeId == TransactionType.CashReward
                                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                                             && m.TransactionDate > _Request.StartTime
                                                                             && m.TransactionDate < _Request.EndTime
                                                                             && m.ModeId == TransactionMode.Credit
                                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                                    _OOverviewResponse.CashRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > DiffStartTime
                                                                            && m.TransactionDate < DiffEndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                              && m.TypeId == TransactionType.CardReward
                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && m.ModeId == TransactionMode.Credit
                                                                              && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmountDiff = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.CreatedBy.BankId == AccountDetails.UserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > DiffStartTime
                                                                           && m.TransactionDate < DiffEndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TransactionSuccess = _HCoreContext.HCUAccountTransaction
                                            .Where(m =>
                                             m.BankId == AccountDetails.UserAccountId
                                             && m.Account.AccountTypeId == UserAccountType.Appuser
                                             && m.TransactionDate > _Request.StartTime
                                             && m.TransactionDate < _Request.EndTime
                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                             && m.StatusId == HelperStatus.Transaction.Success
                                              ).Count();

                                    _OOverviewResponse.TransactionFailed = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                      m.BankId == AccountDetails.UserAccountId
                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Failed
                                                       ).Count();

                                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                       m.BankId == AccountDetails.UserAccountId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Failed)
                                                        ).Count();
                                    if (_OOverviewResponse.Transactions > 0)
                                    {
                                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m =>
                                                                              m.BankId == AccountDetails.UserAccountId
                                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                              && m.TransactionDate > _Request.StartTime
                                                                              && m.TransactionDate < _Request.EndTime
                                                                              && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                                    _OOverviewResponse.PosOverview = HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                        && HCoreDataStore.Terminals.Any(m => m.ProviderId == x.ReferenceId && m.AcquirerId == AccountDetails.UserAccountId) != false)
                                        .Select(x => new OOverview.PosOverview
                                        {
                                            ReferenceId = x.ReferenceId,
                                            ReferenceKey = x.ReferenceKey,
                                            DisplayName = x.DisplayName,
                                            IconUrl = x.IconUrl,
                                        }).ToList();

                                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                                    {
                                        if (!string.IsNullOrEmpty(PosAcc.IconUrl))
                                        {
                                            PosAcc.IconUrl = _AppConfig.StorageUrl + PosAcc.IconUrl;
                                        }

                                        PosAcc.Terminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId);
                                        PosAcc.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                        PosAcc.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                        PosAcc.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                        PosAcc.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                        PosAcc.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.ProviderId == PosAcc.ReferenceId && x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    }

                                    #region Terminals Overview
                                    _OOverviewResponse.Merchants = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).Select(x => x.MerchantId).Distinct().Count();
                                    _OOverviewResponse.Terminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId);
                                    _OOverviewResponse.UnusedTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate == null);
                                    _OOverviewResponse.ActiveTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                                    _OOverviewResponse.IdleTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                                    _OOverviewResponse.DeadTerminals = HCoreDataStore.Terminals.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.LastTransactionDate > TDeadDayEnd);
                                    _OOverviewResponse.LastTransactionDate = HCoreDataStore.Terminals.Where(x => x.AcquirerId == AccountDetails.UserAccountId).OrderByDescending(x => x.LastTransactionDate).Select(x => x.LastTransactionDate).FirstOrDefault();
                                    #endregion
                                    #endregion

                                }


                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }

                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Get  Data
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                             && x.CreateDate > _Request.StartTime
                             && x.CreateDate < _Request.EndTime);
                        _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CreateDate > _Request.StartTime
                    && x.CreateDate < _Request.EndTime
                    && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id) > 1)
                    .Distinct()
                    .Count();

                        _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                   x.AccountTypeId == UserAccountType.Appuser
                   && x.CreateDate > _Request.StartTime
                   && x.CreateDate < _Request.EndTime
                   && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id) == 1)
                   .Distinct()
                   .Count();

                        _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                        _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m =>
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                     m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                                    .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                     ).Count();

                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m =>
                                                         m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m =>
                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                            m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                             m.TransactionDate > _Request.StartTime &&
                                                           m.TransactionDate < _Request.EndTime &&
                                                            m.SourceId == TransactionSource.Settlement
                                                            ).Sum(m => (double?)m.TotalAmount) ?? 0;





                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CashReward
                                                                   && m.ModeId == Helpers.TransactionMode.Credit
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                                      .Where(m =>
                                                                                                        m.TypeId == TransactionType.CardReward
                                                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                       && m.TransactionDate > _Request.StartTime
                                                                                                       && m.TransactionDate < _Request.EndTime
                                                                                                       && m.ModeId == TransactionMode.Credit
                                                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                                      .Where(m =>
                                                                                                            m.TypeId == TransactionType.CardReward
                                                                                                           && m.TypeId == TransactionType.CashReward
                                                                                                           && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                           && m.TransactionDate > _Request.StartTime
                                                                                                           && m.TransactionDate < _Request.EndTime
                                                                                                           && m.ModeId == TransactionMode.Credit
                                                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m =>
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                   m.SourceId == TransactionSource.TUC)
                                                                 .Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m =>
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                   m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                   m.TransactionDate < _Request.EndTime &&
                                                                   m.SourceId == TransactionSource.TUC)
                                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;


                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                           m.ModeId == Helpers.TransactionMode.Credit &&
                                                           m.TransactionDate > _Request.StartTime &&
                                                           m.TransactionDate < _Request.EndTime &&
                                                           m.SourceId == TransactionSource.ThankUCashPlus)
                                                     .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m =>
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Debit &&
                                                                          m.TransactionDate > _Request.StartTime &&
                                                                           m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;

                        #endregion

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }
                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

    }
}
