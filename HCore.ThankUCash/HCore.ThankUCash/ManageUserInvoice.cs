//==================================================================================
// FileName: ManageUserInvoice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash
{
    public class ManageUserInvoice
    {
        FrameworkUserInvoices _FrameworkUserInvoices;

        /// <summary>
        /// Description: Completes the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CompletePaymentInvoice(OInvoice.Manage _Request)
        {
            _FrameworkUserInvoices = new FrameworkUserInvoices();
            return _FrameworkUserInvoices.CompletePaymentInvoice(_Request);
        }
        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserInvoice(OInvoice.Manage _Request)
        {
            _FrameworkUserInvoices = new FrameworkUserInvoices();
            return _FrameworkUserInvoices.GetUserInvoice(_Request);
        }

        /// <summary>
        /// Description: Gets the user invoice list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserInvoice(OList.Request _Request)
        {
            _FrameworkUserInvoices = new FrameworkUserInvoices();
            return _FrameworkUserInvoices.GetUserInvoice(_Request);
        }
    }
}
