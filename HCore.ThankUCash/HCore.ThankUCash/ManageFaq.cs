//==================================================================================
// FileName: ManageFaq.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash
{
    public class ManageFaq
    {
        FrameworkFaq _FrameworkFaq;

        /// <summary>
        /// Description: Saves the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveFaqCategory(OFaqCategory.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.SaveFaqCategory(_Request);
        }
        /// <summary>
        /// Description: Updates the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateFaqCategory(OFaqCategory.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.UpdateFaqCategory(_Request);
        }
        /// <summary>
        /// Description: Deletes the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteFaqCategory(OFaqCategory.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.DeleteFaqCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqCategory(OFaqCategory.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqCategory(OList.Request _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ category client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqCategoryClient(OFaqCategory.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqCategoryClient(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ category client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqCategoryClient(OList.Request _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqCategoryClient(_Request);
        }


        /// <summary>
        /// Description: Saves the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveFaq(OFaq.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.SaveFaq(_Request);
        }
        /// <summary>
        /// Description: Updates the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateFaq(OFaq.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.UpdateFaq(_Request);
        }
        /// <summary>
        /// Description: Deletes the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteFaq(OFaq.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.DeleteFaq(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaq(OFaq.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaq(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaq(OList.Request _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaq(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqClient(OFaq.Manage _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqClient(_Request);
        }
        /// <summary>
        /// Description: Gets the FAQ client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFaqClient(OList.Request _Request)
        {
            _FrameworkFaq = new FrameworkFaq();
            return _FrameworkFaq.GetFaqClient(_Request);
        }
    }
}
