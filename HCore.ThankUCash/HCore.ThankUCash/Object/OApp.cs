//==================================================================================
// FileName: OApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OApp
    {
        public class OAppConfiguration
        {
            public class Request
            {
                public string? SerialNumber { get; set; }
                public string? OsName { get; set; }
                public string? Brands { get; set; }
                public string? Model { get; set; }
                public int Width { get; set; }
                public int Height { get; set; }
                public string? CarrierName { get; set; }
                public string? CountryCode { get; set; }
                public string? Mcc { get; set; }
                public string? Mnc { get; set; }
                public string? NotificationUrl { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string? AppVersion { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? ServerAppVersion { get; set; }
                public bool IsAppAvailable { get; set; }
                public bool IsDonationAvailable { get; set; }
                public bool IsReceiverAvailable { get; set; }
                public Donation Donation { get; set; }
                public List<TucMileStone> TucMileStones { get; set; }
            }
            public class TucMileStone
            {
                public string? Title { get; set; }
                public string? SystemName { get; set; }
                public string? Description { get; set; }
                public double Limit { get; set; }
            }
            public class Donation
            {
                public string? ReceiverTitle { get; set; }
                public string? ReceiverMessage { get; set; }

                public string? Title { get; set; }
                public string? Message { get; set; }
                public string? Description { get; set; }
                public string? Conditions { get; set; }
                public string? IconUrl { get; set; }
                public string? PaymentReference { get; set; }
                public List<DonationRange> DonationRanges { get; set; }
            }
            public class DonationRange
            {
                public string? Title { get; set; }
                public long Quantity { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
            }
        }

        public class OBillersResponse
        {
            public string? error { get; set; }
            public string? status { get; set; }
            public string? message { get; set; }
            public string? responseCode { get; set; }
            public List<OBiller> responseData { get; set; }
        }

        public class OCoralPaymentResponse
        {
            public bool error { get; set; }
            public string? status { get; set; }
            public string? message { get; set; }
            public string? responseCode { get; set; }

            public OCoralPayPaymentDetails responseData { get; set; }
            
        }
        public class OCoralPayPaymentDetails
        {
            public string? billerName { get; set; }
            public string? packageName { get; set; }
            public string? paymentReference { get; set; }
            public string? transactionId { get; set; }
            public string? vendStatus { get; set; }
            public string? narration { get; set; }
            public string? statusCode { get; set; }
            public double amount { get; set; }
            public DateTime? date { get; set; }
            public OCoralPayTokenData tokenData { get; set; }
            public OCoralPayCustomer customer { get; set; }
        }
        public class OCoralPayTokenData
        {
            public OCoralPayStandardToken stdToken { get; set; }
        }
        public class OCoralPayStandardToken
        {
            public double amount { get; set; }
            public string? receiptNumber { get; set; }
            public string? tariff { get; set; }
            public string? units { get; set; }
            public string? unitsType { get; set; }
            public string? value { get; set; }
        }
        public class OCoralPayCustomer
        {
            public string? firstName { get; set; }
            public string? lastName { get; set; }
            public string? customerName { get; set; }
            public string? accountNumber { get; set; }
            public string? accountStatus { get; set; }
            public string? phoneNumber { get; set; }
            public string? emailAddress { get; set; }
            public double totalDue { get; set; }
            public DateTime dueDate { get; set; }
            public bool canVend { get; set; }
            public double amount { get; set; }
            public string? orderId { get; set; }
            public string? statusCode { get; set; }

        }




        public class OBiller
        {
            public int id { get; set; }
            public string? name { get; set; }
            public string? slug { get; set; }
            public int groupId { get; set; }
            public double? amount { get; set; }
            public double rewardPercentage { get; set; }
        }

        public class OPaymentCharge
        {
            public long ReferenceId { get; set; }
            public long AccountId { get; set; }
            public string? EmailAddress { get; set; }
            public double Amount { get; set; }


            public string? Type { get; set; }
            public string? AccountKey { get; set; }
            public string? PaymentReference { get; set; }
            public string? RefTransactionId { get; set; }
            public string? RefStatus { get; set; }
            public string? RefMessage { get; set; }
            public string? AccountNumber { get; set; }
            public string? BillerName { get; set; }
            public string? PackageName { get; set; }
            public string? PackageId { get; set; }
            public string? OrderId { get; set; }


            public OUserReference? UserReference { get; set; }
        }
        public class OPaymentDetails
        {
            public string? Type { get; set; }
            public double Amount { get; set; }
            public string? AccountKey { get; set; }
            public long AccountId { get; set; }
            public string? PaymentReference { get; set; }
            public string? RefTransactionId { get; set; }
            public string? RefStatus { get; set; }
            public string? RefMessage { get; set; }
            public string? AccountNumber { get; set; }
            public string? BillerName { get; set; }
            public string? PackageName { get; set; }
            public string? PackageId { get; set; }
            public string? OrderId { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class OPaymentsConfiguration
        {
            public string? Type { get; set; }
            public double MinAmount { get; set; }
            public double MaxAmount { get; set; }
            public string? ChargeType { get; set; }
            public double ChargeValue { get; set; }
            public double MaxChargeValue { get; set; }
            public double FixedCharge { get; set; }
            public string? RewardType { get; set; }
            public double RewardValue { get; set; }
            public double MaxRewardAmount { get; set; }
            public string? PaymentReference { get; set; }
            public List<OUserCard> UserCards { get; set; }
        }
        public class OUserCard
        {
            public long ReferenceId { get; set; }
            public string? Brand { get; set; }
            public string? Bin { get; set; }
            public string? End { get; set; }
            public string? ExpMonth { get; set; }
            public string? ExpYear { get; set; }
            public string? Channel { get; set; }
        }
        internal class OAppCache
        {
            public DateTime CategoriesCacheTime { get; set; }
            public List<Category> Categories { get; set; }
            public DateTime MerchantsCacheTime { get; set; }
            public List<Merchant> Merchants { get; set; }
            public DateTime StoresCacheTime { get; set; }
            public List<Store> Stores { get; set; }
            public List<MerchantCategory> MerchantCategories { get; set; }
        }
        public class Merchant
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public string? Description { get; set; }
            public double RewardPercentage { get; set; }
            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }
        }
        public class MerchantCategory
        {
            public long? CategoryId { get; set; }
            public long? MerchantId { get; set; }
        }
        public class Category
        {
            public string? IconUrl { get; set; }

            public long ReferenceId { get; set; }
            public string? Name { get; set; }
            public string? IconName { get; set; }
        }
        public class Store
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? MerchantId { get; set; }
            public string? DisplayName { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }
        }
        public class Transaction
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public DateTime TransactionDate { get; set; }
            public DateTime ExpiaryDate { get; set; }

            public string? TypeCategoryCode { get; set; }
            public string? TypeCategoryName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? SourceCode { get; set; }
            public string? SourceName { get; set; }

            public string? ModeCode { get; set; }
            public string? ModeName { get; set; }

            public double TotalAmount { get; set; }
            public double InvoiceAmount { get; set; }

            public string? AccountNumber { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreName { get; set; }
            public string? StoreAddress { get; set; }
            public double? StoreLatitude { get; set; }
            public double? StoreLongitude { get; set; }

          
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? ReferenceNumber { get; set; }
        }
    }
    public class OAppManager
    {
        public class Donations
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public DateTime TransactionDate { get; set; }

            public long? AccountId { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public double TotalAmount { get; set; }
            public double InvoiceAmount { get; set; }

            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
        }
        public class DonationParameter
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? Title { get; set; }
                public long? Quantity { get; set; }
                public double? Amount { get; set; }
                public double? Charge { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? Title { get; set; }
                public long? Quantity { get; set; }
                public string? Amount { get; set; }
                public string? Charge { get; set; }
                public string? TotalAmount { get; set; }

            }
        }

        public class RelifDetails
        {
            public long IsFeatureEnable { get; set; }
            public string? Title { get; set; }
            public string? ShortDescription { get; set; }
            public string? Description { get; set; }
            public string? Conditions { get; set; }
            public OStorageContent IconContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class RelifDetailsResponse
        {
            public long? IsFeatureEnable { get; set; }
            public string? Title { get; set; }
            public string? ShortDescription { get; set; }
            public string? Description { get; set; }
            public string? Conditions { get; set; }
            public string? IconUrl { get; set; }
        }
        public class RelifConfig
        {
            public  RelifDetails Donation {get;set;}
            public  RelifDetails Receiver {get;set;}
            public OUserReference? UserReference { get; set; }


            //public class Donation
            //{
            //    public string? PaymentReference { get; set; }
            //    public List<DonationRange> DonationRanges { get; set; }
            //}
            //public class DonationRange
            //{
            //    public string? Title { get; set; }
            //    public long Quantity { get; set; }
            //    public double Amount { get; set; }
            //    public double Charge { get; set; }
            //    public double TotalAmount { get; set; }
            //}
        }

    }
}
