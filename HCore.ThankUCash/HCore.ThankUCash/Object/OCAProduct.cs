//==================================================================================
// FileName: OCAProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OCAProduct
    {
        public class CreditBalanceRequest
        {
            public long UserAccountId { get; set; }
            public string? MobileNumber { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
            public double Amount { get; set; }
            public string? Comment { get; set; }
            public string? Message { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? SourceCode { get; set; }
            public string? Terms { get; set; }
            public string? UsageInformation { get; set; }
            public string? ProductKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class BalanceRequest
        {
            public long UserAccountId { get; set; }
            public string? SourceCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class BalanceResponse
        {
            public double? Credit { get; set; }
            public double? Debit { get; set; }
            public double? Balance { get; set; }
        }
        public class Manage
        {
            public string? TypeCode { get; set; }
            public string? SubTypeCode { get; set; }
            public string? ParentKey { get; set; }
            public string? AccountKey { get; set; }
            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Terms { get; set; }
            public string? UsageTypeCode { get; set; }
            public string? UsageInformation { get; set; }
            public int IsPinRequired { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CodeValidityStartDate { get; set; }
            public DateTime? CodeValidityEndDate { get; set; }
            public double UnitPrice { get; set; }
            public double SellingPrice { get; set; }
            public double DiscountAmount { get; set; }
            public double DiscountPercentage { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double CommissionAmount { get; set; }
            public double TotalAmount { get; set; }
            public long MaximumUnitSale { get; set; }
            public long TotalUnitSale { get; set; }
            public long TotalUsed { get; set; }
            public double Views { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterStorage { get; set; }
            public OUserReference? UserReference { get; set; }
            public List<ProductLocation> ProductLocations { get; set; }
            public List<ProductShedule> ProductShedule { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? SubTypeCode { get; set; }
            public string? SubTypeName { get; set; }

            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public string? Tite { get; set; }

            public string? UsageTypeCode { get; set; }
            public string? UsageTypeName { get; set; }

            public double? SellingPrice { get; set; }
            public long? MaximumUnitSale { get; set; }
            public long? TotalUnitSale { get; set; }
            public long? TotalUsed { get; set; }
            public double? TotalSaleAmount { get; set; }
            public double? TotalUsedAmount { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? LastUseDate { get; set; }


            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class ProductLocation
        {
            public string? AccountKey { get; set; }
            public string? SubAccountKey { get; set; }
        }
        public class ProductShedule
        {
            public string? TypeCode { get; set; }
            public int StartHour { get; set; }
            public int EndHour { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public int DayOfTheWeek { get; set; }
        }

        public class ProductCode
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long ProductOwnerId { get; set; }
            public string? ProductOwnerKey { get; set; }

            public int TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public int? SubTypeId { get; set; }
            public string? SubTypeCode { get; set; }
            public string? SubTypeName { get; set; }

            public long ProductId { get; set; }
            public string? ProductKey { get; set; }

            public long? SubProductId { get; set; }
            public string? SubProductKey { get; set; }

            public string? AccountKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }

            public string? MerchantKey { get; set; }
            public long? MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }

            public double? ItemAmount { get; set; }
            public double? AvailableAmount { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public long? UseCount { get; set; }

            public DateTime? LastUseDate { get; set; }
            public long LastUseLocationId { get; set; }
            public string? LastUseLocaionKey { get; set; }
            public string? LastUseLocationName { get; set; }

            public long? LastUseSubLocationId { get; set; }
            public string? LastUseSubLocationName { get; set; }
            public string? LastUseSubLocationKey { get; set; }

            public long? UseAttempts { get; set; }
            public string? Comment { get; set; }
            public string? Message { get; set; }

            public DateTime CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class RewardProduct
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? SubAccountKey { get; set; }
            public string? SubAccountDisplayName { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public string? SubCategoryKey { get; set; }
            public string? SubCategoryDisplayName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? IconUrl { get; set; }
            public double? MinimumAmount { get; set; }
            public double? MaximumAmount { get; set; }
            public double? MinimumQuantity { get; set; }
            public double? MaximumQuantity { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public double? AllowMultiple { get; set; }
            public double? RewardPercentage { get; set; }
            public double? MaximumRewardAmount { get; set; }
            public  long TotalStock { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class RewardProductUpdate
        {
            public string? ReferenceKey { get; set; }
            public double RewardPercentage { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
