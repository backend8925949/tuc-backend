//==================================================================================
// FileName: OTucPay.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OTucPay
    {
       public class BuyPoint
        {
            public class Initilize
            {
               public class Request
                {
                    public double Amount { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public double Amount { get; set; }
                    public string? Status { get; set; }
                    public DateTime? InitializeDate { get; set; }
                }
            }
            public class Confirm
            {
                public class Request
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public double Amount { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public double Amount { get; set; }
                    public string? Status { get; set; }
                    public DateTime? InitializeDate { get; set; }
                    public DateTime? ConfirmDate { get; set; }
                }
            }
        }
    }
}
