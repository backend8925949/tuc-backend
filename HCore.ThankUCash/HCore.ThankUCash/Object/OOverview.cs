//==================================================================================
// FileName: OOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OOverview
    {
        public class Request
        {
            public string? UserAccountKey { get; set; }
            public string? SubUserAccountKey { get; set; }
            public string? Type { get; set; }
            public DateTime Date { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public OUserReference? UserReference { get; set; }
        }


        public class Overview
        {




            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            // Configurations
            public long Merchants { get; set; }
            public long ReferredMerchants { get; set; }
            public long ReferredMerchantVisits { get; set; }
            public double ReferredMerchantSale { get; set; }
            public long ReferredReferredStores { get; set; }
            public long Stores { get; set; }
            public long Acquirers { get; set; }
            public long Terminals { get; set; }

            public long ActiveMerchants { get; set; }
            public long ActiveMerchantsDiff { get; set; }
            public long ActiveTerminals { get; set; }
            public long ActiveTerminalsDiff { get; set; }
            public long DeadTerminals { get; set; }
            public long IdleTerminals { get; set; }
            public long UnusedTerminals { get; set; }


            public long Ptsp { get; set; }
            public long Pssp { get; set; }
            public long Cashiers { get; set; }
            public long RewardCards { get; set; }
            public long RewardCardsUsed { get; set; }
            public long ThankUCashPlus { get; set; }
            public long ThankUCashPlusForMerchant { get; set; }
            public long ThankUCashPlusBalanceValidity { get; set; }
            public double? ThankUCashPlusMinRedeemAmount { get; set; }
            public double? ThankUCashPlusMinTransferAmount { get; set; }
            public double? RewardPercentage { get; set; }
            public double? CommissionPercentage { get; set; }
            public double? Balance { get; set; }

            // TUC Plus 
            public double? ClaimedReward { get; set; }
            public double? ClaimedRewardTransations { get; set; }
            public double? Charge { get; set; }

            // Cash Rewards
            public double? CashRewardAmount { get; set; }
            public double? CashRewardPurchaseAmount { get; set; }
            public double? CashRewardPurchaseAmountDiff { get; set; }
            public long CashRewardTransactions { get; set; }

            // Card Rewards
            public double? CardRewardAmount { get; set; }
            public double? CardRewardPurchaseAmount { get; set; }
            public double? CardRewardPurchaseAmountDiff { get; set; }
            public double? CardRewardPurchaseAmountOther { get; set; }
            public long CardRewardTransactions { get; set; }
            public long CardRewardTransactionsOther { get; set; }


            // Other Rewards
            public double? OtherRewardAmount { get; set; }
            public double? OtherRewardPurchaseAmount { get; set; }
            public long OtherRewardTransactions { get; set; }



            // Reward Transactions
            public long RewardTransactions { get; set; }
            public double? RewardAmount { get; set; }
            public double? RewardUserAmount { get; set; }
            public double? RewardChargeAmount { get; set; }
            public double? RewardPurchaseAmount { get; set; }
            public DateTime? RewardLastTransaction { get; set; }

            // Redeem Transactions
            public long RedeemTransactions { get; set; }
            public double? RedeemAmount { get; set; }
            public double? RedeemPurchaseAmount { get; set; }
            public DateTime? RedeemLastTransaction { get; set; }

            public double? Credit { get; set; }
            public double? Debit { get; set; }



            // Transactions
            public long Transactions { get; set; }
            public long TransactionSuccess { get; set; }
            public long TransactionFailed { get; set; }
            public long TransactionsDiff { get; set; }
            public long TransactionsPercentage { get; set; }
            public long NewTransactions { get; set; }
            public long NewTransactionsPercentage { get; set; }
            public long RepeatingTransactions { get; set; }
            public long RepeatingTransactionsPercentage { get; set; }
            public long ReferralTransactions { get; set; }
            public long ReferralTransactionsPercentage { get; set; }

            // Purchase Amount
            public double? PurchaseAmount { get; set; }
            public double? PurchaseAmountDiff { get; set; }
            public double? PurchaseAmountPercentage { get; set; }
            public double? NewPurchaseAmount { get; set; }
            public double? NewPurchaseAmountPercentage { get; set; }
            public double? RepeatingPurchaseAmount { get; set; }
            public double? RepeatingPurchaseAmountPercentage { get; set; }
            public double? ReferralPurchaseAmount { get; set; }
            public double? ReferralPurchaseAmountPercentage { get; set; }

            public long TUCPlusRewardTransactions { get; set; }
            public double? TUCPlusBalance { get; set; }
            public double? TUCPlusReward { get; set; }
            public double? TUCPlusRewardAmount { get; set; }
            public double? TUCPlusUserRewardAmount { get; set; }
            public double? TUCPlusRewardChargeAmount { get; set; }
            public double? TUCPlusRewardPurchaseAmount { get; set; }
            public double? TUCPlusRewardClaimedAmount { get; set; }
            public double? TUCPlusRewardClaimedTransactions { get; set; }
            public double? TUCPlusPurchaseAmount { get; set; }


            // Commissions And Settlements
            public double? Commission { get; set; }
            public DateTime? LastCommissionDate { get; set; }
            public double? IssuerCommissionAmount { get; set; }
            public DateTime? LastIssuerCommissionDate { get; set; }
            public double? CommissionAmount { get; set; }
            public double? SettlementCredit { get; set; }
            public double? SettlementDebit { get; set; }

            public double? UserAmount { get; set; }
            public double? ThankUAmount { get; set; }


            // App Users
            public long AppUsers { get; set; }
            public long ReferredAppUsers { get; set; }
            public long ReferredAppUsersVisit { get; set; }
            public double? ReferredAppUsersPurchase { get; set; }

            public long AppUsersPercentage { get; set; }
            public long OwnAppUsers { get; set; }
            public long OwnAppUsersPercentage { get; set; }
            public long UniqueAppUsers { get; set; }
            public long RepeatingAppUsers { get; set; }
            public long RepeatingAppUsersPercentage { get; set; }
            public long ReferralAppUsers { get; set; }
            public long ReferralAppUsersPercentage { get; set; }
            public long AppUsersMale { get; set; }
            public long AppUsersFemale { get; set; }
            public long AppUsersOther { get; set; }
            public DateTime LastAppUserDate { get; set; }
            public MiniTransaction LastTransaction { get; set; }
            public DateTime? LastTransactionDate { get; set; }


            public double? TransactionIssuerAmountCredit { get; set; }
            public double? TransactionIssuerAmountDebit { get; set; }

            public double? TransactionIssuerChargeCredit { get; set; }
            public double? TransactionIssuerChargeDebit { get; set; }


            public double? TransactionIssuerTotalCreditAmount { get; set; }
            public double? TransactionIssuerTotalDebitAmount { get; set; }

            public List<AppUserByCard> AppUsersCardType { get; set; }
            public List<AppUserByCard> AppUsersBank { get; set; }
            public List<AppUserAgeGroup> AppUsersByAgeGroup { get; set; }
            public List<PosOverview> PosOverview { get; set; }
            public List<MerchantOverview> MerchantOverview { get; set; }
            public List<StoresOverview> StoresOverview { get; set; }
            public List<TerminalsOverview> TerminalsOverview { get; set; }
            public List<AcquirerCollection> AcquirerAmountDistribution { get; set; }
            public List<UserAccountOverview> FrequentBuyers { get; set; }
        }
        public class AppUserByCard
        {
            internal long ReferenceId { get; set; }
            public string? Name { get; set; }
            public double? Purchase { get; set; }
            public double? Transactions { get; set; }
            public double? Users { get; set; }
        }
        public class AppUserAgeGroup
        {
            public string? Name { get; set; }
            public double? Purchase { get; set; }
            public double? Visits { get; set; }
            public double? Users { get; set; }
        }
        public class UserAccountOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public long Transactions { get; set; }
            public double? RewardAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? RewardClaimAmount { get; set; }
            public double? PurchaseAmount { get; set; }
            public DateTime LastTransactionDate { get; set; }
        }
        public class PosOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public long Terminals { get; set; }
            public long ActiveTerminals { get; set; }
            public long IdleTerminals { get; set; }
            public long DeadTerminals { get; set; }
            public long UnusedTerminals { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? StoreDisplayName { get; set; }
            public DateTime? LastTransactionDate { get; set; }
            public string? LastTransactionLocation { get; set; }

        }
        public class TerminalsOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? PtspName { get; set; }
            public string? AcquirerName { get; set; }
            public DateTime LastTransactionDate { get; set; }
        }
        public class MerchantOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public double PurchaseAmount { get; set; }
            public long Transactions { get; set; }
            public long Terminals { get; set; }
            public DateTime? LastTransactionDate { get; set; }
        }
        public class StoresOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public double PurchaseAmount { get; set; }
            public long Transactions { get; set; }
            public long Terminals { get; set; }
            public DateTime? LastTransactionDate { get; set; }
        }

        public class AcquirerCollection
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public double PurchaseAmount { get; set; }
            public long Terminals { get; set; }
        }

        public class MiniTransaction
        {
            public long ReferenceId { get; set; }
            public string? MerchantName { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreAddress { get; set; }
            public double? RewardAmount { get; set; }
            public double? InvoiceAmount { get; set; }

            public string? TypeName { get; set; }
            public DateTime TransactionDate { get; set; }
        }

        public class UserInfo
        {
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }

            public long Transactions { get; set; }
            public double? PurchaseAmount { get; set; }

            public long RewardTransactions { get; set; }
            public double? RewardAmount { get; set; }
            public double? RewardPurchaseAmount { get; set; }

            public long RedeemTransactions { get; set; }
            public double? RedeemAmount { get; set; }
            public double? RedeemPurchaseAmount { get; set; }

            public long Customers { get; set; }
            public long AppUsers { get; set; }

        }


        public class Response
        {
            public long? Transactions { get; set; }
            public double? PurchaseAmount { get; set; }
            public double? Amount { get; set; }
        }


        public class StoreInfo
        {
            internal long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public double? RewardAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public long Transactions { get; set; }
            public double? PurchaseAmount { get; set; }
        }
    }

    public class OStoreBankCollection
    {
        public class Request
        {
            public double ReceivedAmount { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? StartDate { get; set; }
            internal DateTime EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public DateTime Date {get;set;}
            public List<OStore> Stores { get; set; }

             public long? Terminals { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? NibsAmount { get; set; }
            public double? ExpectedAmount { get; set; }
            public double? ReceivedAmount { get; set; }
            public double? AmountDifference { get; set; }
        }
        public class OStore
        {
            internal long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? StoreName { get; set; }
            public long? Acquirers { get; set; }
            public long? Terminals { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? NibsAmount { get; set; }
            public double? ExpectedAmount { get; set; }
            public double? ReceivedAmount { get; set; }
            public double? AmountDifference { get; set; }
            public DateTime CreateDate { get; set; }
            public List<OAcquirerCollection> AmountDistribution { get; set; }
        }

        public class OAcquirerCollection
        {
            public string? ReferenceKey {get;set;}
            public string? DisplayName {get;set;}
            public string? IconUrl {get;set;}
            public long? Terminals {get;set;}
            public double? InvoiceAmount {get;set;}
            public double? NibsAmount {get;set;}
            public double? ExpectedAmount {get;set;}
            public double? ReceivedAmount {get;set;}
            public double? AmountDifference {get;set;}
        }
    }


}
