//==================================================================================
// FileName: OLead.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OLead
    {
        public class ListOverview
        {
            public DateTime Date { get; set; }
            public long Total { get; set; }
            public long CallFailed { get; set; }
            public long CallLater { get; set; }
            public long NotInterested { get; set; }
            public long InvalidNumber { get; set; }
            public long Closed { get; set; }
        }

        public class OverviewResponse
        {

            public long Total { get; set; }
            public long TotalNew { get; set; }
            public long TotalCalling { get; set; }
            public long TotalCallFailed { get; set; }
            public long TotalCallLater { get; set; }
            public long TotalNotInterested { get; set; }
            public long TotalInvalidNumber { get; set; }
            public long TotalClosed { get; set; }


            public long TodayTotal { get; set; }

            public long TodayNew { get; set; }
            public long TodayCalling { get; set; }
            public long TodayCallFailed { get; set; }
            public long TodayCallLater { get; set; }
            public long TodayNotInterested { get; set; }
            public long TodayInvalidNumber { get; set; }
            public long TodayClosed { get; set; }


            public long New { get; set; }
            public long Processing { get; set; }
            public long Invalid { get; set; }
            public long Closed { get; set; }
            public long BackLog { get; set; }


        }

        public class Request
        {
            public int Count { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Comment { get; set; }
            public string? Description { get; set; }
            public string? OwnerKey { get; set; }
            public string? AgentKey { get; set; }
            public string? AccountKey { get; set; }
            public DateTime? EndTime { get; set; }
            public OUserReference? UserReference { get; set; }
            public string? StatusCode { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public string? AgentKey { get; set; }
            public string? AgentDisplayName { get; set; }

            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }


            public string? AccountOwnerKey { get; set; }
            public string? AccountOwnerDisplayName { get; set; }

            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? LeadStatusName { get; set; }
        }
    }
    public class OLeadGroup
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? AgentKey { get; set; }
            public string? MerchantKey { get; set; }
            public OUserReference? UserReference { get; set; }
            public string? StatusCode { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? AgentKey { get; set; }
            public string? AgentDisplayName { get; set; }

            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OLeadActivity
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? Comment { get; set; }
            public string? Description { get; set; }
            public string? OwnerKey { get; set; }
            public string? AgentKey { get; set; }
            public string? AccountKey { get; set; }
            public OUserReference? UserReference { get; set; }
            public string? StatusCode { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? LeadKey { get; set; }


            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Comment { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
