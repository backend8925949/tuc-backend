//==================================================================================
// FileName: OGiftPoints.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OGiftPoints
    {
         public class CreditBalanceRequest
        {
            public long UserAccountId { get;set;}
            public string? Name { get;set;}
            public string? EmailAddress { get;set;}
            public string? MobileNumber { get;set; }
            public double Amount { get;set;}
            public string? Comment { get;set;}
            public string? ReferenceNumber { get;set;}
            public OUserReference? UserReference { get;set;}
        }
        public class BalanceRequest
        {
            public long UserAccountId { get;set;}
            public OUserReference? UserReference { get;set;}
        }
        public class BalanceResponse
        {
            public double? Credit { get;set;}
            public double? Debit { get;set;}
            public double? Balance { get;set;}
        }
    }
}
