//==================================================================================
// FileName: ORmManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class ORmManager
    {
       
        public class Request
        {
            public string? ReferenceKey { get;set;}
            public string? StoreKey { get;set;}
            public OUserReference? UserReference { get; set; }
        }

        public class Response
        {

        }
    }
}
