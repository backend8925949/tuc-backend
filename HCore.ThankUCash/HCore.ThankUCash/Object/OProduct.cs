//==================================================================================
// FileName: OProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OProduct
    {
        internal class ProductCodeProcess
        {
            public int TypeId { get; set; }
            public long BatchId { get; set; }
            public long Quantity { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? ParentKey { get; set; }
            //public string? TypeCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? UserAccountKey { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public long? Quantity { get; set; }
            public string? RewardTypeCode { get; set; }
            public double? RewardValue { get; set; }
            public double? UnitPrice { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent IconContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            internal long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            internal long ParentId { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public long? Quantity { get; set; }
            public string? RewardTypeCode { get; set; }
            public string? RewardTypeName { get; set; }
            public double? RewardValue { get; set; }
            public double? UnitPrice { get; set; }
            public string? IconUrl { get; set; }
            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }


            public long? Batches { get; set; }
            public long? UsedItemCodes { get; set; }
            public long? ItemCodes { get; set; }
        }
    }

    public class OProductCategory
    {
        public class Manage
        {

            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? AccountTypeCode { get; set; }
            public int Sequence { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }

            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public long? Items { get; set; }
            public long? Sequence { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

        }
    }

    public class OProductCode
    {

        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? ItemCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            internal long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }

            internal long BatchId { get; set; }
            public string? BatchKey { get; set; }
            public string? BatchName { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            internal long? ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }


            public string? ItemCode { get; set; }
            public string? TransactionKey { get; set; }

            public double? UnitPrice { get; set; }
            public long? RewardTypeId { get; set; }
            public double? RewardValue { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }

            public DateTime? UseDate { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }


            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }


            internal long ProductStatusId { get; set; }
            internal long BatchStatusId { get; set; }

        }
    }
}
