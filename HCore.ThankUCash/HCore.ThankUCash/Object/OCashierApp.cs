//==================================================================================
// FileName: OCashierApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OCashierApp
    {
        public class Request
        {
            public string? MobileNumber { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class AccessCheck
        {    

            public string? MerchantName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? StoreName { get; set; }
            public string? StoreAddress { get; set; }
        }
    }
}
