//==================================================================================
// FileName: OCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.ThankUCash.Object
{
    public class OCampaign
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? SubTypeCode { get; set; }
            public string? UserAccountKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }

            public double? SubTypeValue { get; set; }


            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public double? MinimumInvoiceAmount { get; set; }
            public double? MaximumInvoiceAmount { get; set; }

            public double? MinimumRewardAmount { get; set; }
            public double? MaximumRewardAmount { get; set; }

            public string? ManagerKey { get; set; }
            public string? SmsText { get; set; }

            public string? Comment { get; set; }

            public string? StatusCode { get; set; }

            public List<Merchant> Merchants { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class Merchant
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }

        public class Response
        {
            public class Details
            {
                public long ReferenceId { get;set;}
                public string? ReferenceKey { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public string? UserAccountKey { get; set; }
                public string? UserAccountDisplayName { get; set; }

                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? Comment { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public double? SubTypeValue { get; set; }

                public double? MinimumInvoiceAmount { get; set; }
                public double? MaximumInvoiceAmount { get; set; }

                public double? MinimumRewardAmount { get; set; }
                public double? MaximumRewardAmount { get; set; }

                public string? ManagerKey { get; set; }
                public string? ManagerDisplayName { get; set; }
                public string? SmsText { get; set; }


                public string? SubTypeCode { get; set; }
                public string? SubTypeName { get; set; }

                public DateTime CreateDate { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class List
            {
                public long ReferenceId { get;set;}
                public string? ReferenceKey { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public string? UserAccountKey { get; set; }
                public string? UserAccountDisplayName { get; set; }


                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? Comment { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public double? SubTypeValue { get; set; }

                public double? MinimumInvoiceAmount { get; set; }
                public double? MaximumInvoiceAmount { get; set; }

                public double? MinimumRewardAmount { get; set; }
                public double? MaximumRewardAmount { get; set; }

                public string? ManagerKey { get; set; }
                public string? ManagerDisplayName { get; set; }
                public string? SmsText { get; set; }


                public string? SubTypeCode { get; set; }
                public string? SubTypeName { get; set; }

                public DateTime CreateDate { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
    }
}
