//==================================================================================
// FileName: OPtspManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
namespace HCore.ThankUCash.Object
{
   public class OPtspManager
    {
        public class Analytics
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long SubAccountId { get; set; }
                public string? SubAccountKey { get; set; }
                public string? Type { get; set; }
                public DateTime Date { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Counts
            {
                public long TotalMerchants { get; set; }
                public long ActiveMerchants { get; set; }
                public long TotalStores { get; set; }
                public long ActiveStores { get; set; }
                public long TotalTerminals { get; set; }
                public long ActiveTerminals { get; set; }
                public long ActivePosUpgradeRequest { get; set; }

                public long TotalTransactions { get; set; }
                public double TotalSale { get; set; }

                public double AverageTransactions { get; set; }

                public double AverageTransactionAmount { get; set; }

                public long CardTransactions { get; set; }
                public double CardTransactionsAmount { get; set; }
                public long CashTransactions { get; set; }
                public double CashTransactionAmount { get; set; }
            }

            public class Sales
            {
              

                public long TotalTransactions { get; set; }
                public long TotalSale { get; set; }

                public long AverageTransactions { get; set; }

                public double AverageTransactionAmount { get; set; }
            }

            public class TerminalStatus
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Idle { get; set; }
                public long Dead { get; set; }

                public long Inactive { get; set; }
            }
        }
        public class Merchant
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

               

                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }

                public string? CityName { get; set; }

                public string? IconUrl { get; set; }

                public long? Stores { get; set; }
                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }
                public string? RmDisplayName { get; set; }
                public DateTime CreateDate { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }
        public class Store
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }

                public string? IconUrl { get; set; }

                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }
                public string? RmDisplayName { get; set; }
                public DateTime CreateDate { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }
        public class Terminal
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TerminalId { get; set; }

                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }

                public long StoreId { get; set; }
                public string? StoreKey { get; set; }
                public string? StoreName { get; set; }

                public string? StoreAddress { get; set; }
                public string? StoreContactNumber { get; set; }

                public long AcquirerId { get; set; }
                public string? AcquirerKey { get; set; }
                public string? AcquirerName { get; set; }

                public double? StoreLatitude { get; set; }
                public double? StoreLongitude { get; set; }


                public DateTime? LastTransactionDate { get; set; }
                public DateTime CreateDate { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }
    }
}
