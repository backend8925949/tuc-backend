//==================================================================================
// FileName: OLiveMerchants.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.ThankUCash.Object
{
    public class OLiveMerchants
    {
         public class ActivityUpdate
        {
            public long TerminalId { get;set;}
            public OUserReference? UserReference { get; set; }
        }
        public class Request
        {

        }
        public class Response
        {
            public List<Merchant> Merchants { get;set;}
        }
        public class Merchant
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get;set;}

             public long? TotalTransaction { get; set; }
            public long? TotalInvoiceAmount { get; set; }

            public double? TodayTotalTransaction { get; set; }
            public double? TodayTotalInvoiceAmount { get; set; }
            public DateTime? LastTransactionDate { get; set; }
            public double? LastTransactionInvoiceAmount { get; set; }

            public long TotalTerminals { get; set; }
            public long ActiveTerminals { get; set; }
            public long IdleTerminals { get; set; }
            public long DeadTerminals { get; set; }
        }
    }
}
