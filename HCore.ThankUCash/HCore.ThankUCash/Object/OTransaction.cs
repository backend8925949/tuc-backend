//==================================================================================
// FileName: OTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace HCore.ThankUCash.Object
{
    // - FINALIZED -  DONOT TOUCH - HG - 12-08-2019

    public class OTransactionOverview
    {
        public long? FailedTransaction { get; set; }
        public long? Customers { get; set; }
        public long? Transactions { get; set; }
        public double? InvoiceAmount { get; set; }
        public double? CardInvoiceAmount { get; set; }
        public double? CashInvoiceAmount { get; set; }
        public double? RewardAmount { get; set; }
        public double? RewardClaimAmount { get; set; }
        public double? RedeemAmount { get; set; }
        public double? UserAmount { get; set; }
        public double? CommissionAmount { get; set; }
        public double? Cashouts { get; set; }
        public double? Points { get; set; }
        public double? CashierRewardAmount { get; set; }
    }
    public class OTransaction
    {
        public class All
        {
            #region Parameters
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }

            public long? ModeId { get; set; }
            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public long? SourceId { get; set; }
            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public long? UserAccountId { get; set; }
            public long? CardBankId { get; set; }
            public long? CardBrandId { get; set; }
            public long? AcquirerId { get; set; }
            public long? ProviderId { get; set; }
            public long? ParentId { get; set; }
            public long? CreatedById { get; set; }

            public long? TypeId { get; set; }
            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }

            public double Amount { get; set; }
            public double Balance { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public double RedeemAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double ConversionAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByMobileNumber { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }

            public double? ParentTransactionAmount { get; set; }
            public double? ParentTransactionChargeAmount { get; set; }
            public double? ParentTransactionCommissionAmount { get; set; }
            public double? ParentTransactionTotalAmount { get; set; }

            public long? OwnerId { get; set; }
            public long? InvoiceId { get; set; }
            public long? InvoiceItemId { get; set; }
            public long Status { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? GroupKey { get; set; }

            public string? TUCCardKey { get; set; }
            public string? TUCCardNumber { get; set; }
            public string? TCode { get; set; }

            public string? CardBrandName { get; set; }
            public string? CardSubBrandName { get; set; }
            public string? CardTypeName { get; set; }
            public string? CardBankName { get; set; }
            public string? CardBinNumber { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? AcquirerAmount { get; set; }
            public double? PTSAAmount { get; set; }
            public double? PSSPAmount { get; set; }
            public double? ProviderAmount { get; set; }
            public double? IssuerAmount { get; set; }
            public double? ThankUAmount { get; set; }
            public double? MerchantAmount { get; set; }
            public string? AcquirerName { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerKey { get; set; }

            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }

            public string? AccountNumber { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
            public DateTime ExpiryDate { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ParentMobileNumber { get; set; }
            #endregion

        }
        public class Sale
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? TypeId { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public long? SourceId { get; set; }
            public string? SourceName { get; set; }
            public string? SourceCode { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? ClaimAmount { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDisplayName { get; set; }
            public string? UserMobileNumber { get; set; }
            public long? UserStatusId { get; set; }
            public string? UserStatusCode { get; set; }
            public string? UserStatusName { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }
            public long? ParentStatusId { get; set; }
            public string? ParentStatusCode { get; set; }
            public string? ParentStatusName { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }
            public long? SubParentStatusId { get; set; }
            public string? SubParentStatusCode { get; set; }
            public string? SubParentStatusName { get; set; }

            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }


            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? CashierMobileNumber { get; set; }
            public long? CashierStatusId { get; set; }
            public string? CashierStatusCode { get; set; }
            public string? CashierStatusName { get; set; }

            public double? Balance { get; set; }

            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }
            public long? AcquirerStatusId { get; set; }
            public string? AcquirerStatusCode { get; set; }
            public string? AcquirerStatusName { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public long? ProviderStatusId { get; set; }
            public string? ProviderStatusCode { get; set; }
            public string? ProviderStatusName { get; set; }

            public string? AccountNumber { get; set; }

            public long? CardBrandId { get; set; }
            public string? CardBrandName { get; set; }
            public string? CardBrandDisplayName { get; set; }

            public long? CardBankId { get; set; }
            public string? CardBankName { get; set; }
            public string? CardBankDisplayName { get; set; }

            public long? CardSubBrandId { get; set; }
            public string? CardSubBrandName { get; set; }
            public string? CardSubBrandDisplayName { get; set; }

            public long? CardTypeId { get; set; }
            public string? CardTypeCode { get; set; }
            public string? CardTypeName { get; set; }

            public string? ReferenceNumber { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? TerminalReferenceId { get; set; }
            public string? PosName { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
            public long? TerminalStatusId { get; set; }
            public string? TerminalStatusCode { get; set; }
            public string? TerminalStatusName { get; set; }
            public long? ProgramId { get; set; }
            public string? ProgramReferenceKey { get; set; }
            public double? RewardUserAmount { get; set; }
            public double? RewardCommissionAmount { get; set; }
            public long? ModeId { get; set; }
            public double? Amount { get; set; }
            public double? CashierReward { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? TypeId { get; set; }
            public string? TypeName { get; set; }
            internal int? TypeCategoryId { get; set; }
            public double? InvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? ClaimAmount { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDisplayName { get; set; }
            public string? UserMobileNumber { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentMobileNumber { get; set; }
            public string? ParentEmailAddress { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }

            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }


            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? CashierMobileNumber { get; set; }


            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }


            public string? AccountNumber { get; set; }
            public long? CardBrandId { get; set; }
            public string? CardBrandName { get; set; }
            public long? CardBankId { get; set; }
            public string? CardBankName { get; set; }
            public string? CardNumber { get; set; }

            public string? ReferenceNumber { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }

            public string? GroupKey { get; set; }

            public bool IsSmsSent { get; set; }
            public SmsNotification SmsNotification { get; set; }
        }

        public class SmsNotification
        {
            public string? MobileNumber { get; set; }
            //public string? Message { get; set; }
            public string? Reference { get; set; }
            public DateTime? SendDate { get; set; }
            public DateTime? DeliveryDate { get; set; }
            public string? StatusCode { get; set; }
            public string? SenderId { get; set; }
        }

        public class SaleDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public string? SourceName { get; set; }
            public double? InvoiceAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public double? CashierReward { get; set; }
        }
        public class RewardDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public string? SourceName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? ConvinenceCharge { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public double? Balance { get; set; }
        }
        public class RedeemDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public double? Balance { get; set; }
        }
        public class RewardClaimDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? ClaimAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
        }
        public class Overview
        {
            public long Total { get; set; }
            public double Amount { get; set; }
        }
    }
}
