//==================================================================================
// FileName: OUserInvoice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.ThankUCash.Object
{
    public class OInvoice
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Comment { get;set;}

               public DateTime? PaymentDate { get;set;}
            public string? PaymentReference { get;set;}

            public string? PaymentModeCode { get;set;}

            public string? PaymentApproverKey { get;set;}

            public OStorageContent PaymentProofContent { get;set;}
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get;set;}
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }

            public long? UserAccountId { get;set;}
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }
            

            public string? InvoiceNumber { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? FromName { get; set; }
            public string? FromAddress { get; set; }
            public string? FromContactNumber { get; set; }
            public string? FromEmailAddress { get; set; }
            public string? FromFax { get; set; }
            public string? ToName { get; set; }
            public string? ToAddress { get; set; }
            public string? ToContactNumber { get; set; }
            public string? ToEmailAddress { get; set; }
            public string? ToFax { get; set; }


            public long? TotalItem { get;set;}
            public double? DiscountAmount { get;set;}
            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }


            public DateTime? PaymentDate { get;set;}
            public string? PaymentReference { get;set;}

            public string? PaymentModeCode { get;set;}
            public string? PaymentModeName { get;set;}

            public string? PaymentApproverKey { get;set;}
            public string? PaymentApproverDisplayName { get;set;}

            public string? PaymentProofUrl  { get;set;}

            public DateTime? InvoiceDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? Comment { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public List<Details> Items { get;set;}
        }
         public class List
        {
            public long ReferenceId { get;set;}
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }
            public string? Name { get; set; }
            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }

            public long? TotalItem { get;set;}
            
            public DateTime? InvoiceDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public DateTime? ModifyDate { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
         }

}
