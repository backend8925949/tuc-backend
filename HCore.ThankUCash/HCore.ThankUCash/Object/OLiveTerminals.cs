//==================================================================================
// FileName: OLiveTerminals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.ThankUCash.Object
{
    public class OLiveTerminal
    {
         public class ActivityUpdate
        {
            public long TerminalId { get;set;}
            public OUserReference? UserReference { get; set; }
        }
        public class Request
        {

        }
        public class Response
        {

            public List<Terminal> Terminals { get;set;}
        }
        public class Terminal
        {


            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }

            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }

            public long? RMId { get;set;}
            public string? RMKey { get;set;}
            public string? RMDisplayName { get;set;}

            public long? TotalTransactions { get;set;}
            public DateTime?  LastTransactionDate { get;set;}
        }
    }
}
