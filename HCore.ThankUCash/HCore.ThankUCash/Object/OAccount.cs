//==================================================================================
// FileName: OAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.Object
{
    // - FINALIZED -  DONOT TOUCH - HG - 12-08-2019
    public class OAccount
    {
        public class Account
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            
            public string? Displayname { get; set; }
            public string? UserName { get; set; }
            public string? DisplayName { get; set; }
            public string? TerminalId { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public long? GenderId { get; set; }
            public string? GenderName { get; set; }
            public string? EmployeeId { get; set; }
            public DateTime? DateOfBirth { get; set; }

            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public string? IconUrl { get; set; }

            public long? OwnerId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public string? OwnerIconUrl { get; set; }

            public long? SubOwnerId { get; set; }
            public string? SubOwnerKey { get; set; }
            public string? SubOwnerDisplayName { get; set; }

            public long? TotalTransaction { get; set; }
            public double? TotalInvoiceAmount { get; set; }
            public double? TotalBalance { get; set; }


            public long? ApplicationStatusId { get; set; }
            public string? ApplicationStatusCode { get; set; }
            public string? ApplicationStatusName { get; set; }
            public long? AccountLevelId { get; set; }
            public string? AccountLevelCode { get; set; }
            public string? AccountLevelName { get; set; }

            public long? TotalGiftPointTransaction { get; set; }
            public double? TotalGiftPointCreditAmount { get; set; }
            public double? TotalGiftPointDebitAmount { get; set; }
            public double? TotalGiftPointBalance { get; set; }

            public long? TotalRewardTransaction { get; set; }
            public double? TotalRewardAmount { get; set; }
            public double? TotalRewardInvoiceAmount { get; set; }

            public long? TucRewardTransaction { get; set; }
            public double? TucRewardAmount { get; set; }
            public double? TucRewardInvoiceAmount { get; set; }
            public double? TucBalance { get; set; }

            public long? TucPlusRewardTransaction { get; set; }
            public double? TucPlusRewardAmount { get; set; }
            public double? TucPlusConvinenceCharge { get; set; }
            public double? TucPlusRewardInvoiceAmount { get; set; }
            public double? TucPlusBalance { get; set; }

            public long? RedeemTransaction { get; set; }
            public double? RedeemAmount { get; set; }
            public double? RedeemInvoiceAmount { get; set; }

            public long? TucPlusRewardClaimTransaction { get; set; }
            public double? TucPlusRewardClaimAmount { get; set; }
            public double? TucPlusRewardClaimInvoiceAmount { get; set; }

            public DateTime? LastTransactionDate { get; set; }
            public DateTime? LastLoginDate { get; set; }
            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? Merchants { get; set; }
            public long? Stores { get; set; }
            public long? Terminals { get; set; }
            public long? Cashiers { get; set; }
            public double? RewardPercentage { get; set; }

            public long? AccountOperationTypeId { get; set; }
            public string? AccountOperationTypeCode { get; set; }
            public string? AccountOperationTypeName { get; set; }


            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderMobileNumber { get; set; }
            public string? ProviderIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreName { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreMobileNumber { get; set; }
            public string? StoreIconUrl { get; set; }

            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerName { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerMobileNumber { get; set; }
            public string? AcquirerIconUrl { get; set; }


            public long? Transactions { get; set; }
            public double TransactionAmount { get; set; }
            public long? ActivityStatusId { get; set; }
            public string? ActivityStatusCode { get; set; }
            public string? ActivityStatusName { get; set; }

            public long? RmId { get; set; }
            public string? RmKey { get; set; }

            public long? ActvityStatusId { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
        //public class AccountDownload
        //{
        //    public string? DisplayName { get; set; }
        //    public string? Name { get; set; }
        //    public string? EmailAddress { get; set; }
        //    public string? MobileNumber { get; set; }
        //    public string? GenderName { get; set; }
        //    //public DateTime? DateOfBirth { get; set; }
        //    public string? Address { get; set; }

        //    public long? TotalTransaction { get; set; }
        //    public double? TotalInvoiceAmount { get; set; }
        //    public double? TotalBalance { get; set; }

        //    public long? TotalGiftPointTransaction { get; set; }
        //    public double? TotalGiftPointCreditAmount { get; set; }
        //    public double? TotalGiftPointDebitAmount { get; set; }
        //    public double? TotalGiftPointBalance { get; set; }

        //    //public long? TotalRewardTransaction { get; set; }
        //    //public double? TotalRewardAmount { get; set; }
        //    //public double? TotalRewardInvoiceAmount { get; set; }

        //    public long? TucRewardTransaction { get; set; }
        //    public double? TucRewardAmount { get; set; }
        //    public double? TucRewardInvoiceAmount { get; set; }

        //    public long? TucPlusRewardTransaction { get; set; }
        //    public double? TucPlusRewardAmount { get; set; }
        //    public double? TucPlusConvinenceCharge { get; set; }
        //    public double? TucPlusRewardInvoiceAmount { get; set; }
        //    public double? TucPlusBalance { get; set; }


        //    public long? RedeemTransaction { get; set; }
        //    public double? RedeemAmount { get; set; }
        //    public double? RedeemInvoiceAmount { get; set; }

        //    public long? TucPlusRewardClaimTransaction { get; set; }
        //    public double? TucPlusRewardClaimAmount { get; set; }
        //    public double? TucPlusRewardClaimInvoiceAmount { get; set; }

        //    public DateTime? LastTransactionDate { get; set; }
        //    public DateTime? RegistrationDate { get; set; }

        //    public string? StatusName { get; set; }

        //    public long? Transactions { get; set; }
        //}
    }
}

