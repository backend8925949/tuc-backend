//==================================================================================
// FileName: OCustomerApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using GeoCoordinatePortable;
using System; 
namespace HCore.ThankUCash.Object
{
    public class OCustomerApp
    {
        public class Transaction
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
                
            public DateTime TransactionDate { get; set; }

            public string? TypeCategoryCode { get; set; }
            public string? TypeCategoryName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public double TotalAmount { get; set; }
            public double InvoiceAmount { get; set; }

            public string? AccountNumber { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreName { get; set; }
            public string? StoreAddress { get; set; }
            public double? StoreLatitude { get; set; }
            public double? StoreLongitude { get; set; }


            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public string? ReferenceNumber { get; set; }
        }
        public class Stores
        {
            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantIconUrl { get; set; }

            public string? PromoContent { get; set; }

            public double? Rating { get; set; }
            public long? RatingCount { get; set; }

            public long StoreId { get; set; }
            public string? StoreKey { get; set; }

                public string? Description { get; set; }

            public string? StoreName { get; set; }
            public string? StoreAddress { get; set; }
            public double StoreLatitude { get; set; }
            public double StoreLongitude { get; set; }

            public string? RewardPercentage { get; set; }

            //public long? Comments { get; set; }
            public double? AverageRatings { get; set; }
            public double? Distance { get; set; }
            internal GeoCoordinate Location { get; set; }
            public double AverageVisits { get; set; }
        }
    }
}
