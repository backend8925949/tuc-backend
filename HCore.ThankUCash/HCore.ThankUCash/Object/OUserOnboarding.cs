//==================================================================================
// FileName: OUserOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using HCore.Object;

namespace HCore.ThankUCash.Object
{
    public class OUserOnboarding
    {
        public class Merchant
        {
            public class Request
            {
                public string? ReferenceKey { get; set; }
                // Organization Details
                public string? OwnerKey { get; set; }
                public string? DisplayName { get; set; }
                public string? CompanyName { get; set; }
                public string? CompanyContactNumber { get; set; }
                public string? CompanyEmailAddress { get; set; }
                public string? CompanyAddress { get; set; }
                public string? CompanyStreetAddress { get; set; }
                public string? CompanyCountryName { get; set; }
                public string? CompanyRegionKey { get; set; }
                public string? CompanyRegionAreaKey { get; set; }
                public string? CompanyCityKey { get; set; }
                public string? CompanyCityAreaKey { get; set; }
                public string? CompanyPostalCode { get; set; }
                public double CompanyLatitude { get; set; }
                public double CompanyLongitude { get; set; }

                public string? UserName { get; set; }
                public string? Password { get; set; }

                public string? WebsiteUrl { get; set; }
                public string? FacebookUrl { get; set; }
                public string? TwitterUrl { get; set; }
                public string? InstagramUrl { get; set; }

                public string? ContactFirstName { get; set; }
                public string? ContactLastName { get; set; }
                public string? ContactMobileNumber { get; set; }

                public string? Description { get; set; }
                public OStorage.Save IconContent { get; set; }
                public OStorage.Save PosterContent { get; set; }


                public string? AccountOperationTypeCode { get; set; }
                //public double RewardPercentage { get; set; }
                //public string? RewardDeductionTypeCode { get; set; }
                //public double MaximumRewardDeductionAmount { get; set; }
                //public string? SettlementShedule { get; set; }
                //public double MinimumSettlementAmount { get; set; }
                //public int EnableThankUCashPlus { get; set; }
                //public string? ThankUCashPlusCriteria { get; set; }
                //public string? ThankUCashPlusCriteriaValue { get; set; }
                //public double ThankUCashPlusMinimumTransferAmount { get; set; }



                //public string[] Categories { get; set; }
                //public List<Bank> Banks { get; set; }
                //public List<Store> Stores { get; set; }

                // Contact Details

                public OUserReference? UserReference { get; set; }


            }
            public class Store
            {
                public string? DisplayName { get; set; }
                public string? StoreContactNumber { get; set; }
                public string? StoreContactEmailAddress { get; set; }


                public string? ContactFistName { get; set; }
                public string? ContactLastName { get; set; }
                public string? ContactMobileNumber { get; set; }
                public string? ContactEmailAddress { get; set; }

                public string? Description { get; set; }
                public string? Address { get; set; }
                public string? StreetAddress { get; set; }
                public string? CountryName { get; set; }
                public string? RegionName { get; set; }
                public string? SubRegionName { get; set; }
                public string? CityName { get; set; }
                public string? CityAreaName { get; set; }
                public string? PostalCode { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string[] Categories { get; set; }



                public double RewardPercentage { get; set; }
                public string? RewardDeductionTypeCode { get; set; }
                public double MaximumRewardDeductionAmount { get; set; }
                public string? SettlementShedule { get; set; }
                public double MinimumSettlementAmount { get; set; }
                public int EnableThankUCashPlus { get; set; }
                public string? ThankUCashPlusCriteria { get; set; }
                public string? ThankUCashPlusCriteriaValue { get; set; }
                public double ThankUCashPlusMinimumTransferAmount { get; set; }

                public List<Terminal> Terminals { get; set; }

            }
            public class Terminal
            {
                public string? TerminalId { get; set; }
                public string? AcquirerKey { get; set; }
                public string? ProviderKey { get; set; }
            }
            public class Bank
            {
                public string? BankName { get; set; }
                public string? BankAccountName { get; set; }
                public string? AccountNumber { get; set; }
                public string? SortCode { get; set; }
            }
        }
    }
}
