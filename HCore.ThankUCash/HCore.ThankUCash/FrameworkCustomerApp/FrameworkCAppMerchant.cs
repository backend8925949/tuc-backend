//==================================================================================
// FileName: FrameworkCAppMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to customer app
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant; 
using HCore.ThankUCash.Object;
using GeoCoordinatePortable;

namespace HCore.ThankUCash.FrameworkCustomerApp
{
    public class FrameworkCAppMerchant
    {
        internal static List<OCustomerApp.Stores> _CacheStores_Default = new List<OCustomerApp.Stores>();
        internal static List<OCustomerApp.Stores> _CacheStores_Categories = new List<OCustomerApp.Stores>();
        HCoreContext _HCoreContext;
        /// <summary>
        /// Gets the nearby store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNearbyStore(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                //if (_CacheStores_Default.Count == 0)
                //{
                //    _CacheStores_Default =
                //           _HCoreContext.HCUAccount.Where(x =>
                //           x.StatusId == HelperStatus.Default.Active
                //                                            && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                //                                            && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                //                                            && x.Latitude != 0
                //                                               && x.Longitude != 0
                //           )
                //           .Select(x => new OCustomerApp.Stores
                //           {
                //               MerchantId = x.Owner.Id,
                //               MerchantKey = x.Owner.Guid,
                //               MerchantName = x.Owner.DisplayName,
                //               MerchantIconUrl = x.Owner.IconStorage.Path,
                //               Rating = x.Owner.AverageValue,
                //               RatingCount = x.Owner.CountValue,
                //               PromoContent = "Best offers",

                //               StoreId = x.Id,
                //               StoreKey = x.Guid,
                //               StoreName = x.DisplayName,
                //               StoreAddress = x.Address,
                //               StoreLatitude = x.Latitude,
                //               StoreLongitude = x.Longitude,
                //               AverageRatings = x.Owner.AverageValue,
                //               RewardPercentage = x.Owner.HCUAccountParameterAccount
                //                                           .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                //                                           .FirstOrDefault().Value,
                              
                //           }).Distinct()
                //                                 .ToList();
                //    foreach (var item in _CacheStores_Default)
                //    {
                //        item.Location = new GeoCoordinate(item.StoreLatitude, item.StoreLongitude);
                //    }

                //}





                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StoreId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "StoreId", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubReferenceId != 0)
                    {
                        #region Set Default Limit
                        if (_Request.Limit == 0)
                        {
                            _Request.Limit = _AppConfig.DefaultRecordsLimit;
                        }
                        #endregion
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                                 where x.StatusId == HelperStatus.Default.Active
                                                    && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                                                    && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                    && x.Latitude != 0
                                                    && x.Longitude != 0
                                                    && x.CountryId == _Request.UserReference.CountryId
                                                    && x.Owner.HCUAccountParameterAccount.Any(c=>c.CommonId == _Request.SubReferenceId)
                                                    && (x.Latitude != 0 && x.Longitude != 0)
                                                    && x.Owner.HCUAccountParameterAccount
                                                .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                .FirstOrDefault().Value != "0"
                                                 select new OCustomerApp.Stores
                                                 {
                                                     MerchantId = x.Owner.Id,
                                                     MerchantKey = x.Owner.Guid,
                                                     MerchantName = x.Owner.DisplayName,
                                                     MerchantIconUrl = x.Owner.IconStorage.Path,
                                                     PromoContent = "Best offers",
                                                     Description = x.Owner.Description,
                                                     Rating = x.Owner.AverageValue,
                                                    RatingCount = x.Owner.CountValue,
                                                     StoreId = x.Id,
                                                     StoreKey = x.Guid,
                                                     StoreName = x.DisplayName,
                                                     StoreAddress = x.Address,
                                                     StoreLatitude = x.Latitude,
                                                     StoreLongitude = x.Longitude,
                                                     AverageRatings = x.Owner.AverageValue,
                                                     RewardPercentage = x.Owner.HCUAccountParameterAccount
                                                .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                .FirstOrDefault().Value,
                                                     //Distance = 0,
                                                 })
                                             .Distinct()
                                            .Where(_Request.SearchCondition)
                                            .Count();

                        #endregion
                        #region Get Data
                        List<OCustomerApp.Stores> Data = (from x in _HCoreContext.HCUAccount
                                                          where x.StatusId == HelperStatus.Default.Active
                                                             && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                                                             && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                             && x.Latitude != 0
                                                             && x.Longitude != 0
                                                            && x.CountryId == _Request.UserReference.CountryId
                                                            && (x.Latitude != 0 && x.Longitude != 0)
                                                            && x.Owner.HCUAccountParameterAccount.Any(c=>c.CommonId == _Request.SubReferenceId)
                                                             && x.Owner.HCUAccountParameterAccount
                                                         .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                         .FirstOrDefault().Value != "0"
                                                          select new OCustomerApp.Stores
                                                          {
                                                              MerchantId = x.Owner.Id,
                                                              MerchantKey = x.Owner.Guid,
                                                              MerchantName = x.Owner.DisplayName,
                                                              MerchantIconUrl = x.Owner.IconStorage.Path,
                                                              Rating = x.Owner.AverageValue,
                                                              RatingCount = x.Owner.CountValue,
                                                              PromoContent = "Best offers",
                                                              Description  =x.Owner.Description,
                                                              StoreId = x.Id,
                                                              StoreKey = x.Guid,
                                                              StoreName = x.DisplayName,
                                                              StoreAddress = x.Address,
                                                              StoreLatitude = x.Latitude,
                                                              StoreLongitude = x.Longitude,
                                                              AverageRatings = x.Owner.AverageValue,
                                                              RewardPercentage = x.Owner.HCUAccountParameterAccount
                                                .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                .FirstOrDefault().Value,
                                                              //Distance = 0,
                                                              Location = new GeoCoordinate(x.Latitude, x.Longitude),
                                                          })
                                                 .Distinct()
                                                  .Where(_Request.SearchCondition)
                                                  //.OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            Details.Distance = Math.Round(Details.Location.GetDistanceTo(_UserCoordinates) / 1000, 2);
                            if (!string.IsNullOrEmpty(Details.MerchantIconUrl))
                            {
                                Details.MerchantIconUrl = _AppConfig.StorageUrl + Details.MerchantIconUrl;
                            }
                            else
                            {
                                Details.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Set Default Limit
                        if (_Request.Limit == 0)
                        {
                            _Request.Limit = _AppConfig.DefaultRecordsLimit;
                        }
                        #endregion
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                                 where x.StatusId == HelperStatus.Default.Active
                                                    && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                                                    && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                    && x.Latitude != 0
                                                    && x.CountryId == _Request.UserReference.CountryId
                                                    && x.Longitude != 0
                                                    && (x.Latitude != 0 && x.Longitude != 0)
                                                 select new OCustomerApp.Stores
                                                 {
                                                     MerchantId = x.Owner.Id,
                                                     MerchantKey = x.Owner.Guid,
                                                     MerchantName = x.Owner.DisplayName,
                                                     MerchantIconUrl = x.Owner.IconStorage.Path,
                                                     Rating = x.Owner.AverageValue,
                                                     RatingCount = x.Owner.CountValue,
                                                     PromoContent = "Best offers",
                                                     Description = x.Owner.Description,
                                                     StoreId = x.Id,
                                                     StoreKey = x.Guid,
                                                     StoreName = x.DisplayName,
                                                     StoreAddress = x.Address,
                                                     StoreLatitude = x.Latitude,
                                                     StoreLongitude = x.Longitude,
                                                     AverageRatings = x.Owner.AverageValue,
                                                     RewardPercentage = x.Owner.HCUAccountParameterAccount
                                                .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                .FirstOrDefault().Value,
                                                     //Distance = 0,
                                                 })
                                             .Distinct()
                                            .Where(_Request.SearchCondition)
                                            .Count();

                        #endregion
                        #region Get Data
                        List<OCustomerApp.Stores> Data =
                            _HCoreContext.HCUAccount.Where(x=>
                            x.StatusId == HelperStatus.Default.Active
                                                             && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                                                    && x.CountryId == _Request.UserReference.CountryId
                                                             && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                             && x.Latitude != 0
                                                                && x.Longitude != 0
                            )
                            .Select(x=> new OCustomerApp.Stores
                            {
                                MerchantId = x.Owner.Id,
                                MerchantKey = x.Owner.Guid,
                                MerchantName = x.Owner.DisplayName,
                                MerchantIconUrl = x.Owner.IconStorage.Path,
                                Rating = x.Owner.AverageValue,
                                RatingCount = x.Owner.CountValue,
                                PromoContent = "Best offers",
                                                     Description = x.Owner.Description,
                                StoreId = x.Id,
                                StoreKey = x.Guid,
                                StoreName = x.DisplayName,
                                StoreAddress = x.Address,
                                StoreLatitude = x.Latitude,
                                StoreLongitude = x.Longitude,
                                AverageRatings = x.Owner.AverageValue,
                                RewardPercentage = x.Owner.HCUAccountParameterAccount
                                                            .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                            .FirstOrDefault().Value,
                                //Location = _CacheStores_Default.Where(a=>a.StoreId == x.Id).FirstOrDefault().Location,
                            }).Distinct()
                                                   .Where(_Request.SearchCondition)
                                                  //.OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();





                        //(from x in _HCoreContext.HCUAccount
                        //                                  where x.StatusId == HelperStatus.Default.Active
                        //                                     && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                        //                                     && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                        //                                     && x.Latitude != null 
                        //                                     && x.Longitude != null 
                        //                                     && x.Latitude != 0
                        //                                        && x.Longitude != 0
                        //                            && (x.Latitude != 0 && x.Longitude != 0)
                        //                                  orderby new GeoCoordinate(x.Latitude, x.Longitude).GetDistanceTo(_UserCoordinates)
                        //                                  select new OCustomerApp.Stores
                        //                                  {
                        //                                      MerchantId = x.Owner.Id,
                        //                                      MerchantKey = x.Owner.Guid,
                        //                                      MerchantName = x.Owner.DisplayName,
                        //                                      MerchantIconUrl = x.Owner.IconStorage.Path,
                        //                                      Rating = x.Owner.AverageValue,
                        //                                      RatingCount = x.Owner.CountValue,
                        //                                      PromoContent = "Best offers",

                        //                                      StoreId = x.Id,
                        //                                      StoreKey = x.Guid,
                        //                                      StoreName = x.DisplayName,
                        //                                      StoreAddress = x.Address,
                        //                                      StoreLatitude = x.Latitude,
                        //                                      StoreLongitude = x.Longitude,
                        //                                      AverageRatings = x.Owner.AverageValue,
                        //                                      RewardPercentage = x.Owner.HCUAccountParameterAccount
                        //                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                        //                                    .FirstOrDefault().Value,
                        //                                      Location = new GeoCoordinate(x.Latitude, x.Longitude),
                        //                                  })
                        //                         .Distinct()
                        //                          .Where(_Request.SearchCon   dition)
                        //                          .OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
                        //                          .Skip(_Request.Offset)
                        //                          .Take(_Request.Limit)
                        //                          .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            Details.Location = new GeoCoordinate(Details.StoreLatitude, Details.StoreLongitude);
                            Details.Distance = Math.Round(Details.Location.GetDistanceTo(_UserCoordinates) / 1000, 2);
                            if (!string.IsNullOrEmpty(Details.MerchantIconUrl))
                            {
                                Details.MerchantIconUrl = _AppConfig.StorageUrl + Details.MerchantIconUrl;
                            }
                            else
                            {
                                Details.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetNearbyStore", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


        //internal OResponse GetNearbyStore(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Add Default Request
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSearchCondition(_Request, "StoreId", "0", "<>");
        //            #endregion
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSortCondition(_Request, "StoreId", "desc");
        //            #endregion
        //        }
        //        #endregion
        //        #region Operation
        //        //var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_CacheStores_Default.Count == 0)
        //            {
        //                _CacheStores_Default = (from x in _HCoreContext.HCUAccount
        //                                        where x.StatusId == HelperStatus.Default.Active
        //                                           && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
        //                                           && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
        //                                            && x.Latitude != null
        //                                              && x.Longitude != null
        //                                  && (x.Latitude != 0 && x.Longitude != 0)
        //                                           && x.Owner.HCUAccountParameterAccount
        //                                       .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                       .FirstOrDefault().Value != "0"
        //                                        select new OCustomerApp.Stores
        //                                        {
        //                                            MerchantId = x.Owner.Id,
        //                                            MerchantKey = x.Owner.Guid,
        //                                            MerchantName = x.Owner.DisplayName,
        //                                            MerchantIconUrl = x.Owner.IconStorage.Path,

        //                                            StoreId = x.Id,
        //                                            StoreKey = x.Guid,
        //                                            StoreName = x.DisplayName,
        //                                            StoreAddress = x.Address,
        //                                            StoreLatitude = x.Latitude,
        //                                            StoreLongitude = x.Longitude,
        //                                            AverageRatings = x.Owner.AverageValue,
        //                                            RewardPercentage = x.Owner.HCUAccountParameterAccount
        //                                              .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                              .FirstOrDefault().Value,
        //                                            Location = new GeoCoordinate(x.Latitude, x.Longitude),
        //                                        })
        //                                      .ToList();
        //            }
        //            #region Set Default Limit
        //            if (_Request.Limit == 0)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            #endregion
        //            #region Total Records
        //                _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
        //                                     where x.StatusId == HelperStatus.Default.Active
        //                                        && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
        //                                        && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
        //                                          && x.Latitude != null
        //                                        && x.Longitude != null
        //                                        &&(x.Latitude != 0 && x.Longitude != 0)
        //                                        && x.Owner.HCUAccountParameterAccount
        //                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                    .FirstOrDefault().Value != "0"
        //                                     select new OCustomerApp.Stores
        //                                     {
        //                                         MerchantId = x.Owner.Id,
        //                                         MerchantKey = x.Owner.Guid,
        //                                         MerchantName = x.Owner.DisplayName,
        //                                         MerchantIconUrl = x.Owner.IconStorage.Path,

        //                                         StoreId = x.Id,
        //                                         StoreKey = x.Guid,
        //                                         StoreName = x.DisplayName,
        //                                         StoreAddress = x.Address,
        //                                         StoreLatitude = x.Latitude,
        //                                         StoreLongitude = x.Longitude,
        //                                         AverageRatings = x.Owner.AverageValue,
        //                                         RewardPercentage  = x.Owner.HCUAccountParameterAccount
        //                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                    .FirstOrDefault().Value,
        //                                         //Distance = 0,
        //                                     })
        //                                     .Distinct()
        //                                    .Where(_Request.SearchCondition)
        //                                    .Count();

        //            #endregion
        //            #region Get Data
        //            List<OCustomerApp.Stores> Data = (from x in _HCoreContext.HCUAccount
        //                                              where x.StatusId == HelperStatus.Default.Active
        //                                                 && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
        //                                                 && x.Owner.IconStorageId != null && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
        //                                                  && x.Latitude != null
        //                                                    && x.Longitude != null
        //                                        &&(x.Latitude != 0 && x.Longitude != 0)
        //                                                 && x.Owner.HCUAccountParameterAccount
        //                                             .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                             .FirstOrDefault().Value != "0"
        //                                              select new OCustomerApp.Stores
        //                                              {
        //                                                  MerchantId = x.Owner.Id,
        //                                                  MerchantKey = x.Owner.Guid,
        //                                                  MerchantName = x.Owner.DisplayName,
        //                                                  MerchantIconUrl = x.Owner.IconStorage.Path,

        //                                                  StoreId = x.Id,
        //                                                  StoreKey = x.Guid,
        //                                                  StoreName = x.DisplayName,
        //                                                  StoreAddress = x.Address,
        //                                                  StoreLatitude = x.Latitude,
        //                                                  StoreLongitude = x.Longitude,
        //                                                  AverageRatings = x.Owner.AverageValue,
        //                                                  RewardPercentage = x.Owner.HCUAccountParameterAccount
        //                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //                                    .FirstOrDefault().Value,
        //                                                  //Distance = 0,
        //                                                  Location = new GeoCoordinate(x.Latitude, x.Longitude),
        //                                              })
        //                                     .Distinct()
        //                                      .OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
        //                                      .Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                                      .ToList();
        //            #endregion
        //            foreach (var Details in Data)
        //            {
        //                Details.Distance = Math.Round(Details.Location.GetDistanceTo(_UserCoordinates) / 1000, 2);
        //                if (!string.IsNullOrEmpty(Details.MerchantIconUrl))
        //                {
        //                    Details.MerchantIconUrl = _AppConfig.StorageUrl + Details.MerchantIconUrl;
        //                }
        //                else
        //                {
        //                    Details.MerchantIconUrl = _AppConfig.Default_Icon;
        //                }
        //            }
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug

        //        HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}




        //internal OResponse GetStores(OList.Request _Request)
        //{ 
        //    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
        //    #region Manage Exception
        //    try
        //    {
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Get Data
        //            _AppCache.Stores = _HCoreContext.HCUAccount
        //        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
        //        && x.StatusId == HelperStatus.Default.Active
        //        && x.Owner.StatusId == HelperStatus.Default.Active
        //        && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
        //        && x.Owner.IconStorageId != null
        //         && x.Owner.HCUAccountParameterAccount
        //        .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
        //        .FirstOrDefault().Value != "0")
        //            .OrderByDescending(x => x.HCUAccountTransactionSubParent.Count())
        //        .Select(x => new OApp.Store
        //        {
        //            ReferenceId = x.Id,
        //            ReferenceKey = x.Guid,
        //            MerchantId = x.OwnerId,
        //            DisplayName = x.DisplayName,
        //            ContactNumber = x.ContactNumber,
        //            EmailAddress = x.EmailAddress,
        //            Address = x.Address,
        //            Latitude = x.Latitude,
        //            Longitude = x.Longitude,
        //            CountValue = x.CountValue,
        //            AverageValue = x.AverageValue,
        //        }).ToList();
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            #endregion
        //        }
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AppCache.Stores, _Request.Offset, _Request.Limit);
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetStores", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}
    }
}
