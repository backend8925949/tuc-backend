//==================================================================================
// FileName: FrameworkCustomerAppTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash.FrameworkCustomerApp
{
    public class FrameworkCustomerAppTransaction
    {
        //HCoreContext _HCoreContext;
        //internal OResponse GetTransactions(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.Type == "loyalty")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "rewards")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.ParentTransaction.TotalAmount > 0
        //                                            && x.CampaignId == null
        //                                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
        //                                                && x.SourceId == TransactionSource.TUC)
        //                                            || x.SourceId == TransactionSource.ThankUCashPlus))
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.CampaignId == null
        //                                           && x.ParentTransaction.TotalAmount > 0
        //                                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
        //                                                && x.SourceId == TransactionSource.TUC)
        //                                            || x.SourceId == TransactionSource.ThankUCashPlus))
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "redeems")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.Type.SubParentId == TransactionTypeCategory.Redeem)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.Type.SubParentId == TransactionTypeCategory.Redeem)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "all")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                #region Total Records
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
        //                                             where x.AccountId == _Request.UserReference.AccountId
        //                                             select new OApp.Transaction
        //                                             {
        //                                                 ReferenceId = x.Id,
        //                                                 ReferenceKey = x.Guid,

        //                                                 TransactionDate = x.TransactionDate,

        //                                                 TypeCode = x.Type.SystemName,
        //                                                 TypeName = x.Type.Name,
        //                                                 TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                 TypeCategoryName = x.Type.SubParent.Name,

        //                                                 ModeCode = x.Mode.SystemName,
        //                                                 ModeName = x.Mode.Name,

        //                                                 SourceCode = x.Source.SystemName,
        //                                                 SourceName = x.Source.Name,

        //                                                 TotalAmount = x.TotalAmount,
        //                                                 InvoiceAmount = x.PurchaseAmount,

        //                                                 StoreId = x.SubParentId,

        //                                                 ReferenceNumber = x.ReferenceNumber,

        //                                                 MerchantId = x.ParentId,
        //                                                 MerchantKey = x.Parent.Guid,
        //                                                 MerchantName = x.Parent.DisplayName,
        //                                                 MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                 StoreKey = x.SubParent.Guid,
        //                                                 StoreName = x.SubParent.DisplayName,
        //                                                 StoreAddress = x.SubParent.Address,
        //                                                 StoreLatitude = x.SubParent.Latitude,
        //                                                 StoreLongitude = x.SubParent.Longitude,

        //                                                 StatusId = x.StatusId,
        //                                                 StatusCode = x.Status.SystemName,
        //                                                 StatusName = x.Status.Name,
        //                                             })
        //                                     .Where(_Request.SearchCondition)
        //                             .Count();
        //                }
        //                #endregion
        //                #region Get Data
        //                List<OApp.Transaction> Data = (from x in _HCoreContext.HCUAccountTransaction
        //                                               where x.AccountId == _Request.UserReference.AccountId
        //                                               select new OApp.Transaction
        //                                               {
        //                                                   ReferenceId = x.Id,
        //                                                   ReferenceKey = x.Guid,

        //                                                   TransactionDate = x.TransactionDate,

        //                                                   TypeCode = x.Type.SystemName,
        //                                                   TypeName = x.Type.Name,
        //                                                   TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                   TypeCategoryName = x.Type.SubParent.Name,

        //                                                   ModeCode = x.Mode.SystemName,
        //                                                   ModeName = x.Mode.Name,

        //                                                   SourceCode = x.Source.SystemName,
        //                                                   SourceName = x.Source.Name,

        //                                                   TotalAmount = x.TotalAmount,
        //                                                   InvoiceAmount = x.PurchaseAmount,

        //                                                   StoreId = x.SubParentId,

        //                                                   ReferenceNumber = x.ReferenceNumber,

        //                                                   MerchantId = x.ParentId,
        //                                                   MerchantKey = x.Parent.Guid,
        //                                                   MerchantName = x.Parent.DisplayName,
        //                                                   MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                   StoreKey = x.SubParent.Guid,
        //                                                   StoreName = x.SubParent.DisplayName,
        //                                                   StoreAddress = x.SubParent.Address,
        //                                                   StoreLatitude = x.SubParent.Latitude,
        //                                                   StoreLongitude = x.SubParent.Longitude,

        //                                                   StatusId = x.StatusId,
        //                                                   StatusCode = x.Status.SystemName,
        //                                                   StatusName = x.Status.Name,
        //                                               })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //                #region Create  Response Object
        //                _HCoreContext.Dispose();
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
        //                #endregion
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "payments")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.SourceId == TransactionSource.TUC
        //                                            && x.TypeId == 479)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,
        //                                                AccountNumber = x.AccountNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.SourceId == TransactionSource.TUC
        //                                            && x.TypeId == 479)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,
        //                                                AccountNumber = x.AccountNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "lcctopup")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.SourceId == TransactionSource.TUC
        //                                            && x.TypeId == 480)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,
        //                                                AccountNumber = x.AccountNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Debit
        //                                            && x.SourceId == TransactionSource.TUC
        //                                            && x.TypeId == 480)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,
        //                                                AccountNumber = x.AccountNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "productpurchase")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.SourceId == 470)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.SourceId == 470)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        else if (_Request.Type == "donations")
        //        {
        //            if (string.IsNullOrEmpty(_Request.SearchCondition))
        //            {
        //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //            }
        //            if (string.IsNullOrEmpty(_Request.SortExpression))
        //            {
        //                HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //            }
        //            if (_Request.Limit < 1)
        //            {
        //                _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.SourceId == 469)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,

        //                                            })
        //                                            .Count(_Request.SearchCondition);
        //                    #endregion
        //                }
        //                #region Get Data
        //                List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
        //                                            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //                                            && x.StatusId == HelperStatus.Transaction.Success
        //                                            && x.ModeId == TransactionMode.Credit
        //                                            && x.SourceId == 469)
        //                                            .Select(x => new OApp.Transaction
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TransactionDate = x.TransactionDate,

        //                                                TypeCode = x.Type.SystemName,
        //                                                TypeName = x.Type.Name,
        //                                                TypeCategoryCode = x.Type.SubParent.SystemName,
        //                                                TypeCategoryName = x.Type.SubParent.Name,

        //                                                ModeCode = x.Mode.SystemName,
        //                                                ModeName = x.Mode.Name,

        //                                                SourceCode = x.Source.SystemName,
        //                                                SourceName = x.Source.Name,

        //                                                TotalAmount = x.TotalAmount,
        //                                                InvoiceAmount = x.PurchaseAmount,

        //                                                StoreId = x.SubParentId,

        //                                                ReferenceNumber = x.ReferenceNumber,

        //                                                MerchantId = x.ParentId,
        //                                                MerchantKey = x.Parent.Guid,
        //                                                MerchantName = x.Parent.DisplayName,
        //                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

        //                                                StoreKey = x.SubParent.Guid,
        //                                                StoreName = x.SubParent.DisplayName,
        //                                                StoreAddress = x.SubParent.Address,
        //                                                StoreLatitude = x.SubParent.Latitude,
        //                                                StoreLongitude = x.SubParent.Longitude,

        //                                                StatusId = x.StatusId,
        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                                 .Where(_Request.SearchCondition)
        //                                                 .OrderBy(_Request.SortExpression)
        //                                                 .Skip(_Request.Offset)
        //                                                 .Take(_Request.Limit)
        //                                                 .ToList();
        //                #endregion
        //                #region Send Response
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
        //                #endregion
        //            }
        //        }
        //        DateTime StartDate = HCoreHelper.GetGMTDate().AddDays(-91);
        //        List<OApp.Transaction> _Data = new List<OApp.Transaction>();
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Get Data
        //            _Data = _HCoreContext.HCUAccountTransaction
        //            .Where(x => x.AccountId == _Request.UserReference.AccountId
        //            && (x.ParentId != 3 && x.ParentId != 971 && x.ParentId != 10 && x.ParentId != 6967)
        //            && x.TotalAmount > 0
        //            && ((x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
        //            || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus))
        //            && x.TransactionDate > StartDate)
        //            .OrderByDescending(x => x.TransactionDate)
        //            .Select(x => new OApp.Transaction
        //            {
        //                ReferenceId = x.Id,
        //                TransactionDate = x.TransactionDate,
        //                MerchantId = x.ParentId,
        //                StoreId = x.SubParentId,
        //                TypeCode = x.Type.SystemName,
        //                TypeName = x.Type.Name,
        //                SourceCode = x.Source.SystemName,
        //                SourceName = x.Source.Name,
        //                TotalAmount = x.TotalAmount,
        //                InvoiceAmount = x.PurchaseAmount,
        //                StatusCode = x.Status.SystemName,
        //                StatusName = x.Status.Name,
        //                ModeCode = x.Mode.SystemName,
        //                ModeName = x.Mode.Name,
        //            }).ToList();
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }

        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}

    }
}
