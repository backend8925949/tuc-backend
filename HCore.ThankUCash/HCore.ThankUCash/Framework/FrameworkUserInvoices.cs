//==================================================================================
// FileName: FrameworkUserInvoices.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to user invoice
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using Akka.Actor;
using HCore.Operations.Object;
using HCore.Operations;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkUserInvoices
    {
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        /// <summary>
        /// Description: Completes the payment invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CompletePaymentInvoice(OInvoice.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Declare
                #endregion
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.PaymentApproverKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001", "Payment approver required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001", "Payment reference missing");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.PaymentModeCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001", "Payment mode missing");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {

                        int PaymentModeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.PaymentModeCode).Select(x => x.Id).FirstOrDefault();
                        if (PaymentModeId == 0)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001", "Payment mode deails not found");
                            #endregion
                        }
                        long PaymentApproverId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.PaymentApproverKey).Select(x => x.Id).FirstOrDefault();
                        if (PaymentApproverId == 0)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR001", "Approver deails not found");
                            #endregion
                        }
                        var InvoiceDetails = _HCoreContext.HCUAccountInvoice.Where(x => x.Guid == _Request.ReferenceKey
                                                                              && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                              .Select(x => new
                                                                              {
                                                                                  InvoiceNumber = x.InoviceNumber,
                                                                                  InvoiceTypeId = x.TypeId,
                                                                                  InvoiceId = x.Id,
                                                                                  UserAccountId = x.AccountId,
                                                                                  UserAccountTypeId = x.Account.AccountTypeId,
                                                                                  TotalAmount = x.TotalAmount,
                                                                              })
                                                           .FirstOrDefault();
                        if (InvoiceDetails != null)
                        {
                            if (_Request.PaymentProofContent != null && !string.IsNullOrEmpty(_Request.PaymentProofContent.Content))
                            {
                                long? PaymentProofId = HCoreHelper.SaveStorage(_Request.PaymentProofContent.Name, _Request.PaymentProofContent.Extension, _Request.PaymentProofContent.Content, null, _Request.UserReference);
                                if (PaymentProofId != null)
                                {
                                    if (PaymentProofId != 0)
                                    {

                                        var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Guid == _Request.ReferenceKey
                                                                                 && x.StatusId == StatusHelperId.Invoice_Pending)

                                                              .FirstOrDefault();
                                        if (InvoiceInfo != null)
                                        {
                                            InvoiceInfo.PaymentProofId = PaymentProofId;
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }

                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardInvoice)
                                {
                                    if (InvoiceDetails.UserAccountTypeId == UserAccountType.Merchant)
                                    {
                                        OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", (long)InvoiceDetails.UserAccountId);
                                        string IsThankUCashEnable = HCoreHelper.GetConfiguration("thankucashplus", InvoiceDetails.UserAccountId);
                                        OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", (long)InvoiceDetails.UserAccountId);
                                        if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay" || RewardDeductionType.TypeCode == "rewarddeductiontype.postpay" || RewardDeductionType.TypeCode == "rewarddeductiontype.directdebit")
                                        {

                                            // Credit Amount To Merchant Amount
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.InvoiceAmount = (double)InvoiceDetails.TotalAmount;
                                            _CoreTransactionRequest.ReferenceNumber = InvoiceDetails.InvoiceNumber;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _CoreTransactionRequest.ParentId = (long)InvoiceDetails.UserAccountId;
                                            _CoreTransactionRequest.ReferenceAmount = (double)InvoiceDetails.TotalAmount;

                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = (long)InvoiceDetails.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.MerchantCredit,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = (double)InvoiceDetails.TotalAmount,
                                                Comission = 0,
                                                TotalAmount = (double)InvoiceDetails.TotalAmount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                                 && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                               .FirstOrDefault();
                                                    if (InvoiceInfo != null)
                                                    {
                                                        if (_Request.PaymentDate != null)
                                                        {
                                                            InvoiceInfo.PaymentDate = _Request.PaymentDate;
                                                        }
                                                        else
                                                        {
                                                            InvoiceInfo.PaymentDate = HCoreHelper.GetGMTDateTime();
                                                        }
                                                        InvoiceInfo.PaymentReference = _Request.PaymentReference;
                                                        InvoiceInfo.PaymentModeId = PaymentModeId;
                                                        InvoiceInfo.PaymentApproverId = PaymentApproverId;
                                                        InvoiceInfo.Comment = _Request.Comment;
                                                        InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                        InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                        InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                        _HCoreContext.SaveChanges();

                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                        #endregion
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }

                                        }
                                        else if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
                                        {
                                            if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardInvoice)
                                            {
                                                var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                               && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                             .FirstOrDefault();
                                                if (InvoiceInfo != null)
                                                {
                                                    if (_Request.PaymentDate != null)
                                                    {
                                                        InvoiceInfo.PaymentDate = _Request.PaymentDate;
                                                    }
                                                    else
                                                    {
                                                        InvoiceInfo.PaymentDate = HCoreHelper.GetGMTDateTime();
                                                    }
                                                    InvoiceInfo.PaymentReference = _Request.PaymentReference;
                                                    InvoiceInfo.PaymentModeId = PaymentModeId;
                                                    InvoiceInfo.PaymentApproverId = PaymentApproverId;
                                                    InvoiceInfo.Comment = _Request.Comment;
                                                    InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                    InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                    InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    _HCoreContext.SaveChanges();

                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                    #endregion
                                                }
                                            }
                                            else if (InvoiceDetails.InvoiceTypeId == InvoiceType.RewardCommissionInvoice)
                                            {
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.InvoiceAmount = (double)InvoiceDetails.TotalAmount;
                                                _CoreTransactionRequest.ReferenceNumber = InvoiceDetails.InvoiceNumber;
                                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                                _CoreTransactionRequest.ReferenceAmount = (double)InvoiceDetails.TotalAmount;
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = (long)InvoiceDetails.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.MerchantCredit,
                                                    SourceId = TransactionSource.Merchant,

                                                    Amount = (double)InvoiceDetails.TotalAmount,
                                                    Comission = 0,
                                                    TotalAmount = (double)InvoiceDetails.TotalAmount,
                                                });
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                                     && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                                   .FirstOrDefault();
                                                        if (InvoiceInfo != null)
                                                        {
                                                            if (_Request.PaymentDate != null)
                                                            {
                                                                InvoiceInfo.PaymentDate = _Request.PaymentDate;
                                                            }
                                                            else
                                                            {
                                                                InvoiceInfo.PaymentDate = HCoreHelper.GetGMTDateTime();
                                                            }
                                                            InvoiceInfo.PaymentReference = _Request.PaymentReference;
                                                            InvoiceInfo.PaymentModeId = PaymentModeId;
                                                            InvoiceInfo.PaymentApproverId = PaymentApproverId;
                                                            InvoiceInfo.Comment = _Request.Comment;
                                                            InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                                            InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                                            InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                            _HCoreContext.SaveChanges();

                                                            #region Send Response
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region Send Response
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                            #endregion
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                            #endregion
                                        }


                                    }
                                    else
                                    {
                                        var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                                && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                              .FirstOrDefault();
                                        if (InvoiceInfo != null)
                                        {

                                            if (_Request.PaymentDate != null)
                                            {
                                                InvoiceInfo.PaymentDate = _Request.PaymentDate;
                                            }
                                            else
                                            {
                                                InvoiceInfo.PaymentDate = HCoreHelper.GetGMTDateTime();
                                            }
                                            InvoiceInfo.PaymentReference = _Request.PaymentReference;
                                            InvoiceInfo.PaymentModeId = PaymentModeId;
                                            InvoiceInfo.PaymentApproverId = PaymentApproverId;
                                            InvoiceInfo.Comment = _Request.Comment;
                                            InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                            InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                            InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.SaveChanges();
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    var InvoiceInfo = _HCoreContext.HCUAccountInvoice.Where(x => x.Id == InvoiceDetails.InvoiceId
                                                                                  && x.StatusId == StatusHelperId.Invoice_Pending)
                                                                                .FirstOrDefault();
                                    if (InvoiceInfo != null)
                                    {
                                        if (_Request.PaymentDate != null)
                                        {
                                            InvoiceInfo.PaymentDate = _Request.PaymentDate;
                                        }
                                        else
                                        {
                                            InvoiceInfo.PaymentDate = HCoreHelper.GetGMTDateTime();
                                        }
                                        InvoiceInfo.PaymentReference = _Request.PaymentReference;
                                        InvoiceInfo.PaymentModeId = PaymentModeId;
                                        InvoiceInfo.PaymentApproverId = PaymentApproverId;
                                        InvoiceInfo.Comment = _Request.Comment;
                                        InvoiceInfo.ModifyById = _Request.UserReference.AccountId;
                                        InvoiceInfo.StatusId = StatusHelperId.Invoice_Paid;
                                        InvoiceInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCR007");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007");
                                        #endregion
                                    }
                                }
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR007", "Unable to update invoice. Invoice must be in pending status to make changes");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdatePaymentInvoice", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserInvoice(OInvoice.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OInvoice.Details Details = (from x in _HCoreContext.HCUAccountInvoice
                                                    select new OInvoice.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        ParentKey = x.Parent.Guid,
                                                        ParentName = x.Parent.Name,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserAccountDisplayName = x.Account.DisplayName,
                                                        UserAccountIconUrl = x.Account.IconStorage.Path,
                                                        UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                        UserAccountTypeName = x.Account.AccountType.Name,
                                                        InvoiceNumber = x.InoviceNumber,
                                                        Name = x.Name,
                                                        Description = x.Description,
                                                        FromName = x.FromName,
                                                        FromAddress = x.FromAddress,
                                                        FromContactNumber = x.FromContactNumber,
                                                        FromEmailAddress = x.FromEmailAddress,
                                                        FromFax = x.FromFax,
                                                        ToName = x.ToName,
                                                        ToAddress = x.ToAddress,
                                                        ToContactNumber = x.ToContactNumber,
                                                        ToEmailAddress = x.ToEmailAddress,
                                                        ToFax = x.ToFax,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        ComissionAmount = x.ComissionAmount,
                                                        DiscountAmount = x.DiscountAmount,
                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceDate = x.InoviceDate,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        Comment = x.Comment,
                                                        CreateDate = x.CreateDate,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                        CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                        ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,


                                                        PaymentDate = x.PaymentDate,
                                                        PaymentApproverDisplayName = x.PaymentApprover.DisplayName,
                                                        PaymentApproverKey = x.PaymentApprover.Guid,
                                                        PaymentReference = x.PaymentReference,
                                                        PaymentModeCode = x.PaymentMode.SystemName,
                                                        PaymentModeName = x.PaymentMode.Name,
                                                        PaymentProofUrl = x.PaymentProof.Path,
                                                        TotalItem = x.TotalItem
                                                    })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();


                        if (Details != null)
                        {
                            Details.Items = _HCoreContext.HCUAccountInvoice
                                .Where(x => x.ParentId == Details.ReferenceId)
                                .Select(x => new OInvoice.Details
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    Name = x.Name,
                                    Description = x.Description,
                                    TotalItem = x.TotalItem,
                                    Amount = x.Amount,
                                    Charge = x.Charge,
                                    ComissionAmount = x.ComissionAmount,
                                    TotalAmount = x.TotalAmount,
                                    InvoiceDate = x.InoviceDate,
                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,
                                    Comment = x.Comment,
                                    CreateDate = x.CreateDate,
                                    CreatedByKey = x.CreatedBy.Guid,
                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                    CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                    ModifyDate = x.ModifyDate,
                                    ModifyByKey = x.ModifyBy.Guid,
                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                    ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                    StatusId = x.StatusId,
                                    StatusCode = x.Status.SystemName,
                                    StatusName = x.Status.Name
                                }).ToList();
                            if (!string.IsNullOrEmpty(Details.PaymentProofUrl))
                            {
                                Details.PaymentProofUrl = _AppConfig.StorageUrl + Details.PaymentProofUrl;
                            }
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            _HCoreContext.Dispose();

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            _HCoreContext.Dispose();

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserLocation", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user invoice list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserInvoice(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountInvoice
                                                 where x.ParentId == null && x.Account.CountryId == _Request.UserReference.CountryId
                                                 select new OInvoice.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,
                                                     ParentKey = x.Parent.Guid,
                                                     ParentName = x.Parent.Name,
                                                     UserAccountId = x.AccountId,
                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,
                                                     UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                     UserAccountTypeName = x.Account.AccountType.Name,
                                                     Name = x.Name,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     InvoiceDate = x.InoviceDate,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     CreateDate = x.CreateDate,
                                                     ModifyDate = x.ModifyDate,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                    }
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OInvoice.List> Data = (from x in _HCoreContext.HCUAccountInvoice
                                                where x.ParentId == null && x.Account.CountryId == _Request.UserReference.CountryId
                                                select new OInvoice.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    ParentKey = x.Parent.Guid,
                                                    ParentName = x.Parent.Name,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserAccountDisplayName = x.Account.DisplayName,
                                                    UserAccountIconUrl = x.Account.IconStorage.Path,
                                                    UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                    UserAccountTypeName = x.Account.AccountType.Name,
                                                    TotalItem = x.TotalItem,
                                                    Name = x.Name,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceDate = x.InoviceDate,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserInvoice", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


    }
}
