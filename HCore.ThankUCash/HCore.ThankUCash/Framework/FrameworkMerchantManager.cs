//==================================================================================
// FileName: FrameworkMerchantManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkMerchantManager
    {

        OCoreTransaction.Request _CoreTransactionRequest;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        /// <summary>
        /// Description: Credits the merchant wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditMerchantWallet(OMerchantCredit.Request _Request)
        {
            try
            {
                _CoreTransactionRequest = new OCoreTransaction.Request();
                _CoreTransactionRequest.UserReference = _Request.UserReference;
                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                _CoreTransactionRequest.ParentId = _Request.AccountId;
                _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                _CoreTransactionRequest.ReferenceAmount = _Request.InvoiceAmount;
                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                {
                    UserAccountId = _Request.AccountId,
                    ModeId = TransactionMode.Credit,
                    TypeId = TransactionType.MerchantCredit,
                    SourceId = TransactionSource.Merchant,
                    Amount = _Request.InvoiceAmount,
                    Comission = 0,
                    TotalAmount = _Request.InvoiceAmount,
                });
                _CoreTransactionRequest.Transactions = _TransactionItems;
                _ManageCoreTransaction = new ManageCoreTransaction();
                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                { 
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG549");
                    #endregion
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                  HCoreHelper.LogException("CreditMerchantWallet", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }

        }
    }
}
