//==================================================================================
// FileName: FrameworkCashierApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to cashier app
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkCashierApp
    {

        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the cashier access.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashierAccess(OCashierApp.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCCA101", "Invalid mobile number");
                    #endregion
                }
                else
                {
                    #region Code Block
                    using (_HCoreContext = new HCoreContext())
                    {
                        string FormatMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        var CashierDetails = _HCoreContext.HCUAccount.Where(x =>
                         x.MobileNumber == FormatMobileNumber
                        && x.AccountTypeId == UserAccountType.MerchantCashier)
                         .Select(x => new OCashierApp.AccessCheck
                         {
                             MerchantName = x.Owner.Owner.DisplayName,
                             MerchantIconUrl = x.Owner.Owner.IconStorage.Path,
                             StoreAddress = x.Owner.Address,
                             StoreName = x.Owner.DisplayName,
                         })
                         .FirstOrDefault();
                        if (CashierDetails != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, CashierDetails, "TUCCA102", "Cashier connected");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCCA103", "Casier account not active for your mobile number. Please contact ThankUCash support.");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCashierAccess", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
