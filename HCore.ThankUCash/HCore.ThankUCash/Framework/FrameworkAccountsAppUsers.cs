//==================================================================================
// FileName: FrameworkAccountsAppUsers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts app user
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Data.Store;
using Akka.Actor;
using HCore.ThankUCash.Actors;
using ClosedXML.Excel;
using System.IO;
using System.Reflection;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkAccountsAppUsers
    {
        HCUAccountParameter _HCUAccountParameter;
        List<OAccount.Account> _Accounts;
        //List<OAccount.AccountDownload> _AccountDownload;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the application users visit.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUsersVisit(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                int TotalRecords = 0;
                _Accounts = new List<OAccount.Account>();
                #region Add Default Request & Limit
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = _Request.ReferenceId;
                    switch (_Request.Type)
                    {
                        case ThankUCashConstant.ListType.All:
                            {
                                #region Total Records
                                if (_Request.RefreshCount)
                                {
                                    TotalRecords = (from x in _HCoreContext.HCUAccount
                                                    where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                                      && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                                    select new OAccount.Account
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        GenderName = x.Gender.Name,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                              .Count(m => m.AccountId == x.Id
                                                                  && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.AccountId == x.Id
                                                               && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                                                          .Where(_Request.SearchCondition)
                                                                                  .Count();
                                }

                                #endregion
                                #region Get Data
                                _Accounts = (from x in _HCoreContext.HCUAccount
                                             where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                             && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                             select new OAccount.Account
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 DisplayName = x.DisplayName,
                                                 MobileNumber = x.MobileNumber,
                                                 EmailAddress = x.EmailAddress,
                                                 GenderName = x.Gender.Name,
                                                 CreateDate = x.CreateDate,
                                                 LastTransactionDate = x.LastTransactionDate,
                                                 TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                  .Count(m => m.AccountId == x.Id
                                                                      && m.TransactionDate > _Request.StartDate
                                                                      && m.TransactionDate < _Request.EndDate
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                   && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                 TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == x.Id
                                                                   && m.TransactionDate > _Request.StartDate
                                                                      && m.TransactionDate < _Request.EndDate
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                   && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                             })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                                foreach (var DataItem in _Accounts)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                break;
                                #endregion
                            }
                        case ThankUCashConstant.ListType.Owner:
                            {
                                #region Total Records
                                if (_Request.RefreshCount)
                                {
                                    TotalRecords = (from x in _HCoreContext.HCUAccount
                                                    where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                                      && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == UserAccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                                    select new OAccount.Account
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        GenderName = x.Gender.Name,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                              .Count(m => m.AccountId == x.Id
                                                                  && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.ParentId == UserAccountId
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.AccountId == x.Id
                                                               && m.ParentId == UserAccountId
                                                               && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                                                          .Where(_Request.SearchCondition)
                                                                                  .Count();
                                }

                                #endregion
                                #region Get Data
                                _Accounts = (from x in _HCoreContext.HCUAccount
                                             where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                             && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == UserAccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                             select new OAccount.Account
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 MobileNumber = x.MobileNumber,
                                                 EmailAddress = x.EmailAddress,
                                                 GenderName = x.Gender.Name,
                                                 CreateDate = x.CreateDate,
                                                 IconUrl = x.IconStorage.Path,
                                                 LastTransactionDate = x.LastTransactionDate,
                                                 TotalTransaction = _HCoreContext.HCUAccountTransaction
                                           .Count(m => m.AccountId == x.Id
                                               && m.TransactionDate > _Request.StartDate
                                               && m.TransactionDate < _Request.EndDate
                                            && m.ParentId == UserAccountId
                                            && m.StatusId == HelperStatus.Transaction.Success
                                            && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                            || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                 TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                           .Where(m => m.AccountId == x.Id
                                            && m.ParentId == UserAccountId
                                            && m.TransactionDate > _Request.StartDate
                                               && m.TransactionDate < _Request.EndDate
                                            && m.StatusId == HelperStatus.Transaction.Success
                                            && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                            || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                             })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                                foreach (var DataItem in _Accounts)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                break;
                                #endregion
                            }
                        case ThankUCashConstant.ListType.SubOwner:
                            {
                                #region Total Records
                                if (_Request.RefreshCount)
                                {
                                    TotalRecords = (from x in _HCoreContext.HCUAccount
                                                    where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                                      && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.SubParentId == UserAccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                                    select new OAccount.Account
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        GenderName = x.Gender.Name,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                              .Count(m => m.AccountId == x.Id
                                                                  && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.SubParentId == UserAccountId
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.AccountId == x.Id
                                                               && m.SubParentId == UserAccountId
                                                               && m.TransactionDate > _Request.StartDate
                                                                  && m.TransactionDate < _Request.EndDate
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                                               || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                                                          .Where(_Request.SearchCondition)
                                                                                  .Count();
                                }

                                #endregion
                                #region Get Data
                                _Accounts = (from x in _HCoreContext.HCUAccount
                                             where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Appuser
                                             && x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.SubParentId == UserAccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)
                                             select new OAccount.Account
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 MobileNumber = x.MobileNumber,
                                                 EmailAddress = x.EmailAddress,
                                                 GenderName = x.Gender.Name,
                                                 CreateDate = x.CreateDate,
                                                 IconUrl = x.IconStorage.Path,
                                                 LastTransactionDate = x.LastTransactionDate,
                                                 TotalTransaction = _HCoreContext.HCUAccountTransaction
                                           .Count(m => m.AccountId == x.Id
                                               && m.TransactionDate > _Request.StartDate
                                               && m.TransactionDate < _Request.EndDate
                                            && m.SubParentId == UserAccountId
                                            && m.StatusId == HelperStatus.Transaction.Success
                                            && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                            || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))),
                                                 TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                           .Where(m => m.AccountId == x.Id
                                            && m.SubParentId == UserAccountId
                                            && m.TransactionDate > _Request.StartDate
                                               && m.TransactionDate < _Request.EndDate
                                            && m.StatusId == HelperStatus.Transaction.Success
                                            && (((m.ModeId == Helpers.TransactionMode.Credit || m.ModeId == Helpers.TransactionMode.Debit) && m.SourceId == Helpers.TransactionSource.TUC && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit)
                                            || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == Helpers.TransactionSource.ThankUCashPlus))).Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                             })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                                foreach (var DataItem in _Accounts)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                break;
                                #endregion
                            }
                        default:
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                            #endregion
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetAppUsers", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        List<object> _DownloadItems;
        /// <summary>
        /// Description: Gets the application user download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetAppUserDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DownloadItems = new List<object>();
                _Accounts = new List<OAccount.Account>();
                //_AccountDownload = new List<OAccount.AccountDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "ThankUCash_Customers_List";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                long IsTucPlusEnable = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    switch (_Request.Type)
                    {
                        case ThankUCashConstant.ListType.All:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.Owner:
                            {
                                IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                                if (IsTucPlusEnable == 1)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                            && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                 && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                    double Iterations = Math.Round((double)_Request.TotalRecords / 500, MidpointRounding.AwayFromZero) + 1;
                                    _Request.Offset = 0;
                                    for (int i = 0; i < Iterations; i++)
                                    {
                                        #region Data
                                        _Accounts.AddRange(_HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                       TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                       TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                       TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(500)
                                                                .ToList());
                                        #endregion
                                        _Request.Offset += 500;
                                    }
                                }
                                else
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                            && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                .Count(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                .Where(m => m.AccountId == x.Id
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.ParentId == _Request.ReferenceId
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                && m.SourceId == TransactionSource.TUC)
                                                                                                .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                            })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                    double Iterations = Math.Round((double)_Request.TotalRecords / 500, MidpointRounding.AwayFromZero) + 1;
                                    _Request.Offset = 0;
                                    for (int i = 0; i < Iterations; i++)
                                    {
                                        #region Data
                                        _Accounts.AddRange(_HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                       TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(500)
                                                                .ToList());
                                        #endregion
                                        _Request.Offset += 500;
                                    }

                                }
                                break;
                            }
                        case ThankUCashConstant.ListType.SubOwner:
                            {
                                break;
                            }
                        case ThankUCashConstant.ListType.CreatedBy:
                            {
                                break;
                            }
                    }
                }
                if (_Request.TotalRecords > 0)
                {
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        if (DataItem.GenderId != null)
                        {
                            DataItem.GenderName = HCoreDataStore.SystemHelpers.Where(m => m.ReferenceId == DataItem.GenderId).Select(m => m.Name).FirstOrDefault();
                        }
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        DataItem.ApplicationStatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.ApplicationStatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.Name).FirstOrDefault();
                        DataItem.AccountLevelCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.AccountLevelName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.Name).FirstOrDefault();

                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(DataItem.OwnerIconUrl))
                        {
                            DataItem.OwnerIconUrl = _AppConfig.StorageUrl + DataItem.OwnerIconUrl;
                        }
                        else
                        {
                            DataItem.OwnerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (DataItem.LastTransactionDate != null)
                        {
                            if (IsTucPlusEnable == 1)
                            {
                                _DownloadItems.Add(new
                                {
                                    DisplayName = DataItem.DisplayName,
                                    Name = DataItem.Name,
                                    EmailAddress = DataItem.EmailAddress,
                                    MobileNumber = DataItem.MobileNumber,
                                    GenderName = DataItem.GenderName,
                                    Address = DataItem.Address,
                                    TotalTransaction = DataItem.TotalTransaction,
                                    TotalInvoiceAmount = DataItem.TotalInvoiceAmount,
                                    TucPlusRewardTransaction = DataItem.TucPlusRewardTransaction,
                                    TucPlusRewardAmount = DataItem.TucPlusRewardAmount,
                                    TucPlusRewardInvoiceAmount = DataItem.TucPlusRewardInvoiceAmount,
                                    TucPlusConvinenceCharge = DataItem.TucPlusConvinenceCharge,
                                    RedeemTransaction = DataItem.RedeemTransaction,
                                    RedeemAmount = DataItem.RedeemAmount,
                                    RedeemInvoiceAmount = DataItem.RedeemInvoiceAmount,
                                    TucPlusRewardClaimTransaction = DataItem.TucPlusRewardClaimTransaction,
                                    TucPlusRewardClaimAmount = DataItem.TucPlusRewardClaimAmount,
                                    TucPlusRewardClaimInvoiceAmount = DataItem.TucPlusRewardClaimInvoiceAmount,
                                    LastTransactionDate = HCoreHelper.GetGMTToNigeria((DateTime)DataItem.LastTransactionDate),
                                    RegistrationDate = HCoreHelper.GetGMTToNigeria(DataItem.CreateDate),
                                    StatusName = DataItem.StatusName,
                                });
                            }
                            else
                            {
                                _DownloadItems.Add(new
                                {
                                    DisplayName = DataItem.DisplayName,
                                    Name = DataItem.Name,
                                    EmailAddress = DataItem.EmailAddress,
                                    MobileNumber = DataItem.MobileNumber,
                                    GenderName = DataItem.GenderName,
                                    Address = DataItem.Address,
                                    TotalTransaction = DataItem.TotalTransaction,
                                    TotalInvoiceAmount = DataItem.TotalInvoiceAmount,
                                    TucRewardTransaction = DataItem.TucRewardTransaction,
                                    TucRewardAmount = DataItem.TucRewardAmount,
                                    TucRewardInvoiceAmount = DataItem.TucRewardInvoiceAmount,
                                    RedeemTransaction = DataItem.RedeemTransaction,
                                    RedeemAmount = DataItem.RedeemAmount,
                                    RedeemInvoiceAmount = DataItem.RedeemInvoiceAmount,
                                    LastTransactionDate = HCoreHelper.GetGMTToNigeria((DateTime)DataItem.LastTransactionDate),
                                    RegistrationDate = HCoreHelper.GetGMTToNigeria(DataItem.CreateDate),
                                    StatusName = DataItem.StatusName,
                                });
                            }
                        }
                        else
                        {
                            if (IsTucPlusEnable == 1)
                            {
                                _DownloadItems.Add(new
                                {
                                    DisplayName = DataItem.DisplayName,
                                    Name = DataItem.Name,
                                    EmailAddress = DataItem.EmailAddress,
                                    MobileNumber = DataItem.MobileNumber,
                                    GenderName = DataItem.GenderName,
                                    Address = DataItem.Address,
                                    TotalTransaction = DataItem.TotalTransaction,
                                    TotalInvoiceAmount = DataItem.TotalInvoiceAmount,
                                    TucPlusRewardTransaction = DataItem.TucPlusRewardTransaction,
                                    TucPlusRewardAmount = DataItem.TucPlusRewardAmount,
                                    TucPlusRewardInvoiceAmount = DataItem.TucPlusRewardInvoiceAmount,
                                    TucPlusConvinenceCharge = DataItem.TucPlusConvinenceCharge,
                                    RedeemTransaction = DataItem.RedeemTransaction,
                                    RedeemAmount = DataItem.RedeemAmount,
                                    RedeemInvoiceAmount = DataItem.RedeemInvoiceAmount,
                                    TucPlusRewardClaimTransaction = DataItem.TucPlusRewardClaimTransaction,
                                    TucPlusRewardClaimAmount = DataItem.TucPlusRewardClaimAmount,
                                    TucPlusRewardClaimInvoiceAmount = DataItem.TucPlusRewardClaimInvoiceAmount,
                                    LastTransactionDate = "n/a",
                                    RegistrationDate = HCoreHelper.GetGMTToNigeria(DataItem.CreateDate),
                                    StatusName = DataItem.StatusName,
                                });
                            }
                            else
                            {
                                _DownloadItems.Add(new
                                {
                                    DisplayName = DataItem.DisplayName,
                                    Name = DataItem.Name,
                                    EmailAddress = DataItem.EmailAddress,
                                    MobileNumber = DataItem.MobileNumber,
                                    GenderName = DataItem.GenderName,
                                    Address = DataItem.Address,
                                    TotalTransaction = DataItem.TotalTransaction,
                                    TotalInvoiceAmount = DataItem.TotalInvoiceAmount,
                                    TucRewardTransaction = DataItem.TucRewardTransaction,
                                    TucRewardAmount = DataItem.TucRewardAmount,
                                    TucRewardInvoiceAmount = DataItem.TucRewardInvoiceAmount,
                                    RedeemTransaction = DataItem.RedeemTransaction,
                                    RedeemAmount = DataItem.RedeemAmount,
                                    RedeemInvoiceAmount = DataItem.RedeemInvoiceAmount,
                                    LastTransactionDate = "n/a",
                                    RegistrationDate = HCoreHelper.GetGMTToNigeria(DataItem.CreateDate),
                                    StatusName = DataItem.StatusName,
                                });
                            }
                        }

                    }
                    #endregion
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("ThankUCash_Customers_List");
                        PropertyInfo[] properties = _DownloadItems.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_DownloadItems);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("ThankUCash_Customers_List", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                        if (TItem != null)
                        {
                            TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            TItem.ModifyById = _Request.UserReference.AccountId;
                            TItem.StatusId = HelperStatus.Default.Blocked;
                            _HCoreContext.SaveChanges();
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAppUserDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application user lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUserLite(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    switch (_Request.Type)
                    {
                        case ThankUCashConstant.ListType.All:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.Owner:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && (x.OwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId)))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId)))
                                                        .Select(x => new OAccount.Account
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            GenderId = x.GenderId,
                                                            DateOfBirth = x.DateOfBirth,
                                                            IconUrl = x.IconStorage.Path,
                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerIconUrl = x.Owner.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            ApplicationStatusId = x.ApplicationStatusId,
                                                            AccountLevelId = x.AccountLevel,
                                                        })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.SubOwner:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        SubOwnerId = x.SubOwnerId,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                        .Select(x => new OAccount.Account
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            GenderId = x.GenderId,
                                                            DateOfBirth = x.DateOfBirth,
                                                            IconUrl = x.IconStorage.Path,
                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerIconUrl = x.Owner.IconStorage.Path,
                                                            SubOwnerId = x.SubOwnerId,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            ApplicationStatusId = x.ApplicationStatusId,
                                                            AccountLevelId = x.AccountLevel,
                                                        })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.CreatedBy:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && (x.CreatedById == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId) || x.HCUAccountTransactionAccount.Any(m => m.CashierId == _Request.ReferenceId)))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        CreateDate = x.CreateDate,
                                                                        CreatedById = x.CreatedById,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.CreatedById == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId) || x.HCUAccountTransactionAccount.Any(m => m.CashierId == _Request.ReferenceId)))
                                                        .Select(x => new OAccount.Account
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            GenderId = x.GenderId,
                                                            DateOfBirth = x.DateOfBirth,
                                                            IconUrl = x.IconStorage.Path,
                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerIconUrl = x.Owner.IconStorage.Path,
                                                            CreateDate = x.CreateDate,
                                                            CreatedById = x.CreatedById,
                                                            StatusId = x.StatusId,
                                                            ApplicationStatusId = x.ApplicationStatusId,
                                                            AccountLevelId = x.AccountLevel,
                                                        })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.Acquirer:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && x.OwnerId == _Request.ReferenceId)
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        SubOwnerId = x.SubOwnerId,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && x.OwnerId == _Request.ReferenceId)
                                                        .Select(x => new OAccount.Account
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            GenderId = x.GenderId,
                                                            DateOfBirth = x.DateOfBirth,
                                                            IconUrl = x.IconStorage.Path,
                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerIconUrl = x.Owner.IconStorage.Path,
                                                            SubOwnerId = x.SubOwnerId,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            ApplicationStatusId = x.ApplicationStatusId,
                                                            AccountLevelId = x.AccountLevel,
                                                        })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        case ThankUCashConstant.ListType.Provider:
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && (x.OwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.ProviderId == _Request.ReferenceId)))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        CreateDate = x.CreateDate,
                                                                        CreatedById = x.CreatedById,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                    #endregion
                                }
                                #region Data
                                _Accounts = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && (x.OwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.ProviderId == _Request.ReferenceId)))
                                                        .Select(x => new OAccount.Account
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            GenderId = x.GenderId,
                                                            DateOfBirth = x.DateOfBirth,
                                                            IconUrl = x.IconStorage.Path,
                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerIconUrl = x.Owner.IconStorage.Path,
                                                            CreateDate = x.CreateDate,
                                                            CreatedById = x.CreatedById,
                                                            StatusId = x.StatusId,
                                                            ApplicationStatusId = x.ApplicationStatusId,
                                                            AccountLevelId = x.AccountLevel,
                                                        })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                                .ToList();
                                #endregion
                                break;
                            }
                        default:
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    }
                }
                #region Create  Response         Object
                _HCoreContext.Dispose();
                foreach (var DataItem in _Accounts)
                {
                    if (DataItem.GenderId != null)
                    {
                        DataItem.GenderName = HCoreDataStore.SystemHelpers.Where(m => m.ReferenceId == DataItem.GenderId).Select(m => m.Name).FirstOrDefault();
                    }
                    DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();

                    DataItem.ApplicationStatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.ApplicationStatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.Name).FirstOrDefault();
                    DataItem.AccountLevelCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.AccountLevelName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.Name).FirstOrDefault();

                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    {
                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    }
                    else
                    {
                        DataItem.IconUrl = _AppConfig.Default_Icon;
                    }

                    if (!string.IsNullOrEmpty(DataItem.OwnerIconUrl))
                    {
                        DataItem.OwnerIconUrl = _AppConfig.StorageUrl + DataItem.OwnerIconUrl;
                    }
                    else
                    {
                        DataItem.OwnerIconUrl = _AppConfig.Default_Icon;
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAppUserLit        e", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application user cards.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUserCards(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    var Data = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.AccountId == _Request.ReferenceId)
                                                .Where(x => x.AccountNumber != null)
                                                .GroupBy(x => x.AccountNumber)
                                                .Select(x => new
                                                {
                                                    AccountNumber = x.Key,
                                                    TotalUseCount = x.Count(),
                                                    UserUseCount = x.Where(m => m.AccountId == _Request.ReferenceId).Count(),
                                                    BankId = x.Select(m => m.CardBankId).FirstOrDefault(),
                                                    BankName = x.Select(m => m.CardBank.Name).FirstOrDefault(),
                                                    CardBrandId = x.Select(m => m.CardBrandId).FirstOrDefault(),
                                                    CardBrandName = x.Select(m => m.CardBrand.Name).FirstOrDefault(),
                                                })
                                                .Skip(0)
                                                .Take(100)
                                                .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserCards", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
