//==================================================================================
// FileName: FrameworkAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Data.Store;
using Akka.Actor;
using HCore.ThankUCash.Actors;
using ClosedXML.Excel;
using System.IO;
using System.Reflection;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkAccounts
    {
        List<OAccount.Account> _Accounts;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Displayname = x.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,

                                                    IconUrl = x.IconStorage.Path,

                                                    LastTransactionDate = x.LastTransactionDate,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    Stores = x.HCUAccountOwnerOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Displayname = x.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,

                                                    IconUrl = x.IconStorage.Path,

                                                    LastTransactionDate = x.LastTransactionDate,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    Stores = x.HCUAccountOwnerOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStore(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    DisplayName = x.DisplayName,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,

                                                    LastTransactionDate = x.LastTransactionDate,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,

                                                    Terminals = x.TUCTerminalStore.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    DisplayName = x.DisplayName,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,

                                                    LastTransactionDate = x.LastTransactionDate,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,

                                                    Terminals = x.TUCTerminalStore.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccountActive("rewardpercentage", DataItem.ReferenceId, (long)DataItem.OwnerId, _Request.UserReference));
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStores", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreLite(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Address = x.Address,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Address = x.Address,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStores", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,

                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    IconUrl = x.IconStorage.Path,

                                                    //Merchants = x.InverseBank.Where(m => m.AccountTypeId == UserAccountType.TerminalAccount).Select(m => m.SubOwner.OwnerId).Distinct().Count(),
                                                    //Stores = x.InverseBank.Where(m => m.AccountTypeId == UserAccountType.TerminalAccount).Select(m => m.SubOwnerId).Distinct().Count(),
                                                    Terminals = x.TUCTerminalAccount.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                               .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,

                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    IconUrl = x.IconStorage.Path,

                                                    //Merchants = x.InverseBank.Where(m => m.AccountTypeId == UserAccountType.TerminalAccount).Select(m => m.SubOwner.OwnerId).Distinct().Count(),
                                                    //Stores = x.InverseBank.Where(m => m.AccountTypeId == UserAccountType.TerminalAccount).Select(m => m.SubOwnerId).Distinct().Count(),
                                                    Terminals = x.TUCTerminalAccount.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirer", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirer merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = 0;
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    }
                    switch (_Request.Type)
                    {

                        case ThankUCashConstant.ListType.SubOwner:
                            #region Total Records
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                                         where x.AccountTypeId == UserAccountType.Merchant
                                                         && x.TUCTerminalMerchant.Any(m =>  m.AcquirerId == UserAccountId)
                                                         select new OAccount.Account
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             IconUrl = x.IconStorage.Path,
                                                             ContactNumber = x.ContactNumber,
                                                             EmailAddress = x.EmailAddress,
                                                             Stores = x.HCUAccountOwnerOwner.Count(n => n.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore),
                                                             Terminals = x.TUCTerminalMerchant.Count(n => n.AcquirerId == UserAccountId),
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             CreateDate = x.CreateDate,

                                                         })
                                         .Where(_Request.SearchCondition)
                                        .Count();
                            }
                            #endregion
                            #region Get Data
                            _Accounts = (from x in _HCoreContext.HCUAccount
                                         where x.AccountTypeId == UserAccountType.Merchant
                                         && x.TUCTerminalMerchant.Any(m => m.AcquirerId == UserAccountId)
                                         select new OAccount.Account
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             DisplayName = x.DisplayName,
                                             IconUrl = x.IconStorage.Path,
                                             ContactNumber = x.ContactNumber,
                                             EmailAddress = x.EmailAddress,
                                             Stores = x.HCUAccountOwnerOwner.Count(n => n.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore),
                                             Terminals = x.TUCTerminalMerchant.Count(n =>  n.AcquirerId == UserAccountId ),
                                             StatusId = x.StatusId,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             CreateDate = x.CreateDate,
                                         })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            break;
                        default:
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerMerchants", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the pg account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPgAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPgAccount", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the position account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPosAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,

                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    IconUrl = x.IconStorage.Path,

                                                    Terminals = x.TUCTerminalProvider.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,

                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    LastLoginDate = x.LastLoginDate,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    IconUrl = x.IconStorage.Path,

                                                    Terminals = x.TUCTerminalProvider.Count(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPosAccount", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashier(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Displayname = x.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    ContactNumber = x.ContactNumber,

                                                    EmployeeId = x.ReferralCode,


                                                    SubOwnerId = x.SubOwnerId,
                                                    SubOwnerKey = x.SubOwner.Guid,
                                                    SubOwnerDisplayName = x.SubOwner.DisplayName,


                                                    OwnerId = x.Owner.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Displayname = x.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    ContactNumber = x.ContactNumber,

                                                    EmployeeId = x.ReferralCode,


                                                    SubOwnerId = x.SubOwnerId,
                                                    SubOwnerKey = x.SubOwner.Guid,
                                                    SubOwnerDisplayName = x.SubOwner.DisplayName,

                                                    OwnerId = x.Owner.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCashier", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId
                                                && x.Provider.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    TerminalId = x.DisplayName,

                                                    Address = x.Store.Address,
                                                    Latitude = x.Store.Latitude,
                                                    Longitude = x.Store.Longitude,

                                                    MerchantId = x.Merchant.Id,
                                                    MerchantKey = x.Merchant.Guid,
                                                    MerchantName = x.Merchant.Name,
                                                    MerchantDisplayName = x.Merchant.DisplayName,
                                                    MerchantMobileNumber = x.Merchant.MobileNumber,
                                                    MerchantIconUrl = x.Merchant.IconStorage.Path,

                                                    StoreId = x.Store.Id,
                                                    StoreKey = x.Store.Guid,
                                                    StoreName = x.Store.Name,
                                                    StoreDisplayName = x.Store.DisplayName,
                                                    StoreMobileNumber = x.Store.MobileNumber,

                                                    ProviderId = x.Provider.Id,
                                                    ProviderKey = x.Provider.Guid,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderMobileNumber = x.Provider.MobileNumber,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,

                                                    AcquirerId = x.AcquirerId,
                                                    AcquirerKey = x.Acquirer.Guid,
                                                    AcquirerName = x.Acquirer.Name,
                                                    AcquirerDisplayName = x.Acquirer.DisplayName,
                                                    AcquirerMobileNumber = x.Acquirer.MobileNumber,
                                                    AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                    ActivityStatusId = x.ActivityStatusId,

                                                    LastTransactionDate = x.LastTransactionDate,
                                                    TransactionAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id && a.ProviderId == _Request.ReferenceId).Sum(m => m.PurchaseAmount),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId
                                                && x.Provider.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    TerminalId = x.DisplayName,

                                                    Address = x.Store.Address,
                                                    Latitude = x.Store.Latitude,
                                                    Longitude = x.Store.Longitude,

                                                    MerchantId = x.Merchant.Id,
                                                    MerchantKey = x.Merchant.Guid,
                                                    MerchantName = x.Merchant.Name,
                                                    MerchantDisplayName = x.Merchant.DisplayName,
                                                    MerchantMobileNumber = x.Merchant.MobileNumber,
                                                    MerchantIconUrl = x.Merchant.IconStorage.Path,

                                                    StoreId = x.Store.Id,
                                                    StoreKey = x.Store.Guid,
                                                    StoreName = x.Store.Name,
                                                    StoreDisplayName = x.Store.DisplayName,
                                                    StoreMobileNumber = x.Store.MobileNumber,

                                                    ProviderId = x.Provider.Id,
                                                    ProviderKey = x.Provider.Guid,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderMobileNumber = x.Provider.MobileNumber,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,

                                                    AcquirerId = x.AcquirerId,
                                                    AcquirerKey = x.Acquirer.Guid,
                                                    AcquirerName = x.Acquirer.Name,
                                                    AcquirerDisplayName = x.Acquirer.DisplayName,
                                                    AcquirerMobileNumber = x.Acquirer.MobileNumber,
                                                    AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                    ActivityStatusId = x.ActivityStatusId,

                                                    LastTransactionDate = x.LastTransactionDate,
                                                    TransactionAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id && a.ProviderId == _Request.ReferenceId).Sum(m => m.PurchaseAmount),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.ActivityStatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ActivityStatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.ActivityStatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ActivityStatusId).Select(x => x.Name).FirstOrDefault();
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            DataItem.StoreIconUrl = DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            DataItem.StoreIconUrl = DataItem.MerchantIconUrl;
                        }

                        if (!string.IsNullOrEmpty(DataItem.AcquirerIconUrl))
                        {
                            DataItem.AcquirerIconUrl = _AppConfig.StorageUrl + DataItem.AcquirerIconUrl;
                        }
                        else
                        {
                            DataItem.AcquirerIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the relationship manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRelationshipManager(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.RelationshipManager)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    UserName = x.User.Username,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmployeeId = x.ReferralCode,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    LastLoginDate = x.LastLoginDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.RelationshipManager)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmployeeId = x.ReferralCode,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.IconStorage.Path,
                                                    LastLoginDate = x.LastLoginDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,


                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRelationshipManager", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.AcquirerSubAccount || x.AccountTypeId == UserAccountType.MerchantSubAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    UserName = x.User.Username,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmployeeId = x.ReferralCode,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    LastLoginDate = x.LastLoginDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.AcquirerSubAccount || x.AccountTypeId == UserAccountType.MerchantSubAccount)
                                                .Select(x => new OAccount.Account
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,

                                                    EmployeeId = x.ReferralCode,

                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,

                                                    IconUrl = x.IconStorage.Path,
                                                    LastLoginDate = x.LastLoginDate,

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,


                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Accounts)
                    {
                        DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSubAccount", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
