//==================================================================================
// FileName: FrameworkRmManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to rm manager
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankUCash.Object;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using HCore.Data.Store;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkRmManager
    {
        HCoreContext _HCoreContext;
        HCUAccountOwner _HCUAccountOwner;
        //List<OHCoreDataStore.Terminal> _Terminals;
        /// <summary>
        /// Description: Assigns the store to rm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse AssignStoreToRm(ORmManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                //long? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM100", "Rm key required");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.StoreKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM101", "Store key required");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {

                        var RmDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.RelationshipManager)
                            .Select(x => new
                            {
                                RmId = x.Id,
                                BankId = x.OwnerId,
                                RmAccountTypeId = x.AccountTypeId,
                            }).FirstOrDefault();

                        long StoreId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.StoreKey && x.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Id).FirstOrDefault();
                        if (RmDetails == null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM102", "Rm details not found");
                            #endregion

                        }
                        if (StoreId == 0)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM103", "Store details not found");
                            #endregion

                        }
                        long CheckRmStore = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == StoreId && x.AccountId == RmDetails.RmId && x.StatusId == HelperStatus.Default.Active).Select(x => x.Id).FirstOrDefault();
                        if (CheckRmStore == 0)
                        {
                            #region Disable Store Rm with same bank id
                            var StoreBankRms = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == StoreId && x.Account.OwnerId == RmDetails.BankId && x.StatusId == HelperStatus.Default.Active).ToList();
                            foreach (var StoreBankRm in StoreBankRms)
                            {
                                StoreBankRm.EndDate = HCoreHelper.GetGMTDateTime();
                                StoreBankRm.StatusId = HelperStatus.Default.Blocked;
                                StoreBankRm.ModifyDate = HCoreHelper.GetGMTDateTime();
                                StoreBankRm.ModifyById = _Request.UserReference.AccountId;
                            }
                            #endregion
                            #region Save New RM 
                            _HCUAccountOwner = new HCUAccountOwner();
                            _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountOwner.AccountId = RmDetails.RmId;
                            _HCUAccountOwner.AccountTypeId = (int)RmDetails.RmAccountTypeId;
                            _HCUAccountOwner.OwnerId = StoreId;
                            _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountOwner.CreatedById = _Request.UserReference.AccountId;
                            _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountOwner.Add(_HCUAccountOwner);
                            _HCoreContext.SaveChanges();
                            #endregion
                            HCoreDataStoreManager.DataStore_RefreshStore(StoreId);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "RM105", "Rm assigned to user");
                            #endregion

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM104", "Rm already assigned to store");
                            #endregion
                        }

                    }
                    #endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Revokes the rm store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RevokeRmStore(ORmManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM200", "Reference key required");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Disable Store Rm with same bank id
                        var AssignmentDetails = _HCoreContext.HCUAccountOwner.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (AssignmentDetails != null)
                        {
                            if (AssignmentDetails.StatusId == HelperStatus.Default.Active)
                            {
                                AssignmentDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                AssignmentDetails.StatusId = HelperStatus.Default.Blocked;
                                AssignmentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                AssignmentDetails.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContext.SaveChanges();
                            HCoreDataStoreManager.DataStore_RefreshStore(AssignmentDetails.OwnerId);
                               #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "RM105", "Store removed from rm");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM201", "Location already removed from rm");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "RM205", "Details not found");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the rm terminals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRmTerminals(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCTerminal
                                                 where  x.AcquirerId == _Request.SubReferenceId
                                                 select new OAccount.Account
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     DisplayName = x.DisplayName,


                                                     StoreId = x.StoreId,
                                                     StoreDisplayName = x.Store.DisplayName,
                                                     Address = x.Store.Address,
                                                     Latitude = x.Store.Latitude,
                                                     Longitude = x.Store.Longitude,

                                                     ProviderId = x.ProviderId,
                                                     OwnerDisplayName = x.Provider.DisplayName,
                                                     OwnerKey = x.Provider.Guid,

                                                     MerchantId = x.MerchantId  , 
                                                     MerchantKey = x.Merchant.Guid,
                                                     MerchantDisplayName = x.Merchant.DisplayName,
                                                     MerchantIconUrl = x.Merchant.IconStorage.Path,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,

                                                     LastTransactionDate = HCoreDataStore.Terminals.Where(m => m.ReferenceId == x.Id).Select(m => m.LastTransactionDate).FirstOrDefault(),
                                                 })
                                     .Where(_Request.SearchCondition)
                             .Count();
                    }
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OAccount.Account> Data = (from x in _HCoreContext.TUCTerminal
                                                   where x.AcquirerId == _Request.SubReferenceId
                                                   select new OAccount.Account
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,


                                                             StoreId = x.StoreId,
                                                             StoreDisplayName = x.Store.DisplayName,
                                                             Address = x.Store.Address,
                                                             Latitude = x.Store.Latitude,
                                                             Longitude = x.Store.Longitude,

                                                             ProviderId = x.ProviderId,
                                                             OwnerDisplayName = x.Provider.DisplayName,
                                                             OwnerKey = x.Provider.Guid,

                                                             MerchantId = x.MerchantId,
                                                             MerchantKey = x.Merchant.Guid,
                                                             MerchantDisplayName = x.Merchant.DisplayName,
                                                             MerchantIconUrl = x.Merchant.IconStorage.Path,


                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,

                                                             LastTransactionDate = HCoreDataStore.Terminals.Where(m => m.ReferenceId == x.Id).Select(m => m.LastTransactionDate).FirstOrDefault(),
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    DateTime TDayStart = HCoreHelper.GetGMTDate();
                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                    foreach (var Details in Data)
                    {
                        if (Details.LastTransactionDate != null)
                        {
                            if (Details.LastTransactionDate > TDayStart && Details.LastTransactionDate < TDayEnd)
                            {
                                Details.ActvityStatusId = 2;
                            }
                            if (Details.LastTransactionDate > T7DayStart && Details.LastTransactionDate < T7DayEnd)
                            {
                                Details.ActvityStatusId = 3;
                            }
                            if (Details.LastTransactionDate > TDeadDayEnd)
                            {
                                Details.ActvityStatusId = 4;
                            }
                        }
                        else
                        {
                            Details.ActvityStatusId = 1;
                        }
                        if (!string.IsNullOrEmpty(Details.MerchantIconUrl))
                        {
                            Details.MerchantIconUrl = _AppConfig.StorageUrl + Details.MerchantIconUrl;
                        }
                        else
                        {
                            Details.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the rm stores.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRmStores(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountOwner
                                                 where x.AccountTypeId == UserAccountType.RelationshipManager
                                                 && x.Owner.AccountTypeId == UserAccountType.MerchantStore
                                                 select new OAccount.Account
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     RmId = x.AccountId,
                                                     RmKey = x.Account.Guid,



                                                     DisplayName = x.Owner.DisplayName,
                                                     Address = x.Owner.Address,
                                                     Latitude = x.Owner.Latitude,
                                                     Longitude = x.Owner.Longitude,


                                                     MerchantId = x.Owner.OwnerId,
                                                     MerchantKey = x.Owner.Owner.Guid,
                                                     MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                     MerchantIconUrl = x.Owner.Owner.IconStorage.Path,


                                                     ContactNumber = x.Owner.ContactNumber,
                                                     EmailAddress = x.Owner.EmailAddress,


                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     Terminals = HCoreDataStore.Terminals.Where(m => m.RmId == x.AccountId && m.StoreId == x.OwnerId).Count(),
                                                     LastTransactionDate = HCoreDataStore.Terminals.Where(m => m.RmId == x.AccountId && m.StoreId == x.OwnerId).Select(m => m.LastTransactionDate).FirstOrDefault(),
                                                 })
                                     .Where(_Request.SearchCondition)
                             .Count();
                    }
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OAccount.Account> Data = (from x in _HCoreContext.HCUAccountOwner
                                                         where x.AccountTypeId == UserAccountType.RelationshipManager
                                                         && x.Owner.AccountTypeId == UserAccountType.MerchantStore
                                                         select new OAccount.Account
                                                         {

                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             RmId = x.AccountId,
                                                             RmKey = x.Account.Guid,



                                                             DisplayName = x.Owner.DisplayName,
                                                             Address = x.Owner.Address,
                                                             Latitude = x.Owner.Latitude,
                                                             Longitude = x.Owner.Longitude,


                                                             MerchantId = x.Owner.OwnerId,
                                                             MerchantKey = x.Owner.Owner.Guid,
                                                             MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                             MerchantIconUrl = x.Owner.Owner.IconStorage.Path,


                                                             ContactNumber = x.Owner.ContactNumber,
                                                             EmailAddress = x.Owner.EmailAddress,


                                                             StartDate = x.StartDate,
                                                             EndDate = x.EndDate,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,

                                                             Terminals = HCoreDataStore.Terminals.Where(m => m.RmId == x.AccountId && m.StoreId == x.OwnerId).Count(),
                                                             LastTransactionDate = HCoreDataStore.Terminals.Where(m => m.RmId == x.AccountId && m.StoreId == x.OwnerId).Select(m => m.LastTransactionDate).FirstOrDefault(),
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {

                         DateTime TDayStart = HCoreHelper.GetGMTDate();
                    DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                    DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                    DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                    DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                        if (Details.LastTransactionDate != null)
                        {
                            if (Details.LastTransactionDate > TDayStart && Details.LastTransactionDate < TDayEnd)
                            {
                                Details.ActvityStatusId = 2;
                            }
                            if (Details.LastTransactionDate > T7DayStart && Details.LastTransactionDate < T7DayEnd)
                            {
                                Details.ActvityStatusId = 3;
                            }
                            if (Details.LastTransactionDate > TDeadDayEnd)
                            {
                                Details.ActvityStatusId = 4;
                            }
                        }
                        else
                        {
                            Details.ActvityStatusId = 1;
                        }
                        if (!string.IsNullOrEmpty(Details.MerchantIconUrl))
                        {
                            Details.MerchantIconUrl = _AppConfig.StorageUrl + Details.MerchantIconUrl;
                        }
                        else
                        {
                            Details.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
