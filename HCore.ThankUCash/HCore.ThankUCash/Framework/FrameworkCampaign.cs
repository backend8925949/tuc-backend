//==================================================================================
// FileName: FrameworkCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to campaign
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkCampaign
    {
        HCoreContext _HCoreContext;
        TUCampaign _TUCampaign;
        /// <summary>
        /// Duplicates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DuplicateCampaign(OCampaign.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA100", "Campaign key missing");
                    #endregion
                }
                else
                {
                    long? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    long? SubTypeId = HCoreHelper.GetSystemHelperId(_Request.SubTypeCode, _Request.UserReference);
                    long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    long? ManagerId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CampaignDetails = _HCoreContext.TUCampaign.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (CampaignDetails != null)
                        {
                            _TUCampaign = new TUCampaign();
                            _TUCampaign.Guid = HCoreHelper.GenerateGuid();
                            _TUCampaign.TypeId = CampaignDetails.TypeId;
                            _TUCampaign.SubTypeId = CampaignDetails.SubTypeId;
                            _TUCampaign.AccountId = CampaignDetails.AccountId;
                            _TUCampaign.Name = CampaignDetails.Name + " COPY " + HCoreHelper.GenerateRandomNumber(4);
                            _TUCampaign.Description = CampaignDetails.Description;
                            _TUCampaign.SubTypeValue = CampaignDetails.SubTypeValue;
                            _TUCampaign.StartDate = CampaignDetails.StartDate;
                            _TUCampaign.EndDate = CampaignDetails.EndDate;
                            _TUCampaign.MinimumInvoiceAmount = CampaignDetails.MinimumInvoiceAmount;
                            _TUCampaign.MaximumInvoiceAmount = CampaignDetails.MaximumInvoiceAmount;
                            _TUCampaign.MinimumRewardAmount = CampaignDetails.MinimumRewardAmount;
                            _TUCampaign.MaximumRewardAmount = CampaignDetails.MaximumRewardAmount;
                            _TUCampaign.ManagerId = CampaignDetails.ManagerId;
                            _TUCampaign.SmsText = CampaignDetails.SmsText;
                            _TUCampaign.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCampaign.CreatedById = _Request.UserReference.AccountId;
                            _TUCampaign.StatusId = HelperStatus.Campaign.Creating;
                            _HCoreContext.TUCampaign.Add(_TUCampaign);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TUCampaign.Guid, "CA104", "Campaign created successfully");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA103", "Campaign details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCampaign", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        TUCampaignAudience _TUCampaignAudience;
        List<TUCampaignAudience> _TUCampaignAudiences;

        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCampaign(OCampaign.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TypeCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA100", "Campaign type missing");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA101", "User key missing");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA102", "Campaign name required");
                    #endregion
                }
                else
                {
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? SubTypeId = HCoreHelper.GetSystemHelperId(_Request.SubTypeCode, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    long? ManagerId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        long CampaignNameCheck = _HCoreContext.TUCampaign.Where(x => x.AccountId == UserAccountId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                        if (CampaignNameCheck == 0)
                        {
                                _TUCampaignAudiences = new List<TUCampaignAudience>();
                            if (_Request.Merchants != null && _Request.Merchants.Count > 0)
                            {
                                foreach (var item in _Request.Merchants)
                                {
                                    _TUCampaignAudiences.Add(new TUCampaignAudience
                                    {
                                        Guid = HCoreHelper.GenerateGuid(),
                                        TypeId  = 1,
                                        UserAccountId = item.ReferenceId,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = _Request.UserReference.AccountId,
                                        StatusId =  HelperStatus.Default.Active,
                                    });
                                }
                            }
                            _TUCampaign = new TUCampaign();
                            _TUCampaign.Guid = HCoreHelper.GenerateGuid();
                            _TUCampaign.TypeId = (int)TypeId;
                            _TUCampaign.SubTypeId = SubTypeId;
                            _TUCampaign.AccountId = (long)UserAccountId;
                            _TUCampaign.Name = _Request.Name;
                            _TUCampaign.Description = _Request.Description;
                            _TUCampaign.SubTypeValue = _Request.SubTypeValue;
                            _TUCampaign.StartDate = (DateTime)_Request.StartDate;
                            _TUCampaign.EndDate =  (DateTime)_Request.EndDate;
                            _TUCampaign.MinimumInvoiceAmount = _Request.MinimumInvoiceAmount;
                            _TUCampaign.MaximumInvoiceAmount = _Request.MaximumInvoiceAmount;
                            _TUCampaign.MinimumRewardAmount = _Request.MinimumRewardAmount;
                            _TUCampaign.MaximumRewardAmount = _Request.MaximumRewardAmount;
                            _TUCampaign.ManagerId = ManagerId;
                            _TUCampaign.SmsText = _Request.SmsText;
                            _TUCampaign.Comment = _Request.Comment;
                            _TUCampaign.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCampaign.CreatedById = _Request.UserReference.AccountId;
                            if (_TUCampaignAudiences.Count > 0)
                            {
                                _TUCampaign.TUCampaignAudience = _TUCampaignAudiences;
                            }
                            _TUCampaign.StatusId = (int)StatusId;
                            _HCoreContext.TUCampaign.Add(_TUCampaign);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TUCampaign.Guid, "CA104", "Campaign created successfully");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA103", "Campaign name already exists");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCampaign", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCampaign(OCampaign.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA100", "Campaign reference missing");
                    #endregion
                }
                else
                {
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? SubTypeId = HCoreHelper.GetSystemHelperId(_Request.SubTypeCode, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    long? ManagerId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        TUCampaign CampaignDetails = _HCoreContext.TUCampaign.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (CampaignDetails != null)
                        {
                            if (TypeId != null)
                            {
                                CampaignDetails.TypeId = (int)TypeId;
                            }
                            if (SubTypeId != null)
                            {
                                CampaignDetails.SubTypeId = (int)SubTypeId;
                            }
                            if (UserAccountId != null)
                            {
                                CampaignDetails.AccountId = (long)UserAccountId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                CampaignDetails.Name = _Request.Name;
                            }

                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                CampaignDetails.Description = _Request.Description;
                            }
                            if (_Request.SubTypeValue != null)
                            {
                                CampaignDetails.SubTypeValue = _Request.SubTypeValue;
                            }
                            if (_Request.StartDate != null)
                            {
                                CampaignDetails.StartDate =  (DateTime)_Request.StartDate;
                            }

                            if (_Request.EndDate != null)
                            {
                                CampaignDetails.EndDate = (DateTime)_Request.EndDate;
                            }

                            if (_Request.MinimumInvoiceAmount != null)
                            {
                                CampaignDetails.MinimumInvoiceAmount = _Request.MinimumInvoiceAmount;
                            }

                            if (_Request.MaximumInvoiceAmount != null)
                            {
                                CampaignDetails.MaximumInvoiceAmount = _Request.MaximumInvoiceAmount;
                            }


                            if (_Request.MinimumRewardAmount != null)
                            {
                                CampaignDetails.MinimumRewardAmount = _Request.MinimumRewardAmount;
                            }


                            if (_Request.MaximumRewardAmount != null)
                            {
                                CampaignDetails.MaximumRewardAmount = _Request.MaximumRewardAmount;
                            }
                            if (ManagerId != null)
                            {
                                CampaignDetails.ManagerId = ManagerId;
                            }

                            if (!string.IsNullOrEmpty(_Request.SmsText))
                            {
                                CampaignDetails.SmsText = _Request.SmsText;
                            }

                            CampaignDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            CampaignDetails.ModifyById = _Request.UserReference.AccountId;
                            if (!string.IsNullOrEmpty(_Request.Comment))
                            {
                                CampaignDetails.Comment = _Request.Comment;
                            }
                            if (StatusId != null)
                            {
                                CampaignDetails.StatusId = (int)StatusId;
                            }
                            _HCoreContext.SaveChanges();

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA103", "Campaign details updated");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA103", "Campaign details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCampaign", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion

        }
        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCampaign(OCampaign.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA100", "Campaign reference missing");
                    #endregion
                }
                else
                {

                    using (_HCoreContext = new HCoreContext())
                    {
                        TUCampaign CampaignDetails = _HCoreContext.TUCampaign.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (CampaignDetails != null)
                        {
                            long CampaignTransactions = _HCoreContext.HCUAccountTransaction.Where(x => x.CampaignId == CampaignDetails.Id).Count();
                            if (CampaignTransactions == 0)
                            {

                                var CampaignAud = _HCoreContext.TUCampaignAudience.Where(x => x.CampaignId == CampaignDetails.Id).ToList();
                                _HCoreContext.TUCampaignAudience.RemoveRange(CampaignAud);
                                _HCoreContext.SaveChanges();

                                TUCampaign CampaignDeleteInfo = _HCoreContext.TUCampaign.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                _HCoreContext.TUCampaign.Remove(CampaignDeleteInfo);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA103", "Campaign deleted successfully");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA100", "Transactions found under campaign operation failed.");
                                #endregion
                            }


                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA103", "Campaign details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCampaign", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion

        }
        /// <summary>
        /// Gets the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaign(OCampaign.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCampaign.Response.Details Data = (from x in _HCoreContext.TUCampaign
                                                       where x.Guid == _Request.ReferenceKey
                                                       select new OCampaign.Response.Details
                                                       {

                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           TypeCode = x.Type.SystemName,
                                                           TypeName = x.Type.Name,

                                                           SubTypeCode = x.SubType.SystemName,
                                                           SubTypeName = x.SubType.Name,

                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,

                                                           Name = x.Name,
                                                           Description = x.Description,

                                                           StartDate = x.StartDate,
                                                           EndDate = x.EndDate,

                                                           SubTypeValue = x.SubTypeValue,

                                                           MinimumInvoiceAmount = x.MinimumInvoiceAmount,
                                                           MaximumInvoiceAmount = x.MaximumInvoiceAmount,

                                                           MinimumRewardAmount = x.MinimumRewardAmount,
                                                           MaximumRewardAmount = x.MaximumRewardAmount,

                                                           ManagerKey = x.Manager.Guid,
                                                           ManagerDisplayName = x.Manager.DisplayName,

                                                           SmsText = x.SmsText,

                                                           Comment = x.Comment,

                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .FirstOrDefault();
                    #endregion
                    if (Data != null)
                    {


                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetCampaign", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the campaign list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaign(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TUCampaign
                                        select new OCampaign.Response.List
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            SubTypeCode = x.SubType.SystemName,
                                            SubTypeName = x.SubType.Name,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,

                                            Name = x.Name,
                                            Description = x.Description,

                                            StartDate = x.StartDate,
                                            EndDate = x.EndDate,

                                            SubTypeValue = x.SubTypeValue,


                                            MinimumInvoiceAmount = x.MinimumInvoiceAmount,
                                            MaximumInvoiceAmount = x.MaximumInvoiceAmount,

                                            MinimumRewardAmount = x.MinimumRewardAmount,
                                            MaximumRewardAmount = x.MaximumRewardAmount,

                                            ManagerKey = x.Manager.Guid,
                                            ManagerDisplayName = x.Manager.DisplayName,

                                            SmsText = x.SmsText,

                                            Comment = x.Comment,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OCampaign.Response.List> Data = (from x in _HCoreContext.TUCampaign
                                                          select new OCampaign.Response.List
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              TypeCode = x.Type.SystemName,
                                                              TypeName = x.Type.Name,

                                                              SubTypeCode = x.SubType.SystemName,
                                                              SubTypeName = x.SubType.Name,

                                                              UserAccountKey = x.Account.Guid,
                                                              UserAccountDisplayName = x.Account.DisplayName,

                                                              Name = x.Name,
                                                              Description = x.Description,

                                                              StartDate = x.StartDate,
                                                              EndDate = x.EndDate,

                                                              SubTypeValue = x.SubTypeValue,


                                                              MinimumInvoiceAmount = x.MinimumInvoiceAmount,
                                                              MaximumInvoiceAmount = x.MaximumInvoiceAmount,

                                                              MinimumRewardAmount = x.MinimumRewardAmount,
                                                              MaximumRewardAmount = x.MaximumRewardAmount,

                                                              ManagerKey = x.Manager.Guid,
                                                              ManagerDisplayName = x.Manager.DisplayName,

                                                              SmsText = x.SmsText,

                                                              Comment = x.Comment,

                                                              CreateDate = x.CreateDate,
                                                              CreatedByKey = x.CreatedBy.Guid,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                              ModifyDate = x.ModifyDate,

                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name
                                                          })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetCampaign", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
