//==================================================================================
// FileName: FrameworkProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to product
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using System.Collections.Generic;
using Akka.Actor;
using HCore.ThankUCash.Actors;
using HCore.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash.Core;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkProduct
    {
        OSaveTransactions _OSaveTransactions;
        OProduct.ProductCodeProcess _ProductCodeProcess;
        HCoreContext _HCoreContext;
        TUProduct _TUProduct;
        ManageUserTransaction _ManageUserTransaction;
        List<OTransactionItem> _TransactionItems;


        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                //long? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                int? RewardTypeId = HCoreHelper.GetSystemHelperId(_Request.RewardTypeCode, _Request.UserReference);
                long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    //long? ParentId = null;
                    //if (!string.IsNullOrEmpty(_Request.ParentKey))
                    //{
                    //    ParentId = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ParentKey).Select(x => x.Id).FirstOrDefault();
                    //}
                    long DetailsCheck = _HCoreContext.TUProduct.Where(x => x.AccountId == UserAccountId && x.TypeId == TUHelpers.Product && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                    if (DetailsCheck == 0)
                    {
                        _TUProduct = new TUProduct();
                        _TUProduct.Guid = HCoreHelper.GenerateGuid();
                        _TUProduct.TypeId = TUHelpers.Product;
                        _TUProduct.AccountId = UserAccountId;
                        _TUProduct.Name = _Request.Name;
                        _TUProduct.SystemName = _Request.SystemName;
                        _TUProduct.Description = _Request.Description;
                        _TUProduct.StartTime = _Request.StartTime;
                        _TUProduct.EndTime = _Request.EndTime;
                        _TUProduct.Quantity = _Request.Quantity;
                        _TUProduct.RewardTypeId = RewardTypeId;
                        _TUProduct.RewardValue = _Request.RewardValue;
                        _TUProduct.UnitPrice = _Request.UnitPrice;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _TUProduct.CreatedById = _Request.UserReference.AccountId;
                        }
                        _TUProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUProduct.StatusId = TUStatusHelper.Product.Active;
                        _HCoreContext.TUProduct.Add(_TUProduct);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _TUProduct.Guid;
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Id == _TUProduct.Id).FirstOrDefault();
                                if (ProductDetails != null)
                                {
                                    ProductDetails.IconStorageId = IconStorageId;
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "TUP108");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP107");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveProductBatch(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                //long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                //long? RewardTypeId = HCoreHelper.GetSystemHelperId(_Request.RewardTypeCode, _Request.UserReference);
                //long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long? ParentId = null;
                    if (!string.IsNullOrEmpty(_Request.ParentKey))
                    {
                        ParentId = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ParentKey).Select(x => x.Id).FirstOrDefault();
                    }

                    long DetailsCheck = _HCoreContext.TUProduct.Where(x => x.ParentId == ParentId && x.TypeId == TUHelpers.ProductBatch && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                    if (DetailsCheck == 0)
                    {
                        _TUProduct = new TUProduct();
                        _TUProduct.Guid = HCoreHelper.GenerateGuid();
                        _TUProduct.TypeId = TUHelpers.ProductBatch;
                        _TUProduct.ParentId = ParentId;
                        _TUProduct.Name = _Request.Name;
                        _TUProduct.SystemName = _Request.SystemName;
                        _TUProduct.Description = _Request.Description;
                        _TUProduct.StartTime = _Request.StartTime;
                        _TUProduct.EndTime = _Request.EndTime;
                        _TUProduct.Quantity = _Request.Quantity;
                        //_TUProduct.RewardTypeId = RewardTypeId;
                        //_TUProduct.RewardValue = _Request.RewardValue;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _TUProduct.CreatedById = _Request.UserReference.AccountId;
                        }
                        _TUProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUProduct.StatusId = TUStatusHelper.ProductBatch.Creating;
                        _HCoreContext.TUProduct.Add(_TUProduct);
                        _HCoreContext.SaveChanges();

                        _ProductCodeProcess = new OProduct.ProductCodeProcess();
                        _ProductCodeProcess.BatchId = _TUProduct.Id;
                        _ProductCodeProcess.Quantity = (long)_Request.Quantity;
                        _ProductCodeProcess.TypeId = TUHelpers.ProductBarCode;
                        _ProductCodeProcess.UserReference = _Request.UserReference;
                        var _ActorSystem = ActorSystem.Create("ActorProductBatchCodeProcess");
                        var _ActorNotifier = _ActorSystem.ActorOf<ActorProductBatchCodeProcess>("ActorProductBatchCodeProcess");
                        _ActorNotifier.Tell(_ProductCodeProcess);

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "TUP110");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP109");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveLead", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                int? RewardTypeId = HCoreHelper.GetSystemHelperId(_Request.RewardTypeCode, _Request.UserReference);
                long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long? ParentId = null;
                    if (!string.IsNullOrEmpty(_Request.ParentKey))
                    {
                        ParentId = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ParentKey).Select(x => x.Id).FirstOrDefault();
                    }
                    var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        long? OldStorageId = ProductDetails.IconStorageId;
                        if (ParentId != null)
                        {
                            ProductDetails.ParentId = ParentId;
                        }
                        if (UserAccountId != null)
                        {
                            ProductDetails.AccountId = UserAccountId;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            ProductDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName))
                        {
                            ProductDetails.SystemName = _Request.SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description))
                        {
                            ProductDetails.Description = _Request.Description;
                        }
                        if (_Request.StartTime != null)
                        {
                            ProductDetails.StartTime = _Request.StartTime;
                        }
                        if (_Request.EndTime != null)
                        {
                            ProductDetails.EndTime = _Request.EndTime;
                        }
                        if (_Request.Quantity != null)
                        {
                            ProductDetails.Quantity = _Request.Quantity;
                        }
                        if (RewardTypeId != null)
                        {
                            ProductDetails.RewardTypeId = RewardTypeId;
                        }
                        if (_Request.RewardValue != null)
                        {
                            ProductDetails.RewardValue = _Request.RewardValue;
                        }
                        if (_Request.UnitPrice != null)
                        {
                            ProductDetails.UnitPrice = _Request.UnitPrice;
                        }
                        if (_Request.UserReference.AccountId != 0)
                        {
                            ProductDetails.ModifyById = _Request.UserReference.AccountId;
                        }
                        if (StatusId != null)
                        {
                            ProductDetails.StatusId = (int)StatusId;
                        }

                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();

                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, OldStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var ProductImageDetails = _HCoreContext.TUProduct.Where(x => x.Id == ProductDetails.Id).FirstOrDefault();
                                if (ProductImageDetails != null)
                                {
                                    ProductImageDetails.IconStorageId = IconStorageId;
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "TUP112");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP111");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProductBatch(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        long? OldStorageId = ProductDetails.IconStorageId;
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            ProductDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName))
                        {
                            ProductDetails.SystemName = _Request.SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description))
                        {
                            ProductDetails.Description = _Request.Description;
                        }
                        if (_Request.StartTime != null)
                        {
                            ProductDetails.StartTime = _Request.StartTime;
                        }
                        if (_Request.EndTime != null)
                        {
                            ProductDetails.EndTime = _Request.EndTime;
                        }
                        if (_Request.Quantity != null)
                        {
                            ProductDetails.Quantity = _Request.Quantity;
                        }
                        if (_Request.UserReference.AccountId != 0)
                        {
                            ProductDetails.ModifyById = _Request.UserReference.AccountId;
                        }
                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the product status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProductStatus(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        ProductDetails.StatusId = (int)StatusId;
                        ProductDetails.ModifyById = _Request.UserReference.AccountId;
                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the product batch status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProductBatchStatus(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        ProductDetails.StatusId = (int)StatusId;
                        ProductDetails.ModifyById = _Request.UserReference.AccountId;
                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        long ProductBatches = _HCoreContext.TUProduct.Where(x => x.ParentId == ProductDetails.Id).Select(x => x.Id).Count();
                        if (ProductBatches > 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Request, "HC1000");
                        }
                        else
                        {
                            _HCoreContext.TUProduct.Remove(ProductDetails);
                            _HCoreContext.SaveChanges();
                            HCoreHelper.DeleteStorage(ProductDetails.IconStorageId, _Request.UserReference);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteProductBatch(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var BatchDetails = _HCoreContext.TUProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (BatchDetails != null)
                    {

                        long ProductCodes = _HCoreContext.TUProductCode.Where(x => x.BatchId == BatchDetails.Id).Select(x => x.Id).Count();
                        if (ProductCodes < 1)
                        {
                            _HCoreContext.TUProduct.Remove(BatchDetails);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        }
                        else
                        {
                            if (BatchDetails.StatusId == TUStatusHelper.ProductBatch.Creating)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Request, "HC1000");
                            }
                            else
                            {
                                long ProductUsedCodes = _HCoreContext.TUProductCode.Where(x => x.BatchId == BatchDetails.Id && x.StatusId == TUStatusHelper.ProductCode.Used).Select(x => x.Id).Count();
                                if (ProductUsedCodes < 1)
                                {
                                    var ProductCodesList = _HCoreContext.TUProductCode.Where(x => x.BatchId == BatchDetails.Id).ToList();
                                    if (ProductCodesList.Count > 0)
                                    {
                                        _HCoreContext.TUProductCode.RemoveRange(ProductCodesList);
                                        _HCoreContext.SaveChanges();
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var ProductToDelete = _HCoreContext.TUProduct.Where(x => x.Id == BatchDetails.Id).FirstOrDefault();
                                            if (ProductToDelete != null)
                                            {
                                                _HCoreContext.TUProduct.Remove(ProductToDelete);
                                                _HCoreContext.SaveChanges();
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");

                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Request, "HC1000");

                                            }
                                        }
                                    }
                                    else
                                    {
                                        _HCoreContext.TUProduct.Remove(BatchDetails);
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");

                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Request, "HC1000");
                                }
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteProductBatch", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProduct.Details Data = (from x in _HCoreContext.TUProduct
                                             where x.Guid == _Request.ReferenceKey
                                             select new OProduct.Details
                                             {

                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 UserAccountKey = x.Account.Guid,
                                                 UserAccountDisplayName = x.Account.DisplayName,

                                                 ParentKey = x.Parent.Guid,
                                                 ParentName = x.Parent.Name,

                                                 TypeCode = x.Type.SystemName,
                                                 TypeName = x.Type.Name,

                                                 Batches = x.InverseParent.Count(),
                                                 Name = x.Name,

                                                 IconUrl = x.IconStorage.Path,

                                                 Quantity = x.Quantity,

                                                 SystemName = x.SystemName,
                                                 Description = x.Description,

                                                 RewardTypeCode = x.RewardType.SystemName,
                                                 RewardTypeName = x.RewardType.Name,

                                                 RewardValue = x.RewardValue,
                                                 UnitPrice = x.UnitPrice,

                                                 CreateDate = x.CreateDate,
                                                 CreatedByKey = x.CreatedBy.Guid,
                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                 ModifyDate = x.ModifyDate,
                                                 ModifyByKey = x.ModifyBy.Guid,
                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                 StartTime = x.StartTime,
                                                 EndTime = x.EndTime,

                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                              .FirstOrDefault();
                    #endregion
                    if (Data != null)
                    {
                        if (!string.IsNullOrEmpty(Data.IconUrl))
                        {
                            Data.IconUrl = _AppConfig.StorageUrl + Data.IconUrl;
                        }
                        else
                        {
                            Data.IconUrl = _AppConfig.Default_Icon;
                        }
                        Data.ItemCodes = _HCoreContext.TUProductCode.Where(x => x.Batch.ParentId == Data.ReferenceId).Count();
                        Data.UsedItemCodes = _HCoreContext.TUProductCode.Where(x => x.Batch.ParentId == Data.ReferenceId && x.AccountId != null).Count();
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductCode(OProductCode.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProductCode.Details Data = (from x in _HCoreContext.TUProductCode
                                                 where x.Guid == _Request.ReferenceKey || x.ItemCode == _Request.ReferenceKey
                                                 select new OProductCode.Details
                                                 {


                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,


                                                     MerchantKey = x.Batch.Parent.Account.Guid,
                                                     MerchantName = x.Batch.Parent.Account.DisplayName,

                                                     BatchKey = x.Batch.Guid,
                                                     BatchName = x.Batch.Name,

                                                     ProductKey = x.Batch.Parent.Guid,
                                                     ProductName = x.Batch.Parent.Name,

                                                     ItemCode = x.ItemCode,


                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                     UseDate = x.UseDate,

                                                     TransactionKey = x.Transaction.Guid,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (Data != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProduct(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TUProduct
                                        where x.TypeId == TUHelpers.Product
                                        select new OProduct.Details
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            ParentKey = x.Parent.Guid,
                                            ParentName = x.Parent.Name,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            //ItemCodes = x.TUProductCode.Count(),
                                            Batches = x.InverseParent.Count(),
                                            Name = x.Name,

                                            IconUrl = x.IconStorage.Path,

                                            Quantity = x.InverseParent.Sum(m => m.Quantity),
                                            UnitPrice = x.UnitPrice,

                                            RewardTypeName = x.RewardType.Name,
                                            RewardValue = x.RewardValue,

                                            CreateDate = x.CreateDate,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OProduct.Details> Data = (from x in _HCoreContext.TUProduct
                                                   select new OProduct.Details
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,

                                                       ParentKey = x.Parent.Guid,
                                                       ParentName = x.Parent.Name,

                                                       TypeCode = x.Type.SystemName,

                                                       UserAccountKey = x.Account.Guid,
                                                       UserAccountDisplayName = x.Account.DisplayName,


                                                       ItemCodes = x.TUProductCode.Count(),
                                                       Batches = x.InverseParent.Count(),
                                                       Name = x.Name,

                                                       IconUrl = x.IconStorage.Path,

                                                       Quantity = x.Quantity,
                                                       UnitPrice = x.UnitPrice,

                                                       RewardTypeName = x.RewardType.Name,
                                                       RewardValue = x.RewardValue,

                                                       CreateDate = x.CreateDate,
                                                       CreatedByKey = x.CreatedBy.Guid,
                                                       CreatedByDisplayName = x.CreatedBy.DisplayName,


                                                       StartTime = x.StartTime,
                                                       EndTime = x.EndTime,

                                                       StatusId = x.StatusId,
                                                       StatusCode = x.Status.SystemName,
                                                       StatusName = x.Status.Name
                                                   })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductBatch(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TUProduct
                                        where x.TypeId == TUHelpers.ProductBatch
                                        select new OProduct.Details
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            ParentKey = x.Parent.Guid,

                                            ItemCodes = x.TUProductCode.Count(),
                                            UsedItemCodes = x.TUProductCode.Where(m => m.BatchId == x.Id && m.AccountId != null).Count(),
                                            Name = x.Name,


                                            Quantity = x.Quantity,

                                            CreateDate = x.CreateDate,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OProduct.Details> Data = (from x in _HCoreContext.TUProduct
                                                   where x.TypeId == TUHelpers.ProductBatch
                                                   select new OProduct.Details
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,

                                                       ParentKey = x.Parent.Guid,

                                                       ItemCodes = x.TUProductCode.Count(),
                                                       UsedItemCodes = x.TUProductCode.Where(m => m.BatchId == x.Id && m.AccountId != null).Count(),
                                                       Name = x.Name,


                                                       Quantity = x.Quantity,

                                                       CreateDate = x.CreateDate,
                                                       StatusId = x.StatusId,
                                                       StatusCode = x.Status.SystemName,
                                                       StatusName = x.Status.Name
                                                   })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductCode(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.TUProductCode
                                        select new OProductCode.Details
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            BatchKey = x.Batch.Guid,
                                            BatchName = x.Batch.Name,

                                            ProductKey = x.Batch.Parent.Guid,
                                            ProductName = x.Batch.Parent.Name,

                                            ItemCode = x.ItemCode,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,

                                            UseDate = x.UseDate,

                                            TransactionKey = x.Transaction.Guid,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OProductCode.Details> Data = (from x in _HCoreContext.TUProductCode
                                                       select new OProductCode.Details
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           TypeCode = x.Type.SystemName,
                                                           TypeName = x.Type.Name,

                                                           BatchKey = x.Batch.Guid,
                                                           BatchName = x.Batch.Name,

                                                           ProductKey = x.Batch.Parent.Guid,
                                                           ProductName = x.Batch.Parent.Name,

                                                           ItemCode = x.ItemCode,

                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,

                                                           UseDate = x.UseDate,

                                                           TransactionKey = x.Transaction.Guid,

                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        //Details.ItemCode = Details.ItemCode.Substring(0, 4) + "xxxx" + Details.ItemCode.Substring(Details.ItemCode.Length - 5, 4);
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProductCode", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the batch codes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBatchCodes(OProduct.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    var Data = (from x in _HCoreContext.TUProductCode
                                where x.Batch.Guid == _Request.ReferenceKey
                                select new
                                {
                                    ItemCode = x.ItemCode,
                                })
                                               .ToList();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProductCode", _Exception, _Request.UserReference);
                #endregion

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Redeems the code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RedeemCode(OProductCode.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProductCode.Details Data = (from x in _HCoreContext.TUProductCode
                                                 where x.ItemCode == _Request.ItemCode
                                                 select new OProductCode.Details
                                                 {

                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,

                                                     MerchantId = x.Batch.Parent.AccountId,
                                                     MerchantKey = x.Batch.Parent.Account.Guid,
                                                     MerchantName = x.Batch.Parent.Account.DisplayName,


                                                     StartTime = x.Batch.Parent.StartTime,
                                                     EndTime = x.Batch.Parent.EndTime,

                                                     BatchKey = x.Batch.Guid,
                                                     BatchName = x.Batch.Name,

                                                     ProductId = x.Batch.ParentId,
                                                     ProductStatusId = x.Batch.Parent.StatusId,
                                                     ProductKey = x.Batch.Parent.Guid,
                                                     ProductName = x.Batch.Parent.Name,

                                                     ItemCode = x.ItemCode,
                                                     BatchId = x.BatchId,
                                                     BatchStatusId = x.Batch.StatusId,


                                                     RewardValue = x.Batch.Parent.RewardValue,
                                                     RewardTypeId = x.Batch.Parent.RewardTypeId,


                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,

                                                     UseDate = x.UseDate,

                                                     TransactionKey = x.Transaction.Guid,

                                                     UnitPrice = x.Batch.Parent.UnitPrice,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    #endregion
                    if (Data != null)
                    {
                        if (Data.ProductStatusId == TUStatusHelper.Product.Active && Data.BatchStatusId == TUStatusHelper.ProductBatch.Active)
                        {
                            if (Data.StatusId == TUStatusHelper.ProductCode.Unused)
                            {
                                DateTime CurrentDate = HCoreHelper.GetGMTDate();
                                if (CurrentDate >= Data.StartTime && CurrentDate <= Data.EndTime)
                                {
                                    var _UserInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId)
                                        .Select(x => new
                                        {
                                            UserAccountId = x.Id,
                                            DisplayName = x.DisplayName,
                                            MobileNumber = x.MobileNumber,
                                            EmailAddress = x.EmailAddress,
                                        }).FirstOrDefault();
                                    double UserRewardPercentage = (double)Math.Round(Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("userrewardpercentage", (long)Data.MerchantId, _Request.UserReference)), 2);
                                    double RewardAmount = 0;
                                    double UserAmount = 0;
                                    double ThankUAmount = 0;
                                    if (Data.RewardTypeId == TUHelpers.ProductRewardTypeAmount)
                                    {
                                        RewardAmount = (double)Data.RewardValue;
                                    }
                                    else if (Data.RewardTypeId == TUHelpers.ProductRewardTypePercentage)
                                    {
                                        RewardAmount = HCoreHelper.GetPercentage((double)Data.UnitPrice, (double)Data.RewardValue, 4);
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                        #endregion
                                    }
                                    UserAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage, 4);
                                    ThankUAmount = RewardAmount - UserAmount;


                                    #region PROCESS TRANSACTION
                                    _OSaveTransactions = new OSaveTransactions();
                                    _OSaveTransactions.UserReference = _Request.UserReference;
                                    _OSaveTransactions.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                    _OSaveTransactions.GroupKey = HCoreHelper.GenerateGuid();
                                    _OSaveTransactions.ParentId = (long)Data.MerchantId;
                                    _OSaveTransactions.InvoiceAmount = (double)Data.UnitPrice;
                                    _OSaveTransactions.ReferenceNumber = Data.ItemCode;
                                    //if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                                    //{
                                    //    _OSaveTransactions.CreatedById = _OGatewayInfo.TerminalId;
                                    //}
                                    _TransactionItems = new List<OTransactionItem>();
                                    // Debit From Merchant
                                    _TransactionItems.Add(new OTransactionItem
                                    {
                                        UserAccountId = (long)Data.MerchantId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.QRCodeReward,
                                        SourceId = TransactionSource.Merchant,
                                        Amount = UserAmount,
                                        Comission = ThankUAmount,
                                        TotalAmount = RewardAmount,
                                        ReferenceAmount = (double)Data.UnitPrice
                                    });
                                    _TransactionItems.Add(new OTransactionItem
                                    {
                                        UserAccountId = _Request.UserReference.AccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.QRCodeReward,
                                        SourceId = TransactionSource.TUC,

                                        Amount = UserAmount,
                                        TotalAmount = UserAmount,
                                        ReferenceAmount = RewardAmount,
                                    });
                                    if (ThankUAmount > 0)
                                    {
                                        _TransactionItems.Add(new OTransactionItem
                                        {
                                            UserAccountId = ThankUCashConstant.ThankUAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.QRCodeReward,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = ThankUAmount,
                                            TotalAmount = ThankUAmount,
                                            ReferenceAmount = RewardAmount,
                                        });
                                    }
                                    _OSaveTransactions.Transactions = _TransactionItems;
                                    _ManageUserTransaction = new ManageUserTransaction();
                                    OTransactionReference TransactionResponse = _ManageUserTransaction.SaveTransaction(_OSaveTransactions);
                                    if (TransactionResponse.Status == TUStatusHelper.Transaction.Success)
                                    {

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var ProdInfo = _HCoreContext.TUProductCode.Where(x => x.Id == Data.ReferenceId).FirstOrDefault();
                                            if (ProdInfo != null)
                                            {
                                                ProdInfo.TransactionId = TransactionResponse.ReferenceId;
                                                ProdInfo.StatusId = TUStatusHelper.ProductCode.Used;
                                                ProdInfo.UseDate = HCoreHelper.GetGMTDateTime();
                                                ProdInfo.AccountId = _Request.UserReference.AccountId;
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                        double Balance = _ManageUserTransaction.GetAppUserBalance(_UserInfo.UserAccountId, _Request.UserReference);
                                        if (HostEnvironment == HostEnvironmentType.Live)
                                        {
                                            #region Send SMS
                                            string Message = "Credit Alert: You got " + UserAmount + " points Cash back from " + Data.MerchantName + " Bal: N" + Balance + ". Download Android http://bit.do/tucan ios http://bit.do/tuci . ThankUCash";
                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                            #endregion
                                        }

                                        #region Send Email 

                                        if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
                                        {
                                            var _EmailParameters = new
                                            {
                                                UserDisplayName = _UserInfo.DisplayName,
                                                MerchantName = Data.MerchantName,
                                                InvoiceAmount = Data.UnitPrice.ToString(),
                                                Amount = UserAmount.ToString(),
                                                Balance = Balance.ToString(),
                                            };
                                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
                                        }
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, UserAmount, "TUP106");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP105");
                                        #endregion
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "TUP104");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP103");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP102");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP101");
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUP100");
                #endregion
            }
            #endregion
        }
    }
}
