//==================================================================================
// FileName: FrameworkOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using HCore.ThankUCash.Object;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
namespace HCore.ThankUCash.Framework
{
    /// <summary>
    /// Class FrameworkOverview.
    /// </summary>
    public class FrameworkOverview
    {
        HCoreContext _HCoreContext;
        OOverview.Overview _OOverviewResponse;
        OChart.Request _OChartRequest;
        List<OChart.RequestDateRange> _OChartRequestRange;
        List<OChart.RequestData> _OChartRequestRangeValue;

        /// <summary>
        /// Description: Gets the position account overview.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <returns>OOverview.Overview.</returns>
        private OOverview.Overview GetPosAccountOverview(long UserAccountId)
        {
            _OOverviewResponse = new OOverview.Overview();
            using (_HCoreContext = new HCoreContext())
            {

                _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal
                   .Where(x => x.ProviderId == UserAccountId)
                   .Count();


                _OOverviewResponse.Merchants = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == Helpers.UserAccountType.Merchant
                    && x.TUCTerminalMerchant.Count() > 0).Count();



                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction
                   .Where(x => x.CreatedBy.OwnerId == UserAccountId
                   && x.SourceId == TransactionSource.TUC
                   && (x.ModeId == TransactionMode.Credit ||
                   x.ModeId == TransactionMode.Debit)
                   ).Select(x => x.AccountId).Distinct()
                   .Count();



                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccountId
                    && x.SourceId == TransactionSource.TUC
                    && (x.ModeId == TransactionMode.Credit ||
                    x.ModeId == TransactionMode.Debit)
                    )
                    .Count();

                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccountId
                    && x.SourceId == TransactionSource.TUC
                    && (x.ModeId == TransactionMode.Credit ||
                    x.ModeId == TransactionMode.Debit)
                    )
                    .Sum(x => x.PurchaseAmount);


                //Rewards
                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccountId
                    && x.SourceId == TransactionSource.Merchant
                    && x.ModeId == TransactionMode.Debit
                    )
                    .Sum(x => x.TotalAmount);

                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                   .Where(x => x.CreatedBy.OwnerId == UserAccountId
                   && x.SourceId == TransactionSource.Merchant
                   && x.ModeId == TransactionMode.Debit
                   )
                   .Sum(x => x.PurchaseAmount);

                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                   .Where(x => x.CreatedBy.OwnerId == UserAccountId
                   && x.SourceId == TransactionSource.Merchant
                   && x.ModeId == TransactionMode.Debit
                   )
                   .Count();

                _OOverviewResponse.RewardLastTransaction = _HCoreContext.HCUAccountTransaction
                  .Where(x => x.CreatedBy.OwnerId == UserAccountId
                  && x.SourceId == TransactionSource.Merchant
                  && x.ModeId == TransactionMode.Debit
                  ).OrderByDescending(x => x.TransactionDate)
                  .Select(x => x.TransactionDate).FirstOrDefault();


                //Redeem
                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.CreatedBy.OwnerId == UserAccountId
                    && x.SourceId == TransactionSource.TUC
                    && x.ModeId == TransactionMode.Debit
                    )
                    .Count();


                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                   .Where(x => x.CreatedBy.OwnerId == UserAccountId
                   && x.SourceId == TransactionSource.TUC
                   && x.ModeId == TransactionMode.Debit
                   )
                   .Sum(x => x.PurchaseAmount);


                _OOverviewResponse.RedeemLastTransaction = _HCoreContext.HCUAccountTransaction
                  .Where(x => x.CreatedBy.OwnerId == UserAccountId
                  && x.SourceId == TransactionSource.TUC
                  && x.ModeId == TransactionMode.Debit
                  )
                  .OrderByDescending(x => x.TransactionDate)
                  .Select(x => x.TransactionDate).FirstOrDefault();


                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                 .Where(x => x.CreatedBy.OwnerId == UserAccountId
                 && x.SourceId == TransactionSource.TUC
                 && x.ModeId == TransactionMode.Debit
                 )
                 .Sum(x => x.TotalAmount);

                //Com
                _OOverviewResponse.Commission = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.AccountId == UserAccountId
                    && x.SourceId == TransactionSource.Settlement
                    && x.ModeId == TransactionMode.Credit
                    )
                    .Sum(x => x.TotalAmount);

            }
            return _OOverviewResponse;
        }
        /// <summary>
        /// Description: Gets the reward overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }

                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId));
                                if (SubUserAccountId != 0)
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                         m.TransactionDate > _Request.StartTime &&
                                                                         m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                    _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                          m.AccountId == SubUserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                          m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                          m.ModeId == Helpers.TransactionMode.Credit &&
                                                                          m.TransactionDate > _Request.StartTime &&
                                                                          m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.TUC)
                                                                         .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                               .Count();
                                    if (_OOverviewResponse.ThankUCashPlus == 1)
                                    {

                                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.AccountId == SubUserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.ThankUCashPlus)
                                                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m =>
                                                                                    m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                                   .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                   .Count();

                                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                     m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                 .Count();
                                    }
                                    #endregion

                                }
                                else
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                         m.TransactionDate > _Request.StartTime &&
                                                                         m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                    _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                          m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                          m.ModeId == Helpers.TransactionMode.Credit &&
                                                                          m.TransactionDate > _Request.StartTime &&
                                                                          m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.TUC)
                                                                         .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                               .Count();
                                    if (_OOverviewResponse.ThankUCashPlus == 1)
                                    {

                                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.ThankUCashPlus)
                                                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m =>
                                                                                    m.ParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                                   .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                   .Count();

                                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                     m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                 .Count();
                                    }
                                    #endregion

                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.OwnerId));
                                if (SubUserAccountId != 0)
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                           m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                           m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                           m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.TUC)
                                                                          .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                    _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                          m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                          m.ModeId == Helpers.TransactionMode.Credit &&
                                                                          m.TransactionDate > _Request.StartTime &&
                                                                          m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.TUC)
                                                                         .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                               .Count();
                                    if (_OOverviewResponse.ThankUCashPlus == 1)
                                    {

                                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m =>
                                                                                    m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                                   .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                   .Count();

                                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                     m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                 .Count();
                                    }
                                    #endregion

                                }
                                else
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                           m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                           m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                           m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.TUC)
                                                                          .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                    _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                          m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                          m.ModeId == Helpers.TransactionMode.Credit &&
                                                                          m.TransactionDate > _Request.StartTime &&
                                                                          m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.TUC)
                                                                         .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                            m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.TUC)
                                                                               .Count();
                                    if (_OOverviewResponse.ThankUCashPlus == 1)
                                    {

                                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                            m.TransactionDate < _Request.EndTime &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m =>
                                                                                    m.SubParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                                   .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                       m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                   .Count();

                                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                     m.TransactionDate > _Request.StartTime &&
                                                                                     m.TransactionDate < _Request.EndTime &&
                                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                 .Count();
                                    }
                                    #endregion

                                }
                                #region Get     Data
                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();
                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                long MerchantId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.OwnerId).FirstOrDefault();
                                long StoreId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.OwnerId).FirstOrDefault();
                                #region Get     Data
                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();






                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data


                                _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                           .Count();

                                _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.ThankUCashPlus)
                                                                         .Count();



                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                              m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0);


                                _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0);
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                                #endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Get     Data
                        _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m =>
                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                             m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                             m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                             m.TransactionDate > _Request.StartTime &&
                                                             m.TransactionDate < _Request.EndTime &&
                                                             m.SourceId == TransactionSource.TUC)
                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m =>
                                                                m.StatusId == HelperStatus.Transaction.Success &&
                                                                m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                m.ModeId == Helpers.TransactionMode.Credit &&
                                                                m.TransactionDate > _Request.StartTime &&
                                                                m.TransactionDate < _Request.EndTime &&
                                                                m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                        _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m =>
                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                              m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                              m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                              m.ModeId == Helpers.TransactionMode.Credit &&
                                                              m.TransactionDate > _Request.StartTime &&
                                                              m.TransactionDate < _Request.EndTime &&
                                                              m.SourceId == TransactionSource.TUC)
                                                             .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m =>
                                                                m.StatusId == HelperStatus.Transaction.Success &&
                                                                m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                m.ModeId == Helpers.TransactionMode.Credit &&
                                                                m.TransactionDate > _Request.StartTime &&
                                                                m.TransactionDate < _Request.EndTime &&
                                                                m.SourceId == TransactionSource.TUC)
                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m =>
                                                                m.StatusId == HelperStatus.Transaction.Success &&
                                                                m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                m.ModeId == Helpers.TransactionMode.Credit &&
                                                                m.TransactionDate > _Request.StartTime &&
                                                                m.TransactionDate < _Request.EndTime &&
                                                                m.SourceId == TransactionSource.TUC)
                                                                   .Count();


                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                  .Where(m =>
                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                        m.TransactionDate > _Request.StartTime &&
                                                        m.TransactionDate < _Request.EndTime &&
                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                  .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(m =>
                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                      m.TransactionDate > _Request.StartTime &&
                                                      m.TransactionDate < _Request.EndTime &&
                                                      m.SourceId == TransactionSource.ThankUCashPlus)
                                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                        _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                      .Where(m =>
                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                            m.TransactionDate > _Request.StartTime &&
                                                            m.TransactionDate < _Request.EndTime &&
                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m =>
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m =>
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                   .Count();

                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m =>
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m =>
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 .Count();

                        #endregion

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRewardOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the reward type overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardTypeOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            _OOverviewResponse.CashRewardTransactions = 0;
            _OOverviewResponse.CashRewardAmount = 0;
            _OOverviewResponse.CashRewardPurchaseAmount = 0;

            _OOverviewResponse.CardRewardTransactions = 0;
            _OOverviewResponse.CardRewardAmount = 0;
            _OOverviewResponse.CardRewardPurchaseAmount = 0;


            _OOverviewResponse.OtherRewardTransactions = 0;
            _OOverviewResponse.OtherRewardAmount = 0;
            _OOverviewResponse.OtherRewardPurchaseAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Appuser)
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                    && m.AccountId == SubUserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }

                                    _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();

                                    foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                    {

                                        CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.AccountId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.AccountId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                         && m.AccountId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBrandId == CardBrand.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                    _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();
                                    foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                    {

                                        CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.AccountId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.AccountId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                         && m.AccountId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBankId == CardBank.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();
                                    #endregion
                                }
                                else if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Acquirer)
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                             && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                              && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                             && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.BankId == SubUserAccountId
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                 && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                 && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            & m.BankId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                               & m.BankId == SubUserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }

                                    _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();

                                    foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                    {

                                        CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                        & m.BankId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBrandId == CardBrand.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                    _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();
                                    foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                    {

                                        CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                        & m.BankId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBankId == CardBank.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();
                                    #endregion


                                }
                                else
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }

                                    _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();

                                    foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                    {

                                        CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBrandId == CardBrand.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                    _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();
                                    foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                    {

                                        CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBankId == CardBank.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();
                                    #endregion
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Appuser)
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                        && m.AccountId == SubUserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.AccountId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.AccountId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                    && m.AccountId == SubUserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }
                                    #endregion

                                }
                                else if (SubUserAccountId != 0 && SubUserAccountTypeId == UserAccountType.Acquirer)
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.BankId == SubUserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.BankId == SubUserAccountId
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.BankId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            & m.BankId == SubUserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                               & m.BankId == SubUserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }

                                    _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();

                                    foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                    {

                                        CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                        & m.BankId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBrandId == CardBrand.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                    _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();
                                    foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                    {

                                        CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                 & m.BankId == SubUserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                        & m.BankId == SubUserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBankId == CardBank.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.Where(x => x.Purchase > 0).OrderByDescending(x => x.Purchase).ToList();
                                    #endregion


                                }

                                else
                                {
                                    #region Get Data
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {

                                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }

                                    _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.OtherRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                                    && m.TypeId == TransactionType.CardReward
                                                                                    && m.TypeId == TransactionType.CashReward
                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TransactionDate > _Request.StartTime
                                                                                    && m.TransactionDate < _Request.EndTime
                                                                                    && m.ModeId == TransactionMode.Credit
                                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }
                                    #endregion
                                    _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();

                                    foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                    {

                                        CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBrandId == CardBrand.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBrandId == CardBrand.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                    _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                        .Select(x => new OOverview.AppUserByCard
                                        {
                                            ReferenceId = x.Id,
                                            Name = x.Name
                                        }).ToList();
                                    foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                    {

                                        CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Select(x => x.AccountId).Distinct().Count();

                                        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.CardBankId == CardBank.ReferenceId
                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();

                                        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                         && m.TransactionDate > _Request.StartTime
                                                                         && m.TransactionDate < _Request.EndTime
                                                                         && m.CardBankId == CardBank.ReferenceId
                                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                    }
                                    _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();


                                }

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                #region Get Data
                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CardReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.OtherRewardTransactions > 0)
                                {
                                    _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                }
                                #endregion
                                _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                            .Select(x => new OOverview.AppUserByCard
                            {
                                ReferenceId = x.Id,
                                Name = x.Name
                            }).ToList();

                                foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                {

                                    CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Select(x => x.AccountId).Distinct().Count();

                                    CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();

                                    CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                                     && m.CardBrandId == CardBrand.ReferenceId
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                    .Select(x => new OOverview.AppUserByCard
                                    {
                                        ReferenceId = x.Id,
                                        Name = x.Name
                                    }).ToList();
                                foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                {

                                    CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBankId == CardBank.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Select(x => x.AccountId).Distinct().Count();

                                    CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBankId == CardBank.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();

                                    CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.TerminalId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                                     && m.CardBankId == CardBank.ReferenceId
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data
                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CardReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.OtherRewardTransactions > 0)
                                {
                                    _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                            && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.TypeId == TransactionType.CashReward
                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                }

                                _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                               .Select(x => new OOverview.AppUserByCard
                               {
                                   ReferenceId = x.Id,
                                   Name = x.Name
                               }).ToList();

                                foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                {

                                    CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Select(x => x.AccountId).Distinct().Count();

                                    CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();

                                    CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                                     && m.CardBrandId == CardBrand.ReferenceId
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                    .Select(x => new OOverview.AppUserByCard
                                    {
                                        ReferenceId = x.Id,
                                        Name = x.Name
                                    }).ToList();
                                foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                {

                                    CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBankId == CardBank.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Select(x => x.AccountId).Distinct().Count();

                                    CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBankId == CardBank.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();

                                    CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                                     && m.CardBankId == CardBank.ReferenceId
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();

                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion


                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Acquirer)
                            {


                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                  .Where(m =>
                                                   m.BankId == AccountDetails.UserAccountId
                                                   && m.Account.AccountTypeId == UserAccountType.Appuser
                                                   && m.TransactionDate > _Request.StartTime
                                                   && m.TransactionDate < _Request.EndTime
                                                   && (((m.ModeId == TransactionMode.Credit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Count();

                                if (_OOverviewResponse.Transactions > 0)
                                {
                                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m =>
                                                                           m.BankId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Count();
                                    if (_OOverviewResponse.CashRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.BankId == AccountDetails.UserAccountId
                                                                           && m.TypeId == TransactionType.CashReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > _Request.StartTime
                                                                           && m.TransactionDate < _Request.EndTime
                                                                          && m.ModeId == TransactionMode.Credit
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    // Card Reward
                                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.BankId == AccountDetails.UserAccountId
                                                                           && m.TypeId == TransactionType.CardReward
                                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                                           && m.TransactionDate > _Request.StartTime
                                                                           && m.TransactionDate < _Request.EndTime
                                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                           && m.ModeId == TransactionMode.Credit
                                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                                            ).Count();
                                    if (_OOverviewResponse.CardRewardTransactions > 0)
                                    {
                                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.BankId == AccountDetails.UserAccountId
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                                && m.TransactionDate > _Request.StartTime
                                                                                && m.TransactionDate < _Request.EndTime
                                                                                && m.ModeId == TransactionMode.Credit
                                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    }
                                }


                                #region Get Data


                                _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                    .Select(x => new OOverview.AppUserByCard
                                    {
                                        ReferenceId = x.Id,
                                        Name = x.Name
                                    }).ToList();

                                foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                                {

                                    CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.BankId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Select(x => x.AccountId).Distinct().Count();

                                    CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.BankId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.CardBrandId == CardBrand.ReferenceId
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();

                                    CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.BankId == AccountDetails.UserAccountId
                                                                     && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                                     && m.CardBrandId == CardBrand.ReferenceId
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                                //_OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                                //    .Select(x => new OOverview.AppUserByCard
                                //    {
                                //        ReferenceId = x.Id,
                                //        Name = x.Name
                                //    }).ToList();
                                //foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                                //{
                                //    CardBank.Users = _HCoreContext.HCUAccountTransaction
                                //                                .Where(m => m.BankId == AccountDetails.UserAccountId
                                //                                 && m.TransactionDate > _Request.StartTime
                                //                                 && m.TransactionDate < _Request.EndTime
                                //                                 && m.CardBankId == CardBank.ReferenceId
                                //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                                 && m.StatusId == HelperStatus.Transaction.Success
                                //                               ).Select(x => x.AccountId).Distinct().Count();
                                //    if (CardBank.Users > 0)
                                //    {
                                //        CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                //                           .Where(m => m.BankId == AccountDetails.UserAccountId
                                //                            && m.TransactionDate > _Request.StartTime
                                //                            && m.TransactionDate < _Request.EndTime
                                //                            && m.CardBankId == CardBank.ReferenceId
                                //                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                            && m.StatusId == HelperStatus.Transaction.Success
                                //                          ).Count();




                                //        CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                //                                        .Where(m => m.BankId == AccountDetails.UserAccountId
                                //                                         && m.TransactionDate > _Request.StartTime
                                //                                         && m.TransactionDate < _Request.EndTime
                                //                                         && m.CardBankId == CardBank.ReferenceId
                                //                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                //                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                //                                         && m.StatusId == HelperStatus.Transaction.Success
                                //                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                //    }
                                //}
                                //_OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.Where(x => x.Users > 0).OrderByDescending(x => x.Purchase).ToList();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {


                        #region Get Data
                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        if (_OOverviewResponse.CashRewardTransactions > 0)
                        {

                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }

                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.CardRewardTransactions > 0)
                        {
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }

                        _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.OtherRewardTransactions > 0)
                        {
                            _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        }
                        _OOverviewResponse.AppUsersCardType = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                 .Select(x => new OOverview.AppUserByCard
                                 {
                                     ReferenceId = x.Id,
                                     Name = x.Name
                                 }).ToList();

                        foreach (var CardBrand in _OOverviewResponse.AppUsersCardType)
                        {

                            CardBrand.Users = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.CardBrandId == CardBrand.ReferenceId
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Select(x => x.AccountId).Distinct().Count();

                            CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.CardBrandId == CardBrand.ReferenceId
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Count();

                            CardBrand.Purchase = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.CardBrandId == CardBrand.ReferenceId
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                        _OOverviewResponse.AppUsersCardType = _OOverviewResponse.AppUsersCardType.OrderByDescending(x => x.Purchase).ToList();

                        _OOverviewResponse.AppUsersBank = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank)
                            .Select(x => new OOverview.AppUserByCard
                            {
                                ReferenceId = x.Id,
                                Name = x.Name
                            }).ToList();
                        foreach (var CardBank in _OOverviewResponse.AppUsersBank)
                        {

                            CardBank.Users = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.CardBankId == CardBank.ReferenceId
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Select(x => x.AccountId).Distinct().Count();

                            CardBank.Transactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.CardBankId == CardBank.ReferenceId
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Count();

                            CardBank.Purchase = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.CardBankId == CardBank.ReferenceId
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                        _OOverviewResponse.AppUsersBank = _OOverviewResponse.AppUsersBank.OrderByDescending(x => x.Purchase).ToList();

                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the redeem overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (SubUserAccountId != 0)
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.TUC)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                         m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.TUC)
                                                                               .Count();

                                    #endregion
                                }
                                else
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.TUC)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.TUC)
                                                                               .Count();

                                    #endregion
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                if (SubUserAccountId != 0)
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.TUC)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.AccountId == SubUserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.TUC)
                                                                               .Count();

                                    #endregion
                                }
                                else
                                {
                                    #region Get     Data
                                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                    m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                  m.SourceId == TransactionSource.TUC)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.TUC)
                                                                               .Count();

                                    #endregion
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {


                                #region Get     Data
                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TerminalId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.TerminalId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TerminalId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data

                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                                #endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {
                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                      m.ModeId == Helpers.TransactionMode.Debit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                                   .Count();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the application users overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUsersOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {

                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.CreateDate > _Request.StartTime
                                && x.CreateDate < _Request.EndTime
                                && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));


                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.CreateDate > _Request.StartTime
                                && x.CreateDate < _Request.EndTime
                                && x.Gender.SystemName == "gender.male"
                                && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));

                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime
                               && x.Gender.SystemName == "gender.female"
                               && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));

                                _OOverviewResponse.AppUsersOther = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.CreateDate > _Request.StartTime
                                && x.CreateDate < _Request.EndTime
                                && x.GenderId == null
                                && (x.OwnerId == AccountDetails.UserAccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));

                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime
                               && x.OwnerId == AccountDetails.UserAccountId);


                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime
                               && x.GenderId == null
                               && (x.OwnerId != AccountDetails.UserAccountId && x.HCUAccountTransactionAccount.Any(m => m.ParentId == AccountDetails.UserAccountId)));


                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                            x.AccountTypeId == UserAccountType.Appuser
                            && x.CreateDate > _Request.StartTime
                            && x.CreateDate < _Request.EndTime
                            && x.HCUAccountTransactionAccount.Count(m => m.ParentId == AccountDetails.UserAccountId
                            && m.AccountId == x.Id) > 1)
                            .Distinct()
                            .Count();

                                _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                           x.AccountTypeId == UserAccountType.Appuser
                           && x.CreateDate > _Request.StartTime
                           && x.CreateDate < _Request.EndTime
                           && x.HCUAccountTransactionAccount.Count(m => m.ParentId == AccountDetails.UserAccountId
                           && m.AccountId == x.Id) == 1)
                           .Distinct()
                           .Count();

                                //_OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                //&& x.Account.AccountTypeId == UserAccountType.Appuser
                                //&& x.Account.CreateDate > _Request.StartTime
                                //&& x.Account.CreateDate < _Request.EndTime)
                                //.Select(x => x.AccountId)
                                //.Distinct()
                                //.Count();
                                //_OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                //&& x.Account.AccountTypeId == UserAccountType.Appuser
                                //&& x.Account.Gender.SystemName == "gender.male" && x.Account.CreateDate > _Request.StartTime && x.Account.CreateDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                //_OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.Account.CreateDate > _Request.StartTime && x.Account.CreateDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                //_OOverviewResponse.AppUsersOther = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.GenderId == null && x.Account.CreateDate > _Request.StartTime && x.Account.CreateDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();


                                // _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                                //x.ParentId == AccountDetails.UserAccountId
                                //&& x.Account.OwnerId == AccountDetails.UserAccountId
                                //&& x.Account.AccountTypeId == UserAccountType.Appuser
                                //&& x.Account.CreateDate > _Request.StartTime
                                //&& x.Account.CreateDate < _Request.EndTime)
                                //.Select(x => x.AccountId)
                                //.Distinct()
                                //.Count();

                                //_OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                                //x.ParentId == AccountDetails.UserAccountId
                                //&& x.Account.OwnerId != AccountDetails.UserAccountId
                                //&& x.Account.AccountTypeId == UserAccountType.Appuser
                                //&& x.Account.CreateDate > _Request.StartTime
                                //&& x.Account.CreateDate < _Request.EndTime)
                                //.Select(x => x.AccountId)
                                //.Distinct()
                                //.Count();





                                // _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                //x.AccountTypeId == UserAccountType.Appuser
                                //&& x.HCUAccountTransactionAccount.Count(m => m.ParentId == AccountDetails.UserAccountId
                                //&& m.AccountId == x.Id
                                //&& m.Account.CreateDate > _Request.StartTime
                                //&& m.Account.CreateDate < _Request.EndTime) == 1)
                                //.Distinct()
                                //.Count();

                                _OOverviewResponse.AppUsersByAgeGroup = new List<OOverview.AppUserAgeGroup>();
                                for (int i = 0; i < 3; i++)
                                {
                                    string RangeText = "";
                                    int StartRange = 0;
                                    int EndRange = 0;
                                    switch (i)
                                    {
                                        case 0:
                                            RangeText = "15-30";
                                            StartRange = 10;
                                            EndRange = 31;
                                            break;
                                        case 1:
                                            RangeText = "31-40";
                                            StartRange = 30;
                                            EndRange = 41;
                                            break;
                                        case 2:
                                            RangeText = "41-50";
                                            StartRange = 40;
                                            EndRange = 51;
                                            break;
                                        case 3:
                                            RangeText = "50+";
                                            StartRange = 50;
                                            EndRange = 120;
                                            break;
                                    }
                                    double PurchaseAgeGroup = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                             .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    long VisitsAgeGroup = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                           .Count();

                                    long UsersAgeGroup = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                   .Select(x => x.AccountId)
                                   .Distinct()
                                   .Count();
                                    _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                    {
                                        Name = RangeText,
                                        Purchase = PurchaseAgeGroup,
                                        Visits = VisitsAgeGroup,
                                        Users = UsersAgeGroup,
                                    });
                                }
                                double PurchaseAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                 .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                long VisitsAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Count();

                                long AgeGroupCount = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.Account.CreateDate > _Request.StartTime
                                             && x.Account.CreateDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Select(x => x.AccountId)
                               .Distinct()
                               .Count();
                                _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                {
                                    Name = "Unknown",
                                    Purchase = PurchaseAgeGroupUnknown,
                                    Visits = VisitsAgeGroupUnknown,
                                    Users = AgeGroupCount,
                                });
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                              && x.Account.AccountTypeId == UserAccountType.Appuser
                              && x.TransactionDate > _Request.StartTime
                              && x.TransactionDate < _Request.EndTime)
                              .Select(x => x.AccountId)
                              .Distinct()
                              .Count();
                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersOther = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.GenderId == null && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();


                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                               x.SubParentId == AccountDetails.UserAccountId
                               && x.Account.OwnerId == AccountDetails.UserAccountId
                               && x.Account.AccountTypeId == UserAccountType.Appuser
                               && x.TransactionDate > _Request.StartTime
                               && x.TransactionDate < _Request.EndTime)
                               .Select(x => x.AccountId)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                                x.SubParentId == AccountDetails.UserAccountId
                                && x.Account.OwnerId != AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();


                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                             x.AccountTypeId == UserAccountType.Appuser
                             && x.HCUAccountTransactionAccount.Count(m => m.SubParentId == AccountDetails.UserAccountId
                             && m.AccountId == x.Id
                             && m.TransactionDate > _Request.StartTime
                             && m.TransactionDate < _Request.EndTime) > 1)
                             .Distinct()
                             .Count();


                                _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.Appuser
                               && x.HCUAccountTransactionAccount.Count(m => m.SubParentId == AccountDetails.UserAccountId
                               && m.AccountId == x.Id
                               && m.TransactionDate > _Request.StartTime
                               && m.TransactionDate < _Request.EndTime) == 1)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.AppUsersByAgeGroup = new List<OOverview.AppUserAgeGroup>();
                                for (int i = 0; i < 5; i++)
                                {
                                    string RangeText = "";
                                    int StartRange = 0;
                                    int EndRange = 0;
                                    switch (i)
                                    {
                                        case 0:
                                            RangeText = "<20";
                                            StartRange = 1;
                                            EndRange = 21;
                                            break;
                                        case 1:
                                            RangeText = "21-30";
                                            StartRange = 20;
                                            EndRange = 31;
                                            break;
                                        case 2:
                                            RangeText = "31-40";
                                            StartRange = 30;
                                            EndRange = 41;
                                            break;
                                        case 3:
                                            RangeText = "41-50";
                                            StartRange = 40;
                                            EndRange = 51;
                                            break;
                                        case 4:
                                            RangeText = "51-60";
                                            StartRange = 50;
                                            EndRange = 61;
                                            break;
                                        case 5:
                                            RangeText = "60+";
                                            StartRange = 60;
                                            EndRange = 120;
                                            break;
                                    }
                                    double PurchaseAgeGroup = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                             .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    long VisitsAgeGroup = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                           .Count();

                                    long UsersAgeGroup = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                   .Select(x => x.AccountId)
                                   .Distinct()
                                   .Count();
                                    _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                    {
                                        Name = RangeText,
                                        Purchase = PurchaseAgeGroup,
                                        Visits = VisitsAgeGroup,
                                        Users = UsersAgeGroup,
                                    });
                                }
                                double PurchaseAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                 .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                long VisitsAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Count();

                                long AgeGroupCount = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Select(x => x.AccountId)
                               .Distinct()
                               .Count();
                                _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                {
                                    Name = "Unknown",
                                    Purchase = PurchaseAgeGroupUnknown,
                                    Visits = VisitsAgeGroupUnknown,
                                    Users = AgeGroupCount,
                                });
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                              && x.Account.AccountTypeId == UserAccountType.Appuser
                              && x.TransactionDate > _Request.StartTime
                              && x.TransactionDate < _Request.EndTime)
                              .Select(x => x.AccountId)
                              .Distinct()
                              .Count();
                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersOther = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.GenderId == null && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                               x.CreatedById == AccountDetails.UserAccountId
                               && x.Account.OwnerId == AccountDetails.UserAccountId
                               && x.Account.AccountTypeId == UserAccountType.Appuser
                               && x.TransactionDate > _Request.StartTime
                               && x.TransactionDate < _Request.EndTime)
                               .Select(x => x.AccountId)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                                x.CreatedById == AccountDetails.UserAccountId
                                && x.Account.OwnerId != AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();


                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                             x.AccountTypeId == UserAccountType.Appuser
                             && x.HCUAccountTransactionAccount.Count(m => m.CreatedById == AccountDetails.UserAccountId
                             && m.AccountId == x.Id
                             && m.TransactionDate > _Request.StartTime
                             && m.TransactionDate < _Request.EndTime) > 1)
                             .Distinct()
                             .Count();


                                _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.Appuser
                               && x.HCUAccountTransactionAccount.Count(m => m.CreatedById == AccountDetails.UserAccountId
                               && m.AccountId == x.Id
                               && m.TransactionDate > _Request.StartTime
                               && m.TransactionDate < _Request.EndTime) == 1)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.AppUsersByAgeGroup = new List<OOverview.AppUserAgeGroup>();
                                for (int i = 0; i < 5; i++)
                                {
                                    string RangeText = "";
                                    int StartRange = 0;
                                    int EndRange = 0;
                                    switch (i)
                                    {
                                        case 0:
                                            RangeText = "<20";
                                            StartRange = 1;
                                            EndRange = 21;
                                            break;
                                        case 1:
                                            RangeText = "21-30";
                                            StartRange = 20;
                                            EndRange = 31;
                                            break;
                                        case 2:
                                            RangeText = "31-40";
                                            StartRange = 30;
                                            EndRange = 41;
                                            break;
                                        case 3:
                                            RangeText = "41-50";
                                            StartRange = 40;
                                            EndRange = 51;
                                            break;
                                        case 4:
                                            RangeText = "51-60";
                                            StartRange = 50;
                                            EndRange = 61;
                                            break;
                                        case 5:
                                            RangeText = "60+";
                                            StartRange = 60;
                                            EndRange = 120;
                                            break;
                                    }
                                    double PurchaseAgeGroup = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.CreatedById == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                             .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                    long VisitsAgeGroup = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.CreatedById == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                           .Count();

                                    long UsersAgeGroup = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth != null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                             && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                   .Select(x => x.AccountId)
                                   .Distinct()
                                   .Count();
                                    _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                    {
                                        Name = RangeText,
                                        Purchase = PurchaseAgeGroup,
                                        Visits = VisitsAgeGroup,
                                        Users = UsersAgeGroup,
                                    });
                                }
                                double PurchaseAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                 .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                long VisitsAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Count();

                                long AgeGroupCount = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                                             && x.Account.AccountTypeId == UserAccountType.Appuser
                                             && x.Account.DateOfBirth == null
                                             && x.TransactionDate > _Request.StartTime
                                             && x.TransactionDate < _Request.EndTime
                                             && x.StatusId == HelperStatus.Transaction.Success
                                             && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                             || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                               .Select(x => x.AccountId)
                               .Distinct()
                               .Count();
                                _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                                {
                                    Name = "Unknown",
                                    Purchase = PurchaseAgeGroupUnknown,
                                    Visits = VisitsAgeGroupUnknown,
                                    Users = AgeGroupCount,
                                });
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {


                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.CreateDate > _Request.StartTime
                               && x.CreateDate < _Request.EndTime);

                        _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                              && x.Gender.SystemName == "gender.male"
                              && x.CreateDate > _Request.StartTime
                              && x.CreateDate < _Request.EndTime);

                        _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                             && x.Gender.SystemName == "gender.female"
                             && x.CreateDate > _Request.StartTime
                             && x.CreateDate < _Request.EndTime);

                        _OOverviewResponse.AppUsersOther = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.GenderId == null
                            && x.CreateDate > _Request.StartTime
                            && x.CreateDate < _Request.EndTime);


                        _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                             x.AccountTypeId == UserAccountType.Appuser
                             && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id
                             && m.TransactionDate > _Request.StartTime
                             && m.TransactionDate < _Request.EndTime) > 1)
                             .Distinct()
                             .Count();


                        _OOverviewResponse.UniqueAppUsers = _HCoreContext.HCUAccount.Where(x =>
                       x.AccountTypeId == UserAccountType.Appuser
                       && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id
                       && m.TransactionDate > _Request.StartTime
                       && m.TransactionDate < _Request.EndTime) == 1)
                       .Distinct()
                       .Count();

                        _OOverviewResponse.AppUsersByAgeGroup = new List<OOverview.AppUserAgeGroup>();
                        for (int i = 0; i < 5; i++)
                        {
                            string RangeText = "";
                            int StartRange = 0;
                            int EndRange = 0;
                            switch (i)
                            {
                                case 0:
                                    RangeText = "<20";
                                    StartRange = 1;
                                    EndRange = 21;
                                    break;
                                case 1:
                                    RangeText = "21-30";
                                    StartRange = 20;
                                    EndRange = 31;
                                    break;
                                case 2:
                                    RangeText = "31-40";
                                    StartRange = 30;
                                    EndRange = 41;
                                    break;
                                case 3:
                                    RangeText = "41-50";
                                    StartRange = 40;
                                    EndRange = 51;
                                    break;
                                case 4:
                                    RangeText = "51-60";
                                    StartRange = 50;
                                    EndRange = 61;
                                    break;
                                case 5:
                                    RangeText = "60+";
                                    StartRange = 60;
                                    EndRange = 120;
                                    break;
                            }
                            double PurchaseAgeGroup = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth != null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                     .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            long VisitsAgeGroup = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth != null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                                   .Count();

                            long UsersAgeGroup = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth != null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) > StartRange
                                     && (DateTime.Now.Year - x.Account.DateOfBirth.Value.Year) < EndRange
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                           .Select(x => x.AccountId)
                           .Distinct()
                           .Count();
                            _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                            {
                                Name = RangeText,
                                Purchase = PurchaseAgeGroup,
                                Visits = VisitsAgeGroup,
                                Users = UsersAgeGroup,
                            });
                        }
                        double PurchaseAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth == null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                         .Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        long VisitsAgeGroupUnknown = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth == null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                       .Count();

                        long AgeGroupCount = _HCoreContext.HCUAccountTransaction.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                     && x.Account.DateOfBirth == null
                                     && x.TransactionDate > _Request.StartTime
                                     && x.TransactionDate < _Request.EndTime
                                     && x.StatusId == HelperStatus.Transaction.Success
                                     && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC)
                                     || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus)))
                       .Select(x => x.AccountId)
                       .Distinct()
                       .Count();
                        _OOverviewResponse.AppUsersByAgeGroup.Add(new OOverview.AppUserAgeGroup
                        {
                            Name = "Unknown",
                            Purchase = PurchaseAgeGroupUnknown,
                            Visits = VisitsAgeGroupUnknown,
                            Users = AgeGroupCount,
                        });
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                        #endregion
                    }


                    //_OOverviewResponse.Transactions = _OOverviewResponse.RewardTransactions + _OOverviewResponse.RedeemTransactions;
                    //_OOverviewResponse.Balance = _OOverviewResponse.RewardAmount - _OOverviewResponse.RedeemAmount;

                    //_OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                    //x.UserAccountId == UserAccountId
                    //&& x.StatusId == HelperStatus.Transaction.Success
                    //&& (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                    //&& (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                    //&& x.SourceId == TransactionSource.TUC)
                    //.Select(x => new OOverview.MiniTransaction
                    //{
                    //    ReferenceId = x.Id,
                    //    TypeName = x.Type.Name,
                    //    InvoiceAmount = x.PurchaseAmount,
                    //    MerchantName = x.Parent.DisplayName,
                    //    RewardAmount = x.TotalAmount,
                    //    TransactionDate = x.TransactionDate
                    //})
                    //.FirstOrDefault();

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the position terminals status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPosTerminalsStatusOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                #region Get  Data
                                _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount
                                       .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                                 && x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new OOverview.PosOverview
                                                 {
                                                     DisplayName = x.DisplayName,
                                                     ReferenceKey = x.Guid,
                                                     ReferenceId = x.Id,
                                                     Terminals = _HCoreContext.TUCTerminal
                                                           .Count(a => a.ProviderId == x.Id
                                                               && a.AcquirerId == AccountDetails.UserAccountId),
                                                 }).ToList();

                                foreach (var PosOverview in _OOverviewResponse.PosOverview)
                                {
                                    PosOverview.ActiveTerminals = _HCoreContext.TUCTerminal
                                    .Count(a => a.StatusId == HelperStatus.Default.Active
                                        && a.AcquirerId == PosOverview.ReferenceId
                                        && a.ProviderId == AccountDetails.UserAccountId
                                        && a.HCUAccountTransaction.Count(m => m.ParentId == AccountDetails.UserAccountId && m.TransactionDate > _Request.StartTime &&
                                                                                   m.TransactionDate < _Request.EndTime) > 0);

                                    PosOverview.IdleTerminals = _HCoreContext.TUCTerminal
                                    .Count(a => a.StatusId == HelperStatus.Default.Active
                                        && a.AcquirerId == PosOverview.ReferenceId
                                        && a.ProviderId == AccountDetails.UserAccountId
                                        && a.HCUAccountTransaction.Count(m => m.ParentId == AccountDetails.UserAccountId && m.TransactionDate > _Request.StartTime &&
                                                                                   m.TransactionDate < _Request.EndTime) == 0);

                                    PosOverview.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId
                                   && a.ParentId == AccountDetails.UserAccountId).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                                    PosOverview.LastTransactionLocation = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId
                                   && a.ParentId == AccountDetails.UserAccountId).OrderByDescending(x => x.TransactionDate).Select(x => x.SubParent.DisplayName).FirstOrDefault();
                                }


                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            if (AccountDetails.AccountTypeId == UserAccountType.Acquirer)
                            {
                                #region Get  Data
                                _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount
                                       .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                                 && x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new OOverview.PosOverview
                                                 {
                                                     DisplayName = x.DisplayName,
                                                     ReferenceKey = x.Guid,
                                                     ReferenceId = x.Id,
                                                     Terminals = _HCoreContext.HCUAccountOwner
                                                           .Count(a => a.Account.StatusId == HelperStatus.Default.Active
                                                               && a.Account.OwnerId == x.Id
                                                               && a.OwnerId == AccountDetails.UserAccountId),
                                                 }).ToList();

                                foreach (var PosOverview in _OOverviewResponse.PosOverview)
                                {
                                    PosOverview.ActiveTerminals = _HCoreContext.TUCTerminal
                                    .Count(a => a.StatusId == HelperStatus.Default.Active
                                        && a.MerchantId == PosOverview.ReferenceId
                                        && a.ProviderId == AccountDetails.UserAccountId
                                        && a.HCUAccountTransaction.Count(m => m.TransactionDate > _Request.StartTime &&
                                                                                   m.TransactionDate < _Request.EndTime) > 0);

                                    PosOverview.IdleTerminals = _HCoreContext.TUCTerminal
                                    .Count(a => a.StatusId == HelperStatus.Default.Active
                                        && a.MerchantId == PosOverview.ReferenceId
                                        && a.ProviderId == AccountDetails.UserAccountId
                                        && a.HCUAccountTransaction.Count(m => m.TransactionDate > _Request.StartTime &&
                                                                                   m.TransactionDate < _Request.EndTime) == 0);

                                    PosOverview.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId)
                                    .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                                    PosOverview.LastTransactionLocation = _HCoreContext.HCUAccountTransaction.Where(a => a.CreatedBy.OwnerId == PosOverview.ReferenceId)
                                    .OrderByDescending(x => x.TransactionDate).Select(x => x.SubParent.DisplayName).FirstOrDefault();
                                }


                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }
                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        //internal OResponse GetPosTerminalsOverviewList(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        List<OAccount.Terminal> Data = new List<OAccount.Terminal>();
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long UserAccountId = 0;
        //            if (!string.IsNullOrEmpty(_Request.ReferenceKey))
        //            {
        //                UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();
        //            }
        //            switch (_Request.Type)
        //            {
        //                case ThankUCashConstant.ListType.All:
        //                    #region Get Data
        //                    Data = (from x in _HCoreContext.HCUAccount
        //                            where x.AccountTypeId == UserAccountType.TerminalAccount && x.StatusId == HelperStatus.Default.Active
        //                            && x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore && n.Owner.StatusId == HelperStatus.Default.Active).Count() > 0
        //                            select new OAccount.Terminal
        //                            {
        //                                ReferenceKey = x.Guid,
        //                                DisplayName = x.DisplayName,
        //                                OwnerDisplayName = x.Owner.DisplayName,
        //                                MerchantDisplayName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                AcquirerKey = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Acquirer).Select(n => n.Owner.Guid).FirstOrDefault(),
        //                                StoreName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                Transactions = _HCoreContext.HCUAccountTransaction
        //                                                    .Count(m =>
        //                                                           m.CreatedById == x.Id
        //                                                           && m.TransactionDate > _Request.StartDate
        //                                                           && m.TransactionDate < _Request.EndDate
        //                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success),
        //                                LastTransactionDate = _HCoreContext.HCUAccountTransaction
        //                                                    .Where(m => m.CreatedById == x.Id && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault().ToString(),
        //                            })
        //                            .OrderBy(_Request.SortExpression)
        //                            .ToList();
        //                    #endregion
        //                    break;

        //                case ThankUCashConstant.ListType.Owner:
        //                    #region Get Data
        //                    Data = (from x in _HCoreContext.HCUAccount
        //                            where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.TerminalAccount
        //                                    && x.OwnerId == UserAccountId && x.StatusId == HelperStatus.Default.Active
        //                            select new OAccount.Terminal
        //                            {
        //                                ReferenceKey = x.Guid,
        //                                DisplayName = x.DisplayName,
        //                                MerchantDisplayName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                AcquirerDisplayName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Acquirer).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                StoreName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                Transactions = _HCoreContext.HCUAccountTransaction
        //                                                    .Count(m =>
        //                                                           m.CreatedById == x.Id
        //                                                           && m.TransactionDate > _Request.StartDate
        //                                                           && m.TransactionDate < _Request.EndDate
        //                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success),
        //                                LastTransactionDate = _HCoreContext.HCUAccountTransaction
        //                                                    .Where(m => m.CreatedById == x.Id
        //                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault().ToString(),
        //                            })
        //                            .OrderBy(_Request.SortExpression)
        //                            .ToList();
        //                    #endregion
        //                    break;
        //                case ThankUCashConstant.ListType.SubOwner:
        //                    #region Get Data
        //                    Data = (from x in _HCoreContext.HCUAccount
        //                            where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.TerminalAccount
        //                                    && x.StatusId == HelperStatus.Default.Active
        //                                    && x.HCUAccountOwnerAccount.Any(m => m.OwnerId == UserAccountId && m.Owner.StatusId == HelperStatus.Default.Active && m.AccountTypeId == Helpers.UserAccountType.TerminalAccount)
        //                            select new OAccount.Terminal
        //                            {
        //                                ReferenceKey = x.Guid,
        //                                DisplayName = x.DisplayName,
        //                                OwnerKey = x.Owner.Guid,
        //                                OwnerDisplayName = x.Owner.DisplayName,
        //                                MerchantKey = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant).Select(n => n.Owner.Guid).FirstOrDefault(),
        //                                MerchantDisplayName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                AcquirerKey = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Acquirer).Select(n => n.Owner.Guid).FirstOrDefault(),
        //                                AcquirerDisplayName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Acquirer).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                StoreName = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore).Select(n => n.Owner.DisplayName).FirstOrDefault(),
        //                                StoreKey = x.HCUAccountOwnerAccount.Where(n => n.Owner.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore).Select(n => n.Owner.Guid).FirstOrDefault(),
        //                                Transactions = _HCoreContext.HCUAccountTransaction
        //                                                    .Count(m =>
        //                                                           m.CreatedById == x.Id
        //                                                           && m.TransactionDate > _Request.StartDate
        //                                                           && m.TransactionDate < _Request.EndDate
        //                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success),
        //                                LastTransactionDate = _HCoreContext.HCUAccountTransaction
        //                                                    .Where(m => m.CreatedById == x.Id && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
        //                                                           && m.StatusId == HelperStatus.Transaction.Success).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault().ToString(),

        //                            })
        //                                             .Where(_Request.SearchCondition)
        //                                             .OrderBy(_Request.SortExpression)
        //                                             .ToList();
        //                    #endregion
        //                    break;
        //                default:
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
        //            }
        //            #region Create  Response Object
        //            if (UserAccountId != 0)
        //            {
        //                int ActiveTerminals = _HCoreContext.HCUAccountOwner
        //                  .Count(a => a.AccountTypeId == UserAccountType.TerminalAccount && a.Account.StatusId == HelperStatus.Default.Active
        //                      && a.OwnerId == UserAccountId
        //                      && a.Account.HCUAccountTransactionCreatedBy.Count(m => m.TransactionDate > _Request.StartDate &&
        //                                                                 m.TransactionDate < _Request.EndDate) > 0);

        //                int IdleTerminals = _HCoreContext.HCUAccountOwner
        //                .Count(a => a.AccountTypeId == UserAccountType.TerminalAccount && a.Account.StatusId == HelperStatus.Default.Active
        //                    && a.OwnerId == UserAccountId
        //                    && a.Account.HCUAccountTransactionCreatedBy.Count(m => m.TransactionDate > _Request.StartDate &&
        //                                                               m.TransactionDate < _Request.EndDate) == 0);
        //                _HCoreContext.Dispose();
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, ActiveTerminals, IdleTerminals);
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion
        //            }
        //            else
        //            {
        //                int ActiveTerminals = _HCoreContext.HCUAccount
        //              .Count(a => a.AccountTypeId == UserAccountType.TerminalAccount && a.StatusId == HelperStatus.Default.Active
        //                  && a.HCUAccountTransactionCreatedBy.Count(m => m.TransactionDate > _Request.StartDate &&
        //                                                             m.TransactionDate < _Request.EndDate) > 0);

        //                int IdleTerminals = _HCoreContext.HCUAccount
        //                 .Count(a => a.AccountTypeId == UserAccountType.TerminalAccount && a.StatusId == HelperStatus.Default.Active
        //                     && a.HCUAccountTransactionCreatedBy.Count(m => m.TransactionDate > _Request.StartDate &&
        //                                                                m.TransactionDate < _Request.EndDate) == 0);
        //                _HCoreContext.Dispose();
        //                OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, ActiveTerminals, IdleTerminals);
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //                #endregion

        //            }

        //            #endregion


        //        }

        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetAppUsers", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}


        /// <summary>
        /// Description: Gets the application user overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUserOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    //long UserId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.UserId).FirstOrDefault();
                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.AccountId == UserAccountId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                  m.SourceId == TransactionSource.TUC)
                                                                .Sum(m => (double?)m.TotalAmount) ?? 0;
                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.AccountId == UserAccountId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                  m.SourceId == TransactionSource.TUC)
                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == UserAccountId &&
                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Count();



                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == UserAccountId &&
                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0;
                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.AccountId == UserAccountId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                  m.SourceId == TransactionSource.TUC)
                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == UserAccountId &&
                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Count();
                    _OOverviewResponse.Transactions = _OOverviewResponse.RewardTransactions + _OOverviewResponse.RedeemTransactions;
                    _OOverviewResponse.Balance = _OOverviewResponse.RewardAmount - _OOverviewResponse.RedeemAmount;

                    _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                x.AccountId == UserAccountId
                                                                && x.StatusId == HelperStatus.Transaction.Success
                                                                && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                                && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                                && x.SourceId == TransactionSource.TUC)
                                                                .Select(x => new OOverview.MiniTransaction
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    TypeName = x.Type.Name,
                                                                    InvoiceAmount = x.PurchaseAmount,
                                                                    MerchantName = x.Parent.DisplayName,
                                                                    RewardAmount = x.TotalAmount,
                                                                    TransactionDate = x.TransactionDate
                                                                })
                                                                .FirstOrDefault();

                }

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the merchant mini overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantMiniOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                    // App Users
                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();


                    _OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    _OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                    //.Where(m => m.ParentId == UserAccountId &&
                    //  m.StatusId == HelperStatus.Transaction.Success &&
                    //  m.Type.SubParentId == TransactionTypeCategory.Reward &&
                    //  m.ModeId == Helpers.TransactionMode.Credit &&
                    //   m.TransactionDate > _Request.StartTime &&
                    //m.TransactionDate < _Request.EndTime &&
                    //m.SourceId == Helpers.TransactionSource.TUC
                    //).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                    //_OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                    //.Where(m => m.ParentId == UserAccountId &&
                    //  m.StatusId == HelperStatus.Transaction.Success &&
                    //  m.Type.SubParentId == TransactionTypeCategory.Reward &&
                    //  m.ModeId == Helpers.TransactionMode.Credit &&
                    //   m.TransactionDate > _Request.StartTime &&
                    //m.TransactionDate < _Request.EndTime &&
                    //m.SourceId == Helpers.TransactionSource.TUC
                    //).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == UserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();



                    //_OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                            .Where(m => m.ParentId == UserAccountId &&
                    //                                              m.StatusId == HelperStatus.Transaction.Success &&
                    //                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                    //                                              m.ModeId == Helpers.TransactionMode.Debit &&
                    //                                               m.TransactionDate > _Request.StartTime &&
                    //                                            m.TransactionDate < _Request.EndTime &&
                    //                                              m.SourceId == TransactionSource.TUC
                    //                                              )
                    //                                            .Sum(m => (double?)m.TotalAmount) ?? 0;
                    //_OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                    //.Where(m => m.ParentId == UserAccountId &&
                    //  m.StatusId == HelperStatus.Transaction.Success &&
                    //  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                    //  m.ModeId == Helpers.TransactionMode.Debit &&
                    //   m.TransactionDate > _Request.StartTime &&
                    //m.TransactionDate < _Request.EndTime &&
                    //  m.SourceId == TransactionSource.TUC
                    //  )
                    //.Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == UserAccountId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                m.TransactionDate < _Request.EndTime &&
                                                                  m.SourceId == TransactionSource.TUC
                                                                  ).Count();
                    //_OOverviewResponse.Transactions = _OOverviewResponse.RewardTransactions + _OOverviewResponse.RedeemTransactions;
                    //_OOverviewResponse.PurchaseAmount = _OOverviewResponse.RewardPurchaseAmount + _OOverviewResponse.RedeemPurchaseAmount;
                }

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _Request.UserAccountKey));
            try
            {
                if (_OOverviewResponse.ThankUCashPlus == 1)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        //_OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId
                        //&& x.Account.AccountTypeId == UserAccountType.Appuser
                        //&& x.TransactionDate > _Request.StartTime
                        //&& x.TransactionDate < _Request.EndTime)
                        //.Select(x => x.AccountId)
                        //.Distinct()
                        //.Count();

                        //_OOverviewResponse.AppUsersPercentage = 100;
                        //_OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccount.Where(x =>
                        //x.OwnerId == UserAccountId
                        //&& x.AccountTypeId == UserAccountType.Appuser
                        //&& x.CreateDate > _Request.StartTime
                        //&& x.CreateDate < _Request.EndTime)
                        //.Count();

                        //_OOverviewResponse.OwnAppUsersPercentage = 100;
                        //_OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                        //x.ParentId == UserAccountId
                        //&& x.Account.OwnerId != UserAccountId
                        //&& x.Account.AccountTypeId == UserAccountType.Appuser
                        //&& x.TransactionDate > _Request.StartTime
                        //&& x.TransactionDate < _Request.EndTime)
                        //.Select(x => x.AccountId)
                        //.Distinct()
                        //.Count();

                        //_OOverviewResponse.ReferralAppUsersPercentage = 100;
                        //_OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                        //x.AccountTypeId == UserAccountType.Appuser
                        //&& x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                        //&& m.AccountId == x.Id
                        //&& m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime)
                        //.Count() > 1)
                        //.Distinct()
                        //.Count();
                        //_OOverviewResponse.RepeatingAppUsersPercentage = 100;

                        //_OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                        //&& x.AccountTypeId == Helpers.UserAccountType.Appuser
                        //&& x.CreateDate > _Request.StartTime
                        //&& x.CreateDate < _Request.EndTime)
                        //.OrderByDescending(x => x.CreateDate)
                        //.Select(x => x.CreateDate)
                        //.FirstOrDefault();

                        //_OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                        //_OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                        ////_OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.Terminals = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.TerminalAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.RewardCardsUsed = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.StatusId != TUStatusHelper.Card.NotAssigned && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //// Transactions 
                        //_OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                        //.Where(m => m.ParentId == UserAccountId
                        //&& m.Account.AccountTypeId == UserAccountType.Appuser
                        //&& m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime
                        //&& m.ModeId == TransactionMode.Credit
                        //&& m.SourceId == TransactionSource.ThankUCashPlus
                        //&& m.StatusId == HelperStatus.Transaction.Success
                        //).Count();


                        //_OOverviewResponse.RepeatingTransactionsPercentage = 100;
                        //_OOverviewResponse.ReferralTransactions = _HCoreContext.HCUAccountTransaction
                        //.Where(m => m.ParentId == UserAccountId
                        //&& m.Account.OwnerId != UserAccountId
                        //&& m.Account.AccountTypeId == UserAccountType.Appuser
                        //&& m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime
                        //&& m.SourceId == TransactionSource.TUC
                        //&& m.StatusId == HelperStatus.Transaction.Success
                        //).Count();
                        _OOverviewResponse.ReferralTransactionsPercentage = 100;
                        // Purchase AMOUNT
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                            && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.PurchaseAmountPercentage = 100;
                        //_OOverviewResponse.NewPurchaseAmount = _HCoreContext.HCUAccountTransaction
                        //.Where(m => m.ParentId == UserAccountId
                        //&& m.Account.OwnerId == UserAccountId
                        // && m.Account.AccountTypeId == UserAccountType.Appuser
                        // && m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime
                        //&& m.SourceId == TransactionSource.TUC
                        //&& m.StatusId == HelperStatus.Transaction.Success
                        //).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.NewPurchaseAmountPercentage = 100;
                        //_OOverviewResponse.RepeatingPurchaseAmount = _HCoreContext.HCUAccount.Where(x =>
                        //x.AccountTypeId == UserAccountType.Appuser
                        //&& x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                        //&& m.AccountId == x.Id
                        //&& m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime).Count() > 0)
                        //.Count();
                        _OOverviewResponse.RepeatingPurchaseAmountPercentage = 100;
                        //_OOverviewResponse.ReferralPurchaseAmount = _HCoreContext.HCUAccountTransaction
                        //.Where(m => m.ParentId == UserAccountId
                        //&& m.Account.OwnerId != UserAccountId
                        //  && m.Account.AccountTypeId == UserAccountType.Appuser
                        // && m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime
                        //&& m.SourceId == TransactionSource.TUC
                        //&& m.StatusId == HelperStatus.Transaction.Success
                        //).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.ReferralPurchaseAmountPercentage = 100;


                        // Cash Reward
                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.TypeId == TransactionType.CashReward
                                                           && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        List<OOverview.AcquirerCollection> Acquirers = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OOverview.AcquirerCollection
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                            PurchaseAmount = 0,
                            Terminals = 0,
                        }).ToList();

                        var Terminals = _HCoreContext.TUCTerminal
                           .Where(x => x.ProviderId == UserAccountId)
                           .Select(x => x.Id)
                           .ToList();
                        foreach (var TerminalId in Terminals)
                        {
                            long TerminalAcquirer = _HCoreContext.TUCTerminal
                         .Where(x => x.Id == TerminalId)
                         .Select(x => (long)x.AcquirerId).FirstOrDefault();

                            double PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.CreatedById == TerminalId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            if (Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer) != null)
                            {
                                Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount + PurchaseAmount;
                                Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals + 1;
                            }
                        }
                        _OOverviewResponse.AcquirerAmountDistribution = Acquirers.Where(x => x.PurchaseAmount > 0).ToList();



                        // Card Reward
                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        // Reward Amount
                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                      x.ParentId == UserAccountId &&
                                                                       x.TransactionDate > _Request.StartTime &&
                                                                   x.TransactionDate < _Request.EndTime
                                                                   && x.StatusId == HelperStatus.Transaction.Success
                                                                   && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                                   && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                           && x.SourceId == TransactionSource.ThankUCashPlus)
                                                                   .Select(x => new OOverview.MiniTransaction
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       TypeName = x.Type.Name,
                                                                       InvoiceAmount = x.PurchaseAmount,
                                                                       MerchantName = x.Parent.DisplayName,
                                                                       RewardAmount = x.TotalAmount,
                                                                       TransactionDate = x.TransactionDate
                                                                   }).OrderByDescending(x => x.TransactionDate)
                                                                   .FirstOrDefault();

                        _OOverviewResponse.LastCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.SourceId == TransactionSource.Settlement &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime
                                                                     ).OrderByDescending(m => m.TransactionDate)
                                                                   .Select(m => m.TransactionDate).FirstOrDefault();
                        _OOverviewResponse.SettlementCredit = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.Settlement
                                                                      ).Sum(m => (double?)m.TotalAmount) ?? 0;


                        _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.LastIssuerCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Select(m => m.TransactionDate).FirstOrDefault();


                        _OOverviewResponse.SettlementDebit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        //_OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                        //                                      .Where(m => m.ParentId == UserAccountId
                        //                                       && m.Type.SubParentId == TransactionTypeCategory.Reward
                        //                                       && m.ModeId == Helpers.TransactionMode.Credit
                        //                                       && m.TransactionDate > _Request.StartTime
                        //                                       && m.TransactionDate < _Request.EndTime
                        //                                       && m.SourceId == TransactionSource.TUC
                        //                                       && m.StatusId == HelperStatus.Transaction.Success
                        //                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        //_OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                        //.Where(m => m.ParentId == UserAccountId
                        //&& m.Type.SubParentId == TransactionTypeCategory.Reward
                        //&& m.ModeId == Helpers.TransactionMode.Credit
                        //&& m.TransactionDate > _Request.StartTime
                        //&& m.TransactionDate < _Request.EndTime
                        //&& m.SourceId == TransactionSource.ThankUCashPlus
                        //&& m.StatusId == HelperStatus.Transaction.Success
                        //).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                        _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == UserAccountId
                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.SourceId == TransactionSource.ThankUCashPlus
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;
                        _OOverviewResponse.ClaimedReward = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.ParentId == UserAccountId
                                                             && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                             && m.ModeId == Helpers.TransactionMode.Debit
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.SourceId == TransactionSource.ThankUCashPlus
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", UserAccountId));
                        if (_OOverviewResponse.ThankUCashPlus > 0)
                        {
                            //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", UserAccountId));
                        }
                    }

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId
                        && x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.TransactionDate > _Request.StartTime
                        && x.TransactionDate < _Request.EndTime)
                        .Select(x => x.AccountId)
                        .Distinct()
                        .Count();

                        _OOverviewResponse.AppUsersPercentage = 100;
                        _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccount.Where(x =>
                        x.OwnerId == UserAccountId
                        && x.AccountTypeId == UserAccountType.Appuser
                        && x.CreateDate > _Request.StartTime
                        && x.CreateDate < _Request.EndTime)
                        .Count();

                        _OOverviewResponse.OwnAppUsersPercentage = 100;
                        _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                        x.ParentId == UserAccountId
                        && x.Account.OwnerId != UserAccountId
                        && x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.TransactionDate > _Request.StartTime
                        && x.TransactionDate < _Request.EndTime)
                        .Select(x => x.AccountId)
                        .Distinct()
                        .Count();

                        _OOverviewResponse.ReferralAppUsersPercentage = 100;
                        _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime)
                        .Count() > 1)
                        .Distinct()
                        .Count();
                        _OOverviewResponse.RepeatingAppUsersPercentage = 100;

                        _OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                        && x.AccountTypeId == Helpers.UserAccountType.Appuser
                        && x.CreateDate > _Request.StartTime
                        && x.CreateDate < _Request.EndTime)
                        .OrderByDescending(x => x.CreateDate)
                        .Select(x => x.CreateDate)
                        .FirstOrDefault();

                        _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                        _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                        _OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.Terminals = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.RewardCardsUsed = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.StatusId != TUStatusHelper.Card.NotAssigned && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();


                        // Transactions 
                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                        _OOverviewResponse.TransactionsPercentage = 100;

                        _OOverviewResponse.NewTransactions = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime)
                        .Count() == 1)
                        .Count();

                        _OOverviewResponse.NewTransactionsPercentage = 100;

                        _OOverviewResponse.RepeatingTransactions = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m =>
                        m.ParentId == UserAccountId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime)
                        .Count() > 1)
                        .Count();
                        _OOverviewResponse.RepeatingTransactionsPercentage = 100;


                        _OOverviewResponse.ReferralTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.Account.OwnerId != UserAccountId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();
                        _OOverviewResponse.ReferralTransactionsPercentage = 100;



                        // Purchase AMOUNT
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.PurchaseAmountPercentage = 100;

                        _OOverviewResponse.NewPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                          && m.Account.OwnerId == UserAccountId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.NewPurchaseAmountPercentage = 100;

                        _OOverviewResponse.RepeatingPurchaseAmount = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime).Count() > 0)
                        .Count();
                        _OOverviewResponse.RepeatingPurchaseAmountPercentage = 100;


                        _OOverviewResponse.ReferralPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.Account.OwnerId != UserAccountId
                                                              && m.Account.AccountTypeId == UserAccountType.Appuser
                                                             && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.ReferralPurchaseAmountPercentage = 100;


                        // Cash Reward
                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        // Card Reward
                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        List<OOverview.AcquirerCollection> Acquirers = _HCoreContext.HCUAccount
                  .Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.StatusId == HelperStatus.Default.Active)
                  .Select(x => new OOverview.AcquirerCollection
                  {
                      ReferenceId = x.Id,
                      ReferenceKey = x.Guid,
                      DisplayName = x.DisplayName,
                      IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                      PurchaseAmount = 0,
                      Terminals = 0,
                  }).ToList();

                        var Terminals = _HCoreContext.TUCTerminal
                           .Where(x => x.ProviderId == UserAccountId)
                           .Select(x => x.Id)
                           .ToList();
                        foreach (var TerminalId in Terminals)
                        {
                            long TerminalAcquirer = _HCoreContext.TUCTerminal
                         .Where(x => x.Id == TerminalId)
                         .Select(x => (long)x.AcquirerId).FirstOrDefault();

                            double PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.CreatedById == TerminalId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            if (Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer) != null)
                            {
                                Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount + PurchaseAmount;
                                Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals + 1;
                            }
                        }
                        _OOverviewResponse.AcquirerAmountDistribution = Acquirers.Where(x => x.PurchaseAmount > 0).ToList();
                        // Reward Amount
                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                      x.ParentId == UserAccountId &&
                                                                       x.TransactionDate > _Request.StartTime &&
                                                                   x.TransactionDate < _Request.EndTime
                                                                   && x.StatusId == HelperStatus.Transaction.Success
                                                                   && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                                   && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                                   && x.SourceId == TransactionSource.TUC)
                                                                   .Select(x => new OOverview.MiniTransaction
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       TypeName = x.Type.Name,
                                                                       InvoiceAmount = x.PurchaseAmount,
                                                                       MerchantName = x.Parent.DisplayName,
                                                                       RewardAmount = x.TotalAmount,
                                                                       TransactionDate = x.TransactionDate
                                                                   }).OrderByDescending(x => x.TransactionDate)
                                                                   .FirstOrDefault();

                        _OOverviewResponse.LastCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.SourceId == TransactionSource.Settlement &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime
                                                                     ).OrderByDescending(m => m.TransactionDate)
                                                                   .Select(m => m.TransactionDate).FirstOrDefault();
                        _OOverviewResponse.SettlementCredit = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.Settlement
                                                                      ).Sum(m => (double?)m.TotalAmount) ?? 0;


                        _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.LastIssuerCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Select(m => m.TransactionDate).FirstOrDefault();


                        _OOverviewResponse.SettlementDebit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == UserAccountId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", UserAccountId));
                        if (_OOverviewResponse.ThankUCashPlus > 0)
                        {
                            //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", UserAccountId));
                        }
                    }

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the merchant overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantOverviewLite(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            _OOverviewResponse.AppUsers = 0;
            _OOverviewResponse.Transactions = 0;
            _OOverviewResponse.PurchaseAmount = 0;
            _OOverviewResponse.RewardTransactions = 0;
            _OOverviewResponse.RewardAmount = 0;
            _OOverviewResponse.RewardPurchaseAmount = 0;
            _OOverviewResponse.CashRewardTransactions = 0;
            _OOverviewResponse.CashRewardAmount = 0;
            _OOverviewResponse.CashRewardPurchaseAmount = 0;
            _OOverviewResponse.CardRewardTransactions = 0;
            _OOverviewResponse.CardRewardAmount = 0;
            _OOverviewResponse.CardRewardPurchaseAmount = 0;
            _OOverviewResponse.RedeemTransactions = 0;
            _OOverviewResponse.RedeemAmount = 0;
            _OOverviewResponse.RedeemPurchaseAmount = 0;
            _OOverviewResponse.Charge = 0;
            _OOverviewResponse.ClaimedRewardTransations = 0;
            _OOverviewResponse.ClaimedReward = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();

                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                   && x.AccountTypeId == UserAccountType.Appuser
                   && x.CreateDate > _Request.StartTime
                   && x.CreateDate < _Request.EndTime)
                   .Count();

                    _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                     x.AccountTypeId == UserAccountType.Appuser
                     && x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                     && m.AccountId == x.Id
                     && m.TransactionDate > _Request.StartTime
                     && m.TransactionDate < _Request.EndTime)
                     .Count() > 1)
                     .Distinct()
                     .Count();

                    _OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                  && x.AccountTypeId == UserAccountType.Appuser
                  && x.CreateDate > _Request.StartTime
                  && x.CreateDate < _Request.EndTime)
                  .OrderByDescending(x => x.CreateDate)
                    .Select(x => x.CreateDate)
                    .FirstOrDefault();

                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ParentId == UserAccountId
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count();
                    if (_OOverviewResponse.Transactions > 0)
                    {
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    }
                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.ParentId == UserAccountId
                                                      && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                      && m.ModeId == Helpers.TransactionMode.Credit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Count();

                    // Reward Amount
                    if (_OOverviewResponse.RewardTransactions > 0)
                    {


                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == UserAccountId
                                                               && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        if (_OOverviewResponse.CashRewardTransactions > 0)
                        {

                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.ParentId == UserAccountId
                                                    && m.TypeId == TransactionType.CashReward
                                                    && m.ModeId == TransactionMode.Credit
                                                     && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                    && m.TransactionDate > _Request.StartTime
                                                    && m.TransactionDate < _Request.EndTime
                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == UserAccountId
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }


                        // Card Reward
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == UserAccountId
                                                               && m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.ModeId == TransactionMode.Credit
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.CardRewardTransactions > 0)
                        {
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == UserAccountId
                                                               && m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                             && m.ModeId == TransactionMode.Credit
                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == UserAccountId
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }
                    }



                    // Redeem
                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                           && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           && m.ModeId == Helpers.TransactionMode.Debit
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();
                    if (_OOverviewResponse.RedeemTransactions > 0)
                    {
                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == UserAccountId
                                                               && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                               && m.ModeId == Helpers.TransactionMode.Debit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && m.SourceId == TransactionSource.TUC
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == UserAccountId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    }

                    _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                  x.ParentId == UserAccountId &&
                                                                   x.TransactionDate > _Request.StartTime &&
                                                               x.TransactionDate < _Request.EndTime
                                                               && x.StatusId == HelperStatus.Transaction.Success
                                                               && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                               && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                               && x.SourceId == TransactionSource.ThankUCashPlus)
                                                               .Select(x => new OOverview.MiniTransaction
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   TypeName = x.Type.Name,
                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   MerchantName = x.Parent.DisplayName,
                                                                   RewardAmount = x.TotalAmount,
                                                                   TransactionDate = x.TransactionDate
                                                               }).OrderByDescending(x => x.TransactionDate)
                                                               .FirstOrDefault();
                    // TUC PLUS
                    _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == UserAccountId
                                                          && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.SourceId == TransactionSource.ThankUCashPlus
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                    _OOverviewResponse.ClaimedRewardTransations = _HCoreContext.HCUAccountTransaction
                                                      .Count(m => m.ParentId == UserAccountId
                                                       && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                       && m.ModeId == Helpers.TransactionMode.Debit
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && m.SourceId == TransactionSource.ThankUCashPlus
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        );
                    if (_OOverviewResponse.ClaimedRewardTransations > 0)
                    {
                        _OOverviewResponse.ClaimedReward = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.ParentId == UserAccountId
                                                      && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                      && m.ModeId == Helpers.TransactionMode.Debit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.SourceId == TransactionSource.ThankUCashPlus
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Sum(m => (double?)m.TotalAmount) ?? 0;
                    }

                    _OOverviewResponse.StoresOverview = _HCoreContext.HCUAccount.Where(x =>
                    x.AccountTypeId == UserAccountType.MerchantStore
                    && x.OwnerId == UserAccountId)
                    .Select(x => new OOverview.StoresOverview
                    {
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        DisplayName = x.DisplayName,
                        Address = x.Address,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                    }).ToList();
                    foreach (var StoreInfo in _OOverviewResponse.StoresOverview)
                    {
                        StoreInfo.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == UserAccountId
                                                          && m.SubParentId == StoreInfo.ReferenceId
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        StoreInfo.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Count(m => m.ParentId == UserAccountId
                                                         && m.SubParentId == StoreInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           );

                        StoreInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == UserAccountId
                                                         && m.SubParentId == StoreInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                    }
                    _OOverviewResponse.StoresOverview = _OOverviewResponse.StoresOverview.OrderByDescending(x => x.Transactions).ToList();

                    _OOverviewResponse.TerminalsOverview = _HCoreContext.HCUAccountOwner.Where(x =>
                  x.AccountTypeId == UserAccountType.Terminal
                  && x.OwnerId == UserAccountId)
                  .Select(x => new OOverview.TerminalsOverview
                  {
                      ReferenceId = x.AccountId,
                      ReferenceKey = x.Guid,
                      DisplayName = x.Account.DisplayName,
                      PtspName = x.Account.Owner.DisplayName,
                  }).ToList();
                    foreach (var TerminalInfo in _OOverviewResponse.TerminalsOverview)
                    {
                        TerminalInfo.StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x =>
                        x.AccountId == TerminalInfo.ReferenceId
                        && x.Owner.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => x.Owner.DisplayName)
                        .FirstOrDefault();

                        TerminalInfo.PtspName = _HCoreContext.HCUAccountOwner.Where(x =>
                       x.AccountId == TerminalInfo.ReferenceId
                       && x.Owner.AccountTypeId == UserAccountType.PosAccount)
                       .Select(x => x.Owner.DisplayName)
                       .FirstOrDefault();

                        TerminalInfo.AcquirerName = _HCoreContext.HCUAccountOwner.Where(x =>
                       x.AccountId == TerminalInfo.ReferenceId
                       && x.Owner.AccountTypeId == UserAccountType.Acquirer)
                       .Select(x => x.Owner.DisplayName)
                       .FirstOrDefault();

                        TerminalInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == UserAccountId
                                                         && m.CreatedById == TerminalInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                    }
                    _OOverviewResponse.TerminalsOverview = _OOverviewResponse.TerminalsOverview.OrderByDescending(x => x.LastTransactionDate).ToList();
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the acquirer overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerOverviewLite(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            _OOverviewResponse.AppUsers = 0;
            _OOverviewResponse.Transactions = 0;
            _OOverviewResponse.PurchaseAmount = 0;
            _OOverviewResponse.RewardTransactions = 0;
            _OOverviewResponse.RewardAmount = 0;
            _OOverviewResponse.RewardPurchaseAmount = 0;
            _OOverviewResponse.CashRewardTransactions = 0;
            _OOverviewResponse.CashRewardAmount = 0;
            _OOverviewResponse.CashRewardPurchaseAmount = 0;
            _OOverviewResponse.CardRewardTransactions = 0;
            _OOverviewResponse.CardRewardAmount = 0;
            _OOverviewResponse.CardRewardPurchaseAmount = 0;
            _OOverviewResponse.RedeemTransactions = 0;
            _OOverviewResponse.RedeemAmount = 0;
            _OOverviewResponse.RedeemPurchaseAmount = 0;
            _OOverviewResponse.Charge = 0;
            _OOverviewResponse.ClaimedRewardTransations = 0;
            _OOverviewResponse.ClaimedReward = 0;
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();

                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                   && x.AccountTypeId == UserAccountType.Appuser
                   && x.CreateDate > _Request.StartTime
                   && x.CreateDate < _Request.EndTime)
                   .Count();


                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m =>
                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count();
                    if (_OOverviewResponse.Transactions > 0)
                    {
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m =>
                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    }
                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m =>
                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                      && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                      && m.ModeId == Helpers.TransactionMode.Credit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Count();

                    // Reward Amount
                    if (_OOverviewResponse.RewardTransactions > 0)
                    {


                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m =>
                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                               && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m =>
                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0

                                                               && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m =>

                                                        m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0

                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        if (_OOverviewResponse.CashRewardTransactions > 0)
                        {



                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                    && m.TypeId == TransactionType.CashReward
                                                    && m.ModeId == TransactionMode.Credit
                                                     && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                    && m.TransactionDate > _Request.StartTime
                                                    && m.TransactionDate < _Request.EndTime
                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }

                        DateTime TDayStart = HCoreHelper.GetGMTDate();
                        DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                        DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                        DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                        DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);


                        _OOverviewResponse.ActiveTerminals = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                             && m.TypeId == TransactionType.CardReward
                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                             && m.TransactionDate > TDayStart
                                                             && m.TransactionDate < TDayEnd
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                             && m.ModeId == TransactionMode.Credit
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Select(x => x.CreatedById).Distinct().Count();

                        _OOverviewResponse.IdleTerminals = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                            && m.TypeId == TransactionType.CardReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > T7DayStart
                                                            && m.TransactionDate < T7DayEnd
                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.ModeId == TransactionMode.Credit
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Select(x => x.CreatedById).Distinct().Count();

                        _OOverviewResponse.DeadTerminals = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                          && m.TypeId == TransactionType.CardReward
                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                          && m.TransactionDate < TDeadDayEnd
                                                          && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                          && m.ModeId == TransactionMode.Credit
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Select(x => x.CreatedById).Distinct().Count();



                        // Card Reward
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                               && m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.ModeId == TransactionMode.Credit
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.CardRewardTransactions > 0)
                        {
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                               && m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                             && m.ModeId == TransactionMode.Credit
                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;



                        }

                        _OOverviewResponse.CardRewardPurchaseAmountOther = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId != UserAccountId).Count() > 0
                                                                 && m.TypeId == TransactionType.CardReward
                                                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                 && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.CardRewardTransactionsOther = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId != UserAccountId).Count() > 0
                                                               && m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.ModeId == TransactionMode.Credit
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                    }

                    // Redeem
                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                           && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           && m.ModeId == Helpers.TransactionMode.Debit
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();
                    if (_OOverviewResponse.RedeemTransactions > 0)
                    {
                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                               && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                               && m.ModeId == Helpers.TransactionMode.Debit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && m.SourceId == TransactionSource.TUC
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    }

                    _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                 x.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0 &&
                                                                   x.TransactionDate > _Request.StartTime &&
                                                               x.TransactionDate < _Request.EndTime
                                                               && x.StatusId == HelperStatus.Transaction.Success
                                                               && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                               && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                               && x.SourceId == TransactionSource.ThankUCashPlus)
                                                               .Select(x => new OOverview.MiniTransaction
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   TypeName = x.Type.Name,
                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   MerchantName = x.Parent.DisplayName,
                                                                   RewardAmount = x.TotalAmount,
                                                                   TransactionDate = x.TransactionDate
                                                               }).OrderByDescending(x => x.TransactionDate)
                                                               .FirstOrDefault();
                    // TUC PLUS
                    _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                          && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.SourceId == TransactionSource.ThankUCashPlus
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                    _OOverviewResponse.ClaimedRewardTransations = _HCoreContext.HCUAccountTransaction
                                                      .Count(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                       && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                       && m.ModeId == Helpers.TransactionMode.Debit
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && m.SourceId == TransactionSource.ThankUCashPlus
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        );
                    if (_OOverviewResponse.ClaimedRewardTransations > 0)
                    {
                        _OOverviewResponse.ClaimedReward = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.CreatedBy.HCUAccountOwnerAccount.Where(a => a.OwnerId == UserAccountId).Count() > 0
                                                      && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                      && m.ModeId == Helpers.TransactionMode.Debit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.SourceId == TransactionSource.ThankUCashPlus
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Sum(m => (double?)m.TotalAmount) ?? 0;
                    }






                    _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                  x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                  && (_HCoreContext.HCUAccountOwner.Where(m => m.OwnerId == UserAccountId && m.Account.OwnerId == x.Id).Count() > 0)
                  ).Select(x => new OOverview.PosOverview
                  {
                      ReferenceId = x.Id,
                      ReferenceKey = x.Guid,
                      DisplayName = x.DisplayName,
                  }).Skip(0).Take(50).ToList();
                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                    {
                        PosAcc.Terminals = _HCoreContext.HCUAccountOwner.Count(x =>
                            x.Account.OwnerId == PosAcc.ReferenceId
                            && x.OwnerId == UserAccountId
                            && x.AccountTypeId == UserAccountType.Terminal);
                        var LastTransaction = _HCoreContext.HCUAccountTransaction
                                                          .Where(m =>
                                                              m.CreatedBy.HCUAccountOwnerAccount.Count(x => x.Account.OwnerId == PosAcc.ReferenceId
                                                            && x.OwnerId == UserAccountId
                                                            && x.AccountTypeId == UserAccountType.Terminal) > 0
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            )
                                                           .OrderByDescending(m => m.TransactionDate)
                                                           .Select(m => new
                                                           {
                                                               MerchantDisplayName = m.Parent.DisplayName,
                                                               StoreDisplayName = m.SubParent.DisplayName,
                                                               TransactionDate = m.TransactionDate
                                                           })
                                                           .FirstOrDefault();
                        if (LastTransaction != null)
                        {
                            PosAcc.MerchantDisplayName = LastTransaction.MerchantDisplayName;
                            PosAcc.StoreDisplayName = LastTransaction.StoreDisplayName;
                            PosAcc.LastTransactionDate = LastTransaction.TransactionDate;
                        }
                    }






                    _OOverviewResponse.TerminalsOverview = _HCoreContext.HCUAccountOwner.Where(x =>
               x.AccountTypeId == UserAccountType.Terminal
               && x.OwnerId == UserAccountId)
               .Select(x => new OOverview.TerminalsOverview
               {
                   ReferenceId = x.AccountId,
                   ReferenceKey = x.Guid,
                   DisplayName = x.Account.DisplayName,
               }).ToList();
                    foreach (var TerminalInfo in _OOverviewResponse.TerminalsOverview)
                    {
                        TerminalInfo.StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x =>
                        x.AccountId == TerminalInfo.ReferenceId
                        && x.Owner.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => x.Owner.DisplayName)
                        .FirstOrDefault();

                        TerminalInfo.PtspName = _HCoreContext.HCUAccountOwner.Where(x =>
                       x.AccountId == TerminalInfo.ReferenceId
                       && x.Owner.AccountTypeId == UserAccountType.PosAccount)
                       .Select(x => x.Owner.DisplayName)
                       .FirstOrDefault();

                        TerminalInfo.AcquirerName = _HCoreContext.HCUAccountOwner.Where(x =>
                       x.AccountId == TerminalInfo.ReferenceId
                       && x.Owner.AccountTypeId == UserAccountType.Acquirer)
                       .Select(x => x.Owner.DisplayName)
                       .FirstOrDefault();

                        TerminalInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == UserAccountId
                                                         && m.CreatedById == TerminalInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                    }
                    _OOverviewResponse.TerminalsOverview = _OOverviewResponse.TerminalsOverview.OrderByDescending(x => x.LastTransactionDate).ToList();




                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreOverview(OOverview.Request _Request)
        {
            long StoreId = 0;
            long? MerchantId = 0;
            _OOverviewResponse = new OOverview.Overview();
            using (_HCoreContext = new HCoreContext())
            {
                StoreId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                MerchantId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.OwnerId).FirstOrDefault();
                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", MerchantId));
            }
            try
            {
                if (_OOverviewResponse.ThankUCashPlus == 1)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        //long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                        x.ParentId == MerchantId
                        && x.SubParentId == StoreId
                       && x.Account.AccountTypeId == UserAccountType.Appuser
                       && x.TransactionDate > _Request.StartTime
                       && x.TransactionDate < _Request.EndTime)
                       .Select(x => x.AccountId)
                       .Distinct()
                       .Count();




                        _OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x =>
                        x.OwnerId == MerchantId
                        && x.AccountTypeId == Helpers.UserAccountType.Appuser
                        && x.CreateDate > _Request.StartTime
                        && x.CreateDate < _Request.EndTime)
                        .OrderByDescending(x => x.CreateDate)
                        .Select(x => x.CreateDate)
                        .FirstOrDefault();

                        _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == MerchantId && x.SubParentId == StoreId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                        _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == MerchantId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                        _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.OwnerId == StoreId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.Terminals = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == StoreId && x.AccountTypeId == Helpers.UserAccountType.Terminal && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        //_OOverviewResponse.RewardCardsUsed = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.StatusId != TUStatusHelper.Card.NotAssigned && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();


                        // Transactions 
                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m =>
                                                            m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.ModeId == TransactionMode.Credit
                                                            && m.SourceId == TransactionSource.ThankUCashPlus
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                            && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                            && m.TypeId == TransactionType.CashReward
                                                           && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        // Card Reward
                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                              && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        // Reward Amount
                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                      x.ParentId == MerchantId
                                                            && x.SubParentId == StoreId &&
                                                                       x.TransactionDate > _Request.StartTime &&
                                                                   x.TransactionDate < _Request.EndTime
                                                                   && x.StatusId == HelperStatus.Transaction.Success
                                                                   && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                                   && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                           && x.SourceId == TransactionSource.ThankUCashPlus)
                                                                   .Select(x => new OOverview.MiniTransaction
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       TypeName = x.Type.Name,
                                                                       InvoiceAmount = x.PurchaseAmount,
                                                                       MerchantName = x.Parent.DisplayName,
                                                                       RewardAmount = x.TotalAmount,
                                                                       TransactionDate = x.TransactionDate
                                                                   }).OrderByDescending(x => x.TransactionDate)
                                                                   .FirstOrDefault();

                        _OOverviewResponse.LastCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == MerchantId
                                                            && m.SubParentId == StoreId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.SourceId == TransactionSource.Settlement &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime
                                                                     ).OrderByDescending(m => m.TransactionDate)
                                                                   .Select(m => m.TransactionDate).FirstOrDefault();
                        _OOverviewResponse.SettlementCredit = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == MerchantId
                                                            && m.SubParentId == StoreId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.Settlement
                                                                      ).Sum(m => (double?)m.TotalAmount) ?? 0;


                        _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == MerchantId
                                                            && m.SubParentId == StoreId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.LastIssuerCommissionDate = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == MerchantId
                                                            && m.SubParentId == StoreId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Select(m => m.TransactionDate).FirstOrDefault();


                        _OOverviewResponse.SettlementDebit = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == MerchantId
                                                            && m.SubParentId == StoreId &&
                                                                     m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.Settlement
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.SourceId == TransactionSource.ThankUCashPlus
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;
                        _OOverviewResponse.ClaimedReward = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                             && m.TypeId == TransactionType.ThankUCashPlusCredit
                                                             && m.ModeId == Helpers.TransactionMode.Debit
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.SourceId == TransactionSource.ThankUCashPlus
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId));
                        if (_OOverviewResponse.ThankUCashPlus > 0)
                        {
                            //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", MerchantId));
                            _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", MerchantId));
                            _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", MerchantId));
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        // App Users
                        _OOverviewResponse.TransactionIssuerAmountCredit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Amount) ?? 0;

                        _OOverviewResponse.TransactionIssuerAmountDebit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Amount) ?? 0;



                        _OOverviewResponse.TransactionIssuerChargeCredit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.AccountId == UserAccountId &&
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                 m.TransactionDate < _Request.EndTime
                                                                   ).Sum(m => (double?)m.Charge) ?? 0;

                        _OOverviewResponse.TransactionIssuerChargeDebit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Charge) ?? 0;


                        _OOverviewResponse.TransactionIssuerTotalCreditAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == UserAccountId &&
                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                              m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                 m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                  m.TransactionDate > _Request.StartTime &&
                                                               m.TransactionDate < _Request.EndTime
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.TransactionIssuerTotalDebitAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.TotalAmount) ?? 0;

                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        //long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == MerchantId
                        && x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.TransactionDate > _Request.StartTime
                        && x.TransactionDate < _Request.EndTime)
                        .Select(x => x.AccountId)
                        .Distinct()
                        .Count();


                        _OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x => x.OwnerId == MerchantId
                        && x.AccountTypeId == Helpers.UserAccountType.Appuser
                        && x.CreateDate > _Request.StartTime
                        && x.CreateDate < _Request.EndTime)
                        .OrderByDescending(x => x.CreateDate)
                        .Select(x => x.CreateDate)
                        .FirstOrDefault();

                        _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == MerchantId && x.SubParentId == StoreId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                        _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == MerchantId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                        _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.OwnerId == StoreId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                        _OOverviewResponse.Terminals = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == StoreId && x.AccountTypeId == Helpers.UserAccountType.Terminal && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();

                        // Transactions 
                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == MerchantId
                                                           && m.SubParentId == StoreId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                        _OOverviewResponse.TransactionsPercentage = 100;

                        _OOverviewResponse.NewTransactions = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m => m.ParentId == MerchantId
                                                           && m.SubParentId == StoreId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime)
                        .Count() == 1)
                        .Count();

                        _OOverviewResponse.NewTransactionsPercentage = 100;

                        _OOverviewResponse.RepeatingTransactions = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(m =>
                       m.ParentId == MerchantId
                                                           && m.SubParentId == StoreId
                        && m.AccountId == x.Id
                        && m.TransactionDate > _Request.StartTime
                        && m.TransactionDate < _Request.EndTime)
                        .Count() > 1)
                        .Count();


                        _OOverviewResponse.ReferralTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == MerchantId
                                                           && m.SubParentId == StoreId
                                                            && m.Account.OwnerId != MerchantId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                        // Purchase AMOUNT
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == MerchantId
                                                          && m.SubParentId == StoreId
                                                            && m.Account.AccountTypeId == UserAccountType.Appuser
                                                            && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.PurchaseAmountPercentage = 100;






                        // Cash Reward
                        _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == MerchantId
                                                            && m.SubParentId == StoreId
                                                            && m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        // Card Reward
                        _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.TypeId == TransactionType.CardReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();


                        // Reward Amount
                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == MerchantId
                                                                && m.SubParentId == StoreId
                                                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                        _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                         x.ParentId == MerchantId &&
                                                           x.SubParentId == StoreId &&
                                                                       x.TransactionDate > _Request.StartTime &&
                                                                   x.TransactionDate < _Request.EndTime
                                                                   && x.StatusId == HelperStatus.Transaction.Success
                                                                   && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                                   && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                                   && x.SourceId == TransactionSource.TUC)
                                                                   .Select(x => new OOverview.MiniTransaction
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       TypeName = x.Type.Name,
                                                                       InvoiceAmount = x.PurchaseAmount,
                                                                       MerchantName = x.Parent.DisplayName,
                                                                       RewardAmount = x.TotalAmount,
                                                                       TransactionDate = x.TransactionDate
                                                                   }).OrderByDescending(x => x.TransactionDate)
                                                                   .FirstOrDefault();
                        _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId));
                        if (_OOverviewResponse.ThankUCashPlus > 0)
                        {
                            //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                            _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", MerchantId));
                            _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", MerchantId));
                            _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", MerchantId));
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                        // App Users
                        _OOverviewResponse.TransactionIssuerAmountCredit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Amount) ?? 0;

                        _OOverviewResponse.TransactionIssuerAmountDebit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Amount) ?? 0;



                        _OOverviewResponse.TransactionIssuerChargeCredit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.AccountId == UserAccountId &&
                                                                   m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Credit &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                 m.TransactionDate < _Request.EndTime
                                                                   ).Sum(m => (double?)m.Charge) ?? 0;

                        _OOverviewResponse.TransactionIssuerChargeDebit = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.Charge) ?? 0;


                        _OOverviewResponse.TransactionIssuerTotalCreditAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == UserAccountId &&
                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                              m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                 m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                  m.TransactionDate > _Request.StartTime &&
                                                               m.TransactionDate < _Request.EndTime
                                                                 ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.TransactionIssuerTotalDebitAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.TypeId == Helpers.TransactionType.TransactionBonus &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == Helpers.TransactionSource.Settlement &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime
                                                                    ).Sum(m => (double?)m.TotalAmount) ?? 0;

                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the merchant application user overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantAppUserOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey).Select(x => x.Id).FirstOrDefault();
                    long SubUserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey).Select(x => x.Id).FirstOrDefault();
                    //_OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId
                    //&& x.Account.AccountTypeId == UserAccountType.Appuser
                    //&& x.TransactionDate > _Request.StartTime
                    //&& x.TransactionDate < _Request.EndTime)
                    //.Select(x => x.AccountId)
                    //.Distinct()
                    //.Count();

                    //_OOverviewResponse.AppUsersPercentage = 100;
                    //_OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccount.Where(x =>
                    //x.OwnerId == UserAccountId
                    //&& x.AccountTypeId == UserAccountType.Appuser
                    //&& x.CreateDate > _Request.StartTime
                    //&& x.CreateDate < _Request.EndTime)
                    //.Count();

                    //_OOverviewResponse.OwnAppUsersPercentage = 100;
                    //_OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x =>
                    //x.ParentId == UserAccountId
                    //&& x.Account.OwnerId != UserAccountId
                    //&& x.Account.AccountTypeId == UserAccountType.Appuser
                    //&& x.TransactionDate > _Request.StartTime
                    //&& x.TransactionDate < _Request.EndTime)
                    //.Select(x => x.AccountId)
                    //.Distinct()
                    //.Count();

                    //_OOverviewResponse.ReferralAppUsersPercentage = 100;
                    //_OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                    //x.AccountTypeId == UserAccountType.Appuser
                    //&& x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                    //&& m.AccountId == x.Id
                    //&& m.TransactionDate > _Request.StartTime
                    //&& m.TransactionDate < _Request.EndTime)
                    //.Count() > 1)
                    //.Distinct()
                    //.Count();
                    // _OOverviewResponse.RepeatingAppUsersPercentage = 100;

                    //_OOverviewResponse.LastAppUserDate = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId
                    //&& x.AccountTypeId == Helpers.UserAccountType.Appuser
                    //&& x.CreateDate > _Request.StartTime
                    //&& x.CreateDate < _Request.EndTime)
                    //.OrderByDescending(x => x.CreateDate)
                    //.Select(x => x.CreateDate)
                    //.FirstOrDefault();

                    //_OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                    //_OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                    //_OOverviewResponse.Stores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Where(x => x.Owner.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.Terminals = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.TerminalAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.Pssp = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == Helpers.UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.RewardCards = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();
                    //_OOverviewResponse.RewardCardsUsed = _HCoreContext.TUCard.Where(x => x.ActiveMerchantId == UserAccountId && x.StatusId != TUStatusHelper.Card.NotAssigned && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime).Count();


                    // Transactions 
                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Count();






                    //_OOverviewResponse.TransactionsPercentage = 100;

                    //_OOverviewResponse.NewTransactions = _HCoreContext.HCUAccount.Where(x =>
                    //x.AccountTypeId == UserAccountType.Appuser
                    //&& x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                    //&& m.AccountId == x.Id
                    //&& m.TransactionDate > _Request.StartTime
                    //&& m.TransactionDate < _Request.EndTime)
                    //.Count() == 1)
                    //.Count();

                    //_OOverviewResponse.NewTransactionsPercentage = 100;

                    //_OOverviewResponse.RepeatingTransactions = _HCoreContext.HCUAccount.Where(x =>
                    //x.AccountTypeId == UserAccountType.Appuser
                    //&& x.HCUAccountTransactionAccount.Where(m =>
                    //m.ParentId == UserAccountId
                    //&& m.AccountId == x.Id
                    //&& m.TransactionDate > _Request.StartTime
                    //&& m.TransactionDate < _Request.EndTime)
                    //.Count() > 1)
                    //.Count();
                    //_OOverviewResponse.RepeatingTransactionsPercentage = 100;


                    //_OOverviewResponse.ReferralTransactions = _HCoreContext.HCUAccountTransaction
                    //                                   .Where(m => m.ParentId == UserAccountId
                    //                                    && m.Account.OwnerId != UserAccountId
                    //                                    && m.Account.AccountTypeId == UserAccountType.Appuser
                    //                                    && m.TransactionDate > _Request.StartTime
                    //                                    && m.TransactionDate < _Request.EndTime
                    //                                    && m.SourceId == TransactionSource.TUC
                    //                                    && m.StatusId == HelperStatus.Transaction.Success
                    //                                     ).Count();
                    //_OOverviewResponse.ReferralTransactionsPercentage = 100;



                    // Purchase AMOUNT
                    _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //_OOverviewResponse.PurchaseAmountPercentage = 100;

                    //_OOverviewResponse.NewPurchaseAmount = _HCoreContext.HCUAccountTransaction
                    //                                  .Where(m => m.ParentId == UserAccountId
                    //                                  && m.Account.OwnerId == UserAccountId
                    //                                    && m.Account.AccountTypeId == UserAccountType.Appuser
                    //                                    && m.TransactionDate > _Request.StartTime
                    //                                   && m.TransactionDate < _Request.EndTime
                    //                                   && m.SourceId == TransactionSource.TUC
                    //                                   && m.StatusId == HelperStatus.Transaction.Success
                    //                                    ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //_OOverviewResponse.NewPurchaseAmountPercentage = 100;

                    //_OOverviewResponse.RepeatingPurchaseAmount = _HCoreContext.HCUAccount.Where(x =>
                    //x.AccountTypeId == UserAccountType.Appuser
                    //&& x.HCUAccountTransactionAccount.Where(m => m.ParentId == UserAccountId
                    //&& m.AccountId == x.Id
                    //&& m.TransactionDate > _Request.StartTime
                    //&& m.TransactionDate < _Request.EndTime).Count() > 0)
                    //.Count();
                    //_OOverviewResponse.RepeatingPurchaseAmountPercentage = 100;


                    //_OOverviewResponse.ReferralPurchaseAmount = _HCoreContext.HCUAccountTransaction
                    //                                   .Where(m => m.ParentId == UserAccountId
                    //                                    && m.Account.OwnerId != UserAccountId
                    //                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                    //                                     && m.TransactionDate > _Request.StartTime
                    //                                    && m.TransactionDate < _Request.EndTime
                    //                                    && m.SourceId == TransactionSource.TUC
                    //                                    && m.StatusId == HelperStatus.Transaction.Success
                    //                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //_OOverviewResponse.ReferralPurchaseAmountPercentage = 100;


                    // Cash Reward
                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                        && m.TypeId == TransactionType.CashReward
                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && m.SourceId == TransactionSource.TUC
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();


                    // Card Reward
                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.TypeId == TransactionType.CardReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.TypeId == TransactionType.CardReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.TypeId == TransactionType.CardReward
                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();


                    // Reward Amount
                    _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.AccountId == SubUserAccountId
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                    _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.AccountId == SubUserAccountId
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.AccountId == SubUserAccountId
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            && m.StatusId == HelperStatus.Transaction.Success

                                                             ).Count();

                    _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                            && m.AccountId == SubUserAccountId
                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.TotalAmount) ?? 0;

                    _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.ParentId == UserAccountId
                                                        && m.AccountId == SubUserAccountId
                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.SourceId == TransactionSource.TUC
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                    _OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                                                                  x.ParentId == UserAccountId &&
                                                                   x.TransactionDate > _Request.StartTime &&
                                                               x.TransactionDate < _Request.EndTime
                                                               && x.AccountId == SubUserAccountId
                                                               && x.StatusId == HelperStatus.Transaction.Success
                                                               && (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                                                               && (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                                                               && x.SourceId == TransactionSource.TUC)
                                                               .Select(x => new OOverview.MiniTransaction
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   TypeName = x.Type.Name,
                                                                   InvoiceAmount = x.PurchaseAmount,
                                                                   MerchantName = x.Parent.DisplayName,
                                                                   RewardAmount = x.TotalAmount,
                                                                   TransactionDate = x.TransactionDate
                                                               })
                                                               .FirstOrDefault();






                }

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {


                    var UserAccountDetails = _HCoreContext.HCUAccount
                    .Where(x => x.Guid == _Request.UserAccountKey)
                    .Select(x => new
                    {
                        AccountId = x.Id,
                        AccountTypeId = x.AccountTypeId,
                    }).FirstOrDefault();
                    if (UserAccountDetails != null)
                    {
                        if (UserAccountDetails.AccountTypeId == HCoreConstant.Helpers.UserAccountType.PosAccount)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, GetPosAccountOverview(UserAccountDetails.AccountId), "HC0001");
                            #endregion
                        }
                        else
                        {
                            _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Merchants = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Cashiers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Ptsp = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.PosAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Pssp = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.PgAccount && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Terminals = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Terminal && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.Acquirers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Acquirer && x.CreateDate > _Request.StartTime && x.CreateDate < _Request.EndTime);
                            _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           && m.ModeId == Helpers.TransactionMode.Credit
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                            _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Count();





                            _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.TotalAmount) ?? 0;

                            _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Count();

                            _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && m.SourceId == TransactionSource.ThankUCashPlus
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                            _OOverviewResponse.TUCPlusPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                        .Where(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                         && m.ModeId == Helpers.TransactionMode.Credit
                                                         && m.TransactionDate > _Request.StartTime
                                                         && m.TransactionDate < _Request.EndTime
                                                         && m.SourceId == TransactionSource.ThankUCashPlus
                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                          ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _OOverviewResponse.TUCPlusReward = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ModeId == Helpers.TransactionMode.Credit
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.ThankUCashPlus
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;


                            _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                        .Where(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                         && m.ModeId == Helpers.TransactionMode.Debit
                                                         && m.TransactionDate > _Request.StartTime
                                                         && m.TransactionDate < _Request.EndTime
                                                         && m.SourceId == TransactionSource.ThankUCashPlus
                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                          ).Sum(m => (double?)m.TotalAmount) ?? 0;

                            _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && m.SourceId == TransactionSource.ThankUCashPlus
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count();



                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                  .Where(m => m.TypeId == TransactionType.CashReward
                                                   && m.ModeId == Helpers.TransactionMode.Credit
                                                   && m.TransactionDate > _Request.StartTime
                                                   && m.TransactionDate < _Request.EndTime
                                                   && m.SourceId == TransactionSource.TUC
                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Count();


                            // Card Reward
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Count();


                            // Other Reward
                            _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m =>
                                                                   (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m =>
                                                                    (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m =>
                                                                    (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.SourceId == TransactionSource.TUC
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Count();


                            #region Send Responsex
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                            #endregion
                        }

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        #endregion
                    }

                    //    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                    //        .Where(x => x.Type.Value == HCoreConstant.Helpers.TransactionTypeValue.Reward
                    //               && x.ModeId == HCoreConstant.Helpers.TransactionMode.Debit
                    //               && x.TransactionDate > StartTime && x.TransactionDate < EndTime
                    //               && (x.Account.AccountTypeId == UserAccountType.MerchantCashier
                    //                   || x.Account.AccountTypeId == UserAccountType.Merchant
                    //                  ))
                    //                .Sum(x => (double?)x.TotalAmount) ?? 0;
                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the system overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemOverviewLite(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            _OOverviewResponse.AppUsers = 0;
            _OOverviewResponse.Transactions = 0;
            _OOverviewResponse.PurchaseAmount = 0;
            _OOverviewResponse.RewardTransactions = 0;
            _OOverviewResponse.RewardAmount = 0;
            _OOverviewResponse.RewardPurchaseAmount = 0;
            _OOverviewResponse.CashRewardTransactions = 0;
            _OOverviewResponse.CashRewardAmount = 0;
            _OOverviewResponse.CashRewardPurchaseAmount = 0;
            _OOverviewResponse.CardRewardTransactions = 0;
            _OOverviewResponse.CardRewardAmount = 0;
            _OOverviewResponse.CardRewardPurchaseAmount = 0;
            _OOverviewResponse.RedeemTransactions = 0;
            _OOverviewResponse.RedeemAmount = 0;
            _OOverviewResponse.RedeemPurchaseAmount = 0;
            _OOverviewResponse.Charge = 0;
            _OOverviewResponse.ClaimedRewardTransations = 0;
            _OOverviewResponse.ClaimedReward = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                   && x.CreateDate > _Request.StartTime
                   && x.CreateDate < _Request.EndTime)
                   .Count();

                    _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                     x.AccountTypeId == UserAccountType.Appuser
                     && x.HCUAccountTransactionAccount.Where(m => m.AccountId == x.Id
                     && m.TransactionDate > _Request.StartTime
                     && m.TransactionDate < _Request.EndTime)
                     .Count() > 1)
                     .Distinct()
                     .Count();
                    _OOverviewResponse.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                    _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         ).Count();
                    if (_OOverviewResponse.Transactions > 0)
                    {

                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    }
                    _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                      && m.ModeId == Helpers.TransactionMode.Credit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Count();

                    // Reward Amount
                    if (_OOverviewResponse.RewardTransactions > 0)
                    {


                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;


                        _OOverviewResponse.UserAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && m.SourceId == TransactionSource.TUC
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(m => (double?)m.TotalAmount) ?? 0;


                        _OOverviewResponse.ThankUAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.AccountId == ThankUAccountId
                                                             && m.ModeId == Helpers.TransactionMode.Credit
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.SourceId == TransactionSource.Settlement
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.TotalAmount) ?? 0;


                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        if (_OOverviewResponse.CashRewardTransactions > 0)
                        {

                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.TypeId == TransactionType.CashReward
                                                    && m.ModeId == TransactionMode.Credit
                                                     && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                    && m.TransactionDate > _Request.StartTime
                                                    && m.TransactionDate < _Request.EndTime
                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }


                        // Card Reward
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.ModeId == TransactionMode.Credit
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.CardRewardTransactions > 0)
                        {
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                             && m.ModeId == TransactionMode.Credit
                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }
                    }



                    // Redeem
                    _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           && m.ModeId == Helpers.TransactionMode.Debit
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && m.SourceId == TransactionSource.TUC
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Count();
                    if (_OOverviewResponse.RedeemTransactions > 0)
                    {
                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                               && m.ModeId == Helpers.TransactionMode.Debit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && m.SourceId == TransactionSource.TUC
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == Helpers.TransactionMode.Debit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                    }


                    // TUC PLUS
                    _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.Type.SubParentId == TransactionTypeCategory.Reward
                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.SourceId == TransactionSource.ThankUCashPlus
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                    _OOverviewResponse.ClaimedRewardTransations = _HCoreContext.HCUAccountTransaction
                                                      .Count(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                       && m.ModeId == Helpers.TransactionMode.Debit
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && m.SourceId == TransactionSource.ThankUCashPlus
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                        );
                    if (_OOverviewResponse.ClaimedRewardTransations > 0)
                    {
                        _OOverviewResponse.ClaimedReward = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TypeId == TransactionType.ThankUCashPlusCredit
                                                      && m.ModeId == Helpers.TransactionMode.Debit
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && m.SourceId == TransactionSource.ThankUCashPlus
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Sum(m => (double?)m.TotalAmount) ?? 0;
                    }


                    _OOverviewResponse.MerchantOverview = _HCoreContext.HCUAccount.Where(x =>
                  x.AccountTypeId == UserAccountType.Merchant
                  && x.HCUAccountTransactionParent.Count(m => m.ParentId == x.Id
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success) > 0
                  )
                  .Select(x => new OOverview.MerchantOverview
                  {
                      ReferenceId = x.Id,
                      ReferenceKey = x.Guid,
                      DisplayName = x.DisplayName,
                      Address = x.Address,
                      Latitude = x.Latitude,
                      Longitude = x.Longitude,
                  }).Skip(0).Take(10).ToList();
                    foreach (var MerchantInfo in _OOverviewResponse.MerchantOverview)
                    {
                        MerchantInfo.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.ParentId == MerchantInfo.ReferenceId
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        MerchantInfo.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Count(m => m.ParentId == MerchantInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           );

                        MerchantInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == MerchantInfo.ReferenceId
                                                          && m.Account.AccountTypeId == UserAccountType.Appuser
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                    }
                    _OOverviewResponse.MerchantOverview = _OOverviewResponse.MerchantOverview.OrderByDescending(x => x.Transactions).ToList();
                    _OOverviewResponse.PosOverview = _HCoreContext.HCUAccount.Where(x =>
                    x.AccountTypeId == UserAccountType.PosAccount && x.StatusId == HelperStatus.Default.Active
                    ).Select(x => new OOverview.PosOverview
                    {
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        DisplayName = x.DisplayName,
                    }).Skip(0).Take(50).ToList();
                    foreach (var PosAcc in _OOverviewResponse.PosOverview)
                    {
                        PosAcc.Terminals = _HCoreContext.HCUAccountOwner.Count(x =>
                            x.OwnerId == PosAcc.ReferenceId
                            && x.AccountTypeId == UserAccountType.Terminal);
                        var LastTransaction = _HCoreContext.HCUAccountTransaction
                                                          .Where(m => m.CreatedBy.OwnerId == PosAcc.ReferenceId
                                                           && m.Account.AccountTypeId == UserAccountType.Appuser
                                                           && m.TransactionDate > _Request.StartTime
                                                           && m.TransactionDate < _Request.EndTime
                                                           && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            )
                                                           .OrderByDescending(m => m.TransactionDate)
                                                           .Select(m => new
                                                           {
                                                               MerchantDisplayName = m.Parent.DisplayName,
                                                               StoreDisplayName = m.SubParent.DisplayName,
                                                               TransactionDate = m.TransactionDate
                                                           })
                                                           .FirstOrDefault();
                        if (LastTransaction != null)
                        {
                            PosAcc.MerchantDisplayName = LastTransaction.MerchantDisplayName;
                            PosAcc.StoreDisplayName = LastTransaction.StoreDisplayName;
                            PosAcc.LastTransactionDate = LastTransaction.TransactionDate;
                        }
                    }


                    _OOverviewResponse.FrequentBuyers = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    && x.StatusId == HelperStatus.Default.Active
                    && (x.HCUAccountTransactionAccount.Count(m =>
                    m.TransactionDate > _Request.StartTime
                    && m.TransactionDate < _Request.EndTime
                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                           || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           && m.StatusId == HelperStatus.Transaction.Success
                    ) > 3))
                    .Select(x => new OOverview.UserAccountOverview
                    {
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        DisplayName = x.DisplayName,
                        MobileNumber = x.MobileNumber,
                    }).Skip(0).Take(20).ToList();
                    foreach (var User in _OOverviewResponse.FrequentBuyers)
                    {
                        User.Transactions = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.AccountId == User.ReferenceId
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Count();
                        User.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.AccountId == User.ReferenceId
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                        User.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.AccountId == User.ReferenceId
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        User.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.AccountId == User.ReferenceId
                                                      && m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC)
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Sum(x => (double?)x.TotalAmount) ?? 0;

                        User.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.AccountId == User.ReferenceId
                                                     && m.TransactionDate > _Request.StartTime
                                                     && m.TransactionDate < _Request.EndTime
                                                     && (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)
                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(x => (double?)x.TotalAmount) ?? 0;
                        User.RewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                   .Where(m => m.AccountId == User.ReferenceId
                                                    && m.TransactionDate > _Request.StartTime
                                                    && m.TransactionDate < _Request.EndTime
                                                    && (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                     ).Sum(x => (double?)x.TotalAmount) ?? 0;
                    }





                    //_OOverviewResponse.TerminalsOverview = _HCoreContext.HCUAccount.Where(x =>
                    //x.AccountTypeId == UserAccountType.TerminalAccount
                    //&& x.HCUAccountTransactionCreatedBy.Count(m => m.CreatedById == x.Id
                    //                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                    //                                        && m.TransactionDate > _Request.StartTime
                    //                                        && m.TransactionDate < _Request.EndTime
                    //                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                    //                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                        && m.StatusId == HelperStatus.Transaction.Success
                    //) > 0)
                    //.Select(x => new OOverview.TerminalsOverview
                    //{
                    //    ReferenceId = x.Id,
                    //    ReferenceKey = x.Guid,
                    //    DisplayName = x.DisplayName,
                    //}).Skip(0).Take(10).ToList();
                    //foreach (var TerminalInfo in _OOverviewResponse.TerminalsOverview)
                    //{
                    //    TerminalInfo.StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x =>
                    //    x.UserAccountId == TerminalInfo.ReferenceId
                    //    && x.Owner.AccountTypeId == UserAccountType.MerchantStore)
                    //    .Select(x => x.Owner.DisplayName)
                    //    .FirstOrDefault();

                    //    TerminalInfo.PtspName = _HCoreContext.HCUAccountOwner.Where(x =>
                    //   x.UserAccountId == TerminalInfo.ReferenceId
                    //   && x.Owner.AccountTypeId == UserAccountType.PosAccount)
                    //   .Select(x => x.Owner.DisplayName)
                    //   .FirstOrDefault();
                    //    TerminalInfo.AcquirerName = _HCoreContext.HCUAccountOwner.Where(x =>
                    //   x.UserAccountId == TerminalInfo.ReferenceId
                    //   && x.Owner.AccountTypeId == UserAccountType.Acquirer)
                    //   .Select(x => x.Owner.DisplayName)
                    //   .FirstOrDefault();
                    //    TerminalInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                    //                                     .Where(m => m.CreatedById == TerminalInfo.ReferenceId
                    //                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                    //                                      && m.TransactionDate > _Request.StartTime
                    //                                      && m.TransactionDate < _Request.EndTime
                    //                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                    //                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                      && m.StatusId == HelperStatus.Transaction.Success
                    //                                       ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                    //}
                    //_OOverviewResponse.TerminalsOverview = _OOverviewResponse.TerminalsOverview.OrderByDescending(x => x.LastTransactionDate).ToList();


                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the account balance overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalanceOverview(OOverview.Request _Request)
        {
            _OOverviewResponse = new OOverview.Overview();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.UserAccountId));
                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();
                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.HCUAccountTransactionAccount.Where(m => m.ParentId == AccountDetails.UserAccountId
                                && m.AccountId == x.Id
                                && m.TransactionDate > _Request.StartTime
                                && m.TransactionDate < _Request.EndTime)
                                .Count() > 1)
                                .Distinct()
                                .Count();



                                List<OOverview.AcquirerCollection> Acquirers = _HCoreContext.HCUAccount
                             .Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.StatusId == HelperStatus.Default.Active)
                             .Select(x => new OOverview.AcquirerCollection
                             {
                                 ReferenceId = x.Id,
                                 ReferenceKey = x.Guid,
                                 DisplayName = x.DisplayName,
                                 IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                 PurchaseAmount = 0,
                                 Terminals = 0,
                             }).ToList();
                                var Terminals = _HCoreContext.HCUAccountOwner
                                   .Where(x => x.OwnerId == AccountDetails.UserAccountId
                                   && x.AccountTypeId == Helpers.UserAccountType.Terminal)
                                   .Select(x => x.AccountId)
                                   .ToList();
                                foreach (var TerminalId in Terminals)
                                {
                                    long TerminalAcquirer = _HCoreContext.HCUAccountOwner
                                 .Where(x => x.Owner.AccountTypeId == Helpers.UserAccountType.Acquirer
                                 && x.AccountId == TerminalId
                                 && x.AccountTypeId == Helpers.UserAccountType.Terminal)
                                 .Select(x => x.OwnerId).FirstOrDefault();
                                    double PurchaseAmount = 0;
                                    if (_OOverviewResponse.ThankUCashPlus == 1)
                                    {
                                        PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                      .Where(m => m.ParentId == AccountDetails.UserAccountId
                                       && m.CreatedById == TerminalId
                                       && m.TypeId == TransactionType.CardReward
                                       && m.ModeId == Helpers.TransactionMode.Credit
                                       && m.TransactionDate > _Request.StartTime
                                       && m.TransactionDate < _Request.EndTime
                                      && m.ModeId == TransactionMode.Credit
                                  && m.SourceId == TransactionSource.ThankUCashPlus
                                       && m.StatusId == HelperStatus.Transaction.Success
                                        ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    else
                                    {
                                        PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                     .Where(m => m.ParentId == AccountDetails.UserAccountId
                                      && m.CreatedById == TerminalId
                                      && m.TypeId == TransactionType.CardReward
                                      && m.ModeId == Helpers.TransactionMode.Credit
                                      && m.TransactionDate > _Request.StartTime
                                      && m.TransactionDate < _Request.EndTime
                                     && m.ModeId == TransactionMode.Credit
                                 && m.SourceId == TransactionSource.TUC
                                      && m.StatusId == HelperStatus.Transaction.Success
                                       ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                    }
                                    if (Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer) != null)
                                    {
                                        Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).PurchaseAmount + PurchaseAmount;
                                        Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals = Acquirers.FirstOrDefault(x => x.ReferenceId == TerminalAcquirer).Terminals + 1;
                                    }
                                }



                                _OOverviewResponse.AcquirerAmountDistribution = Acquirers.Where(x => x.PurchaseAmount > 0).ToList();

                                _OOverviewResponse.StoresOverview = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.MerchantStore
                               && x.OwnerId == AccountDetails.UserAccountId)
                               .Select(x => new OOverview.StoresOverview
                               {
                                   ReferenceId = x.Id,
                                   ReferenceKey = x.Guid,
                                   DisplayName = x.DisplayName,
                                   Address = x.Address,
                                   Latitude = x.Latitude,
                                   Longitude = x.Longitude,
                               }).ToList();
                                foreach (var StoreInfo in _OOverviewResponse.StoresOverview)
                                {
                                    StoreInfo.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                      && m.SubParentId == StoreInfo.ReferenceId
                                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    StoreInfo.Transactions = _HCoreContext.HCUAccountTransaction
                                                                     .Count(m => m.ParentId == AccountDetails.UserAccountId
                                                                     && m.SubParentId == StoreInfo.ReferenceId
                                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                      && m.TransactionDate > _Request.StartTime
                                                                      && m.TransactionDate < _Request.EndTime
                                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                       );

                                    StoreInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                     && m.SubParentId == StoreInfo.ReferenceId
                                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                      && m.TransactionDate > _Request.StartTime
                                                                      && m.TransactionDate < _Request.EndTime
                                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                                }
                                _OOverviewResponse.StoresOverview = _OOverviewResponse.StoresOverview.OrderByDescending(x => x.Transactions).ToList();

                                _OOverviewResponse.TerminalsOverview = _HCoreContext.HCUAccountOwner.Where(x =>
                              x.AccountTypeId == UserAccountType.Terminal
                              && x.OwnerId == AccountDetails.UserAccountId)
                              .Select(x => new OOverview.TerminalsOverview
                              {
                                  ReferenceId = x.AccountId,
                                  ReferenceKey = x.Guid,
                                  DisplayName = x.Account.DisplayName,
                                  PtspName = x.Account.Owner.DisplayName,
                              }).ToList();
                                foreach (var TerminalInfo in _OOverviewResponse.TerminalsOverview)
                                {
                                    TerminalInfo.StoreDisplayName = _HCoreContext.HCUAccountOwner.Where(x =>
                                    x.AccountId == TerminalInfo.ReferenceId
                                    && x.Owner.AccountTypeId == UserAccountType.MerchantStore)
                                    .Select(x => x.Owner.DisplayName)
                                    .FirstOrDefault();

                                    TerminalInfo.PtspName = _HCoreContext.HCUAccountOwner.Where(x =>
                                   x.AccountId == TerminalInfo.ReferenceId
                                   && x.Owner.AccountTypeId == UserAccountType.PosAccount)
                                   .Select(x => x.Owner.DisplayName)
                                   .FirstOrDefault();

                                    TerminalInfo.AcquirerName = _HCoreContext.HCUAccountOwner.Where(x =>
                                   x.AccountId == TerminalInfo.ReferenceId
                                   && x.Owner.AccountTypeId == UserAccountType.Acquirer)
                                   .Select(x => x.Owner.DisplayName)
                                   .FirstOrDefault();

                                    TerminalInfo.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                     && m.CreatedById == TerminalInfo.ReferenceId
                                                                      && m.Account.AccountTypeId == UserAccountType.Appuser
                                                                      && m.TransactionDate > _Request.StartTime
                                                                      && m.TransactionDate < _Request.EndTime
                                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                       ).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault();
                                }
                                _OOverviewResponse.TerminalsOverview = _OOverviewResponse.TerminalsOverview.OrderByDescending(x => x.LastTransactionDate).ToList();


                                #region Get     Data
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();


                                _OOverviewResponse.RepeatingTransactions = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.Appuser
                               && x.HCUAccountTransactionAccount.Where(m => m.ParentId == AccountDetails.UserAccountId
                               && m.AccountId == x.Id
                               && m.TransactionDate > _Request.StartTime
                               && m.TransactionDate < _Request.EndTime)
                               .Count() > 1)
                               .Distinct()
                               .Count();


                                _OOverviewResponse.NewTransactions = _HCoreContext.HCUAccount.Where(x =>
                               x.AccountTypeId == UserAccountType.Appuser
                               && x.HCUAccountTransactionAccount.Where(m => m.ParentId == AccountDetails.UserAccountId
                               && m.AccountId == x.Id
                               && m.TransactionDate > _Request.StartTime
                               && m.TransactionDate < _Request.EndTime)
                               .Count() == 1)
                               .Distinct()
                               .Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.IssuerCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.TypeId == Helpers.TransactionType.ReferralBonus &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                   m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.Settlement
                                                                    ).Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                  m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.ModeId == Helpers.TransactionMode.Credit &&
                                                                  m.TransactionDate > _Request.StartTime &&
                                                                  m.TransactionDate < _Request.EndTime &&
                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.ParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.ParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }

                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.ParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                //if (_OOverviewResponse.CashRewardTransactions > 0)
                                //{

                                _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                        .Where(m => m.TypeId == TransactionType.CashReward
                                                                 && m.ParentId == AccountDetails.UserAccountId
                                                        && m.ModeId == TransactionMode.Credit
                                                         && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                        && m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                          ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CashReward
                                                                 && m.ParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                //}


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.ParentId == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                //if (_OOverviewResponse.CardRewardTransactions > 0)
                                //{
                                _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.TypeId == TransactionType.CardReward
                                                                 && m.ParentId == AccountDetails.UserAccountId
                                                                   && m.ModeId == Helpers.TransactionMode.Credit
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                 && m.ModeId == TransactionMode.Credit
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                    ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CardReward
                                                                 && m.ParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                //}
                                _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                         (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.ParentId == AccountDetails.UserAccountId
                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && m.SourceId == TransactionSource.TUC
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.ParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.ParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();


                                #endregion


                                _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.UserAccountId));
                                if (_OOverviewResponse.ThankUCashPlus > 0)
                                {
                                    //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.UserAccountId));
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", AccountDetails.OwnerId));
                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();

                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                              && x.Account.AccountTypeId == UserAccountType.Appuser
                              && x.Account.OwnerId == AccountDetails.OwnerId
                              && x.TransactionDate > _Request.StartTime
                              && x.TransactionDate < _Request.EndTime)
                              .Select(x => x.AccountId)
                              .Distinct()
                              .Count();

                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.SubParentId == AccountDetails.UserAccountId
                             && x.Account.AccountTypeId == UserAccountType.Appuser
                             && x.Account.OwnerId != AccountDetails.OwnerId
                             && x.TransactionDate > _Request.StartTime
                             && x.TransactionDate < _Request.EndTime)
                             .Select(x => x.AccountId)
                             .Distinct()
                             .Count();



                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.HCUAccountTransactionAccount.Where(m => m.SubParentId == AccountDetails.UserAccountId
                                && m.AccountId == x.Id
                                && m.TransactionDate > _Request.StartTime
                                && m.TransactionDate < _Request.EndTime)
                                .Count() > 1)
                                .Distinct()
                                .Count();

                                #region Get     Data
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;


                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.SubParentId == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }

                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                            && m.ModeId == TransactionMode.Credit
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                     && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                         (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && m.SourceId == TransactionSource.TUC
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.SubParentId == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();


                                #endregion


                                _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.OwnerId));
                                if (_OOverviewResponse.ThankUCashPlus > 0)
                                {
                                    //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.OwnerId));
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                long MerchantId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.Merchant).Select(x => x.OwnerId).FirstOrDefault();
                                long StoreId = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == AccountDetails.UserAccountId && x.Owner.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.OwnerId).FirstOrDefault();
                                _OOverviewResponse.ThankUCashPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", MerchantId));
                                _OOverviewResponse.AppUsersMale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.male" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();
                                _OOverviewResponse.AppUsersFemale = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId && x.Account.AccountTypeId == UserAccountType.Appuser && x.Account.Gender.SystemName == "gender.female" && x.TransactionDate > _Request.StartTime && x.TransactionDate < _Request.EndTime).Select(x => x.AccountId).Distinct().Count();

                                _OOverviewResponse.AppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                                && x.Account.AccountTypeId == UserAccountType.Appuser
                                && x.TransactionDate > _Request.StartTime
                                && x.TransactionDate < _Request.EndTime)
                                .Select(x => x.AccountId)
                                .Distinct()
                                .Count();

                                _OOverviewResponse.OwnAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                              && x.Account.AccountTypeId == UserAccountType.Appuser
                              && x.Account.OwnerId == MerchantId
                              && x.TransactionDate > _Request.StartTime
                              && x.TransactionDate < _Request.EndTime)
                              .Select(x => x.AccountId)
                              .Distinct()
                              .Count();

                                _OOverviewResponse.ReferralAppUsers = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == AccountDetails.UserAccountId
                             && x.Account.AccountTypeId == UserAccountType.Appuser
                             && x.Account.OwnerId != MerchantId
                             && x.TransactionDate > _Request.StartTime
                             && x.TransactionDate < _Request.EndTime)
                             .Select(x => x.AccountId)
                             .Distinct()
                             .Count();



                                _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.HCUAccountTransactionAccount.Where(m => m.CreatedById == AccountDetails.UserAccountId
                                && m.AccountId == x.Id
                                && m.TransactionDate > _Request.StartTime
                                && m.TransactionDate < _Request.EndTime)
                                .Count() > 1)
                                .Distinct()
                                .Count();

                                #region Get     Data
                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Count();

                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.Charge = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;


                                _OOverviewResponse.RewardUserAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                       m.StatusId == HelperStatus.Transaction.Success &&
                                                                       m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                       m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                       m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                       m.TransactionDate < _Request.EndTime &&
                                                                       m.SourceId == TransactionSource.TUC)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;

                                _OOverviewResponse.RewardChargeAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                      m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                      m.TransactionDate > _Request.StartTime &&
                                                                      m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                     .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0;

                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                          .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.TypeId != TransactionType.ThankUCashPlusCredit &&
                                                                        m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                if (_OOverviewResponse.ThankUCashPlus == 1)
                                {

                                    _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                    m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                    m.TransactionDate < _Request.EndTime &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusUserRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                        m.StatusId == HelperStatus.Transaction.Success &&
                                                                        m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                        m.TransactionDate < _Request.EndTime &&
                                                                        m.SourceId == TransactionSource.ThankUCashPlus)
                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0;
                                    _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                                .Where(m =>
                                                                                m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                               .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                   m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                                 m.SourceId == TransactionSource.ThankUCashPlus)
                                                                               .Count();

                                    _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                        .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.ThankUCashPlus)
                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0;

                                    _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Where(m => m.CreatedById == AccountDetails.UserAccountId &&
                                                                                 m.StatusId == HelperStatus.Transaction.Success &&
                                                                               m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                 m.TransactionDate > _Request.StartTime &&
                                                                                 m.TransactionDate < _Request.EndTime &&
                                                                               m.SourceId == TransactionSource.ThankUCashPlus)
                                                                             .Count();
                                }

                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                            && m.ModeId == TransactionMode.Credit
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CashReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                     && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.TypeId == TransactionType.CardReward
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m =>
                                                                         (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                          && m.TransactionDate > _Request.StartTime
                                                                          && m.TransactionDate < _Request.EndTime
                                                                          && m.SourceId == TransactionSource.TUC
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m =>
                                                                        (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                     && m.CreatedById == AccountDetails.UserAccountId
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                        && m.SourceId == TransactionSource.TUC
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();


                                #endregion


                                _OOverviewResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", AccountDetails.OwnerId));
                                if (_OOverviewResponse.ThankUCashPlus > 0)
                                {
                                    //_OOverviewResponse.ThankUCashPlusForMerchant = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusformerchant", UserAccountId));
                                    _OOverviewResponse.ThankUCashPlusBalanceValidity = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplusbalancevalidity", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinRedeemAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusminredeemamount", AccountDetails.OwnerId));
                                    _OOverviewResponse.ThankUCashPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", AccountDetails.OwnerId));
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details loaded");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                #region Get Data

                                _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Count();
                                _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                              && m.TransactionDate > _Request.StartTime
                                                              && m.TransactionDate < _Request.EndTime
                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                            ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.ThankUCashPlus)
                                                                           .Count();

                                _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                          m.StatusId == HelperStatus.Transaction.Success &&
                                                                          m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                          m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0;

                                _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                           m.StatusId == HelperStatus.Transaction.Success &&
                                                                           m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                           m.SourceId == TransactionSource.ThankUCashPlus)
                                                                         .Count();



                                _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                         m.ModeId == Helpers.TransactionMode.Credit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                              m.ModeId == Helpers.TransactionMode.Credit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();



                                _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                         m.StatusId == HelperStatus.Transaction.Success &&
                                                                         m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                         m.ModeId == Helpers.TransactionMode.Debit &&
                                                                           m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                         m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0;
                                _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                              m.StatusId == HelperStatus.Transaction.Success &&
                                                                              m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                              m.ModeId == Helpers.TransactionMode.Debit &&
                                                                                m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                              m.SourceId == TransactionSource.TUC)
                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                                _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                               m.TransactionDate > _Request.StartTime &&
                                                                             m.TransactionDate < _Request.EndTime &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                           .Count();


                                _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Credit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                             m.StatusId == HelperStatus.Transaction.Success &&
                                                                             m.ModeId == Helpers.TransactionMode.Debit &&
                                                                             m.SourceId == TransactionSource.TUC)
                                                                       .Sum(m => (double?)m.TotalAmount) ?? 0);


                                _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Credit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                                            m.StatusId == HelperStatus.Transaction.Success &&
                                                                            m.ModeId == Helpers.TransactionMode.Debit &&
                                                                            m.SourceId == TransactionSource.ThankUCashPlus)
                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0);



                                _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                        && m.TypeId == TransactionType.CashReward
                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                        && m.TransactionDate > _Request.StartTime
                                                                        && m.TransactionDate < _Request.EndTime
                                                                       && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                         ).Count();
                                if (_OOverviewResponse.CashRewardTransactions > 0)
                                {

                                    _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId &&
                                                            m.TypeId == TransactionType.CashReward
                                                            && m.ModeId == TransactionMode.Credit
                                                             && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                              ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CashReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                        && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }


                                // Card Reward
                                _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                       && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.ModeId == TransactionMode.Credit
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                                if (_OOverviewResponse.CardRewardTransactions > 0)
                                {
                                    _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && m.ModeId == Helpers.TransactionMode.Credit
                                                                       && m.TransactionDate > _Request.StartTime
                                                                       && m.TransactionDate < _Request.EndTime
                                                                     && m.ModeId == TransactionMode.Credit
                                                                   && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                                    _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                           .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                            && m.TypeId == TransactionType.CardReward
                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                            && m.TransactionDate > _Request.StartTime
                                                                            && m.TransactionDate < _Request.EndTime
                                                                           && m.ModeId == TransactionMode.Credit
                                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                             ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                                #endregion

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OOverviewResponse, "HC0001", "Details not found");
                            #endregion
                        }
                    }
                    else
                    {
                        _OOverviewResponse.AppUsers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                          && x.CreateDate > _Request.StartTime
                          && x.CreateDate < _Request.EndTime)
                          .Count();

                        _OOverviewResponse.RepeatingAppUsers = _HCoreContext.HCUAccount.Where(x =>
                         x.AccountTypeId == UserAccountType.Appuser
                         && x.HCUAccountTransactionAccount.Where(m => m.AccountId == x.Id
                         && m.TransactionDate > _Request.StartTime
                         && m.TransactionDate < _Request.EndTime)
                         .Count() > 1)
                         .Distinct()
                         .Count();
                        _OOverviewResponse.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartTime
                                                        && m.TransactionDate < _Request.EndTime
                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Count();
                        _OOverviewResponse.PurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartTime
                                                      && m.TransactionDate < _Request.EndTime
                                                      && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                    ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.TUCPlusRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.ThankUCashPlus)
                                                                   .Count();

                        _OOverviewResponse.TUCPlusRewardClaimedAmount = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                  m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                  m.SourceId == TransactionSource.ThankUCashPlus)
                                                            .Sum(m => (double?)m.TotalAmount) ?? 0;

                        _OOverviewResponse.TUCPlusRewardClaimedTransactions = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                   m.ModeId == Helpers.TransactionMode.Debit &&
                                                                     m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                   m.SourceId == TransactionSource.ThankUCashPlus)
                                                                 .Count();



                        _OOverviewResponse.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                 m.ModeId == Helpers.TransactionMode.Credit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                      m.ModeId == Helpers.TransactionMode.Credit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.Type.SubParentId == TransactionTypeCategory.Reward &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                                   .Count();



                        _OOverviewResponse.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                 m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                 m.ModeId == Helpers.TransactionMode.Debit &&
                                                                   m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                 m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0;
                        _OOverviewResponse.RedeemPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                      m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                      m.ModeId == Helpers.TransactionMode.Debit &&
                                                                        m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                      m.SourceId == TransactionSource.TUC)
                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0;

                        _OOverviewResponse.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.Type.SubParentId == TransactionTypeCategory.Redeem &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                       m.TransactionDate > _Request.StartTime &&
                                                                     m.TransactionDate < _Request.EndTime &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                                   .Count();


                        _OOverviewResponse.Balance = (_HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Credit &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                     m.ModeId == Helpers.TransactionMode.Debit &&
                                                                     m.SourceId == TransactionSource.TUC)
                                                               .Sum(m => (double?)m.TotalAmount) ?? 0);


                        _OOverviewResponse.TUCPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Credit &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.StatusId == HelperStatus.Transaction.Success &&
                                                                    m.ModeId == Helpers.TransactionMode.Debit &&
                                                                    m.SourceId == TransactionSource.ThankUCashPlus)
                                                              .Sum(m => (double?)m.TotalAmount) ?? 0);



                        _OOverviewResponse.CashRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.TypeId == TransactionType.CashReward
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                               && m.ModeId == TransactionMode.Credit
                                                            && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        if (_OOverviewResponse.CashRewardTransactions > 0)
                        {

                            _OOverviewResponse.CashRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.TypeId == TransactionType.CashReward
                                                    && m.ModeId == TransactionMode.Credit
                                                     && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                    && m.TransactionDate > _Request.StartTime
                                                    && m.TransactionDate < _Request.EndTime
                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                      ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CashRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CashReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }


                        // Card Reward
                        _OOverviewResponse.CardRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                               && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.ModeId == TransactionMode.Credit
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Count();
                        if (_OOverviewResponse.CardRewardTransactions > 0)
                        {
                            _OOverviewResponse.CardRewardAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.TypeId == TransactionType.CardReward
                                                               && m.ModeId == Helpers.TransactionMode.Credit
                                                               && m.TransactionDate > _Request.StartTime
                                                               && m.TransactionDate < _Request.EndTime
                                                             && m.ModeId == TransactionMode.Credit
                                                           && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                                ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                            _OOverviewResponse.CardRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.TypeId == TransactionType.CardReward
                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                   && m.ModeId == TransactionMode.Credit
                                                                    && (m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUC)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        }
                        _OOverviewResponse.OtherRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                 .Where(m =>
                                                                 (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                  && m.TransactionDate > _Request.StartTime
                                                                  && m.TransactionDate < _Request.EndTime
                                                                  && m.SourceId == TransactionSource.TUC
                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0;
                        _OOverviewResponse.OtherRewardPurchaseAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m =>
                                                                (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        _OOverviewResponse.OtherRewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(m =>
                                                                (m.TypeId != TransactionType.CardReward || m.TypeId == TransactionType.CashReward)
                                                                && m.ModeId == Helpers.TransactionMode.Credit
                                                                && m.TransactionDate > _Request.StartTime
                                                                && m.TransactionDate < _Request.EndTime
                                                                && m.SourceId == TransactionSource.TUC
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OOverviewResponse, "HC0001", "Details not found");
                        #endregion
                    }


                    //_OOverviewResponse.Transactions = _OOverviewResponse.RewardTransactions + _OOverviewResponse.RedeemTransactions;
                    //_OOverviewResponse.Balance = _OOverviewResponse.RewardAmount - _OOverviewResponse.RedeemAmount;

                    //_OOverviewResponse.LastTransaction = _HCoreContext.HCUAccountTransaction.Where(x =>
                    //x.UserAccountId == UserAccountId
                    //&& x.StatusId == HelperStatus.Transaction.Success
                    //&& (x.Type.SubParentId == TransactionTypeCategory.Redeem || x.Type.SubParentId == TransactionTypeCategory.Reward)
                    //&& (x.ModeId == Helpers.TransactionMode.Credit || x.ModeId == Helpers.TransactionMode.Debit)
                    //&& x.SourceId == TransactionSource.TUC)
                    //.Select(x => new OOverview.MiniTransaction
                    //{
                    //    ReferenceId = x.Id,
                    //    TypeName = x.Type.Name,
                    //    InvoiceAmount = x.PurchaseAmount,
                    //    MerchantName = x.Parent.DisplayName,
                    //    RewardAmount = x.TotalAmount,
                    //    TransactionDate = x.TransactionDate
                    //})
                    //.FirstOrDefault();

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the visit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVisitHistory(OOverview.Request _Request)
        {
            #region Intialize Object
            _OChartRequest = new OChart.Request();
            _OChartRequestRange = new List<OChart.RequestDateRange>();
            #endregion
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    int CurrentHour = HCoreHelper.GetGMTDateTime().Hour + 2;
                    int HoursDifference = (_Request.EndTime - _Request.StartTime).Days;
                    if (HoursDifference != 0)
                    {
                        CurrentHour = 25;
                    }
                    if (!string.IsNullOrEmpty(_Request.UserAccountKey))
                    {
                        long SubUserAccountId = 0;
                        long SubUserAccountTypeId = 0;
                        if (!string.IsNullOrEmpty(_Request.SubUserAccountKey))
                        {
                            var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubUserAccountKey)
                             .Select(x => new
                             {
                                 UserAccountId = x.Id,
                                 AccountTypeId = x.AccountTypeId,
                             }).FirstOrDefault();
                            if (SubUserAccountDetails != null)
                            {
                                SubUserAccountId = SubUserAccountDetails.UserAccountId;
                                SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.UserAccountKey)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                for (int i = 0; i < 24; i++)
                                {
                                    long Transactions = 0;
                                    DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                                    int StartHour = i;
                                    if (StartHour < CurrentHour)
                                    {
                                        if (SubUserAccountId != 0)
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                             && m.AccountId == SubUserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        else
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.ParentId == AccountDetails.UserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = Transactions.ToString(),
                                        });
                                    }
                                    else
                                    {
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = "0",
                                        });
                                    }
                                    _OChartRequestRange.Add(new OChart.RequestDateRange()
                                    {
                                        StartTime = TStartTime,
                                        EndTime = TEndTime,
                                        Label = i.ToString(),
                                        Data = _OChartRequestRangeValue
                                    });
                                }
                                #region Build Response
                                _OChartRequest.DateRange = _OChartRequestRange;
                                _OChartRequest.StartTime = _Request.StartTime;
                                _OChartRequest.EndTime = _Request.EndTime;
                                _OChartRequest.Type = "";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                for (int i = 0; i < 24; i++)
                                {
                                    long Transactions = 0;
                                    DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                                    int StartHour = i;
                                    if (StartHour < CurrentHour)
                                    {
                                        if (SubUserAccountId != 0)
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                             && m.AccountId == SubUserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        else
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.SubParentId == AccountDetails.UserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = Transactions.ToString(),
                                        });
                                    }
                                    else
                                    {
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = "0",
                                        });
                                    }
                                    _OChartRequestRange.Add(new OChart.RequestDateRange()
                                    {
                                        StartTime = TStartTime,
                                        EndTime = TEndTime,
                                        Label = i.ToString(),
                                        Data = _OChartRequestRangeValue
                                    });
                                }
                                #region Build Response
                                _OChartRequest.DateRange = _OChartRequestRange;
                                _OChartRequest.StartTime = _Request.StartTime;
                                _OChartRequest.EndTime = _Request.EndTime;
                                _OChartRequest.Type = "";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Terminal)
                            {
                                for (int i = 0; i < 24; i++)
                                {
                                    long Transactions = 0;
                                    DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                                    int StartHour = i;
                                    if (StartHour < CurrentHour)
                                    {
                                        if (SubUserAccountId != 0)
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                             && m.AccountId == SubUserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        else
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.CreatedById == AccountDetails.UserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = Transactions.ToString(),
                                        });
                                    }
                                    else
                                    {
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = "0",
                                        });
                                    }
                                    _OChartRequestRange.Add(new OChart.RequestDateRange()
                                    {
                                        StartTime = TStartTime,
                                        EndTime = TEndTime,
                                        Label = i.ToString(),
                                        Data = _OChartRequestRangeValue
                                    });
                                }
                                #region Build Response
                                _OChartRequest.DateRange = _OChartRequestRange;
                                _OChartRequest.StartTime = _Request.StartTime;
                                _OChartRequest.EndTime = _Request.EndTime;
                                _OChartRequest.Type = "";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.PosAccount)
                            {
                                for (int i = 0; i < 24; i++)
                                {
                                    long Transactions = 0;
                                    DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                                    int StartHour = i;
                                    if (StartHour < CurrentHour)
                                    {
                                        if (SubUserAccountId != 0)
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.CreatedBy.OwnerId == AccountDetails.UserAccountId
                                                             && m.AccountId == SubUserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        else
                                        {
                                            Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.CreatedBy.OwnerId == AccountDetails.UserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        }
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = Transactions.ToString(),
                                        });
                                    }
                                    else
                                    {
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = "0",
                                        });
                                    }
                                    _OChartRequestRange.Add(new OChart.RequestDateRange()
                                    {
                                        StartTime = TStartTime,
                                        EndTime = TEndTime,
                                        Label = i.ToString(),
                                        Data = _OChartRequestRangeValue
                                    });
                                }
                                #region Build Response
                                _OChartRequest.DateRange = _OChartRequestRange;
                                _OChartRequest.StartTime = _Request.StartTime;
                                _OChartRequest.EndTime = _Request.EndTime;
                                _OChartRequest.Type = "";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                                #endregion
                            }
                            else if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                for (int i = 0; i < 24; i++)
                                {
                                    long Transactions = 0;
                                    DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                                    DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                                    int StartHour = i;
                                    if (StartHour < CurrentHour)
                                    {
                                        Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                             && m.TransactionDate > _Request.StartTime
                                                             && m.TransactionDate < _Request.EndTime
                                                             && m.TransactionDate.Hour == StartHour
                                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                             && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             && m.StatusId == HelperStatus.Transaction.Success
                                                           ).Count();
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = Transactions.ToString(),
                                        });
                                    }
                                    else
                                    {
                                        _OChartRequestRangeValue = new List<OChart.RequestData>();
                                        _OChartRequestRangeValue.Add(new OChart.RequestData
                                        {
                                            Name = i.ToString(),
                                            Value = "0",
                                        });
                                    }
                                    _OChartRequestRange.Add(new OChart.RequestDateRange()
                                    {
                                        StartTime = TStartTime,
                                        EndTime = TEndTime,
                                        Label = i.ToString(),
                                        Data = _OChartRequestRangeValue
                                    });
                                }
                                #region Build Response
                                _OChartRequest.DateRange = _OChartRequestRange;
                                _OChartRequest.StartTime = _Request.StartTime;
                                _OChartRequest.EndTime = _Request.EndTime;
                                _OChartRequest.Type = "";
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OChartRequest, "HC1086");
                            #endregion
                        }

                    }
                    else
                    {

                        for (int i = 0; i < 24; i++)
                        {
                            DateTime TStartTime = new DateTime(2018, 1, 1, 0, 0, 0).AddHours(i);
                            DateTime TEndTime = TStartTime.AddHours(1).AddSeconds(-1);
                            int StartHour = i;
                            if (StartHour < CurrentHour)
                            {
                                long Transactions = _HCoreContext.HCUAccountTransaction
                                                    .Where(m => m.TransactionDate > _Request.StartTime
                                                     && m.TransactionDate < _Request.EndTime
                                                     && m.TransactionDate.Hour == StartHour
                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                     && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                   ).Count();
                                _OChartRequestRangeValue = new List<OChart.RequestData>();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = i.ToString(),
                                    Value = Transactions.ToString(),
                                });
                            }
                            else
                            {
                                _OChartRequestRangeValue = new List<OChart.RequestData>();
                                _OChartRequestRangeValue.Add(new OChart.RequestData
                                {
                                    Name = i.ToString(),
                                    Value = "0",
                                });
                            }
                            _OChartRequestRange.Add(new OChart.RequestDateRange()
                            {
                                StartTime = TStartTime,
                                EndTime = TEndTime,
                                Label = i.ToString(),
                                Data = _OChartRequestRangeValue
                            });
                        }
                        #region Build Response
                        _OChartRequest.DateRange = _OChartRequestRange;
                        _OChartRequest.StartTime = _Request.StartTime;
                        _OChartRequest.EndTime = _Request.EndTime;
                        _OChartRequest.Type = "";
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OChartRequest, "HC1086");
                        #endregion

                    }






                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
    }

}