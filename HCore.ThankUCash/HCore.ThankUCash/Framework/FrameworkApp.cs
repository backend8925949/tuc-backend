//==================================================================================
// FileName: FrameworkApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to app and it's features
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.ThankUCash.Object;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using HCore.Operations.Object;
using HCore.Operations;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkApp
    {
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        public class PayStackResponse
        {
            public bool id { get; set; }
            public PayStackResponseData data { get; set; }
        }
        public class PayStackResponseData
        {
            public string? status { get; set; }
            public long amount { get; set; }
            public Authorization authorization { get; set; }
        }
        public class Authorization
        {
            public string? authorization_code { get; set; }
            public string? bin { get; set; }
            public string? last4 { get; set; }
            public string? exp_month { get; set; }
            public string? exp_year { get; set; }
            public string? channel { get; set; }
            public string? card_type { get; set; }
            public string? bank { get; set; }
            public string? country_code { get; set; }
            public string? brand { get; set; }
            public bool reusable { get; set; }
            public string? signature { get; set; }
            public string? account_name { get; set; }
            public string? receiver_bank_account_number { get; set; }
            public string? receiver_bank { get; set; }
        }

        public class PaymentResource
        {
            public const string PR2123 = "PR2123";
            public const string PR2123M = "Transaction details not found";
            public const string PR2124 = "PR2124";
            public const string PR2124M = "Amount received. Thank U for making payments towards donation";

            public const string PR2125 = "PR2124";
            public const string PR2125M = "Payment successful";


            public const string PR2126 = "PR2126";
            public const string PR2126M = "Payment successful";

            public const string PR2127 = "PR2127";
            public const string PR2127M = "Payment option not available at the moment";

            public const string PR2128 = "PR2128";
            public const string PR2128M = "Billers loaded";

            public const string PR2129 = "PR2129";
            public const string PR2129M = "Payment failed. Please contact support";

            public const string PR2130 = "PR2130";
            public const string PR2130M = "Topup  successful";

            public const string PR2131 = "PR2131";
            public const string PR2131M = "Topup  failed. Please contact support. Your amount will be refunded to your account within 24 hours";

        }
        OApp.OAppConfiguration.Donation _DonationDetails;
        List<OApp.OAppConfiguration.DonationRange> _DonationRanges;
        List<OApp.OAppConfiguration.TucMileStone> _TucMileStones;
        int donationconfig = 476;
        int donationreceiverconfig = 477;
        int donationparameters = 478;
        internal int SyncInterval = 6;
        HCoreContext _HCoreContext;
        OApp.OAppConfiguration.Response _AppConfigResponse;
        internal static OApp.OAppCache _AppCache = new OApp.OAppCache();
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategories(OList.Request _Request)
        {
            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
            int CacheDiff = (CurrentTime - _AppCache.CategoriesCacheTime).Hours;
            #region Manage Exception
            try
            {
                if (CacheDiff > SyncInterval || _AppCache.Categories == null)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _AppCache.CategoriesCacheTime = HCoreHelper.GetGMTDateTime();
                        _AppCache.Categories = _HCoreContext.TUCMerchantCategory.Where(x =>
                          x.StatusId == HelperStatus.Default.Active
                          && x.RootCategory.ParentCategoryId == null)
                        .OrderBy(x => x.RootCategory.Name)
                         .Select(x => new OApp.Category
                         {
                             IconUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                             ReferenceId = x.RootCategoryId,
                             Name = x.RootCategory.Name,
                             //IconName = x.RootCategory.Value,
                         }).ToList();
                        _HCoreContext.Dispose();
                    }
                }
                #region Create  Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AppCache.Categories, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
            int CacheDiff = (CurrentTime - _AppCache.MerchantsCacheTime).Hours;
            #region Manage Exception
            try
            {
                if (CacheDiff > SyncInterval || _AppCache.Merchants == null)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _AppCache.MerchantsCacheTime = HCoreHelper.GetGMTDateTime();
                        #region Get Data
                        _AppCache.Merchants = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Merchant
                        && x.StatusId == HelperStatus.Default.Active
                        && (x.Id != 3 && x.Id != 971 && x.Id != 10 && x.Id != 6967)
                        && x.IconStorageId != null
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.HCUAccountParameterAccount
                        .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                        .FirstOrDefault().Value != "0")
                        .OrderByDescending(x => x.HCUAccountTransactionParent.Count())
                        .Select(x => new OApp.Merchant
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.Name,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            CountValue = x.CountValue,
                            AverageValue = x.AverageValue,
                        }).ToList();
                        #endregion
                        #region Get Data
                        _AppCache.MerchantCategories = _HCoreContext.HCUAccountParameter
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Merchant
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.TypeId == 205
                        && x.Account.CountryId == _Request.UserReference.CountryId
                                                    && (x.AccountId != 3 && x.AccountId != 971 && x.AccountId != 10 && x.AccountId != 6967)
                                                    && x.Account.IconStorageId != null
                                                     && x.Account.HCUAccountParameterAccount
                                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                    .FirstOrDefault().Value != "0")
                                                    .OrderBy(x => x.Common.Name)
                                                    .Select(x => new OApp.MerchantCategory
                                                    {
                                                        CategoryId = x.CommonId,
                                                        MerchantId = x.AccountId,
                                                    }).ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _AppCache.Merchants)
                        {
                            DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        #endregion
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AppCache.Merchants, _Request.Offset, _Request.Limit);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the merchant categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantCategories(OList.Request _Request)
        {
            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
            int CacheDiff = (CurrentTime - _AppCache.MerchantsCacheTime).Hours;
            #region Manage Exception
            try
            {
                if (CacheDiff > SyncInterval || _AppCache.Merchants == null)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _AppCache.MerchantsCacheTime = HCoreHelper.GetGMTDateTime();
                        #region Get Data
                        _AppCache.Merchants = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Merchant
                        && x.StatusId == HelperStatus.Default.Active
                        && (x.Id != 3 && x.Id != 971 && x.Id != 10 && x.Id != 6967)
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.IconStorageId != null
                        && x.HCUAccountParameterAccount
                        .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                        .FirstOrDefault().Value != "0")
                        .OrderByDescending(x => x.HCUAccountTransactionParent.Count())
                        .Select(x => new OApp.Merchant
                        {
                            ReferenceId = x.Id,
                            DisplayName = x.DisplayName,
                            Name = x.Name,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            CountValue = x.CountValue,
                            AverageValue = x.AverageValue,
                        }).ToList();
                        #endregion
                        #region Get Data
                        _AppCache.MerchantCategories = _HCoreContext.HCUAccountParameter
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Merchant
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.TypeId == 205
                                                    && (x.AccountId != 3 && x.AccountId != 971 && x.AccountId != 10 && x.AccountId != 6967)
                                                    && x.Account.IconStorageId != null
                                                     && x.Account.HCUAccountParameterAccount
                                                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                                                    .FirstOrDefault().Value != "0")
                                                    .OrderBy(x => x.Common.Name)
                                                    .Select(x => new OApp.MerchantCategory
                                                    {
                                                        CategoryId = x.CommonId,
                                                        MerchantId = x.AccountId,
                                                    }).ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _AppCache.Merchants)
                        {
                            DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        #endregion
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AppCache.MerchantCategories, _Request.Offset, _Request.Limit);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantCategories", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the store list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStores(OList.Request _Request)
        {
            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
            int CacheDiff = (CurrentTime - _AppCache.StoresCacheTime).Hours;
            #region Manage Exception
            try
            {
                if (CacheDiff > SyncInterval || _AppCache.Stores == null)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _AppCache.StoresCacheTime = HCoreHelper.GetGMTDateTime();
                        #region Get Data
                        _AppCache.Stores = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                        && x.CountryId == _Request.UserReference.CountryId
                    && x.StatusId == HelperStatus.Default.Active
                    && x.Owner.StatusId == HelperStatus.Default.Active
                    && (x.OwnerId != 3 && x.OwnerId != 971 && x.OwnerId != 10 && x.OwnerId != 6967)
                    && x.Owner.IconStorageId != null
                     && x.Owner.HCUAccountParameterAccount
                    .Where(m => m.StatusId == HelperStatus.Default.Active && m.Common.SystemName == "rewardpercentage" && m.TypeId == HelperType.ConfigurationValue)
                    .FirstOrDefault().Value != "0")
                        .OrderByDescending(x => x.HCUAccountTransactionSubParent.Count())
                    .Select(x => new OApp.Store
                    {
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        MerchantId = x.OwnerId,
                        DisplayName = x.DisplayName,
                        ContactNumber = x.ContactNumber,
                        EmailAddress = x.EmailAddress,
                        Address = x.Address,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                        CountValue = x.CountValue,
                        AverageValue = x.AverageValue,
                    }).ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion

                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AppCache.Stores, _Request.Offset, _Request.Limit);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStores", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppConfiguration(OApp.OAppConfiguration.Request _Request)
        {


            _AppConfigResponse = new OApp.OAppConfiguration.Response();
            _AppConfigResponse.IsAppAvailable = true;
            _AppConfigResponse.IsReceiverAvailable = true;
            if (_Request.OsName.ToLower() == "android")
            {
                _AppConfigResponse.ServerAppVersion = "1.0.7";
            }
            else if (_Request.OsName.ToLower() == "ios")
            {
                _AppConfigResponse.ServerAppVersion = "1.0.7";
            }
            else
            {
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                #endregion
            }
            _TucMileStones = new List<OApp.OAppConfiguration.TucMileStone>();
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Purple",
                SystemName = "purple",
                Description = "You get registered to receive rewards from all ThankUCash merchant partners and you also get access to information on Deals and Utilities. <br> <br> Purple class is our entry level and once you reach N50,000 in accumulated spend you will graduate to the Bronze Class.",
                Limit = 50000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Bronze",
                SystemName = "bronze",
                Description = "You get every benefit of Purple class and also:  <br> <br> Get instant Rewards for attaining the bronze class.   <br> <br> Get access to bronze class type rewards and also early notifications to deals.   <br> <br> Once you have reached a total of N200,000 accumulated spend you will graduate to the Silver Class.",
                Limit = 200000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Silver",
                SystemName = "silver",
                Description = "You get every benefit of Bronze class and also:   <br> <br> Get instant Rewards for attaining the Silver class, which you can choose from a list on your phone screen.   <br> <br> Get access to Silver class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Silver Class programs.   <br> <br> Once you have reached a total of N750,000 accumulated spend you will graduate to the Gold Class.",
                Limit = 750000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Gold",
                SystemName = "gold",
                Description = "You get every benefit of Silver class and also:  <br> <br>Get instant Rewards for attaining the Gold class, which you can choose from a list on your phone screen. <br> <br>Get access to Gold class type rewards and also early notifications to deals with rights higher than previous classes. <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Gold member. <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Gold Class programs. <br> <br> Once you have reached a total of N3,000,000 accumulated spend you will graduate to the Diamond Class.",
                Limit = 3000000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Diamond",
                SystemName = "diamond",
                Description = "You get every benefit of Gold class and also: <br> <br> Get instant Rewards for attaining the Diamond class, which you can choose from a list on your phone screen.  <br> <br> Get access to Diamond class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Diamond member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruse liners. <br> <br> You get Hotel get away and more. You earn more on rewards percentage and have access to Airport partner lounges and early access to Diamond Class programs with special lunch at particular partner lounges.  <br> <br> Once you have reached a total of N5,000,000 accumulated spend you will graduate to the Platinum Class.",
                Limit = 5000000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Platinum",
                SystemName = "platinum",
                Description = "You get every benefit of Diamond class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Get access to Platinum class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas, free car checkup, home delivery and also Monthly rewards for being a Platinum member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruise liners.  <br> <br> You get Hotel get away and more.  You earn more on rewards percentage and have access to Airport partner lounges, car rentals and early access to Platinum Class programs with special lunch at particular partner lounges. <br> <br> Once you have reached a total of N15,000,000 accumulated spend you will graduate to the Platinum Plus Class. ",
                Limit = 15000000
            });
            _TucMileStones.Add(new OApp.OAppConfiguration.TucMileStone
            {
                Title = "Platinum Plus",
                SystemName = "platinumplus",
                Description = "You get every benefit of Platinum class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Personalized gifts from our partners and also get Free Tickets to global networking events.  <br> <br> Also get access to our partner Platinum Airport Network and free upgrades on our Airline partners.",
                Limit = 30000000
            });
            _AppConfigResponse.TucMileStones = _TucMileStones;
            using (_HCoreContext = new HCoreContext())
            {
                _DonationDetails = new OApp.OAppConfiguration.Donation();
                _DonationDetails.PaymentReference = "DN" + _Request.UserReference.AccountId + "O" + HCoreHelper.GenerateRandomNumber(8);
                OAppManager.RelifDetailsResponse DonationConfig = _HCoreContext.HCCoreParameter
                       .Where(x => x.TypeId == donationconfig)
                       .Select(x => new OAppManager.RelifDetailsResponse
                       {
                           Title = x.Name,
                           ShortDescription = x.SystemName,
                           Description = x.Description,
                           Conditions = x.Data,
                           IsFeatureEnable = x.Sequence,
                           IconUrl = x.IconStorage.Path,
                       })
                      .FirstOrDefault();
                if (DonationConfig != null)
                {
                    if (DonationConfig.IsFeatureEnable == 1)
                    {
                        _AppConfigResponse.IsDonationAvailable = true;
                    }
                    else
                    {
                        _AppConfigResponse.IsDonationAvailable = false;
                    }
                    if (!string.IsNullOrEmpty(DonationConfig.Title))
                    {
                        _DonationDetails.Title = DonationConfig.Title;
                    }
                    else
                    {
                        _DonationDetails.Title = "Support COVID-19 Relief";
                    }
                    if (!string.IsNullOrEmpty(DonationConfig.ShortDescription))
                    {
                        _DonationDetails.Message = DonationConfig.ShortDescription;
                    }
                    else
                    {
                        _DonationDetails.Message = "Supported 80 families 1000 families are waiting.";
                    }
                    if (!string.IsNullOrEmpty(DonationConfig.ShortDescription))
                    {
                        _DonationDetails.Description = DonationConfig.Description;
                    }
                    else
                    {
                        _DonationDetails.Description = "100% of your contribution goes to the nonprofit. You can support between 1 to 100 families. Just donate 7500 per family, we will make sure that your help reaches a needy family.";
                    }
                    if (!string.IsNullOrEmpty(DonationConfig.Conditions))
                    {
                        _DonationDetails.Conditions = DonationConfig.Conditions;
                    }
                    else
                    {
                        _DonationDetails.Conditions = "100% of your contribution goes to the nonprofit. You can support between 1 to 100 families. Just donate 7500 per family, we will make sure that your help reaches a needy family.";
                    }
                    if (!string.IsNullOrEmpty(DonationConfig.IconUrl))
                    {
                        _DonationDetails.IconUrl = _AppConfig.StorageUrl + DonationConfig.IconUrl;
                    }
                    else
                    {
                        _DonationDetails.IconUrl = "http://cdn.thankucash.com/temp/virus.png";
                    }
                }
                else
                {
                    _AppConfigResponse.IsDonationAvailable = false;
                    _DonationDetails.Title = "Support COVID-19 Relief";
                    _DonationDetails.Message = "Supported 80 families 1000 families are waiting.";
                    _DonationDetails.Description = "100% of your contribution goes to the nonprofit. You can support between 1 to 100 families. Just donate 7500 per family, we will make sure that your help reaches a needy family.";
                    _DonationDetails.Conditions = "";
                    _DonationDetails.IconUrl = "http://cdn.thankucash.com/temp/virus.png";
                }

                OAppManager.RelifDetailsResponse ReceiverConfig = _HCoreContext.HCCoreParameter
               .Where(x => x.TypeId == donationreceiverconfig)
               .Select(x => new OAppManager.RelifDetailsResponse
               {
                   Title = x.Name,
                   ShortDescription = x.SystemName,
                   Description = x.Description,
                   Conditions = x.Data,
                   IsFeatureEnable = x.Sequence,
                   IconUrl = x.IconStorage.Path,
               })
              .FirstOrDefault();
                if (ReceiverConfig != null)
                {
                    if (ReceiverConfig.IsFeatureEnable == 1)
                    {
                        _AppConfigResponse.IsReceiverAvailable = true;
                    }
                    else
                    {
                        _AppConfigResponse.IsReceiverAvailable = false;
                    }
                    if (!string.IsNullOrEmpty(ReceiverConfig.Title))
                    {
                        _DonationDetails.ReceiverTitle = ReceiverConfig.Title;
                    }
                    else
                    {
                        _DonationDetails.ReceiverTitle = "Support COVID-19 Relief";
                    }
                    if (!string.IsNullOrEmpty(ReceiverConfig.ShortDescription))
                    {
                        _DonationDetails.ReceiverMessage = ReceiverConfig.ShortDescription;
                    }
                    else
                    {
                        _DonationDetails.ReceiverMessage = "Supported 80 families 1000 families are waiting.";
                    }
                    //if (!string.IsNullOrEmpty(ReceiverConfig.IconUrl))
                    //{
                    //    _DonationDetails.IconUrl = _AppConfig.StorageUrl + ReceiverConfig.IconUrl;
                    //}
                    //else
                    //{
                    //    _DonationDetails.IconUrl = "http://cdn.thankucash.com/temp/virus.png";
                    //}
                }
                else
                {
                    _AppConfigResponse.IsDonationAvailable = false;
                    _DonationDetails.Title = "Support COVID-19 Relief";
                    _DonationDetails.Message = "Supported 80 families 1000 families are waiting.";
                    _DonationDetails.Description = "100% of your contribution goes to the nonprofit. You can support between 1 to 100 families. Just donate 7500 per family, we will make sure that your help reaches a needy family.";
                    _DonationDetails.Conditions = "";
                    _DonationDetails.IconUrl = "http://cdn.thankucash.com/temp/virus.png";
                }

                List<OAppManager.DonationParameter.Response> _Data = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == donationparameters)
                                                .Select(x => new OAppManager.DonationParameter.Response
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Title = x.Name,
                                                    Quantity = x.Sequence,
                                                    Amount = x.Value,
                                                    Charge = x.SubValue,
                                                    TotalAmount = x.Data,
                                                }).ToList();
                _DonationRanges = new List<OApp.OAppConfiguration.DonationRange>();
                foreach (var item in _Data)
                {
                    _DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                    {
                        Title = item.Title,
                        Quantity = (long)item.Quantity,
                        Amount = Convert.ToDouble(item.Amount),
                        Charge = Convert.ToDouble(item.Charge),
                        TotalAmount = Convert.ToDouble(item.TotalAmount)
                    });
                }
                //_DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                //{
                //    Title = "Support 1 family",
                //    Quantity = 1,
                //    Amount = 7500,
                //    Charge = 0,
                //    TotalAmount = 7500
                //});
                //_DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                //{
                //    Title = "Support 3 families",
                //    Quantity = 3,
                //    Amount = 22500,
                //    Charge = 0,
                //    TotalAmount = 22500
                //});
                //_DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                //{
                //    Title = "Support 5 families",
                //    Quantity = 5,
                //    Amount = 37500,
                //    Charge = 0,
                //    TotalAmount = 37500
                //});
                //_DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                //{
                //    Title = "Support 10 families",
                //    Quantity = 10,
                //    Amount = 75000,
                //    Charge = 0,
                //    TotalAmount = 75000
                //});
                //_DonationRanges.Add(new OApp.OAppConfiguration.DonationRange
                //{
                //    Title = "Support 20 families",
                //    Quantity = 20,
                //    Amount = 150000,
                //    Charge = 0,
                //    TotalAmount = 150000
                //});
                _DonationDetails.DonationRanges = _DonationRanges;
                _AppConfigResponse.Donation = _DonationDetails;
            }
            #region Send Response
            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppConfigResponse, "HC0001");
            #endregion
        }
        /// <summary>
        /// Gets the pay stack payment status.
        /// </summary>
        /// <param name="PaymentReference">The payment reference.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>PayStackResponseData.</returns>
        private static PayStackResponseData GetPayStackPaymentStatus(string PaymentReference, OUserReference UserReference)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/verify/";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl + PaymentReference);
                request.ContentType = "application/json";
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    request.Headers["Authorization"] = "Bearer sk_live_505bddd9f974f9dcedbfe21c1c31d526b41479e5";
                }
                else
                {
                    request.Headers["Authorization"] = "Bearer sk_test_5766345ff22d84b730a356092d8e54a528dfd814";
                }
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    PayStackResponse _RequestBodyContent = JsonConvert.DeserializeObject<PayStackResponse>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        return _RequestBodyContent.data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetPayStackPaymentStatus", _Exception, UserReference);
                #endregion
                return null;
            }
        }
        /// <summary>
        /// Confirms the donation.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConfirmDonation(OApp.OPaymentDetails _Request)
        {
            #region Manage Exception
            try
            {
                PayStackResponseData _PayStackResponseData = GetPayStackPaymentStatus(_Request.PaymentReference, _Request.UserReference);
                if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                {
                    #region Operation
                    if (_PayStackResponseData.authorization != null)
                    {
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = _Request.AccountId;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = 3;
                        _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.amount / 100;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.amount / 100;
                        _CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                        _CoreTransactionRequest.ReferenceAmount = _PayStackResponseData.amount / 100;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = 467,
                            SourceId = 469,
                            Amount = _PayStackResponseData.amount / 100,
                            Charge = 0,
                            TotalAmount = _PayStackResponseData.amount / 100,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Debit,
                            TypeId = 467,
                            SourceId = 469,
                            Amount = _PayStackResponseData.amount / 100,
                            Charge = 0,
                            TotalAmount = _PayStackResponseData.amount / 100,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2124, PaymentResource.PR2124M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                            #endregion
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                    }
                    #endregion

                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("PaymentConfirm", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the donar configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDonarConfiguration(OAppManager.RelifDetails _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC100, ResponseCodes.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ShortDescription))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC101, ResponseCodes.HC101M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC102, ResponseCodes.HC102);
                }
                if (string.IsNullOrEmpty(_Request.Conditions))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC103, ResponseCodes.HC103M);
                }

                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationconfig)
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        //Maping
                        //Title = Name
                        //ShortDescritpion = Systemname,
                        //Description = Description
                        //IsEnable = Sequence
                        Details.Name = _Request.Title;
                        Details.SystemName = _Request.ShortDescription;
                        Details.Description = _Request.Description;
                        Details.Sequence = _Request.IsFeatureEnable;
                        Details.Data = _Request.Conditions;
                        long? TPosterStorageId = Details.PosterStorageId;
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        if (_Request.IconContent != null)
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UpdateDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Details.Guid).FirstOrDefault();
                                if (UpdateDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UpdateDetails.IconStorageId = IconStorageId;
                                    }


                                    UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = Details.Id,
                            ReferenceKey = Details.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.HC104, ResponseCodes.HC104M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC501, ResponseCodes.HC501M);
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDonarConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the donar configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDonarConfiguration(OAppManager.RelifDetails _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                using (_HCoreContext = new HCoreContext())
                {
                    OAppManager.RelifDetailsResponse Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationconfig)
                        .Select(x => new OAppManager.RelifDetailsResponse
                        {
                            Title = x.Name,
                            ShortDescription = x.SystemName,
                            Description = x.Description,
                            Conditions = x.Data,
                            IsFeatureEnable = x.Sequence,
                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                        })
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.HC105M, ResponseCodes.HC105M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC501, ResponseCodes.HC501M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDonarConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the receiver configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateReceiverConfiguration(OAppManager.RelifDetails _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC100, ResponseCodes.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ShortDescription))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC101, ResponseCodes.HC101M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC102, ResponseCodes.HC102);
                }
                if (string.IsNullOrEmpty(_Request.Conditions))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC103, ResponseCodes.HC103M);
                }

                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationreceiverconfig)
                       .FirstOrDefault();
                    if (Details != null)
                    {

                        //Maping
                        //Title = Name
                        //ShortDescritpion = Systemname,
                        //Description = Description
                        //IsEnable = Sequence
                        Details.Name = _Request.Title;
                        Details.SystemName = _Request.ShortDescription;
                        Details.Description = _Request.Description;
                        Details.Sequence = _Request.IsFeatureEnable;
                        Details.Data = _Request.Conditions;
                        long? TPosterStorageId = Details.PosterStorageId;
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        if (_Request.IconContent != null)
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UpdateDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Details.Guid).FirstOrDefault();
                                if (UpdateDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UpdateDetails.IconStorageId = IconStorageId;
                                    }

                                    UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = Details.Id,
                            ReferenceKey = Details.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.HC104, ResponseCodes.HC104M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC501, ResponseCodes.HC501M);
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateReceiverConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the receiver configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReceiverConfiguration(OAppManager.RelifDetails _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                using (_HCoreContext = new HCoreContext())
                {
                    OAppManager.RelifDetailsResponse Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationreceiverconfig)
                        .Select(x => new OAppManager.RelifDetailsResponse
                        {
                            Title = x.Name,
                            ShortDescription = x.SystemName,
                            Description = x.Description,
                            Conditions = x.Data,
                            IsFeatureEnable = x.Sequence,
                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                        })
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.HC105M, ResponseCodes.HC105M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC501, ResponseCodes.HC501M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDonarConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactions(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.Type == "loyalty")
                {
                    if (_Request.SearchCondition.Contains("transaction.pending"))
                    {
                        if (string.IsNullOrEmpty(_Request.SearchCondition))
                        {
                            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                        }
                        if (string.IsNullOrEmpty(_Request.SortExpression))
                        {
                            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                        }
                        if (_Request.Limit < 1)
                        {
                            _Request.Limit = _AppConfig.DefaultRecordsLimit;
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.ToAccountId == _Request.UserReference.AccountId
                                                        && x.TotalAmount > 0
                                                        && (x.StatusId == HelperStatus.Transaction.Pending))
                                                        .Select(x => new OApp.Transaction
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            TransactionDate = x.TransactionDate,

                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            TypeCategoryCode = x.Type.SubParent.SystemName,
                                                            TypeCategoryName = x.Type.SubParent.Name,

                                                            ModeCode = x.Mode.SystemName,
                                                            ModeName = x.Mode.Name,

                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,

                                                            TotalAmount = x.TotalAmount,
                                                            InvoiceAmount = x.InvoiceAmount,

                                                            StoreId = x.StoreId,

                                                            ReferenceNumber = x.ReferenceNumber,

                                                            MerchantId = x.MerchantId,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = _AppConfig.StorageUrl + x.Merchant.IconStorage.Path,

                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                        })
                                                        .Count(_Request.SearchCondition);
                                #endregion
                            }
                            List<OApp.Transaction> Data = _HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.ToAccountId == _Request.UserReference.AccountId
                                                        && x.TotalAmount > 0
                                                        && (x.StatusId == HelperStatus.Transaction.Pending))
                                                        .Select(x => new OApp.Transaction
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            TransactionDate = x.TransactionDate,

                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            TypeCategoryCode = x.Type.SubParent.SystemName,
                                                            TypeCategoryName = x.Type.SubParent.Name,

                                                            ModeCode = x.Mode.SystemName,
                                                            ModeName = x.Mode.Name,

                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,

                                                            TotalAmount = x.TotalAmount,
                                                            InvoiceAmount = x.InvoiceAmount,

                                                            StoreId = x.StoreId,

                                                            ReferenceNumber = x.ReferenceNumber,

                                                            MerchantId = x.MerchantId,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = _AppConfig.StorageUrl + x.Merchant.IconStorage.Path,

                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                        })
                                                             .Where(_Request.SearchCondition)
                                                             .OrderBy(_Request.SortExpression)
                                                             .Skip(_Request.Offset)
                                                             .Take(_Request.Limit)
                                                             .ToList();
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                            #endregion
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(_Request.SearchCondition))
                        {
                            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                        }
                        if (string.IsNullOrEmpty(_Request.SortExpression))
                        {
                            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                        }
                        if (_Request.Limit < 1)
                        {
                            _Request.Limit = _AppConfig.DefaultRecordsLimit;
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                        .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                        && x.TotalAmount > 0
                                                        && x.StatusId == HelperStatus.Transaction.Success
                                                        && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
                                                        .Select(x => new OApp.Transaction
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            TransactionDate = x.TransactionDate,

                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            TypeCategoryCode = x.Type.SubParent.SystemName,
                                                            TypeCategoryName = x.Type.SubParent.Name,

                                                            ModeCode = x.Mode.SystemName,
                                                            ModeName = x.Mode.Name,

                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,

                                                            TotalAmount = x.TotalAmount,
                                                            InvoiceAmount = x.PurchaseAmount,

                                                            StoreId = x.SubParentId,

                                                            ReferenceNumber = x.ReferenceNumber,

                                                            MerchantId = x.ParentId,
                                                            MerchantKey = x.Parent.Guid,
                                                            MerchantName = x.Parent.DisplayName,
                                                            MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                            StoreKey = x.SubParent.Guid,
                                                            StoreName = x.SubParent.DisplayName,
                                                            StoreAddress = x.SubParent.Address,
                                                            StoreLatitude = x.SubParent.Latitude,
                                                            StoreLongitude = x.SubParent.Longitude,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                        })
                                                        .Count(_Request.SearchCondition);
                                #endregion
                            }
                            List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                        .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                        && x.TotalAmount > 0
                                                        && x.StatusId == HelperStatus.Transaction.Success
                                                        //&& (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                                        && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
                                                        .Select(x => new OApp.Transaction
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            TransactionDate = x.TransactionDate,

                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            TypeCategoryCode = x.Type.SubParent.SystemName,
                                                            TypeCategoryName = x.Type.SubParent.Name,

                                                            ModeCode = x.Mode.SystemName,
                                                            ModeName = x.Mode.Name,

                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,

                                                            TotalAmount = x.TotalAmount,
                                                            InvoiceAmount = x.PurchaseAmount,

                                                            StoreId = x.SubParentId,

                                                            ReferenceNumber = x.ReferenceNumber,

                                                            MerchantId = x.ParentId,
                                                            MerchantKey = x.Parent.Guid,
                                                            MerchantName = x.Parent.DisplayName,
                                                            MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                            StoreKey = x.SubParent.Guid,
                                                            StoreName = x.SubParent.DisplayName,
                                                            StoreAddress = x.SubParent.Address,
                                                            StoreLatitude = x.SubParent.Latitude,
                                                            StoreLongitude = x.SubParent.Longitude,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                        })
                                                             .Where(_Request.SearchCondition)
                                                             .OrderBy(_Request.SortExpression)
                                                             .Skip(_Request.Offset)
                                                             .Take(_Request.Limit)
                                                             .ToList();
                            #region Send Response
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                            #endregion
                        }

                    }
                }
                else if (_Request.Type == "rewards")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                        && x.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                        && x.SourceId == TransactionSource.TUC)
                                                    || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack))
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        ExpiaryDate = x.TransactionDate.AddHours(24),

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                        && x.TotalAmount > 0
                                                    && x.CampaignId == null
                                                   && x.ParentTransaction.TotalAmount > 0
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                        && x.SourceId == TransactionSource.TUC)
                                                    || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack))
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,
                                                        ExpiaryDate = x.TransactionDate.AddHours(24),

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "referralbonus")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.TypeId == TransactionType.ReferralBonus
                                                    && x.ModeId == TransactionMode.Credit
                                                    )
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,
                                                        ExpiaryDate = x.TransactionDate.AddHours(24),

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.TypeId == TransactionType.ReferralBonus
                                                    && x.ModeId == TransactionMode.Credit
                                                    )
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,
                                                        ExpiaryDate = x.TransactionDate.AddHours(24),

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "redeems")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack)
                                                    && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack)
                                                    && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "all")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                     where x.AccountId == _Request.UserReference.AccountId
                                                     select new OApp.Transaction
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         TransactionDate = x.TransactionDate,

                                                         TypeCode = x.Type.SystemName,
                                                         TypeName = x.Type.Name,
                                                         TypeCategoryCode = x.Type.SubParent.SystemName,
                                                         TypeCategoryName = x.Type.SubParent.Name,

                                                         ModeCode = x.Mode.SystemName,
                                                         ModeName = x.Mode.Name,

                                                         SourceCode = x.Source.SystemName,
                                                         SourceName = x.Source.Name,

                                                         TotalAmount = x.TotalAmount,
                                                         InvoiceAmount = x.PurchaseAmount,

                                                         StoreId = x.SubParentId,

                                                         ReferenceNumber = x.ReferenceNumber,

                                                         MerchantId = x.ParentId,
                                                         MerchantKey = x.Parent.Guid,
                                                         MerchantName = x.Parent.DisplayName,
                                                         MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                         StoreKey = x.SubParent.Guid,
                                                         StoreName = x.SubParent.DisplayName,
                                                         StoreAddress = x.SubParent.Address,
                                                         StoreLatitude = x.SubParent.Latitude,
                                                         StoreLongitude = x.SubParent.Longitude,

                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                     })
                                             .Where(_Request.SearchCondition)
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OApp.Transaction> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                       where x.AccountId == _Request.UserReference.AccountId
                                                       select new OApp.Transaction
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           TransactionDate = x.TransactionDate,

                                                           TypeCode = x.Type.SystemName,
                                                           TypeName = x.Type.Name,
                                                           TypeCategoryCode = x.Type.SubParent.SystemName,
                                                           TypeCategoryName = x.Type.SubParent.Name,

                                                           ModeCode = x.Mode.SystemName,
                                                           ModeName = x.Mode.Name,

                                                           SourceCode = x.Source.SystemName,
                                                           SourceName = x.Source.Name,

                                                           TotalAmount = x.TotalAmount,
                                                           InvoiceAmount = x.PurchaseAmount,

                                                           StoreId = x.SubParentId,

                                                           ReferenceNumber = x.ReferenceNumber,

                                                           MerchantId = x.ParentId,
                                                           MerchantKey = x.Parent.Guid,
                                                           MerchantName = x.Parent.DisplayName,
                                                           MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                           StoreKey = x.SubParent.Guid,
                                                           StoreName = x.SubParent.DisplayName,
                                                           StoreAddress = x.SubParent.Address,
                                                           StoreLatitude = x.SubParent.Latitude,
                                                           StoreLongitude = x.SubParent.Longitude,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                       })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "payments")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && x.SourceId == TransactionSource.TUC
                                                    && x.TypeId == 479)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,
                                                        AccountNumber = x.AccountNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && x.SourceId == TransactionSource.TUC
                                                    && x.TypeId == 479)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,
                                                        AccountNumber = x.AccountNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "lcctopup")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && x.SourceId == TransactionSource.TUC
                                                    && x.TypeId == 480)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,
                                                        AccountNumber = x.AccountNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Debit
                                                    && x.SourceId == TransactionSource.TUC
                                                    && x.TypeId == 480)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,
                                                        AccountNumber = x.AccountNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "productpurchase")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.SourceId == 470)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.SourceId == 470)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }
                else if (_Request.Type == "donations")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.SourceId == 469)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        List<OApp.Transaction> Data = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Success
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.SourceId == 469)
                                                    .Select(x => new OApp.Transaction
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        TransactionDate = x.TransactionDate,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategoryCode = x.Type.SubParent.SystemName,
                                                        TypeCategoryName = x.Type.SubParent.Name,

                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        TotalAmount = x.TotalAmount,
                                                        InvoiceAmount = x.PurchaseAmount,

                                                        StoreId = x.SubParentId,

                                                        ReferenceNumber = x.ReferenceNumber,

                                                        MerchantId = x.ParentId,
                                                        MerchantKey = x.Parent.Guid,
                                                        MerchantName = x.Parent.DisplayName,
                                                        MerchantIconUrl = _AppConfig.StorageUrl + x.Parent.IconStorage.Path,

                                                        StoreKey = x.SubParent.Guid,
                                                        StoreName = x.SubParent.DisplayName,
                                                        StoreAddress = x.SubParent.Address,
                                                        StoreLatitude = x.SubParent.Latitude,
                                                        StoreLongitude = x.SubParent.Longitude,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                        #endregion
                    }
                }

                DateTime StartDate = HCoreHelper.GetGMTDate().AddDays(-91);
                List<OApp.Transaction> _Data = new List<OApp.Transaction>();
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _Data = _HCoreContext.HCUAccountTransaction
                    .Where(x => x.AccountId == _Request.UserReference.AccountId
                    && (x.ParentId != 3 && x.ParentId != 971 && x.ParentId != 10 && x.ParentId != 6967)
                    && x.TotalAmount > 0
                    && ((x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                    || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus))
                    && x.TransactionDate > StartDate)
                    .OrderByDescending(x => x.TransactionDate)
                    .Select(x => new OApp.Transaction
                    {
                        ReferenceId = x.Id,
                        TransactionDate = x.TransactionDate,
                        MerchantId = x.ParentId,
                        StoreId = x.SubParentId,
                        TypeCode = x.Type.SystemName,
                        TypeName = x.Type.Name,
                        SourceCode = x.Source.SystemName,
                        SourceName = x.Source.Name,
                        TotalAmount = x.TotalAmount,
                        InvoiceAmount = x.PurchaseAmount,
                        StatusCode = x.Status.SystemName,
                        StatusName = x.Status.Name,
                        ModeCode = x.Mode.SystemName,
                        ModeName = x.Mode.Name,
                    }).ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        public class ResponseCodes
        {
            public static string HC501 = "HC501";
            public static string HC501M = "Details not found";

            public static string HC100 = "HC100";
            public static string HC100M = "Title required";
            public static string HC101 = "HC101";
            public static string HC101M = "Short description required";
            public static string HC102 = "HC102";
            public static string HC102M = "Description required";
            public static string HC103 = "HC103";
            public static string HC103M = "Terms and conditions required";

            public static string HC104 = "HC104";
            public static string HC104M = "Configuration udpated";

            public static string HC105 = "HC105";
            public static string HC105M = "Configuration loaded";

            public static string HC106 = "HC106";
            public static string HC106M = "Title required";

            public static string HC107 = "HC107";
            public static string HC107M = "Amount must be greater than 0";

            public static string HC108 = "HC108";
            public static string HC108M = "Charge must be greater than 0";

            public static string HC109 = "HC109";
            public static string HC109M = "Reference id missing";

            public static string HC110 = "HC110";
            public static string HC110M = "Reference key missing";

            public static string HC111 = "HC111";
            public static string HC111M = "Quantity must be greater than 0";

            public static string HC112 = "HC112";
            public static string HC112M = "Parameter updated";

            public static string HC113 = "HC113";
            public static string HC113M = "Parameter details not found";

            public static string HC114 = "HC114";
            public static string HC114M = "Amount must be greater than 0";

            public static string HC115 = "HC115";
            public static string HC115M = "Parameter title already exists";

            public static string HC116 = "HC116";
            public static string HC116M = "Parameter added successfully";

            public static string HC117 = "HC117";
            public static string HC117M = "Parameter deleted successfully";

        }
        HCCoreParameter _HCCoreParameter;
        /// <summary>
        /// Saves the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC106, ResponseCodes.HC106M);
                }
                if (_Request.Quantity < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC111, ResponseCodes.HC111M);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC114, ResponseCodes.HC114M);
                }
                if (_Request.Charge < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC108, ResponseCodes.HC108M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationparameters && x.Name == _Request.Title)
                       .FirstOrDefault();
                    if (Details == null)
                    {
                        _HCCoreParameter = new HCCoreParameter();
                        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreParameter.TypeId = donationparameters;
                        _HCCoreParameter.Name = _Request.Title;
                        _HCCoreParameter.SystemName = _Request.Title;
                        _HCCoreParameter.Sequence = _Request.Quantity;
                        _HCCoreParameter.Value = _Request.Amount.ToString();
                        _HCCoreParameter.SubValue = _Request.Charge.ToString();
                        _HCCoreParameter.Data = (_Request.Amount + _Request.Charge).ToString();
                        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _HCCoreParameter.Id,
                            ReferenceKey = _HCCoreParameter.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.HC116, ResponseCodes.HC116M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC115, ResponseCodes.HC115M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDonationParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Updates the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC109, ResponseCodes.HC109M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC110, ResponseCodes.HC110M);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC106, ResponseCodes.HC106M);
                }
                if (_Request.Quantity < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC111, ResponseCodes.HC111M);
                }
                if (_Request.Amount < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC107, ResponseCodes.HC107M);
                }
                if (_Request.Charge < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC108, ResponseCodes.HC108M);
                }
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationparameters && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Title) && Details.Name != _Request.Title)
                        {
                            Details.Name = _Request.Title;
                            Details.SystemName = _Request.Title;
                        }
                        if (!string.IsNullOrEmpty(_Request.Title) && Details.Name != _Request.Title)
                        {
                            Details.Name = _Request.Title;
                        }
                        if (_Request.Quantity > 0 && Details.Sequence != _Request.Quantity)
                        {
                            Details.Sequence = _Request.Quantity;
                        }
                        if (_Request.Quantity > 0 && Details.Sequence != _Request.Quantity)
                        {
                            Details.Sequence = _Request.Quantity;
                        }
                        Details.Value = _Request.Amount.ToString();
                        Details.SubValue = _Request.Charge.ToString();
                        Details.Data = (_Request.Amount + _Request.Charge).ToString();
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = Details.Id,
                            ReferenceKey = Details.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.HC112, ResponseCodes.HC112M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC113, ResponseCodes.HC113M);
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDonationParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Deletes the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC109, ResponseCodes.HC109M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC110, ResponseCodes.HC110M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == donationparameters && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.HCCoreParameter.Remove(Details);
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.HC117, ResponseCodes.HC117M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.HC113, ResponseCodes.HC113M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteDonationParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDonationParameter(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == donationparameters)
                                                .Select(x => new OAppManager.DonationParameter.Response
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Title = x.Name,
                                                    Quantity = x.Sequence,
                                                    Amount = x.Value,
                                                    Charge = x.SubValue,
                                                    TotalAmount = x.Data,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    List<OAppManager.DonationParameter.Response> Data = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == donationparameters)
                                                .Select(x => new OAppManager.DonationParameter.Response
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Title = x.Name,
                                                    Quantity = x.Sequence,
                                                    Amount = x.Value,
                                                    Charge = x.SubValue,
                                                    TotalAmount = x.Data,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                    #endregion
                }


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDonationParameter", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Ges the donations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GeDonations(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                && x.ModeId == TransactionMode.Credit
                                                && x.SourceId == 469)
                                                .Select(x => new OAppManager.Donations
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TransactionDate = x.TransactionDate,
                                                    AccountId = x.AccountId,
                                                    DisplayName = x.Account.DisplayName,
                                                    MobileNumber = x.Account.MobileNumber,
                                                    EmailAddress = x.Account.EmailAddress,
                                                    TotalAmount = x.TotalAmount,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    List<OAppManager.Donations> Data = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                && x.ModeId == TransactionMode.Credit
                                                && x.SourceId == 469)
                                                .Select(x => new OAppManager.Donations
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TransactionDate = x.TransactionDate,
                                                    AccountId = x.AccountId,
                                                    DisplayName = x.Account.DisplayName,
                                                    MobileNumber = x.Account.MobileNumber,
                                                    EmailAddress = x.Account.EmailAddress,
                                                    TotalAmount = x.TotalAmount,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        public class Req
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? Address { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByMobileNumber { get; set; }
            public string? CreatedByEmailAddress { get; set; }
            public DateTime? CreateDate { get; set; }
        }
        /// <summary>
        /// Gets the receivers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReceivers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreParameter
                                                .Where(x => x.StatusId == HelperStatus.Default.Active
                                                && x.TypeId == 473)
                                                .Select(x => new Req
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    MobileNumber = x.SystemName,
                                                    Address = x.Value,

                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    CreatedByMobileNumber = x.CreatedBy.MobileNumber,
                                                    CreatedByEmailAddress = x.CreatedBy.EmailAddress,

                                                    CreateDate = x.CreateDate,

                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    var Data = _HCoreContext.HCCoreParameter
                                                .Where(x => x.StatusId == HelperStatus.Default.Active
                                                && x.TypeId == 473)
                                                .Select(x => new Req
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    MobileNumber = x.SystemName,
                                                    Address = x.Value,

                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    CreatedByMobileNumber = x.CreatedBy.MobileNumber,
                                                    CreatedByEmailAddress = x.CreatedBy.EmailAddress,

                                                    CreateDate = x.CreateDate,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit), "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetReceivers", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }


        //internal OResponse GetTransactions(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        DateTime StartDate = HCoreHelper.GetGMTDate().AddDays(-91);
        //        List<OApp.Transaction> _Data = new List<OApp.Transaction>();
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Get Data
        //            _Data = _HCoreContext.HCUAccountTransaction
        //        .Where(x => x.AccountId == _Request.UserReference.AccountId
        //        && (x.ParentId != 3 && x.ParentId != 971 && x.ParentId != 10 && x.ParentId != 6967)
        //        && x.TotalAmount > 0
        //        && ((x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
        //        || (x.ModeId == TransactionMode.Credit && x.SourceId == TransactionSource.ThankUCashPlus))
        //        && x.TransactionDate > StartDate)
        //        .OrderByDescending(x => x.TransactionDate)
        //        .Select(x => new OApp.Transaction
        //        {
        //            ReferenceId = x.Id,
        //            TransactionDate = x.TransactionDate,
        //            MerchantId = x.ParentId,
        //            StoreId = x.SubParentId,
        //            TypeCode = x.Type.SystemName,
        //            TypeName = x.Type.Name,
        //            SourceCode = x.Source.SystemName,
        //            SourceName = x.Source.Name,
        //            TotalAmount = x.TotalAmount,
        //            InvoiceAmount = x.PurchaseAmount,
        //            StatusCode = x.Status.SystemName,
        //            StatusName = x.Status.Name,
        //            ModeCode = x.Mode.SystemName,
        //            ModeName = x.Mode.Name,
        //        }).ToList();
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }

        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}
    }
}
