//==================================================================================
// FileName: AppUsersList.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to app user list
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Data.Store;
using Akka.Actor;
using HCore.ThankUCash.Actors;
using ClosedXML.Excel;
using System.IO;
using System.Reflection;
namespace HCore.ThankUCash.Framework
{
    public class AppUsersList
    {
        //HCUAccountParameter _HCUAccountParameter;
        List<OAccount.Account> _Accounts;
        //List<OAccount.AccountDownload> _AccountDownload;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the application user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUser(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorAppUserDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorAppUserDownloader>("ActorAppUserDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                long IsTucPlusEnable = 0;
                _Accounts = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ListType == 1)
                    {
                        switch (_Request.Type)
                        {
                            case ThankUCashConstant.ListType.ThankUCash:
                                {
                                    if (_Request.Type == "thankucash")
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber)

                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),
                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),
                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                        TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber)

                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),
                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),
                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                        TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    break;
                                }
                            case ThankUCashConstant.ListType.Owner:
                                {
                                    IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                                    if (IsTucPlusEnable == 1)
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                       TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                       TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                       TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    else
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                       TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    break;
                                }
                            case ThankUCashConstant.ListType.SubOwner:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                                .Select(x => new OAccount.Account
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    MobileNumber = x.MobileNumber,
                                                                    GenderId = x.GenderId,
                                                                    DateOfBirth = x.DateOfBirth,
                                                                    IconUrl = x.IconStorage.Path,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerKey = x.Owner.Guid,
                                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                                    OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                    SubOwnerId = x.SubOwnerId,
                                                                    CreateDate = x.CreateDate,
                                                                    StatusId = x.StatusId,
                                                                    ApplicationStatusId = x.ApplicationStatusId,
                                                                    AccountLevelId = x.AccountLevel,
                                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                    TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                SubOwnerId = x.SubOwnerId,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            case ThankUCashConstant.ListType.CreatedBy:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                                && (x.CreatedById == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.CreatedById == _Request.ReferenceId)))
                                                              .Select(x => new OAccount.Account
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,
                                                                  DisplayName = x.DisplayName,
                                                                  Name = x.Name,
                                                                  EmailAddress = x.EmailAddress,
                                                                  MobileNumber = x.MobileNumber,
                                                                  GenderId = x.GenderId,
                                                                  DateOfBirth = x.DateOfBirth,
                                                                  IconUrl = x.IconStorage.Path,
                                                                  OwnerId = x.OwnerId,
                                                                  OwnerKey = x.Owner.Guid,
                                                                  OwnerDisplayName = x.Owner.DisplayName,
                                                                  OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                  SubOwnerId = x.SubOwnerId,
                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  ApplicationStatusId = x.ApplicationStatusId,
                                                                  AccountLevelId = x.AccountLevel,
                                                                  TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                  TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                  TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                  TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                  TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                  RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                  RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                  TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                              })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                           .Select(x => new OAccount.Account
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               DisplayName = x.DisplayName,
                                                               Name = x.Name,
                                                               EmailAddress = x.EmailAddress,
                                                               MobileNumber = x.MobileNumber,
                                                               GenderId = x.GenderId,
                                                               DateOfBirth = x.DateOfBirth,
                                                               IconUrl = x.IconStorage.Path,
                                                               OwnerId = x.OwnerId,
                                                               OwnerKey = x.Owner.Guid,
                                                               OwnerDisplayName = x.Owner.DisplayName,
                                                               OwnerIconUrl = x.Owner.IconStorage.Path,
                                                               SubOwnerId = x.SubOwnerId,
                                                               CreateDate = x.CreateDate,
                                                               StatusId = x.StatusId,
                                                               ApplicationStatusId = x.ApplicationStatusId,
                                                               AccountLevelId = x.AccountLevel,
                                                               TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                               TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                               TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                               TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                               TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                               RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                               RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                               TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            default:
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        }
                    }
                    else if (_Request.ListType == 2)
                    {
                        switch (_Request.Type)
                        {
                            case ThankUCashConstant.ListType.Merchant:
                                {
                                    if (_Request.Type == "merchant")
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)

                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                        TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)

                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                        TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    break;
                                }
                            case ThankUCashConstant.ListType.Owner:
                                {
                                    IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                                    if (IsTucPlusEnable == 1)
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                       TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                       TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                       TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    else
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                       TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    break;
                                }
                            case ThankUCashConstant.ListType.SubOwner:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                                .Select(x => new OAccount.Account
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    MobileNumber = x.MobileNumber,
                                                                    GenderId = x.GenderId,
                                                                    DateOfBirth = x.DateOfBirth,
                                                                    IconUrl = x.IconStorage.Path,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerKey = x.Owner.Guid,
                                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                                    OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                    SubOwnerId = x.SubOwnerId,
                                                                    CreateDate = x.CreateDate,
                                                                    StatusId = x.StatusId,
                                                                    ApplicationStatusId = x.ApplicationStatusId,
                                                                    AccountLevelId = x.AccountLevel,
                                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                    TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                SubOwnerId = x.SubOwnerId,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            case ThankUCashConstant.ListType.CreatedBy:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                                && (x.CreatedById == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.CreatedById == _Request.ReferenceId)))
                                                              .Select(x => new OAccount.Account
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,
                                                                  DisplayName = x.DisplayName,
                                                                  Name = x.Name,
                                                                  EmailAddress = x.EmailAddress,
                                                                  MobileNumber = x.MobileNumber,
                                                                  GenderId = x.GenderId,
                                                                  DateOfBirth = x.DateOfBirth,
                                                                  IconUrl = x.IconStorage.Path,
                                                                  OwnerId = x.OwnerId,
                                                                  OwnerKey = x.Owner.Guid,
                                                                  OwnerDisplayName = x.Owner.DisplayName,
                                                                  OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                  SubOwnerId = x.SubOwnerId,
                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  ApplicationStatusId = x.ApplicationStatusId,
                                                                  AccountLevelId = x.AccountLevel,
                                                                  TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                  TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                  TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                  TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                  TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                  RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                  RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                  TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                              })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                           .Select(x => new OAccount.Account
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               DisplayName = x.DisplayName,
                                                               Name = x.Name,
                                                               EmailAddress = x.EmailAddress,
                                                               MobileNumber = x.MobileNumber,
                                                               GenderId = x.GenderId,
                                                               DateOfBirth = x.DateOfBirth,
                                                               IconUrl = x.IconStorage.Path,
                                                               OwnerId = x.OwnerId,
                                                               OwnerKey = x.Owner.Guid,
                                                               OwnerDisplayName = x.Owner.DisplayName,
                                                               OwnerIconUrl = x.Owner.IconStorage.Path,
                                                               SubOwnerId = x.SubOwnerId,
                                                               CreateDate = x.CreateDate,
                                                               StatusId = x.StatusId,
                                                               ApplicationStatusId = x.ApplicationStatusId,
                                                               AccountLevelId = x.AccountLevel,
                                                               TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                               TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                               TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                               TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                               TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                               RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                               RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                               TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            default:
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        }
                    }
                    else
                    {
                        switch (_Request.Type)
                        {
                            case ThankUCashConstant.ListType.All:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                                .Select(x => new OAccount.Account
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    MobileNumber = x.MobileNumber,
                                                                    GenderId = x.GenderId,
                                                                    DateOfBirth = x.DateOfBirth,
                                                                    IconUrl = x.IconStorage.Path,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerKey = x.Owner.Guid,
                                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                                    OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                    LastTransactionDate = x.LastTransactionDate,
                                                                    CreateDate = x.CreateDate,
                                                                    StatusId = x.StatusId,
                                                                    ApplicationStatusId = x.ApplicationStatusId,
                                                                    AccountLevelId = x.AccountLevel,
                                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                    TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled)
                                                                .Select(x => new OAccount.Account
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    MobileNumber = x.MobileNumber,
                                                                    GenderId = x.GenderId,
                                                                    DateOfBirth = x.DateOfBirth,
                                                                    IconUrl = x.IconStorage.Path,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerKey = x.Owner.Guid,
                                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                                    OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                    LastTransactionDate = x.LastTransactionDate,
                                                                    CreateDate = x.CreateDate,
                                                                    StatusId = x.StatusId,
                                                                    ApplicationStatusId = x.ApplicationStatusId,
                                                                    AccountLevelId = x.AccountLevel,
                                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                    TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                })
                                                            .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            case ThankUCashConstant.ListType.Owner:
                                {
                                    IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                                    if (IsTucPlusEnable == 1)
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                       TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,

                                                                       TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                       TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                       TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    else
                                    {
                                        if (_Request.RefreshCount)
                                        {
                                            #region Total Records
                                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                    .Select(x => new OAccount.Account
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        Name = x.Name,
                                                                        EmailAddress = x.EmailAddress,
                                                                        MobileNumber = x.MobileNumber,
                                                                        GenderId = x.GenderId,
                                                                        DateOfBirth = x.DateOfBirth,
                                                                        IconUrl = x.IconStorage.Path,
                                                                        OwnerId = x.OwnerId,
                                                                        OwnerKey = x.Owner.Guid,
                                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                                        OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                        LastTransactionDate = x.LastTransactionDate,
                                                                        CreateDate = x.CreateDate,
                                                                        StatusId = x.StatusId,
                                                                        ApplicationStatusId = x.ApplicationStatusId,
                                                                        AccountLevelId = x.AccountLevel,
                                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                    })
                                                                    .Where(_Request.SearchCondition)
                                                           .Count();
                                            #endregion
                                        }
                                        #region Data
                                        _Accounts = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.ReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                                   .Select(x => new OAccount.Account
                                                                   {
                                                                       ReferenceId = x.Id,
                                                                       ReferenceKey = x.Guid,
                                                                       DisplayName = x.DisplayName,
                                                                       Name = x.Name,
                                                                       EmailAddress = x.EmailAddress,
                                                                       MobileNumber = x.MobileNumber,
                                                                       GenderId = x.GenderId,
                                                                       DateOfBirth = x.DateOfBirth,
                                                                       IconUrl = x.IconStorage.Path,
                                                                       OwnerId = x.OwnerId,
                                                                       OwnerKey = x.Owner.Guid,
                                                                       OwnerDisplayName = x.Owner.DisplayName,
                                                                       OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                       LastTransactionDate = x.LastTransactionDate,
                                                                       CreateDate = x.CreateDate,
                                                                       StatusId = x.StatusId,
                                                                       ApplicationStatusId = x.ApplicationStatusId,
                                                                       AccountLevelId = x.AccountLevel,
                                                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                                       TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                                       RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                                       RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.ReferenceId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                                   })
                                                                .Where(_Request.SearchCondition)
                                                                .OrderBy(_Request.SortExpression)
                                                                .Skip(_Request.Offset)
                                                                .Take(_Request.Limit)
                                                                .ToList();
                                        #endregion
                                    }
                                    break;
                                }
                            case ThankUCashConstant.ListType.SubOwner:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                                .Select(x => new OAccount.Account
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    DisplayName = x.DisplayName,
                                                                    Name = x.Name,
                                                                    EmailAddress = x.EmailAddress,
                                                                    MobileNumber = x.MobileNumber,
                                                                    GenderId = x.GenderId,
                                                                    DateOfBirth = x.DateOfBirth,
                                                                    IconUrl = x.IconStorage.Path,
                                                                    OwnerId = x.OwnerId,
                                                                    OwnerKey = x.Owner.Guid,
                                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                                    OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                    SubOwnerId = x.SubOwnerId,
                                                                    CreateDate = x.CreateDate,
                                                                    StatusId = x.StatusId,
                                                                    ApplicationStatusId = x.ApplicationStatusId,
                                                                    AccountLevelId = x.AccountLevel,
                                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                    TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                                })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                            .Select(x => new OAccount.Account
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                Name = x.Name,
                                                                EmailAddress = x.EmailAddress,
                                                                MobileNumber = x.MobileNumber,
                                                                GenderId = x.GenderId,
                                                                DateOfBirth = x.DateOfBirth,
                                                                IconUrl = x.IconStorage.Path,
                                                                OwnerId = x.OwnerId,
                                                                OwnerKey = x.Owner.Guid,
                                                                OwnerDisplayName = x.Owner.DisplayName,
                                                                OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                SubOwnerId = x.SubOwnerId,
                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                ApplicationStatusId = x.ApplicationStatusId,
                                                                AccountLevelId = x.AccountLevel,
                                                                TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.SubParentId == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == x.OwnerId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            case ThankUCashConstant.ListType.CreatedBy:
                                {
                                    if (_Request.RefreshCount)
                                    {
                                        #region Total Records
                                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                                && (x.CreatedById == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.CreatedById == _Request.ReferenceId)))
                                                              .Select(x => new OAccount.Account
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,
                                                                  DisplayName = x.DisplayName,
                                                                  Name = x.Name,
                                                                  EmailAddress = x.EmailAddress,
                                                                  MobileNumber = x.MobileNumber,
                                                                  GenderId = x.GenderId,
                                                                  DateOfBirth = x.DateOfBirth,
                                                                  IconUrl = x.IconStorage.Path,
                                                                  OwnerId = x.OwnerId,
                                                                  OwnerKey = x.Owner.Guid,
                                                                  OwnerDisplayName = x.Owner.DisplayName,
                                                                  OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                  SubOwnerId = x.SubOwnerId,
                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  ApplicationStatusId = x.ApplicationStatusId,
                                                                  AccountLevelId = x.AccountLevel,
                                                                  TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                                  TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                                  TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                                  TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                  TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                                  TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                                  RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                                  RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                  RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                                  TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                                  TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                              })
                                                                .Where(_Request.SearchCondition)
                                                       .Count();
                                        #endregion
                                    }
                                    #region Data
                                    _Accounts = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                            && (x.SubOwnerId == _Request.ReferenceId || x.HCUAccountTransactionAccount.Any(m => m.SubParentId == _Request.ReferenceId)))
                                                           .Select(x => new OAccount.Account
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               DisplayName = x.DisplayName,
                                                               Name = x.Name,
                                                               EmailAddress = x.EmailAddress,
                                                               MobileNumber = x.MobileNumber,
                                                               GenderId = x.GenderId,
                                                               DateOfBirth = x.DateOfBirth,
                                                               IconUrl = x.IconStorage.Path,
                                                               OwnerId = x.OwnerId,
                                                               OwnerKey = x.Owner.Guid,
                                                               OwnerDisplayName = x.Owner.DisplayName,
                                                               OwnerIconUrl = x.Owner.IconStorage.Path,
                                                               SubOwnerId = x.SubOwnerId,
                                                               CreateDate = x.CreateDate,
                                                               StatusId = x.StatusId,
                                                               ApplicationStatusId = x.ApplicationStatusId,
                                                               AccountLevelId = x.AccountLevel,
                                                               TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                               TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                               TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                               TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                               TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus),

                                                               TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,



                                                               RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC),


                                                               RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                               RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                               TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                               TucBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.CreatedById == _Request.ReferenceId
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           })
                                                                .Where(_Request.SearchCondition)
                                                            .OrderBy(_Request.SortExpression)
                                                            .Skip(_Request.Offset)
                                                            .Take(_Request.Limit)
                                                            .ToList();
                                    #endregion
                                    break;
                                }
                            default:
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                        }
                    }
                }
                _HCoreContext.Dispose();
                foreach (var DataItem in _Accounts)
                {
                    if (DataItem.GenderId != null)
                    {
                        DataItem.GenderName = HCoreDataStore.SystemHelpers.Where(m => m.ReferenceId == DataItem.GenderId).Select(m => m.Name).FirstOrDefault();
                    }
                    DataItem.StatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.StatusId).Select(x => x.Name).FirstOrDefault();
                    DataItem.ApplicationStatusCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.ApplicationStatusName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.ApplicationStatusId).Select(x => x.Name).FirstOrDefault();
                    DataItem.AccountLevelCode = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.SystemName).FirstOrDefault();
                    DataItem.AccountLevelName = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == DataItem.AccountLevelId).Select(x => x.Name).FirstOrDefault();

                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    {
                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    }
                    else
                    {
                        DataItem.IconUrl = _AppConfig.Default_Icon;
                    }

                    if (!string.IsNullOrEmpty(DataItem.OwnerIconUrl))
                    {
                        DataItem.OwnerIconUrl = _AppConfig.StorageUrl + DataItem.OwnerIconUrl;
                    }
                    else
                    {
                        DataItem.OwnerIconUrl = _AppConfig.Default_Icon;
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccount", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
