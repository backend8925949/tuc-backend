//==================================================================================
// FileName: FrameworkTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to transactions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Data.Store;
using ClosedXML.Excel;
using System.IO;
using Akka.Actor;
using HCore.ThankUCash.Actors;
using System.Reflection;
using HCore.Data.Logging;

namespace HCore.ThankUCash.Framework
{
    internal class FrameworkTransactions
    {
        List<OTransaction.Sale> _Sales;
        List<OTransaction.SaleDownload> _SaleDownload;
        List<OTransaction.RewardDownload> _RewardDownload;
        List<OTransaction.RewardClaimDownload> _RewardClaimDownload;
        List<OTransaction.RedeemDownload> _RedeemDownload;
        List<OTransaction.All> _AllTransactions;
        OTransaction.SmsNotification _SmsNotification;
        OTransactionOverview _OTransactionOverview;
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        HCUAccountParameter _HCUAccountParameter;
        OTransaction.Overview _Overview;
        /// <summary>
        /// Formats the transaction list.
        /// </summary>
        /// <param name="_Sales">The sales.</param>
        /// <returns>List&lt;OTransaction.Sale&gt;.</returns>
        private List<OTransaction.Sale> FormatTransactions(List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.SourceName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.SourceId).Select(q => q.Name).FirstOrDefault();
                DataItem.SourceCode = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.SourceId).Select(q => q.SystemName).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                if (ParentDetails != null)
                {
                    DataItem.ParentKey = ParentDetails.ReferenceKey;
                    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                }
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.SubParentId);
                if (SubParentDetails != null)
                {
                    DataItem.SubParentKey = SubParentDetails.ReferenceKey;
                    DataItem.SubParentDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }
                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }
                if (!string.IsNullOrEmpty(DataItem.TerminalId))
                {
                    var TerminalDetails = HCoreDataStore.Terminals.FirstOrDefault(q => q.DisplayName == DataItem.TerminalId);
                    if (TerminalDetails != null)
                    {
                        DataItem.TerminalId = TerminalDetails.DisplayName;
                        DataItem.TerminalReferenceId = TerminalDetails.ReferenceId;
                        DataItem.TerminalReferenceKey = TerminalDetails.ReferenceKey;
                    }
                }
                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return _Sales;
            #endregion
        }
        /// <summary>
        /// Formats the transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="_Sales">The sales.</param>
        /// <returns>OList.Response.</returns>
        private OList.Response FormatTransactions(OList.Request _Request, List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.SourceName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.SourceId).Select(q => q.Name).FirstOrDefault();
                DataItem.SourceCode = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.SourceId).Select(q => q.SystemName).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                if (ParentDetails != null)
                {
                    DataItem.ParentKey = ParentDetails.ReferenceKey;
                    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                }
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.SubParentId);
                if (SubParentDetails != null)
                {
                    DataItem.SubParentKey = SubParentDetails.ReferenceKey;
                    DataItem.SubParentDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = ProviderDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }
                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }
                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return HCoreHelper.GetListResponse(_Request.TotalRecords, _Sales, _Request.Offset, _Request.Limit);
            #endregion
        }
        /// <summary>
        /// Formats the transaction details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OTransaction.Details.</returns>
        private OTransaction.Details FormatTransaction(OTransaction.Details _Request)
        {
            #region Create  Response Object
            _Request.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == _Request.TypeId).Select(q => q.Name).FirstOrDefault();
            _Request.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == _Request.CardBankId).Select(q => q.Name).FirstOrDefault();
            _Request.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == _Request.CardBrandId).Select(q => q.Name).FirstOrDefault();
            _Request.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == _Request.StatusId).Select(q => q.Name).FirstOrDefault();
            var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.ParentId);
            if (ParentDetails != null)
            {
                _Request.ParentKey = ParentDetails.ReferenceKey;
                _Request.ParentDisplayName = ParentDetails.DisplayName;
                _Request.ParentIconUrl = ParentDetails.IconUrl;
                _Request.ParentMobileNumber = ParentDetails.ContactNumber;
                _Request.ParentEmailAddress = ParentDetails.EmailAddress;
            }
            var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.SubParentId);
            if (SubParentDetails != null)
            {
                _Request.SubParentKey = SubParentDetails.ReferenceKey;
                _Request.SubParentDisplayName = SubParentDetails.DisplayName;
            }
            var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.AcquirerId);
            if (AcquirerDetails != null)
            {
                _Request.AcquirerKey = AcquirerDetails.ReferenceKey;
                _Request.AcquirerDisplayName = AcquirerDetails.DisplayName;
                _Request.AcquirerIconUrl = AcquirerDetails.IconUrl;
            }
            var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.ProviderId);
            if (ProviderDetails != null)
            {
                _Request.ProviderKey = ProviderDetails.ReferenceKey;
                _Request.ProviderDisplayName = ProviderDetails.DisplayName;
                _Request.ProviderIconUrl = ProviderDetails.IconUrl;
            }
            var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.CashierId);
            if (CashierDetails != null)
            {
                _Request.CashierKey = CashierDetails.ReferenceKey;
                _Request.CashierCode = CashierDetails.DisplayName;
                _Request.CashierDisplayName = CashierDetails.Name;
                _Request.CashierMobileNumber = CashierDetails.MobileNumber;
            }
            var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == _Request.CreatedById);
            if (CreatedByDetails != null)
            {
                _Request.CreatedByKey = CreatedByDetails.ReferenceKey;
                _Request.CreatedByDisplayName = CreatedByDetails.DisplayName;
                _Request.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
            }
            if (!string.IsNullOrEmpty(_Request.TypeName))
            {
                _Request.TypeName = _Request.TypeName.Replace("Reward", "").Replace("Rewards", "");
            }
            return _Request;
            #endregion
        }

        /// <summary>
        /// Gets the transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransaction(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TRA1004", "Transaction reference required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TRA1005", "Transaction reference key");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                                 .Select(x => new OTransaction.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     InvoiceAmount = x.PurchaseAmount,
                                                     TransactionDate = x.TransactionDate,
                                                     TypeId = x.TypeId,
                                                     TypeCategoryId = x.Type.SubParentId,
                                                     CardBankId = x.CardBankId,
                                                     CardBankName = x.CardBank.Name,
                                                     CardBrandId = x.CardBrandId,
                                                     CardBrandName = x.CardBrand.Name,
                                                     CardNumber = x.BinNumber.Bin,
                                                     AccountNumber = x.AccountNumber,
                                                     ReferenceNumber = x.ReferenceNumber,
                                                     UserAccountId = x.AccountId,
                                                     UserAccountKey = x.Account.Guid,
                                                     UserDisplayName = x.Account.DisplayName,
                                                     UserMobileNumber = x.Account.MobileNumber,
                                                     ParentId = x.ParentId,
                                                     SubParentId = x.SubParentId,
                                                     ProviderId = x.ProviderId,
                                                     AcquirerId = x.BankId,
                                                     CreatedById = x.CreatedById,
                                                     CashierId = x.CashierId,
                                                     CashierDisplayName = x.Cashier.DisplayName,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusId = x.StatusId,
                                                     TerminalReferenceId = x.TerminalId,
                                                     TerminalReferenceKey = x.Terminal.Guid,
                                                     TerminalId = x.Terminal.IdentificationNumber,
                                                     RewardAmount = x.ReferenceAmount,
                                                     RedeemAmount = x.TotalAmount,
                                                     UserAmount = x.ReferenceInvoiceAmount,
                                                     CommissionAmount = x.ComissionAmount,
                                                     IsSmsSent = false,
                                                 }).FirstOrDefault();
                    if (_Details != null)
                    {
                        _Details = FormatTransaction(_Details);
                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            var SmsDetails = _HCoreContextLogging.HCLSmsNotification.Where(x => x.SystemReference == _Details.GroupKey).FirstOrDefault();
                            if (SmsDetails != null)
                            {
                                _SmsNotification = new OTransaction.SmsNotification();
                                _SmsNotification.SenderId = SmsDetails.Source;
                                _SmsNotification.MobileNumber = SmsDetails.MobileNumber;
                                //_SmsNotification.Message = SmsDetails.Message;
                                _SmsNotification.SendDate = SmsDetails.SendDate;
                                _SmsNotification.DeliveryDate = SmsDetails.DeliveryDate;
                                _SmsNotification.StatusCode = SmsDetails.StatusCode;
                                _Details.IsSmsSent = true;
                                _Details.SmsNotification = _SmsNotification;

                                //RewardAmount = x.ReferenceAmount,
                                //CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                            }
                            else
                            {
                                _Details.IsSmsSent = false;
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HC200");
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC404", "Transaction details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransaction", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the sale transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorSaleTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorSaleTransactionDownloader>("ActorSaleTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CashierReward = x.CashierReward,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CashierReward = x.CashierReward,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the sale transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetSaleTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SaleDownload = new List<OTransaction.SaleDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Sales_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CashierReward = x.CashierReward,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CashierReward = x.CashierReward,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        //foreach (var _Sale in _Sales)
                        //{
                        if (SalesItem.StatusId == HelperStatus.Transaction.Pending)
                        {
                            SalesItem.StatusCode = "transaction.pending";
                            SalesItem.StatusId = HelperStatus.Transaction.Success;
                        }
                        //}
                        _SaleDownload.Add(new OTransaction.SaleDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                            CashierReward = SalesItem.CashierReward,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Sales_Sheet");
                        PropertyInfo[] properties = _SaleDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_SaleDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Sales_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the sale transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", ">");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                               && (x.StatusId == HelperStatus.Transaction.Failed || x.StatusId == HelperStatus.Transaction.Success)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUC)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();


                    _OTransactionOverview.FailedTransaction = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                               && x.StatusId == HelperStatus.Transaction.Failed
                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                               .Select(x => new OTransaction.Sale
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   InvoiceAmount = x.PurchaseAmount,
                                                   TransactionDate = x.TransactionDate,
                                                   TypeId = x.TypeId,

                                                   SourceId = x.SourceId,
                                                   SourceCode = x.Source.SystemName,
                                                   SourceName = x.Source.Name,
                                                   CardBankId = x.CardBankId,
                                                   CardBankName = x.CardBank.Name,
                                                   CardBrandId = x.CardBrandId,
                                                   CardBrandName = x.CardBrand.Name,
                                                   AccountNumber = x.AccountNumber,
                                                   ReferenceNumber = x.ReferenceNumber,
                                                   UserAccountId = x.AccountId,
                                                   UserAccountKey = x.Account.Guid,
                                                   UserDisplayName = x.Account.DisplayName,
                                                   UserMobileNumber = x.Account.MobileNumber,
                                                   ParentId = x.ParentId,
                                                   ParentDisplayName = x.Parent.DisplayName,
                                                   SubParentId = x.SubParentId,
                                                   ProviderId = x.ProviderId,
                                                   AcquirerId = x.BankId,
                                                   CreatedById = x.CreatedById,
                                                   CashierId = x.CashierId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusId = x.StatusId,
                                                   TerminalReferenceId = x.TerminalId,
                                                   TerminalReferenceKey = x.Terminal.Guid,
                                                   TerminalId = x.Terminal.IdentificationNumber,
                                                   ProgramId = x.ProgramId,
                                                   ProgramReferenceKey = x.Program.Guid
                                               })
                                                 .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);

                    _OTransactionOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CardReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);

                    _OTransactionOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransactionsOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }



        /// <summary>
        /// Gets the reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardTransaction(OList.Request _Request)
        {
            if (_Request.IsDownload)
            {
                var _Actor = ActorSystem.Create("ActorRewardTransactionDownloader");
                var _ActorNotify = _Actor.ActorOf<ActorRewardTransactionDownloader>("ActorRewardTransactionDownloader");
                _ActorNotify.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
            }
            else
            {
                #region Manage Exception
                try
                {

                    _SaleDownload = new List<OTransaction.SaleDownload>();
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                    && (((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash)
                                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || (x.SourceId == TransactionSource.ThankUCashPlus && x.SourceId != TransactionSource.TUCSuperCash)))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        ModeId = x.ModeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        CommissionAmount = x.ParentTransaction.ComissionAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        SubParentId = x.SubParentId,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        TerminalReferenceId = x.TerminalId,
                                                        TerminalReferenceKey = x.Terminal.Guid,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        Balance = x.Balance,
                                                        SourceId = x.SourceId,
                                                        ProgramId = x.ProgramId,
                                                        ProgramReferenceKey = x.Program.Guid,
                                                        CashierReward = x.CashierReward,
                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data 
                        _Sales = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.CampaignId == null
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                   && x.ParentTransaction.TotalAmount > 0
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || (x.SourceId == TransactionSource.ThankUCashPlus && x.SourceId != TransactionSource.TUCSuperCash)))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        ModeId = x.ModeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CommissionAmount = x.ParentTransaction.ComissionAmount,
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        SubParentId = x.SubParentId,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        TerminalReferenceId = x.TerminalId,
                                                        TerminalReferenceKey = x.Terminal.Guid,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        Balance = x.Balance,
                                                        SourceId = x.SourceId,
                                                        ProgramId = x.ProgramId,
                                                        ProgramReferenceKey = x.Program.Guid,
                                                        CashierReward = x.CashierReward,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                        #endregion
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("GetRewardTransaction", _Exception, _Request.UserReference);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                }
                #endregion
            }
        }
        /// <summary>
        /// Gets the reward transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRewardTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardDownload = new List<OTransaction.RewardDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Rewards_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = x.ParentTransaction.ComissionAmount,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = x.ParentTransaction.ComissionAmount,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RewardDownload.Add(new OTransaction.RewardDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            RewardAmount = SalesItem.RewardAmount,
                            ConvinenceCharge = SalesItem.CommissionAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName ?? "",
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                            Balance = SalesItem.Balance,
                            SourceName = SalesItem.SourceName
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Rewards_Sheet");
                        PropertyInfo[] properties = _RewardDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RewardDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Rewards_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    CashierReward = x.CashierReward,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.CampaignId == null
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    CashierReward = x.CashierReward,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                 && (((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash)
                                                && (x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.CardReward)
                                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                 || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    CashierReward = x.CashierReward,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                              && x.ModeId == TransactionMode.Credit
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  ModeId = x.ModeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  Balance = x.Balance,
                                                  SourceId = x.SourceId,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid,
                                                  CashierReward = x.CashierReward,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);

                    _OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                                             .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                             && x.ModeId == TransactionMode.Credit
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                               && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                             && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                             || x.SourceId == TransactionSource.ThankUCashPlus))
                                             .Select(x => new OTransaction.Sale
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 InvoiceAmount = x.PurchaseAmount,
                                                 TransactionDate = x.TransactionDate,
                                                 TypeId = x.TypeId,
                                                 RewardAmount = x.ReferenceAmount,
                                                 UserAmount = x.TotalAmount,
                                                 CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                 CardBankId = x.CardBankId,
                                                 CardBrandId = x.CardBrandId,
                                                 AccountNumber = x.AccountNumber,
                                                 ReferenceNumber = x.ReferenceNumber,
                                                 UserAccountId = x.AccountId,
                                                 UserAccountKey = x.Account.Guid,
                                                 UserDisplayName = x.Account.DisplayName,
                                                 UserMobileNumber = x.Account.MobileNumber,
                                                 ParentId = x.ParentId,
                                                 SubParentId = x.SubParentId,
                                                 ProviderId = x.ProviderId,
                                                 AcquirerId = x.BankId,
                                                 CreatedById = x.CreatedById,
                                                 CashierId = x.CashierId,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 TerminalReferenceId = x.TerminalId,
                                                 TerminalReferenceKey = x.Terminal.Guid,
                                                 TerminalId = x.Terminal.IdentificationNumber,
                                                 Balance = x.Balance,
                                                 SourceId = x.SourceId,
                                                 ProgramId = x.ProgramId,
                                                 ProgramReferenceKey = x.Program.Guid,
                                                 CashierReward = x.CashierReward,
                                             })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.CommissionAmount);

                    _OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
                                             .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                             && x.ModeId == TransactionMode.Credit
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                               && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                             && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                             || x.SourceId == TransactionSource.ThankUCashPlus))
                                             .Select(x => new OTransaction.Sale
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 InvoiceAmount = x.PurchaseAmount,
                                                 TransactionDate = x.TransactionDate,
                                                 TypeId = x.TypeId,
                                                 RewardAmount = x.ReferenceAmount,
                                                 UserAmount = x.TotalAmount,
                                                 CommissionAmount = (x.ReferenceAmount - x.Charge - x.TotalAmount),
                                                 CardBankId = x.CardBankId,
                                                 CardBrandId = x.CardBrandId,
                                                 AccountNumber = x.AccountNumber,
                                                 ReferenceNumber = x.ReferenceNumber,
                                                 UserAccountId = x.AccountId,
                                                 UserAccountKey = x.Account.Guid,
                                                 UserDisplayName = x.Account.DisplayName,
                                                 UserMobileNumber = x.Account.MobileNumber,
                                                 ParentId = x.ParentId,
                                                 SubParentId = x.SubParentId,
                                                 ProviderId = x.ProviderId,
                                                 AcquirerId = x.BankId,
                                                 CreatedById = x.CreatedById,
                                                 CashierId = x.CashierId,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 TerminalReferenceId = x.TerminalId,
                                                 TerminalReferenceKey = x.Terminal.Guid,
                                                 TerminalId = x.Terminal.IdentificationNumber,
                                                 Balance = x.Balance,
                                                 SourceId = x.SourceId,
                                                 ProgramId = x.ProgramId,
                                                 ProgramReferenceKey = x.Program.Guid,
                                                 CashierReward = x.CashierReward,
                                             })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.UserAmount);

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the pending reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingRewardTransaction(OList.Request _Request)
        {
            if (_Request.IsDownload)
            {
                var _Actor = ActorSystem.Create("ActorPendingRewardTransactionDownloader");
                var _ActorNotify = _Actor.ActorOf<ActorPendingRewardTransactionDownloader>("ActorPendingRewardTransactionDownloader");
                _ActorNotify.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
            }
            else
            {
                #region Manage Exception
                //try
                //{

                //    _SaleDownload = new List<OTransaction.SaleDownload>();
                //    if (string.IsNullOrEmpty(_Request.SearchCondition))
                //    {
                //        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                //    }
                //    if (string.IsNullOrEmpty(_Request.SortExpression))
                //    {
                //        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                //    }
                //    if (_Request.Limit < 1)
                //    {
                //        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                //    }
                //    using (_HCoreContext = new HCoreContext())
                //    {
                //        if (_Request.AccountId > 0)
                //        {
                //            if (_Request.RefreshCount)
                //            {
                //                #region Total Records
                //                _Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                //                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                //                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                //                                        && x.StatusId == HelperStatus.Transaction.Pending
                //                                        && x.FromAccountId == _Request.AccountId)
                //                                        .Select(x => new OTransaction.Sale
                //                                        {
                //                                            ReferenceId = x.Id,
                //                                            ReferenceKey = x.Guid,
                //                                            InvoiceAmount = x.InvoiceAmount,
                //                                            TransactionDate = x.TransactionDate,
                //                                            TypeId = x.TypeId,

                //                                            SourceId = x.SourceId,
                //                                            SourceCode = x.Source.SystemName,
                //                                            SourceName = x.Source.Name,
                //                                            RewardAmount = x.TotalAmount,
                //                                            CommissionAmount = x.CommissionAmount,
                //                                            UserAmount = x.Amount,
                //                                        //CardBankId = x.CardBankId,
                //                                        //CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                            ReferenceNumber = x.ReferenceNumber,
                //                                            UserAccountId = x.ToAccountId,
                //                                            UserAccountKey = x.ToAccount.Guid,
                //                                            UserDisplayName = x.ToAccount.DisplayName,
                //                                            UserMobileNumber = x.ToAccount.MobileNumber,
                //                                            ParentId = x.MerchantId,
                //                                            SubParentId = x.StoreId,
                //                                            ProviderId = x.ProviderId,
                //                                            AcquirerId = x.AcquirerId,
                //                                            CreatedById = x.CreatedById,
                //                                            CashierId = x.CashierId,
                //                                            StatusId = x.StatusId,
                //                                            StatusCode = x.Status.SystemName,
                //                                            TerminalReferenceId = x.TerminalId,
                //                                            TerminalReferenceKey = x.Terminal.Guid,
                //                                            TerminalId = x.Terminal.IdentificationNumber,
                //                                        })
                //                                        .Count(_Request.SearchCondition);
                //                #endregion
                //            }
                //            #region Get Data
                //            _Sales = _HCoreContext.TUCLoyaltyPending
                //                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                //                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                //                                        && x.StatusId == HelperStatus.Transaction.Pending
                //                                        && x.FromAccountId == _Request.AccountId)
                //                                        .Select(x => new OTransaction.Sale
                //                                        {
                //                                            ReferenceId = x.Id,
                //                                            ReferenceKey = x.Guid,
                //                                            InvoiceAmount = x.InvoiceAmount,
                //                                            TransactionDate = x.TransactionDate,
                //                                            TypeId = x.TypeId,

                //                                            SourceId = x.SourceId,
                //                                            SourceCode = x.Source.SystemName,
                //                                            SourceName = x.Source.Name,
                //                                            RewardAmount = x.TotalAmount,
                //                                            CommissionAmount = x.CommissionAmount,
                //                                            UserAmount = x.Amount,
                //                                        //CardBankId = x.CardBankId,
                //                                        //CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                            ReferenceNumber = x.ReferenceNumber,
                //                                            UserAccountId = x.ToAccountId,
                //                                            UserAccountKey = x.ToAccount.Guid,
                //                                            UserDisplayName = x.ToAccount.DisplayName,
                //                                            UserMobileNumber = x.ToAccount.MobileNumber,
                //                                            ParentId = x.MerchantId,
                //                                            SubParentId = x.StoreId,
                //                                            ProviderId = x.ProviderId,
                //                                            AcquirerId = x.AcquirerId,
                //                                            CreatedById = x.CreatedById,
                //                                            CashierId = x.CashierId,
                //                                            StatusId = x.StatusId,
                //                                            StatusCode = x.Status.SystemName,
                //                                            TerminalReferenceId = x.TerminalId,
                //                                            TerminalReferenceKey = x.Terminal.Guid,
                //                                            TerminalId = x.Terminal.IdentificationNumber,
                //                                        })
                //                                             .Where(_Request.SearchCondition)
                //                                             .OrderBy(_Request.SortExpression)
                //                                             .Skip(_Request.Offset)
                //                                             .Take(_Request.Limit)
                //                                             .ToList();
                //            #endregion
                //            #region Send Response
                //            _HCoreContext.Dispose();
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                //            #endregion
                //        }
                //        else
                //        {
                //            if (_Request.RefreshCount)
                //            {
                //                #region Total Records
                //                _Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                //                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                //                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                //                                        && x.StatusId == HelperStatus.Transaction.Pending)
                //                                        .Select(x => new OTransaction.Sale
                //                                        {
                //                                            ReferenceId = x.Id,
                //                                            ReferenceKey = x.Guid,
                //                                            InvoiceAmount = x.InvoiceAmount,
                //                                            TransactionDate = x.TransactionDate,
                //                                            TypeId = x.TypeId,

                //                                            SourceId = x.SourceId,
                //                                            SourceCode = x.Source.SystemName,
                //                                            SourceName = x.Source.Name,
                //                                            RewardAmount = x.TotalAmount,
                //                                            CommissionAmount = x.CommissionAmount,
                //                                            UserAmount = x.Amount,
                //                                        //CardBankId = x.CardBankId,
                //                                        //CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                            ReferenceNumber = x.ReferenceNumber,
                //                                            UserAccountId = x.ToAccountId,
                //                                            UserAccountKey = x.ToAccount.Guid,
                //                                            UserDisplayName = x.ToAccount.DisplayName,
                //                                            UserMobileNumber = x.ToAccount.MobileNumber,
                //                                            ParentId = x.MerchantId,
                //                                            SubParentId = x.StoreId,
                //                                            ProviderId = x.ProviderId,
                //                                            AcquirerId = x.AcquirerId,
                //                                            CreatedById = x.CreatedById,
                //                                            CashierId = x.CashierId,
                //                                            StatusId = x.StatusId,
                //                                            StatusCode = x.Status.SystemName,
                //                                            TerminalReferenceId = x.TerminalId,
                //                                            TerminalReferenceKey = x.Terminal.Guid,
                //                                            TerminalId = x.Terminal.IdentificationNumber,
                //                                            //ParentId = x.FromAccountId,

                //                                        })
                //                                        .Count(_Request.SearchCondition);
                //                #endregion
                //            }
                //            #region Get Data
                //            _Sales = _HCoreContext.TUCLoyaltyPending
                //                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                //                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                //                                        && x.StatusId == HelperStatus.Transaction.Pending)
                //                                        .Select(x => new OTransaction.Sale
                //                                        {
                //                                            ReferenceId = x.Id,
                //                                            ReferenceKey = x.Guid,
                //                                            InvoiceAmount = x.InvoiceAmount,
                //                                            TransactionDate = x.TransactionDate,
                //                                            TypeId = x.TypeId,

                //                                            SourceId = x.SourceId,
                //                                            SourceCode = x.Source.SystemName,
                //                                            SourceName = x.Source.Name,
                //                                            RewardAmount = x.TotalAmount,
                //                                            CommissionAmount = x.CommissionAmount,
                //                                            UserAmount = x.Amount,
                //                                        //CardBankId = x.CardBankId,
                //                                        //CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                            ReferenceNumber = x.ReferenceNumber,
                //                                            UserAccountId = x.ToAccountId,
                //                                            UserAccountKey = x.ToAccount.Guid,
                //                                            UserDisplayName = x.ToAccount.DisplayName,
                //                                            UserMobileNumber = x.ToAccount.MobileNumber,
                //                                            ParentId = x.MerchantId,
                //                                            SubParentId = x.StoreId,
                //                                            ProviderId = x.ProviderId,
                //                                            AcquirerId = x.AcquirerId,
                //                                            CreatedById = x.CreatedById,
                //                                            CashierId = x.CashierId,
                //                                            StatusId = x.StatusId,
                //                                            StatusCode = x.Status.SystemName,
                //                                            TerminalReferenceId = x.TerminalId,
                //                                            TerminalReferenceKey = x.Terminal.Guid,
                //                                            TerminalId = x.Terminal.IdentificationNumber,
                //                                            //ParentId = x.FromAccountId,
                //                                        })
                //                                             .Where(_Request.SearchCondition)
                //                                             .OrderBy(_Request.SortExpression)
                //                                             .Skip(_Request.Offset)
                //                                             .Take(_Request.Limit)
                //                                             .ToList();
                //            #endregion
                //            #region Send Response
                //            _HCoreContext.Dispose();
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                //            #endregion
                //        }
                //    }
                //}
                //catch (Exception _Exception)
                //{
                //    HCoreHelper.LogException("GetPendingRewardTransaction", _Exception, _Request.UserReference);
                //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                //}
                #endregion

                #region Manage Exception
                //try
                //{

                //    _SaleDownload = new List<OTransaction.SaleDownload>();
                //    if (string.IsNullOrEmpty(_Request.SearchCondition))
                //    {
                //        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                //    }
                //    if (string.IsNullOrEmpty(_Request.SortExpression))
                //    {
                //        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                //    }
                //    if (_Request.Limit < 1)
                //    {
                //        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                //    }
                //    using (_HCoreContext = new HCoreContext())
                //    {
                //        if (_Request.RefreshCount)
                //        {
                //            #region Total Records
                //            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                //                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                //                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                //                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                //                                    && x.ModeId == TransactionMode.Credit
                //                                    && x.ParentTransaction.TotalAmount > 0
                //                                    && x.CampaignId == null
                //                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                //                                    && (((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash)
                //                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                //                                    || (x.SourceId == TransactionSource.ThankUCashPlus && x.SourceId != TransactionSource.TUCSuperCash)))
                //                                    .Select(x => new OTransaction.Sale
                //                                    {
                //                                        ReferenceId = x.Id,
                //                                        ReferenceKey = x.Guid,
                //                                        InvoiceAmount = x.PurchaseAmount,
                //                                        TransactionDate = x.TransactionDate,
                //                                        TypeId = x.TypeId,
                //                                        RewardAmount = x.ReferenceAmount,
                //                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                //                                        UserAmount = x.TotalAmount,
                //                                        CardBankId = x.CardBankId,
                //                                        CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                        ReferenceNumber = x.ReferenceNumber,
                //                                        UserAccountId = x.AccountId,
                //                                        UserAccountKey = x.Account.Guid,
                //                                        UserDisplayName = x.Account.DisplayName,
                //                                        UserMobileNumber = x.Account.MobileNumber,
                //                                        ParentId = x.ParentId,
                //                                        SubParentId = x.SubParentId,
                //                                        ProviderId = x.ProviderId,
                //                                        AcquirerId = x.BankId,
                //                                        CreatedById = x.CreatedById,
                //                                        CashierId = x.CashierId,
                //                                        StatusId = x.StatusId,
                //                                        StatusCode = x.Status.SystemName,
                //                                        TerminalReferenceId = x.TerminalId,
                //                                        TerminalReferenceKey = x.Terminal.Guid,
                //                                        TerminalId = x.Terminal.IdentificationNumber,
                //                                        Balance = x.Balance,
                //                                        SourceId = x.SourceId,
                //                                    })
                //                                    .Count(_Request.SearchCondition);
                //            #endregion
                //        }
                //        #region Get Data
                //        _Sales = _HCoreContext.HCUAccountTransaction
                //                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                //                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                //                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                //                                    && x.ModeId == TransactionMode.Credit
                //                                    && x.CampaignId == null
                //                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                //                                   && x.ParentTransaction.TotalAmount > 0
                //                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                //                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                //                                    || (x.SourceId == TransactionSource.ThankUCashPlus && x.SourceId != TransactionSource.TUCSuperCash)))
                //                                    .Select(x => new OTransaction.Sale
                //                                    {
                //                                        ReferenceId = x.Id,
                //                                        ReferenceKey = x.Guid,
                //                                        InvoiceAmount = x.PurchaseAmount,
                //                                        TransactionDate = x.TransactionDate,
                //                                        TypeId = x.TypeId,
                //                                        RewardAmount = x.ReferenceAmount,
                //                                        UserAmount = x.TotalAmount,
                //                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                //                                        CardBankId = x.CardBankId,
                //                                        CardBrandId = x.CardBrandId,
                //                                        AccountNumber = x.AccountNumber,
                //                                        ReferenceNumber = x.ReferenceNumber,
                //                                        UserAccountId = x.AccountId,
                //                                        UserAccountKey = x.Account.Guid,
                //                                        UserDisplayName = x.Account.DisplayName,
                //                                        UserMobileNumber = x.Account.MobileNumber,
                //                                        ParentId = x.ParentId,
                //                                        SubParentId = x.SubParentId,
                //                                        ProviderId = x.ProviderId,
                //                                        AcquirerId = x.BankId,
                //                                        CreatedById = x.CreatedById,
                //                                        CashierId = x.CashierId,
                //                                        StatusId = x.StatusId,
                //                                        StatusCode = x.Status.SystemName,
                //                                        TerminalReferenceId = x.TerminalId,
                //                                        TerminalReferenceKey = x.Terminal.Guid,
                //                                        TerminalId = x.Terminal.IdentificationNumber,
                //                                        Balance = x.Balance,
                //                                        SourceId = x.SourceId,
                //                                    })
                //                                         .Where(_Request.SearchCondition)
                //                                         .OrderBy(_Request.SortExpression)
                //                                         .Skip(_Request.Offset)
                //                                         .Take(_Request.Limit)
                //                                         .ToList();
                //        #endregion
                //        #region Send Response
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                //        #endregion
                //    }
                //}
                //catch (Exception _Exception)
                //{
                //    HCoreHelper.LogException("GetPendingRewardTransaction", _Exception, _Request.UserReference);
                //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                //}
                #endregion

                #region Manage Exception
                try
                {

                    _SaleDownload = new List<OTransaction.SaleDownload>();
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                    //&& x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        UserAmount = x.TotalAmount,
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        SubParentId = x.SubParentId,
                                                        SubParentDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        ProgramId = x.ProgramId,
                                                        ProgramReferenceKey = x.Program.Guid
                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        _Sales = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                    //&& x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.CampaignId == null
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack)) || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        SubParentId = x.SubParentId,
                                                        SubParentDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        ProgramId = x.ProgramId,
                                                        ProgramReferenceKey = x.Program.Guid,
                                                        PosName = x.Terminal.IdentificationNumber,
                                                        //PosName  = getPosNameFromTerminalId((long)x.TerminalId)
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                        #endregion
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("GetRewardTransaction", _Exception, _Request.UserReference);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                }
                #endregion
            }
        }
        private string getPosNameFromTerminalId(long terminalId)
        {
            var terminal = _HCoreContext.TUCTerminal.Where(x => x.Id == terminalId).SingleOrDefault();
            _HCoreContext.Dispose();
            return terminal.DisplayName;
        }
        /// <summary>
        /// Gets the pending reward transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetPendingRewardTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardDownload = new List<OTransaction.RewardDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Pending_Rewards_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                                                && x.Merchant.CountryId == _Request.UserReference.CountryId
                                                        && x.StatusId == HelperStatus.Transaction.Pending && x.FromAccountId > _Request.AccountId)
                                                        .Select(x => new OTransaction.Sale
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            InvoiceAmount = x.InvoiceAmount,
                                                            TransactionDate = x.TransactionDate,
                                                            TypeId = x.TypeId,

                                                            SourceId = x.SourceId,
                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,
                                                            RewardAmount = x.TotalAmount,
                                                            CommissionAmount = x.CommissionAmount,
                                                            UserAmount = x.Amount,
                                                            //CardBankId = x.CardBankId,
                                                            //CardBrandId = x.CardBrandId,
                                                            AccountNumber = x.AccountNumber,
                                                            ReferenceNumber = x.ReferenceNumber,
                                                            UserAccountId = x.ToAccountId,
                                                            UserAccountKey = x.ToAccount.Guid,
                                                            UserDisplayName = x.ToAccount.DisplayName,
                                                            UserMobileNumber = x.ToAccount.MobileNumber,
                                                            ParentId = x.MerchantId,
                                                            SubParentId = x.StoreId,
                                                            ProviderId = x.ProviderId,
                                                            AcquirerId = x.AcquirerId,
                                                            CreatedById = x.CreatedById,
                                                            CashierId = x.CashierId,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            TerminalId = x.Terminal.IdentificationNumber,
                                                            //ProgramId = x.ProgramId,
                                                            //ProgramReferenceKey = x.Program.Guid
                                                        })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            _Sales.AddRange(_HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                                                && x.Merchant.CountryId == _Request.UserReference.CountryId
                                                        && x.StatusId == HelperStatus.Transaction.Pending && x.FromAccountId > _Request.AccountId)
                                                        .Select(x => new OTransaction.Sale
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            InvoiceAmount = x.InvoiceAmount,
                                                            TransactionDate = x.TransactionDate,
                                                            TypeId = x.TypeId,

                                                            SourceId = x.SourceId,
                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,
                                                            RewardAmount = x.TotalAmount,
                                                            CommissionAmount = x.CommissionAmount,
                                                            UserAmount = x.Amount,
                                                            //CardBankId = x.CardBankId,
                                                            //CardBrandId = x.CardBrandId,
                                                            AccountNumber = x.AccountNumber,
                                                            ReferenceNumber = x.ReferenceNumber,
                                                            UserAccountId = x.ToAccountId,
                                                            UserAccountKey = x.ToAccount.Guid,
                                                            UserDisplayName = x.ToAccount.DisplayName,
                                                            UserMobileNumber = x.ToAccount.MobileNumber,
                                                            ParentId = x.MerchantId,
                                                            SubParentId = x.StoreId,
                                                            ProviderId = x.ProviderId,
                                                            AcquirerId = x.AcquirerId,
                                                            CreatedById = x.CreatedById,
                                                            CashierId = x.CashierId,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            TerminalId = x.Terminal.IdentificationNumber,
                                                            //ProgramId = x.ProgramId,
                                                            //ProgramReferenceKey = x.Program.Guid
                                                        })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(1000)
                                                         .ToList());
                            _Request.Offset += 1000;
                        }
                        #endregion
                        _Sales = FormatTransactions(_Sales);
                        foreach (var SalesItem in _Sales)
                        {
                            _RewardDownload.Add(new OTransaction.RewardDownload
                            {
                                ReferenceId = SalesItem.ReferenceId,
                                TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                                User = SalesItem.UserDisplayName,
                                MobileNumber = SalesItem.UserMobileNumber,
                                TypeName = SalesItem.TypeName,
                                InvoiceAmount = SalesItem.InvoiceAmount,
                                RewardAmount = SalesItem.RewardAmount,
                                ConvinenceCharge = SalesItem.CommissionAmount,
                                CardNumber = SalesItem.AccountNumber,
                                CardBrand = SalesItem.CardBrandName ?? "",
                                CardBank = SalesItem.CardBankName,

                                Store = SalesItem.SubParentDisplayName,
                                CashierId = SalesItem.CashierCode,
                                CashierDisplayName = SalesItem.CashierDisplayName,
                                Bank = SalesItem.AcquirerDisplayName,
                                Provider = SalesItem.ProviderDisplayName,
                                Issuer = SalesItem.CreatedByDisplayName,
                                Status = SalesItem.StatusName,
                                TerminalId = SalesItem.TerminalId,
                            });
                        }
                        using (var _XLWorkbook = new XLWorkbook())
                        {
                            var _WorkSheet = _XLWorkbook.Worksheets.Add("Pending_Rewards_Sheet");
                            PropertyInfo[] properties = _RewardDownload.First().GetType().GetProperties();
                            List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                            for (int i = 0; i < headerNames.Count; i++)
                            {
                                _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                            }
                            _WorkSheet.Cell(2, 1).InsertData(_RewardDownload);
                            MemoryStream _MemoryStream = new MemoryStream();
                            _XLWorkbook.SaveAs(_MemoryStream);
                            long? _StorageId = HCoreHelper.SaveStorage("Pending_Rewards_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                            if (_StorageId != null && _StorageId != 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                    if (TItem != null)
                                    {
                                        TItem.IconStorageId = _StorageId;
                                        TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        TItem.ModifyById = _Request.UserReference.AccountId;
                                        TItem.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                        #region Send Response
                        _HCoreContext.Dispose();
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                                                        && x.StatusId == HelperStatus.Transaction.Pending)
                                                        .Select(x => new OTransaction.Sale
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            InvoiceAmount = x.InvoiceAmount,
                                                            TransactionDate = x.TransactionDate,
                                                            TypeId = x.TypeId,

                                                            SourceId = x.SourceId,
                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,
                                                            RewardAmount = x.TotalAmount,
                                                            CommissionAmount = x.CommissionAmount,
                                                            UserAmount = x.Amount,
                                                            //CardBankId = x.CardBankId,
                                                            //CardBrandId = x.CardBrandId,
                                                            AccountNumber = x.AccountNumber,
                                                            ReferenceNumber = x.ReferenceNumber,
                                                            UserAccountId = x.ToAccountId,
                                                            UserAccountKey = x.ToAccount.Guid,
                                                            UserDisplayName = x.ToAccount.DisplayName,
                                                            UserMobileNumber = x.ToAccount.MobileNumber,
                                                            ParentId = x.MerchantId,
                                                            SubParentId = x.StoreId,
                                                            ProviderId = x.ProviderId,
                                                            AcquirerId = x.AcquirerId,
                                                            CreatedById = x.CreatedById,
                                                            CashierId = x.CashierId,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            TerminalId = x.Terminal.IdentificationNumber,
                                                            //ProgramId = x.ProgramId,
                                                            //ProgramReferenceKey = x.Program.Guid
                                                        })
                                                    .Count(_Request.SearchCondition);
                            #endregion 
                        }
                        #region Get Data
                        double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            _Sales.AddRange(_HCoreContext.TUCLoyaltyPending
                                                        .Where(x => x.LoyaltyTypeId == Loyalty.Reward
                                                && x.Merchant.CountryId == _Request.UserReference.SystemCountry
                                                        && x.StatusId == HelperStatus.Transaction.Pending)
                                                        .Select(x => new OTransaction.Sale
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            InvoiceAmount = x.InvoiceAmount,
                                                            TransactionDate = x.TransactionDate,
                                                            TypeId = x.TypeId,

                                                            SourceId = x.SourceId,
                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,
                                                            RewardAmount = x.TotalAmount,
                                                            CommissionAmount = x.CommissionAmount,
                                                            UserAmount = x.Amount,
                                                            //CardBankId = x.CardBankId,
                                                            //CardBrandId = x.CardBrandId,
                                                            AccountNumber = x.AccountNumber,
                                                            ReferenceNumber = x.ReferenceNumber,
                                                            UserAccountId = x.ToAccountId,
                                                            UserAccountKey = x.ToAccount.Guid,
                                                            UserDisplayName = x.ToAccount.DisplayName,
                                                            UserMobileNumber = x.ToAccount.MobileNumber,
                                                            ParentId = x.MerchantId,
                                                            SubParentId = x.StoreId,
                                                            ProviderId = x.ProviderId,
                                                            AcquirerId = x.AcquirerId,
                                                            CreatedById = x.CreatedById,
                                                            CashierId = x.CashierId,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            TerminalId = x.Terminal.IdentificationNumber,
                                                        })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(1000)
                                                         .ToList());
                            _Request.Offset += 1000;
                        }
                        #endregion
                        _Sales = FormatTransactions(_Sales);
                        foreach (var SalesItem in _Sales)
                        {
                            _RewardDownload.Add(new OTransaction.RewardDownload
                            {
                                ReferenceId = SalesItem.ReferenceId,
                                TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                                User = SalesItem.UserDisplayName,
                                MobileNumber = SalesItem.UserMobileNumber,
                                TypeName = SalesItem.TypeName,
                                InvoiceAmount = SalesItem.InvoiceAmount,
                                RewardAmount = SalesItem.RewardAmount,
                                ConvinenceCharge = SalesItem.CommissionAmount,
                                CardNumber = SalesItem.AccountNumber,
                                CardBrand = SalesItem.CardBrandName ?? "",
                                CardBank = SalesItem.CardBankName,

                                Store = SalesItem.SubParentDisplayName,
                                CashierId = SalesItem.CashierCode,
                                CashierDisplayName = SalesItem.CashierDisplayName,
                                Bank = SalesItem.AcquirerDisplayName,
                                Provider = SalesItem.ProviderDisplayName,
                                Issuer = SalesItem.CreatedByDisplayName,
                                Status = SalesItem.StatusName,
                                TerminalId = SalesItem.TerminalId,
                            });
                        }
                        using (var _XLWorkbook = new XLWorkbook())
                        {
                            var _WorkSheet = _XLWorkbook.Worksheets.Add("Pending_Rewards_Sheet");
                            PropertyInfo[] properties = _RewardDownload.First().GetType().GetProperties();
                            List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                            for (int i = 0; i < headerNames.Count; i++)
                            {
                                _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                            }
                            _WorkSheet.Cell(2, 1).InsertData(_RewardDownload);
                            MemoryStream _MemoryStream = new MemoryStream();
                            _XLWorkbook.SaveAs(_MemoryStream);
                            long? _StorageId = HCoreHelper.SaveStorage("Pending_Rewards_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                            if (_StorageId != null && _StorageId != 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                    if (TItem != null)
                                    {
                                        TItem.IconStorageId = _StorageId;
                                        TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        TItem.ModifyById = _Request.UserReference.AccountId;
                                        TItem.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                        #region Send Response
                        _HCoreContext.Dispose();
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPendingRewardTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the pending reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingRewardTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            //try
            //{

            //    if (_Request.AccountId > 0)
            //    {

            //        using (_HCoreContext = new HCoreContext())
            //        {
            //            if (string.IsNullOrEmpty(_Request.SearchCondition))
            //            {
            //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
            //            }
            //            _OTransactionOverview = new OTransactionOverview();
            //            //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
            //            //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                            && x.ModeId == TransactionMode.Credit
            //            //                            && x.CampaignId == null
            //            //                             && x.ParentTransaction.TotalAmount > 0
            //            //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                                && x.SourceId == TransactionSource.TUC)
            //            //                            || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                            .Select(x => new OTransaction.Sale
            //            //                            {
            //            //                                ReferenceId = x.Id,
            //            //                                ReferenceKey = x.Guid,
            //            //                                InvoiceAmount = x.PurchaseAmount,
            //            //                                TransactionDate = x.TransactionDate,
            //            //                                TypeId = x.TypeId,
            //            //                                RewardAmount = x.ReferenceAmount,
            //            //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                                UserAmount = x.TotalAmount,
            //            //                                CardBankId = x.CardBankId,
            //            //                                CardBrandId = x.CardBrandId,
            //            //                                AccountNumber = x.AccountNumber,
            //            //                                ReferenceNumber = x.ReferenceNumber,
            //            //                                UserAccountId = x.AccountId,
            //            //                                UserAccountKey = x.Account.Guid,
            //            //                                UserDisplayName = x.Account.DisplayName,
            //            //                                UserMobileNumber = x.Account.MobileNumber,
            //            //                                StoreReferenceId = x.SubParentId,
            //            //                                ProviderId = x.ProviderId,
            //            //                                AcquirerId = x.BankId,
            //            //                                CreatedById = x.CreatedById,
            //            //                                CashierId = x.CashierId,
            //            //                                StatusId = x.StatusId,
            //            //                                StatusCode = x.Status.SystemName,
            //            //                            })
            //            //                            .Where(_Request.SearchCondition)
            //            //                            .Select(x => x.AccountId)
            //            //                            .Distinct()
            //            //                            .Count();

            //            _OTransactionOverview.Transactions = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry
            //                                            && x.StatusId == HelperStatus.Transaction.Pending && x.FromAccountId == _Request.AccountId)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                            })
            //                                        .Where(_Request.SearchCondition)
            //                                        .Count();

            //            _OTransactionOverview.InvoiceAmount = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry

            //                                            && x.StatusId == HelperStatus.Transaction.Pending && x.FromAccountId == _Request.AccountId)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                            })
            //                                        .Where(_Request.SearchCondition)
            //                                        .Sum(x => x.InvoiceAmount);


            //            _OTransactionOverview.RewardAmount = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry
            //                                            && x.StatusId == HelperStatus.Transaction.Pending && x.FromAccountId == _Request.AccountId)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                            })
            //                                      .Where(_Request.SearchCondition)
            //                                      .Sum(x => x.RewardAmount);

            //            //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
            //            //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                         && x.ModeId == TransactionMode.Credit
            //            //                           && x.CampaignId == null
            //            //                          && x.ParentTransaction.TotalAmount > 0
            //            //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                             && x.SourceId == TransactionSource.TUC)
            //            //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                         .Select(x => new OTransaction.Sale
            //            //                         {
            //            //                             ReferenceId = x.Id,
            //            //                             ReferenceKey = x.Guid,
            //            //                             InvoiceAmount = x.PurchaseAmount,
            //            //                             TransactionDate = x.TransactionDate,
            //            //                             TypeId = x.TypeId,
            //            //                             RewardAmount = x.ReferenceAmount,
            //            //                             UserAmount = x.TotalAmount,
            //            //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                             CardBankId = x.CardBankId,
            //            //                             CardBrandId = x.CardBrandId,
            //            //                             AccountNumber = x.AccountNumber,
            //            //                             ReferenceNumber = x.ReferenceNumber,
            //            //                             UserAccountId = x.AccountId,
            //            //                             UserAccountKey = x.Account.Guid,
            //            //                             UserDisplayName = x.Account.DisplayName,
            //            //                             UserMobileNumber = x.Account.MobileNumber,
            //            //                             StoreReferenceId = x.SubParentId,
            //            //                             ProviderId = x.ProviderId,
            //            //                             AcquirerId = x.BankId,
            //            //                             CreatedById = x.CreatedById,
            //            //                             CashierId = x.CashierId,
            //            //                             StatusId = x.StatusId,
            //            //                             StatusCode = x.Status.SystemName,
            //            //                         })
            //            //                         .Where(_Request.SearchCondition)
            //            //                         .Sum(x => x.CommissionAmount);

            //            //_OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
            //            //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                         && x.ModeId == TransactionMode.Credit
            //            //                           && x.CampaignId == null
            //            //                          && x.ParentTransaction.TotalAmount > 0
            //            //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                             && x.SourceId == TransactionSource.TUC)
            //            //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                         .Select(x => new OTransaction.Sale
            //            //                         {
            //            //                             ReferenceId = x.Id,
            //            //                             ReferenceKey = x.Guid,
            //            //                             InvoiceAmount = x.PurchaseAmount,
            //            //                             TransactionDate = x.TransactionDate,
            //            //                             TypeId = x.TypeId,
            //            //                             RewardAmount = x.ReferenceAmount,
            //            //                             UserAmount = x.TotalAmount,
            //            //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                             CardBankId = x.CardBankId,
            //            //                             CardBrandId = x.CardBrandId,
            //            //                             AccountNumber = x.AccountNumber,
            //            //                             ReferenceNumber = x.ReferenceNumber,
            //            //                             UserAccountId = x.AccountId,
            //            //                             UserAccountKey = x.Account.Guid,
            //            //                             UserDisplayName = x.Account.DisplayName,
            //            //                             UserMobileNumber = x.Account.MobileNumber,
            //            //                             StoreReferenceId = x.SubParentId,
            //            //                             ProviderId = x.ProviderId,
            //            //                             AcquirerId = x.BankId,
            //            //                             CreatedById = x.CreatedById,
            //            //                             CashierId = x.CashierId,
            //            //                             StatusId = x.StatusId,
            //            //                             StatusCode = x.Status.SystemName,
            //            //                         })
            //            //                         .Where(_Request.SearchCondition)
            //            //                         .Sum(x => x.UserAmount);

            //            #region Send Response
            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
            //            #endregion
            //        }

            //    }
            //    else
            //    {
            //        using (_HCoreContext = new HCoreContext())
            //        {
            //            if (string.IsNullOrEmpty(_Request.SearchCondition))
            //            {
            //                HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
            //            }
            //            _OTransactionOverview = new OTransactionOverview();
            //            //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
            //            //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                            && x.ModeId == TransactionMode.Credit
            //            //                            && x.CampaignId == null
            //            //                             && x.ParentTransaction.TotalAmount > 0
            //            //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                                && x.SourceId == TransactionSource.TUC)
            //            //                            || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                            .Select(x => new OTransaction.Sale
            //            //                            {
            //            //                                ReferenceId = x.Id,
            //            //                                ReferenceKey = x.Guid,
            //            //                                InvoiceAmount = x.PurchaseAmount,
            //            //                                TransactionDate = x.TransactionDate,
            //            //                                TypeId = x.TypeId,
            //            //                                RewardAmount = x.ReferenceAmount,
            //            //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                                UserAmount = x.TotalAmount,
            //            //                                CardBankId = x.CardBankId,
            //            //                                CardBrandId = x.CardBrandId,
            //            //                                AccountNumber = x.AccountNumber,
            //            //                                ReferenceNumber = x.ReferenceNumber,
            //            //                                UserAccountId = x.AccountId,
            //            //                                UserAccountKey = x.Account.Guid,
            //            //                                UserDisplayName = x.Account.DisplayName,
            //            //                                UserMobileNumber = x.Account.MobileNumber,
            //            //                                StoreReferenceId = x.SubParentId,
            //            //                                ProviderId = x.ProviderId,
            //            //                                AcquirerId = x.BankId,
            //            //                                CreatedById = x.CreatedById,
            //            //                                CashierId = x.CashierId,
            //            //                                StatusId = x.StatusId,
            //            //                                StatusCode = x.Status.SystemName,
            //            //                            })
            //            //                            .Where(_Request.SearchCondition)
            //            //                            .Select(x => x.AccountId)
            //            //                            .Distinct()
            //            //                            .Count();

            //            _OTransactionOverview.Transactions = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry
            //                                            && x.StatusId == HelperStatus.Transaction.Pending)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                                //ParentId = x.FromAccountId,
            //                                            })
            //                                        .Where(_Request.SearchCondition)
            //                                        .Count();

            //            _OTransactionOverview.InvoiceAmount = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry
            //                                            && x.StatusId == HelperStatus.Transaction.Pending)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                                //ParentId = x.FromAccountId,
            //                                            })
            //                                        .Where(_Request.SearchCondition)
            //                                        .Sum(x => x.InvoiceAmount);


            //            _OTransactionOverview.RewardAmount = _HCoreContext.TUCLoyaltyPending
            //                                            .Where(x => x.LoyaltyTypeId == Loyalty.Reward
            //                                    && x.Merchant.CountryId == _Request.UserReference.SystemCountry
            //                                            && x.StatusId == HelperStatus.Transaction.Pending)
            //                                            .Select(x => new OTransaction.Sale
            //                                            {
            //                                                ReferenceId = x.Id,
            //                                                ReferenceKey = x.Guid,
            //                                                InvoiceAmount = x.InvoiceAmount,
            //                                                TransactionDate = x.TransactionDate,
            //                                                TypeId = x.TypeId,

            //                                                SourceId = x.SourceId,
            //                                                SourceCode = x.Source.SystemName,
            //                                                SourceName = x.Source.Name,
            //                                                RewardAmount = x.TotalAmount,
            //                                                CommissionAmount = x.CommissionAmount,
            //                                                UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                                ReferenceNumber = x.ReferenceNumber,
            //                                                UserAccountId = x.ToAccountId,
            //                                                UserAccountKey = x.ToAccount.Guid,
            //                                                UserDisplayName = x.ToAccount.DisplayName,
            //                                                UserMobileNumber = x.ToAccount.MobileNumber,
            //                                                ParentId = x.MerchantId,
            //                                                SubParentId = x.StoreId,
            //                                                ProviderId = x.ProviderId,
            //                                                AcquirerId = x.AcquirerId,
            //                                                CreatedById = x.CreatedById,
            //                                                CashierId = x.CashierId,
            //                                                StatusId = x.StatusId,
            //                                                StatusCode = x.Status.SystemName,
            //                                                TerminalReferenceId = x.TerminalId,
            //                                                TerminalReferenceKey = x.Terminal.Guid,
            //                                                TerminalId = x.Terminal.IdentificationNumber,
            //                                                //ParentId = x.FromAccountId,
            //                                            })
            //                                      .Where(_Request.SearchCondition)
            //                                      .Sum(x => x.RewardAmount);

            //            //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
            //            //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                         && x.ModeId == TransactionMode.Credit
            //            //                           && x.CampaignId == null
            //            //                          && x.ParentTransaction.TotalAmount > 0
            //            //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                             && x.SourceId == TransactionSource.TUC)
            //            //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                         .Select(x => new OTransaction.Sale
            //            //                         {
            //            //                             ReferenceId = x.Id,
            //            //                             ReferenceKey = x.Guid,
            //            //                             InvoiceAmount = x.PurchaseAmount,
            //            //                             TransactionDate = x.TransactionDate,
            //            //                             TypeId = x.TypeId,
            //            //                             RewardAmount = x.ReferenceAmount,
            //            //                             UserAmount = x.TotalAmount,
            //            //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                             CardBankId = x.CardBankId,
            //            //                             CardBrandId = x.CardBrandId,
            //            //                             AccountNumber = x.AccountNumber,
            //            //                             ReferenceNumber = x.ReferenceNumber,
            //            //                             UserAccountId = x.AccountId,
            //            //                             UserAccountKey = x.Account.Guid,
            //            //                             UserDisplayName = x.Account.DisplayName,
            //            //                             UserMobileNumber = x.Account.MobileNumber,
            //            //                             StoreReferenceId = x.SubParentId,
            //            //                             ProviderId = x.ProviderId,
            //            //                             AcquirerId = x.BankId,
            //            //                             CreatedById = x.CreatedById,
            //            //                             CashierId = x.CashierId,
            //            //                             StatusId = x.StatusId,
            //            //                             StatusCode = x.Status.SystemName,
            //            //                         })
            //            //                         .Where(_Request.SearchCondition)
            //            //                         .Sum(x => x.CommissionAmount);

            //            //_OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
            //            //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //            //                            && x.ParentId == _Request.AccountId
            //            //                         && x.ModeId == TransactionMode.Credit
            //            //                           && x.CampaignId == null
            //            //                          && x.ParentTransaction.TotalAmount > 0
            //            //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //            //                             && x.SourceId == TransactionSource.TUC)
            //            //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //            //                         .Select(x => new OTransaction.Sale
            //            //                         {
            //            //                             ReferenceId = x.Id,
            //            //                             ReferenceKey = x.Guid,
            //            //                             InvoiceAmount = x.PurchaseAmount,
            //            //                             TransactionDate = x.TransactionDate,
            //            //                             TypeId = x.TypeId,
            //            //                             RewardAmount = x.ReferenceAmount,
            //            //                             UserAmount = x.TotalAmount,
            //            //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //            //                             CardBankId = x.CardBankId,
            //            //                             CardBrandId = x.CardBrandId,
            //            //                             AccountNumber = x.AccountNumber,
            //            //                             ReferenceNumber = x.ReferenceNumber,
            //            //                             UserAccountId = x.AccountId,
            //            //                             UserAccountKey = x.Account.Guid,
            //            //                             UserDisplayName = x.Account.DisplayName,
            //            //                             UserMobileNumber = x.Account.MobileNumber,
            //            //                             StoreReferenceId = x.SubParentId,
            //            //                             ProviderId = x.ProviderId,
            //            //                             AcquirerId = x.BankId,
            //            //                             CreatedById = x.CreatedById,
            //            //                             CashierId = x.CashierId,
            //            //                             StatusId = x.StatusId,
            //            //                             StatusCode = x.Status.SystemName,
            //            //                         })
            //            //                         .Where(_Request.SearchCondition)
            //            //                         .Sum(x => x.UserAmount);

            //            #region Send Response
            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
            //            #endregion
            //        }
            //    }

            //}
            //catch (Exception _Exception)
            //{
            //    HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
            //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            //}
            #endregion

            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && x.ModeId == TransactionMode.Credit
                    //                            && x.CampaignId == null
                    //                             && x.ParentTransaction.TotalAmount > 0
                    //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                            || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                RewardAmount = x.ReferenceAmount,
                    //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                                UserAmount = x.TotalAmount,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusId = x.StatusId,
                    //                                StatusCode = x.Status.SystemName,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Select(x => x.AccountId)
                    //                            .Distinct()
                    //                            .Count();
                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.CampaignId == null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    UserAmount = x.TotalAmount,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    UserAmount = x.TotalAmount,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  UserAmount = x.TotalAmount,
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  SubParentId = x.SubParentId,
                                                  SubParentDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);

                    //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                    //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                         && x.ModeId == TransactionMode.Credit
                    //                           && x.CampaignId == null
                    //                          && x.ParentTransaction.TotalAmount > 0
                    //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                             && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                         || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                         .Select(x => new OTransaction.Sale
                    //                         {
                    //                             ReferenceId = x.Id,
                    //                             ReferenceKey = x.Guid,
                    //                             InvoiceAmount = x.PurchaseAmount,
                    //                             TransactionDate = x.TransactionDate,
                    //                             TypeId = x.TypeId,
                    //                             RewardAmount = x.ReferenceAmount,
                    //                             UserAmount = x.TotalAmount,
                    //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                             CardBankId = x.CardBankId,
                    //                             CardBrandId = x.CardBrandId,
                    //                             AccountNumber = x.AccountNumber,
                    //                             ReferenceNumber = x.ReferenceNumber,
                    //                             UserAccountId = x.AccountId,
                    //                             UserAccountKey = x.Account.Guid,
                    //                             UserDisplayName = x.Account.DisplayName,
                    //                             UserMobileNumber = x.Account.MobileNumber,
                    //                             StoreReferenceId = x.SubParentId,
                    //                             ProviderId = x.ProviderId,
                    //                             AcquirerId = x.BankId,
                    //                             CreatedById = x.CreatedById,
                    //                             CashierId = x.CashierId,
                    //                             StatusId = x.StatusId,
                    //                             StatusCode = x.Status.SystemName,
                    //                         })
                    //                         .Where(_Request.SearchCondition)
                    //                         .Sum(x => x.CommissionAmount);

                    _OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  UserAmount = x.TotalAmount,
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  SubParentId = x.SubParentId,
                                                  SubParentDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid
                                              })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.UserAmount);

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
        }

        /// <summary>
        /// Gets the reward claim transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardClaimTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorRewardClaimTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorRewardClaimTransactionDownloader>("ActorRewardClaimTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the reward claim transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRewardClaimTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardClaimDownload = new List<OTransaction.RewardClaimDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "RewardClaim_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RewardClaimDownload.Add(new OTransaction.RewardClaimDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            ClaimAmount = SalesItem.ClaimAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("RewardClaim_Sheet");
                        PropertyInfo[] properties = _RewardClaimDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RewardClaimDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("RewardClaim_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            //using (_HCoreContext = new HCoreContext())
                            //{
                            //    _HCUAccountParameter = new HCUAccountParameter();
                            //    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            //    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                            //    _HCUAccountParameter.IconStorageId = _StorageId;
                            //    _HCUAccountParameter.Name = "RewardClaim_Sheet";
                            //    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            //    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                            //    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            //    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            //    _HCoreContext.SaveChanges();
                            //}
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the reward claim transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardClaimTransactionOverview(OList.Request _Request)
        {
            if (string.IsNullOrEmpty(_Request.SearchCondition))
            {
                HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", ">");
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.ClaimAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the redeem transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorRedeemTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorRedeemTransactionDownloader>("ActorRedeemTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the redeem transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRedeemTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RedeemDownload = new List<OTransaction.RedeemDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Redeem_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {

                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RedeemDownload.Add(new OTransaction.RedeemDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            RedeemAmount = SalesItem.RedeemAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                            Balance = SalesItem.Balance,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Redeem_Sheet");
                        PropertyInfo[] properties = _RedeemDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RedeemDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Redeem_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,

                                                  SourceId = x.SourceId,
                                                  SourceCode = x.Source.SystemName,
                                                  SourceName = x.Source.Name,
                                                  RedeemAmount = x.TotalAmount,
                                                  RewardAmount = x.ReferenceAmount,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  Balance = x.Balance,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RedeemAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the campaign transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ReferenceAmount > 0
                                                && x.CampaignId != null
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCampaignTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the campaign transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.CampaignId != null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                              && x.ReferenceAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,

                                                  SourceId = x.SourceId,
                                                  SourceCode = x.Source.SystemName,
                                                  SourceName = x.Source.Name,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCampaignTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactions(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AllTransactions = new List<OTransaction.All>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                 && x.TypeId != TransactionType.ThankUCashPlusCredit
                                                 && x.TypeId != TransactionType.TucSuperCash
                                                 && x.SourceId != TransactionSource.TUCSuperCash
                                                 //&& x.ModeId == TransactionMode.Debit
                                                 )
                                                 .Select(x => new OTransaction.All
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     ParentId = x.ParentId,
                                                     ParentKey = x.Parent.Guid,
                                                     ParentDisplayName = x.Parent.DisplayName,
                                                     ParentMobileNumber = x.Parent.MobileNumber,

                                                     UserAccountId = x.AccountId,
                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,
                                                     UserAccountMobileNumber = x.Account.MobileNumber,

                                                     SubParentId = x.SubParentId,
                                                     SubParentDisplayName = x.SubParent.DisplayName,
                                                     SubParentKey = x.SubParent.Guid,
                                                     OwnerId = x.Account.OwnerId,

                                                     TypeId = x.TypeId,
                                                     TypeKey = x.Type.SystemName,
                                                     TypeName = x.Type.Name,
                                                     //TypeCategory = x.Type.Parent.SystemName,
                                                     TypeCategory = x.Type.SubParent.SystemName,

                                                     ModeId = x.ModeId,
                                                     ModeKey = x.Mode.SystemName,
                                                     ModeName = x.Mode.Name,

                                                     SourceId = x.SourceId,
                                                     SourceKey = x.Source.SystemName,
                                                     SourceName = x.Source.Name,

                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     CommissionAmount = x.ComissionAmount,

                                                     TotalAmount = x.TotalAmount,
                                                     RedeemAmount = x.TotalAmount,
                                                     InvoiceAmount = x.PurchaseAmount,
                                                     ReferenceInvoiceAmount = x.PurchaseAmount,
                                                     TransactionDate = x.TransactionDate,

                                                     ReferenceNumber = x.ReferenceNumber,
                                                     GroupKey = x.GroupKey,

                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                     ParentTransactionAmount = x.ParentTransaction.Amount,
                                                     ParentTransactionChargeAmount = x.ParentTransaction.Charge,
                                                     ParentTransactionCommissionAmount = x.ParentTransaction.ComissionAmount,
                                                     ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,

                                                     CashierId = x.CashierId,
                                                     CashierKey = x.Cashier.Guid,
                                                     CashierDisplayName = x.Cashier.Name,
                                                     CashierCode = x.Cashier.DisplayName,
                                                     CardBankId = x.CardBankId,
                                                     CardBankName = x.CardBank.Name,
                                                     CardBrandId = x.CardBrandId,
                                                     CardBrandName = x.CardBrand.Name,
                                                     ProviderId = x.ProviderId,
                                                     AcquirerId = x.BankId,
                                                     AccountNumber = x.AccountNumber,
                                                     TerminalReferenceId = x.TerminalId,
                                                     TerminalReferenceKey = x.Terminal.Guid,
                                                     TerminalId = x.Terminal.IdentificationNumber,
                                                     Balance = x.Balance

                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion

                    #region Get Data
                    _AllTransactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                 && x.TypeId != TransactionType.ThankUCashPlusCredit
                                                 && x.TypeId != TransactionType.TucSuperCash
                                                 && x.SourceId != TransactionSource.TUCSuperCash
                                                 //&& x.ModeId == TransactionMode.Debit
                                                 )
                                                .Select(x => new OTransaction.All
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ParentId = x.ParentId,
                                                    ParentKey = x.Parent.Guid,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentMobileNumber = x.Parent.MobileNumber,

                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,

                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserAccountDisplayName = x.Account.DisplayName,
                                                    UserAccountMobileNumber = x.Account.MobileNumber,

                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentKey = x.SubParent.Guid,
                                                    OwnerId = x.Account.OwnerId,

                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    //TypeCategory = x.Type.Parent.SystemName,
                                                    TypeCategory = x.Type.SubParent.SystemName,

                                                    ModeId = x.ModeId,
                                                    ModeKey = x.Mode.SystemName,
                                                    ModeName = x.Mode.Name,

                                                    SourceId = x.SourceId,
                                                    SourceKey = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    CommissionAmount = x.ComissionAmount,

                                                    TotalAmount = x.TotalAmount,
                                                    RedeemAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    ReferenceInvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    GroupKey = x.GroupKey,

                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                    ParentTransactionAmount = x.ParentTransaction.Amount,
                                                    ParentTransactionChargeAmount = x.ParentTransaction.Charge,
                                                    ParentTransactionCommissionAmount = x.ParentTransaction.ComissionAmount,
                                                    ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierDisplayName = x.Cashier.Name,
                                                    CashierCode = x.Cashier.DisplayName,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    CreatedDate = x.CreateDate,
                                                    Balance = x.Balance
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _AllTransactions)
                    {
                        DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                        var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                        if (ParentDetails != null)
                        {
                            DataItem.ParentKey = ParentDetails.ReferenceKey;
                            DataItem.ParentDisplayName = ParentDetails.DisplayName;
                            DataItem.ParentIconUrl = ParentDetails.IconUrl;
                        }
                        var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.SubParentId);
                        if (SubParentDetails != null)
                        {
                            DataItem.SubParentKey = SubParentDetails.ReferenceKey;
                            DataItem.SubParentDisplayName = SubParentDetails.DisplayName;
                        }
                        var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                        if (AcquirerDetails != null)
                        {
                            DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                            DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                            //DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                        }
                        var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                        if (ProviderDetails != null)
                        {
                            DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                            DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                            //DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                        }
                        var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                        if (CashierDetails != null)
                        {
                            DataItem.CashierKey = CashierDetails.ReferenceKey;
                            DataItem.CashierCode = CashierDetails.DisplayName;
                            DataItem.CashierDisplayName = CashierDetails.Name;
                        }

                        var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                        if (CreatedByDetails != null)
                        {
                            DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                            DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                            DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                        }
                        if (!string.IsNullOrEmpty(DataItem.TypeName))
                        {
                            DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                        }
                        DataItem.ExpiryDate = DataItem.CreatedDate.AddYears(1);
                    }

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AllTransactions, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the transactions overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactionsOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _Overview = new OTransaction.Overview();
                    #region Get Data
                    _Overview.Total = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                 && x.TypeId != TransactionType.ThankUCashPlusCredit
                                                 && x.TypeId != TransactionType.TucSuperCash
                                                 && x.SourceId != TransactionSource.TUCSuperCash
                                                 //&& x.ModeId == TransactionMode.Debit
                                                 )
                                                .Select(x => new OTransaction.All
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ParentId = x.ParentId,
                                                    UserAccountId = x.AccountId,
                                                    OwnerId = x.Account.OwnerId,
                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.SystemName,
                                                    ModeId = x.ModeId,
                                                    ModeKey = x.Mode.SystemName,
                                                    SourceId = x.SourceId,
                                                    SourceKey = x.Source.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TransactionDate = x.TransactionDate
                                                })
                                             .Where(_Request.SearchCondition)
                                             .Count();
                    _Overview.Amount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                 && x.TypeId != TransactionType.ThankUCashPlusCredit
                                                 && x.TypeId != TransactionType.TucSuperCash
                                                 && x.SourceId != TransactionSource.TUCSuperCash
                                                 //&& x.ModeId == TransactionMode.Debit
                                                 )
                                                .Select(x => new OTransaction.All
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ParentId = x.ParentId,
                                                    UserAccountId = x.AccountId,
                                                    OwnerId = x.Account.OwnerId,
                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.SystemName,
                                                    ModeId = x.ModeId,
                                                    ModeKey = x.Mode.SystemName,
                                                    SourceId = x.SourceId,
                                                    SourceKey = x.Source.SystemName,
                                                    Amount = x.Amount,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TransactionDate = x.TransactionDate
                                                })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.Amount);
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactionsOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the acquirer merchant sale list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerMerchantSales(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                List<OAccount.Account> Data = new List<OAccount.Account>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = 0;
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    }
                    switch (_Request.Type)
                    {

                        case ThankUCashConstant.ListType.SubOwner:
                            #region Total Records
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                                         where x.CountryId == _Request.UserReference.SystemCountry
                                                         where x.AccountTypeId == UserAccountType.Merchant
                                                         && x.TUCTerminalMerchant.Any(m => m.AcquirerId == UserAccountId)
                                                         select new OAccount.Account
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             IconUrl = x.IconStorage.Path,
                                                             ContactNumber = x.ContactNumber,
                                                             EmailAddress = x.EmailAddress,
                                                             StatusId = x.StatusId,
                                                             CreateDate = x.CreateDate,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                         .Count(m => m.ParentId == x.Id
                                                                          && m.CreatedBy.BankId == UserAccountId
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))),
                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == x.Id
                                                                          && m.CreatedBy.BankId == UserAccountId
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))).Sum(m => m.PurchaseAmount),

                                                         })
                                         .Where(_Request.SearchCondition)
                                        .Count();
                            }
                            #endregion
                            #region Get Data
                            Data = (from x in _HCoreContext.HCUAccount
                                    where x.CountryId == _Request.UserReference.SystemCountry
                                    where x.AccountTypeId == UserAccountType.Merchant
                                    && x.TUCTerminalMerchant.Any(m => m.AcquirerId == UserAccountId)
                                    select new OAccount.Account
                                    {
                                        ReferenceId = x.Id,
                                        ReferenceKey = x.Guid,
                                        DisplayName = x.DisplayName,
                                        IconUrl = x.IconStorage.Path,
                                        ContactNumber = x.ContactNumber,
                                        EmailAddress = x.EmailAddress,
                                        StatusId = x.StatusId,
                                        CreateDate = x.CreateDate,
                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                         .Count(m => m.ParentId == x.Id
                                                                          && m.CreatedBy.BankId == UserAccountId
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))),
                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == x.Id
                                                                          && m.CreatedBy.BankId == UserAccountId
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))).Sum(m => m.PurchaseAmount),


                                    })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            break;
                        default:
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerMerchantSales", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion


        }

        /// <summary>
        /// Gets the reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerRewardTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  Balance = x.Balance,
                                                  SourceId = x.SourceId,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);

                    _OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                                             .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                             .Select(x => new OTransaction.Sale
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 InvoiceAmount = x.PurchaseAmount,
                                                 TransactionDate = x.TransactionDate,
                                                 TypeId = x.TypeId,
                                                 RewardAmount = x.ReferenceAmount,
                                                 UserAmount = x.TotalAmount,
                                                 CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                 CardBankId = x.CardBankId,
                                                 CardBrandId = x.CardBrandId,
                                                 AccountNumber = x.AccountNumber,
                                                 ReferenceNumber = x.ReferenceNumber,
                                                 UserAccountId = x.AccountId,
                                                 UserAccountKey = x.Account.Guid,
                                                 UserDisplayName = x.Account.DisplayName,
                                                 UserMobileNumber = x.Account.MobileNumber,
                                                 ParentId = x.ParentId,
                                                 SubParentId = x.SubParentId,
                                                 ProviderId = x.ProviderId,
                                                 AcquirerId = x.BankId,
                                                 CreatedById = x.CreatedById,
                                                 CashierId = x.CashierId,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 TerminalReferenceId = x.TerminalId,
                                                 TerminalReferenceKey = x.Terminal.Guid,
                                                 TerminalId = x.Terminal.IdentificationNumber,
                                                 Balance = x.Balance,
                                                 SourceId = x.SourceId,
                                                 ProgramId = x.ProgramId,
                                                 ProgramReferenceKey = x.Program.Guid
                                             })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.CommissionAmount);

                    _OTransactionOverview.CashierRewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                              && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                              && x.ModeId == TransactionMode.Credit
                                              && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                              && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusId = x.StatusId,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  Balance = x.Balance,
                                                  SourceId = x.SourceId,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid,
                                                  CashierReward = x.CashierReward
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.CashierReward);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerRewardTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerRedeemTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    CardBankId = x.CardBankId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                  .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    Balance = x.Balance,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    CardBankId = x.CardBankId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,

                                                  SourceId = x.SourceId,
                                                  SourceCode = x.Source.SystemName,
                                                  SourceName = x.Source.Name,
                                                  RedeemAmount = x.TotalAmount,
                                                  RewardAmount = x.ReferenceAmount,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  TerminalReferenceId = x.TerminalId,
                                                  TerminalReferenceKey = x.Terminal.Guid,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  Balance = x.Balance,
                                                  ProgramId = x.ProgramId,
                                                  ProgramReferenceKey = x.Program.Guid,
                                                  CardBankId = x.CardBankId
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RedeemAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerRedeemTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the redeem transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerRedeemTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorAcquirerReedeemTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorAcquirerReedeemTransactionDownloader>("ActorAcquirerReedeemTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerRedeemTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the redeem transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetAcquirerRedeemTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SaleDownload = new List<OTransaction.SaleDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "RedeemSales_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                )
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        //foreach (var _Sale in _Sales)
                        //{
                        if (SalesItem.StatusId == HelperStatus.Transaction.Pending)
                        {
                            SalesItem.StatusCode = "transaction.pending";
                            SalesItem.StatusId = HelperStatus.Transaction.Success;
                        }
                        //}
                        _SaleDownload.Add(new OTransaction.SaleDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("RedeemSales_Sheet");
                        PropertyInfo[] properties = _SaleDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_SaleDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("RedeemSales_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerRedeemTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the sale transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorSaleTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorSaleTransactionDownloader>("ActorSaleTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBankDisplayName = x.CardBank.SystemName,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    CardBrandDisplayName = x.CardBrand.SystemName,

                                                    CardSubBrandId = x.CardSubBrandId,
                                                    CardSubBrandName = x.CardSubBrand.Name,
                                                    CardSubBrandDisplayName = x.CardSubBrand.SystemName,

                                                    CardTypeId = x.CardTypeId,
                                                    CardTypeCode = x.CardType.Guid,
                                                    CardTypeName = x.CardType.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    UserStatusId = x.Account.StatusId,
                                                    UserStatusCode = x.Account.Status.SystemName,
                                                    UserStatusName = x.Account.Status.Name,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    ParentStatusId = x.Account.StatusId,
                                                    ParentStatusCode = x.Account.Status.SystemName,
                                                    ParentStatusName = x.Account.Status.Name,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,
                                                    ProviderStatusId = x.Account.StatusId,
                                                    ProviderStatusCode = x.Account.Status.SystemName,
                                                    ProviderStatusName = x.Account.Status.Name,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,
                                                    AcquirerStatusId = x.Account.StatusId,
                                                    AcquirerStatusCode = x.Account.Status.SystemName,
                                                    AcquirerStatusName = x.Account.Status.Name,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierStatusId = x.Account.StatusId,
                                                    CashierStatusCode = x.Account.Status.SystemName,
                                                    CashierStatusName = x.Account.Status.Name,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalStatusId = x.Account.StatusId,
                                                    TerminalStatusCode = x.Account.Status.SystemName,
                                                    TerminalStatusName = x.Account.Status.Name,

                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the sale transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetAcquirerTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SaleDownload = new List<OTransaction.SaleDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Transaction_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                // && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //  && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid,
                                                    ModeId = x.ModeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    RewardUserAmount = x.TotalAmount,
                                                    RewardCommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        //foreach (var _Sale in _Sales)
                        //{
                        if (SalesItem.StatusId == HelperStatus.Transaction.Pending)
                        {
                            SalesItem.StatusCode = "transaction.pending";
                            SalesItem.StatusId = HelperStatus.Transaction.Success;
                        }
                        //}
                        _SaleDownload.Add(new OTransaction.SaleDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.SubParentDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            Status = SalesItem.StatusName,
                            TerminalId = SalesItem.TerminalId,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Transaction_Sheet");
                        PropertyInfo[] properties = _SaleDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_SaleDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Transaction_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Gets the sale transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", ">");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                               && (x.StatusId == HelperStatus.Transaction.Failed || x.StatusId == HelperStatus.Transaction.Success)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUC))
                                                // && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,
                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();


                    _OTransactionOverview.FailedTransaction = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                               && x.StatusId == HelperStatus.Transaction.Failed
                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                               // && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                               .Select(x => new OTransaction.Sale
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   InvoiceAmount = x.PurchaseAmount,
                                                   TransactionDate = x.TransactionDate,
                                                   TypeId = x.TypeId,
                                                   ModeId = x.ModeId,

                                                   SourceId = x.SourceId,
                                                   SourceCode = x.Source.SystemName,
                                                   SourceName = x.Source.Name,
                                                   CardBankId = x.CardBankId,
                                                   CardBankName = x.CardBank.Name,
                                                   CardBrandId = x.CardBrandId,
                                                   CardBrandName = x.CardBrand.Name,
                                                   AccountNumber = x.AccountNumber,
                                                   ReferenceNumber = x.ReferenceNumber,
                                                   UserAccountId = x.AccountId,
                                                   UserAccountKey = x.Account.Guid,
                                                   UserDisplayName = x.Account.DisplayName,
                                                   UserMobileNumber = x.Account.MobileNumber,
                                                   ParentId = x.ParentId,
                                                   ParentDisplayName = x.Parent.DisplayName,
                                                   SubParentId = x.SubParentId,
                                                   ProviderId = x.ProviderId,
                                                   AcquirerId = x.BankId,
                                                   CreatedById = x.CreatedById,
                                                   CashierId = x.CashierId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusId = x.StatusId,
                                                   TerminalReferenceId = x.TerminalId,
                                                   TerminalReferenceKey = x.Terminal.Guid,
                                                   TerminalId = x.Terminal.IdentificationNumber,
                                                   ProgramId = x.ProgramId,
                                                   ProgramReferenceKey = x.Program.Guid
                                               })
                                                 .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                // && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);

                    _OTransactionOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CardReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);

                    _OTransactionOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                                               && (x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ModeId = x.ModeId,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    ProgramId = x.ProgramId,
                                                    ProgramReferenceKey = x.Program.Guid
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Cancels the transaction
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CancelTransaction(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Comment is required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var TransactionInfo = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey
                        && x.StatusId == HelperStatus.Transaction.Success)
                        .FirstOrDefault();
                    if (TransactionInfo != null)
                    {
                        if (TransactionInfo.StatusId == HelperStatus.Transaction.Success)
                        {
                            var TransactionsList = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == TransactionInfo.GroupKey).ToList();
                            foreach (var item in TransactionsList)
                            {
                                item.StatusId = HelperStatus.Transaction.Cancelled;
                                item.Comment = _Request.Comment;
                                item.ModifyById = _Request.UserReference.AccountId;
                                item.ModifyDate = HCoreHelper.GetGMTDate();
                                _HCoreContext.SaveChanges();
                            }
                            var _Response = new
                            {
                                ReferenceId = _Request.ReferenceId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            //DateTime today = DateTime.Now;
                            //if ((today - Details.TransactionDate).TotalHours < 24)
                            //{
                            //    Details.StatusId = HelperStatus.Transaction.Cancelled;
                            //    Details.Comment = _Request.Comment;
                            //    Details.ModifyById = _Request.UserReference.AccountId;
                            //    Details.ModifyDate = HCoreHelper.GetGMTDate();
                            //    _HCoreContext.SaveChanges();
                            //}
                            //else
                            //{
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Response, "CA0404", "Reward amount can only be cancelled if transaction timespan is less than 24 hours.");
                            //}
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA1037", "Transaction cancelled successfully.");

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1037", "Transaction must be successful for cancellation");
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Details not found. It might be removed or not available at the moment");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CancelTransaction", _Exception, _Request.UserReference, "CA0500", "Internal server error occured. Please try after some time");
            }
            #endregion
        }

        /// <summary>
        /// Gets the wallet history overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetWalletHistoryOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Points = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && (x.TypeId == TransactionType.TUCWalletTopup))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    Amount = x.Amount
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.Amount);


                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Credit
                                                && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                 && (((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash)
                                                && (x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.CardReward)
                                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                 || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Amount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.Amount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                              && x.ModeId == TransactionMode.Credit
                                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  Amount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  Balance = x.Balance,
                                                  SourceId = x.SourceId,
                                                  SourceCode = x.Source.SystemName
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.Amount);
                    _OTransactionOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,

                                                  SourceId = x.SourceId,
                                                  SourceCode = x.Source.SystemName,
                                                  SourceName = x.Source.Name,
                                                  Amount = x.TotalAmount,
                                                  RewardAmount = x.ReferenceAmount,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  ParentId = x.ParentId,
                                                  SubParentId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  Balance = x.Balance
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.Amount);

                    _OTransactionOverview.Cashouts = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && x.ModeId == TransactionMode.Debit
                                                && x.TypeId == TransactionType.TUCCashOut.CashOut
                                                && (x.SourceId == TransactionSource.Merchant || x.SourceId == TransactionSource.TUC)
                                               )
                                                .Select(x => new OTransaction.All
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    Amount = x.Amount,
                                                    TotalAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    ParentId = x.ParentId,
                                                    SubParentId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    Balance = x.Balance,
                                                    SourceId = x.SourceId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.Amount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetWalletHistoryOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
