//==================================================================================
// FileName: FrameworkLiveMerchants.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to live merchant
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using HCore.Data.Store;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkLiveMerchants
    {
        List<OHCoreDataStore.Terminal> _Terminals;
        OLiveMerchants.Response _LiveMerchantResponse;
        List<OLiveMerchants.Merchant> _Merchants;
        /// <summary>
        /// Description: Gets the live merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLiveMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                DateTime ActiveTerminalsStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime ActiveTerminalEnd = ActiveTerminalsStart.AddDays(1);
                DateTime IdleTerminalStart = ActiveTerminalsStart.AddDays(-7);
                DateTime IdleTerminalEnd = ActiveTerminalsStart.AddSeconds(1);
                DateTime DeadTerminalStart = ActiveTerminalsStart.AddDays(7);
                _LiveMerchantResponse = new OLiveMerchants.Response();
                _Merchants = new List<OLiveMerchants.Merchant>();
                _Terminals = new List<OHCoreDataStore.Terminal>();
                if (_Request.Type == "acquirer")
                {

                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.AcquirerId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderBy(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "provider")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.ProviderId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "merchant")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.MerchantId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "store")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.StoreId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "acquirersubaccount")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.RmId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else
                {
                    _Terminals = HCoreDataStore.Terminals
                        .Where(x => x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                foreach (var Terminal in _Terminals)
                {
                    if (_Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault() != null)
                    {
                        _Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault().LastTransactionDate = Terminal.LastTransactionDate;
                        _Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault().TodayTotalTransaction = _Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault().TodayTotalTransaction + Terminal.TodayTotalTransaction;
                        _Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault().TodayTotalInvoiceAmount = _Merchants.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault().TodayTotalInvoiceAmount + Terminal.TodayTotalInvoiceAmount;
                    }
                    else
                    {
                        if (Terminal.MerchantId != null)
                        {
                            _Merchants.Add(new OLiveMerchants.Merchant
                            {
                                ReferenceId = Terminal.MerchantId,
                                ReferenceKey = Terminal.MerchantKey,
                                DisplayName = Terminal.MerchantDisplayName,
                                IconUrl = Terminal.MerchantIconUrl,
                                LastTransactionDate = Terminal.LastTransactionDate
                            });
                        }
                    }
                }
                foreach (var Merchant in _Merchants)
                {
                    Merchant.TotalTerminals = _Terminals.Count(x => x.MerchantId == Merchant.ReferenceId);
                    Merchant.ActiveTerminals = _Terminals.Count(x => x.MerchantId == Merchant.ReferenceId && x.LastTransactionDate > ActiveTerminalsStart && x.LastTransactionDate < ActiveTerminalEnd);
                    Merchant.IdleTerminals = _Terminals.Count(x => x.MerchantId == Merchant.ReferenceId && x.LastTransactionDate > IdleTerminalStart && x.LastTransactionDate < IdleTerminalEnd);
                    Merchant.DeadTerminals = _Terminals.Count(x => x.MerchantId == Merchant.ReferenceId && x.LastTransactionDate < DeadTerminalStart);
                }

                OList.Response _OResponse = HCoreHelper.GetListResponse(0, _Merchants.OrderByDescending(x=>x.LastTransactionDate).ToList(), 0, 0);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAppUsers", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

    }
}
