//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using HCore.ThankUCash.Core;
using System.Linq.Dynamic.Core;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkAnalytics
    {
        HCoreContext _HCoreContext;
        OStoreBankCollection.Response _OStoreBankCollectionResponse;
        /// <summary>
        /// Description: Synchronizes the store bank collections.
        /// </summary>
        internal void SyncStoreBankCollections()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var StoresList = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new
                        {
                            x.Id,
                            CreateDate = x.Owner.CreateDate,

                        }
                    ).ToList();
                    var Acquirers = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Acquirer && x.StatusId == HelperStatus.Default.Active)
                    .Select(x => x.Id).ToList();
                    var Terminals = _HCoreContext.TUCTerminal
                    .Select(x => new
                    {
                        TerminalId = x.Id,
                        AcquirerId = x.AcquirerId,
                        StoreId = x.StoreId,
                    }).ToList();
                    _HCoreContext.Dispose();
                    DateTime SummaryDate = HCoreHelper.GetGMTDate().AddDays(-5);
                    DateTime SummaryStartDate = HCoreHelper.GetNigeriaToGMT(SummaryDate).AddSeconds(-1);
                    DateTime SummaryEndDate = SummaryStartDate.AddDays(1).AddSeconds(1);
                    DateTime FinalDate = HCoreHelper.GetGMTDate();
                    int DaysDifference = (FinalDate - SummaryDate).Days;
                    for (int i = 0; i < DaysDifference; i++)
                    {

                        foreach (var StoreInfo in StoresList)
                        {
                            long StoreId = StoreInfo.Id;

                            using (_HCoreContext = new HCoreContext())
                            {
                                long StoreTransactions = _HCoreContext.HCUAccountTransaction
                                                                             .Count(m => m.SubParentId == StoreId
                                                                                && m.TransactionDate > SummaryStartDate
                                                                                && m.TransactionDate < SummaryEndDate
                                                                                && m.TypeId == TransactionType.CardReward
                                                                                && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                               );

                                if (StoreTransactions > 0)
                                {
                                    foreach (var AcquirerId in Acquirers)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            double AcquireInvoiceAmount = 0;
                                            var AcquirerTerminals = Terminals.Where(x => x.AcquirerId == AcquirerId && x.StoreId == StoreInfo.Id).ToList();
                                            foreach (var AcquirerTerminal in AcquirerTerminals)
                                            {
                                                AcquireInvoiceAmount = AcquireInvoiceAmount + _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.SubParentId == StoreId
                                                                          && m.TypeId == TransactionType.CardReward
                                                                          && m.CreatedById == AcquirerTerminal.TerminalId
                                                                             && m.TransactionDate > SummaryStartDate
                                                                             && m.TransactionDate < SummaryEndDate
                                                                           && ((m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                            }
                                            if (AcquireInvoiceAmount > 0)
                                            {

                                                long DataCheck = _HCoreContext.TUCStoreAcquirerCollection
                                                    .Where(x => x.StoreId == StoreId && x.AcquirerId == AcquirerId && x.Date == SummaryDate).Select(x => x.Id).FirstOrDefault();
                                                if (DataCheck == 0)
                                                {
                                                    TUCStoreAcquirerCollection _TUCStoreAcquirerCollection = new TUCStoreAcquirerCollection();
                                                    _TUCStoreAcquirerCollection.Guid = HCoreHelper.GenerateGuid();
                                                    _TUCStoreAcquirerCollection.Date = SummaryDate;
                                                    _TUCStoreAcquirerCollection.StoreId = StoreId;
                                                    _TUCStoreAcquirerCollection.AcquirerId = AcquirerId;
                                                    _TUCStoreAcquirerCollection.Terminals = AcquirerTerminals.Count();
                                                    _TUCStoreAcquirerCollection.InvoiceAmount = AcquireInvoiceAmount;
                                                    _TUCStoreAcquirerCollection.NibsPercentage = 0.75;
                                                    _TUCStoreAcquirerCollection.NibsAmount = HCoreHelper.GetPercentage(AcquireInvoiceAmount, 0.75, 3);
                                                    _TUCStoreAcquirerCollection.ExpectedAmount = _TUCStoreAcquirerCollection.InvoiceAmount - _TUCStoreAcquirerCollection.NibsAmount;
                                                    _TUCStoreAcquirerCollection.ReceivedAmount = 0;
                                                    _TUCStoreAcquirerCollection.AmountDifference = _TUCStoreAcquirerCollection.ReceivedAmount - _TUCStoreAcquirerCollection.ExpectedAmount;
                                                    _TUCStoreAcquirerCollection.CreateDate = HCoreHelper.GetGMTDateTime();
                                                    _TUCStoreAcquirerCollection.CreatedById = 1;
                                                    _TUCStoreAcquirerCollection.StatusId = HelperStatus.Default.Active;
                                                    _HCoreContext.TUCStoreAcquirerCollection.Add(_TUCStoreAcquirerCollection);
                                                    _HCoreContext.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        SummaryDate = SummaryDate.AddDays(1);
                        SummaryStartDate = SummaryStartDate.AddDays(1);
                        SummaryEndDate = SummaryEndDate.AddDays(1);
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SyncStoreBankCollections", _Exception);
                #endregion

            }
        }
        /// <summary>
        /// Description: Gets the store bank collection.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreBankCollection(OStoreBankCollection.Request _Request)
        {
            _OStoreBankCollectionResponse = new OStoreBankCollection.Response();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long UserAccountId = 0;
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();
                    }
                    //_Request.EndDate = _Request.StartDate.Value.AddDays(1);
                    //_Request.StartDate = _Request.StartDate.Value.AddSeconds(-1);
                    _OStoreBankCollectionResponse.Stores = _HCoreContext.HCUAccount
                        .Where(x => x.OwnerId == UserAccountId && x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new OStoreBankCollection.OStore
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            StoreName = x.DisplayName,
                        }).ToList();
                    foreach (var StoreInfo in _OStoreBankCollectionResponse.Stores)
                    {
                        StoreInfo.AmountDistribution = _HCoreContext.TUCStoreAcquirerCollection
                            .Where(x => x.StoreId == StoreInfo.ReferenceId
                            && x.Date == _Request.StartDate.Value.Date)
                            .Select(x => new OStoreBankCollection.OAcquirerCollection
                            {
                                ReferenceKey = x.Guid,
                                DisplayName = x.Acquirer.DisplayName,
                                IconUrl = _AppConfig.StorageUrl + x.Acquirer.IconStorage.Path,
                                Terminals = x.Terminals,
                                InvoiceAmount = x.InvoiceAmount,
                                NibsAmount = x.NibsAmount,
                                ExpectedAmount = x.ExpectedAmount,
                                ReceivedAmount = x.ReceivedAmount,
                                AmountDifference = x.AmountDifference
                            }
                            ).ToList();
                        StoreInfo.Acquirers = StoreInfo.AmountDistribution.Count();
                        StoreInfo.Terminals = StoreInfo.AmountDistribution.Sum(x => x.Terminals);
                        StoreInfo.InvoiceAmount = StoreInfo.AmountDistribution.Sum(x => x.InvoiceAmount);
                        StoreInfo.NibsAmount = StoreInfo.AmountDistribution.Sum(x => x.NibsAmount);
                        StoreInfo.ExpectedAmount = StoreInfo.AmountDistribution.Sum(x => x.ExpectedAmount);
                        StoreInfo.ReceivedAmount = StoreInfo.AmountDistribution.Sum(x => x.ReceivedAmount);
                        StoreInfo.AmountDifference = StoreInfo.AmountDistribution.Sum(x => x.AmountDifference);
                    }
                }
                _OStoreBankCollectionResponse.Terminals = _OStoreBankCollectionResponse.Stores.Sum(x => x.Terminals);
                _OStoreBankCollectionResponse.InvoiceAmount = _OStoreBankCollectionResponse.Stores.Sum(x => x.InvoiceAmount);
                _OStoreBankCollectionResponse.NibsAmount = _OStoreBankCollectionResponse.Stores.Sum(x => x.NibsAmount);
                _OStoreBankCollectionResponse.ExpectedAmount = _OStoreBankCollectionResponse.Stores.Sum(x => x.ExpectedAmount);
                _OStoreBankCollectionResponse.ReceivedAmount = _OStoreBankCollectionResponse.Stores.Sum(x => x.ReceivedAmount);
                _OStoreBankCollectionResponse.AmountDifference = _OStoreBankCollectionResponse.Stores.Sum(x => x.AmountDifference);
                _OStoreBankCollectionResponse.Stores = _OStoreBankCollectionResponse.Stores.Where(x => x.InvoiceAmount > 0).ToList();
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OStoreBankCollectionResponse, "HC0001");
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Updates the received amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateReceivedAmount(OStoreBankCollection.Request _Request)
        {
            _OStoreBankCollectionResponse = new OStoreBankCollection.Response();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCStoreAcquirerCollection.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {

                        Details.ReceivedAmount = _Request.ReceivedAmount;
                        Details.AmountDifference = Details.ReceivedAmount - Details.ExpectedAmount;
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001", "Received amount updated succcessfully");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }
                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
    }
}
