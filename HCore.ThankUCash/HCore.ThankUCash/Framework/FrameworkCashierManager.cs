//==================================================================================
// FileName: FrameworkCashierManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to cashier
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkCashierManager
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Generates the cashier identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GenerateCashierId(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    string CashierId  = "";
                    long CashierCount = _HCoreContext.HCUAccount.Where(x=>x.Owner.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.MerchantCashier).Select(x=>x.Id).Count();
                    CashierCount  += 1;
                    if (CashierCount < 10)
                    {
                        CashierId = "100"+CashierCount;
                    }
                    else if (CashierCount >  9 && CashierCount < 100)
                    {
                        CashierId = "10"+CashierCount;
                    }
                    else if (CashierCount >  99 && CashierCount < 1000)
                    {
                        CashierId = "1"+CashierCount;
                    }
                    else if (CashierCount > 999)
                    {
                        CashierId = CashierCount.ToString();
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, CashierId, "HC0001");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
