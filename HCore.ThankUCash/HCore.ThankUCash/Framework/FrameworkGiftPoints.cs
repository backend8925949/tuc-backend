//==================================================================================
// FileName: FrameworkGiftPoints.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to giftpoints
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using static HCore.CoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.ThankUCash.Object;
using HCore.Operations;
using HCore.Operations.Object;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkGiftPoints
    {
        OGiftPoints.BalanceResponse _BalanceResponse;
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        Random _Random;
        /// <summary>
        /// Gets the gift points balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGiftPointsBalance(OGiftPoints.BalanceRequest _Request)
        {
            _BalanceResponse = new OGiftPoints.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.AccountId == _Request.UserAccountId
                       && x.SourceId == TransactionSource.GiftPoints
                      && x.StatusId == HelperStatus.Transaction.Success
                       && x.ModeId == TransactionMode.Credit)
                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                      .Where(x => x.AccountId == _Request.UserAccountId
                      && x.StatusId == HelperStatus.Transaction.Success
                      && x.SourceId == TransactionSource.GiftPoints
                      && x.ModeId == TransactionMode.Debit)
                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetGiftPointsBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Credits the gift points.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditGiftPoints(OGiftPoints.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OGiftPoints.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            AccountTypeId = x.AccountTypeId,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                double CommissionPercentage = 0;
                                double SystemAmount = 0;
                                double UserAmount = 0;
                                string TConvincenceCharges = HCoreHelper.GetConfiguration("giftpointsconvinencecharges", AccountDetails.Id);
                                if (!string.IsNullOrEmpty(TConvincenceCharges))
                                {
                                    CommissionPercentage = Convert.ToDouble(TConvincenceCharges);
                                    SystemAmount = HCoreHelper.GetPercentage(_Request.Amount, CommissionPercentage);
                                    UserAmount = _Request.Amount - SystemAmount;
                                }
                                else
                                {
                                    SystemAmount = 0;
                                    UserAmount = _Request.Amount;
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.Gift,
                                    SourceId = TransactionSource.GiftPoints,

                                    Comment = _Request.Comment,
                                    Amount = _Request.Amount,
                                    Comission = SystemAmount,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                           .Where(x => x.AccountId == _Request.UserAccountId
                                           && x.SourceId == TransactionSource.GiftPoints
                                           && x.ModeId == TransactionMode.Credit)
                                           .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.UserAccountId
                                          && x.SourceId == TransactionSource.GiftPoints
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;

                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Amount credited to account");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "This feature is only available for merchants");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                            #endregion
                        }

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                        #endregion
                    }


                }


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditGiftPoints", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Credits the gift points to customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditGiftPointsToCustomer(OGiftPoints.CreditBalanceRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OGiftPoints.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be greater thank 0");
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer mobile number is required");
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            })
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                             .Select(x => new
                                             {
                                                 Id = x.Id,
                                                 AccountTypeId = x.AccountTypeId,
                                                 StatusId = x.StatusId,
                                             })
                                             .FirstOrDefault();
                                    if (CustomerDetails != null)
                                    {
                                        if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                             .Where(x => x.AccountId == _Request.UserAccountId
                                             && x.SourceId == TransactionSource.GiftPoints
                                             && x.ModeId == TransactionMode.Credit)
                                             .Sum(x => (double?)x.TotalAmount) ?? 0;
                                            _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.AccountId == _Request.UserAccountId
                                              && x.SourceId == TransactionSource.GiftPoints
                                              && x.ModeId == TransactionMode.Debit)
                                              .Sum(x => (double?)x.TotalAmount) ?? 0;
                                            _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                            if (_Request.Amount < (_BalanceResponse.Balance + 1))
                                            {
                                                _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                                _CoreTransactionRequest.CustomerId = CustomerDetails.Id;
                                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Request.UserAccountId,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionType.Gift,
                                                    SourceId = TransactionSource.GiftPoints,
                                                    Comment = _Request.Comment,

                                                    Amount = _Request.Amount,
                                                    Comission = 0,
                                                    TotalAmount = _Request.Amount,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = CustomerDetails.Id,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.Gift,
                                                    SourceId = TransactionSource.GiftPoints,
                                                    Amount = _Request.Amount,
                                                    Comment = _Request.Comment,
                                                    Comission = 0,
                                                    TotalAmount = _Request.Amount,
                                                });
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                {
                                                    string Message = "";
                                                    if (!string.IsNullOrEmpty(_Request.Comment))
                                                    {
                                                        Message = _Request.Comment;
                                                    }
                                                    else
                                                    {
                                                        Message = HCoreHelper.GetConfigurationValueByUserAccount("customergiftpointcreditalert", AccountDetails.Id, _Request.UserReference);
                                                    }
                                                    if (!string.IsNullOrEmpty(Message))
                                                    {
                                                        Message = Message.Replace("[AMOUNT]", _Request.Amount.ToString());
                                                        Message = Message.Replace("[DISPLAYNAME]", AccountDetails.DisplayName);
                                                        if (Message.Length > 159)
                                                        {
                                                            Message = Message.Substring(0, 158);
                                                        }
                                                        HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _Request.MobileNumber, Message, CustomerDetails.Id, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    }
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Customer account credited to with " + _Request.Amount.ToString());
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Gift card wallet balance is low. Credit wallet to giveout rewards");
                                            }


                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer account is not active");
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer not registered. Download app or do transactions at partnered merchant stores");
                                    }

                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "This feature is only available for merchants");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditGiftPoints", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Credits the gift points to customer web.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditGiftPointsToCustomerWeb(OGiftPoints.CreditBalanceRequest _Request)
        {
            _Request.Amount = 100;
            _ManageCoreTransaction = new ManageCoreTransaction();
            _BalanceResponse = new OGiftPoints.BalanceResponse();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Amount must be greater thank 0");
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Customer mobile number is required");
                }
                else
                {
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Request.UserAccountId = 3;
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserAccountId)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            })
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                             .Select(x => new
                                             {
                                                 Id = x.Id,
                                                 AccountTypeId = x.AccountTypeId,
                                                 StatusId = x.StatusId,
                                             })
                                             .FirstOrDefault();
                                    if (CustomerDetails != null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Your number already registered with ThankUCash.");
                                    }
                                    else
                                    {
                                        DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                        string UserKey = HCoreHelper.GenerateGuid();
                                        string UserAccountKey = HCoreHelper.GenerateGuid();
                                        _Random = new Random();
                                        string AccessPin = _Random.Next(1111, 9999).ToString();
                                        #region Save User
                                        _HCUAccountAuth = new HCUAccountAuth();
                                        _HCUAccountAuth.Guid = UserKey;
                                        _HCUAccountAuth.Username = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                        _HCUAccountAuth.CreateDate = CreateDate;
                                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                        if (_Request.UserReference.AccountId != 0)
                                        {
                                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                        }
                                        #endregion
                                        #region Online Account
                                        _HCUAccount = new HCUAccount();
                                        _HCUAccount.Guid = UserAccountKey;
                                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _HCUAccount.DisplayName = _Request.Name;
                                        }
                                        else
                                        {
                                            _HCUAccount.DisplayName = _Request.MobileNumber;
                                        }
                                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _HCUAccount.Name = _Request.Name;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                        {
                                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                                            _HCUAccount.ContactNumber = _Request.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                        {
                                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                                            _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                                        }
                                        _HCUAccount.CountryId = _Request.UserReference.CountryId;
                                        _HCUAccount.EmailVerificationStatus = 0;
                                        _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                        _HCUAccount.NumberVerificationStatus = 0;
                                        _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                        _Random = new Random();
                                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                        _HCUAccount.AccountCode = AccountCode;
                                        _HCUAccount.ReferralCode = _Request.MobileNumber;
                                        _HCUAccount.RegistrationSourceId = RegistrationSource.Website;
                                        if (_Request.UserReference.AppVersionId != 0)
                                        {
                                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                        }
                                        if (_Request.UserReference.AccountId != 0)
                                        {
                                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                        }
                                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                        _HCUAccount.CreateDate = CreateDate;
                                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                                        _HCUAccount.User = _HCUAccountAuth;
                                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                                        _HCoreContext.SaveChanges();
                                        long CustomerId = _HCUAccount.Id;
                                        #endregion
                                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                                        {
                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", CustomerId, null);
                                        }
                                        _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == _Request.UserAccountId
                                            && x.SourceId == TransactionSource.GiftPoints
                                            && x.ModeId == TransactionMode.Credit)
                                            .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.UserAccountId
                                          && x.SourceId == TransactionSource.GiftPoints
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                                        if (_Request.Amount < (_BalanceResponse.Balance + 1))
                                        {
                                            _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _Request.UserAccountId;
                                            _CoreTransactionRequest.CustomerId = CustomerId;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserAccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Gift,
                                                SourceId = TransactionSource.GiftPoints,
                                                Comment = _Request.Comment,

                                                Amount = _Request.Amount,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = CustomerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.Gift,
                                                SourceId = TransactionSource.GiftPoints,
                                                Amount = _Request.Amount,
                                                Comment = _Request.Comment,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                string Message = "";
                                                if (!string.IsNullOrEmpty(_Request.Comment))
                                                {
                                                    Message = _Request.Comment;
                                                }
                                                else
                                                {
                                                    Message = HCoreHelper.GetConfigurationValueByUserAccount("customergiftpointcreditalert", AccountDetails.Id, _Request.UserReference);
                                                }
                                                if (!string.IsNullOrEmpty(Message))
                                                {
                                                    Message = Message.Replace("[AMOUNT]", _Request.Amount.ToString());
                                                    Message = Message.Replace("[DISPLAYNAME]", AccountDetails.DisplayName);
                                                    if (Message.Length > 159)
                                                    {
                                                        Message = Message.Substring(0, 158);
                                                    }
                                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _Request.MobileNumber, Message, CustomerDetails.Id, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                }
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HC0001", "Your account credited to with " + _Request.Amount.ToString() + " gift points");
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", TransactionResponse.Message);
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000", "Gift card wallet balance is low. Credit wallet to giveout rewards");
                                        }
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "This feature is only available for merchants");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account must be active to credit amount");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0001", "Account details not found");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditGiftPointsToCustomerWeb", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
            }
            #endregion
        }
    }
}
