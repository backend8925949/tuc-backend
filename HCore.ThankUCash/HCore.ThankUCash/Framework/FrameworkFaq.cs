//==================================================================================
// FileName: FrameworkFaq.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to Faq
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkFaq
    {
        #region Declare
        HCoreContext _HCoreContext;
        HCCoreParameter _HCCoreParameter;
        #endregion


        public class ResponseCodes
        {
            public const string FAQ100 = "FAQ100";
            public const string FAQ100M = "Title required";
            public const string FAQ101 = "FAQ100";
            public const string FAQ101M = "Description required";
            public const string FAQ102 = "FAQ102";
            public const string FAQ102M = "Status required";
            public const string FAQ103 = "FAQ103";
            public const string FAQ103M = "Invalid status code";
            public const string FAQ104 = "FAQ103";
            public const string FAQ104M = "Account type code required";
            public const string FAQ105 = "FAQ105";
            public const string FAQ105M = "Invalid account type id";
            public const string FAQ106 = "FAQ106";
            public const string FAQ106M = "Title already exists please try with title";
            public const string FAQ107 = "FAQ107";
            public const string FAQ107M = "Reference id required";

            public const string FAQ108 = "FAQ108";
            public const string FAQ108M = "Reference key required";

            public const string FAQ109 = "FAQ109";
            public const string FAQ109M = "Details not found. It might be removed or not available at the moment. Please try after some time";

            public const string FAQ110 = "FAQ110";
            public const string FAQ110M = "Title already exists";

            public const string FAQ111 = "FAQ111";
            public const string FAQ111M = "Operation failed. Faq category contain items. Please delete items and try again";

            public const string FAQ112 = "FAQ112";
            public const string FAQ112M = "Details loaded";

            public const string FAQ113 = "FAQ113";
            public const string FAQ113M = "Title missing";

            public const string FAQ114 = "FAQ114";
            public const string FAQ114M = "Details missing";

            public const string FAQ115 = "FAQ113";
            public const string FAQ115M = "Statuscode missing";

            public const string FAQ116 = "FAQ116";
            public const string FAQ116M = "Faq category key missing";

            public const string FAQ117 = "FAQ116";
            public const string FAQ117M = "Invalid category key";

            public const string FAQ118 = "FAQ118";
            public const string FAQ118M = "Title already exists";

            public const string FAQ119 = "FAQ119";
            public const string FAQ119M = "Category added successfully";

            public const string FAQ120 = "FAQ120";
            public const string FAQ120M = "Category updated successfully";

            public const string FAQ121 = "FAQ121";
            public const string FAQ121M = "New faq added successfully";

            public const string FAQ122 = "FAQ122";
            public const string FAQ122M = "faq updated successfully";

            public const string FAQ123 = "FAQ123";
            public const string FAQ123M = "Category deleted successfuly";

            public const string FAQ124 = "FAQ124";
            public const string FAQ124M = "Faq deleted successfully";

        }

        internal const int FaqCategory = 474;
        internal const int Faq = 475;

        /// <summary>
        /// Description: Saves the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveFaqCategory(OFaqCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ100, ResponseCodes.FAQ100M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ101, ResponseCodes.FAQ101M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ102, ResponseCodes.FAQ102M);
                }
                if (string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ104, ResponseCodes.FAQ104M);
                }
                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                int? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ103, ResponseCodes.FAQ103M);
                }
                if (AccountTypeId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ105, ResponseCodes.FAQ105M);
                }

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == FaqCategory && x.HelperId == AccountTypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                    if (NameCheck != 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ106, ResponseCodes.FAQ106M);
                    }
                    _HCCoreParameter = new HCCoreParameter();
                    _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreParameter.TypeId = FaqCategory;
                    _HCCoreParameter.Name = _Request.Name;
                    _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    _HCCoreParameter.Description = _Request.Description;
                    _HCCoreParameter.Sequence = _Request.Sequence;
                    _HCCoreParameter.HelperId = AccountTypeId;
                    _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreParameter.StatusId = (int)StatusId;
                    _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                    _HCoreContext.SaveChanges();
                    object _Response = new
                    {
                        ReferenceId = _HCCoreParameter.Id,
                        ReferenceKey = _HCCoreParameter.Guid,
                    };
                    long? IconStorageId = null;
                    long? PosterStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                    {
                        PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null || PosterStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _HCCoreParameter.Guid).FirstOrDefault();
                            if (ItemDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    ItemDetails.IconStorageId = IconStorageId;
                                }
                                if (PosterStorageId != null)
                                {
                                    ItemDetails.PosterStorageId = PosterStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ119, ResponseCodes.FAQ119M);
                    #endregion
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveFaqCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateFaqCategory(OFaqCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }

                #region Code Block

                long? StatusId = null;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                }
                if (!string.IsNullOrEmpty(_Request.StatusCode) && StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ103, ResponseCodes.FAQ103M);
                }

                int? AccountTypeId = null;
                if (!string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                }
                if (!string.IsNullOrEmpty(_Request.AccountTypeCode) && AccountTypeId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ105, ResponseCodes.FAQ105M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == FaqCategory && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == FaqCategory && x.HelperId == Details.HelperId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ110, ResponseCodes.FAQ110M);
                                #endregion
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                            Details.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && Details.Description != _Request.Description)
                        {
                            Details.Description = _Request.Description;
                        }
                        if (AccountTypeId != null && Details.HelperId != AccountTypeId)
                        {
                            Details.HelperId = AccountTypeId;
                        }
                        if (_Request.Sequence > 0)
                        {
                            Details.Sequence = _Request.Sequence;
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != null && Details.StatusId != StatusId)
                        {
                            Details.StatusId = (int)StatusId;
                        }
                        long? TIconStorageId = Details.IconStorageId;
                        long? TPosterStorageId = Details.PosterStorageId;
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null)
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null)
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UpdateDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Details.Guid).FirstOrDefault();
                                if (UpdateDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UpdateDetails.IconStorageId = IconStorageId;
                                    }

                                    if (PosterStorageId != null)
                                    {
                                        UpdateDetails.PosterStorageId = PosterStorageId;
                                    }
                                    UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ120, ResponseCodes.FAQ120M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateFaqCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteFaqCategory(OFaqCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == FaqCategory && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool SubItems = _HCoreContext.HCCoreParameter.Any(x => x.TypeId == FaqCategory && x.ParentId == _Request.ReferenceId);
                        if (SubItems)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ111, ResponseCodes.FAQ111M);
                        }
                        _HCoreContext.HCCoreParameter.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ123, ResponseCodes.FAQ123M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqCategory(OFaqCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    OFaqCategory.Details Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == FaqCategory && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OFaqCategory.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            AccountTypeCode = x.Helper.SystemName,
                            AccountTypeName = x.Helper.Name,
                            Name = x.Name,
                            Description = x.Description,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            CreateDate = x.CreateDate,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                            Items = x.InverseParent.Count(),
                            Sequence = x.Sequence,

                            ModifyDate = x.ModifyDate,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        })
                        .FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (Details != null)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.FAQ112, ResponseCodes.FAQ112M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetFaqCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                 where x.TypeId == FaqCategory
                                                 select new OFaqCategory.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     AccountTypeCode = x.Helper.SystemName,
                                                     AccountTypeName = x.Helper.Name,
                                                     Name = x.Name,
                                                     Description = x.Description,
                                                     IconUrl = x.IconStorage.Path,
                                                     PosterUrl = x.PosterStorage.Path,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                     Items = x.InverseParent.Count(),

                                                     Sequence = x.Sequence,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                    }
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OFaqCategory.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                       where x.TypeId == FaqCategory
                                                       select new OFaqCategory.Details
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           AccountTypeCode = x.Helper.SystemName,
                                                           AccountTypeName = x.Helper.Name,
                                                           Name = x.Name,
                                                           Description = x.Description,
                                                           IconUrl = x.IconStorage.Path,
                                                           PosterUrl = x.PosterStorage.Path,

                                                           Items = x.InverseParent.Count(),
                                                           Sequence = x.Sequence,

                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                           ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion


                    }
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetFaqCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }




        /// <summary>
        /// Description: Gets the FAQ category client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqCategoryClient(OFaqCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    OFaqCategory.Details Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == FaqCategory && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OFaqCategory.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            Description = x.Description,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            Sequence = x.Sequence,
                        })
                        .FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (Details != null)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.FAQ112, ResponseCodes.FAQ112M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetFaqCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ category client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqCategoryClient(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Sequence", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    if (_Request.TypeId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                     where x.TypeId == FaqCategory
                                                     && x.StatusId == HelperStatus.Default.Active
                                                     && x.HelperId == _Request.TypeId
                                                     orderby x.Sequence
                                                     select new OFaqCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         Name = x.Name,
                                                         Description = x.Description,
                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,
                                                         Sequence = x.Sequence,
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OFaqCategory.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                           where x.TypeId == FaqCategory
                                                           && x.StatusId == HelperStatus.Default.Active
                                                           && x.HelperId == _Request.TypeId
                                                           orderby x.Sequence
                                                           select new OFaqCategory.Details
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               AccountTypeCode = x.Helper.SystemName,
                                                               AccountTypeName = x.Helper.Name,
                                                               Name = x.Name,
                                                               Description = x.Description,
                                                               IconUrl = x.IconStorage.Path,
                                                               PosterUrl = x.PosterStorage.Path,
                                                               Sequence = x.Sequence,
                                                           })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            //if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            //}
                            //if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            //}
                            #endregion


                        }
                        _HCoreContext.Dispose();
                        #region Create  Response Object
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                     where x.TypeId == FaqCategory && x.StatusId == HelperStatus.Default.Active
                                                     orderby x.Sequence
                                                     select new OFaqCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         AccountTypeCode = x.Helper.SystemName,
                                                         AccountTypeName = x.Helper.Name,
                                                         Name = x.Name,
                                                         Description = x.Description,
                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,
                                                         Sequence = x.Sequence,
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OFaqCategory.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                           where x.TypeId == FaqCategory && x.StatusId == HelperStatus.Default.Active
                                                           orderby x.Sequence
                                                           select new OFaqCategory.Details
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               AccountTypeCode = x.Helper.SystemName,
                                                               AccountTypeName = x.Helper.Name,
                                                               Name = x.Name,
                                                               Description = x.Description,
                                                               IconUrl = x.IconStorage.Path,
                                                               PosterUrl = x.PosterStorage.Path,
                                                               Sequence = x.Sequence,
                                                           })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            //if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            //}
                            //if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            //}
                            #endregion


                        }
                        _HCoreContext.Dispose();
                        #region Create  Response Object
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }


                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetFaqCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveFaq(OFaq.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ113, ResponseCodes.FAQ113M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ114, ResponseCodes.FAQ114M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ115, ResponseCodes.FAQ115M);
                }
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ116, ResponseCodes.FAQ116M);
                }

                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                long? CategoryId = HCoreHelper.GetCoreParameterId(_Request.CategoryKey, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ103, ResponseCodes.FAQ103M);
                }
                if (CategoryId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ117, ResponseCodes.FAQ117M);
                }

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == Faq && x.ParentId == CategoryId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                    if (NameCheck != 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ118, ResponseCodes.FAQ118M);
                    }
                    _HCCoreParameter = new HCCoreParameter();
                    _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreParameter.TypeId = Faq;
                    _HCCoreParameter.ParentId = CategoryId;
                    _HCCoreParameter.Name = _Request.Name;
                    _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    _HCCoreParameter.Description = _Request.Description;
                    _HCCoreParameter.Sequence = _Request.Sequence;
                    _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreParameter.StatusId = (int)StatusId;
                    _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                    _HCoreContext.SaveChanges();
                    object _Response = new
                    {
                        ReferenceId = _HCCoreParameter.Id,
                        ReferenceKey = _HCCoreParameter.Guid,
                    };
                    long? IconStorageId = null;
                    long? PosterStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                    {
                        PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null || PosterStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Id == _HCCoreParameter.Id).FirstOrDefault();
                            if (ItemDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    ItemDetails.IconStorageId = IconStorageId;
                                }
                                if (PosterStorageId != null)
                                {
                                    ItemDetails.PosterStorageId = PosterStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ121, ResponseCodes.FAQ121M);
                    #endregion
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveFaqCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateFaq(OFaq.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }

                #region Code Block
                long? StatusId = null;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                }
                if (!string.IsNullOrEmpty(_Request.StatusCode) && StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ103, ResponseCodes.FAQ103M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == Faq && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                       .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == Faq && x.ParentId == Details.ParentId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ110, ResponseCodes.FAQ110M);
                                #endregion
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                            Details.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && Details.Description != _Request.Description)
                        {
                            Details.Description = _Request.Description;
                        }
                        if (_Request.Sequence > 0)
                        {
                            Details.Sequence = _Request.Sequence;
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != null && Details.StatusId != StatusId)
                        {
                            Details.StatusId = (int)StatusId;
                        }
                        long? TIconStorageId = Details.IconStorageId;
                        long? TPosterStorageId = Details.PosterStorageId;
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null)
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null)
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UpdateDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Details.Guid).FirstOrDefault();
                                if (UpdateDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UpdateDetails.IconStorageId = IconStorageId;
                                    }

                                    if (PosterStorageId != null)
                                    {
                                        UpdateDetails.PosterStorageId = PosterStorageId;
                                    }
                                    UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ122, ResponseCodes.FAQ122M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateFaq", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteFaq(OFaq.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    HCCoreParameter Details = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == Faq && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.HCCoreParameter.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.FAQ124, ResponseCodes.FAQ124M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaq(OFaq.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    OFaq.Details Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == Faq && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OFaq.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            CategoryId = x.ParentId,
                            CategoryKey = x.Parent.Guid,
                            CategoryName = x.Parent.Name,

                            Sequence = x.Sequence,

                            Name = x.Name,
                            Description = x.Description,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            CreateDate = x.CreateDate,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                            ModifyDate = x.ModifyDate,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        })
                        .FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (Details != null)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.FAQ112, ResponseCodes.FAQ112M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetFaq", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaq(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                 where x.TypeId == Faq
                                                 select new OFaq.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     CategoryId = x.ParentId,
                                                     CategoryKey = x.Parent.Guid,
                                                     CategoryName = x.Parent.Name,

                                                     Sequence = x.Sequence,

                                                     Name = x.Name,
                                                     Description = x.Description,
                                                     IconUrl = x.IconStorage.Path,
                                                     PosterUrl = x.PosterStorage.Path,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                    }
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OFaq.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                               where x.TypeId == Faq
                                               select new OFaq.Details
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   CategoryId = x.ParentId,
                                                   CategoryKey = x.Parent.Guid,
                                                   CategoryName = x.Parent.Name,

                                                   Sequence = x.Sequence,

                                                   Name = x.Name,
                                                   Description = x.Description,
                                                   IconUrl = x.IconStorage.Path,
                                                   PosterUrl = x.PosterStorage.Path,
                                                   CreateDate = x.CreateDate,
                                                   CreatedByKey = x.CreatedBy.Guid,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                   CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                   ModifyDate = x.ModifyDate,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                   ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                    }
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetFaq", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqClient(OFaq.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ107, ResponseCodes.FAQ107M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ108, ResponseCodes.FAQ108M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    OFaq.Details Details = _HCoreContext.HCCoreParameter
                        .Where(x => x.TypeId == Faq && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OFaq.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            Description = x.Description,
                            IconUrl = x.IconStorage.Path,
                            PosterUrl = x.PosterStorage.Path,
                            Sequence = x.Sequence,
                        })
                        .FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (Details != null)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, ResponseCodes.FAQ112, ResponseCodes.FAQ112M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.FAQ109, ResponseCodes.FAQ109M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetFaqClient", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the FAQ client.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFaqClient(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Sequence", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    if (_Request.ReferenceId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                     where x.TypeId == Faq && x.ParentId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                                     orderby x.Sequence
                                                     select new OFaq.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         CategoryId = x.ParentId,
                                                         CategoryKey = x.Parent.Guid,
                                                         CategoryName = x.Parent.Name,
                                                         Name = x.Name,
                                                         Description = x.Description,
                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,
                                                         Sequence = x.Sequence,
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OFaq.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                   where x.TypeId == Faq && x.ParentId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                                   orderby x.Sequence
                                                   select new OFaq.Details
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       CategoryId = x.ParentId,
                                                       CategoryKey = x.Parent.Guid,
                                                       CategoryName = x.Parent.Name,
                                                       Name = x.Name,
                                                       Description = x.Description,
                                                       IconUrl = x.IconStorage.Path,
                                                       PosterUrl = x.PosterStorage.Path,
                                                       Sequence = x.Sequence,
                                                   })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #endregion
                        }
                        _HCoreContext.Dispose();
                        #region Create  Response Object
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                                     where x.TypeId == Faq && x.StatusId == HelperStatus.Default.Active
                                                     orderby x.Sequence
                                                     select new OFaq.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         CategoryId = x.ParentId,
                                                         CategoryKey = x.Parent.Guid,
                                                         CategoryName = x.Parent.Name,
                                                         Name = x.Name,
                                                         Description = x.Description,
                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,
                                                         Sequence = x.Sequence,
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OFaq.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                   where x.TypeId == Faq && x.StatusId == HelperStatus.Default.Active
                                                   orderby x.Sequence
                                                   select new OFaq.Details
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       CategoryId = x.ParentId,
                                                       CategoryKey = x.Parent.Guid,
                                                       CategoryName = x.Parent.Name,
                                                       Name = x.Name,
                                                       Description = x.Description,
                                                       IconUrl = x.IconStorage.Path,
                                                       PosterUrl = x.PosterStorage.Path,
                                                       Sequence = x.Sequence,
                                                   })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #endregion

                        }
                        _HCoreContext.Dispose();
                        #region Create  Response Object
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetFaqClient", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
