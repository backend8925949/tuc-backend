//==================================================================================
// FileName: FrameworkLiveTerminals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to live terminals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using HCore.Data.Store;
namespace HCore.ThankUCash.Framework
{
    public class FrameworkLiveTerminals
    {
        List<OHCoreDataStore.Terminal> _Terminals;
        /// <summary>
        /// Description: Updates the terminal transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateTerminalTransaction(OLiveTerminal.ActivityUpdate _Request)
        {
            var Terminal = HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.TerminalId).FirstOrDefault();
            if (Terminal != null)
            {
                HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.TerminalId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001");
                #endregion
            }
            else
            {
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the live terminal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLiveTerminals(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Terminals = new List<OHCoreDataStore.Terminal>();
                if (_Request.Type == "acquirer")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.AcquirerId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "provider")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.ProviderId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "merchant")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.MerchantId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "store")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.StoreId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else if (_Request.Type == "relationshipmanager")
                {
                    _Terminals = HCoreDataStore.Terminals
                       .Where(x => x.RmId == _Request.ReferenceId && x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                else
                {
                    _Terminals = HCoreDataStore.Terminals
                        .Where(x => x.LastTransactionDate != null)
                       .OrderByDescending(x => x.LastTransactionDate)
                       .ToList();
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, _Terminals, 0, 0);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAppUsers", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
