//==================================================================================
// FileName: FrameworkUserOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant onboarding
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.ThankUCash.Object;
using Microsoft.AspNetCore.Http;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkUserOnboarding
    {
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCUAccountOwner _HCUAccountOwner;
        //HCUAccountParameter _HCUAccountParameter;
        HCoreContext _HCoreContext;
        Random _Random;
        List<HCUAccount> _HCUAccounts;
        List<HCUAccountOwner> _HCUAccountOwners;
        List<HCUAccountParameter> _HCUAccountParameters;
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveMerchant(OUserOnboarding.Merchant.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Random = new Random();
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else
                {
                    int AccountTypeId = UserAccountType.Merchant;
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    string AccessPin = _Random.Next(1000, 9999).ToString();
                    string ReferralCode = _Random.Next(100000000, 999999999).ToString();
                    string SecondaryPassword = HCoreHelper.GenerateRandomNumber(10);
                    if (string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _Request.DisplayName = "User";
                    }
                    if (string.IsNullOrEmpty(_Request.ContactMobileNumber))
                    {
                        _Request.ContactMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactMobileNumber);
                    }
                    if (string.IsNullOrEmpty(_Request.UserName))
                    {
                        _Request.UserName = HCoreHelper.GenerateRandomNumber(10);
                    }
                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreHelper.GenerateRandomNumber(8);
                    }
                    long? AccountOperationTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountOperationTypeCode, _Request.UserReference);
                    long? OwnerId = HCoreHelper.GetUserAccountId(_Request.OwnerKey, _Request.UserReference);
                    long? RegionId = HCoreHelper.GetCoreParameterId(_Request.CompanyRegionKey, _Request.UserReference);
                    long? RegionAreaId = HCoreHelper.GetCoreParameterId(_Request.CompanyRegionAreaKey, _Request.UserReference);
                    long? CityId = HCoreHelper.GetCoreParameterId(_Request.CompanyCityKey, _Request.UserReference);
                    long? CityAreaId = HCoreHelper.GetCoreParameterId(_Request.CompanyCityAreaKey, _Request.UserReference);
                    long? PostalCodeId = HCoreHelper.GetCoreParameterId(_Request.CompanyPostalCode, _Request.UserReference);
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountDetails = (from n in _HCoreContext.HCUAccountAuth
                                                   select n).Where(x => x.Username == _Request.UserName).Select(x => x.Id).FirstOrDefault();
                        if (UserAccountDetails == 0)
                        {
                            _HCUAccountOwners = new List<HCUAccountOwner>();
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            if (OwnerId != null && OwnerId != 0)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)OwnerId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            //if (_Request.Configurations != null && _Request.Configurations.Count > 0)
                            //{
                            //    foreach (var Configuration in _Request.Configurations)
                            //    {
                            //        if (!string.IsNullOrEmpty(Configuration.Value) && !string.IsNullOrEmpty(Configuration.SystemName))
                            //        {
                            //            long ConfigurationId = _HCoreContext.HCCoreCommon
                            //                                  .Where(x => x.SystemName == Configuration.SystemName)
                            //                                  .Select(x => x.Id)
                            //                                  .FirstOrDefault();
                            //            if (ConfigurationId != 0)
                            //            {
                            //                _HCUAccountParameter = new HCUAccountParameter();
                            //                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            //                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            //                _HCUAccountParameter.CommonId = ConfigurationId;
                            //                _HCUAccountParameter.Value = Configuration.Value;
                            //                _HCUAccountParameter.StartTime = Configuration.StartTime;
                            //                _HCUAccountParameter.EndTime = Configuration.EndTime;
                            //                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            //                if (_Request.UserReference.AccountId != 0)
                            //                {
                            //                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            //                }
                            //                _HCUAccountParameter.StatusId = HCoreConstant.HelperStatus.Default.Active;
                            //                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                            //                _HCUAccountParameters.Add(_HCUAccountParameter);
                            //            }
                            //        }
                            //    }
                            //}

                            _HCUAccounts = new List<HCUAccount>();
                            _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = _Request.ReferenceKey;
                            _HCUAccount.AccountTypeId = (int)AccountTypeId;
                            if (AccountOperationTypeId != null)
                            {
                                _HCUAccount.AccountOperationTypeId = (int)AccountOperationTypeId;
                            }
                            else
                            {
                                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                            }
                            if (OwnerId != null)
                            {
                                _HCUAccount.OwnerId = OwnerId;
                            }
                            _HCUAccount.DisplayName = _Request.DisplayName;
                            _HCUAccount.Name = _Request.CompanyName;
                            _HCUAccount.FirstName = _Request.ContactFirstName;
                            _HCUAccount.LastName = _Request.ContactLastName;
                            _HCUAccount.MobileNumber = _Request.ContactMobileNumber;
                            _HCUAccount.ContactNumber = _Request.CompanyContactNumber;
                            _HCUAccount.EmailAddress = _Request.CompanyEmailAddress;
                            _HCUAccount.Address = _Request.CompanyAddress;
                            if (_Request.CompanyLatitude != 0)
                            {
                                _HCUAccount.Latitude = _Request.CompanyLatitude;
                            }
                            if (_Request.CompanyLongitude != 0)
                            {
                                _HCUAccount.Longitude = _Request.CompanyLongitude;
                            }
                            _HCUAccount.CountryId = _Request.UserReference.CountryId;
                            if (RegionId != null)
                            {
                                _HCUAccount.StateId = RegionId;
                            }
                            //if (RegionAreaId != null)
                            //{
                            //    _HCUAccount.RegionAreaId = RegionAreaId;
                            //}
                            if (CityId != null)
                            {
                                _HCUAccount.CityId = CityId;
                            }
                            if (CityAreaId != null)
                            {
                                _HCUAccount.CityAreaId = CityAreaId;
                            }
                            _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                            _HCUAccount.EmailVerificationStatus = 0;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.NumberVerificationStatus = 0;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.ReferralCode = ReferralCode;
                            _HCUAccount.Description = _Request.Description;
                            _HCUAccount.CountValue = 0;
                            _HCUAccount.AverageValue = 0;
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                            if (_HCUAccountOwners != null && _HCUAccountOwners.Count > 0)
                            {
                                _HCUAccount.HCUAccountOwnerAccount = _HCUAccountOwners;
                            }
                            if (_HCUAccountParameters != null && _HCUAccountParameters.Count > 0)
                            {
                                _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            }
                            _HCUAccounts.Add(_HCUAccount);

                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = _Request.UserName;
                            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(SecondaryPassword);
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccountAuth.StatusId = HCoreConstant.HelperStatus.Default.Active;
                            _HCUAccountAuth.HCUAccount = _HCUAccounts;
                            _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                            _HCoreContext.SaveChanges();
                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UserAccDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UserAccDetails.IconStorageId = IconStorageId;
                                        }
                                        if (PosterStorageId != null)
                                        {
                                            UserAccDetails.PosterStorageId = PosterStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #region Add Merchant Email Address to Mailerlite
                            MailerliteImple mailerlite = new MailerliteImple();
                            var createEmailObj = new CreateListRequest
                            {
                                name = "New Merchant Email",
                                emails = new string[] { _Request.CompanyEmailAddress}
                            };
                            var jsonResponse = mailerlite.AddEmailToMailerList(createEmailObj);
                            if (jsonResponse != null)
                            {
                                if (jsonResponse.StatusCode.Equals(StatusCodes.Status201Created))
                                
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1097");
                                    #endregion
                                
                            }
                            
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1097");
                                #endregion
                            




                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("OnboardMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
#endregion