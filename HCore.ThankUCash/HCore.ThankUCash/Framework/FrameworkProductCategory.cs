//==================================================================================
// FileName: FrameworkProductCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash.Framework
{
    public class FrameworkProductCategory
    {
        #region Declare
        HCoreContext  _HCoreContext;
        HCCoreParameter _HCCoreParameter;
        #endregion

        public class ResponseCodes
        {
            public const string PC100 = "PC100";
            public const string PC100M = "Title required";
            public const string PC101 = "PC100";
            public const string PC101M = "Description required";
            public const string PC102 = "PC102";
            public const string PC102M = "Status required";
            public const string PC103 = "PC103";
            public const string PC103M = "Invalid status code";
            public const string PC104 = "PC103";
            public const string PC104M = "Account type code required";
            public const string PC105 = "PC105";
            public const string PC105M = "Invalid account type id";
            public const string PC106 = "PC106";
            public const string PC106M = "Title already exists please try with title";
            public const string PC107 = "PC107";
            public const string PC107M = "Reference id required";

            public const string PC108 = "PC108";
            public const string PC108M = "Reference key required";

            public const string PC109 = "PC109";
            public const string PC109M = "Details not found. It might be removed or not available at the moment. Please try after some time";

            public const string PC110 = "PC110";
            public const string PC110M = "Title already exists";

            public const string PC111 = "PC111";
            public const string PC111M = "Operation failed. PC category contain items. Please delete items and try again";

            public const string PC112 = "PC112";
            public const string PC112M = "Details loaded";

            public const string PC113 = "PC113";
            public const string PC113M = "Title missing";

            public const string PC114 = "PC114";
            public const string PC114M = "Details missing";

            public const string PC115 = "PC113";
            public const string PC115M = "Statuscode missing";

            public const string PC116 = "PC116";
            public const string PC116M = "Product category key missing";

            public const string PC117 = "PC116";
            public const string PC117M = "Invalid category key";

            public const string PC118 = "PC118";
            public const string PC118M = "Title already exists";

            public const string PC119 = "PC119";
            public const string PC119M = "Category added successfully";

            public const string PC120 = "PC120";
            public const string PC120M = "Category updated successfully";

            public const string PC121 = "PC121";
            public const string PC121M = "New Product added successfully";

            public const string PC122 = "PC122";
            public const string PC122M = "Product updated successfully";

            public const string PC123 = "PC123";
            public const string PC123M = "Category deleted successfuly";

            public const string PC124 = "PC124";
            public const string PC124M = "Product deleted successfully";

        }

        internal const int PCCategory = 474;
        internal const int PC = 475;
        internal OResponse SavePCCategory(OProductCategory.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC100, ResponseCodes.PC100M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC101, ResponseCodes.PC101M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC102, ResponseCodes.PC102M);
                }
                if (string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC104, ResponseCodes.PC104M);
                }

                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                int? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC103, ResponseCodes.PC103M);
                }
                if (AccountTypeId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC105, ResponseCodes.PC105M);
                }

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == PCCategory && x.HelperId == AccountTypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                    if (NameCheck != 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCodes.PC106, ResponseCodes.PC106M);
                    }
                    _HCCoreParameter = new HCCoreParameter();
                    _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreParameter.TypeId = PCCategory;
                    _HCCoreParameter.Name = _Request.Name;
                    _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    _HCCoreParameter.Description = _Request.Description;
                    _HCCoreParameter.Sequence = _Request.Sequence;
                    _HCCoreParameter.HelperId = AccountTypeId;
                    _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreParameter.StatusId = (int)StatusId;
                    _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                    _HCoreContext.SaveChanges();
                    object _Response = new
                    {
                        ReferenceId = _HCCoreParameter.Id,
                        ReferenceKey = _HCCoreParameter.Guid,
                    };
                    long? IconStorageId = null;
                    long? PosterStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                    {
                        PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null || PosterStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ItemDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _HCCoreParameter.Guid).FirstOrDefault();
                            if (ItemDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    ItemDetails.IconStorageId = IconStorageId;
                                }
                                if (PosterStorageId != null)
                                {
                                    ItemDetails.PosterStorageId = PosterStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCodes.PC119, ResponseCodes.PC119M);
                    #endregion
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SavePCCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
