//==================================================================================
// FileName: ManageCashier.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageCashier
    {
        FrameworkCashierManager _FrameworkCashierManager;
        /// <summary>
        /// Description: Generates the cashier identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GenerateCashierId(OList.Request _Request)
        {
            _FrameworkCashierManager = new FrameworkCashierManager();
            return _FrameworkCashierManager.GenerateCashierId(_Request);
        }
    }
}
