//==================================================================================
// FileName: ManageTucPay.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.FrameworkTucPay;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageTucPay
    {
        FrameworkBuyPoints _FrameworkBuyPoints;
        /// <summary>
        /// Buypoint initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPoint_Initialize(OTucPay.BuyPoint.Initilize.Request _Request)
        {
            _FrameworkBuyPoints = new FrameworkBuyPoints();
            return _FrameworkBuyPoints.BuyPoint_Initialize(_Request);
        }
        /// <summary>
        /// Buypoint confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPoint_Confirm(OTucPay.BuyPoint.Confirm.Request _Request)
        {
            _FrameworkBuyPoints = new FrameworkBuyPoints();
            return _FrameworkBuyPoints.BuyPoint_Confirm(_Request);
        }
        /// <summary>
        /// Buypoint cancel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPoint_Cancel(OTucPay.BuyPoint.Confirm.Request _Request)
        {
            _FrameworkBuyPoints = new FrameworkBuyPoints();
            return _FrameworkBuyPoints.BuyPoint_Cancel(_Request);
        }
    }
}
