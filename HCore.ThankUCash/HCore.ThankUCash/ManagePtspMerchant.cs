//==================================================================================
// FileName: ManagePtspMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
//using HCore.ThankUCash.FrameworkPtsp;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
   public class ManagePtspMerchant
    {
        //FrameworkPtspMerchantManager _FrameworkPtspMerchantManager;

        //public OResponse GetAccountOverview(OPtspManager.Analytics.Request _Request)
        //{
        //    _FrameworkPtspMerchantManager = new FrameworkPtspMerchantManager();
        //    return _FrameworkPtspMerchantManager.GetAccountOverview(_Request);
        //}
        //public OResponse GetMerchant(OList.Request _Request)
        //{
        //    _FrameworkPtspMerchantManager = new FrameworkPtspMerchantManager();
        //    return _FrameworkPtspMerchantManager.GetMerchant(_Request);
        //}
        //public OResponse GetStore(OList.Request _Request)
        //{
        //    _FrameworkPtspMerchantManager = new FrameworkPtspMerchantManager();
        //    return _FrameworkPtspMerchantManager.GetStore(_Request);
        //}

        //public OResponse GetTerminal(OList.Request _Request)
        //{
        //    _FrameworkPtspMerchantManager = new FrameworkPtspMerchantManager();
        //    return _FrameworkPtspMerchantManager.GetTerminal(_Request);
        //}
    }
}
