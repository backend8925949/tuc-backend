//==================================================================================
// FileName: ManageTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.ThankUCash.Framework;
using HCore.Helper;
namespace HCore.ThankUCash
{
    public class ManageTransactions
    {
        FrameworkTransactions _FrameworkTransactions;

        /// <summary>
        /// Description: Gets the sale transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSaleTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetSaleTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the sale transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSaleTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetSaleTransactionOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the reward transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardTransactionOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the pending reward transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingRewardTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetPendingRewardTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the pending reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingRewardTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetPendingRewardTransactionOverview(_Request);
        }



        /// <summary>
        /// Description: Gets the reward claim transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardClaimTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardClaimTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the reward claim transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardClaimTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardClaimTransactionOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the redeem transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRedeemTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRedeemTransactionOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the campaign transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaignTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetCampaignTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the campaign transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaignTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetCampaignTransactionOverview(_Request);
        }






        /// <summary>
        /// Description: Gets the transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactions(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetTransactions(_Request);
        }

        /// <summary>
        /// Description: Gets the transactions overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactionsOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetTransactionsOverview(_Request);
        }


        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransaction(OReference _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetTransaction(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer merchant sales.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerMerchantSales(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerMerchantSales(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerRewardTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerRewardTransactionOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerRedeemTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerRedeemTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer redeem transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerRedeemTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerRedeemTransaction(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer  transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer  transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Cancels the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CancelTransaction(OReference _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.CancelTransaction(_Request);
        }

        /// <summary>
        /// Description: Gets the wallet history transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetWalletHistoryOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetWalletHistoryOverview(_Request);
        }

    }
}
