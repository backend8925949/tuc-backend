//==================================================================================
// FileName: ManagePayments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.ThankUCash.FrameworkPayments;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.ThankUCash
{
    public class ManagePayments
    {
        FrameworkPaymentsManager _FrameworkPaymentsManager;

        /// <summary>
        /// Description: Gets the bill payments.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBillPayments(OList.Request _Request)
        {
            _FrameworkPaymentsManager = new FrameworkPaymentsManager();
            return _FrameworkPaymentsManager.GetBillPayments(_Request);
        }
    }
}
