//==================================================================================
// FileName: ThankUCashConstant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.Core
{
    public static class ThankUCashConstant
    {

        public const int ThankUAccountId = 3;
        public class ListType
        {
            public const string All = "all";
            public const string ThankUCash = "thankucash";
            public const string Merchant = "merchant";
            public const string App = "appdownloads";
            public const string Owner = "owner";
            public const string SubOwner = "subowner";
            public const string CreatedBy = "createdby";
            public const string Bank = "bank";
            public const string Provider = "provider";
            


            public const string Today = "today";
            public const string CreatedByOwner = "createdbyowner";
            public const string Acquirer = "acquirer";
        }

        public class TULeadStatus
        {
            public const int New = 214;
            public const int Calling = 215;
            public const int CallFailed = 216;
            public const int CallLater = 217;
            public const int NotInterested = 218;
            public const int Closed = 219;
            public const int InvalidNumber = 220;
        }
        public class TUHelpers
        {
            public const int Product = 237;
            public const int ProductBatch = 238;
            public const int ProductBarCode = 239;
            public const int ProductRewardTypeAmount = 241;
            public const int ProductRewardTypePercentage = 242;
        }
        public class TUHelperType
        {
            public const int ProductRewardType = 240;
            public const int MerchantCategories = 205;
            public const int TransactionTypeValueReward = 207;
            public const int TransactionTypeValueRedeem = 208;
            public const int TransactionTypeValueSettlement = 209;
            public const int TransactionTypeValuePendingSettlement = 210;
            public const int TransactionTypeValuePointTransfer = 211;
            public const int TransactionTypeValuePayment = 212;
        }

        //public class TUTransactionType
        //{
        //    public const int CashPayment = 147;
        //    public const int CardReward = 148;
        //    public const int RewardCommission = 204;
        //    public const int OnlineGiftPoints = 149;
        //    public const int MerchantCredit = 150;
        //    public const int StoreCredit = 151;
        //    public const int CashierCredit = 152; //
        //    public const int TUCardReward = 153;  // Updated To TUCardReward from CardReward
        //    public const int CardChange = 154; // Correct
        //    public const int CashReward = 155; // Correct
        //    public const int CardRedeem = 156; // Correct
        //    public const int PendingSettlement = 157;
        //    public const int PointsSettlement = 158;
        //    public const int AtmCardPayment = 159;
        //    public const int BankSettlement = 160;
        //    public const int BankPayment = 161;
        //    public const int CreditCardPayment = 162;
        //    public const int OnlinePayment = 163;
        //    public const int OnlineRedeem = 164;
        //    public const int InvoiceRewards = 165;
        //    public const int ReferralBonus = 202;
        //    public const int QRCodeReward = 243;
        //}
        //public class TUTransactionTypeCategory
        //{
        //    public const int Reward = 207;
        //    public const int Redeem = 208;
        //    public const int Settlement = 209;
        //    public const int PendingSettlement = 210;
        //    public const int PointTransfer = 211;
        //    public const int Payments = 212;
        //}
        //public class TUTransactionSource
        //{
        //    public const int Merchant = 166;
        //    public const int App = 167;
        //    public const int Card = 168;
        //    public const int Store = 169;
        //    public const int Cashier = 170;
        //    public const int Settlement = 171;
        //    public const int Payments = 172;
        //    public const int ThankUCashPlus = 244;
        //}
        //public class TUTransactionMode
        //{
        //    public const int Credit = 145;
        //    public const int Debit = 146;
        //}

        public class TUStatusHelper
        {
            public class Card
            {
                public const int NotAssigned = 22;
                public const int Assigned = 23;
                public const int Blocked = 24;
                public const int Lost = 25;
                public const int Damaged = 26;
            }
            public class Product
            {
                public const int Inactive = 225;
                public const int Active = 226;
                public const int Expired = 227;
                public const int Archived = 228;
            }
            public class ProductBatch
            {
                public const int Creating = 229;
                public const int Active = 230;
                public const int Inactive = 231;
                public const int Expired = 232;
            }
            public class ProductCode
            {
                public const int Unused = 233;
                public const int Used = 234;
                public const int Expired = 235;
                public const int Disabled = 236;
            }
            public class Transaction
            {
                public const int Processing = 27;
                public const int Success = 28;
                public const int Pending = 29;
                public const int Failed = 30;
                public const int Cancelled = 31;
                public const int Error = 32;
            }
        }
    }
}
