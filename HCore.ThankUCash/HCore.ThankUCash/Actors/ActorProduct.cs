//==================================================================================
// FileName: ActorProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.ThankUCash.Core.ThankUCashConstant;

namespace HCore.ThankUCash.Actors
{
    internal class ActorProductBatchCodeProcess : ReceiveActor
    {
        public ActorProductBatchCodeProcess()
        {
            Receive<OProduct.ProductCodeProcess>(_Request =>
            {
                using (HCoreContext _HCoreContentCore = new HCoreContext())
                {
                    var BatchCodes = _HCoreContentCore.TUProduct
                               .Where(x => x.Id == _Request.BatchId)
                               .Select(x => new
                               {
                                   Quantity = x.Quantity,
                                   SavedItems = x.TUProductCode.Count(),
                               }).FirstOrDefault();
                    _HCoreContentCore.Dispose();
                    if (BatchCodes != null && BatchCodes.SavedItems < (BatchCodes.Quantity + 1))
                    {
                        long RemainingQuantity = (long)BatchCodes.Quantity - BatchCodes.SavedItems;
                        if (RemainingQuantity < 21)
                        {
                            try
                            {
                                using (HCoreContext _HCoreContent = new HCoreContext())
                                {
                                    for (int i = 0; i < RemainingQuantity; i++)
                                    {
                                        Random _Random = new Random();
                                        string ItemCode = _Random.Next(100000000, 999999999).ToString() + "" + _Random.Next(100000000, 999999999).ToString();
                                        TUProductCode _TUProductCode = new TUProductCode();
                                        _TUProductCode.Guid = HCoreHelper.GenerateGuid();
                                        _TUProductCode.TypeId = _Request.TypeId;
                                        _TUProductCode.BatchId = _Request.BatchId;
                                        _TUProductCode.ItemCode = ItemCode;
                                        _TUProductCode.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _TUProductCode.CreatedById = _Request.UserReference.AccountId;
                                        _TUProductCode.StatusId = TUStatusHelper.ProductCode.Unused;
                                        _HCoreContent.TUProductCode.Add(_TUProductCode);
                                    }
                                    _HCoreContent.SaveChanges();
                                    _HCoreContent.Dispose();
                                }
                            }
                            catch (Exception _Exception)
                            {
                                HCoreHelper.LogException("ActorProductBatchCodeProcess", _Exception);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < RemainingQuantity; i++)
                            {
                                using (HCoreContext _HCoreContent = new HCoreContext())
                                {
                                    var BatchDetails = _HCoreContent.TUProduct
                                    .Where(x => x.Id == _Request.BatchId)
                                    .Select(x => new
                                    {
                                        Quantity = x.Quantity,
                                        SavedItems = x.TUProductCode.Count(),
                                    }).FirstOrDefault();
                                    if (BatchDetails != null && BatchDetails.SavedItems < (BatchDetails.Quantity + 1))
                                    {
                                        Random _Random = new Random();
                                        string ItemCode = _Random.Next(100000000, 999999999).ToString() + "" + _Random.Next(100000000, 999999999).ToString();
                                        TUProductCode _TUProductCode = new TUProductCode();
                                        _TUProductCode.Guid = HCoreHelper.GenerateGuid();
                                        _TUProductCode.TypeId = _Request.TypeId;
                                        _TUProductCode.BatchId = _Request.BatchId;
                                        _TUProductCode.ItemCode = ItemCode;
                                        _TUProductCode.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _TUProductCode.CreatedById = _Request.UserReference.AccountId;
                                        _TUProductCode.StatusId = TUStatusHelper.ProductCode.Unused;
                                        _HCoreContent.TUProductCode.Add(_TUProductCode);
                                        _HCoreContent.SaveChanges();
                                        _HCoreContent.Dispose();
                                    }
                                }
                            }
                        }
                    }
                    using (HCoreContext _HCoreContentBatchUpdate = new HCoreContext())
                    {
                        var BatchCheck = _HCoreContentBatchUpdate.TUProduct
                                   .Where(x => x.Id == _Request.BatchId)
                                   .Select(x => new
                                   {
                                       Quantity = x.Quantity,
                                       SavedItems = x.TUProductCode.Count(),
                                   }).FirstOrDefault();
                        if (BatchCheck.Quantity == BatchCheck.SavedItems)
                        {
                            #region Update Batch
                            var BatchInfo = _HCoreContentBatchUpdate.TUProduct.Where(x => x.Id == _Request.BatchId).FirstOrDefault();
                            if (BatchInfo != null)
                            {
                                BatchInfo.StatusId = TUStatusHelper.ProductBatch.Active;
                                BatchInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                BatchInfo.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContentBatchUpdate.SaveChanges();
                            }
                            #endregion
                        }
                    }
                }
            });
        }
    }
}
