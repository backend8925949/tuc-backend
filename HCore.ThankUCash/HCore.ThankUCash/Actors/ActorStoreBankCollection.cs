//==================================================================================
// FileName: ActorStoreBankCollection.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Akka.Actor;
using HCore.ThankUCash.Framework;
namespace HCore.ThankUCash.Actors
{
    internal class ActorStoreBankCollection : ReceiveActor
    {
        public ActorStoreBankCollection()
        {
            Receive<string>(_Request =>
            {
            FrameworkAnalytics _FrameworkAnalytics = new FrameworkAnalytics();
                _FrameworkAnalytics.SyncStoreBankCollections();
            });
        }
    }
}
