//==================================================================================
// FileName: TransactionPostProcessManagerActor.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using Akka.Actor;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using Newtonsoft.Json;
using HCore.ThankUCash.Object;
using static HCore.ThankUCash.Core.ThankUCashConstant;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Operations.Object;
using HCore.Data;
using HCore.Data.Models;

namespace HCore.ThankUCash.Actors
{
    public class TransactionPostProcessManagerActor : ReceiveActor
    {
        public TransactionPostProcessManagerActor()
        {
            Receive<OCoreTransaction.Request>(_Request =>
            {
                if (_Request.CreatedByAccountTypeId == UserAccountType.Terminal)
                {
                    if (_Request.TerminalId > 0)
                    {
                        var CreatedBy = HCore.Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.TerminalId).FirstOrDefault();
                        if (CreatedBy != null)
                        {
                            HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.CreatedById).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                            HCore.Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.CreatedById).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                    var ParentIdD = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault();
                    if (ParentIdD != null)
                    {
                        HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        HCore.Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                    }
                    var SubParentIdD = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.SubParentId).FirstOrDefault();
                    if (SubParentIdD != null)
                    {
                        HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.SubParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                    }
                    var BankIdD = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.BankId).FirstOrDefault();
                    if (BankIdD != null)
                    {
                        HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.BankId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                    }
                    var ProviderIdD = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ProviderId).FirstOrDefault();
                    if (ProviderIdD != null)
                    {
                        HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ProviderId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                    }

                    foreach (var TrItem in _Request.Transactions)
                    {
                        var AccDetails = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == TrItem.UserAccountId).FirstOrDefault();
                        if (AccDetails != null)
                        {
                            HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == TrItem.UserAccountId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                }
            });
        }
    }
}
