//==================================================================================
// FileName: ManageCashierApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageCashierApp
    {
        FrameworkCashierApp _FrameworkCashierApp;
        /// <summary>
        /// Description: Gets the cashier access.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashierAccess(OCashierApp.Request _Request)
        {
            _FrameworkCashierApp = new FrameworkCashierApp();
            return _FrameworkCashierApp.GetCashierAccess(_Request);
        }

    }
}
