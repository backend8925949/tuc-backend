//==================================================================================
// FileName: ManageOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using Akka.Actor;
using HCore.Helper;
using HCore.ThankUCash.Actors;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;
using HCore.ThankUCash.Analytics;

namespace HCore.ThankUCash
{   
    public class ManageOverview
    {
        /// <summary>
        /// Creates the reward settlement invoices.
        /// </summary>
        public void CreateRewardSettlementInvoices()
        {
            #region TransactionPostProcess
            var _Actor = ActorSystem.Create("ActorStoreBankCollection");
            var _ActorNotify = _Actor.ActorOf<ActorStoreBankCollection>("ActorStoreBankCollection");
            _ActorNotify.Tell("callssytem");
            #endregion
        }

        AnalyticsOverview _AnalyticsOverview;
        FrameworkAnalytics _FrameworkAnalytics;
        FrameworkOverview _FrameworkOverview;
        /// <summary>
        /// Description: Gets the store bank collection.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreBankCollection(OStoreBankCollection.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetStoreBankCollection(_Request);
        }
        /// <summary>
        /// Description: Updates the received amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReceivedAmount(OStoreBankCollection.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.UpdateReceivedAmount(_Request);
        }


        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountOverview(OOverview.Request _Request)
        {
            _AnalyticsOverview = new AnalyticsOverview();
            return _AnalyticsOverview.GetAccountOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the account overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountOverviewLite(OOverview.Request _Request)
        {
           _AnalyticsOverview = new AnalyticsOverview();
            return _AnalyticsOverview.GetAccountOverviewLite(_Request);
        }

        /// <summary>
        /// Description: Gets the reward overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetRewardOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the reward type overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardTypeOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetRewardTypeOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the redeem overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetRedeemOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the application users overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUsersOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetAppUsersOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the position terminals status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPosTerminalsStatusOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetPosTerminalsStatusOverview(_Request);
        }
        //public OResponse GetPosTerminalsOverviewList(OList.Request _Request)
        //{
        //    _FrameworkOverview = new FrameworkOverview();
        //    return _FrameworkOverview.GetPosTerminalsOverviewList(_Request);
        //}






        /// <summary>
        /// Description: Gets the account balance overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalanceOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetAccountBalanceOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the application overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetAppUserOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant mini overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantMiniOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetMerchantMiniOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetStoreOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetMerchantOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantOverviewLite(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetMerchantOverviewLite(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerOverviewLite(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetAcquirerOverviewLite(_Request);
        }

        /// <summary>
        /// Description: Gets the system overview lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemOverviewLite(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetSystemOverviewLite(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant application user overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantAppUserOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetMerchantAppUserOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOverview(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the visit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVisitHistory(OOverview.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetVisitHistory(_Request);
        }
        //public OResponse GetMerchantStoreSaleOverview(OOverview.Request _Request)
        //{
        //    _FrameworkOverview = new FrameworkOverview();
        //    return _FrameworkOverview.GetMerchantStoreSaleOverview(_Request);
        //}

    }
}
