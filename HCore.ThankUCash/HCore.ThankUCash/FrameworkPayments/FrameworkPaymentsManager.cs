//==================================================================================
// FileName: FrameworkPaymentsManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to payments
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using RestSharp;
using HCore.Operations.Object;
using HCore.Operations;
using System.Security.Cryptography;

namespace HCore.ThankUCash.FrameworkPayments
{
    internal class FrameworkPaymentsManager
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the bill payments.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBillPayments(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.VasPayment
                                             select new OPayments.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 AccountId = x.AccountId,
                                                 AccountKey = x.Account.Guid,
                                                 AccountDisplayName = x.Account.DisplayName,
                                                 AccountMobileNumber = x.Account.MobileNumber,

                                                 ParentId = x.Account.Owner.OwnerId,
                                                 ParentKey = x.Account.Owner.Owner.Guid,
                                                 ParentName = x.Account.Owner.Owner.Name,
                                                 ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                 ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                 SubParentId = x.Account.OwnerId,
                                                 SubParentKey = x.Account.Owner.Guid,
                                                 SubParentName = x.Account.Owner.Owner.Name,
                                                 SubParentDisplayName = x.Account.Owner.DisplayName,
                                                 SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                 CashierId = x.Account.SubOwnerId,
                                                 CashierKey = x.Account.SubOwner.Guid,
                                                 CashierName = x.Account.Owner.Owner.Name,
                                                 CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                 CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                 TypeId = x.TypeId,
                                                 TypeCode = x.Type.SystemName,
                                                 TypeName = x.Type.Name,    

                                                 Amount = x.Amount,
                                                 RewardAmount = x.RewardAmount,
                                                 BillerName = x.BillerName,
                                                 PackageName = x.PackageName,
                                                 PaymentReference = x.PaymentReference,
                                                 PaymentSource = x.PaymentSource,

                                                 AccountNumber = x.AccountNumber,

                                                 CreateDate = x.CreateDate,
                                                 CreatedById = x.CreatedById,
                                                 CreatedByKey = x.CreatedBy.Guid,
                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                 ModifyDate = x.ModifyDate,
                                                 ModifyById = x.ModifyById,
                                                 ModifyByKey = x.ModifyBy.Guid,
                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<OPayments.List> Data = (from x in _HCoreContext.VasPayment
                                                 select new OPayments.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.AccountId,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountMobileNumber = x.Account.MobileNumber,

                                                     ParentId = x.Account.Owner.OwnerId,
                                                     ParentKey = x.Account.Owner.Owner.Guid,
                                                     ParentName = x.Account.Owner.Owner.Name,
                                                     ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                     ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                     SubParentId = x.Account.OwnerId,
                                                     SubParentKey = x.Account.Owner.Guid,
                                                     SubParentName = x.Account.Owner.Owner.Name,
                                                     SubParentDisplayName = x.Account.Owner.DisplayName,
                                                     SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                     CashierId = x.Account.SubOwnerId,
                                                     CashierKey = x.Account.SubOwner.Guid,
                                                     CashierName = x.Account.Owner.Owner.Name,
                                                     CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                     CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                     TypeId = x.TypeId,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,

                                                     Amount = x.Amount,
                                                     RewardAmount = x.RewardAmount,
                                                     BillerName = x.BillerName,
                                                     PackageName = x.PackageName,
                                                     PaymentReference = x.PaymentReference,
                                                     PaymentSource = x.PaymentSource,

                                                     AccountNumber = x.AccountNumber,


                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                       
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetCustomerOrder", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
