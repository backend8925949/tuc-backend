//==================================================================================
// FileName: FrameworkPayments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to payments
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using RestSharp;
using HCore.Operations.Object;
using HCore.Operations;
using System.Security.Cryptography;

namespace HCore.ThankUCash.FrameworkPayments
{
    public class FrameworkPayments
    {
        public class PaymentItem
        {
            public string? Name { get; set; }
            public string? Value { get; set; }
        }
        public List<PaymentItem> _PaymentItems;
        OCoreTransaction.Response _TransactionResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        OApp.OPaymentsConfiguration _OPaymentsConfiguration;
        HCoreContext _HCoreContext;
        List<OApp.OBiller> _Billers;
        VasPayment _VasPayment;
        internal const int UserCard = 465;
        internal class TransactionTypeReward
        {
            internal const int TransactionTypePointPurchase = 567;
            internal const int BillPayment = 568;
            internal const int LCCTopup = 569;
        }
        internal class TransactionTypeRedeem
        {
            internal const int BillPayment = 479;
            internal const int LccTopup = 480;
        }
        internal const int TransactionSourcePayment = 471;
        private const int PaymentTypePointPurchase = 564;
        private const int PaymentTypeBillPayment = 565;
        private const int PaymentTypeLccTopup = 566;
        HCUAccountParameter _HCUAccountParameter;
        /// <summary>
        /// Gets the billers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBillers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Billers = new List<OApp.OBiller>();
                if (_Request.Type == "electricity")
                {
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 73,
                        name = "Enugu",
                        slug = "ENUGU_DISCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 1,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 73,
                        name = "Eko",
                        slug = "EKO_DISCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.5,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 74,
                        name = "Abuja",
                        slug = "ABUJA_DISCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.5,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 75,
                        name = "PortHarcourt",
                        slug = "PORTHARCOURT_DISCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.5,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 77,
                        name = "IKEDC",
                        slug = "IKEDC",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.5,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 78,
                        name = "IBEDC",
                        slug = "IBEDC",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.5,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 79,
                        name = "KEDCO",
                        slug = "KEDCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0.25,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 83,
                        name = "KAEDCO",
                        slug = "KAEDCO",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 84,
                        name = "JEDC",
                        slug = "JEDC",
                        groupId = 1,
                        amount = 0,
                        rewardPercentage = 0,

                    });
                }
                if (_Request.Type == "tv")
                {
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 2,
                        name = "Dstv",
                        slug = "DSTV",
                        groupId = 2,
                        amount = 0,
                        rewardPercentage = 0.75,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 3,
                        name = "GOtv",
                        slug = "GOTV",
                        groupId = 2,
                        amount = 0,
                        rewardPercentage = 0.75,

                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 80,
                        name = "Startimes",
                        slug = "STARTIMES",
                        groupId = 2,
                        amount = 0,
                        rewardPercentage = 0.5,
                    });
                }
                if (_Request.Type == "airtime")
                {
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 81,
                        name = "Smile",
                        slug = "SMILE",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 0.75,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 82,
                        name = "Spectranet",
                        slug = "SPECTRANET",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 1,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 87,
                        name = "Mtn",
                        slug = "MTN_NIGERIA",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 1.25,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 88,
                        name = "Airtel",
                        slug = "AIRTEL_NIGERIA",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 1.25,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 89,
                        name = "Glo",
                        slug = "GLO_NIGERIA",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 2,
                    });
                    _Billers.Add(new OApp.OBiller
                    {
                        id = 90,
                        name = "9 Mobile",
                        slug = "9MOBILE_NIGERIA",
                        groupId = 3,
                        amount = 0,
                        rewardPercentage = 2,
                    });
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    //string username = "smashlabs";
                    //string password = "$m@$hl@b$1234";
                    //string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    //string Type = "disco";
                    string Url = "http://204.8.207.124:8080/coralpay-vas/api/billers/group/slug/";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        Url = "https://vas.coralpay.com/vas-service/api/billers/group/slug/";
                        if (_Request.Type == "electricity")
                        {
                            _Request.Type = "ELECTRIC_DISCO";
                        }
                        if (_Request.Type == "tv")
                        {
                            _Request.Type = "PAY_TV";
                        }

                        if (_Request.Type == "airtime")
                        {
                            _Request.Type = "AIRTIME_AND_DATA";
                        }
                    }
                    else
                    {
                        if (_Request.Type == "electricity")
                        {
                            _Request.Type = "DISCO";
                        }
                        if (_Request.Type == "tv")
                        {
                            _Request.Type = "PAID_TV";
                        }
                        if (_Request.Type == "airtime")
                        {
                            _Request.Type = "AIRTIME_AND_DATA";
                        }
                    }
                    // HCoreHelper.LogData(HCoreConstant.LogType.Log, "BLABLA", HostName, HostEnvironment.ToString(), _Request.Type);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + "" + _Request.Type);
                    request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        string username = "thankucash";
                        string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        request.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    else
                    {
                        string username = "smashlabs";
                        string password = "$m@$hl@b$1234";
                        //string username = "thankucash";
                        //string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        request.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string _readerRespose = reader.ReadToEnd();
                        //HCoreHelper.LogData(HCoreConstant.LogType.Log, "GetBillers", Url + "" + _Request.Type, _readerRespose, "");
                        OApp.OBillersResponse _RequestBodyContent = JsonConvert.DeserializeObject<OApp.OBillersResponse>(_readerRespose);
                        if (_RequestBodyContent != null && _RequestBodyContent.status == "success" && _RequestBodyContent.responseData != null && _RequestBodyContent.responseData.Count > 0)
                        {

                            foreach (var tItem in _RequestBodyContent.responseData)
                            {
                                var BItem = _Billers.Where(x => x.slug == tItem.slug).FirstOrDefault();
                                if (BItem != null)
                                {
                                    tItem.rewardPercentage = BItem.rewardPercentage;
                                }
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _RequestBodyContent.responseData, PaymentResource.PR2128, PaymentResource.PR2128M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2127, PaymentResource.PR2127M);
                            #endregion
                        }
                    }

                    //_Billers = new List<OApp.OBiller>();
                    //if (_Request.Type == "electricity")
                    //{
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 73,
                    //        name = "Enugu",
                    //        slug = "ENUGU_DISCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 1,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 73,
                    //        name = "Eko",
                    //        slug = "EKO_DISCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 74,
                    //        name = "Abuja",
                    //        slug = "ABUJA_DISCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 75,
                    //        name = "PortHarcourt",
                    //        slug = "PORTHARCOURT_DISCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 77,
                    //        name = "IKEDC",
                    //        slug = "IKEDC",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 78,
                    //        name = "IBEDC",
                    //        slug = "IBEDC",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 79,
                    //        name = "KEDCO",
                    //        slug = "KEDCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0.25,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 83,
                    //        name = "KAEDCO",
                    //        slug = "KAEDCO",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 84,
                    //        name = "JEDC",
                    //        slug = "JEDC",
                    //        groupId = 1,
                    //        amount = 0,
                    //        rewardPercentage = 0,

                    //    });
                    //}
                    //if (_Request.Type == "tv")
                    //{
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 2,
                    //        name = "Dstv",
                    //        slug = "DSTV",
                    //        groupId = 2,
                    //        amount = 0,
                    //        rewardPercentage = 0.75,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 3,
                    //        name = "GOtv",
                    //        slug = "GOTV",
                    //        groupId = 2,
                    //        amount = 0,
                    //        rewardPercentage = 0.75,

                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 80,
                    //        name = "Startimes",
                    //        slug = "STARTIMES",
                    //        groupId = 2,
                    //        amount = 0,
                    //        rewardPercentage = 0.5,
                    //    });
                    //}
                    //if (_Request.Type == "airtime")
                    //{
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 81,
                    //        name = "Smile",
                    //        slug = "SMILE",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 0.75,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 82,
                    //        name = "Spectranet",
                    //        slug = "SPECTRANET",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 1,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 87,
                    //        name = "Mtn",
                    //        slug = "MTN_NIGERIA",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 1.25,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 88,
                    //        name = "Airtel",
                    //        slug = "AIRTEL_NIGERIA",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 1.25,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 89,
                    //        name = "Glo",
                    //        slug = "GLO_NIGERIA",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 2,
                    //    });
                    //    _Billers.Add(new OApp.OBiller
                    //    {
                    //        id = 90,
                    //        name = "9 Mobile",
                    //        slug = "9MOBILE_NIGERIA",
                    //        groupId = 3,
                    //        amount = 0,
                    //        rewardPercentage = 2,
                    //    });
                    //}
                    //#region Send Response
                    //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Billers, PaymentResource.PR2128, PaymentResource.PR2128M);
                    //#endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetBillers", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the biller packages.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBillerPackages(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    string Url = "http://204.8.207.124:8080/coralpay-vas/api/";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        Url = "https://vas.coralpay.com/vas-service/api/";
                    }
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + "packages/biller/slug/" + _Request.Type);
                    request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        string username = "thankucash";
                        string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        request.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    else
                    {
                        string username = "smashlabs";
                        string password = "$m@$hl@b$1234";
                        //string username = "thankucash";
                        //string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        request.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string _readerRespose = reader.ReadToEnd();
                        // HCoreHelper.LogData(HCoreConstant.LogType.Log, "GetBillerPackages", Url + "" + _Request.Type, _readerRespose, "");
                        OApp.OBillersResponse _RequestBodyContent = JsonConvert.DeserializeObject<OApp.OBillersResponse>(_readerRespose);
                        if (_RequestBodyContent != null && _RequestBodyContent.status == "success" && _RequestBodyContent.responseData != null && _RequestBodyContent.responseData.Count > 0)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _RequestBodyContent.responseData, PaymentResource.PR2128, PaymentResource.PR2128M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2127, PaymentResource.PR2127M);
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetBillerPackages", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the payments configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPaymentsConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                _OPaymentsConfiguration = new OApp.OPaymentsConfiguration();
                using (_HCoreContext = new HCoreContext())
                {

                    if (_Request.Type == "donation")
                    {
                        _OPaymentsConfiguration.PaymentReference = "DNR" + _Request.ReferenceId + "O" + HCoreHelper.GenerateDateString();
                        _OPaymentsConfiguration.Type = "donation";
                        _OPaymentsConfiguration.MinAmount = 100;
                        _OPaymentsConfiguration.MaxAmount = 10000000;
                        _OPaymentsConfiguration.ChargeType = "percentage";
                        _OPaymentsConfiguration.ChargeValue = 0;
                        _OPaymentsConfiguration.MaxChargeValue = 0;
                        _OPaymentsConfiguration.FixedCharge = 0;
                        _OPaymentsConfiguration.RewardType = "percentage";
                        _OPaymentsConfiguration.RewardValue = 1;
                        _OPaymentsConfiguration.MaxRewardAmount = 500;
                        //_OPaymentsConfiguration.UserCards = UserCards;
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OPaymentsConfiguration, "HC0001");
                    }
                    if (_Request.Type == "buypoint")
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "App Update Required", "Please update your app to enjoy our latest features and updates");
                        _OPaymentsConfiguration.PaymentReference = "OBP" + _Request.ReferenceId + "O" + HCoreHelper.GenerateDateString();
                        _OPaymentsConfiguration.Type = "buypoint";
                        _OPaymentsConfiguration.MinAmount = 500;
                        _OPaymentsConfiguration.MaxAmount = 50000;
                        _OPaymentsConfiguration.ChargeType = "percentage";
                        _OPaymentsConfiguration.ChargeValue = 1.5;
                        _OPaymentsConfiguration.MaxChargeValue = 2500;
                        _OPaymentsConfiguration.FixedCharge = 100;
                        _OPaymentsConfiguration.RewardType = "percentage";
                        _OPaymentsConfiguration.RewardValue = 1;
                        _OPaymentsConfiguration.MaxRewardAmount = 500;
                        //_OPaymentsConfiguration.UserCards = UserCards;
                        using (_HCoreContext = new HCoreContext())
                        {
                            _VasPayment = new VasPayment();
                            _VasPayment.Guid = HCoreHelper.GenerateGuid();
                            _VasPayment.ProductItemId = 1;
                            _VasPayment.TypeId = PaymentTypePointPurchase;
                            _VasPayment.AccountId = _Request.UserReference.AccountId;
                            _VasPayment.AccountNumber = _Request.UserReference.AccountCode;
                            _VasPayment.Amount = 0;
                            _VasPayment.RewardAmount = 0;
                            _VasPayment.UserRewardAmount = 0;
                            _VasPayment.CommissionAmount = 0;
                            _VasPayment.PaymentReference = _OPaymentsConfiguration.PaymentReference;
                            _VasPayment.PaymentSource = "payment";
                            _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                            _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                            _VasPayment.CreatedById = _Request.UserReference.AccountId;
                            _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                            _HCoreContext.VasPayment.Add(_VasPayment);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OPaymentsConfiguration, "HC0001");
                    }
                    else if (_Request.Type == "lcctopup")
                    {
                        _OPaymentsConfiguration.PaymentReference = "LCC" + _Request.ReferenceId + "O" + HCoreHelper.GenerateDateString();
                        _OPaymentsConfiguration.Type = "lcctopup";
                        _OPaymentsConfiguration.MinAmount = 500;
                        _OPaymentsConfiguration.MaxAmount = 40000;
                        _OPaymentsConfiguration.ChargeType = "percentage";
                        _OPaymentsConfiguration.ChargeValue = 1.5;
                        _OPaymentsConfiguration.MaxChargeValue = 2500;
                        _OPaymentsConfiguration.FixedCharge = 100;
                        _OPaymentsConfiguration.RewardType = "percentage";
                        _OPaymentsConfiguration.RewardValue = 0.5;
                        _OPaymentsConfiguration.MaxRewardAmount = 500;
                        //_OPaymentsConfiguration.UserCards = UserCards;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OPaymentsConfiguration, "HC0001");
                        #endregion
                    }
                    else if (_Request.Type == "billers")
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "App Update Required", "Please update your app to enjoy our latest features and updates");
                        _OPaymentsConfiguration.PaymentReference = "PM" + _Request.ReferenceId + "O" + HCoreHelper.GenerateDateString();
                        _OPaymentsConfiguration.Type = "billers";
                        _OPaymentsConfiguration.MinAmount = 0;
                        _OPaymentsConfiguration.MaxAmount = 40000;
                        _OPaymentsConfiguration.ChargeType = "percentage";
                        _OPaymentsConfiguration.ChargeValue = 1.5;
                        _OPaymentsConfiguration.MaxChargeValue = 2500;
                        _OPaymentsConfiguration.FixedCharge = 100;
                        _OPaymentsConfiguration.RewardType = "percentage";
                        _OPaymentsConfiguration.RewardValue = 1;
                        _OPaymentsConfiguration.MaxRewardAmount = 500;
                        //_OPaymentsConfiguration.UserCards = UserCards;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OPaymentsConfiguration, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OPaymentsConfiguration, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Payment account verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse PaymentAccountVerify(OApp.OPaymentDetails _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                if (_Request.Type == "billers")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "App Update Required", "Please update your app to enjoy our latest features and updates");
                    //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "NOTAVAILABLE", "Bill payments not available at the moment. Please try after some time.");
                    var TopupResponse = CorePayments.CoralPayAccountVerify(_Request.AccountNumber, _Request.BillerName, _Request.PackageName);
                    if (TopupResponse != null)
                    {
                        if (TopupResponse.error == false && TopupResponse.status == "success")
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long ProductItemId = _HCoreContext.VASProductItem.Where(x => x.ReferenceKey == _Request.PackageName).Select(x => x.Id).FirstOrDefault();
                                _VasPayment = new VasPayment();
                                _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                _VasPayment.TypeId = PaymentTypeBillPayment;
                                if (ProductItemId > 0)
                                {
                                    _VasPayment.ProductItemId = ProductItemId;
                                }
                                _VasPayment.AccountId = _Request.AccountId;
                                _VasPayment.AccountNumber = _Request.AccountNumber;
                                _VasPayment.Amount = _Request.Amount;
                                _VasPayment.RewardAmount = 0;
                                _VasPayment.PaymentReference = _Request.PaymentReference;
                                if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                                {
                                    _VasPayment.PaymentSource = "wallet";
                                }
                                else
                                {
                                    _VasPayment.PaymentSource = "payment";
                                }
                                _VasPayment.BillerName = _Request.BillerName;
                                _VasPayment.PackageName = _Request.PackageName;
                                _VasPayment.OrderId = _Request.OrderId;
                                _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                                _HCoreContext.VasPayment.Add(_VasPayment);
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, TopupResponse, PaymentResource.PR2134, PaymentResource.PR2134M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2135C, "Enter valid account number. " + TopupResponse.message);
                            #endregion
                        }
                        //&& _RequestBodyContent.status == "success" && _RequestBodyContent.responseData != null

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2135, PaymentResource.PR2135M);
                        #endregion
                    }

                }
                else if (_Request.Type == "lcctopup")
                {
                    bool TopupResponse = CorePayments.GetLccAccount(_Request.AccountNumber);
                    if (TopupResponse)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            _VasPayment = new VasPayment();
                            _VasPayment.Guid = HCoreHelper.GenerateGuid();
                            _VasPayment.ProductItemId = 2;
                            _VasPayment.TypeId = PaymentTypeLccTopup;
                            _VasPayment.AccountId = _Request.AccountId;
                            _VasPayment.AccountNumber = _Request.AccountNumber;
                            _VasPayment.Amount = _Request.Amount;
                            _VasPayment.RewardAmount = 0;
                            _VasPayment.PaymentReference = _Request.PaymentReference;
                            if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                            {
                                _VasPayment.PaymentSource = "wallet";
                            }
                            else
                            {
                                _VasPayment.PaymentSource = "payment";
                            }
                            _VasPayment.BillerName = _Request.BillerName;
                            _VasPayment.PackageName = _Request.PackageName;
                            _VasPayment.OrderId = _Request.OrderId;
                            _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                            _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                            _VasPayment.CreatedById = _Request.UserReference.AccountId;
                            _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                            _HCoreContext.VasPayment.Add(_VasPayment);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2134, PaymentResource.PR2134M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2135, PaymentResource.PR2135M);
                        #endregion
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("PaymentAccountVerify", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Payment confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse PaymentConfirm(OApp.OPaymentDetails _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                _TransactionResponse = new OCoreTransaction.Response();
                if (_Request.Type == "buypoint")
                {
                    PayStackResponseData _PayStackResponseData = CorePayments.GetPayStackPaymentStatus(_Request.PaymentReference, _Request.UserReference);
                    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                    {
                        #region Operation
                        if (_PayStackResponseData.authorization != null)
                        {
                            if (_PayStackResponseData.authorization.reusable)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var CardDetailsCheck = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _PayStackResponseData.authorization.authorization_code && x.AccountId == _Request.AccountId && x.TypeId == UserCard).FirstOrDefault();
                                    if (CardDetailsCheck == null)
                                    {
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = _PayStackResponseData.authorization.authorization_code;
                                        _HCUAccountParameter.TypeId = UserCard;
                                        _HCUAccountParameter.AccountId = _Request.AccountId;
                                        _HCUAccountParameter.Name = _PayStackResponseData.authorization.brand;
                                        _HCUAccountParameter.SystemName = _PayStackResponseData.authorization.bin;
                                        _HCUAccountParameter.Value = _PayStackResponseData.authorization.last4;
                                        _HCUAccountParameter.SubValue = _PayStackResponseData.authorization.exp_month;
                                        _HCUAccountParameter.Description = _PayStackResponseData.authorization.exp_year;
                                        _HCUAccountParameter.Data = _PayStackResponseData.authorization.channel;
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                var BillPayment = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                if (BillPayment != null)
                                {
                                    BillPayment.Amount = _PayStackResponseData.amount / 100;
                                    BillPayment.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPayment.ModifyById = _Request.UserReference.AccountId;
                                    BillPayment.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                    _HCoreContext.SaveChanges();
                                }
                                _HCoreContext.Dispose();
                            }
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = 3;
                            _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.amount / 100;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.amount / 100;
                            _CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _PayStackResponseData.amount / 100;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeReward.TransactionTypePointPurchase,
                                SourceId = TransactionSource.TUC,
                                Amount = _PayStackResponseData.amount / 100,
                                Charge = 0,
                                TotalAmount = _PayStackResponseData.amount / 100,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BillPayment = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                    if (BillPayment != null)
                                    {
                                        BillPayment.TransactionId = TransactionResponse.ReferenceId;
                                        BillPayment.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        BillPayment.ModifyById = _Request.UserReference.AccountId;
                                        BillPayment.StatusId = HelperStatus.BillPaymentStatus.Success;
                                        BillPayment.EndDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                    _HCoreContext.Dispose();
                                }
                                _PaymentItems = new List<PaymentItem>();
                                _PaymentItems.Add(new PaymentItem
                                {
                                    Name = "Payment Ref",
                                    Value = TransactionResponse.ReferenceId.ToString(),
                                });
                                _PaymentItems.Add(new PaymentItem
                                {
                                    Name = "Purchase Amount",
                                    Value = _CoreTransactionRequest.InvoiceAmount.ToString(),
                                });
                                _PaymentItems.Add(new PaymentItem
                                {
                                    Name = "Charges",
                                    Value = "0",
                                });
                                _PaymentItems.Add(new PaymentItem
                                {
                                    Name = "Total Amount",
                                    Value = _CoreTransactionRequest.InvoiceAmount.ToString(),
                                });
                                _PaymentItems.Add(new PaymentItem
                                {
                                    Name = "Transaction Date",
                                    Value = TransactionResponse.TransactionDate.AddHours(1).ToString("dd-MM-yyy HH:mm"),
                                });
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _PaymentItems, PaymentResource.PR2124, PaymentResource.PR2124M);
                                #endregion
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BillPayment = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                    if (BillPayment != null)
                                    {
                                        if (TransactionResponse.ReferenceId != 0)
                                        {
                                            BillPayment.TransactionId = TransactionResponse.ReferenceId;
                                        }
                                        BillPayment.EndDate = HCoreHelper.GetGMTDateTime();
                                        BillPayment.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        BillPayment.ModifyById = _Request.UserReference.AccountId;
                                        BillPayment.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                        _HCoreContext.SaveChanges();
                                    }
                                    _HCoreContext.Dispose();
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                #endregion
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                        }
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                    }
                }
                else if (_Request.Type == "billers")
                {
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    double Balance = _ManageCoreTransaction.GetAppUserBalance(_Request.UserReference.AccountId);

                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Billers

                        _Billers = new List<OApp.OBiller>();
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 73,
                            name = "Enugu",
                            slug = "ENUGU_DISCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 1,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 73,
                            name = "Eko",
                            slug = "EKO_DISCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.5,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 74,
                            name = "Abuja",
                            slug = "ABUJA_DISCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.5,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 75,
                            name = "PortHarcourt",
                            slug = "PORTHARCOURT_DISCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.5,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 77,
                            name = "IKEDC",
                            slug = "IKEDC",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.5,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 78,
                            name = "IBEDC",
                            slug = "IBEDC",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.5,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 79,
                            name = "KEDCO",
                            slug = "KEDCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0.25,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 83,
                            name = "KAEDCO",
                            slug = "KAEDCO",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 84,
                            name = "JEDC",
                            slug = "JEDC",
                            groupId = 1,
                            amount = 0,
                            rewardPercentage = 0,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 2,
                            name = "Dstv",
                            slug = "DSTV",
                            groupId = 2,
                            amount = 0,
                            rewardPercentage = 0.75,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 3,
                            name = "GOtv",
                            slug = "GOTV",
                            groupId = 2,
                            amount = 0,
                            rewardPercentage = 0.75,

                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 80,
                            name = "Startimes",
                            slug = "STARTIMES",
                            groupId = 2,
                            amount = 0,
                            rewardPercentage = 0.5,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 81,
                            name = "Smile",
                            slug = "SMILE",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 0.75,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 82,
                            name = "Spectranet",
                            slug = "SPECTRANET",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 1,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 87,
                            name = "MTN NIGERIA",
                            slug = "MTN_NIGERIA",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 1.25,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 88,
                            name = "AIRTEL NIGERIA",
                            slug = "AIRTEL_NIGERIA",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 1.25,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 89,
                            name = "GLO NIGERIA",
                            slug = "GLO_NIGERIA",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 2,
                        });
                        _Billers.Add(new OApp.OBiller
                        {
                            id = 90,
                            name = "9MOBILE NIGERIA",
                            slug = "9MOBILE_NIGERIA",
                            groupId = 3,
                            amount = 0,
                            rewardPercentage = 2,
                        });
                        #endregion
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId)
                              .Select(x => new
                              {
                                  AccountId = x.Id,
                                  MobileNumber = x.MobileNumber,
                                  EmailAddress = x.EmailAddress,
                              }).FirstOrDefault();
                        if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                        {
                            if (Balance < _Request.Amount)
                            {
                                var BillPaymentItemDetails = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference).FirstOrDefault();
                                if (BillPaymentItemDetails != null)
                                {
                                    BillPaymentItemDetails.AccountId = _Request.AccountId;
                                    BillPaymentItemDetails.AccountNumber = _Request.AccountNumber;
                                    BillPaymentItemDetails.TransactionId = _TransactionResponse.ReferenceId;
                                    BillPaymentItemDetails.Amount = _Request.Amount;
                                    BillPaymentItemDetails.RewardAmount = 0;
                                    BillPaymentItemDetails.PaymentReference = _Request.PaymentReference;
                                    BillPaymentItemDetails.BillerName = _Request.BillerName;
                                    BillPaymentItemDetails.PackageName = _Request.PackageName;
                                    BillPaymentItemDetails.OrderId = _Request.OrderId;
                                    BillPaymentItemDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentItemDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                    BillPaymentItemDetails.Comment = "Low balance";
                                    _HCoreContext.SaveChanges();
                                }
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "Low Balance", "Your account balance is low.");
                            }
                            // Full tuc points used for payment
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = 3;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionTypeRedeem.BillPayment,
                                SourceId = TransactionSource.TUC,
                                Amount = _Request.Amount,
                                Charge = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = 3,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeReward.BillPayment,
                                SourceId = TransactionSourcePayment,
                                Amount = _Request.Amount,
                                Charge = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                #endregion
                            }
                        }
                        else
                        {
                            // New full payment
                            PayStackResponseData _PayStackResponseData = CorePayments.GetPayStackPaymentStatus(_Request.PaymentReference, _Request.UserReference);
                            if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                            {
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = 3;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeReward.BillPayment,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeRedeem.BillPayment,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = 3,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeReward.BillPayment,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2137, PaymentResource.PR2137M);
                            }
                        }
                        if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long BillPaymentId = 0;
                                var BillPaymentItemDetails = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference).FirstOrDefault();
                                if (BillPaymentItemDetails != null)
                                {
                                    BillPaymentId = BillPaymentItemDetails.Id;
                                    BillPaymentItemDetails.AccountId = _Request.AccountId;
                                    BillPaymentItemDetails.AccountNumber = _Request.AccountNumber;
                                    BillPaymentItemDetails.TransactionId = _TransactionResponse.ReferenceId;
                                    BillPaymentItemDetails.Amount = _Request.Amount;
                                    BillPaymentItemDetails.RewardAmount = 0;
                                    BillPaymentItemDetails.PaymentReference = _Request.PaymentReference;
                                    if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                                    {
                                        BillPaymentItemDetails.PaymentSource = "wallet";
                                    }
                                    else
                                    {
                                        BillPaymentItemDetails.PaymentSource = "payment";
                                    }
                                    BillPaymentItemDetails.BillerName = _Request.BillerName;
                                    BillPaymentItemDetails.PackageName = _Request.PackageName;
                                    BillPaymentItemDetails.OrderId = _Request.OrderId;
                                    BillPaymentItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentItemDetails.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    long PackageId = _HCoreContext.VASProductItem.Where(x => x.ReferenceKey == _Request.PackageName).Select(x => x.Id).FirstOrDefault();
                                    _VasPayment = new VasPayment();
                                    _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                    if (PackageId > 0)
                                    {
                                        _VasPayment.ProductItemId = PackageId;
                                    }
                                    _VasPayment.AccountId = _Request.AccountId;
                                    _VasPayment.AccountNumber = _Request.AccountNumber;
                                    _VasPayment.TransactionId = _TransactionResponse.ReferenceId;
                                    _VasPayment.Amount = _Request.Amount;
                                    _VasPayment.RewardAmount = 0;
                                    _VasPayment.PaymentReference = _Request.PaymentReference;
                                    if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                                    {
                                        _VasPayment.PaymentSource = "wallet";
                                    }
                                    else
                                    {
                                        _VasPayment.PaymentSource = "payment";
                                    }
                                    _VasPayment.BillerName = _Request.BillerName;
                                    _VasPayment.PackageName = _Request.PackageName;
                                    _VasPayment.OrderId = _Request.OrderId;
                                    _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                    _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                    _HCoreContext.VasPayment.Add(_VasPayment);
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                    BillPaymentId = _VasPayment.Id;
                                }
                                var PaymentResponse = CorePayments.CoralPayPayment(_Request.PaymentReference, _Request.AccountNumber, _Request.PackageName, _Request.Amount, _Request.UserReference.DisplayName, CustomerDetails.MobileNumber, CustomerDetails.EmailAddress, CustomerDetails.AccountId, _Request.OrderId);
                                if (PaymentResponse != null && PaymentResponse.status == "success" && PaymentResponse.error == false)
                                {
                                    var PackageDetails = _Billers.Where(x => x.name == _Request.BillerName || x.slug == _Request.BillerName).FirstOrDefault();
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var BillPaymentDetails = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                        if (BillPaymentDetails != null)
                                        {
                                            BillPaymentDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                                            BillPaymentDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyById = _Request.UserReference.AccountId;
                                            if (PackageDetails != null && PackageDetails.rewardPercentage > 0)
                                            {
                                                BillPaymentDetails.RewardAmount = HCoreHelper.GetPercentage(_Request.Amount, PackageDetails.rewardPercentage);
                                            }
                                        }
                                        var tDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _TransactionResponse.ReferenceId).FirstOrDefault();
                                        tDetails.ReferenceNumber = PaymentResponse.responseData.paymentReference;
                                        _HCoreContext.SaveChanges();
                                    }

                                    if (PackageDetails != null && PackageDetails.rewardPercentage > 0)
                                    {
                                        double RewardAmount = HCoreHelper.GetPercentage(_Request.Amount, PackageDetails.rewardPercentage);
                                        if (RewardAmount > 0)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var BillPaymentItem = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                                if (BillPaymentItem != null)
                                                {
                                                    BillPaymentItem.RewardAmount = RewardAmount;
                                                }
                                                _HCoreContext.SaveChanges();
                                            }
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = 3;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                            _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = 3,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeRedeem.BillPayment,
                                                SourceId = TransactionSource.Merchant,
                                                Amount = RewardAmount,
                                                Comission = 0,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeReward.BillPayment,
                                                SourceId = TransactionSource.TUC,
                                                Amount = RewardAmount,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        }
                                    }


                                    _PaymentItems = new List<PaymentItem>();
                                    _PaymentItems.Add(new PaymentItem
                                    {
                                        Name = "Payment Ref",
                                        Value = BillPaymentId.ToString(),
                                    });
                                    _PaymentItems.Add(new PaymentItem
                                    {
                                        Name = "Purchase Amount",
                                        Value = _Request.Amount.ToString(),
                                    });
                                    _PaymentItems.Add(new PaymentItem
                                    {
                                        Name = "Charges",
                                        Value = "0",
                                    });
                                    _PaymentItems.Add(new PaymentItem
                                    {
                                        Name = "Total Amount",
                                        Value = _Request.Amount.ToString(),
                                    });
                                    if (PackageDetails != null && PackageDetails.rewardPercentage > 0)
                                    {
                                        double RewardAmount = HCoreHelper.GetPercentage(_Request.Amount, PackageDetails.rewardPercentage);
                                        _PaymentItems.Add(new PaymentItem
                                        {
                                            Name = "Reward Earned",
                                            Value = RewardAmount.ToString(),
                                        });
                                    }
                                    _PaymentItems.Add(new PaymentItem
                                    {
                                        Name = "Transaction Date",
                                        Value = HCoreHelper.GetGMTDateTime().AddHours(1).ToString("dd-MM-yyy HH:mm"),
                                    });
                                    if (PaymentResponse.responseData != null && PaymentResponse.responseData.tokenData != null && PaymentResponse.responseData.tokenData.stdToken != null && !string.IsNullOrEmpty(PaymentResponse.responseData.tokenData.stdToken.value))
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _PaymentItems, PaymentResource.PR2132, PaymentResource.PR2132M + ". Your Token : " + PaymentResponse.responseData.tokenData.stdToken.value);
                                    }
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _PaymentItems, PaymentResource.PR2132, PaymentResource.PR2132M);
                                }
                                else
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var BillPaymentDetails = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                        if (BillPaymentDetails != null)
                                        {
                                            BillPaymentDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                            BillPaymentDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyById = _Request.UserReference.AccountId;
                                        }
                                        _HCoreContext.SaveChanges();
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = 3;
                                        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                        _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                        _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = 3,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeRedeem.BillPayment,
                                            SourceId = TransactionSourcePayment,
                                            Amount = _Request.Amount,
                                            Charge = 0,
                                            TotalAmount = _Request.Amount,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request.AccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeReward.BillPayment,
                                            SourceId = TransactionSource.TUC,
                                            Amount = _Request.Amount,
                                            Charge = 0,
                                            TotalAmount = _Request.Amount,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {

                                        }
                                        else
                                        {

                                        }
                                        //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2133, PaymentResource.PR2133M);
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2138, PaymentResource.PR2138M);
                                        // return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2133, PaymentResource.PR2133M);
                                    }
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                        }
                    }

                }
                else if (_Request.Type == "lcctopup")
                {
                    _ManageCoreTransaction = new ManageCoreTransaction();

                    double Balance = _ManageCoreTransaction.GetAppUserBalance(_Request.UserReference.AccountId);
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId)
                               .Select(x => new
                               {
                                   AccountId = x.Id,
                                   MobileNumber = x.MobileNumber,
                                   EmailAddress = x.EmailAddress,
                               }).FirstOrDefault();
                        if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                        {
                            if (Balance < _Request.Amount)
                            {
                                var BillPaymentItemDetails = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference).FirstOrDefault();
                                if (BillPaymentItemDetails != null)
                                {
                                    BillPaymentItemDetails.AccountId = _Request.AccountId;
                                    BillPaymentItemDetails.AccountNumber = _Request.AccountNumber;
                                    BillPaymentItemDetails.TransactionId = _TransactionResponse.ReferenceId;
                                    BillPaymentItemDetails.Amount = _Request.Amount;
                                    BillPaymentItemDetails.RewardAmount = 0;
                                    BillPaymentItemDetails.PaymentReference = _Request.PaymentReference;
                                    BillPaymentItemDetails.BillerName = _Request.BillerName;
                                    BillPaymentItemDetails.PackageName = _Request.PackageName;
                                    BillPaymentItemDetails.OrderId = _Request.OrderId;
                                    BillPaymentItemDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentItemDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                    BillPaymentItemDetails.Comment = "Low balance";
                                    _HCoreContext.SaveChanges();
                                }
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "Low Balance", "Your account balance is low.");
                            }
                            // Full tuc points used for payment
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = 3;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionTypeRedeem.LccTopup,
                                SourceId = TransactionSource.TUC,
                                Amount = _Request.Amount,
                                Charge = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = 3,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeReward.LCCTopup,
                                SourceId = TransactionSourcePayment,
                                Amount = _Request.Amount,
                                Charge = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                #endregion
                            }
                        }
                        else
                        {
                            // New full payment
                            PayStackResponseData _PayStackResponseData = CorePayments.GetPayStackPaymentStatus(_Request.PaymentReference, _Request.UserReference);
                            if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                            {
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = 3;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeReward.LCCTopup,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeRedeem.LccTopup,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = 3,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeReward.LCCTopup,
                                    SourceId = TransactionSourcePayment,
                                    Amount = _Request.Amount,
                                    Charge = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2137, PaymentResource.PR2137M);
                            }
                        }
                        if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long BillPaymentId = 0;
                                var BillPaymentItemDetails = _HCoreContext.VasPayment.Where(x => x.PaymentReference == _Request.PaymentReference).FirstOrDefault();
                                if (BillPaymentItemDetails != null)
                                {
                                    BillPaymentId = BillPaymentItemDetails.Id;
                                    BillPaymentItemDetails.AccountId = _Request.AccountId;
                                    BillPaymentItemDetails.AccountNumber = _Request.AccountNumber;
                                    BillPaymentItemDetails.TransactionId = _TransactionResponse.ReferenceId;
                                    BillPaymentItemDetails.Amount = _Request.Amount;
                                    BillPaymentItemDetails.RewardAmount = 0;
                                    BillPaymentItemDetails.PaymentReference = _Request.PaymentReference;
                                    if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                                    {
                                        BillPaymentItemDetails.PaymentSource = "wallet";
                                    }
                                    else
                                    {
                                        BillPaymentItemDetails.PaymentSource = "payment";
                                    }
                                    BillPaymentItemDetails.BillerName = _Request.BillerName;
                                    BillPaymentItemDetails.PackageName = _Request.PackageName;
                                    BillPaymentItemDetails.OrderId = _Request.OrderId;
                                    BillPaymentItemDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    BillPaymentItemDetails.ModifyById = _Request.UserReference.AccountId;
                                    BillPaymentItemDetails.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {

                                    _VasPayment = new VasPayment();
                                    _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                    _VasPayment.ProductItemId = 2;
                                    _VasPayment.TypeId = PaymentTypeLccTopup;
                                    _VasPayment.AccountId = _Request.AccountId;
                                    _VasPayment.AccountNumber = _Request.AccountNumber;
                                    _VasPayment.TransactionId = _TransactionResponse.ReferenceId;
                                    _VasPayment.Amount = _Request.Amount;
                                    _VasPayment.RewardAmount = 0;
                                    _VasPayment.PaymentReference = _Request.PaymentReference;
                                    if (_Request.RefMessage == "tuc balance used" && _Request.PaymentReference == _Request.RefTransactionId)
                                    {
                                        _VasPayment.PaymentSource = "wallet";
                                    }
                                    else
                                    {
                                        _VasPayment.PaymentSource = "payment";
                                    }
                                    _VasPayment.BillerName = _Request.BillerName;
                                    _VasPayment.PackageName = _Request.PackageName;
                                    _VasPayment.OrderId = _Request.OrderId;
                                    _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                    _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Processing;
                                    _HCoreContext.VasPayment.Add(_VasPayment);
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                    BillPaymentId = _VasPayment.Id;
                                }
                                var PaymentResponse = CorePayments.InitializeTransaction(_Request.PaymentReference, _Request.AccountNumber, _Request.Amount);
                                //var PaymentResponse = CorePayments.CoralPayPayment(_Request.PaymentReference, _Request.AccountNumber, _Request.PackageName, _Request.Amount, _Request.UserReference.DisplayName, CustomerDetails.MobileNumber, CustomerDetails.EmailAddress, CustomerDetails.AccountId, _Request.OrderId);
                                if (PaymentResponse == true)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var BillPaymentDetails = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                        if (BillPaymentDetails != null)
                                        {
                                            BillPaymentDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                                            BillPaymentDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyById = _Request.UserReference.AccountId;
                                        }
                                        var tDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _TransactionResponse.ReferenceId).FirstOrDefault();
                                        //tDetails.ReferenceNumber = PaymentResponse.responseData.paymentReference;
                                        _HCoreContext.SaveChanges();
                                    }
                                    //if (PackageDetails != null && PackageDetails.rewardPercentage > 0)
                                    //{
                                    //    double RewardAmount = HCoreHelper.GetPercentage(_Request.Amount, PackageDetails.rewardPercentage);
                                    //    if (RewardAmount > 0)
                                    //    {
                                    //using (_HCoreContext = new HCoreContext())
                                    //{
                                    //    var BillPaymentItem = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                    //    if (BillPaymentItem != null)
                                    //    {
                                    //        BillPaymentItem.RewardAmount = RewardAmount;
                                    //    }
                                    //    _HCoreContext.SaveChanges();
                                    //}
                                    //        _CoreTransactionRequest = new OCoreTransaction.Request();
                                    //        _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                    //        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    //        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    //        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    //        _CoreTransactionRequest.ParentId = 3;
                                    //        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                    //        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                    //        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                    //        _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                    //        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    //        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    //        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    //        {
                                    //            UserAccountId = 3,
                                    //            ModeId = TransactionMode.Debit,
                                    //            TypeId = TransactionTypePayment,
                                    //            SourceId = TransactionSource.Merchant,
                                    //            Amount = RewardAmount,
                                    //            Comission = 0,
                                    //            TotalAmount = RewardAmount,
                                    //            TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    //        });
                                    //        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    //        {
                                    //            UserAccountId = _Request.AccountId,
                                    //            ModeId = TransactionMode.Credit,
                                    //            TypeId = TransactionTypePayment,
                                    //            SourceId = TransactionSource.TUC,
                                    //            Amount = RewardAmount,
                                    //            TotalAmount = RewardAmount,
                                    //            TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    //        });
                                    //        _CoreTransactionRequest.Transactions = _TransactionItems;
                                    //        _ManageCoreTransaction = new ManageCoreTransaction();
                                    //        OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    //    }
                                    //}
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2132, PaymentResource.PR2132M);
                                }
                                else
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var BillPaymentDetails = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                                        if (BillPaymentDetails != null)
                                        {
                                            BillPaymentDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                            BillPaymentDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            BillPaymentDetails.ModifyById = _Request.UserReference.AccountId;
                                        }
                                        _HCoreContext.SaveChanges();
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = 3;
                                        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                        _CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                        _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = 3,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeRedeem.LccTopup,
                                            SourceId = TransactionSourcePayment,
                                            Amount = _Request.Amount,
                                            Charge = 0,
                                            TotalAmount = _Request.Amount,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request.AccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeReward.LCCTopup,
                                            SourceId = TransactionSource.TUC,
                                            Amount = _Request.Amount,
                                            Charge = 0,
                                            TotalAmount = _Request.Amount,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {

                                        }
                                        else
                                        {

                                        }
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2132, PaymentResource.PR2132M);
                                        // return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2133, PaymentResource.PR2133M);
                                    }
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                        }
                    }
                    //long BillPaymentId = 0;
                    //bool TopupResponse = CorePayments.InitializeTransaction(_CoreTransactionRequest.GroupKey, _Request.AccountNumber, _Request.Amount);
                    //if (TopupResponse)
                    //{
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        var BillPaymentDetails = _HCoreContext.VasPayment.Where(x => x.Id == BillPaymentId).FirstOrDefault();
                    //        if (BillPaymentDetails != null)
                    //        {
                    //            BillPaymentDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                    //            BillPaymentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //            BillPaymentDetails.ModifyById = _Request.UserReference.AccountId;
                    //        }
                    //        var tDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _TransactionResponse.ReferenceId).FirstOrDefault();
                    //        tDetails.ReferenceNumber = PaymentResponse.responseData.paymentReference;
                    //        _HCoreContext.SaveChanges();
                    //    }
                    //    #region Send Response
                    //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2130, PaymentResource.PR2130M);
                    //    #endregion
                    //}
                    //else
                    //{
                    //    #region Send Response
                    //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2131, PaymentResource.PR2131);
                    //    #endregion
                    //}
                    //_CoreTransactionRequest = new OCoreTransaction.Request();
                    //_CoreTransactionRequest.CustomerId = _Request.AccountId;
                    //_CoreTransactionRequest.UserReference = _Request.UserReference;
                    //_CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                    //_CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                    //_CoreTransactionRequest.ParentId = 3;
                    //_CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                    //_CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                    //_CoreTransactionRequest.AccountNumber = _Request.AccountNumber;
                    //_CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                    //_CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                    //_CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                    //_TransactionItems = new List<OCoreTransaction.TransactionItem>();
                    //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                    //{
                    //    UserAccountId = _Request.AccountId,
                    //    ModeId = TransactionMode.Debit,
                    //    TypeId = 480,
                    //    SourceId = TransactionSource.TUC,
                    //    Amount = _Request.Amount,
                    //    Charge = 0,
                    //    TotalAmount = _Request.Amount,
                    //});
                    //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                    //{
                    //    UserAccountId = 3,
                    //    ModeId = TransactionMode.Credit,
                    //    TypeId = 480,
                    //    SourceId = 481,
                    //    Amount = _Request.Amount,
                    //    Charge = 0,
                    //    TotalAmount = _Request.Amount,
                    //});
                    //_CoreTransactionRequest.Transactions = _TransactionItems;
                    //_ManageCoreTransaction = new ManageCoreTransaction();
                    //OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                    //if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                    //{
                    //    bool TopupResponse = CorePayments.InitializeTransaction(_CoreTransactionRequest.GroupKey, _Request.AccountNumber, _Request.Amount);
                    //    if (TopupResponse)
                    //    {
                    //        #region Send Response
                    //        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2130, PaymentResource.PR2130M);
                    //        #endregion
                    //    }
                    //    else
                    //    {
                    //        #region Send Response
                    //        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2131, PaymentResource.PR2131);
                    //        #endregion
                    //    }

                    //}
                    //else
                    //{
                    //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                    //}
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("PaymentConfirm", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Payment charge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse PaymentCharge(OApp.OPaymentCharge _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long AccountStatus = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.StatusId).FirstOrDefault();
                    if (AccountStatus != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "User Unauthorized", "You account is suspended or blocked. Please contact support to activate account");
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ChargeDetails = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.ReferenceId && x.AccountId == _Request.AccountId)
                        .Select(x => new { AuthCode = x.Guid, x.Account.EmailAddress }).FirstOrDefault();
                    if (ChargeDetails != null)
                    {
                        var CInfo = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.ReferenceId && x.AccountId == _Request.AccountId)
                        .FirstOrDefault();
                        if (CInfo != null)
                        {
                            CInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                            CInfo.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                        }
                        OChargeData _PayStackResponseData = CorePayments.GetPayStackCharge(ChargeDetails.EmailAddress, _Request.Amount, ChargeDetails.AuthCode, _Request.AccountId + "" + HCoreHelper.GenerateRandomNumber(10), _Request.UserReference);
                        if (_PayStackResponseData.status == true)
                        {
                            if (_Request.Type == "buypoint")
                            {

                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = 3;
                                _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.data.amount / 100;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.data.amount / 100;
                                //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.data.;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _PayStackResponseData.data.amount / 100;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = 3,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = 466,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = _PayStackResponseData.data.amount / 100,
                                    Charge = 0,
                                    TotalAmount = _PayStackResponseData.data.amount / 100,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = 466,
                                    SourceId = TransactionSource.TUC,
                                    Amount = _PayStackResponseData.data.amount / 100,
                                    Charge = 0,
                                    TotalAmount = _PayStackResponseData.data.amount / 100,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {

                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2124, PaymentResource.PR2124M);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                    #endregion
                                }
                            }
                            else if (_Request.Type == "donation")
                            {
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = 3;
                                _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.data.amount / 100;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.data.amount / 100;
                                //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _PayStackResponseData.data.amount / 100;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = 467,
                                    SourceId = 469,
                                    Amount = _PayStackResponseData.data.amount / 100,
                                    Charge = 0,
                                    TotalAmount = _PayStackResponseData.data.amount / 100,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = 467,
                                    SourceId = 469,
                                    Amount = _PayStackResponseData.data.amount / 100,
                                    Charge = 0,
                                    TotalAmount = _PayStackResponseData.data.amount / 100,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2124, PaymentResource.PR2124M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2129, PaymentResource.PR2129M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2125, PaymentResource.PR2125M);
                            }
                            //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, PaymentResource.PR2125, PaymentResource.PR2125M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2136, PaymentResource.PR2136M);
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0001");
                        #endregion
                    }

                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("PaymentConfirm", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
