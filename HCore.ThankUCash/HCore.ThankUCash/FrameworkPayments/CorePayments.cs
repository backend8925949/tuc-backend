//==================================================================================
// FileName: CorePayments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core payment
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.ThankUCash.Object;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using RestSharp;
using HCore.Operations.Object;
using HCore.Operations;
using System.Security.Cryptography;

namespace HCore.ThankUCash.FrameworkPayments
{
    public class CorePayments
    {
        /// <summary>
        /// Gets the pay stack payment status.
        /// </summary>
        /// <param name="PaymentReference">The payment reference.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>PayStackResponseData.</returns>
        internal static PayStackResponseData GetPayStackPaymentStatus(string PaymentReference, OUserReference UserReference)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/verify/";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl + PaymentReference);
                request.ContentType = "application/json";
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    request.Headers["Authorization"] = "Bearer sk_live_505bddd9f974f9dcedbfe21c1c31d526b41479e5";
                }
                else
                {
                    request.Headers["Authorization"] = "Bearer sk_test_5766345ff22d84b730a356092d8e54a528dfd814";
                }
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    PayStackResponse _RequestBodyContent = JsonConvert.DeserializeObject<PayStackResponse>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        return _RequestBodyContent.data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetPayStackPaymentStatus", _Exception, UserReference);
                #endregion
                return null;
            }
        }
        /// <summary>
        /// Gets the pay stack charge.
        /// </summary>
        /// <param name="EmailAddress">The email address.</param>
        /// <param name="Amount">The amount.</param>
        /// <param name="authorization_code">The authorization code.</param>
        /// <param name="PaymentReference">The payment reference.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>OChargeData.</returns>
        internal static OChargeData GetPayStackCharge(string EmailAddress, double Amount, string? authorization_code, string? PaymentReference, OUserReference UserReference)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/charge";
                var client = new RestSharp.RestClient(PayStackUrl);
                var request = new RestSharp.RestRequest();
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    request.AddHeader("Authorization", "Bearer sk_live_505bddd9f974f9dcedbfe21c1c31d526b41479e5");
                }
                else
                {
                    request.AddHeader("Authorization", "Bearer sk_test_5766345ff22d84b730a356092d8e54a528dfd814");
                }
                request.AddParameter("reference", PaymentReference);
                request.AddParameter("authorization_code", authorization_code);
                request.AddParameter("email", EmailAddress);
                request.AddParameter("amount", Amount * 100);
                var response = client.Post(request);
                var content = response.Content; // raw content as string
                OChargeData _RequestBodyContent = JsonConvert.DeserializeObject<OChargeData>(content);
                if (_RequestBodyContent != null)
                {
                    return _RequestBodyContent;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetPayStackCharge", _Exception, UserReference);
                #endregion
                return null;
            }
        }
        /// <summary>
        /// Gets the LCC account.
        /// </summary>
        /// <param name="AccountNumber">The account number.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal static bool GetLccAccount(string AccountNumber)
        {
            try
            {
                string ApiKey = "139384af76804776b5f93094f77e7782139384af76804776b5f93094f77e7782";
                string ApiSecretKey = "ea41a087289f44998a02963653b531d7ea41a087289f44998a02963653b531d7";
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    string ServerUrl = "https://testapi.topuplcc.com/api/v2/getaccount";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        ServerUrl = "https://api.topuplcc.com/api/v2/getaccount";
                        ApiKey = "4b00ffab812f40a09aa44addd52ea6184b00ffab812f40a09aa44addd52ea618";
                        ApiSecretKey = "4cfd4fb880cb4d39934ada176499dada4cfd4fb880cb4d39934ada176499dada";
                    }

                    OLccManager.Request _LccRequest = new OLccManager.Request();
                    _LccRequest.reference = HCoreHelper.GenerateRandomNumber(10);
                    _LccRequest.accountNumber = AccountNumber;
                    _LccRequest.hash = EncryptHash(_LccRequest.reference + _LccRequest.accountNumber, ApiSecretKey);
                    string RequestContent = JsonConvert.SerializeObject(_LccRequest);
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                    byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                    _HttpWebRequest.ContentType = "application/json";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                        if (_RequestBodyContent.responseCode == "00")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetLccAccount", _Exception, null);
                #endregion
                return false;
            }
        }
        /// <summary>
        /// Initializes the transaction.
        /// </summary>
        /// <param name="Reference">The reference.</param>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="Amount">The amount.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal static bool InitializeTransaction(string Reference, string? AccountNumber, double Amount)
        {
            try
            {
                string ApiKey = "139384af76804776b5f93094f77e7782139384af76804776b5f93094f77e7782";
                string ApiSecretKey = "ea41a087289f44998a02963653b531d7ea41a087289f44998a02963653b531d7";
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    string ServerUrl = "https://testapi.topuplcc.com/api/v2/initializetopup";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        ServerUrl = "https://api.topuplcc.com/api/v2/initializetopup";
                        ApiKey = "4b00ffab812f40a09aa44addd52ea6184b00ffab812f40a09aa44addd52ea618";
                        ApiSecretKey = "4cfd4fb880cb4d39934ada176499dada4cfd4fb880cb4d39934ada176499dada";
                    }

                    OLccManager.Request _LccRequest = new OLccManager.Request();
                    _LccRequest.reference = Reference;
                    _LccRequest.accountNumber = AccountNumber;
                    _LccRequest.amount = Convert.ToInt64(Amount * 100);
                    _LccRequest.hash = EncryptHash(_LccRequest.reference + _LccRequest.accountNumber + _LccRequest.amount, ApiSecretKey);
                    string RequestContent = JsonConvert.SerializeObject(_LccRequest);
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                    byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                    _HttpWebRequest.ContentType = "application/json";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                        if (_RequestBodyContent.responseCode == "00")
                        {
                            return ConfirmTransaction(_RequestBodyContent, Reference, AccountNumber, Amount);
                        }
                        else
                        {
                            return false;
                        }
                        //OTokenResponse _RequestBodyContent = JsonConvert.DeserializeObject<OTokenResponse>(responseStr);
                        //if (_RequestBodyContent != null)
                        //{

                        //}
                        //else
                        //{
                        //    _HttpWebResponse.Close();
                        //}
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("InitializeTransaction", _Exception, null);
                #endregion
                return false;

            }
        }
        /// <summary>
        /// Confirms the transaction.
        /// </summary>
        /// <param name="_IniResponse">The ini response.</param>
        /// <param name="Reference">The reference.</param>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="Amount">The amount.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal static bool ConfirmTransaction(OLccManager.Response _IniResponse, string? Reference, string? AccountNumber, double Amount)
        {
            try
            {
                string ApiKey = "139384af76804776b5f93094f77e7782139384af76804776b5f93094f77e7782";
                string ApiSecretKey = "ea41a087289f44998a02963653b531d7ea41a087289f44998a02963653b531d7";
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    string ServerUrl = "https://testapi.topuplcc.com/api/v2/confirmtopup";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        ServerUrl = "https://api.topuplcc.com/api/v2/confirmtopup";
                        ApiKey = "4b00ffab812f40a09aa44addd52ea6184b00ffab812f40a09aa44addd52ea618";
                        ApiSecretKey = "4cfd4fb880cb4d39934ada176499dada4cfd4fb880cb4d39934ada176499dada";
                    }

                    OLccManager.Request _LccRequest = new OLccManager.Request();

                    _LccRequest.reference = Reference;
                    _LccRequest.accountNumber = AccountNumber;
                    _LccRequest.amount = Convert.ToInt64(Amount * 100);
                    _LccRequest.transactionReference = _IniResponse.result.transactionReference;
                    _LccRequest.hash = EncryptHash(_LccRequest.reference + _LccRequest.accountNumber + _LccRequest.amount + _IniResponse.result.transactionReference, ApiSecretKey);
                    string RequestContent = JsonConvert.SerializeObject(_LccRequest);
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                    byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                    _HttpWebRequest.ContentType = "application/json";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();

                        OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                        if (_RequestBodyContent.responseCode == "00")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("InitializeTransaction", _Exception, null);
                #endregion
                return false;

            }
        }
        /// <summary>
        /// Corals the pay account verify.
        /// </summary>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="billerSlug">The biller slug.</param>
        /// <param name="productName">Name of the product.</param>
        /// <returns>OApp.OCoralPaymentResponse.</returns>
        internal static OApp.OCoralPaymentResponse CoralPayAccountVerify(string AccountNumber, string? billerSlug, string? productName)
        {
            try
            {
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    //string ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/transactions/customer-lookup";
                    //if (HostEnvironment == HostEnvironmentType.Live)
                    //{
                    //    ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/transactions/customer-lookup";
                    //}

                    string ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        ServerUrl = "https://vas.coralpay.com/vas-service/api/";
                    }

                    OCoralPayPaymentRequest _Request = new OCoralPayPaymentRequest();
                    _Request.customerId = AccountNumber;
                    _Request.billerSlug = billerSlug;
                    _Request.productName = productName;
                    string RequestContent = JsonConvert.SerializeObject(_Request);
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl + "transactions/customer-lookup");
                    byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                    _HttpWebRequest.ContentType = "application/json";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        string username = "thankucash";
                        string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    else
                    {
                        string username = "smashlabs";
                        string password = "$m@$hl@b$1234";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        OApp.OCoralPaymentResponse _RequestBodyContent = JsonConvert.DeserializeObject<OApp.OCoralPaymentResponse>(responseStr);
                        if (_RequestBodyContent != null)
                        {
                            return _RequestBodyContent;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("InitializeTransaction", _Exception, null);
                #endregion
                return null;

            }
        }
        /// <summary>
        /// Corals the pay payment.
        /// </summary>
        /// <param name="paymentReference">The payment reference.</param>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="packageSlug">The package slug.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="customerName">Name of the customer.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="email">The email.</param>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>OApp.OCoralPaymentResponse.</returns>
        public static OApp.OCoralPaymentResponse CoralPayPayment(string paymentReference, string? AccountNumber, string? packageSlug, double amount, string? customerName, string? phoneNumber, string? email, long AccountId, string? orderId)
        {
            try
            {
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {

                    string ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        ServerUrl = "https://vas.coralpay.com/vas-service/api/";
                    }
                    //string ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/transactions/process-payment";
                    //if (HostEnvironment == HostEnvironmentType.Live)
                    //{
                    //    ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/transactions/process-payment";
                    //}

                    OCoralPayPaymentRequest _Request = new OCoralPayPaymentRequest();
                    _Request.paymentReference = paymentReference;
                    _Request.customerId = AccountNumber;
                    _Request.packageSlug = packageSlug;
                    _Request.orderId = orderId;
                    _Request.channel = "WEB";
                    _Request.amount = amount;
                    _Request.customerName = customerName;
                    _Request.phoneNumber = phoneNumber;
                    _Request.email = email;
                    _Request.accountNumber = AccountId.ToString();
                    string RequestContent = JsonConvert.SerializeObject(_Request,
                                                new JsonSerializerSettings()
                                                {
                                                    NullValueHandling = NullValueHandling.Ignore
                                                });
                    //JsonConvert.SerializeObject(_Request);
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl + "transactions/process-payment");
                    byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                    _HttpWebRequest.ContentType = "application/json";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        string username = "thankucash";
                        string password = "Th@nkUc@$h123";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    else
                    {
                        string username = "smashlabs";
                        string password = "$m@$hl@b$1234";
                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                        _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    }
                    //string username = "smashlabs";
                    //string password = "$m@$hl@b$1234";
                    //string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    //_HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        OApp.OCoralPaymentResponse _RequestBodyContent = JsonConvert.DeserializeObject<OApp.OCoralPaymentResponse>(responseStr);
                        if (_RequestBodyContent != null && _RequestBodyContent.status == "success" && _RequestBodyContent.responseData != null)
                        {
                            return _RequestBodyContent;
                            //#region Send Response
                            //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _RequestBodyContent.responseData, PaymentResource.PR2128, PaymentResource.PR2128M);
                            //#endregion
                        }
                        else
                        {
                            return null;
                            //#region Send Response
                            //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2127, PaymentResource.PR2127M);
                            //#endregion
                        }


                        //OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                        //if (_RequestBodyContent.responseCode == "00")
                        //{
                        //    //return ConfirmTransaction(_RequestBodyContent, Reference, AccountNumber, Amount);
                        //}
                        //else
                        //{
                        //    return false;
                        //}
                        //OTokenResponse _RequestBodyContent = JsonConvert.DeserializeObject<OTokenResponse>(responseStr);
                        //if (_RequestBodyContent != null)
                        //{

                        //}
                        //else
                        //{
                        //    _HttpWebResponse.Close();
                        //}
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("InitializeTransaction", _Exception, null);
                #endregion
                return null;

            }
        }
        /// <summary>
        /// Encrypts the hash.
        /// </summary>
        /// <param name="Content">The content.</param>
        /// <param name="SecretKey">The secret key.</param>
        /// <returns>System.String.</returns>
        internal static string EncryptHash(string Content, string? SecretKey)
        {
            var hash = new StringBuilder();
            byte[] secretkeyBytes = Encoding.UTF8.GetBytes(SecretKey);
            byte[] inputBytes = Encoding.UTF8.GetBytes(Content);
            using (var hmac = new HMACSHA512(secretkeyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(inputBytes);
                foreach (var theByte in hashValue)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }
            return hash.ToString();
        }
    }
}
