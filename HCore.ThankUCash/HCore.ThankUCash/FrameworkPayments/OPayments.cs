//==================================================================================
// FileName: OPayments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.FrameworkPayments
{


    public class OPayments
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentMobileNumber { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentName { get; set; }
            public string? SubParentDisplayName { get; set; }
            public string? SubParentMobileNumber { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierName { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? CashierMobileNumber { get; set; }

            public double? Amount { get; set; }
            public double? RewardAmount { get; set; }

            public string? AccountNumber { get; set; }

            public string? PaymentReference { get; set; }
            public string? PaymentSource { get; set; }

            public string? BillerName { get; set; }
            public string? PackageName { get; set; }

            public string? Request { get; set; }
            public string? Response { get; set; }

            public string? Comment { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }

            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class PayStackCharge
    {
        public string? status { get; set; }
        public long amount { get; set; }
        public Authorization authorization { get; set; }
    }
    public class OChargeData
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public OChargeInformation data { get; set; }

    }
    public class OChargeInformation
    {
        public long id { get; set; }
        public string? domain { get; set; }
        public string? status { get; set; }
        public string? reference { get; set; }
        public long amount { get; set; }
        public string? message { get; set; }
        public string? gateway_response { get; set; }
    }
    public class PayStackResponse
    {
        public bool id { get; set; }
        public PayStackResponseData data { get; set; }
    }
    public class PayStackResponseData
    {
        public string? status { get; set; }
        public long amount { get; set; }
        public Authorization authorization { get; set; }
    }
    public class Authorization
    {
        public string? authorization_code { get; set; }
        public string? bin { get; set; }
        public string? last4 { get; set; }
        public string? exp_month { get; set; }
        public string? exp_year { get; set; }
        public string? channel { get; set; }
        public string? card_type { get; set; }
        public string? bank { get; set; }
        public string? country_code { get; set; }
        public string? brand { get; set; }
        public bool reusable { get; set; }
        public string? signature { get; set; }
        public string? account_name { get; set; }
        public string? receiver_bank_account_number { get; set; }
        public string? receiver_bank { get; set; }
    }
    public class OLccManager
    {
        public class Request
        {
            public string? reference { get; set; }
            public string? accountNumber { get; set; }
            public long amount { get; set; }
            public string? hash { get; set; }
            public string? transactionReference { get; set; }
        }

        public class Response
        {
            public string? responseCode { get; set; }
            public string? status { get; set; }
            public string? message { get; set; }
            public string? reference { get; set; }
            public string? requestId { get; set; }
            public string? hash { get; set; }
            public InitializeDetails result { get; set; }
        }

        public class InitializeDetails
        {
            public string? balance { get; set; }
            public string? email { get; set; }
            public string? name { get; set; }
            public string? phoneNumber { get; set; }
            public string? accountNumber { get; set; }
            public string? transactionReference { get; set; }
            public string? reference { get; set; }
            public long amount { get; set; }
            public DateTime initializeDate { get; set; }
        }
    }
    public class OCoralPayPaymentRequest
    {
        public string? paymentReference { get; set; }
        public string? customerId { get; set; }
        public string? orderId { get; set; }
        public string? packageSlug { get; set; }
        public string? channel { get; set; }
        public double amount { get; set; }
        public string? customerName { get; set; }
        public string? phoneNumber { get; set; }
        public string? email { get; set; }
        public string? accountNumber { get; set; }
        public string? billerSlug { get; set; }
        public string? productName { get; set; }
    }

}
