//==================================================================================
// FileName: PaymentResource.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.FrameworkPayments
{
    public class PaymentResource
    {
        public const string PR2123 = "PR2123";
        public const string PR2123M = "Transaction details not found";
        public const string PR2124 = "PR2124";
        public const string PR2124M = "Payment successful. Points credited to your account";

        public const string PR2125 = "PR2124";
        public const string PR2125M = "Payment successful";

        public const string PR2126 = "PR2126";
        public const string PR2126M = "Payment successful";

        public const string PR2127 = "PR2127";
        public const string PR2127M = "Payment option not available at the moment";

        public const string PR2128 = "PR2128";
        public const string PR2128M = "Billers loaded";

        public const string PR2129 = "PR2129";
        public const string PR2129M = "Payment failed. Please contact support";

        public const string PR2130 = "PR2130";
        public const string PR2130M = "Topup  successful";



        public const string PR2131 = "PR2131";
        public const string PR2131M = "Topup  failed. Please contact support. Your amount will be refunded to your account within 24 hours";

        public const string PR2132 = "PR2132";
        public const string PR2132M = "Payment  successful";

        public const string PR2133 = "PR2133";
        public const string PR2133M = "Payment failed. Debited amount will be refunded to your account within 24 hours";

        public const string PR2134 = "PR2134";
        public const string PR2134M = "Account details loaded";

        public const string PR2135 = "PR2135";
        public const string PR2135M = "Account details not found. Please check your account number and try again";

        public const string PR2135C = "PR2135C";

        public const string PR2136 = "PR2136";
        public const string PR2136M = "Unable to make payment. Please try other payment option";

        public const string PR2137 = "PR2137";
        public const string PR2137M = "Unable to verify payment. Please contact support";

        public const string PR2138 = "PR2138";
        public const string PR2138M = "Unable to process payment. Amount is credited to your ThankUCash wallet.";


    }

}
