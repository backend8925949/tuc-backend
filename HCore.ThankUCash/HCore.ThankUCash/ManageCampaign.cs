//==================================================================================
// FileName: ManageCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;
namespace HCore.ThankUCash
{
    public class ManageCampaign
    {
        FrameworkCampaign _FrameworkCampaign;
        /// <summary>
        /// Duplicates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DuplicateCampaign(OCampaign.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.DuplicateCampaign(_Request);
        }
        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCampaign(OCampaign.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.SaveCampaign(_Request);
        }
        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCampaign(OCampaign.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.UpdateCampaign(_Request);
        }
        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCampaign(OCampaign.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.DeleteCampaign(_Request);
        }
        /// <summary>
        /// Gets the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaign(OCampaign.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.GetCampaign(_Request);
        }
        /// <summary>
        /// Gets the campaign list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaign(OList.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.GetCampaign(_Request);
        }
    }
}
