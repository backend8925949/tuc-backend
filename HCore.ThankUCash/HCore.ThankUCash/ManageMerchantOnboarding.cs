//==================================================================================
// FileName: ManageMerchantOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageMerchantOnboarding
    {
        FrameworkUserOnboarding _FrameworkUserOnboarding;
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveMerchant(OUserOnboarding.Merchant.Request _Request)
        {
            _FrameworkUserOnboarding = new FrameworkUserOnboarding();
            return _FrameworkUserOnboarding.SaveMerchant(_Request);
        }
    }
}
