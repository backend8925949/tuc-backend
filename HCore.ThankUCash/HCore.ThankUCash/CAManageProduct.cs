//==================================================================================
// FileName: CAManageProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.CAProductManager;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    /// <summary>
    /// Class CAManageProduct.
    /// </summary>
    public class CAManageProduct
    {
        FrameworkProductManager _FramworkProductManager;
        /// <summary>
        /// Description: Gets the product balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductBalance(OCAProduct.BalanceRequest _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.GetProductBalance(_Request);
        }
        /// <summary>
        /// Description: Credits the product account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditProductAccount(OCAProduct.CreditBalanceRequest _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.CreditProductAccount(_Request);
        }
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveProduct(OCAProduct.Manage _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.SaveProduct(_Request);
        }
        /// <summary>
        /// Description: Creates the quick gift card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreateQuickGiftCard(OCAProduct.CreditBalanceRequest _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.CreateQuickGiftCard(_Request);
        }
        /// <summary>
        /// Description: Creates the gift card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreateGiftCard(OCAProduct.CreditBalanceRequest _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.CreateGiftCard(_Request);
        }
        /// <summary>
        /// Description: Creates the gift card code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreateGiftCardCode(OCAProduct.CreditBalanceRequest _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.CreateGiftCardCode(_Request);
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProduct(OList.Request _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.GetProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductCode(OList.Request _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.GetProductCode(_Request);
        }
        /// <summary>
        /// Description: Gets the reward product list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardProductList(OList.Request _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.GetRewardProductList(_Request);
        }
        /// <summary>
        /// Description: Updates the prduct reward percentage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePrductRewardPercentage(OCAProduct.RewardProductUpdate _Request)
        {
            _FramworkProductManager = new FrameworkProductManager();
            return _FramworkProductManager.UpdatePrductRewardPercentage(_Request);
        }
        
    }
}
