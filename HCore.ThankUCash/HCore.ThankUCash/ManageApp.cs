//==================================================================================
// FileName: ManageApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.FrameworkPayments;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageApp
    {
        FrameworkApp _FrameworkApp;
        FrameworkPayments.FrameworkPayments _FrameworkPayments;
        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppConfiguration(OApp.OAppConfiguration.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetAppConfiguration(_Request);
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategories(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetCategories(_Request);
        }
        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetMerchant(_Request);
        }
        /// <summary>
        /// Gets the merchant categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantCategories(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetMerchantCategories(_Request);
        }
        /// <summary>
        /// Gets the stores.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStores(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetStores(_Request);
        }
        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactions(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetTransactions(_Request);
        }
        /// <summary>
        /// Gets the payments configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPaymentsConfiguration(OList.Request _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.GetPaymentsConfiguration(_Request);
        }

        /// <summary>
        /// Payment confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse PaymentConfirm(OApp.OPaymentDetails _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.PaymentConfirm(_Request);
        }
        /// <summary>
        /// Payment account verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse PaymentAccountVerify(OApp.OPaymentDetails _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.PaymentAccountVerify(_Request);
        }
        /// <summary>
        /// Payment charge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse PaymentCharge(OApp.OPaymentCharge _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.PaymentCharge(_Request);
        }

        /// <summary>
        /// Gets the biller list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBillers(OList.Request _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.GetBillers(_Request);
        }
        /// <summary>
        /// Gets the biller package list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBillerPackages(OList.Request _Request)
        {
            _FrameworkPayments = new FrameworkPayments.FrameworkPayments();
            return _FrameworkPayments.GetBillerPackages(_Request);
        }
        /// <summary>
        /// Confirms the donation.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConfirmDonation(OApp.OPaymentDetails _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.ConfirmDonation(_Request);
        }


        /// <summary>
        /// Updates the donar configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDonarConfiguration(OAppManager.RelifDetails _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.UpdateDonarConfiguration(_Request);
        }
        /// <summary>
        /// Gets the donar configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDonarConfiguration(OAppManager.RelifDetails _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetDonarConfiguration(_Request);
        }
        /// <summary>
        /// Updates the receiver configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReceiverConfiguration(OAppManager.RelifDetails _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.UpdateReceiverConfiguration(_Request);
        }
        /// <summary>
        /// Gets the receiver configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReceiverConfiguration(OAppManager.RelifDetails _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetReceiverConfiguration(_Request);
        }

        /// <summary>
        /// Saves the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.SaveDonationParameter(_Request);
        }
        /// <summary>
        /// Updates the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.UpdateDonationParameter(_Request);
        }
        /// <summary>
        /// Deletes the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteDonationParameter(OAppManager.DonationParameter.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.DeleteDonationParameter(_Request);
        }
        /// <summary>
        /// Gets the donation parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDonationParameter(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetDonationParameter(_Request);
        }

        /// <summary>
        /// Ges the donations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GeDonations(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GeDonations(_Request);
        }
        /// <summary>
        /// Gets the receivers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReceivers(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetReceivers(_Request);
        }



    }
}
