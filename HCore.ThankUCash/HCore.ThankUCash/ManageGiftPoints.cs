//==================================================================================
// FileName: ManageGiftPoints.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageGiftPoints
    {
        FrameworkGiftPoints _FrameworkGiftPoints;
        /// <summary>
        /// Gets the gift points balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGiftPointsBalance(OGiftPoints.BalanceRequest _Request)
        {
            _FrameworkGiftPoints = new FrameworkGiftPoints();
            return _FrameworkGiftPoints.GetGiftPointsBalance(_Request);
        }

        /// <summary>
        /// Credits the gift points.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditGiftPoints(OGiftPoints.CreditBalanceRequest _Request)
        {
            _FrameworkGiftPoints = new FrameworkGiftPoints();
            return _FrameworkGiftPoints.CreditGiftPoints(_Request);
        }
        /// <summary>
        /// Credits the gift points to customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditGiftPointsToCustomer(OGiftPoints.CreditBalanceRequest _Request)
        {
            _FrameworkGiftPoints = new FrameworkGiftPoints();
            return _FrameworkGiftPoints.CreditGiftPointsToCustomer(_Request);
        }
        /// <summary>
        /// Credits the gift points to customer web.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditGiftPointsToCustomerWeb(OGiftPoints.CreditBalanceRequest _Request)
        {
            _FrameworkGiftPoints = new FrameworkGiftPoints();
            return _FrameworkGiftPoints.CreditGiftPointsToCustomerWeb(_Request);
        }
        
    }
}
