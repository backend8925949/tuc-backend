//==================================================================================
// FileName: ManageProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageProduct
    {
        FrameworkProduct _FrameworkProduct;
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.SaveProduct(_Request);
        }
        /// <summary>
        /// Description: Saves the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveProductBatch(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.SaveProductBatch(_Request);
        }
        /// <summary>
        /// Description: Updates the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.UpdateProduct(_Request);
        }
        /// <summary>
        /// Description: Updates the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProductBatch(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.UpdateProductBatch(_Request);
        }
        /// <summary>
        /// Description: Updates the product status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProductStatus(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.UpdateProductStatus(_Request);
        }
        /// <summary>
        /// Description: Updates the product batch status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProductBatchStatus(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.UpdateProductBatchStatus(_Request);
        }
        /// <summary>
        /// Description: Deletes the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.DeleteProduct(_Request);
        }
        /// <summary>
        /// Description: Deletes the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteProductBatch(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.DeleteProductBatch(_Request);
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductCode(OProductCode.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductCode(_Request);
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProduct(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product batch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductBatch(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductBatch(_Request);
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductCode(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductCode(_Request);
        }
        /// <summary>
        /// Description: Gets the batch codes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBatchCodes(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetBatchCodes(_Request);
        }
        /// <summary>
        /// Description: Redeems the code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RedeemCode(OProductCode.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.RedeemCode(_Request);
        }
    }
}
