//==================================================================================
// FileName: ManageRm.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageRm
    {
        FrameworkRmManager _FrameworkRmManager;
        /// <summary>
        /// Description: Assigns the store to rm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse AssignStoreToRm(ORmManager.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.AssignStoreToRm(_Request);
        }
        /// <summary>
        /// Description: Revokes the rm store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RevokeRmStore(ORmManager.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.RevokeRmStore(_Request);
        }
        /// <summary>
        /// Description: Gets the rm terminals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRmTerminals(OList.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.GetRmTerminals(_Request);
        }
        /// <summary>
        /// Description: Gets the rm stores.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRmStores(OList.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.GetRmStores(_Request);
        }
    }
}
