//==================================================================================
// FileName: ManageLiveTerminals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Framework;
using HCore.ThankUCash.Object;

namespace HCore.ThankUCash
{
    public class ManageLiveTerminals
    {
        FrameworkLiveTerminals _FrameworkLiveTerminals;
        /// <summary>
        /// Description: Gets the live terminals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLiveTerminals(OList.Request _Request)
        {
            _FrameworkLiveTerminals = new FrameworkLiveTerminals();
            return _FrameworkLiveTerminals.GetLiveTerminals(_Request);
        }
        /// <summary>
        /// Description: Updates the terminal transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateTerminalTransaction(OLiveTerminal.ActivityUpdate _Request)
        {
            _FrameworkLiveTerminals = new FrameworkLiveTerminals();
            return _FrameworkLiveTerminals.UpdateTerminalTransaction(_Request);
        }

    }
}
