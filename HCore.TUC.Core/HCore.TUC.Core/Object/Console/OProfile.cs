//==================================================================================
// FileName: OProfile.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Object.Console
{
    public class OProfile
    {
        public class Details
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }

                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? ContactNumber { get; set; }

                public long? TimeZoneReferenceId { get; set; }
                public string? TimeZoneReferenceKey { get; set; }
                public string? TimeZoneId { get; set; }
                public string? TimeZoneName { get; set; }
                public string? TimeZoneValue { get; set; }
                public string? TimeZoneDescription { get; set; }

                public DateTime? DateOfBirth { get; set; }

                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }

                public long? IconStorageId { get; set; }
                public string? IconUrl { get; set; }

                public string? RoleName { get; set; }
                public string? RoleKey { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public string?  Address { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? HostEnvironment { get; set; }
            }

        }

        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? OwnerKey { get; set; }

            public string? DisplayName { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? StatusCode { get; set; }
            public string? GenderCode { get; set; }
            public string? RoleKey { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public OStorageContent IconContent { get; set; }
            public string? Address { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public ResponseStatus Status { get; set; }
            public string? StatusCode { get; set; }
            public string? Message { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
        public class Profile
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }


        public class Password
        {

            public class Request
            {
                public string? OldPassword { get; set; }
                public string? NewPassword { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class ResetRequest
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }


    }
}
