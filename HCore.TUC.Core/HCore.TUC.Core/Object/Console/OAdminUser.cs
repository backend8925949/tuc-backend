//==================================================================================
// FileName: OAdminUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.TUC.Core.Object.Console.OSystemAdministration.Feature;

namespace HCore.TUC.Core.Object.Console
{
    public class OSystemAdministration
    {

        public class PushNotification
        {
            public class Request
            {
                public string? Title { get; set; }
                public string? Message { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class SystemVerification
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? MobileNumber { get; set; }
                public string? RequestIpAddress { get; set; }
                public DateTime RequestDate { get; set; }
                public string? VerifyIpAddress { get; set; }
                public int? VerifyAttemptCount { get; set; }
                public DateTime? VerifyDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public long Attempts { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                //public string? TypeCode { get; set; }
                //public string? TypeName { get; set; }
                //public string? CountryIsd { get; set; }
                public string? MobileNumber { get; set; }
                //public string? EmailAddress { get; set; }

                public string? AccessKey { get; set; }
                public string? AccessCode { get; set; }
                public string? AccessCodeStart { get; set; }


                //public string? EmailMessage { get; set; }
                //public string? MobileMessage { get; set; }


                public DateTime? ExpiaryDate { get; set; }
                public DateTime? VerifyDate { get; set; }


                public string? RequestIpAddress { get; set; }
                public double? RequestLatitude { get; set; }
                public double? RequestLongitude { get; set; }
                public string? RequestLocation { get; set; }
                public int? VerifyAttemptCount { get; set; }
                public string? VerifyIpAddress { get; set; }
                public double? VerifyLatitude { get; set; }
                public double? VerifyLongitude { get; set; }
                public string? VerifyAddress { get; set; }
                public string? ExternalReferenceKey { get; set; }



                public DateTime RequestDate { get; set; }

                //public long Status { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class AppSlider
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? StatusCode { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public OStorageContent IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? IconUrl { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
            }
        }

        public class Category
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? StatusCode { get; set; }
                public OStorageContent IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }

        public class ExceptionLog
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public long? TypeId { get; set; }
                public string? Reference { get; set; }
                public string? ReferenceKey { get; set; }
                //public string? TypeCode { get; set; }
                //public string? TypeName { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                //public string? Comment { get; set; }
                public string? UserReferenceContent { get; set; }
                public string? Data { get; set; }
                public DateTime? CreateDate { get; set; }
                //public string? CreatedByKey { get; set; }
                //public string? CreatedByDisplayName { get; set; }
                //public string? CreatedByIconUrl { get; set; }
                //public DateTime? ModifyDate { get; set; }
                //public string? ModifyByKey { get; set; }
                //public string? ModifyByDisplayName { get; set; }
                //public string? ModifyByIconUrl { get; set; }

                //public int StatusId { get; set; }
                //public string? StatusCode { get; set; }
                //public string? StatusName { get; set; }

            }
            public class List
            {
                public long ReferenceId { get; set; }
                public long? TypeId { get; set; }
                public string? Name { get; set; }
                public string? Description { get; set; }
                public DateTime? CreateDate { get; set; }
            }
        }
        public class ResponseCode
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Message { get; set; }
                public string? Description { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? Message { get; set; }
                public string? Description { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }
        public class Configuration
        {

            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public string? Description { get; set; }
                public string? Value { get; set; }
                public string? DataTypeCode { get; set; }
                public string? DataTypeName { get; set; }

                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public string? Value { get; set; }
                public string? Description { get; set; }
                public string? DataTypeCode { get; set; }
                public string? DataTypeName { get; set; }

                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }


        }
        public class ConfigurationValue
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? Value { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class AccountConfiguration
        {
            public class Request
            {
                public string? ConfigurationKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? Value { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class BulkRequest
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
                public List<Config> Configurations { get; set; }
            }

            public class Config
            {
                public string? ConfigurationKey { get; set; }
                public string? Value { get; set; }
            }
        }
        public class ConfigurationHistory
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? Value { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }


        public class AuthFeatureCategory
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public List<AuthFeature> Features { get; set; }
        }
        public class AuthFeature
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public List<AuthFeatureOption> Permissions { get; set; }
        }
        public class AuthFeatureOption
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public string? ActionTypeCode { get; set; }
            public string? ActionTypeName { get; set; }
            public bool IsSelected { get; set; }
        }
        public class Feature
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public long StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusSystemName { get; set; }
                public string? ActionTypeCode { get; set; }
                public string? ActionTypeName { get; set; }
            }
            public class FeatureList
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public bool? isEdit { get; set; }
                public bool? isView { get; set; }
                public bool? isDelete { get; set; }
                public bool? isAdd { get; set; }
                public bool? isTab { get; set; }
                public List<FeatureList> Child { get; set; }

            }
            public class RoleDetails
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? StatusCode { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public List<FeatureList> featureLists { get; set; }

            }

        }
        public class Department
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
            }

        }
        public class App
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? IpAddress { get; set; }
                public string? StatusCode { get; set; }

                public sbyte LogRequest { get; set; } = 0;
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? IconUrl { get; set; }
                public DateTime? LastRequestDateTime { get; set; }

                public string? ActiveVersion { get; set; }
                public long? TotalRequest { get; set; }
                public DateTime? CreateDate { get; set; }
                //public long? CreatedById { get; set; }
                //public string? CreatedByKey { get; set; }
                //public string? CreatedByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? IconUrl { get; set; }
                public DateTime? LastRequestDateTime { get; set; }

                public long? LogRequest { get; set; }

                public string? ActiveAppKey { get; set; }
                public string? ActiveAppVersionKey { get; set; }

                public string? ActiveVersion { get; set; }
                public long? TotalRequest { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }


                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }


                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class Usage
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? AppVersionName { get; set; }

                public long? ApiId { get; set; }
                public string? ApiKey { get; set; }
                public string? ApiName { get; set; }

                public string? IpAddress { get; set; }

                public DateTime? RequestTime { get; set; }
                public DateTime? ResponseTime { get; set; }
                public int ProcessingTime { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }


        public class Role
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? DepartmentKey { get; set; }
                public string? StatusCode { get; set; }
                public List<FeatureList> Features { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Feature
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }

                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }

                public long Features { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }

        public class RoleFeature
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long RoleId { get; set; }
                public string? RoleKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? Description { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
            }
        }

        public class SmsNotification
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? SourceName { get; set; }
                public string? MobileNumber { get; set; }
                public string? Message { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? StatusName { get; set; }
                public string? StatusCode { get; set; }
                public DateTime? SendDate { get; set; }
                public DateTime? DeliveryDate { get; set; }
                public double? Amount { get; set; }
                public DateTime? CreateDate { get; set; }

            }
        }
    }
    public class OAdminUser
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }

            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? TimeZoneId { get; set; }
            public string? TimeZoneKey { get; set; }
            public string? TimeZoneName { get; set; }
            public string? TimeZoneSystemName { get; set; }
            public string? TimeZoneValue { get; set; }
            public string? TimeZoneDescription { get; set; }

            public string? GenderName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? IconUrl { get; set; }
            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
            public long? OwnerId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public DateTime? LastActivityDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class Details
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }

                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public DateTime? DateOfBirth { get; set; }

                public string? UserName { get; set; }

                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }

                public string? IconUrl { get; set; }

                public string? RoleName { get; set; }
                public string? RoleKey { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public string? Address { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public int? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }

                public long? TimeZoneId { get; set; }
                public string? TimeZoneKey { get; set; }
                public string? TimeZoneName { get; set; }
                public string? TimeZoneSystemName { get; set; }
                public string? TimeZoneValue { get; set; }
                public string? TimeZoneDescription { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }

                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? OwnerKey { get; set; }
            public string? DisplayName { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
            public string? RoleKey { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string? Address { get; set; }
            public string? CountryKey { get; set; }
            public string? TimeZoneKey { get; set; }
            public OStorageContent IconContent { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public ResponseStatus Status { get; set; }
            public string? StatusCode { get; set; }
            public string? Message { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
        //public class Password
        //{
        //    public class Request
        //    {
        //        public string? OldPassword { get; set; }
        //        public string? NewPassword { get; set; }
        //        public OUserReference? UserReference { get; set; }
        //    }
        //    public class ResetRequest
        //    {
        //        public long ReferenceId { get; set; }
        //        public string? ReferenceKey { get; set; }
        //        public OUserReference? UserReference { get; set; }
        //    }
        //}
        public class Session
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                //public string? UserAccountKey { get; set; }
                //public string? UserAccountDisplayName { get; set; }

                //public string? UserAccountDeviceKey { get; set; }
                //public string? UserAccountDeviceName { get; set; }

                //public string? OsKey { get; set; }
                //public string? OsName { get; set; }

                //public string? AppKey { get; set; }
                public string? AppName { get; set; }

                //public string? AppVersionKey { get; set; }
                public string? AppVersionName { get; set; }

                public DateTime LastActivityDate { get; set; }
                public DateTime LoginDate { get; set; }
                public DateTime? LogoutDate { get; set; }

                public string? IpAddress { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public string? Location { get; set; }

                public string? BrowserName { get; set; }
                public string? BrowserVersion { get; set; }

                //public long Status { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? IconUrl { get; set; }

            }
        }


        public class AuthUpdate
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? UserName { get; set; }

                public string? OldPassword { get; set; }
                public string? NewPassword { get; set; }

                public string? OldPin { get; set; }
                public string? NewPin { get; set; }

                public OUserReference? UserReference { get; set; }
            }

            public class LogoRequest
            {
                public string? Panel { get; set; }
                public OStorageContent ImageContent { get; set; }
                public OUserReference? UserReference { get; set; }

            }
            public class Response
            {
                public string? UserName { get; set; }
                public string? NewPassword { get; set; }
                public string? NewPin { get; set; }
            }

            public class LogoResponse
            {
                public string? ImageUrl { get; set; }
                public string? UploadedBy { get; set; }
                public DateTime? UploadDate { get; set; }
                public OUserReference? UserReference { get; set; }

            }
        }

    }
}
