//==================================================================================
// FileName: OAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
namespace HCore.TUC.Core.Object.Console
{
    public class OAccounts
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? AccountTypeCode { get; set; }
            public DateTime CreateDate { get; set; }
        }





        public class Category
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }

        public class Cashier
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Blocked { get; set; }
            }
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class List
            {

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CashierId { get; set; }

                public string? Name { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? EmployeeId { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }

                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? IconUrl { get; set; }

                public DateTime? LastActivityDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public double SaleAmount { get; set; }
                public long? Transactions { get; set; }
                public double? MainBalance { get; set; }
            }

            public class ListDownload
            {

                public long ReferenceId { get; set; }
                public string? CashierId { get; set; }
                public string? Name { get; set; }
                public string? GenderName { get; set; }
                public string? EmployeeId { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }


                public string? Merchant { get; set; }
                public string? Store { get; set; }
                public string? StoreAddress { get; set; }
                public long? Transactions { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CashierId { get; set; }

                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? EmployeeId { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }

                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? IconUrl { get; set; }


                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public OAddress Address { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }

        public class Partners
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public string? RmDisplayName { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? RelationshipManager { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? Description { get; set; }
                public string? UserName { get; set; }
                public DateTime? StartDate { get; set; }
                public string? IconUrl { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }

                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? ReferralCode { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }
        public class Acquirers
        {
            public class Overview
            {
                public long TotalMerchants { get; set; }
                public long ReferredMerchants { get; set; }
                public long TerminalMerchants { get; set; }
                public long Stores { get; set; }
                public long Terminals { get; set; }
            }

            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }

                public string? RmDisplayName { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? RelationshipManager { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? Description { get; set; }
                public string? UserName { get; set; }
                public DateTime? StartDate { get; set; }
                public string? IconUrl { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public Overview Overview { get; set; }

                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? ReferralCode { get; set; }
                public List<ProgramDetails> ProgramDetails { get; set; }
            }

            public class ProgramDetails {
                public long ProgramId { get; set; }
                public string? ProgramReferenceKey { get; set; }
            }
        }
        public class Ptsp
        {

            public class Overview
            {
                public long TotalMerchants { get; set; }
                public long ReferredMerchants { get; set; }
                public long TerminalMerchants { get; set; }
                public long Stores { get; set; }
                public long Terminals { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }

                public string? RmDisplayName { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? RelationshipManager { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? Description { get; set; }
                public string? UserName { get; set; }
                public DateTime? StartDate { get; set; }
                public string? IconUrl { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public Overview Overview { get; set; }

                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? ReferralCode { get; set; }
            }

        }
        public class Pssp
        {
            public class Overview
            {
                public long TotalMerchants { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public string? RmDisplayName { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? RelationshipManager { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? Description { get; set; }
                public string? UserName { get; set; }
                public DateTime? StartDate { get; set; }
                public string? IconUrl { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public Overview Overview { get; set; }

                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? ReferralCode { get; set; }
            }

        }
        public class Merchant
        {

            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Inactive { get; set; }
                public long Suspended { get; set; }
                public long Blocked { get; set; }
            }
            public class DetailsOverview
            {
                public long Stores { get; set; }
                public long Terminals { get; set; }
                public long Cashiers { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? IconUrl { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }
                public long? Stores { get; set; }

                public string? PrimaryCategoryKey { get; set; }
                public string? PrimaryCategoryName { get; set; }

                public string? RmDisplayName { get; set; }
                public string? SubscriptionName { get; set; }
                public double? MainBalance { get; set; }
                public double? RewardPercentage { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }

                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? MerchantType { get; set; }
                public long MerchantTypeId { get; set; }
            }

            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? Referrer { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }
                public long? Stores { get; set; }

                public string? RelationshipManager { get; set; }
                public string? ActiveSubscription { get; set; }
                public double? WalletBalance { get; set; }
                public double? RewardPercentage { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusName { get; set; }
                public string? MerchantType { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? Description { get; set; }
                public string? UserName { get; set; }
                public DateTime? StartDate { get; set; }
                public string? IconUrl { get; set; }
                public OAccounts.Category Categories { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public DetailsOverview Overview { get; set; }

                public Subscription Subscription { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? ReferralCode { get; set; }
                public string? MerchantType { get; set; }
                public long MerchantTypeId { get; set; }

            }

            public class Subscription
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public double? Amount { get; set; }
            }
        }
        public class Store
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Inactive { get; set; }
                public long Suspended { get; set; }
                public long Blocked { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public int MerchantStatusId { get; set; }
                public string? MerchantStatusCode { get; set; }
                public string? MerchantStatusName { get; set; }
                public double? MerchantRewardPecentage { get; set; }

                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? IconUrl { get; set; }

                public double? Latitude { get; set; }
                public double? Longitude { get; set; }


                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }

                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }

                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }

                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }
                public long? Cashiers { get; set; }

                public string? PrimaryCategoryKey { get; set; }
                public string? PrimaryCategoryName { get; set; }

                public string? RmDisplayName { get; set; }
                public DateTime? LastActivityDate { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? MobileNumber { get; set; }
            }
            public class ListDownload
            {
                public long ReferenceId { get; set; }
                public string? Merchant { get; set; }
                public string? StoreName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }
                public long? Cashiers { get; set; }
                public string? PrimaryCategory { get; set; }
                public string? RelationshipManager { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? Status { get; set; }
            }





            public class Location
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }

                public double? Latitude { get; set; }
                public double? Longitude { get; set; }

                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? IconUrl { get; set; }

                public long? ActiveTerminals { get; set; }
                public long? TotalTerminals { get; set; }
                public long? Cashiers { get; set; }

                public string? RmDisplayName { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Terminal
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Idle { get; set; }
                public long Dead { get; set; }
                public long Unused { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                //public string? TypeCode { get; set; }
                //public string? TypeName { get; set; }
                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }

                public long? ProviderReferenceId { get; set; }
                public string? ProviderReferenceKey { get; set; }
                public string? ProviderDisplayName { get; set; }

                public long? AcquirerReferenceId { get; set; }
                public string? AcquirerReferenceKey { get; set; }
                public string? AcquirerDisplayName { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }

                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }


                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public string? IconUrl { get; set; }

                public string? RmDisplayName { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Customer
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }
                public int? OwnerAccountTypeId { get; set; }
                public string? OwnerAccounttypeCode { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderName { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? IconUrl { get; set; }
                public double? MainBalance { get; set; }
                public long? Transactions { get; set; }

                public string? SubscriptionKey { get; set; }
                public string? SubscriptionName { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public string? AppRegistrationDate { get; set; }
                public DateTime? LastActivityDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public long? TotalTransaction { get; set; }
                public double? TotalInvoiceAmount { get; set; }
                public long? TucPlusRewardTransaction { get; set; }
                public double? TucPlusRewardInvoiceAmount { get; set; }


                public string? RegistrationSourceCode { get; set; }
                public string? RegistrationSourceName { get; set; }

                public double? TucPlusRewardAmount { get; set; }
                public double? TucPlusConvinenceCharge { get; set; }
                public long? TucPlusRewardClaimTransaction { get; set; }
                public double? TucPlusRewardClaimInvoiceAmount { get; set; }
                public double? TucPlusRewardClaimAmount { get; set; }
                public long? RedeemTransaction { get; set; }
                public double? RedeemInvoiceAmount { get; set; }
                public double? RedeemAmount { get; set; }
                public double? TucPlusBalance { get; set; }


                public long? TucRewardTransaction { get; set; }
                public double? TucRewardInvoiceAmount { get; set; }
                public double? TucRewardAmount { get; set; }
            }

            public class    ListDownload
            {
                public long ReferenceId { get; set; }

                public string? ReferredBy { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public string? DateOfBirth { get; set; }


                public double? MainBalance { get; set; }
                //public long? Transactions { get; set; }
                //public string? SubscriptionKey { get; set; }
                //public string? SubscriptionName { get; set; }
                public string? LastTransactionDate { get; set; }
                public string? AppRegistrationDate { get; set; }
                public DateTime? RegisteredOn { get; set; }

                //public long? TotalTransaction { get; set; }
                //public double? TotalInvoiceAmount { get; set; }
                //public long? TucPlusRewardTransaction { get; set; }
                //public double? TucPlusRewardInvoiceAmount { get; set; }

                //public double? TucPlusRewardAmount { get; set; }
                //public double? TucPlusConvinenceCharge { get; set; }
                //public long? TucPlusRewardClaimTransaction { get; set; }
                //public double? TucPlusRewardClaimInvoiceAmount { get; set; }
                //public double? TucPlusRewardClaimAmount { get; set; }
                //public long? RedeemTransaction { get; set; }
                //public double? RedeemInvoiceAmount { get; set; }
                //public double? RedeemAmount { get; set; }
                //public double? TucPlusBalance { get; set; }

                //public long? TucRewardTransaction { get; set; }
                //public double? TucRewardInvoiceAmount { get; set; }
                //public double? TucRewardAmount { get; set; }
                public string? Status { get; set; }
            }


        }

    }
}
