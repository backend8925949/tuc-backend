//==================================================================================
// FileName: OMerchantOnBoarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Console
{
    public class OMerchantOnBoarding
    {
        public class EmailVerificationRequest
        {
            public string? Reference { get; set; }
            public string? Host { get; set; }
            public string? Source { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class MobileVerificationRequest
        {
            public string? Reference { get; set; }
            public string? Source { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? EmailAddress { get; set; }
            public short? IsEmailVerified { get; set; }
            public DateTime? EmailVerificationDate { get; set; }
            public string? EmailCode { get; set; }
            public string? DisplayName { get; set; }
            public string? Categories { get; set; }
            public long? CountryId { get; set; }
            public string? MobileNumber { get; set; }
            public short? IsMobileVrified { get; set; }
            public DateTime? MobileVerificationDate { get; set; }
            public string? MobileVerificationToken { get; set; }
            public string? Address { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
