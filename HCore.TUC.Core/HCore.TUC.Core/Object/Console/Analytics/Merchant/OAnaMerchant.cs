//==================================================================================
// FileName: OAnaMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Console.Analytics.Merchant
{
    public class OAnaMerchant
    {
        public class Request
        {
            public int Offset { get; set; }
            public int Limit { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class StatusCount
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Inactive { get; set; }
            public long Blocked { get; set; }
        }
        public class ActivityCount
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }
            public long Days { get; set; }
        }


        public class SaleRange
        {
            public string? Title { get; set; }
            public double MinimumRange { get; set; }
            public double MaximumRange { get; set; }
            public long Count { get; set; }
        }

        public class Account
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public long Count { get; set; }
            public double Amount { get; set; }
            public double InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double Transactions { get; set; }
            public DateTime? CreateDate { get; set; }
        }
    }
}
