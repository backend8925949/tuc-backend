//==================================================================================
// FileName: OAnaCustom.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Console.Analytics.Customer
{
    public class OAnaCustom
    {
        public  class Request
        {
            public DateTime BaseDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class CustomerCount
        {
            public long DailyActiveCustomers { get; set; }
            public long WeeklyActiveCustomers { get; set; }
            public long MonthlyActiveCustomers { get; set; }
            public long TotalCustomers { get; set; }
            public long ThisMonthNewCustomers { get; set; }
            public long ThisMonthLostCustomers { get; set; }
            public long ThisMonthIdleCustomers { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Balance
        {
            public double AverageRevenue { get; set; }
            public double AverageWalletBalance { get; set; }
        }
    }
}
