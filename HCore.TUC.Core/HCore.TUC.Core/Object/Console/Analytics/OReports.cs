﻿using System;
namespace HCore.TUC.Core.Object.Console.Analytics
{
    public class OReports
    {
        public class SalesReport
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public long? TotalStores { get; set; }
            public List<Stores> Stores { get; set; }

            public long? CardTransactions { get; set; }
            public double? CardTransactionsAmount { get; set; }
            public long? CashTransactions { get; set; }
            public double? CashTransactionsAmount { get; set; }
            public long? SuccessfullTransactions { get; set; }
            public double? SuccessfullTransactionsAmount { get; set; }
            public long? FailedTransactions { get; set; }
            public double? FailedTransactionsAmount { get; set; }
        }

        public class RewardReport
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public long? TotalStores { get; set; }
            public List<Stores> Stores { get; set; }

            public long? RewardTransactions { get; set; }
            public double? RewardTransactionsAmount { get; set; }
            public double? CustomerRewardAmount { get; set; }
            public double? TUCCommission { get; set; }
        }

        public class RedeemReport
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public long? TotalStores { get; set; }
            public List<Stores> Stores { get; set; }

            public long? RedeemTransactions { get; set; }
            public double? RedeemTransactionsAmount { get; set; }
            public double? CustomerRedeemAmount { get; set; }
        }

        public class RewardClaimReport
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public long? TotalStores { get; set; }
            public List<Stores> Stores { get; set; }

            public long? TucplusRewardTransactions { get; set; }
            public double? TucplusRewardTransactionsAmount { get; set; }
            public double? RewardClaimAmount { get; set; }
        }

        public class Stores
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }

            public long? CardTransactions { get; set; }
            public double? CardTransactionsAmount { get; set; }

            public long? CashTransactions { get; set; }
            public double? CashTransactionsAmount { get; set; }

            public long? SuccessfullTransactions { get; set; }
            public double? SuccessfullTransactionsAmount { get; set; }

            public long? FailedTransactions { get; set; }
            public double? FailedTransactionsAmount { get; set; }

            public long? RedeemTransactions { get; set; }
            public double? RedeemTransactionsAmount { get; set; }
            public double? CustomerRedeemAmount { get; set; }

            public long? RewardTransactions { get; set; }
            public double? RewardTransactionsAmount { get; set; }
            public double? CustomerRewardAmount { get; set; }
            public double? TUCCommission { get; set; }

            public long? TucplusRewardTransactions { get; set; }
            public double? TucplusRewardTransactionsAmount { get; set; }
            public double? RewardClaimAmount { get; set; }
        }

        public class DownloadReports
        {
            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public List<Stores> Stores { get; set; }

            public long? CardTransactions { get; set; }
            public double? CardTransactionsAmount { get; set; }

            public long? CashTransactions { get; set; }
            public double? CashTransactionsAmount { get; set; }

            public long? SuccessfullTransactions { get; set; }
            public double? SuccessfullTransactionsAmount { get; set; }

            public long? FailedTransactions { get; set; }
            public double? FailedTransactionsAmount { get; set; }

            public long? RedeemTransactions { get; set; }
            public double? RedeemTransactionsAmount { get; set; }
            public double? CustomerRedeemAmount { get; set; }

            public long? RewardTransactions { get; set; }
            public double? RewardTransactionsAmount { get; set; }
            public double? CustomerRewardAmount { get; set; }
            public double? TUCCommission { get; set; }

            public long? TucplusRewardTransactions { get; set; }
            public double? TucplusRewardTransactionsAmount { get; set; }
            public double? RewardClaimAmount { get; set; }
        }
    }
}

