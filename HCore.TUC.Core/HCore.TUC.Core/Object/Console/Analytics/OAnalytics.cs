//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Console.Analytics
{


    public class OAnalytics
    {

        public class Overview
        {
            public long Stores { get; set; }
            public long Terminals { get; set; }
            public long Cashiers { get; set; }
            public long Subaccounts { get; set; }
            public bool IsTransactionPresent { get; set; }
            public double? Balance { get; set; }
            public double RewardPercentage { get; set; }

            public string? SubscriptionName { get; set; }
        }
        public class Request
        {
            public string? Type { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long StoreReferenceId { get; set; }
            public long CustomerReferenceId { get; set; }
            public string? CustomerReferenceIdKey { get; set; }
            public string? StoreReferenceKey { get; set; }

            public long CashierReferenceId { get; set; }
            public string? CashierReferenceKey { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool AmountDistribution { get; set; } = false;
            public OUserReference? UserReference { get; set; }


            public  int Offset { get; set; }
            public int Limit { get; set; } = 5;
        }
        public class SalesOverview
        {
            public long ActiveMerchants { get; set; }
            public long TotalMerchants { get; set; }
            public long Customers { get; set; }
            public long Transactions { get; set; }
            public double InvoiceAmount { get; set; }
            public long CardTransaction { get; set; }
            public double CardInvoiceAmount { get; set; }
            public long CashTransaction { get; set; }
            public double CashInvoiceAmount { get; set; }
        }
        public class CustomerOverview
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long Repeat { get; set; }
            public double AverageInvoiceAmount { get; set; }
            public double AverageVisit { get; set; }
        }
        public class Loyalty
        {

            public long? NewCustomers { get; set; }
            public long? RepeatingCustomers { get; set; }
            public long? VisitsByRepeatingCustomers { get; set; }

            public double? NewCustomerInvoiceAmount { get; set; }
            public double? RepeatingCustomerInvoiceAmount { get; set; }

            public long? Transaction { get; set; }
            public long? TransactionCustomer { get; set; }
            public double? TransactionInvoiceAmount { get; set; }

            public long? RewardTransaction { get; set; }
            public double? RewardInvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? RewardCommissionAmount { get; set; }

            public long? TucRewardTransaction { get; set; }
            public double? TucRewardInvoiceAmount { get; set; }
            public double? TucRewardAmount { get; set; }
            public double? TucRewardCommissionAmount { get; set; }

            public long? TucPlusRewardTransaction { get; set; }
            public double? TucPlusRewardInvoiceAmount { get; set; }
            public double? TucPlusRewardAmount { get; set; }
            public double? TucPlusRewardCommissionAmount { get; set; }

            public long? TucPlusRewardClaimTransaction { get; set; }
            public double? TucPlusRewardClaimInvoiceAmount { get; set; }
            public double? TucPlusRewardClaimAmount { get; set; }


            public long? RedeemTransaction { get; set; }
            public double? RedeemInvoiceAmount { get; set; }
            public double? RedeemAmount { get; set; }
        }
        public class Sale
        {
            public string? TerminalId { get; set; }
            public long? Hour { get; set; }
            public string? WeekDay { get; set; }
            public string? Date { get; set; }
            public string? MonthName { get; set; }
            public long? Year { get; set; }
            public long? Month { get; set; }
            public long? TotalCustomer { get; set; }
            public long? TotalTransaction { get; set; }
            public double? TotalInvoiceAmount { get; set; }

            public string? StoreDisplayName { get; set; }
            public string? StoreAddress { get; set; }

            public DateTime? LastTransactionDate { get; set; }
        }
        public class SalesMetrics
        {
            public string? Title { get; set; }
            public int Hour { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public DateTime? Date { get; set; }
            public long? TotalTransaction { get; set; }
            public long? TotalCustomer { get; set; }
            public double? TotalInvoiceAmount { get; set; }
            public long NewCustomer { get; set; }
            public double NewCustomerInvoiceAmount { get; set; }
            public long RepeatingCustomer { get; set; }
            public long VisitsByRepeatingCustomers { get; set; }
            public double RepeatingCustomerInvoiceAmount { get; set; }
            public long Total { get; set; }
            public double? RewardAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? CommissionAmount { get; set; }

        }
    
    
        public class Response
        {
            public long? TotalTransaction { get; set; }
            public long? TotalCustomer { get; set; }
            public double? TotalAmount { get; set; }
            public double? TotalInvoiceAmount { get; set; }
            public List<Data> Data { get; set; }
        }
        public class Data
        {
            public string? Title { get; set; }
            public long? TotalTransaction { get; set; }
            public long? TotalCustomer { get; set; }
            public double? TotalAmount { get; set; }
            public double? TotalInvoiceAmount { get; set; }
        }
    }
    
    public class OCustomerAnalytics
    {
        public class Request
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public Count Counts { get; set; }
            public List<User> TopSpenders { get; set; }
            public List<User> TopVisitors { get; set; }
            public List<Range> AgeRange { get; set; }
            public List<Range> GenderRange { get; set; }
            public List<Range> SpendRange { get; set; }
            public List<Range> PaymentRange { get; set; }
        }
        public class Count
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long Tuc { get; set; }
            public long NonTuc { get; set; }
            public long Active { get; set; }
        }
        public class ActivityStatusCount
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }
            public AppDownloadStatus AppDownloads{get;set;}
        }
        public class ReferrerCount
        {
            public long Total { get; set; }
            public long Tuc { get; set; }
            public long NonTuc { get; set; }

            public long Customers { get; set; }
            public long Merchants { get; set; }
            public long Partners { get; set; }

        }
        public class StatusCount
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Suspended { get; set; }
            public long Blocked { get; set; }
            public long AppUsers { get; set; }
            public long NonAppUsers { get; set; }
            public AppDownloadStatus AppDownloads{get;set;}
            public ThankUCashCustomersStatus ThankUCashCustomers { get;set;}
            public MerchantCustomersStatus MerchantCustomers { get;set;}

            //public List<AppDownloadStatus> AppDownloads { get; set; }
            //public List<ThankUCashCustomersStatus> ThankUCashCustomers { get; set; }
            //public List<MerchantCustomersStatus> MerchantCustomers { get; set; }
        }
        

        public class AppDownloadStatus
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }
        }
        public class ThankUCashCustomersStatus
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }
        }
        public class MerchantCustomersStatus
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }
        }
        public class SaleRange
        {
            public string? Title { get; set; }
            public double MinimumRange { get; set; }
            public double MaximumRange { get; set; }
            public long Count { get; set; }
        }
        public class PaymentMode
        {
            public long CardUsers { get; set; }
            public long CashUsers { get; set; }
            public long CardTransactions { get; set; }
            public long CashTransactions { get; set; }
        }

        public class Category
        {
            public string? Name { get; set; }
            public long Count { get; set; }
        }

        public class User
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? IconUrl { get; set; }
            public long Visits { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
        }
        public class Range
        {
            public string? Title { get; set; }
            public long Count { get; set; }
            public int Percentage { get; set; }
            public long Visits { get; set; }
            public long Male { get; set; }
            public long Female { get; set; }
            public double InvoiceAmount { get; set; }
        }
        public class Account
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public long Count { get; set; }
            public double Amount { get; set; }
            public double? RewardAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public double Transactions { get; set; }
            public DateTime? CreateDate { get; set; }
        }
    }

    partial class OMerchantAnalytics
    {

        public class Response
        {
            public List<Account> NewMerchants { get; set; }
            public List<Account> LowBalancewMerchants { get; set; }
            public List<Terminal> NewTerminals { get; set; }
            public Count Counts { get; set; }
        }
        public class Count
        {
            //public long? ActiveMerchants { get; set; }
            //public long? ActiveCustomers { get; set; }
            //public long? ActiveAppUsers { get; set; }
            public  double? CommissionAmount { get; set; }
            public long? Transactions { get; set; }
            public long? RewardTransactions { get; set; }
            public double? RewardAmount { get; set; }
            public long? RedeemTransactions { get; set; }
            public double? RedeemAmount { get; set; }
        }

        public class Account
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public double? Balance { get; set; }
            public DateTime Date { get; set; }
        }

        public class Terminal
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? TerminalId { get; set; }
            public string? Address { get; set; }

            public string? ProviderDisplayName { get; set; }
            public string? AcquirerDisplayName { get; set; }

            public string? IconUrl { get; set; }
            public double? LastBalance { get; set; }
            public DateTime? CreateDate { get; set; }
        }
    }

}
