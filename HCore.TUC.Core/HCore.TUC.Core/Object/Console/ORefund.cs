//==================================================================================
// FileName: ORefund.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Console
{
  public  class ORefund
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public string? Pin { get; set; }
            public string? ReferenceNumber { get; set; }
            public double Amount { get; set; }
            public string? Comment { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }
            public string? EmailAddress { get; set; }
            public double Amount { get; set; }
            public string? Comment { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? AccountIconUrl { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
}
