﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Console.Analytics
{
    public class OCustomerOverview
    {
        public class Request
        {
            public int Offset { get; set; }
            public int Limit { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public long AccountId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Categories
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public double? Amount { get; set; }
            public DateTime? CreateDate { get; set; }
        }

        public class Locations
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public long Count { get; set; }
        }
        public class Transactions
        {
            public long CardUsers { get; set; }
            public long CashUsers { get; set; }
            public long CardTransactions { get; set; }
            public long CashTransactions { get; set; }
        }

        public class TransactionsOverview
        {
            public long? RewardTransactions { get; set; }
            public double? RewardsAmount { get; set; }

            public long? RedeemTransactions { get; set; }
            public double? RedeemAmount { get; set; }

            public long? RewardClaimTransactions { get; set; }
            public double? RewardClaimAmount { get; set; }
        }
    }
}

