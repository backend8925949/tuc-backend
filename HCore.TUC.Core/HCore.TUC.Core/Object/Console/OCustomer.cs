//==================================================================================
// FileName: OCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Console
{
    public class OCustomer
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string ReferenceKey { get; set; }
            public string DisplayName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailAddress { get; set; }
            public string GenderCode { get; set; }
            public string ReferralCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string? Address { get; set; }
            public OStorageContent IconContent { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class PointPurchaseHistory
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? TransactionDate { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? TotalAmount { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? PaymentMethodCode { get; set; }
            public string? PaymentMethodName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
        }

        public class Overview
        {
            public long Total { get; set; }
            public double Amount { get; set; }
        }
    }
}
