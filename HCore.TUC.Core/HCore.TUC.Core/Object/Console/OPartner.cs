//==================================================================================
// FileName: OPartner.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Console
{
    public class OAccount
    {
        public class ManageStatus
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? StatusCode { get; set; }
                public string? Comment { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
    }
    public class OPartner
    {
        public class Request
        {
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? ReferralCode { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? StartDate { get; set; }
            public OStorageContent IconContent { get; set; }
            public string? Address { get; set; }
            public OAddress AddressComponent { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
    }
    public class OAcquirer
    {
        public class Request
        {
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? ReferralCode { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? StartDate { get; set; }
            public OStorageContent IconContent { get; set; }
             public string? Address { get; set; }              public OAddress AddressComponent { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
    }
    public class OPssp
    {
        public class Request
        {
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? SecondaryEmailAddress { get; set; }
            public string? ReferralCode { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? StartDate { get; set; }
            public OStorageContent IconContent { get; set; }
             public string? Address { get; set; }              public OAddress AddressComponent { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
    }
    public class OPtsp
    {
        public class Request
        {
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? ReferralCode { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? StartDate { get; set; }
            public OStorageContent IconContent { get; set; }
             public string? Address { get; set; }              public OAddress AddressComponent { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
    }
    public class OMerchant
    {
        public class Request
        {
            public long OwnerId { get; set; }
            public string? OwnerKey { get; set; }

            public long AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }

            public string? AccountOperationTypeCode { get; set; }

            public string? DisplayName { get; set; }
            public string? BusinessOwnerName { get; set; }
            public string? CompanyName { get; set; }
            public string? ContactNumber { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? ReferralCode { get; set; }

            public string? UserName { get; set; }
            public string? Password { get; set; }

            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }

            public long BranchId { get; set; }
            public string? BranchKey { get; set; }

            public long RmId { get; set; }
            public string? RmKey { get; set; }
            public DateTime? StartDate { get; set; }
            public Configuration Configurations { get; set; }
             public string? Address { get; set; }              public OAddress AddressComponent { get; set; }

            public ContactPerson ContactPerson { get; set; }
            public bool IsDealMerchant { get; set; }
            public List<Store> Stores { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
            public List<Category> Categories { get; set; }
        }
        public class Configuration
        {
            public double RewardPercentage { get; set; }
            public string? RewardDeductionTypeCode { get; set; }
        }
        public class Store
        {
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AccountOperationTypeCode { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Description { get; set; }
            public DateTime? StartDate { get; set; }
             public string? Address { get; set; }              public OAddress AddressComponent { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public List<Terminal> Terminals { get; set; }
            public List<Category> Categories { get; set; }
        }
        public class Terminal
        {
            public string? TerminalId { get; set; }
            public string? SerialNumber { get; set; }
            public long ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public long AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
        }

        public class TopupHistory
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? TransactionDate { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? TotalAmount { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? PaymentMethodCode { get; set; }
            public string? PaymentMethodName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }



            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public long? ModeId { get; set; }
            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public long? TypeId { get; set; }
            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }
        }

    }
}
