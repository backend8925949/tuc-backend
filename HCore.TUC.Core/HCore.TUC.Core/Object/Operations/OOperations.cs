//==================================================================================
// FileName: OOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Operations
{
   public class OOperations
    {
        public class Category
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }

        public class Download
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }
            public string? Url { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

    public class OCoralPay
    {
        public class ReqestHeader
        {
            public string? UserName { get; set; }
            public string? Password { get; set; }
        }
        public class RequestDetails
        {
            public string? TerminalId { get; set; }
            public string? Channel { get; set; }
            public double Amount { get; set; }
            public string? MerchantId { get; set; }
            public string? TransactionType { get; set; }
            public string? SubMerchantName { get; set; }
            public string? TraceID { get; set; }
        }
        public class OPaymentInitializeResponse
        {
            public OResponseHeader ResponseHeader { get; set; }
            public OResponseDetails ResponseDetails { get; set; }
        }
        public class OResponseHeader
        {
            public string? ResponseCode { get; set; }
            public string? ResponseMessage { get; set; }
        }
        public class OResponseDetails
        {
            public string? Reference { get; set; }
            public double Amount { get; set; }
            public string? TransactionID { get; set; }
            public string? TraceID { get; set; }
        }
        public class OPaymentDetails
        {
            public string? responseCode { get; set; }
            public string? responsemessage { get; set; }
            public string? reference { get; set; }
            public double amount { get; set; }
            public string? terminalId { get; set; }
            public string? merchantId { get; set; }
            public string? retrievalReference { get; set; }
            public string? institutionCode { get; set; }
            public string? shortName { get; set; }
            public string? customer_mobile { get; set; }
            public string? SubMerchantName { get; set; }
            public string? TransactionID { get; set; }
            public string? UserID { get; set; }
            public string? TraceID { get; set; }
        }
    }


}
