//==================================================================================
// FileName: OMerchantCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.Operations
{
    public class OMerchantCategory
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public int RootCategoryId { get; set; }
            public string? RootCategoryKey { get; set; }
            public double Commission { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int? RootCategoryId { get; set; }
            public string? RootCategoryKey { get; set; }
            public string? RootCategoryName { get; set; }
            public string? RootCategoryDescription { get; set; }
            public string? RootCategoryIconUrl { get; set; }
            public string? RootCategoryPosterUrl { get; set; }
            public double? RootCategoryCommission { get; set; }

            public int? ParentCateforyId { get; set; }
            public string? ParentCategoryKey { get; set; }
            public string? ParentCategoryName { get; set; }
            public double? ParentCategoryCommission { get; set; }

            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public double? Commission { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModiFyByDisplayName { get; set; }

            public DateTime? SyncTime { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? RootCategoryId { get; set; }
            public string? RootCategoryKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public double? Commission { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModiFyByDisplayName { get; set; }
            public DateTime? SyncTime { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
