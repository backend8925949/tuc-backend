//==================================================================================
// FileName: OStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using HCore.Operations.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Operations
{
    public class OStorageManager
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? IconUrl { get; set; }
            public long? IsPublic { get; set; }
            public DateTime CreateDate { get; set; }
        }

        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long IsPublic { get; set; }
            public OStorageContent StorageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
