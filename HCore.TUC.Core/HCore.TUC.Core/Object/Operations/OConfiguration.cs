//==================================================================================
// FileName: OConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;

namespace HCore.Operation.Object
{
    public class OCoreConfiguration
    {
        public class Save
        {
            public class Request
            {
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public string? Description { get; set; }
                public string? Value { get; set; }
                public string? ValueHelperCode { get; set; }
                public string? DataTypeCode { get; set; }
                public string? AccountTypeCode { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? Value { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? DataTypeCode { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? StatusCode { get; set; }
            public string? Comment { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public int DataTypeId { get; set; }
            public string? DataTypeCode { get; set; }
            public string? Value { get; set; }

            public int? ValueHelperId { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? ValueHelperName { get; set; }

            public int? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Details
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? Value { get; set; }

            public int? ValueHelperId { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? ValueHelperName { get; set; }


            public int? DataTypeId { get; set; }
            public string? DataTypeCode { get; set; }



            public int? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreConfigurationHistory
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Value { get; set; }
            public int? ValueTypeId { get; set; }
            public string? ValueTypeCode { get; set; }
            public string? ValueTypeName { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }

            public string? Comment { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OAccountConfiguration
    {
        public class Save
        {
            public class Request
            {
                public int ConfigurationId { get; set; }
                public string? ConfigurationKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? Value { get; set; }
                public string? ValueHelperCode { get; set; }

                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Value { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? StatusCode { get; set; }
            public string? Comment { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public int? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }


            public int ConfigurationId { get; set; }
            public string? ConfigurationKey { get; set; }
            public string? ConfigurationName { get; set; }
            public string? ConfigurationSystemName { get; set; }

            public string? DefaultValue { get; set; }
            public int? DefaultValueHelperId { get; set; }
            public string? DefaultValueHelperCode { get; set; }
            public string? DefaultValueHelperName { get; set; }

            public string? Value { get; set; }
            public int? ValueHelperId { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? ValueHelperName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            //public int ReferenceId { get; set; }
            //public string? ReferenceKey { get; set; }

            //public string? Value { get; set; }
            //public int ValueHelperId { get; set; }
            //public int ConfigurationId { get; set; }
            //public int AccountId { get; set; }

            //public DateTime? CreateDate { get; set; }

            //public string? StatusCode { get; set; }
            //public string? StatusName { get; set; }
            //public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public int? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }


            public int? ConfigurationId { get; set; }
            public string? ConfigurationKey { get; set; }
            public string? ConfigurationName { get; set; }
            public string? ConfigurationSystemName { get; set; }

            public string? DefaultValue { get; set; }
            public int? DefaultValueHelperId { get; set; }
            public string? DefaultValueHelperCode { get; set; }
            public string? DefaultValueHelperName { get; set; }

            public string? Value { get; set; }
            public int? ValueHelperId { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? ValueHelperName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

    public class OAccountConfigurationHistory
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Value { get; set; }
            public int? ValueHelperId { get; set; }
            public string? ValueHelperCode { get; set; }
            public string? ValueHelperName { get; set; }

            public string? Comment { get; set; }


            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

}
