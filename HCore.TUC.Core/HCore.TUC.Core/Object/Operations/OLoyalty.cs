﻿using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.Operations
{
    public class OLoyalty
    {
        public class WakanowSendEmailRequest { 
            public string? TemplateId { get; set; }
            public string? TemplateName { get; set; }
            public string? EmailAddress { get; set; }
            public string? DisplayName { get; set; }
            public string? Month { get; set; }
            public string? MobileNumber { get; set; }
            public string? AccountCode { get; set; }
            public  List<HCUAccountTransaction>? CustomerTransactions{get;set;}
        }
        public class AppCustomerSendEmailRequest
        {          
            public string? EmailAddress { get; set; }
            public string? UserDisplayName { get; set; }
            public string? Month { get; set; }
            public double RewardPoints { get; set; }
            public double Balance { get; set; }
            public double RedeemPoints { get; set; }
            public string? MerchantStore { get; set; }
            public string? MobileNumber { get; set; }
            public double custBalance { get; set; }
            public DateTime EndDate { get; set; }
            public int StatusId { get; set; }
            public List<HCUAccountTransaction>? CustomerTransactions { get; set; }
        }
    }
}
