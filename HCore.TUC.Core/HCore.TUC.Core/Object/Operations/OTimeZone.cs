//==================================================================================
// FileName: OTimeZone.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.Core.Object.Operations
{
	public class OTimeZone
	{
		public long ReferenceId { get; set; }
		public string? ReferenceKey { get; set; }
		public long? CountryId { get; set; }
		public string? CountryKey { get; set; }
		public string? CountryName { get; set; }
		public string? CountrySystemName { get; set; }
		public string? Name { get; set; }
		public string? SystemName { get; set; }
		public string? Value { get; set; }
		public string? Description { get; set; }
		public int TypeId { get; set; }
		public string? TypeCode { get; set; }
		public string? TypeName { get; set; }
		public DateTime? CreateDate { get; set; }
		public long? CreatedById { get; set; }
		public string? CreatedByDisplayName { get; set; }
		public int StatusId { get; set; }
		public string? StatusCode { get; set; }
		public string? StatusName { get; set; }
	}
}

