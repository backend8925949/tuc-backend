//==================================================================================
// FileName: OBank.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{

    public class OBank
    {

        public class BankCode
        {
            public string? Name { get; set; }
            public string? TName { get; set; }
            public string? SystemName { get; set; }
            public string? Code { get; set; }
            public string? LongCode { get; set; }
            public string? Gateway { get; set; }
            public bool PayWithBank { get; set; }
            public bool Active { get; set; }
            public bool IsDeleted { get; set; }
            public string? Country { get; set; }
            public string? Currency { get; set; }
            public string? Type { get; set; }
            public long ReferenceId { get; set; }
        }
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? AccountNumber { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
            public string? BankId { get; set; }
            public string? BankReferenceCode { get; set; }
            public string? BankReferenceKey { get; set; }
            public string? BvnNumber { get; set; }
            public string? Comment { get; set; }
            public bool IsPrimaryAccount { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class QuickSave
        {
            public long Id { get; set; }
            public long UserId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountNumber { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? AccountNumber { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
            public long? BankId { get; set; }
            public string? BankReferenceCode { get; set; }
            public string? BankReferenceKey { get; set; }
            public string? BvnNumber { get; set; }
            public string? Comment { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public sbyte? IsPrimary { get; set; }
            public bool IsPrimaryAccount { get; set; }

            public DateTime? CreateDate { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? AccountNumber { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
            public long? BankId { get; set; }
            public string? BankReferenceCode { get; set; }
            public string? BankReferenceKey { get; set; }
            public string? BvnNumber { get; set; }
            public string? Comment { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public sbyte? IsPrimary { get; set; }
            public bool IsPrimaryAccount { get; set; }

            public DateTime? CreateDate { get; set; }
        }
        public class ListDownload
        {
            public string? Name { get; set; }
            public string? AccountNumber { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
            public string? BvnNumber { get; set; }
            public DateTime? CreatedOn { get; set; }
            public string? StatusName { get; set; }
        }



    }

}
