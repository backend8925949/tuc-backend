//==================================================================================
// FileName: OAppPromotion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{
    public class OAppPromotion
    {
        public class Image
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Save
        {
            public class Request
            {
                public string? NavigationType { get; set; }
                public string? NavigateUrl { get; set; }
                public string? NavigationData { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public string? ImageReference { get; set; }
                public OUserReference? UserReference { get; set; }
                public OStorageContent ImageContent { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? NavigationType { get; set; }
            public string? NavigateUrl { get; set; }
            public string? NavigationData { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? StatusCode { get; set; }
            public string? ImageReference { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public int AppId { get; set; }
            public long? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryCode { get; set; }
            public string? CountryName { get; set; }
            public string? NavigationType { get; set; }
            public string? NavigateUrl { get; set; }
            public string? ImageUrl { get; set; }
            public string? NavigationData { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OStorageContent ImageContent { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public int AppId { get; set; }
            public long? CountryId { get; set; }
            public string? CountryCode { get; set; }
            public string? CountryName { get; set; }
            public string? NavigationType { get; set; }
            public string? NavigateUrl { get; set; }
            public string? ImageUrl { get; set; }
            public string? NavigationData { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Slider
        {
            public string? Name { get; set; }
            public string? SystemName { get; set; }
        }
    }
}
