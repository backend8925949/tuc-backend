//==================================================================================
// FileName: OCashout.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{
    public class OCashOut
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? BankAccountName { get; set; }
            public string? BankName { get; set; }
            public string? BankAccountNumber { get; set; }
            public string? CardNumber { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? SystemComment { get; set; }
            public string? TransactionType { get; set; }
            public long TransactionTypeId { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? WalletType { get; set; }
            public string? SentFrom { get; set; }
            public string? Description { get; set; }
            public string? MobileNumber { get; set; }
            public string? Account { get; set; }
            public long StatusId { get; set; }
            public string? StoreName { get; set; }
            public DateTime TransactionDate { get; set; }
        }

        public class RewardList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public double Amount { get; set; }
            public DateTime TransactionDate { get; set; }
            public string? PaymentReference { get; set; }
            public string? PaymentMethodCode { get; set; }
            public string? PaymentMethodName { get; set; }
            public long? CreatedByReferenceId { get; set; }
            public string? CreatedByReferenceKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? TransactionType { get; set; }
            public long TransactionTypeId { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? WalletType { get; set; }
            public string? SentFrom { get; set; }
            public string? Description { get; set; }
            public string? MobileNumber { get; set; }
            public long RedeemFrom { get; set; }
            public string? Account { get; set; }
            public string? StoreName { get; set; }
        }

        public class ListDownload
        {
            public string? Date { get; set; }
            public string? Time { get; set; }
            public string? StoreName { get; set; }
            public string? Amount { get; set; }
            public string? TransactionType { get; set; }
            public string? TransactionID { get; set; }
        }

        public class Configuration
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public double TransferBalance { get; set; }
                public double MinimumTransferBalance { get; set; }
                public double MinimumTransferAmount { get; set; }
                public double MaximumTransferAmount { get; set; }
                public double Charge { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public List<OBank.List>? Banks { get; set; }
            }

            public class SummarizeResponse
            {

                public Response? Deals { get; set; }
                public Response? Loyalty { get; set; }
                public Response? BNPL { get; set; }
                public List<OBank.List>? Banks { get; set; }
            }
        }
        public class Initialize
        {
            public class Request
            {

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long BankId { get; set; }
                public string? BankKey { get; set; }
                public double Amount { get; set; }
                public int RedeemFrom { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public List<BanksList>? Banks { get; set; }
                public List<Card>? Cards { get; set; }
            }
            public class Card
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? CardBrand { get; set; } // Name
                public string? CardBin { get; set; } // SystemName
                public string? CardLast4 { get; set; } // Value
                public string? ExpMonth { get; set; } //  SubValue
                public string? ExpYear { get; set; }  //Description
            }


            public class BanksList
            {
                public string? SystemName { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public string? LocalUrl { get; set; }
                public string? BankCode { get; set; }
                public string? Code { get; set; }
                public string? DialCode { get; set; }
            }
        }
        public class Verify
        {
            public class Request
            {
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Charge
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? AuthMode { get; set; }
                public string? AuthPin { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Cancel
        {
            public class Request
            {
                //public double Amount { get; set; }
                //public double Charge { get; set; }
                //public double TotalAmount { get; set; }
                //public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }


        //public class List
        //{
        //    public long ReferenceId { get; set; }
        //    public string? ReferenceKey { get; set; }
        //    public DateTime? TransactionDate { get; set; }

        //    public double? Amount { get; set; }
        //    public double? Charge { get; set; }
        //    public double? TotalAmount { get; set; }

        //    public string? ReferenceNumber { get; set; }

        //    public string? PaymentMethodCode { get; set; }
        //    public string? PaymentMethodName { get; set; }

        //    public string? StatusCode { get; set; }
        //    public string? StatusName { get; set; }

        //    public long? AccountId { get; set; }
        //    public string? AccountKey { get; set; }
        //    public string? AccountDisplayName { get; set; }
        //    public string? AccountMobileNumber { get; set; }
        //    public string? AccountIconUrl { get; set; }
        //}
    }

    public class OCashOutOperation
    {
        public class Overview
        {
            public long Transactions { get; set; }
            public long Customers { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double CommissionAmount { get; set; }
            public double TotalAmount { get; set; }
            public long Total { get; set; }
            public double? WalletBalance { get; set; }
            public long? TotalApprovedCashouts { get; set; }
            public double? ApprovedAmount { get; set; }
            public long? TotalRejectedCashouts { get; set; }
            public double? RejectedAmount { get; set; }
            public long? TotalPendingCashouts { get; set; }
            public double? PendingAmount { get; set; }
        }
        public class CashOutOverview
        {
            public double? WalletBalance { get; set; }
            public long? TotalApprovedCashouts { get; set; }
            public double? ApprovedAmount { get; set; }
            public long? TotalRejectedCashouts { get; set; }
            public double? RejectedAmount { get; set; }
            public long? TotalPendingCashouts { get; set; }
            public double? PendingAmount { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? BankAccountName { get; set; }
            public string? BankName { get; set; }
            public string? BankAccountNumber { get; set; }
            public string? EmailAddress { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountIconUrl { get; set; }

            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public double CommissionAmount { get; set; }
            public long? RedeemFromId { get; set; }
            public string? RedeemFromName { get; set; }
            public long StatusId { get; set; }
            public string? SystemComment { get; set; }

            public DateTime? CreateDate { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? BankAccountName { get; set; }
            public string? BankName { get; set; }
            public string? BankAccountNumber { get; set; }
            public string? AccountTypeCode { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
            public string? EmailAddress { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? SystemComment { get; set; }

            public int? SourceId { get; set; }
            public string? SourceName { get; set; }
            public string? SourceCode { get; set; }
            public string? SourceSystemName { get; set; }

            public DateTime CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public string? UserComment { get; set; }

            internal long StatusId { get; set; }
            public long? RedeemFromId { get; set; }
            public string? RedeemFromName { get; set; }
        }

    }

}
