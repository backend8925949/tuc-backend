//==================================================================================
// FileName: OSubscription.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;


namespace HCore.TUC.Core.Object.Operations
{
    public class OSubscription
    {
        public class Update
        {
            public List<OFeatures> Features { get; set; }
            public List<OUserAccountRolePermission> Permissions { get; set; }
        }
        public class OFeatures
        {
            public string? SystemName { get; set; }
            public string? Name { get; set; }
        }
        public class OUserAccountRolePermission
        {
            internal long PermissionId { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public bool IsDefault { get; set; }
            public bool IsAccessPinRequired { get; set; }
            public bool IsPasswordRequired { get; set; }

            public double? MinimumValue { get; set; }
            public double? MaximumValue { get; set; }
            public long? MinimumLimit { get; set; }
            public long? MaximumLimit { get; set; }
        }
        public class SubscriptionUpdate
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long SubscriptionId { get; set; }
                public string? SubscriptionKey { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class MonthlySubscriptionRequest {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long AccountId { get; set; }
                public string? PaymentReference { get; set; }
                public long TransactionId { get; set; }
                public double TotalAmount { get; set; }
                public long NoOfStores { get; set; }
                public double AmountPerStore { get; set; }
                public List<Stores> Stores { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Stores { 
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public double Amount { get; set; }
            }
        }

        public class Subscription
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeName { get; set; }

            public string? Name { get; set; }
            public string? Description { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public string? Policy { get; set; }

            public  long Customers { get; set; }
            public List<SubscriptionFeature> Features { get; set; }
        }
        public class SubscriptionFeature
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? Name { get; set; }
            public string? ShortDescription { get; set; }
            public string? Description { get; set; }

            public long? MinimumLimit { get; set; }

            public long? MaximumLimit { get; set; }

            public double? MinimumAmount { get; set; }
            public double? MaximumAmount { get; set; }
            public string? Policy { get; set; }
        }

        public class AccountSubscription
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long SubscriptionId { get; set; }
            public string? SubscriptionKey { get; set; }


            public string? TypeName { get; set; }

            public string? Name { get; set; }
            public string? Description { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? RenewDate { get; set; }

            public string? Policy { get; set; }


            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }

            public List<AccountSubscriptionFeature> Features { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? PaymentReference { get; set; }
            public long? TransactionId { get; set; }
            public DateTime CreatedDate { get; set; }
            public List<Stores> Stores { get; set; }
            public long NoOfStores { get; set; }
        }
        public class Stores { 
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
        }
        public class AccountSubscriptionFeature
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? Name { get; set; }
            public string? ShortDescription { get; set; }
            public string? Description { get; set; }

            public long? MinimumLimit { get; set; }

            public long? MaximumLimit { get; set; }

            public double? MinimumAmount { get; set; }
            public double? MaximumAmount { get; set; }
            public string? Policy { get; set; }
        }

    }
}
