﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.Operations
{
    public class OProgram
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public long AccountId { get; set; }
            public string? ProgramTypeCode { get; set; }
            public long SubscriptionId { get; set; }
            public string? ComissionTypeCode { get; set; }
            public double Comission { get; set; }           
            public double RewardCap { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public double MaxInvoiceAmount { get; set; }
            public double MinInvoiceAmount { get; set; }
            public bool IsEnableDisable { get; set; }
            public bool IsRemove { get; set; }
            public double MaxRewards { get; set; }
            public double MaxComission { get; set; }
            public double CashierCommission { get; set; }
            public double CashierMaxReward { get; set; }
            public List<MerchantList> MerchantList { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class MerchantList { 
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? DisplayName { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public long AccountId { get; set; }
            public string? AccountDisplayName { get; set; }
            public long? SubscriptionId { get; set; }
            public string? SubscriptionName { get; set; }
            internal int ComissionTypeId { get; set; }
            internal string ComissionTypeName { get; set; }
            internal int? ProgramTypeId { get; set; }
            internal string ProgramTypeName { get; set; }
            public double Comission { get; set; }           
            public DateTime? CreateDate { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public long ProgramId { get; set; }
            public string? ProgramReferencekey { get; set; }
            public double? RewardCap { get; set; }
            public double? MaxRewards { get; set; }
            public double? MaxComission { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public List<MerchantList> MerchantList { get; set; }
            public double? MaxInvoiceAmount { get; set; }
            public double? MinInvoiceAmount { get; set; }
            public string? ProgramTypeCode { get; set; }
            public string? ComissionTypeCode { get; set; }
            public double CashierCommission { get; set; }
            public double CashierMaxReward { get; set; }
        }
    }


}
