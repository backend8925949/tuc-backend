//==================================================================================
// FileName: OCityManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.Operations
{
    public class OCityManager
    {
        public class Save
        {
            public class Request
            {
                public long StateId { get; set; }
                public string? StateKey { get; set; }
                public string? Name { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long StateId { get; set; }
            public string? StateKey { get; set; }
            public string? Name { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }


        public class ListLite
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
        }
    }



    public class OCityAreaManager
    {
        public class Save
        {
            public class Request
            {
                public long CityId { get; set; }
                public string? CityKey { get; set; }
                public string? Name { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long CityId { get; set; }
            public string? CityKey { get; set; }
            public string? Name { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            //public double Latitude { get; set; }
            //public double Longitude { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }


        public class ListLite
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
        }
    }
}
