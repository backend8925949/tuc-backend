//==================================================================================
// FileName: OPromoCode.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{
    public class OPromoCode
    {
        public class PromoCodeConditions
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public int CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }
            public int? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public double Value { get; set; }
        }

        public class PromoCodeAccounts
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long PromoCodeId { get; set; }
            public string? PromoCodeKey { get; set; }
            public string? PromoCode { get; set; }
            public double PromoCodeValue { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccoountEmailAddress { get; set; }
            public int? AccountRegistrationSourceId { get; set; }
            public string? AccountRegistrationSourceCode { get; set; }
            public string? AccountRegistratonSourceName { get; set; }
            public double Value { get; set; }
            public DateTime? UseDate { get; set; }
        }

        public class PromoCodeAccount
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long PromoCodeId { get; set; }
            public string? PromoCodeKey { get; set; }
            public string? PromoCode { get; set; }
            public double PromoCodeValue { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccoountEmailAddress { get; set; }
            public string? AccountIconUrl { get; set; }
            public double Value { get; set; }
            public DateTime? UseDate { get; set; }
        }

        public class PromoCodeOperations
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Expired { get; set; }
            }
            public class Usage
            {

                public long Used { get; set; }
                public double? UsedAmount { get; set; }
                //public long ReferenceId { get; set; }
                //public string? ReferenceKey { get; set; }
                //public long MaximumLimit { get; set; }
                //public double Budget { get; set; }
                //public long UsedCodes { get; set; }
                //public long UnUsedCodes { get; set; }
                //public OUserReference? UserReference { get; set; }
            }
            public class Save
            {
                public int CountryId { get; set; }
                public string? CountryKey { get; set; }
                public int AccountTypeId { get; set; }
                public string? AccountTypeCode { get; set; }
                public int ConditionId { get; set; }
                public string? ConditionKey { get; set; }
                public int TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public DateTime StartDate { get; set; }
                public DateTime EndDate { get; set; }
                public string? PromoCode { get; set; }
                public double PromoCodeValue { get; set; }
                public double Budget { get; set; }
                public long MaximumLimit { get; set; }
                public int MaximumLimitPerUser { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Update
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int CountryId { get; set; }
                public string? CountryKey { get; set; }
                public int AccountTypeId { get; set; }
                public string? AccountTypeCode { get; set; }
                public int ConditionId { get; set; }
                public string? ConditionKey { get; set; }
                public int TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? PromoCode { get; set; }
                public double PromoCodeValue { get; set; }
                public double Budget { get; set; }
                public long MaximumLimit { get; set; }
                public int MaximumLimitPerUser { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Promocodes
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public int AccountTypeId { get; set; }
                public string? AccountTypeCode { get; set; }
                public string? AccountTypeName { get; set; }
                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public int ConditionId { get; set; }
                public string? ConditionKey { get; set; }
                public string? ConditionName { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public DateTime StartDate { get; set; }
                public DateTime EndDate { get; set; }
                public string? PromoCode { get; set; }
                public double PromoCodeValue { get; set; }
                public double Budget { get; set; }
                public long MaximumLimit { get; set; }
                public int MaximumLimitPerUser { get; set; }
                public long TotalUsed { get; set; }
                public long? TotalAvailable { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }

            public class PromoCode
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public int AccountTypeId { get; set; }
                public string? AccountTypeCode { get; set; }
                public string? AccountTypeName { get; set; }
                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public int ConditionId { get; set; }
                public string? ConditionKey { get; set; }
                public string? ConditionName { get; set; }
                public double ConditionValue { get; set; }
                public string? ConditionDescription { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public DateTime StartDate { get; set; }
                public DateTime EndDate { get; set; }
                public string? Code { get; set; }
                public double PromoCodeValue { get; set; }
                public double Budget { get; set; }
                public long MaximumLimit { get; set; }
                public int MaximumLimitPerUser { get; set; }
                public bool PromocodeUse { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class CustomerReferrals
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public DateTime? LastActivityDate { get; set; }
            public string? IconUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
