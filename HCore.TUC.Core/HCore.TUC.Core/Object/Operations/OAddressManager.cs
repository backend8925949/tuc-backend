//==================================================================================
// FileName: OAddressManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{
    public class OAddressManager
    {
        public class Save
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }


                public bool? IsPrimary { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public int LocationTypeId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? AdditionalMobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? AddressLine1 { get; set; }
                public string? AddressLine2 { get; set; }
                public string? ZipCode { get; set; }
                public string? Landmark { get; set; }
                public long CityId { get; set; }
                public long CityAreaId { get; set; }
                public long StateId { get; set; }
                public int CountryId { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string? MapAddress { get; set; }
                public string? Instructions { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int? LocationTypeId { get; set; }
            public string? LocationTypeName { get; set; }

            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? AdditionalMobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? ZipCode { get; set; }
            public string? Landmark { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public int CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? MapAddress { get; set; }
            public string? Instructions { get; set; }
            public sbyte? IsPrimaryAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

        }
    }
}
