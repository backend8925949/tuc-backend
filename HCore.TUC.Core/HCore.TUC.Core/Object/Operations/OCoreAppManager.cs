//==================================================================================
// FileName: OCoreAppManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.Operations
{
    public class OCoreAppManager
    {
        public class Save
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? Accountkey { get; set; }
                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? IpAddress { get; set; }
                public sbyte AllowLogging { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? Accountkey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? AppKey { get; set; }
            public string? IpAddress { get; set; }
            public sbyte AllowLogging { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? Accountkey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long AccountTypeId { get; set; }
            public string? AccountTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? Accountkey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? AppKey { get; set; }
            public string? IpAddress { get; set; }
            public sbyte AllowLogging { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }

    public class OCoreAppVersionManager
    {
        public class Save
        {
            public class Request
            {
                public long AppId { get; set; }
                public string? AppKey { get; set; }
                public string? VersionId { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class UpdateStatus
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AppId { get; set; }
            public string? AppKey { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AppId { get; set; }
            public string? AppKey { get; set; }
            public string? AppName { get; set; }
            public string? VersionId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AppId { get; set; }
            public string? AppKey { get; set; }
            public string? AppName { get; set; }
            public string? AppSystemName { get; set; }
            public string? VersionId { get; set; }
            public string? PublicKey { get; set; }
            public string? PrivateKey { get; set; }
            public string? SystemPublicKey { get; set; }
            public string? SystemPrivateKey { get; set; }
            public string? Comment { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}