//==================================================================================
// FileName: OCustomerOboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Operations
{
    public class OCustomerOboarding
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }
            public string? GenderCode { get; set; }
            public string? CountryIsd { get; set; }
            public string ReferralCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public OStorageContent IconContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
