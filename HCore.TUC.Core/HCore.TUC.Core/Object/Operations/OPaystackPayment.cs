//==================================================================================
// FileName: OPaystackPayment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.Core.Object.Operations
{
    public class OPaystackPayment
    {
        public class Request
        {
            public string? eventt { get; set; }
            public Data data { get; set; }
        }
        public class Data
        {
            public string? id { get; set; }
            public string? domain { get; set; }
            public string? status { get; set; }
            public string? reference { get; set; }
            public long amount { get; set; }
            public string? message { get; set; }
            public string? gateway_response { get; set; }
            public DateTime? paid_at { get; set; }
            public DateTime? created_at { get; set; }
            public string? channel { get; set; }
            public string? currency { get; set; }
            public string? ip_address { get; set; }
            public object metadata { get; set; }
            public string? fees { get; set; }
            public Customer customer { get; set; }
            public Authorization authorization { get; set; }
        }
        public class Customer
        {
            public long id { get; set; }
            public string? first_name { get; set; }
            public string? last_name { get; set; }
            public string? email { get; set; }
            public string? customer_code { get; set; }
            public string? phone { get; set; }
            public string? metadata { get; set; }
            public string? risk_action { get; set; }
        }
        public class Authorization
        {
            public string? authorization_code { get; set; }
            public string? bin { get; set; }
            public string? last4 { get; set; }
            public string? exp_month { get; set; }
            public string? exp_year { get; set; }
            public string? channel { get; set; }
            public string? card_type { get; set; }
            public string? bank { get; set; }
            public string? brand { get; set; }
            public bool reusable { get; set; }
            public string? signature { get; set; }
            public string? account_name { get; set; }

        }
    }
}
