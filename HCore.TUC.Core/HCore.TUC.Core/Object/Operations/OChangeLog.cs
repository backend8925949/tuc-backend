//==================================================================================
// FileName: OChangeLog.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Operations
{
    public class OCoreChangeLog
    {
        public class Save
        {
            public class Request
            {
                public string? Title { get; set; }
                public string? LocationName { get; set; }
                public string? Log { get; set; }
                public string? Comment { get; set; }
                public DateTime? ReleaseDate { get; set; }
                public string? VersionName { get; set; }
                public int? StatusId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }
            public string? LocationName { get; set; }
            public string? Log { get; set; }
            public string? Comment { get; set; }
            public DateTime? ReleaseDate { get; set; }
            public string? VersionName { get; set; }
            public int? StatusId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }
            public string? LocationName { get; set; }
            public string? Log { get; set; }
            public string? Comment { get; set; }
            public DateTime? ReleaseDate { get; set; }
            public string? VersionName { get; set; }
            public long? StatusId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }
            public string? LocationName { get; set; }
            public string? Log { get; set; }
            public string? Comment { get; set; }
            public DateTime? ReleaseDate { get; set; }
            public string? VersionName { get; set; }
            public long? StatusId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
        }
    }
}
