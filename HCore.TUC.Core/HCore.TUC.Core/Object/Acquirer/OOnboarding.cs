//==================================================================================
// FileName: OOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Acquirer
{
    public class OOnboarding
    { 
        public class Merchant
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<Details> Data { get; set; }
                public OUserReference? UserReference { get; set; }

            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long OwnerId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? CityAreaName { get; set; }
                public string? CityName { get; set; }
                public string? StateName { get; set; }
                public string? CountryName { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public long? AccountId { get; set; }
                public string? CategoryName { get; set; }
                public string? ContactPersonName { get; set; }
                public string? ContactPersonMobileNumber { get; set; }
                public string? ContactPersonEmailAddress { get; set; }
                public string? ReferenceNumber { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public string? StatusMessage { get; set; }
                public double? RewardPercentage { get; set; }

            }

        }
        public class Terminal
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<Details> Data { get; set; }
                public OUserReference? UserReference { get; set; }

            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? AccountId { get; set; }
                public long? MerchantId { get; set; }
                public string? MerchantDisplayName { get; set; }
                public long? StoreId { get; set; }
                public string? StoreDisplayName { get; set; }
                public long? AcquirerId { get; set; }
                public string? AcquirerDisplayName { get; set; }
                public long? ProviderId { get; set; }
                public string? ProviderDisplayName { get; set; }
                public long? CashierId { get; set; }
                public string? CashierDisplayName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public long? ImportStatusId { get; set; }
                public string? Comment { get; set; }

            }

        }

            public class Customer
            {
                public class Request
                {
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public string? FileName { get; set; }
                    public List<Details> Data { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Details
                {
                    public long ReferenceId { get; set; }
                    public long OwnerId { get; set; }
                    public string? Name { get; set; }
                    public string? FirstName { get; set; }
                    public string? LastName { get; set; }
                    public string? MobileNumber { get; set; }
                    public string? FormattedMobileNumber { get; set; }
                    public string? EmailAddress { get; set; }
                    public string? Gender { get; set; }
                    public DateTime? DateOfBirth { get; set; }
                    public string? ReferenceNumber { get; set; }
                    public DateTime? CreateDate { get; set; }
                    public long? CreatedById { get; set; }
                    public string? CreatedByDisplayName { get; set; }
                    public DateTime? ModifyDate { get; set; }
                    public long? ModifyById { get; set; }
                    public long? StatusId { get; set; }
                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                    public string? StatusMessage { get; set; }


                }
                public class File
                {
                    public long ReferenceId { get; set; }
                    public long OwnerId { get; set; }
                    public string? Name { get; set; }
                    public long TotalRecord { get; set; }
                    public long Processing { get; set; }
                    public long Pending { get; set; }
                    public long Success { get; set; }
                    public long Error { get; set; }
                    public DateTime? CreateDate { get; set; }
                    public long? CreatedById { get; set; }
                    public string? CreatedByDisplayName { get; set; }
                    public DateTime? ModifyDate { get; set; }
                    public long? ModifyById { get; set; }
                    public long? StatusId { get; set; }
                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                    public string? StatusMessage { get; set; }

                }
            }
    }
}
