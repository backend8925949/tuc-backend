//==================================================================================
// FileName: ORmManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Acquirer
{
    public class ORmManager
    {
        public class BulkTarget
        {
            public class UpdateRequest
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Status { get; set; }
                public string? Message { get; set; }

                public long BranchId { get; set; }
                public string? BranchKey { get; set; }

                public long ManagerId { get; set; }
                public string? ManagerKey { get; set; }
                public long RmId { get; set; }
                public string? RmKey { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public double TotalTarget { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public List<Target> Targets { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public List<Target> Target { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Target
        {
            public string? Status { get; set; }
            public string? Message { get; set; }

            public long BranchId { get; set; }
            public string? BranchKey { get; set; }

            public long ManagerId { get; set; }
            public string? ManagerKey { get; set; }
            public long RmId { get; set; }
            public string? RmKey { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public double TotalTarget { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? BranchId { get; set; }
            public string? BranchKey { get; set; }

            public long ManagerId { get; set; }
            public string? ManagerKey { get; set; }

            public long RmId { get; set; }
            public string? RmKey { get; set; }

            public string? RmDisplayName { get; set; }
            public string? RmMobileNumber { get; set; }

            public double? TotalTarget { get; set; }
            public double? AchievedTarget { get; set; }
            public double? RemainingTarget { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }


            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }


            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }
    }
}
