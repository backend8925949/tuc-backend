//==================================================================================
// FileName: OAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.TUC.Core.Object.Acquirer
{
    public class OAccounts
    {
        public class Customer
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? IconUrl { get; set; }
                public DateTime CreateDate { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? IconUrl { get; set; }
                public DateTime CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Merchant
        {

            public class Onboarding
            {
                internal long ReferenceId { get; set; }
                internal string ReferenceKey { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? DisplayName { get; set; }
                internal string ReferralCode { get; set; }
                internal long OwnerId { get; set; }
                public string? Name { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }


                public double RewardPercentage { get; set; }

                public long BranchId { get; set; }
                public string? BranchKey { get; set; }

                public long RmId { get; set; }
                public string? RmKey { get; set; }
                public string? StatusCode { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public OnboardingContact ContactPerson { get; set; }
                public List<OnboardingStore> Stores { get; set; }
                public OUserReference? UserReference { get; set; }
                public long ProgramId { get; set; }
                public string? ProgramReferenceKey { get; set; }
            }
            public class OnboardingContact
            {
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
            public class OnboardingStore
            {
                internal long ReferenceId { get; set; }
                internal string ReferenceKey { get; set; }


                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public OnboardingContact ContactPerson { get; set; }
                public List<OnboardingTerminal> Terminals { get; set; }
            }
            public class OnboardingTerminal
            {
                internal long ReferenceId { get; set; }
                internal string ReferenceKey { get; set; }
                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }
                public long ProviderId { get; set; }
                public string? ProviderKey { get; set; }
            }


            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }
                public string? OwnerName { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public string? FullAddress { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }

                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }

                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }

                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }

                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }

                public string? ReferralCode { get; set; }


                public long? Stores { get; set; }
                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }
                public double? RewardPercentage { get; set; }

                public DateTime? LastActivityDate { get; set; }

                public DateTime CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public ContactDetails ContactPerson { get; set; }
                public ManagerDetails Rm { get; set; }
                public List<Category> Categories { get; set; }
            }
            public class ContactDetails
            {
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
            public class ManagerDetails
            {
                public long RmId { get; set; }
                public string? RmKey { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
            public class Category
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }

                public string? CityName { get; set; }

                public string? IconUrl { get; set; }

                public long? Stores { get; set; }
                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }

                public long? RmId { get; set; }
                public string? RmName { get; set; }
                public string? RmMobileNumber { get; set; }
                public string? RmEmailAddress { get; set; }
                public string? RmDisplayName { get; set; }

                public DateTime CreateDate { get; set; }

                public double? RewardPercentage { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public long ProgramId { get; set; }
                public string? ProgramReferenceKey { get; set; }
                public string? ProgramStatus { get; set; }
                public string? ProgramStatusCode { get; set; }
            }
        }
        public class Store
        {
            public class Request
            {

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }


                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                //public string? Address { get; set; }
                //public double Latitude { get; set; }
                //public double Longitude { get; set; }
                //public long? StateId { get; set; }
                //public long? CityId { get; set; }
                //public long? CityAreaId { get; set; }
                //public string? CountryName { get; set; }
                //public string? MapAddress { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }

                public long BranchId { get; set; }
                public string? BranchKey { get; set; }

                public long RmId { get; set; }
                public string? RmKey { get; set; }

                public string? StatusCode { get; set; }
                public ContactDetails ContactPerson { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Details
            {
                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }

                public long? BranchId { get; set; }
                public string? BranchKey { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountCode { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                //public double? Latitude { get; set; }
                //public double? Longitude { get; set; }

                //public long? CountryId { get; set; }
                //public string? CountryKey { get; set; }
                //public string? CountryName { get; set; }

                //public long? StateId { get; set; }
                //public string? StateKey { get; set; }
                //public string? StateName { get; set; }

                //public long? CityId { get; set; }
                //public string? CityKey { get; set; }
                //public string? CityName { get; set; }

                //public long? CityAreaId { get; set; }
                //public string? CityAreaKey { get; set; }
                //public string? CityAreaName { get; set; }

                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }

                public double? RewardPercentage { get; set; }

                public string? RmName { get; set; }
                public string? RmMobileNumber { get; set; }
                public string? RmEmailAddress { get; set; }

                public DateTime? LastActivityDate { get; set; }
                public DateTime CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public ContactDetails ContactPerson { get; set; }
                public ContactDetails Rm { get; set; }
                public List<Category> Categories { get; set; }
                public BranchDetails Branch { get; set; }
            }
            public class ContactDetails
            {
                public long? ReferenceId { get; set; }
                public string? Name { get; set; }
                public string? DisplayName { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
            public class Category
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
            }
            public class BranchDetails
            {
                public long BranchId { get; set; }
                public string? BranchKey { get; set; }
                public string? Name { get; set; }
                public string? DisplayName { get; set; }
                public string? BranchCode { get; set; }
            }

            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double? RewardPercentage { get; set; }
                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantEmailAddress { get; set; }
                public string? DisplayName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }

                public long? RmId { get; set; }
                public string? RmName { get; set; }
                public string? RmMobileNumber { get; set; }
                public string? RmEmailAddress { get; set; }

                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }

                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }

                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }

                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }
                public string? IconUrl { get; set; }

                public DateTime? LastTransactionDate { get; set; }

                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public double? TodaysTransactionAmount { get; set; }
                public long? Terminals { get; set; }
                public long? ActiveTerminals { get; set; }
                public string? RmDisplayName { get; set; }
                public DateTime CreateDate { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }
        public class Terminal
        {
            public class BulkRequest
            {
                public List<BulkRequestItem> Terminals { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class BulkRequestItem
            {

                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }
                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public long StoreId { get; set; }
                public string? StoreKey { get; set; }
                public long ProviderId { get; set; }
                public string? ProviderKey { get; set; }
                public long AcquirerId { get; set; }
                public string? AcquirerKey { get; set; }
                //public long BranchId { get; set; }
                //public string? BranchKey { get; set; }

                //public long RmId { get; set; }
                //public string? RmKey { get; set; }
                public long CashierId { get; set; }
                public string? CashierKey { get; set; }

                public string? TypeCode { get; set; }

            }

            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }
                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public long StoreId { get; set; }
                public string? StoreKey { get; set; }
                public long ProviderId { get; set; }
                public string? ProviderKey { get; set; }
                public long AcquirerId { get; set; }
                public string? AcquirerKey { get; set; }
                //public long BranchId { get; set; }
                //public string? BranchKey { get; set; }

                //public long RmId { get; set; }
                //public string? RmKey { get; set; }
                public long CashierId { get; set; }
                public string? CashierKey { get; set; }

                public string? TypeCode { get; set; }

                public OUserReference? UserReference { get; set; }
            }



            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }

                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }
                public string? MerchantIconUrl { get; set; }

                public long? ProviderId { get; set; }
                public string? ProviderKey { get; set; }
                public string? ProviderName { get; set; }
                public string? ProviderIconUrl { get; set; }

                public long? StoreId { get; set; }
                public string? StoreKey { get; set; }
                public string? StoreName { get; set; }
                public string? StoreAddress { get; set; }
                public double? StoreLatitude { get; set; }
                public double? StoreLongitude { get; set; }

                public double InvoiceAmount { get; set; }

                public DateTime? LastActivityDate { get; set; }

                public DateTime CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public ContactDetails ContactPerson { get; set; }
                public ContactDetails Rm { get; set; }
            }
            public class ContactDetails
            {
                public long? ReferenceId { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TerminalId { get; set; }
                public string? IdentificationNumber { get; set; }

                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }

                public long? RmId { get; set; }
                public string? RmName { get; set; }
                public string? RmMobileNumber { get; set; }
                public string? RmEmailAddress { get; set; }

                public long? StoreId { get; set; }
                public string? StoreKey { get; set; }
                public string? StoreName { get; set; }
                public string? StoreAddress { get; set; }
                public string? StoreMobileNumber { get; set; }

                public long? ProviderId { get; set; }
                public string? ProviderKey { get; set; }
                public string? ProviderName { get; set; }
                public string? ProviderIconUrl { get; set; }

                public double? StoreLatitude { get; set; }
                public double? StoreLongitude { get; set; }
                public double? TodaysTransactionAmount { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                public double InvoiceAmount { get; set; }
                public DateTime CreateDate { get; set; }

                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public ContactDetails Rm { get; set; }

            }
        }
        public class TerminalUpload
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<Terminal> Terminals { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Terminal
            {
                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }
                public long MerchantId { get; set; }
                public long StoreId { get; set; }
                public string? ProviderName { get; set; }
                public string? AcquirerName { get; set; }
                public string? CashierCode { get; set; }
                public string? Status { get; set; }
            }

        }
    }
}
