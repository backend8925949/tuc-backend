//==================================================================================
// FileName: OOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Merchant
{ 


    public class OOnboarding
    {
        public class St1
        {
           public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Password { get; set; }
                public string? Source { get; set; }
                public string? SubscriptionKey { get; set; }
                public string? Host { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class St2
        {
            public class Request
            {
                public string? Reference { get; set; }
                public string? Host { get; set; }
                public string? Source { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class St3
        {
            public class Request
            {
                public string? Token { get; set; }
                public string? Source { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class St4
        {
            public class Request
            {
                public string? Reference { get; set; }
                public string? Source { get; set; }
                public string? MobileNumber { get; set; }
                public string? Isd { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class St5
        {
            public class Request
            {
                public string? Reference { get; set; }
                public string? Source { get; set; }
                public string? Otp { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class St6
        {
            public class Request
            {
                public string? Reference { get; set; }
                public string? Source { get; set; }
                public string? DisplayName { get; set; }
                public string? ReferralCode { get; set; }
                public List<int> Categories { get; set; }
                public OAddress Address { get; set; }
                public string? SubscriptionKey { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }


        public class Customer
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? FileName { get; set; }
                public List<Details> Data { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public long OwnerId { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? FormattedMobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? ReferenceNumber { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusMessage { get; set; }

                public double? InvoiceAmount { get; set; }
                public double? RewardAmount { get; set; }

            }
            public class File
            {
                public long ReferenceId { get; set; }
                public long OwnerId { get; set; }
                public string? Name { get; set; }
                public long TotalRecord { get; set; }
                public long Processing { get; set; }
                public long Pending { get; set; }
                public long Success { get; set; }
                public long Error { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusMessage { get; set; }

            }
        }
    }



    public class OOnboardingV2
    {
        public class Profile
        {
            public class Request
            {
                public string? businessName { get; set; }
                public string? businessEmailAddress { get; set; }
                public string? name { get; set; }
                public string? password { get; set; }
                public string? source { get; set; }

                public string? countryIsd { get; set; }
                public string? mobileNumber { get; set; }

                public string? subscriptionKey { get; set; }
                public string? host { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? businessName { get; set; }
                public string? businessEmailAddress { get; set; }
                public string? name { get; set; }
                public string? password { get; set; }
                public string? source { get; set; }

                public string? countryIsd { get; set; }
                public string? mobileNumber { get; set; }

                public string? subscriptionKey { get; set; }
                public string? host { get; set; }
                public string? reference { get; set; }
            }
        }
        public class EmailChange
        {
            public class Request
            {
                public string? reference { get; set; }
                public string? businessEmailAddress { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? reference { get; set; }
                public string? businessEmailAddress { get; set; }
            }
        }
        public class EmailVerify
        {
            public class Request
            {
                public string? reference { get; set; }
                public string? businessEmailAddress { get; set; }
                public string? code { get; set; }
                public string? IsNewsLetter { get; set; }
                public string? IsPolicy { get; set; }
                public OUserReference? UserReference { get; set; }
                public bool? CellAuth { get; set; } // Mobile Number Verification required or not
            }
            public class Response
            {
                public string? reference { get; set; }
                public string? businessEmailAddress { get; set; }
            }
        }
        public class MobileChange
        {
            public class Request
            {
                public string? reference { get; set; }
                public string? mobileNumber { get; set; }
                public string? businessEmailAddress { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? reference { get; set; }
                public string? token { get; set; }
                public string? mobileNumber { get; set; }
            }
        }
        public class MobileVerify
        {
            public class Request
            {
                public string? reference { get; set; }
                public string? businessEmailAddress { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? reference { get; set; }
                public string? token { get; set; }
                public string? mobileNumber { get; set; }
            }
        }

        public class MobileVerifyConfirm
        {
            public class Request
            {
                public string? reference { get; set; }
                public string? token { get; set; }
                public string? code { get; set; }
                public string? IsNewsLetter { get; set; }
                public string? IsPolicy { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? reference { get; set; }
            }
        }
        public class CreateAccount
        {
            public class Request
            {
                public string? Reference { get; set; }
                public string? Source { get; set; }
                public string? DisplayName { get; set; }
                public string? ReferralCode { get; set; }
                public List<int> Categories { get; set; }
                public OAddress Address { get; set; }
                public string? SubscriptionKey { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }


        public class Customer
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? FileName { get; set; }
                public List<Details> Data { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public long OwnerId { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? FormattedMobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? ReferenceNumber { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusMessage { get; set; }

                public double? InvoiceAmount { get; set; }
                public double? RewardAmount { get; set; }

            }
            public class File
            {
                public long ReferenceId { get; set; }
                public long OwnerId { get; set; }
                public string? Name { get; set; }
                public long TotalRecord { get; set; }
                public long Processing { get; set; }
                public long Pending { get; set; }
                public long Success { get; set; }
                public long Error { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusMessage { get; set; }

            }
        }

        public class BusinessInformation
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? BusinessName { get; set; }
            public string? BusinessEmail { get; set; }
            public long AccountId { get; set; }
            public string? MobileNumber { get; set; }
            public string? countryIsd { get; set; }
            public Category Category { get; set; }
            public BankDetails Bankdetails { get; set; }
            public ContactPerson ContactPerson { get; set; }
            public OAddress Address { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class BankDetails
        {
            public long BankId { get; set; }
            public string? BankKey { get; set; }
            public string? AccountNumber { get; set; }
            public string? AccountName { get; set; }
            public string? BankName { get; set; }
            public string? BankCode { get; set; }
        }
        public class ContactPerson
        {
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? countryIsd { get; set; }
        }

        public class AccountType
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? AccountTypeCode { get; set; }
            public List<AccountTypeCode> AccountTypes { get; set; }
            public string? ConfigurationKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class AccountTypeCode
        {
            public string? AccountTypeCodes { get; set; }
            public string? Value { get; set; }
            public string? ConfigurationValue { get; set; }
        }

        public class MerchantDetails
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public double? RewardPercentage { get; set; }

            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? UserName { get; set; }

            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? IconUrl { get; set; }

            public OAddress AddressComponent { get; set; }
            public Category? Category { get; set; }
            public List<Configuration> Configuration { get; set; }
            public ContactPerson? ContactPerson { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
        }

        public class Category
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }

        public class Configuration
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public int? TypeId { get; set; }
            public int? SubTypeId { get; set; }
            public long? CommonId { get; set; }
            public string? Value { get; set; }
        }

        public class OnboardMerchants
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public sbyte? DocumentVerificationStatus { get; set; }
            public DateTime? DocumentVerificationStatusDate { get; set; }

            public string? Address { get; set; }
            public BankDetails Bankdetails { get; set; }
            //public List<Documents> Documents { get; set; }
            public Documents Documents { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Documents
        {
            public string? DocumentUrl { get; set; }
        }

        public class VerifyDocument
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public sbyte? VerifyDocuments { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }


}
