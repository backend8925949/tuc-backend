//==================================================================================
// FileName: OCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Merchant.Object
{

    public class OCategory
    {
        public class AllCats
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public double? Fee { get; set; }
            public List<Item> Items { get; set; }
        }

        public class Item
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public double? Fee { get; set; }
        }
        public class Save
        {
            public class Request
            {
                public int RootCategoryId { get; set; }
                public string? RootCategoryKey { get; set; }
                public double? Commission { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public int RootCategoryId { get; set; }
            public string? RootCategoryKey { get; set; }
            public double? Commission { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public DateTime? CategoryCreateDate { get; set; }
            public int? CategoryStatusId { get; set; }
            public string? CategoryStatusCode { get; set; }
            public string? CategoryStatusName { get; set; }
            public string? IconStorageUrl { get; set; }
            public string? PosterStorageUrl { get; set; }

            public double? Commission { get; set; }

            internal int? ParentCategoryId { get; set; }
            internal string Name { get; set; }
            internal string Description { get; set; }
            internal string IconUrl { get; set; }
            internal string PosterUrl { get; set; }
            internal double? Fee { get; set; }



            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public int? CategoryStatusId { get; set; }
            public string? IconStorageUrl { get; set; }

            public double? Commission { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }

}