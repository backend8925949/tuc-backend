//==================================================================================
// FileName: OAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OAccounts
    {
        public class Category
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }
        public class ContactPerson
        {
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
        }
        public class RelationshipManager
        {
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
        }
        public class Merchant
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double? RewardPercentage { get; set; }
                public string? OwnerName { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? UserName { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? IconUrl { get; set; }

                public string? AccountCode { get; set; }
                public string? WebsiteUrl { get; set; }

                public string? Description { get; set; }
                public string? ReferralCode { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public RelationshipManager Rm { get; set; }
                public List<Category> Categories { get; set; }
                public long? Stores { get; set; }
                public long? Terminals { get; set; }
                public long? Cashiers { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? NickName { get; set; }
                public string? MerchantType { get; set; }
            }
        }
        public class MerchantOnboardingV4
        {
            public class Request
            {
                public string? Reference { get; set; }
                public Business Business { get; set; }
                public Account Account { get; set; }
                public User Owner { get; set; }
                public User ContactPerson { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Business
            {
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? ContactNumber { get; set; }
                public string? AccountOperationTypeCode { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? About { get; set; }
                public List<Category> Categories { get; set; }
                public OAddress AddressComponent { get; set; }
            }

            public class Account
            {
                public string? UserName { get; set; }
                public string? Password { get; set; }
                public string? ReferralCode { get; set; }
            }
            public class User
            {
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
            }
        }
        public class MerchantOnboarding
        {
            public class MerchantDetails
            {
                //internal long ReferenceId { get; set; }
                //internal string ReferenceKey { get; set; }
                //public long AccountId { get; set; }
                //public string? AccountKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Password { get; set; }
                public string? AccountOperationTypeCode { get; set; }
                public string? WebsiteUrl { get; set; }
                public string? About { get; set; }
                public string? Description { get; set; }
                public string? ReferralCode { get; set; }
                public string? ShortCode { get; set; }
                public double RewardPercentage { get; set; } = 0;
                public int RegistrationSourceId { get; set; }
                public OAddress Address { get; set; }
                public OUserReference? UserReference { get; set; }
                public List<Category> Categories { get; set; }
            }

            public class ChangeNumber
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? MobileNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class VerifyOtp
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Token { get; set; }
                public string? AccessCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Cashier
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Blocked { get; set; }
            }
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class List
            {

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CashierId { get; set; }

                public string? Name { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? EmployeeId { get; set; }



                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? IconUrl { get; set; }

                public DateTime? LastActivityDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public long? Transactions { get; set; }
                public long? Transaction { get; set; }
                public double TotalTransAmount { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CashierId { get; set; }

                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }
                public string? EmployeeId { get; set; }

                public string? UserName { get; set; }
                public string? Password { get; set; }

                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? IconUrl { get; set; }


                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class SubAccount
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CashierId { get; set; }

                public string? UserName { get; set; }
                public string? Name { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }

                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string StoreDislayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? IconUrl { get; set; }

                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? LastActivityDate { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? GenderCode { get; set; }
                public string? GenderName { get; set; }

                public string? IconUrl { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }


                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }

                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDislayName { get; set; }
                public string? StoreAddress { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class ListDownload
            {
                public string? UserName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderName { get; set; }
                public string? RoleName { get; set; }
                public string? StoreDislayName { get; set; }
                public string? StoreAddress { get; set; }
                public string? IconUrl { get; set; }
                public DateTime? AddedOn { get; set; }
                public string? StatusName { get; set; }
                public DateTime? LastActivityDate { get; set; }
            }
        }
        public class Store
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }


            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? ManagerName { get; set; }
                public string? ManagerMobileNumber { get; set; }

                public double? Latitude { get; set; }
                public double? Longitude { get; set; }

                public string? Address { get; set; }
                public string? CityCode { get; set; }

                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryCode { get; set; }
                public string? CountryName { get; set; }

                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }

                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }

                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }

                public string? CityAreaCode { get; set; }

                public long? Cashiers { get; set; }
                public long Terminals { get; set; }

                public DateTime? CreateDate { get; set; }


                public string? StateCode { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? CpFirstName { get; set; }
                public string? CpLastName { get; set; }
                public string? CpMobileNumber { get; set; }
                public string? CpEmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? RoleName { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerName { get; set; }
                public int OwnerStatusId { get; set; }
                public string? OwnerStatusCode { get; set; }
                public double? RewardPercentage { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Username { get; set; }
                public string? Password { get; set; }

                public string? IconUrl { get; set; }
                public long Terminals { get; set; }
                public long Cashiers { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public List<Category> Categories { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public RelationshipManager Rm { get; set; }
                public string? MobileNumber { get; set; }
                public long? StoreManagerId { get; set; }
            }
        }
        public class Terminal
        {
            public class Overview
            {
                public long Total { get; set; }
                public long Active { get; set; }
                public long Idle { get; set; }
                public long Dead { get; set; }
                public long Unused { get; set; }
            }
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }


                public string? IdentificationNumber { get; set; }
                public string? TerminalId { get; set; }

                public string? SerialNumber { get; set; }


                public long? ProviderReferenceId { get; set; }
                public string? ProviderReferenceKey { get; set; }
                public string? ProviderDisplayName { get; set; }

                public string? ProviderIconUrl { get; set; }


                public long? AcquirerReferenceId { get; set; }
                public string? AcquirerReferenceKey { get; set; }
                public string? AcquirerDisplayName { get; set; }
                public string? AcquirerIconUrl { get; set; }


                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreAddress { get; set; }


                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }


                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? LastActivityDate { get; set; }
                public DateTime? LastTransactionDate { get; set; }


                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public string? TerminalId { get; set; }
                public string? TerminalIdentificationNumber { get; set; }

                public string? SerialNumber { get; set; }


                public long? ProviderReferenceId { get; set; }
                public string? ProviderReferenceKey { get; set; }
                public string? ProviderDisplayName { get; set; }

                public string? ProviderIconUrl { get; set; }


                public long? AcquierReferenceId { get; set; }
                public string? AcquirerReferenceKey { get; set; }
                public string? AcquirerDisplayName { get; set; }
                public string? AcquirerIconUrl { get; set; }


                public string? SupportContactName { get; set; }
                public string? SupportContactEmailAddress { get; set; }
                public string? SupportContactContactNumber { get; set; }


                public long? StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }

                public string? StoreDisplayName { get; set; }

                public string? StoreAddress { get; set; }
                public OAddress StoreAddressComponent { get; set; }

                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }



                public DateTime? LastActivityDate { get; set; }


                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }

        }
        public class Customer
        {
            public class Overview
            {
                public long Total { get; set; }
                public long New { get; set; }
                public long Repeating { get; set; }
                public long Lost { get; set; }
            }
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string ReferenceKey { get; set; }
                public string UserName { get; set; }
                public string DisplayName { get; set; }
                public string Name { get; set; }
                public string MiddleName { get; set; }
                public string ReferralCode { get; set; }
                public string EmailAddress { get; set; }
                public string MobileNumber { get; set; }
                public string ContactNumber { get; set; }
                public string GenderCode { get; set; }
                public string GenderName { get; set; }
                public string EmployeeId { get; set; }
                public DateTime? DateOfBirth { get; set; }

                public string? FirstName { get; set; }
                public string? LastName { get; set; }

                public string? Address { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public string? IconUrl { get; set; }

                public long? OwnerId { get; set; }
                public string? OwnerKey { get; set; }
                public string? OwnerDisplayName { get; set; }
                public string? OwnerIconUrl { get; set; }

                public long? SubOwnerId { get; set; }
                public string? SubOwnerKey { get; set; }
                public string? SubOwnerDisplayName { get; set; }

                public long? TotalTransaction { get; set; }
                public double? TotalInvoiceAmount { get; set; }
                public double? TotalBalance { get; set; }


                public long? ApplicationStatusId { get; set; }
                public string? ApplicationStatusCode { get; set; }
                public string? ApplicationStatusName { get; set; }
                public long? AccountLevelId { get; set; }
                public string? AccountLevelCode { get; set; }
                public string? AccountLevelName { get; set; }

                public long? TotalGiftPointTransaction { get; set; }
                public double? TotalGiftPointCreditAmount { get; set; }
                public double? TotalGiftPointDebitAmount { get; set; }
                public double? TotalGiftPointBalance { get; set; }

                public long? TotalRewardTransaction { get; set; }
                public double? TotalRewardAmount { get; set; }
                public double? TotalRewardInvoiceAmount { get; set; }

                public string? RegistrationSource { get; set; }

                public long? TucRewardTransaction { get; set; }
                public double? TucRewardAmount { get; set; }
                public double? TucRewardInvoiceAmount { get; set; }
                public double? TucBalance { get; set; }

                public long? TucPlusRewardTransaction { get; set; }
                public double? TucPlusRewardAmount { get; set; }
                public double? TucPlusConvinenceCharge { get; set; }
                public double? TucPlusRewardInvoiceAmount { get; set; }
                public double? TucPlusBalance { get; set; }

                public long? RedeemTransaction { get; set; }
                public double? RedeemAmount { get; set; }
                public double? RedeemInvoiceAmount { get; set; }

                public long? TucPlusRewardClaimTransaction { get; set; }
                public double? TucPlusRewardClaimAmount { get; set; }
                public double? TucPlusRewardClaimInvoiceAmount { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                //internal DateTime TLastTransactionDate { get; set; }
                public DateTime? LastLoginDate { get; set; }
                public DateTime? LastActivityDate { get; set; }
                public DateTime CreateDate { get; set; }
                public long? CreatedById { get; set; }

                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? LoyaltyTypeName { get; set; }

                public long? Merchants { get; set; }
                public long? Stores { get; set; }
                public long? Terminals { get; set; }
                public long? Cashiers { get; set; }
                public double? RewardPercentage { get; set; }

                public long? AccountOperationTypeId { get; set; }
                public string? AccountOperationTypeCode { get; set; }
                public string? AccountOperationTypeName { get; set; }


                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }

                public long? ProviderId { get; set; }
                public string? ProviderKey { get; set; }
                public string? ProviderDisplayName { get; set; }
                public string? ProviderIconUrl { get; set; }

                public long? StoreId { get; set; }
                public string? StoreKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreIconUrl { get; set; }

                public long? AcquirerId { get; set; }
                public string? AcquirerKey { get; set; }
                public string? AcquirerDisplayName { get; set; }
                public string? AcquirerIconUrl { get; set; }


                public long? Transactions { get; set; }
                public long? ActivityStatusId { get; set; }
                public string? ActivityStatusCode { get; set; }
                public string? ActivityStatusName { get; set; }

                public long? RmId { get; set; }
                public string? RmKey { get; set; }
                public long? ActvityStatusId { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
            }
            public class Download
            {
                public long CustomerId { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }

                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? Gender { get; set; }
                public DateTime? DateOfBirth { get; set; }

                public string? FirstName { get; set; }
                public string? MiddleName { get; set; }
                public string? LastName { get; set; }

                //public string? Address { get; set; }

                //public long? TotalTransaction { get; set; }
                //public double? TotalInvoiceAmount { get; set; }
                //public double? TotalBalance { get; set; }


                //public long? TotalGiftPointTransaction { get; set; }
                //public double? TotalGiftPointCreditAmount { get; set; }
                //public double? TotalGiftPointDebitAmount { get; set; }
                //public double? TotalGiftPointBalance { get; set; }

                public long? TotalRewardTransaction { get; set; }
                public double? TotalRewardAmount { get; set; }
                public double? TotalRewardInvoiceAmount { get; set; }

                public string? RegistrationSource { get; set; }

                //public long? TucRewardTransaction { get; set; }
                //public double? TucRewardAmount { get; set; }
                //public double? TucRewardInvoiceAmount { get; set; }
                //public double? TucBalance { get; set; }

                //public long? TucPlusRewardTransaction { get; set; }
                //public double? TucPlusRewardAmount { get; set; }
                //public double? TucPlusConvinenceCharge { get; set; }
                //public double? TucPlusRewardInvoiceAmount { get; set; }
                //public double? TucPlusBalance { get; set; }

                public long? TotalRedeemTransaction { get; set; }
                public double? TotalRedeemAmount { get; set; }
                public double? TotalRedeemInvoiceAmount { get; set; }

                //public long? TucPlusRewardClaimTransaction { get; set; }
                //public double? TucPlusRewardClaimAmount { get; set; }
                //public double? TucPlusRewardClaimInvoiceAmount { get; set; }

                public DateTime? LastTransactionDate { get; set; }
                //internal DateTime TLastTransactionDate { get; set; }
                //public DateTime? LastLoginDate { get; set; }
                public DateTime RegisteredOn { get; set; }
                //public long? CreatedById { get; set; }

                //public string? StatusName { get; set; }
            }


            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? GenderName { get; set; }
                public DateTime? FirstTransactionDate { get; set; }
                public DateTime? LastTransactionDate { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? IconUrl { get; set; }
                public string? WakaPointId { get; set; }
            }
        }
        public class OCoreMerchantWebPay
        {
            public class Initialize
            {
                public class Request
                {
                    public string? Reference { get; set; }
                    //public string? MerchantCode { get; set; }
                    public IniBusinessInformation Business { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class IniBusinessInformation
                {
                    public string? Name { get; set; }
                    public string? EmailAddress { get; set; }
                    public string? MobileNumber { get; set; }
                    public string? Address { get; set; }
                }
            }

            public class Confirm
            {
                public string? Reference { get; set; }
                public string? MerchantCode { get; set; }
                public BusinessInformation Business { get; set; }
                public ContactInformation Contact { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class BusinessInformation
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? Password { get; set; }
                public string? Address { get; set; }
                //public OAddress Address { get; set; }
                public double RewardPercentage { get; set; }
                public List<Category> Categories { get; set; }
            }
            public class ContactInformation
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
            }

            public class Category
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
            }
            //internal class Category
            //{
            //    internal long ReferenceId { get; set; }
            //    internal string ReferenceKey { get; set; }
            //    internal string Name { get; set; }
            //    internal string IconUrl { get; set; }
            //}
            //internal class ContactPerson
            //{
            //    internal string FirstName { get; set; }
            //    internal string LastName { get; set; }
            //    internal string MobileNumber { get; set; }
            //    internal string EmailAddress { get; set; }
            //}

            //internal class Request
            //{
            //    internal long OwnerId { get; set; }
            //    internal string OwnerKey { get; set; }

            //    internal long AcquirerId { get; set; }
            //    internal string AcquirerKey { get; set; }

            //    internal string AccountOperationTypeCode { get; set; }

            //    internal string DisplayName { get; set; }
            //    internal string CompanyName { get; set; }
            //    internal string ContactNumber { get; set; }
            //    internal string MobileNumber { get; set; }
            //    internal string EmailAddress { get; set; }
            //    internal string UserName { get; set; }
            //    internal string Password { get; set; }

            //    internal string WebsiteUrl { get; set; }
            //    internal string Description { get; set; }

            //    internal long BranchId { get; set; }
            //    internal string BranchKey { get; set; }

            //    internal long RmId { get; set; }
            //    internal string RmKey { get; set; }

            //    internal DateTime? StartDate { get; set; }

            //    internal Configuration Configurations { get; set; }
            //    internal OAddress Address { get; set; }
            //    internal ContactPerson ContactPerson { get; set; }
            //    internal List<Store> Stores { get; set; }
            //    internal string StatusCode { get; set; }
            //    internal OUserReference UserReference { get; set; }
            //    internal List<Category> Categories { get; set; }
            //}
            //internal class Configuration
            //{
            //    internal double RewardPercentage { get; set; }
            //    internal string RewardDeductionTypeCode { get; set; }
            //}
            //internal class Store
            //{
            //    internal string DisplayName { get; set; }
            //    internal string Name { get; set; }
            //    internal string ContactNumber { get; set; }
            //    internal string EmailAddress { get; set; }
            //    internal string AccountOperationTypeCode { get; set; }
            //    internal string WebsiteUrl { get; set; }
            //    internal string Description { get; set; }
            //    internal DateTime? StartDate { get; set; }
            //    internal OAddress Address { get; set; }
            //    internal ContactPerson ContactPerson { get; set; }
            //    internal List<Terminal> Terminals { get; set; }
            //    internal List<Category> Categories { get; set; }
            //}
            //internal class Terminal
            //{
            //    internal string TerminalId { get; set; }
            //    internal string SerialNumber { get; set; }
            //    internal long ProviderId { get; set; }
            //    internal string ProviderKey { get; set; }
            //    internal long AcquirerId { get; set; }
            //    internal string AcquirerKey { get; set; }
            //}
            //internal class Response
            //{
            //    internal string StatusCode { get; set; }
            //    internal string Message { get; set; }
            //    internal long ReferenceId { get; set; }
            //    internal string ReferenceKey { get; set; }
            //}
        }

    }


}
