//==================================================================================
// FileName: OOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OOperations
    {
        public class Category
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }
        public class Configuration
        {
            public class Config
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? ConfigurationKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class BulkUpdate
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<BulkUpdateItem>Items { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class BulkUpdateItem
            {
                public string? ConfigurationKey { get; set; }
                public string? Value { get; set; }
                public string? SubValue { get; set; }
                public string? HelperCode { get; set; }
                public string? Comment { get; set; }
            }

            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? ConfigurationKey { get; set; }
                public string? Value { get; set; }
                public string? SubValue { get; set; }
                public string? HelperCode { get; set; }
                public string? Comment { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? ConfigurationName { get; set; }
                public string? ConfigurationSystemName { get; set; }
                public long? ConfigurationId { get; set; }
                public string? ConfigurationKey { get; set; }
                public string? Value { get; set; }
                public string? SubValue { get; set; }
                public string? HelperCode { get; set; }
                public string? HelperName { get; set; }


                public string? Comment { get; set; }

                public DateTime? StartDate { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? EndDate { get; set; }

                public long? ModifyByReferenceId { get; set; }
                public string? ModifyByReferenceKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }


            public class LoyaltyConfiguration
            {
                public bool IsTucPlus { get; set; }
                public bool IsTucGold { get; set; }
                public string? CategoryName { get; set; }
                public Reward Reward { get;set;}
                public Redeem Redeem { get;set;}

               
            }
            public class Reward
            {
                public bool IsCustomReward { get; set; }
                public string? TucCommissionSource { get; set; }
                public double? RewardPercentage { get; set; }
                public double? UserPercentage { get; set; }
                public double? SystemPercentage { get; set; }
                public double? MerchantPercentage { get; set; }
                public string? PaymentMethod { get; set; }
            }
            public class Redeem
            {
                public string? TucCommissionSource { get; set; }
                public double? UserPercentage { get; set; }
                public double? SystemPercentage { get; set; }
                public double? MerchantPercentage { get; set; }
            }
        }
    }
}
