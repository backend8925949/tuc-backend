//==================================================================================
// FileName: OCustomersOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OCustomersOverview
    {
        public class OAnalytics
        {
            public class Request
            {
                public string? Type { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long StoreReferenceId { get; set; }
                public long CustomerReferenceId { get; set; }
                public string? CustomerReferenceIdKey { get; set; }
                public string? StoreReferenceKey { get; set; }

                public long CashierReferenceId { get; set; }
                public string? CashierReferenceKey { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public bool AmountDistribution { get; set; } = false;
                public OUserReference? UserReference { get; set; }

                public int Offset { get; set; }
                public int Limit { get; set; } = 5;
            }

            public class Response
            {
                public Count Counts { get; set; }
                public List<User> TopSpenders { get; set; }
                public List<User> TopVisitors { get; set; }
                public List<Range> AgeRange { get; set; }
                public List<Range> GenderRange { get; set; }
                public List<Range> SpendRange { get; set; }
                public List<Range> PaymentRange { get; set; }
            }

            public class Count
            {
                public long Total { get; set; }
                public long New { get; set; }
                public long Tuc { get; set; }
                public long NonTuc { get; set; }
                public long Active { get; set; }
            }

            public class User
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? MobileNumber { get; set; }
                public string? IconUrl { get; set; }
                public long Visits { get; set; }
                public double? InvoiceAmount { get; set; }
                public double? RewardAmount { get; set; }
            }

            public class Loyalty
            {

                public long? NewCustomers { get; set; } = 0;
                public long? RepeatingCustomers { get; set; } = 0;
                public long? VisitsByRepeatingCustomers { get; set; } = 0;

                public double? NewCustomerInvoiceAmount { get; set; } = 0;
                public double? RepeatingCustomerInvoiceAmount { get; set; } = 0;

                public long? Transaction { get; set; } = 0;
                public long? TransactionCustomer { get; set; } = 0;
                public double? TransactionInvoiceAmount { get; set; } = 0;

                public long? RewardTransaction { get; set; } = 0;
                public double? RewardInvoiceAmount { get; set; } = 0;
                public double? RewardAmount { get; set; } = 0;
                public double? RewardCommissionAmount { get; set; } = 0;

                public long? TucRewardTransaction { get; set; } = 0;
                public double? TucRewardInvoiceAmount { get; set; } = 0;
                public double? TucRewardAmount { get; set; } = 0;
                public double? TucRewardCommissionAmount { get; set; } = 0;

                public long? TucPlusRewardTransaction { get; set; } = 0;
                public double? TucPlusRewardInvoiceAmount { get; set; } = 0;
                public double? TucPlusRewardAmount { get; set; } = 0;
                public double? TucPlusRewardCommissionAmount { get; set; } = 0;

                public long? TucPlusRewardClaimTransaction { get; set; } = 0;
                public double? TucPlusRewardClaimInvoiceAmount { get; set; } = 0;
                public double? TucPlusRewardClaimAmount { get; set; } = 0;

                public long? RedeemTransaction { get; set; } = 0;
                public double? RedeemInvoiceAmount { get; set; } = 0;
                public double? RedeemAmount { get; set; } = 0;
            }

            public class Range
            {
                public string? Title { get; set; }
                public long Count { get; set; }
                public int Percentage { get; set; }
                public long Visits { get; set; }
                public long Male { get; set; }
                public long Female { get; set; }
                public double InvoiceAmount { get; set; }
            }

            public class RewardsOverview
            {
                public double? TotalRewards { get; set; }
                public double? PendingRewardsAmount { get; set; }
                public double? RedeemAmount { get; set; }
                public double? UnClaimedRewardsAmount { get; set; }
            }
        }
    }
}

