//==================================================================================
// FileName: OStoreBankCollection.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OStoreBankCollection
    {
        public class Request
        {
            public double ReceivedAmount { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? StartDate { get; set; }
            internal DateTime EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public DateTime Date { get; set; }
            public List<OStore> Stores { get; set; }

            public long? Terminals { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? NibsAmount { get; set; }
            public double? ExpectedAmount { get; set; }
            public double? ReceivedAmount { get; set; }
            public double? AmountDifference { get; set; }
        }
        public class OStore
        {
            internal long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? StoreName { get; set; }
            public long? Acquirers { get; set; }
            public long? Terminals { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? NibsAmount { get; set; }
            public double? ExpectedAmount { get; set; }
            public double? ReceivedAmount { get; set; }
            public double? AmountDifference { get; set; }
            public DateTime CreateDate { get; set; }
            public List<OAcquirerCollection> AmountDistribution { get; set; }
        }

        public class OAcquirerCollection
        {
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public long? Terminals { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? NibsAmount { get; set; }
            public double? ExpectedAmount { get; set; }
            public double? ReceivedAmount { get; set; }
            public double? AmountDifference { get; set; }
        }
    }
}
