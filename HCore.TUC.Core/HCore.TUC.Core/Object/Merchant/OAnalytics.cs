//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{

    public class OAnalytics
    {
        public class Overview
        {
            public long Stores { get; set; } = 0;
            public long Terminals { get; set; } = 0;
            public long Cashiers { get; set; } = 0;
            public long Subaccounts { get; set; } = 0;
            public bool IsTransactionPresent { get; set; }
            public double? Balance { get; set; } = 0;
            public double RewardPercentage { get; set; } = 0;

            public string? SubscriptionName { get; set; }
        }

        public class Request
        {
            public string? Type { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public long SubAccountId { get; set; }
            public string? SubAccountKey { get; set; }

            public long StoreReferenceId { get; set; }
            public long CustomerReferenceId { get; set; }
            public string? CustomerReferenceIdKey { get; set; }
            public string? StoreReferenceKey { get; set; }

            public int SourceId { get; set; }

            public long CashierReferenceId { get; set; }
            public string? CashierReferenceKey { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool AmountDistribution { get; set; } = false;
            public OUserReference? UserReference { get; set; }
            public long ProgramId { get; set; }
        }

        public class SalesOverview
        {
            public long Customers { get; set; }
            public long Transactions { get; set; }
            public double InvoiceAmount { get; set; }
            public long CardTransaction { get; set; }
            public double CardInvoiceAmount { get; set; }
            public long CashTransaction { get; set; }
            public double CashInvoiceAmount { get; set; }
            public double HighSales { get; set; }
            public long HighestTimeSale { get; set; }
            public long LowestTimeSale { get; set; }
        }
        public class CustomerOverview
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long Repeat { get; set; }
            public double AverageInvoiceAmount { get; set; }
            public double AverageVisit { get; set; }
        }

        public class Loyalty
        {

            public long? NewCustomers { get; set; } = 0;
            public long? RepeatingCustomers { get; set; } = 0;
            public long? VisitsByRepeatingCustomers { get; set; } = 0;
            public long? VisitsByNewCustomers { get; set; } = 0;

            public double? NewCustomerInvoiceAmount { get; set; } = 0;
            public double? RepeatingCustomerInvoiceAmount { get; set; } = 0;

            public long? Transaction { get; set; } = 0;
            public long? TransactionCustomer { get; set; } = 0;
            public double? TransactionInvoiceAmount { get; set; } = 0;

            public long?  RewardTransaction { get; set; } = 0;
            public double? RewardInvoiceAmount { get; set; } = 0;
            public double? RewardAmount { get; set; } = 0;
            public double? RewardCommissionAmount { get; set; } = 0;

            public long? TucRewardTransaction { get; set; } = 0;
            public double? TucRewardInvoiceAmount { get; set; } = 0;
            public double? TucRewardAmount { get; set; } = 0;
            public double? TucRewardCommissionAmount { get; set; } = 0;

            public long? TucPlusRewardTransaction { get; set; } = 0;
            public double? TucPlusRewardInvoiceAmount { get; set; } = 0;
            public double? TucPlusRewardAmount { get; set; } = 0;
            public double? TucPlusRewardCommissionAmount { get; set; } = 0;

            public long? TucPlusRewardClaimTransaction { get; set; } = 0;
            public double? TucPlusRewardClaimInvoiceAmount { get; set; } = 0;
            public double? TucPlusRewardClaimAmount { get; set; } = 0;

            public long? RedeemTransaction { get; set; } = 0;
            public double?  RedeemInvoiceAmount { get; set; } = 0;
            public double? RedeemAmount { get; set; } = 0;
            public long? AllVisit { get; set; } = 0;
            public long TotalTimeofVisit { get; set; } = 0;
        }

        public class Sale
        {
            public string? TerminalId { get; set; }
            public long? Hour { get; set; }
            public string? WeekDay { get; set; }
            public string? Date { get; set; }
            public string? MonthName { get; set; }
            public long? Year { get; set; }
            public long? Month { get; set; }
            public long? TotalCustomer { get; set; }
            public long? TotalTransaction { get; set; }
            public double? TotalInvoiceAmount { get; set; }

            public string? StoreDisplayName { get; set; }
            public string? StoreAddress { get; set; }

            public DateTime? LastTransactionDate { get; set; }
            public long RepeatVisits { get; set; }
            public long NewVisits { get; set; }
            public long Transactions { get; set; }
            public long? UserAccountId { get; set; }

        }


        public class SalesMetrics
        {
          public  string Title { get; set; }
            public int? Hour { get; set; }
            public int? Year { get; set; }
            public int? Month { get; set; }
            public DateTime? Date { get; set; }
            public long TotalTransaction { get; set; }
            public long TotalCustomer { get; set; }
            public double TotalInvoiceAmount { get; set; }
            public long NewCustomer { get; set; }
            public double NewCustomerInvoiceAmount { get; set; }
            public long RepeatingCustomer { get; set; }
            public long VisitsByRepeatingCustomers { get; set; }
            public double RepeatingCustomerInvoiceAmount { get; set; }
        }



        public class Counts
        {
            public long TotalTransactions { get; set; }
            public double TotalSale { get; set; }

            public double AverageTransactions { get; set; }

            public double AverageTransactionAmount { get; set; }

            public long CardTransactions { get; set; }
            public double CardTransactionsAmount { get; set; }
            public long CashTransactions { get; set; }
            public double CashTransactionAmount { get; set; }


            public long OwnerCardTransactions { get; set; }
            public double OwnerCardTransactionsAmount { get; set; }
            public List<CardTypeSale> CardTypeSale { get; set; }
            public List<CardTypeSale> OwnerCardTypeSale { get; set; }
        }

        public class CardTypeSale
        {
            internal long ReferenceId { get; set; }
            public string? Name { get; set; }
            public long Transactions { get; set; }
            public double Amount { get; set; }
        }

        public class DateRange
        {
            public DateTime Date { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public object Data { get; set; }
        }

        public class DateRangeResponse
        {
            public List<DateRange> DateRange { get; set; }
        }

        public class TerminalStatus
        {
            public double? TransactionAmount { get; set; }
        }
    }
}
