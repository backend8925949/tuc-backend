//==================================================================================
// FileName: OReward.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Merchant.App
{
    public class OReward
    {


        public class Initialize
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountNumber { get; set; }
                public string? MobileNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? IconUrl { get; set; }
                public bool IsNewCustomer { get; set; } = false;
                public bool AllowCustomReward { get; set; } = false;
                internal long StatusId { get; set; }
                public double AccountBalance { get; set; }
                public double RewardPercentage { get; set; }
                public string? AccountNumber { get; set; }
            }
        }

        public class Confirm
        {
            public class Request
            {
                public string? CashierCode { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public double InvoiceAmount { get; set; }
                public double RewardAmount { get; set; }
                public string? Comment { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountNumber { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? Name { get; set; }
                public string? Nmae { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                internal string CountryIsd { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? DisplayName { get; set; }
                public string? IconUrl { get; set; }
                internal long StatusId { get; set; }
                public double AccountBalance { get; set; }
                public string? AccountNumber { get; set; }
                public string? Comment { get; set; }
                public double RewardAmount { get; set; }
                public double InvoiceAmount { get; set; }
                public double RewardPercentage { get; set; }
                public DateTime TransactionDate { get; set; }
                public long TransactionReferenceId { get; set; }
                public string? StatusName { get; set; }
            }
        }


    }
    public class ORewardBulk
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public List<Item> Customers { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Response
        {
            public int Total { get; set; }
            public List<Item> Customers { get; set; }
        }

        public class Item
        {
            public string? AccountNumber { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }
            public string? ReferenceNumber { get; set; }


            public string? Message { get; set; }
            public double InvoiceAmount { get; set; }
            public double RewardAmount { get; set; }
            public string? TransactionReference { get; set; }
            public string? StatusName { get; set; }
            public DateTime TransactionDate { get; set; }
        }

    }
}
