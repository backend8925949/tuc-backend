//==================================================================================
// FileName: OTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{
    // - FINALIZED -  DONOT TOUCH - HG - 12-08-2019

    public class OTransactionOverview
    {
        public long? Customers { get; set; }
        public long? Transactions { get; set; }
        public double? InvoiceAmount { get; set; }
        public double? CardInvoiceAmount { get; set; }
        public double? CashInvoiceAmount { get; set; }
        public double? RewardAmount { get; set; }
        public double? RewardClaimAmount { get; set; }
        public double? RedeemAmount { get; set; }
        public double? UserAmount { get; set; }
        public double? CommissionAmount { get; set; }
        public double? RedeemAmountByThirdParty { get; set; }
        public double? CashierRewardTotal { get; set; }
    }
    public class OTransaction
    {
        public class All
        {
            #region Parameters

            public long FromAccountId { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }

            public long? ModeId { get; set; }
            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public long? SourceId { get; set; }
            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public long? UserAccountId { get; set; }
            public long? CardBankId { get; set; }
            public long? CardBrandId { get; set; }
            public long? AcquirerId { get; set; }
            public long? ProviderId { get; set; }
            public long? ParentId { get; set; }
            public long? SubParentId { get; set; }
            public long? CreatedById { get; set; }

            public long? TypeId { get; set; }
            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }

            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double ConversionAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByMobileNumber { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }

            public double? ParentTransactionAmount { get; set; }
            public double? ParentTransactionChargeAmount { get; set; }
            public double? ParentTransactionCommissionAmount { get; set; }
            public double? ParentTransactionTotalAmount { get; set; }

            public long? OwnerId { get; set; }
            public long? InvoiceId { get; set; }
            public long? InvoiceItemId { get; set; }
            public long Status { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? GroupKey { get; set; }

            public string? TUCCardKey { get; set; }
            public string? TUCCardNumber { get; set; }
            public string? TCode { get; set; }

            public string? CardBrandName { get; set; }
            public string? CardSubBrandName { get; set; }
            public string? CardTypeName { get; set; }
            public string? CardBankName { get; set; }
            public string? CardBinNumber { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? AcquirerAmount { get; set; }
            public double? PTSAAmount { get; set; }
            public double? PSSPAmount { get; set; }
            public double? ProviderAmount { get; set; }
            public double? IssuerAmount { get; set; }
            public double? ThankUAmount { get; set; }
            public double? MerchantAmount { get; set; }
            public string? AcquirerName { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerKey { get; set; }

            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }

            public string? AccountNumber { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }

            public string? TerminalId { get; set; }
            public long TerminalReferenceId { get; set; }

            #endregion

        }
        public class Sale
        {
            public long LoyaltyTypeId { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }

            public long? TypeId { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public long? SourceId { get; set; }
            public string? SourceName { get; set; }
            public string? SourceCode { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? ClaimAmount { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDisplayName { get; set; }
            public string? UserMobileNumber { get; set; }

            internal double TotalAmount { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }
            public long? SubParentStatusId { get; set; }
            public string? SubParentStatusCode { get; set; }
            public string? SubParentStatusName { get; set; }

            public long? StoreReferenceId { get; set; }
            public string? StoreReferenceKey { get; set; }
            public string? StoreDisplayName { get; set; }

            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }


            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? CashierMobileNumber { get; set; }

            public double? Balance { get; set; }

            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }


            public string? AccountNumber { get; set; }
            public long? CardBrandId { get; set; }
            public string? CardBrandName { get; set; }
            public long? CardBankId { get; set; }
            public string? CardBankName { get; set; }

            public string? ReferenceNumber { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
            public double? CashierReward { get; set; }
        }
        public class SaleDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public string? ReferenceNumber { get; set; }
        }
        public class RewardDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? ConvinenceCharge { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public string? ReferenceNumber { get; set; } 
        }
        public class RedeemDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public string? ReferenceNumber { get; set; }
        }
        public class RewardClaimDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? ClaimAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
            public string? ReferenceNumber { get; set; }
        }
    }
}
