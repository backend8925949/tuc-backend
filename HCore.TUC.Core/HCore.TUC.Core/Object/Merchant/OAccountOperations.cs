//==================================================================================
// FileName: OAccountOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OAccountOperations
    {
        public class Category
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
        }
        public class ContactPerson
        {
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
        }
        public class Merchant
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? DisplayName { get; set; }
                public string? OwnerName { get; set; }

                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? WebsiteUrl { get; set; }

                public string? Description { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public ContactPerson ContactPerson { get; set; }
                public OStorageContent IconContent { get; set; }
                public List<Category> Categories { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Customer
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string AccountKey { get; set; }
                public string ReferralCode { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string MobileNumber { get; set; }
                public string EmailAddress { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? GenderCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

        }

        public class Store
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? Username { get; set; }
                public string? Password { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? SecondaryEmailAddress { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }

                public ContactPerson ContactPerson { get; set; }

                public OUserReference? UserReference { get; set; }

                public List<Category> Categories { get; set; }
                public long? StoreManagerId { get; set; }
            }

            public class Response
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public OAddress Address { get; set; }

                public ContactPerson ContactPerson { get; set; }

            }

        }

        public class Cashier
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long StoreId { get; set; }
                public string? StoreKey { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public string? EmployeeCode { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public OStorageContent IconContent { get; set; }

                public OUserReference? UserReference { get; set; }
                public bool IsAutoGenerated { get; set; }
                public string? Password { get; set; }
                public bool IsAskUser { get; set; }
            }
        }

        public class SubAccount
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public long StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public long RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? Address { get; set; }
                public OAddress AddressComponent { get; set; }
                public OStorageContent IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
                public bool IsAutoGenerated { get; set; }
                public string? Password { get; set; }
                public bool IsAskUser { get; set; }
            }
        }
        public class Terminal
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? TerminalId { get; set; }
                public string? SerialNumber { get; set; }
                public long MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public long StoreReferenceId { get; set; }
                public string? StoreReferenceKey { get; set; }
                public long ProviderReferenceId { get; set; }
                public string? ProviderReferenceKey { get; set; }
                public long AcquirerReferenceId { get; set; }
                public string? AcquirerReferenceKey { get; set; }
                public long CashierReferenceId { get; set; }
                public string? CashierReferenceKey { get; set; }

                public string? TypeCode { get; set; }

                public OStorageContent IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
    }
}
