//==================================================================================
// FileName: OUpload.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace HCore.TUC.Core.Object.Merchant
{
    public class OUpload
    {
      public class RewardUpload
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? FileName { get; set; }
                public List<Item> Customers { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class UploadRequest
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long MerchantId { get; set; }
                public string? CountryIsd { get; set; }
                public int MobileNumberLength { get; set; }
                public IFormFile? File { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class CustomerData
            {
                [JsonProperty("FirstName")]
                public string? FirstName { get; set; }

                [JsonProperty("MiddleName")]
                public string? MiddleName { get; set; }

                [JsonProperty("LastName")]
                public string? LastName { get; set; }

                [JsonProperty("MobileNumber")]
                public string? MobileNumber { get; set; }

                [JsonProperty("EmailAddress")]
                public string? EmailAddress { get; set; }

                [JsonProperty("Gender")]
                public string? Gender { get; set; }

                [JsonProperty("InvoiceAmount")]
                public double InvoiceAmount { get; set; }

                [JsonProperty("RewardAmount")]
                public double RewardAmount { get; set; }

                [JsonProperty("ReferenceNumber")]
                public string? ReferenceNumber { get; set; }
            }

            public class MQUploadRequest
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long MerchantId { get; set; }
                public string? CountryIsd { get; set; }
                public int MobileNumberLength { get; set; }
                public string? FileName { get; set; }
                public List<CustomerData>? Customers { get; set; }
                public OUserReference? UserReference { get; set; }
                public string? Environment { get; set; }
            }

            public class Response
            {
                public int Total { get; set; }
                public List<Item> Customers { get; set; }
            }

            public class Item
            {
                public string? Name { get; set; }
                public string? FirstName { get; set; }
                public string? MiddleName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? Comment { get; set; }
                public double InvoiceAmount { get; set; }
                public double RewardAmount { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public long MerchantId { get; set; }
                public int MerchantStatusId { get; set; }
                public string MerchantDisplayName { get; set; }
                public string CountryIsd { get; set; }
                public int MobileNumberLength { get; set; }
                public long? MerchantCategoryId { get; set; }
                public int CurrentRetryCount { get; set; } = 0;
                public OUserReference UserReference { get; set; }
                public string? FileName { get; set; }
                //public string? Message { get; set; }
                //public string? TransactionReference { get; set; }
                //public string? StatusName { get; set; }
                //public DateTime TransactionDate { get; set; }
            }

            //public class Request
            //{
            //    public long AccountId { get; set; }
            //    public string? AccountKey { get; set; }
            //    public string? FileName { get; set; }
            //    public List<Details> Data { get; set; }
            //    public OUserReference? UserReference { get; set; }
            //}
            //public class Details
            //{
            //    public long ReferenceId { get; set; }
            //    public long OwnerId { get; set; }
            //    public string? Name { get; set; }
            //    public string? FirstName { get; set; }
            //    public string? LastName { get; set; }
            //    public string? MobileNumber { get; set; }
            //    public string? FormattedMobileNumber { get; set; }
            //    public string? EmailAddress { get; set; }
            //    public string? Gender { get; set; }
            //    public DateTime? DateOfBirth { get; set; }
            //    public string? ReferenceNumber { get; set; }
            //    public DateTime? CreateDate { get; set; }
            //    public long? CreatedById { get; set; }
            //    public string? CreatedByDisplayName { get; set; }
            //    public DateTime? ModifyDate { get; set; }
            //    public long? ModifyById { get; set; }
            //    public long? StatusId { get; set; }
            //    public string? StatusCode { get; set; }
            //    public string? StatusName { get; set; }
            //    public string? StatusMessage { get; set; }


            //}

            public class RewardCustomerResponse
            {
                public string? ResponseMessage { get; set; }
                public RewardCustomerStatus Status { get; set; }
                public RewardCustomerReprocessFlag ReprocessFlag { get; set; }
            }

            public enum RewardCustomerReprocessFlag
            {
                Reprocess,
                DoNotReprocess
            }

            public enum RewardCustomerStatus
            {
                Completed,
                PartlyProcessed,
                Failed
            }
        }
    }
}
