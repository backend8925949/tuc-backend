//==================================================================================
// FileName: OOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Partner
{
    public class OOnboarding
    { 
        public class Merchant
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<Details> Data { get; set; }
                public OUserReference? UserReference { get; set; }

            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long OwnerId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
                public string? CityAreaName { get; set; }
                public string? CityName { get; set; }
                public string? StateName { get; set; }
                public string? CountryName { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public double? RewardPercentage { get; set; }
                public long? AccountId { get; set; }
                public string? CategoryName { get; set; }
                public string? ContactPersonName { get; set; }
                public string? ContactPersonMobileNumber { get; set; }
                public string? ContactPersonEmailAddress { get; set; }
                public string? ReferenceNumber { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public long? StatusId { get; set; }

                public string? StatusMessage { get; set; }

            }

        }
    }
}
