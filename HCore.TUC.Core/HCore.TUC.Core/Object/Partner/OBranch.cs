//==================================================================================
// FileName: OBranch.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Partner
{
    public class OBranch
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? BranchCode { get; set; }
            public string? PhoneNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? StateName { get; set; }

            public string? CityName { get; set; }

            public long RegionId { get; set; }
            public string? RegionKey { get; set; }
            public string? RegionName { get; set; }

            public OManager Manager { get; set; }

            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class OManager
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
                
            public long RoleId { get; set; }
            public string? RoleKey { get; set; }

            public long OwnerId { get; set; }
            public string? OwnerKey { get; set; }

            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
        }
        public class List
        {
            public  long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? BranchCode { get; set; }


            public string? DisplayName { get; set; }
            public string? PhoneNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? CityName { get; set; }
            public long? Stores { get; set; }
            public long? Terminals { get; set; }
            public long? ActiveTerminals { get; set; }

            public long? ManagerId { get; set; }
            public string? ManagerKey { get; set; }
            public string? ManagerName { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? BranchCode { get; set; }


            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? PhoneNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? CityName { get; set; }
            public string? StateName { get; set; }
            public string? RegionName { get; set; }

            public long? Stores { get; set; }
            public long? Terminals { get; set; }
            public long? ActiveTerminals { get; set; }

            public long? ManagerId { get; set; }
            public string? ManagerKey { get; set; }
            public string? ManagerName { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OManager
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long BranchId { get; set; }
            public string? BranchKey { get; set; }

            public long RoleId { get; set; }
            public string? RoleKey { get; set; }

            public long OwnerId { get; set; }
            public string? OwnerKey { get; set; }

            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long BranchId { get; set; }
            public string? BranchKey { get; set; }
            public string? BranchName { get; set; }
            public string? BranchAddress { get; set; }

            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public long? OwnerId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }

            public string? IconUrl { get; set; }
            public string? CityName { get; set; }

            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }

            public long Terminals { get; set; }
            public long Stores { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long BranchId { get; set; }
            public string? BranchKey { get; set; }
            public string? BranchName { get; set; }
            public string? BranchAddress { get; set; }

            public long? OwnerId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }

            public string? UserName { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? CityName { get; set; }

            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
            public string? IconUrl { get; set; }
            public long Terminals { get; set; }
            public long Stores { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
