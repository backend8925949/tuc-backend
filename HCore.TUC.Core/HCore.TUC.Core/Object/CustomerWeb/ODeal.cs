//==================================================================================
// FileName: ODeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.CustomerWeb
{
    public class ODeal
    {
        public class Request
        {
            public string? DealCodeId { get; set; }
            public long AuthAccountId { get; set; }
            public string? DealId { get; set; }
            public string? CategoryId { get; set; }
            public int Offset { get; set; }
            public int Limit { get; set; }
            public string? MobileNumber { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public string? DealId { get; set; }

            public string? MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }

            //public string? TypeCode { get; set; }
            //public string? TypeName { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? ImageUrl { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            //public long? Locations { get; set; }

            //public bool? IsFlashDeal { get; set; }

            //public double? MerchantAmount { get; set; }

            //public long? MaximumUnitSale { get; set; }
            //public double? Budget { get; set; }
            public long? ActualPrice { get; set; }
            public long? SellingPrice { get; set; }

            //public long TotalPurchase { get; set; }
            //public double? TotalAvailable { get; set; }


            public long Amount { get; set; }
            internal double? TAmount { get; set; }


            internal double? TActualPrice { get; set; }
            internal double? TSellingPrice { get; set; }

            public string? CategoryId { get; set; }
            public string? CategoryName { get; set; }

            public string SubCategoryId { get; set; }
            public string SubCategoryName { get; set; }
            public string SubCategoryImage { get; set; }
            //public DateTime CreateDate { get; set; }
            //public string? StatusCode { get; set; }
            //public string? StatusName { get; set; }
        }

        public class Details
        {
            internal long DealReferenceId { get; set; }
            internal long MerchantReferenceId { get; set; }
            public string? DealId { get; set; }
            public string? Title { get; set; }
            public string? Description { get; set; }
            internal string MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? ImageUrl { get; set; }
            public string? CategoryId { get; set; }
            public string? CategoryName { get; set; }
            public DateTime? StartDate { get; set; }
            public string SubCategoryId { get; set; }
            public string SubCategoryName { get; set; }
            public string SubCategoryImageUrl { get; set; }
            public DateTime? EndDate { get; set; }
            internal double? TActualPrice { get; set; }
            internal double? TSellingPrice { get; set; }
            internal double? TAmount { get; set; }

            public long ActualPrice { get; set; }
            public long SellingPrice { get; set; }
            public long Amount { get; set; }
            public long TotalPurchase { get; set; }
            public string? Terms { get; set; }
            public List<ImageUrlItem> Images { get; set; }
            public List<Location> Locations { get; set; }
        }
        public class Location
        {
            public string? LocationId { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class ImageUrlItem
        {
            public string? ImageUrl { get; set; }
        }

        public class Purchase
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                public string? DealKey { get; set; }
                public string? DealCodeKey { get; set; }

                public string? Reference { get; set; }
                public string? Status { get; set; }
                public string? Transaction { get; set; }

                public OUserReference? UserReference { get; set; }
            }

            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DealCode { get; set; }

                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public string? Title { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }
                public string? Name { get; set; }
                public string? ImageUrl { get; set; }
                public DateTime? DealEndDate { get; set; }
                public DateTime? DealCreateDate { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }
                public double? Saving { get; set; }
                public double? DiscountPercentage { get; set; }
                public int StoreCount { get; set; }
                public string? Address { get; set; }

                public string? CityAreaName { get; set; }
                public string? CityName { get; set; }
                public string? StateName { get; set; }

                public int? Quantity { get; set; }
                public int? DealTypeId { get; set; }
                public string? DealTypeCode { get; set; }
                public string? DealTypeName { get; set; }

                public int? DeliveryTypeId { get; set; }
                public string? DeliveryTypeCode { get; set; }
                public string? DeliveryTypeName { get; set; }

                public int? DealStatusId { get; set; }
                public string? DealStatusCode { get; set; }
                public string? DealStatusName { get; set; }

                public DateTime? PurchaseDate { get; set; }
                public DateTime? ExpiaryDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
            public class Details
            {
                internal long ReferenceId { get; set; }
                public string? DealCodeId { get; set; }

                internal long DealReferenceId { get; set; }
                public string? DealId { get; set; }


                public DateTime? UseDate { get; set; }
                public long? UseLocationId { get; set; }
                public string? UseLocationKey { get; set; }
                public string? UseLocationDisplayName { get; set; }
                public string? UseLocationAddress { get; set; }

                public string? ItemCode { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                //public double? ActualPrice { get; set; }
                //public double? SellingPrice { get; set; }
                public long? TotalAmount { get; set; }
                public double? TTotalAmount { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public string? ImageUrl { get; set; }
                public string? UsageInformation { get; set; }
                public string? Terms { get; set; }
                internal long MerchantReferenceId { get; set; }
                public string? MerchantId { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantContactNumber { get; set; }
                public string? MerchantEmailAddress { get; set; }
                public string? RedeemInstruction { get; set; }
                public int StatusId { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? DealStartDate { get; set; }
                public DateTime? DealEndDate { get; set; }

                public List<DealCodeLocation> Locations { get; set; }
            }
            public class DealCodeLocation
            {
                public string? DisplayName { get; set; }
                public string? Address { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
            }
        }
        public class View
        {
            public long AuthAccountId { get; set; }
            public int AccountSessionId { get; set; }
            public string? AccountSessionKey { get; set; }
            public string? DealKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
