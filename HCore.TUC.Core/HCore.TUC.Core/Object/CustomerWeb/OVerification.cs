//==================================================================================
// FileName: OVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.CustomerWeb
{
    public class OAccountAccess
    {
        public string? Key { get; set; }
        public bool IsNewAccount { get; set; }
        //public string? AccountKey { get; set; }
        public DateTime LoginTime { get; set; }
        public string? DisplayName { get; set; }
        public string? EmailAddress { get; set; }
        public string? Name { get; set; }
        public string? IconUrl { get; set; }
        public string? CountryIsd { get; set; }
        public OCountry Country { get; set; }
    }
    public class OCountry
    {
        public string? Name { get; set; }
        public string? Isd { get; set; }
        public string? Iso { get; set; }
        public string? Symbol { get; set; }
    }

    public class OCoreVerificationManager
    {
        public class Request
        {
            //public int Type { get; set; }
            public string? RequestToken { get; set; }

            //public string? Reference { get; set; }

            public string? CountryIsd { get; set; }

            public string? MobileNumber { get; set; }
            //public string? MobileMessage { get; set; }

            //public string? EmailAddress { get; set; }
            //public string? EmailMessage { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            internal long ReferenceId { get; set; }
            //public long Type { get; set; }
            //public string? Reference { get; set; }
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            //public string? CodeStart { get; set; }
            public string? RequestToken { get; set; }

            public bool IsNewAccount { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }

        }
        public class RequestVerify
        {
            public string? RequestToken { get; set; }
            //public string? AccessKey { get; set; }
            public string? AccessCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class RequestSignupUser
        {
            public string? RequestToken { get; set; }
            //public string? AccessKey { get; set; }
            public string? AccessCode { get; set; }
            public string? EmailAddress { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Password { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class RequestSignIn
        {
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? Password { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class RequestGuestCheckout
        {
            public string? CountryIsd { get; set; }

            public string? MobileNumber { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public int LocationTypeId { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? ZipCode { get; set; }
            public string? Landmark { get; set; }
            public long CityId { get; set; }
            public long CityAreaId { get; set; }
            public long StateId { get; set; }
            public int CountryId { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? MapAddress { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
}
