//==================================================================================
// FileName: OBuyPoint.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Object.CustomerWeb
{
    public class OBuyPoint
    {
        public class Initialize
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                public double Amount { get; set; }
                public string? Comment { get; set; }
                public string? PaymentMode { get; set; }
                public long? MerchantId { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public string? PromoCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? EmailAddress { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? PaystackKey { get; set; }
                public string? FlutterwaveKey { get; set; }
                public string? MobileNumber { get; set; }
                public string? DisplayName { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? PaymentDescription { get; set; }
                public string? IconUrl { get; set; }
                public long? PromoCodeId { get; set; }
                public string? PromoCodeKey { get; set; }
                public List<BanksList> Banks { get; set; }
                public List<Card> Cards { get; set; }
            }
            public class Card
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? CardBrand { get; set; } // Name
                public string? CardBin { get; set; } // SystemName
                public string? CardLast4 { get; set; } // Value
                public string? ExpMonth { get; set; } //  SubValue
                public string? ExpYear { get; set; }  //Description
            }


            public class BanksList
            {
                public string? SystemName { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public string? LocalUrl { get; set; }
                public string? BankCode { get; set; }
                public string? Code { get; set; }
                public string? DialCode { get; set; }
            }
        }

        public class Verify
        {
            public class Request
            {
                //public double Amount { get; set; }
                //public double Charge { get; set; }
                //public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? RefTransactionId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Charge
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? AuthMode { get; set; }
                public string? AuthPin { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Cancel
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                //public double Amount { get; set; }
                //public double Charge { get; set; }
                //public double TotalAmount { get; set; }
                //public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? TransactionDate { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? TotalAmount { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? PaymentMethodCode { get; set; }
            public string? PaymentMethodName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
        }

        public class WebHook
        {
            public class Response
            {
                public int? id { get; set; }
                public string? txRef { get; set; }
                public string? flwRef { get; set; }
                public string? orderRef { get; set; }
                public string? paymentPlan { get; set; }
                public string? paymentPage { get; set; }
                public DateTime? createdAt { get; set; }
                public int? amount { get; set; }
                public int? charged_amount { get; set; }
                public string? status { get; set; }
                public string? IP { get; set; }
                public string? currency { get; set; }
                public int? appfee { get; set; }
                public int? merchantfee { get; set; }
                public int? merchantbearsfee { get; set; }
                public Customer? customer { get; set; }
                public Entity? entity { get; set; }

                [JsonProperty("event.type")]
                public string? eventtype { get; set; }
            }

            public class Customer
            {
                public int? id { get; set; }
                public string? phone { get; set; }
                public string? fullName { get; set; }
                public string? customertoken { get; set; }
                public string? email { get; set; }
                public DateTime? createdAt { get; set; }
                public DateTime? updatedAt { get; set; }
                public object? deletedAt { get; set; }
                public int? AccountId { get; set; }
            }

            public class Entity
            {
                public string? card6 { get; set; }
                public string? card_last4 { get; set; }
                public string? card_country_iso { get; set; }
                public DateTime createdAt { get; set; }
            }
        }
    }
}

