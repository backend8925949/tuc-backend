//==================================================================================
// FileName: OAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.CustomerWeb
{
    public class OAccount
    {
        public class ValidatePin
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                public string? Pin { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? NewPin { get; set; }
            }
        }
        public class OCustReq
        {
            public class Req
            {
                public string? Host { get; set; }
                public string? MobileNumber { get; set; }
                public string? Pin { get; set; }
            }
            public class Res
            {
                public double Balance { get; set; }
                public string? MobileNumber { get; set; }
            }
        }
        public class OCustRegReq
        {
            public class Req
            {
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public DateTime? DateOfBirth { get; set; }
            }
            public class Res
            {
                public double Balance { get; set; }
                public string? MobileNumber { get; set; }
            }
        }

        public class OBalance
        {
            public long Balance { get; set; }
        }

        public class Request
        {
            public long AuthAccountId { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? Gender { get; set; }
            public string? ReferralCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public OUserReference? UserReference { get; set; }
            public OStorageContent ImageContent { get; set; }
        }

        public class LogoRequest
        {
            public long AuthAccountId { get; set; }
            public string? Uploader { get; set; }
            public OUserReference? UserReference { get; set; }
            public string logoimage { get; set; }
        }

        public class Response
        {
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? IconUrl { get; set; }
            public string? Gender { get; set; }
            public string? MobileNumber { get; set; }
            public string? ReferralCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
        }

        public class MerchantReferralRequest
        {
            public string? Referrer { get; set; }
            public int TotalRecords { get; set; }
            public int Offset { get; set; }
            public int Limit { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class MerchantReferralsResponse
        {
            public string? Name { get; set; }
            public string? PhoneNumber { get; set; }
            public decimal? Amount { get; set; }
            public OUserReference? UserReference { get; set; }
        }


        public class ReferralAmountRequest
        {
            public long AuthAccountId { get; set; }
          
            public string? EmailAddress { get; set; }
            public decimal? Amount { get; set; }
            public DateTime? Date { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class ReferralAmountResponse
        {
            
            public string? ResponseCode { get; set; }
            public string? ResponseMessage { get; set; }

        }

        public class Datum
        {
            public int Id { get; set; }
            public double Amount { get; set; }
            public string SetBy { get; set; }
            public int Status { get; set; }
            public DateTime DateSet { get; set; }
        }

        public class Result
        {
            public List<Datum> Data { get; set; }
        }

        public class ReferralResult
        {
            public Result Result { get; set; }
        }

    }
}
