﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.CustomerWeb
{
    public class OUserRegistration
    {
        public class UserLogInRequest
        {
            public string? MobileNumber { get; set; }
            public string? AccessPin { get; set; }
            public string? CountryIsd { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class UserRegistration
        {
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? AccountPin { get; set; }
            public string? ReferredBy { get; set; }
            public string? CountryIsd { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class UserVerificationRequest
        {
            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class UserVerificationResponse
        {
            internal long ReferenceId { get; set; }
            public bool IsNewAccount { get; set; }
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? RequestToken { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
        }

        public class ChangeUserAccountPinRequest
        {
            public string? AuthAccountKey { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? OldPin { get; set; }
            public string? NewPin { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class ChangeUserAccountPinResponse
        {
            public string? NewPin { get; set; }
        }

        public class UserProfileDetails
        {
            public string? Key { get; set; }
            public bool IsNewAccount { get; set; }
            public string? AccountKey { get; set; }
            public long? AccountId { get; set; }
            public DateTime LoginTime { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public string? CountryIsd { get; set; }
            public bool IsMobileNumberVerified { get; set; }
            public string? RequestToken { get; set; }
            public string? ReferralCode { get; set; }
            public DateTime? CreatedAt { get; set; }
            public OCountry? Country { get; set; }
        }

        public class ResendUserVerificationRequest
        {
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? RequestToken { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class ResetUserAccountPinRequest
        {
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class ResetUserAccountPinResponse
        {
            public string? NewPin { get; set; }
        }

        public class UpdateAccountPinRequest
        {
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? TempAccessPin { get; set; }
            public string? NewAccessPin { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}

