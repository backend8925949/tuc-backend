//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;

namespace HCore.TUC.Core.Object.Ptsp
{
    public class OAnalytics
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public long SubAccountId { get; set; }
            public string? SubAccountKey { get; set; }
            public string? Type { get; set; }
            public DateTime Date { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class DateRange
        {
            public DateTime Date { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public object Data { get; set; }
        }
        public class DateRangeResponse
        {
            public List<DateRange> DateRange { get; set; }
        }

        public class Counts
        {
            public long TotalMerchants { get; set; }
            public long ActiveMerchants { get; set; }

            public long TotalStores { get; set; }
            public long ActiveStores { get; set; }

            public long TotalTerminals { get; set; }
            public long ActiveTerminals { get; set; }
            public long ActivePosUpgradeRequest { get; set; }

            
            public long TotalTransactions { get; set; }
            public double TotalSale { get; set; }

            public double AverageTransactions { get; set; }

            public double AverageTransactionAmount { get; set; }

            public long CardTransactions { get; set; }
            public double CardTransactionsAmount { get; set; }
            public long CashTransactions { get; set; }
            public double CashTransactionAmount { get; set; }
            public List<CardTypeSale> CardTypeSale { get; set; }
            public TerminalStatus TerminalStatus { get; set; }
        }
        public class CardTypeSale
        {
            internal long ReferenceId { get; set; }
            public string? Name { get; set; }
            public long Transactions { get; set; }
            public double Amount { get; set; }
        }

        public class Sales
        {


            public long TotalTransactions { get; set; }
            public long TotalSale { get; set; }

            public long AverageTransactions { get; set; }

            public double AverageTransactionAmount { get; set; }
        }

        public class TerminalStatus
        {
            public long Total { get; set; }
            public long Active { get; set; }
            public long Idle { get; set; }
            public long Dead { get; set; }

            public long Inactive { get; set; }
        }
    }
}
