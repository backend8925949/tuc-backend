//==================================================================================
// FileName: OTerminalProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Ptsp
{
    public class OTerminalProduct
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Sku { get; set; }
            public string? ReferenceNumber { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? Name { get; set; }
            //public string? ShortDescription { get; set; }
            public string? Description { get; set; }
            public string? CategoryName { get; set; }
            public string? SubCategoryName { get; set; }
            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }
            public long TotalStock { get; set; }
            public double RewardPercentage { get; set; }
            //public double MaximumRewardAmount { get; set; }
            public long AvailableStock { get; set; }
            public string? StatusCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Sku { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? Name { get; set; }
            public string? CategoryName { get; set; }
            public double SellingPrice { get; set; }
            public long AvailableStock { get; set; }
            public double RewardPercentage { get; set; }
            public DateTime CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Sku { get; set; }
            public string? ReferenceNumber { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }

            public string? Description { get; set; }

            public string? CategoryName { get; set; }
            public string? SubCategoryName { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public double RewardPercentage { get; set; }

            public long TotalStock { get; set; }
            public long AvailableStock { get; set; }

            public DateTime CreateDate { get; set; }

            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
