//==================================================================================
// FileName: OTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.Core.Object.Ptsp
{
    public class OTransactionOverview
    {
        public long? Customers { get; set; }
        public long? Transactions { get; set; }
        public double? InvoiceAmount { get; set; }
        public double? CardInvoiceAmount { get; set; }
        public double? CashInvoiceAmount { get; set; }
        public double? RewardAmount { get; set; }
        public double? RewardClaimAmount { get; set; }
        public double? RedeemAmount { get; set; }
        public double? UserAmount { get; set; }
        public double? CommissionAmount { get; set; }
    }
    public class OTransaction
    {

        public class Sale
        {

            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? TypeId { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? RedeemAmount { get; set; }
            public double? ClaimAmount { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDisplayName { get; set; }
            public string? UserMobileNumber { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? StoreReferenceId { get; set; }
            public string? StoreReferenceKey { get; set; }
            public string? StoreDisplayName { get; set; }

            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }


            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? CashierMobileNumber { get; set; }


            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }


            public string? AccountNumber { get; set; }
            public long? CardBrandId { get; set; }
            public string? CardBrandName { get; set; }
            public long? CardBankId { get; set; }
            public string? CardBankName { get; set; }

            public string? ReferenceNumber { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
        }
        public class SaleDownload
        {
            public long ReferenceId { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string? User { get; set; }
            public string? MobileNumber { get; set; }
            public string? TypeName { get; set; }
            public double? InvoiceAmount { get; set; }
            public string? CardNumber { get; set; }
            public string? CardBrand { get; set; }
            public string? CardBank { get; set; }
            public string? Merchant { get; set; }
            public string? Store { get; set; }
            public string? CashierId { get; set; }
            public string? CashierDisplayName { get; set; }
            public string? Bank { get; set; }
            public string? Provider { get; set; }
            public string? Issuer { get; set; }
            public string? Status { get; set; }
            public string? TerminalId { get; set; }
        }
    }
}
