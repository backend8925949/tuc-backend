//==================================================================================
// FileName: OMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.OpenApi
{
    public class OMerchant
    {
        public class List
        {
            internal long ReferenceId { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public string? Address { get; set; }
            public double? RewardPercentage { get; set; }
            public List<Store> Stores { get; set; }
        }
        public class Store
        {
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
        }
    }
}
