//==================================================================================
// FileName: OOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Customer
{
    public class OOperation
    {
        public class Balance
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? Type { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public Details ThankUCash { get; set; }
                public Details ThankUCashPlus { get; set; }
                public Details GiftCard { get; set; }
                public Details GiftPoint { get; set; }
            }
            public class Details
            {
                public double Credit { get; set; }
                public double Debit { get; set; }
                public double Balance { get; set; }
                public DateTime? LastTransactionDate { get; set; }
            }
        }
        public class ReferralCode
        {
            public class Request
            {
                public string? ReferralCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

        }
        public class ReferralBonus
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

        }

        public class LogoRequest
        {
            public long AuthAccountId { get; set; }
            public string? Uploader { get; set; }
            public OUserReference? UserReference { get; set; }
            public string? logoimage { get; set; }
        }

        public class PinManager
        {
            public class Update
            {
                public class Request
                {
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public string? OldPin { get; set; }
                    public string? NewPin { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
            }
        }
    }
}
