//==================================================================================
// FileName: OBuyPoint.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Object.Customer
{
    public class OBuyPoint
    {
        public class Initialize
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public double Amount { get; set; }
                public string? Comment { get; set; }
                public string? PaymentMode { get; set; }
                public long? MerchantId { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? PaymentProvider { get; set; }
                public string? PaystackKey { get; set; }
                public string? Payload { get; set; }
                public string? EmailAddress { get; set; }
                public string? Currency { get; set; }
                public string? CountryCode { get; set; }
                public string? AccessKey { get; set; }
                public string? PaymentUrl { get; set; }
                public string? FlutterwaveKey { get; set; }
                public string? MobileNumber { get; set; }
                public string? DisplayName { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? PaymentDescription { get; set; }
                public string? IconUrl { get; set; }
                public List<BanksList> Banks { get; set; }
                public List<Card> Cards { get; set; }
            }
            public class Card
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? CardBrand { get; set; } // Name
                public string? CardBin { get; set; } // SystemName
                public string? CardLast4 { get; set; } // Value
                public string? ExpMonth { get; set; } //  SubValue
                public string? ExpYear { get; set; }  //Description
            }


            public class BanksList
            {
                public string? SystemName { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public string? LocalUrl { get; set; }
                public string? BankCode { get; set; }
                public string? Code { get; set; }
                public string? DialCode { get; set; }
            }
        }
        public class Verify
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? RefTransactionId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Charge
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
                public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public string? AuthMode { get; set; }
                public string? AuthPin { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Cancel
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                //public double Amount { get; set; }
                //public double Charge { get; set; }
                //public double TotalAmount { get; set; }
                //public string? PaymentMode { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }


        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? TransactionDate { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? TotalAmount { get; set; }

            public string? ReferenceNumber { get; set; }

            public string? PaymentMethodCode { get; set; }
            public string? PaymentMethodName { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
        }
    }
}
