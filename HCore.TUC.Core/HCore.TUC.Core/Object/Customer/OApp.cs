//==================================================================================
// FileName: OApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Customer
{
    public class OApp
    {
        public class Configuration
        {
            public class Request
            {
                public string? SerialNumber { get; set; }
                public string? OsName { get; set; }
                public string? Brands { get; set; }
                public string? Model { get; set; }
                public int Width { get; set; }
                public int Height { get; set; }
                public string? CarrierName { get; set; }
                public string? CountryCode { get; set; }
                public string? Mcc { get; set; }
                public string? Mnc { get; set; }
                public string? NotificationUrl { get; set; }
                public double Latitude { get; set; }
                public double Longitude { get; set; }
                public string? AppVersion { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? ServerAppVersion { get; set; }
                public string? AboutApp { get; set; }
                public bool IsAppAvailable { get; set; }
                public bool IsReceiverAvailable { get; set; }
                public bool IsDonationAvailable { get; set; }
                public List<ContactItem> Contacts { get; set; }
                public List<Country> Countries { get; set; }
                public List<Slider> HomeSlider { get; set; }
                public List<Feature> Features { get; set; }
                public Donation Donation { get; set; }
                public Referral Referral { get; set; }
                public List<TucMileStone> TucMileStones { get; set; }
            }

           
            public class Referral
            {
                public string? Title { get; set; }
                public string? Description { get; set; }
                public string? ShareMessageTitle { get; set; }
                public string? ShareMessageDescription { get; set; }
            }
            public class TucMileStone
            {
                public string? Title { get; set; }
                public string? SystemName { get; set; }
                public string? Description { get; set; }
                public double Limit { get; set; }
            }
            public class Donation
            {
                public string? ReceiverTitle { get; set; }
                public string? ReceiverMessage { get; set; }

                public string? Title { get; set; }
                public string? Message { get; set; }
                public string? Description { get; set; }
                public string? Conditions { get; set; }
                public string? IconUrl { get; set; }
                public string? PaymentReference { get; set; }
                public List<DonationRange> DonationRanges { get; set; }
            }
            public class DonationRange
            {
                public string? Title { get; set; }
                public long Quantity { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double TotalAmount { get; set; }
            }
        }

     
        public class ContactItem
        {
            public string? type { get; set; }
            public string? icon { get; set; }
            public string? title { get; set; }
            public string? value { get; set; }
            public string? systemvalue { get; set; }
        }
        public class Feature
        {
            public string? SystemName { get; set; }
            public string? Name { get; set; }
            public bool IsActive { get; set; } = false;
            public bool IsNew { get; set; } = false;
        }
        public class Slider
        {
            public string? ImageUrl { get; set; }
            public string? NavigationType { get; set; }
            public string? NavigateUrl { get; set; }
            public object NavigationData { get; set; }
            public object SliderContent { get; set; }
        }
        public class Country
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? IconUrl { get; set; }
            public string? Name { get; set; }
            public string? Iso { get; set; }
            public string? Isd { get; set; }
            public string? CurrencyName { get; set; }
            public string? CurrencyNotation { get; set; }
            public string? CurrencySymbol { get; set; }
            public string? CurrencyHex { get; set; }
            public int NumberLength { get; set; }
            public List<Feature> ActiveFeatures { get; set; }
        }
    }
}
