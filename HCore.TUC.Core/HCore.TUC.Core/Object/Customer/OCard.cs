//==================================================================================
// FileName: OCard.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Object.Customer
{
    public class OCard
    {
        public class List
        {
            public long ReferenceId { get; set; }
            //public string? ReferenceKey { get; set; }

            public string? CardType { get; set; }
            //public string? Bin { get; set; }
            public string? CardEnd { get; set; }
            //public string? ExpYear { get; set; }
            //public string? ExpMonth { get; set; }
        }

    }
}
