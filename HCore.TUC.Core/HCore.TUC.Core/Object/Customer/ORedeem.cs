//==================================================================================
// FileName: ORedeem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Core.Object.Customer
{
    public class ORedeem
    {
        public class Initialize
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                internal long? MerchantId { get; set; }
                internal long AccountTypeId { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? IconUrl { get; set; }
                internal long StatusId { get; set; }
                public double AccountBalance { get; set; }
                public string? AccountNumber { get; set; }
            }
        }

        public class Confirm
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public double RedeemAmount { get; set; }
                public string? Pin { get; set; }

                public string? Comment { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AccountNumber { get; set; }

                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                internal long AccountTypeId { get; set; }
                internal long? MerchantId { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? DisplayName { get; set; }
                public string? IconUrl { get; set; }
                internal long StatusId { get; set; }
                public double AccountBalance { get; set; }
                public string? AccountNumber { get; set; }
                public string? Comment { get; set; }
                public double RedeemAmount { get; set; }
                public DateTime TransactionDate { get; set; }
                public long TransactionReferenceId { get; set; }
                public string? StatusName { get; set; }
            }
        }
    }
}
