////==================================================================================
//// FileName: CoreActivityStatusUpdate.cs
//// Author : Harshal Gandole
//// Created On : 
//// Description : Class for defining database related functions
////
//// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
////
//// Revision History:
//// Date             : Changed By        : Comments
//// ---------------------------------------------------------------------------------
////                 | Harshal Gandole   : Created New File
////
////
////==================================================================================

//﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Dynamic.Core;
//using Akka.Actor;
//using HCore.Data;
//using HCore.Data.Models;
//using HCore.Helper;
//using HCore.Operations;
//using HCore.Operations.Object;
//using Z.EntityFramework.Plus;
//using static HCore.Helper.HCoreConstant;
//using static HCore.Helper.HCoreConstant.Helpers;
//using static HCore.Helper.HCoreConstant.HelperStatus;

//namespace HCore.TUC.Core.CoreBackground
//{
//    internal class CoreActivityStatusUpdate
//    {
//        HCoreContext _HCoreContext;
//        internal void UpdateAccountActivityStatus()
//        {
//            #region Manage Exception
//            try
//            {
//                var UAccActivityStatusActor = ActorSystem.Create("ActorCustomerActivityStatusUpdate");
//                var UAccActivityStatusActorGreet = UAccActivityStatusActor.ActorOf<ActorCustomerActivityStatusUpdate>("ActorCustomerActivityStatusUpdate");
//                UAccActivityStatusActorGreet.Tell("ActorCustomerActivityStatusUpdate");

//                //var UAccAppActivityStatusActor = ActorSystem.Create("ActorCustomerAppActivityStatusUpdate");
//                //var UAccAppActivityStatusActorGreet = UAccActivityStatusActor.ActorOf<ActorCustomerAppActivityStatusUpdate>("ActorCustomerAppActivityStatusUpdate");
//                //UAccAppActivityStatusActorGreet.Tell("ActorCustomerAppActivityStatusUpdate");


//                var UAccMerchantActivityStatusActor = ActorSystem.Create("ActorMerchantActivityStatusUpdate");
//                var UAccMerchantActivityStatusActorGreet = UAccMerchantActivityStatusActor.ActorOf<ActorMerchantActivityStatusUpdate>("ActorMerchantActivityStatusUpdate");
//                UAccMerchantActivityStatusActorGreet.Tell("ActorMerchantActivityStatusUpdate");

//                var UAccStoreActivityStatusActor = ActorSystem.Create("ActorStoreActivityStatusUpdate");
//                var UAccStoreActivityStatusActorGreet = UAccStoreActivityStatusActor.ActorOf<ActorStoreActivityStatusUpdate>("ActorStoreActivityStatusUpdate");
//                UAccStoreActivityStatusActorGreet.Tell("ActorStoreActivityStatusUpdate");



//                var UAccCashierActivityStatusActor = ActorSystem.Create("ActorCashierActivityStatusUpdate");
//                var UAccCashierActivityStatusActorGreet = UAccCashierActivityStatusActor.ActorOf<ActorCashierActivityStatusUpdate>("ActorCashierActivityStatusUpdate");
//                UAccCashierActivityStatusActorGreet.Tell("ActorCashierActivityStatusUpdate");


//                var UAccTerminalActivityStatusActor = ActorSystem.Create("ActorTerminalActivityStatusUpdate");
//                var UAccTerminalActivityStatusActorGreet = UAccTerminalActivityStatusActor.ActorOf<ActorTerminalActivityStatusUpdate>("ActorTerminalActivityStatusUpdate");
//                UAccTerminalActivityStatusActorGreet.Tell("ActorTerminalActivityStatusUpdate");


//                var UAccountAccountActivityStatusActor = ActorSystem.Create("ActorAccountsActivityStatusUpdate");
//                var UAccountAccountActivityStatusActorGreet = UAccountAccountActivityStatusActor.ActorOf<ActorAccountsActivityStatusUpdate>("ActorStoreActivityStatusUpdate");
//                UAccountAccountActivityStatusActorGreet.Tell("ActorAccountsActivityStatusUpdate");
//            }
//            catch (Exception _Exception)
//            {
//                HCoreHelper.LogException("UpdateAccountActivityStatus", _Exception, null);
//            }
//            #endregion
//        }
//    }

//    internal class ActorCustomerActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorCustomerActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.HCUAccount
//                         .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                         && !x.HCUAccountTransactionAccount.Any(a => a.CreateDate > Cust_Idle_Start_Date)
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                          .Skip(0)
//                            .Take(500)
//                         .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.HCUAccount
//                              .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                           && !x.HCUAccountTransactionAccount.Any(a => a.CreateDate > Cust_Active_Start_Date)
//                           && x.HCUAccountTransactionAccount.Any(a => a.CreateDate > Cust_Idle_Start_Date && a.CreateDate < Cust_Active_Start_Date
//                           && x.ActivityStatusId != ActivityStatusType.Idle))
//                               .Skip(0)
//                            .Take(500)
//                           .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.HCUAccount
//                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                            && x.HCUAccountTransactionAccount.Any(a => a.CreateDate > Cust_Active_Start_Date && a.CreateDate < Cust_Active_End_Date)
//                            && x.ActivityStatusId != ActivityStatusType.Active)
//                            .Skip(0)
//                            .Take(500)
//                            .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorCustomerActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }

//    //internal class ActorCustomerAppActivityStatusUpdate : ReceiveActor
//    //{
//    //    HCoreContext _HCoreContext;
//    //    public ActorCustomerAppActivityStatusUpdate()
//    //    {
//    //        Receive<string>(_Request =>
//    //        {
//    //            try
//    //            {
//    //                using (_HCoreContext = new HCoreContext())
//    //                {
//    //                    DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//    //                    DateTime Cust_Active_Start_Date = TodaysDate.AddSeconds(-1);
//    //                    DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//    //                    DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);
//    //                    var Dead_Customers = _HCoreContext.HCUAccountOption
//    //                     .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//    //                     && x.Account.HCUAccountSessionAccount.Any(a => a.LastActivityDate > Cust_Idle_Start_Date)
//    //                     && x.Account.ActivityStatusId != ActivityStatusType.Dead)
//    //                     .Update(x => new HCUAccountOption() { AppActivityStatusId = ActivityStatusType.Dead });
//    //                    var Idle_Customers = _HCoreContext.HCUAccountOption
//    //                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//    //                        && x.Account.HCUAccountSessionAccount.Any(a => a.LastActivityDate > Cust_Idle_Start_Date && a.LastActivityDate < Cust_Active_Start_Date)
//    //                       && x.Account.ActivityStatusId != ActivityStatusType.Idle)
//    //                       .Update(x => new HCUAccountOption() { AppActivityStatusId = ActivityStatusType.Idle });
//    //                    var Active_Customers = _HCoreContext.HCUAccountOption
//    //                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//    //                        && x.Account.HCUAccountSessionAccount.Any(a => a.LastActivityDate > Cust_Active_Start_Date && a.LastActivityDate < Cust_Active_End_Date)
//    //                        && x.Account.ActivityStatusId != ActivityStatusType.Active)
//    //                        .Update(x => new HCUAccountOption() { AppActivityStatusId = ActivityStatusType.Active });
//    //                }
//    //            }
//    //            catch (Exception _Exception)
//    //            {
//    //                HCoreHelper.LogException("ActorCustomerAppActivityStatusUpdate", _Exception);
//    //            }
//    //        });
//    //    }
//    //}

//    internal class ActorMerchantActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorMerchantActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.HCUAccount
//                         .Where(x => x.AccountTypeId == UserAccountType.Merchant
//                         && !x.HCUAccountTransactionParent.Any(a => a.CreateDate > Cust_Idle_Start_Date)
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                          .Skip(0)
//                            .Take(500)
//                         .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.HCUAccount
//                              .Where(x => x.AccountTypeId == UserAccountType.Merchant
//                           && !x.HCUAccountTransactionParent.Any(a => a.CreateDate > Cust_Active_Start_Date)
//                           && x.HCUAccountTransactionParent.Any(a => a.CreateDate > Cust_Idle_Start_Date && a.CreateDate < Cust_Active_Start_Date
//                           && x.ActivityStatusId != ActivityStatusType.Idle))
//                               .Skip(0)
//                            .Take(500)
//                           .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.HCUAccount
//                            .Where(x => x.AccountTypeId == UserAccountType.Merchant
//                            && x.HCUAccountTransactionParent.Any(a => a.CreateDate > Cust_Active_Start_Date && a.CreateDate < Cust_Active_End_Date)
//                            && x.ActivityStatusId != ActivityStatusType.Active)
//                             .Skip(0)
//                            .Take(500)
//                            .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorCustomerActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }
//    internal class ActorStoreActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorStoreActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.HCUAccount
//                         .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
//                         && !x.HCUAccountTransactionSubParent.Any(a => a.CreateDate > Cust_Idle_Start_Date)
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                          .Skip(0)
//                            .Take(500)
//                         .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.HCUAccount
//                              .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
//                           && !x.HCUAccountTransactionSubParent.Any(a => a.CreateDate > Cust_Active_Start_Date)
//                           && x.HCUAccountTransactionSubParent.Any(a => a.CreateDate > Cust_Idle_Start_Date && a.CreateDate < Cust_Active_Start_Date
//                           && x.ActivityStatusId != ActivityStatusType.Idle))
//                               .Skip(0)
//                            .Take(500)
//                           .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.HCUAccount
//                            .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
//                            && x.HCUAccountTransactionSubParent.Any(a => a.CreateDate > Cust_Active_Start_Date && a.CreateDate < Cust_Active_End_Date)
//                            && x.ActivityStatusId != ActivityStatusType.Active)
//                             .Skip(0)
//                            .Take(500)
//                            .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorStoreActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }
//    internal class ActorCashierActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorCashierActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.HCUAccount
//                         .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
//                         && !x.HCUAccountTransactionCashier.Any(a => a.CreateDate > Cust_Idle_Start_Date)
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                         .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.HCUAccount
//                              .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
//                           && !x.HCUAccountTransactionCashier.Any(a => a.CreateDate > Cust_Active_Start_Date)
//                           && x.HCUAccountTransactionCashier.Any(a => a.CreateDate > Cust_Idle_Start_Date && a.CreateDate < Cust_Active_Start_Date
//                           && x.ActivityStatusId != ActivityStatusType.Idle))
//                           .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.HCUAccount
//                            .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
//                            && x.HCUAccountTransactionCashier.Any(a => a.CreateDate > Cust_Active_Start_Date && a.CreateDate < Cust_Active_End_Date)
//                            && x.ActivityStatusId != ActivityStatusType.Active)
//                            .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorCashierActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }
//    internal class ActorTerminalActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorTerminalActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.TUCTerminal
//                         .Where(x =>  !x.HCUAccountTransaction.Any(a => a.CreateDate > Cust_Idle_Start_Date)
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                         .Update(x => new TUCTerminal() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.TUCTerminal
//                              .Where(x =>  !x.HCUAccountTransaction.Any(a => a.CreateDate > Cust_Active_Start_Date)
//                           && x.HCUAccountTransaction.Any(a => a.CreateDate > Cust_Idle_Start_Date && a.CreateDate < Cust_Active_Start_Date
//                           && x.ActivityStatusId != ActivityStatusType.Idle))
//                           .Update(x => new TUCTerminal() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.TUCTerminal
//                            .Where(x => x.HCUAccountTransaction.Any(a => a.CreateDate > Cust_Active_Start_Date && a.CreateDate < Cust_Active_End_Date)
//                            && x.ActivityStatusId != ActivityStatusType.Active)
//                            .Update(x => new TUCTerminal() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorTerminalActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }
//    internal class ActorAccountsActivityStatusUpdate : ReceiveActor
//    {
//        HCoreContext _HCoreContext;
//        public ActorAccountsActivityStatusUpdate()
//        {
//            Receive<string>(_Request =>
//            {
//                try
//                {
//                    using (_HCoreContext = new HCoreContext())
//                    {
//                        DateTime TodaysDate = HCoreHelper.GetGMTToNigeria().Date;
//                        DateTime Cust_Active_Start_Date = TodaysDate.AddDays(-7).AddSeconds(-1);
//                        DateTime Cust_Active_End_Date = TodaysDate.AddDays(1);
//                        DateTime Cust_Idle_Start_Date = TodaysDate.AddDays(-7);

//                        var Dead_Customers = _HCoreContext.HCUAccount
//                         .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                         && x.AccountTypeId == UserAccountType.Merchant
//                         && x.AccountTypeId == UserAccountType.MerchantStore
//                         && x.AccountTypeId == UserAccountType.MerchantCashier
//                         && x.AccountTypeId == UserAccountType.Terminal
//                         && x.LastActivityDate > Cust_Idle_Start_Date
//                         && x.ActivityStatusId != ActivityStatusType.Dead)
//                         .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Dead });

//                        var Idle_Customers = _HCoreContext.HCUAccount
//                              .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                         && x.AccountTypeId == UserAccountType.Merchant
//                         && x.AccountTypeId == UserAccountType.MerchantStore
//                         && x.AccountTypeId == UserAccountType.MerchantCashier
//                         && x.AccountTypeId == UserAccountType.Terminal
//                         && x.LastActivityDate > Cust_Active_Start_Date
//                         && x.LastActivityDate > Cust_Idle_Start_Date && x.LastActivityDate < Cust_Active_Start_Date
//                         && x.ActivityStatusId != ActivityStatusType.Idle)
//                           .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Idle });

//                        var Active_Customers = _HCoreContext.HCUAccount
//                          .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                         && x.AccountTypeId == UserAccountType.Merchant
//                         && x.AccountTypeId == UserAccountType.MerchantStore
//                         && x.AccountTypeId == UserAccountType.MerchantCashier
//                         && x.AccountTypeId == UserAccountType.Terminal
//                         && x.LastActivityDate > Cust_Active_Start_Date && x.LastActivityDate < Cust_Active_End_Date
//                         && x.ActivityStatusId != ActivityStatusType.Idle)
//                            .Update(x => new HCUAccount() { ActivityStatusId = ActivityStatusType.Active });
//                    }
//                }
//                catch (Exception _Exception)
//                {
//                    HCoreHelper.LogException("ActorAccountsActivityStatusUpdate", _Exception);
//                }
//            });
//        }
//    }


//}
