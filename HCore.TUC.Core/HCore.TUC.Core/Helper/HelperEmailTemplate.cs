//==================================================================================
// FileName: HelperEmailTemplate.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Helper
{
    internal static class HelperEmailTemplate
    {
        internal static class MerchantOnboarding
        {
            internal const string WelcomeEmail = "d-8a7bc7cd93d84cd7ac25684a3e851aea";
            internal const string SubscriptionConfirmed = "d-a8e8d1277b2340059f10f73bf332fe08";
            internal const string SubscriptionReminderDays = "d-8f840dcbf6ad45e28502bef78206660f";
            internal const string SubscriptionRemindeToday = "d-41f92edebef74dd1a95b77bda1a3d8ba";
        }
        internal const string CredentailsEmail = "d-846b0e29e98e4ce0a485ec305ce419b1";

    }
}
