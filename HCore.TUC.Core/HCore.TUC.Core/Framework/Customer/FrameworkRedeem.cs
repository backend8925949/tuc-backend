//==================================================================================
// FileName: FrameworkRedeem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to redeem
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.Framework.Customer
{
    public class FrameworkRedeem
    {
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        ORedeem.Confirm.Response _ConfirmResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        TUCLoyaltyPending _TUCLoyaltyPending;

        /// <summary>
        /// Description: Redeem initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Redeem_Initialize(ORedeem.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1153, TUCCoreResource.CA1153M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1154, TUCCoreResource.CA1154M);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1155, TUCCoreResource.CA1155M);
                }
                else
                {

                    using (_HCoreContext = new HCoreContext())
                    {

                        var MerchantDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountCode == _Request.AccountNumber
                            && (x.AccountTypeId == UserAccountType.Merchant
                            || x.AccountTypeId == UserAccountType.MerchantCashier))
                            .Select(x => new ORedeem.Initialize.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                DisplayName = x.DisplayName,
                                IconUrl = x.IconStorage.Path,
                                StatusId = x.StatusId,
                                AccountBalance = 0,
                                AccountNumber = x.AccountCode,
                                AccountTypeId = x.AccountTypeId,
                                MerchantId = x.Owner.OwnerId
                            }).FirstOrDefault();
                        if (MerchantDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                        if (MerchantDetails.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                && x.Id == MerchantDetails.MerchantId)
                                .Select(x => new ORedeem.Initialize.Response
                                {
                                    ReferenceId = MerchantDetails.ReferenceId,
                                    ReferenceKey = MerchantDetails.ReferenceKey,
                                    DisplayName = x.DisplayName,
                                    IconUrl = x.IconStorage.Path,
                                    StatusId = x.StatusId,
                                    AccountBalance = 0,
                                    AccountNumber = MerchantDetails.AccountNumber,
                                }).FirstOrDefault();
                        }

                        //var MerchantDetails = _HCoreContext.HCUAccount
                        //    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                        //                && x.AccountCode == _Request.AccountNumber)
                        //    .Select(x => new ORedeem.Initialize.Response
                        //    {
                        //        ReferenceId = x.Id,
                        //        ReferenceKey = x.Guid,
                        //        DisplayName = x.DisplayName,
                        //        IconUrl = x.IconStorage.Path,
                        //        StatusId = x.StatusId,
                        //        AccountBalance = 0,
                        //        AccountNumber = x.AccountCode,
                        //    }).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            if (MerchantDetails.StatusId == HelperStatus.Default.Active)
                            {
                                if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                                {
                                    MerchantDetails.IconUrl = _AppConfig.StorageUrl + MerchantDetails.IconUrl;
                                }
                                else
                                {
                                    MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                                }


                                var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.Id == _Request.AccountId
                                && x.Id == _Request.UserReference.AccountId
                                && x.Guid == _Request.UserReference.AccountKey)
                                    .Select(x => new
                                    {
                                        x.StatusId
                                    }).FirstOrDefault();
                                if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    //_ManageCoreTransaction = new ManageCoreTransaction();
                                    //MerchantDetails.AccountBalance = _ManageCoreTransaction.GetAppUserBalance(_Request.AccountId, true);
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, MerchantDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Redeem confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Redeem_Confirm(ORedeem.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1153, TUCCoreResource.CA1153M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1154, TUCCoreResource.CA1154M);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1155, TUCCoreResource.CA1155M);
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1160, TUCCoreResource.CA1160M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1161, TUCCoreResource.CA1161M);
                }
                if (_Request.RedeemAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1162, TUCCoreResource.CA1162M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        long CashierId = 0;
                        var MerchantDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountCode == _Request.AccountNumber
                            && (x.AccountTypeId == UserAccountType.Merchant
                            || x.AccountTypeId == UserAccountType.MerchantCashier))
                            .Select(x => new ORedeem.Confirm.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                DisplayName = x.DisplayName,
                                IconUrl = x.IconStorage.Path,
                                StatusId = x.StatusId,
                                AccountBalance = 0,
                                AccountNumber = x.AccountCode,
                                AccountTypeId = x.AccountTypeId,
                                MerchantId = x.Owner.OwnerId
                            }).FirstOrDefault();
                        if (MerchantDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                        if (MerchantDetails.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            CashierId = MerchantDetails.ReferenceId;
                            MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                && x.Id == MerchantDetails.MerchantId)
                                .Select(x => new ORedeem.Confirm.Response
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    DisplayName = x.DisplayName,
                                    IconUrl = x.IconStorage.Path,
                                    StatusId = x.StatusId,
                                    AccountBalance = 0,
                                    AccountNumber = x.AccountCode,
                                }).FirstOrDefault();
                        }
                        //var MerchantDetails = _HCoreContext.HCUAccount
                        //    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                        //                && x.AccountCode == _Request.AccountNumber
                        //                && x.Id == _Request.ReferenceId
                        //                && x.Guid == _Request.ReferenceKey)
                        //    .Select(x => new ORedeem.Confirm.Response
                        //    {
                        //        ReferenceId = x.Id,
                        //        ReferenceKey = x.Guid,
                        //        DisplayName = x.DisplayName,
                        //        IconUrl = x.IconStorage.Path,
                        //        StatusId = x.StatusId,
                        //    }).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            if (MerchantDetails.StatusId == HelperStatus.Default.Active)
                            {
                                var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.Id == _Request.AccountId
                                && x.Id == _Request.UserReference.AccountId
                                && x.Guid == _Request.UserReference.AccountKey)
                                    .Select(x => new
                                    {
                                        x.DisplayName,
                                        x.StatusId,
                                        x.AccessPin
                                    }).FirstOrDefault();
                                if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    string SPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                                    if (string.IsNullOrEmpty(_Request.Pin))
                                    {
                                        _Request.Pin = SPin;
                                    }
                                    if (SPin == _Request.Pin)
                                    {
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double Balance = _ManageCoreTransaction.GetAppUserBalance(_Request.AccountId, true);
                                        if (Balance > _Request.RedeemAmount)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.GroupKey = _Request.AccountId + "O" + HCoreHelper.GenerateDateString() + "O" + _Request.ReferenceId;
                                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.ParentId = _Request.ReferenceId;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.RedeemAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.RedeemAmount;
                                            if (CashierId > 0)
                                            {
                                                _CoreTransactionRequest.CashierId = CashierId;
                                            }
                                            //_CoreTransactionRequest.ReferenceNumber = TransactionDetails.ReferenceNumber;
                                            _CoreTransactionRequest.ReferenceAmount = _Request.RedeemAmount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.AccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.TUCPay,
                                                SourceId = TransactionSource.TUC,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.ReferenceId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.TUCPay,
                                                SourceId = TransactionSource.Settlement,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                                                {
                                                    MerchantDetails.IconUrl = _AppConfig.StorageUrl + MerchantDetails.IconUrl;
                                                }
                                                else
                                                {
                                                    MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                                                }
                                                MerchantDetails.RedeemAmount = _Request.RedeemAmount;
                                                MerchantDetails.Comment = _Request.Comment;
                                                MerchantDetails.TransactionReferenceId = TransactionResponse.ReferenceId;
                                                MerchantDetails.TransactionDate = TransactionResponse.TransactionDate;
                                                MerchantDetails.StatusName = "success";
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                    {
                                                        if (HostEnvironment == HostEnvironmentType.Live)
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + _Request.RedeemAmount + " amount paid.", "Your have paid N" + _Request.RedeemAmount + " at " + MerchantDetails.DisplayName + " with TUC Pay", "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                        else
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + _Request.RedeemAmount + " amount paid.", "Your have paid N" + _Request.RedeemAmount + " at " + MerchantDetails.DisplayName + " with TUC Pay", "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                    }

                                                    if (CashierId > 0)
                                                    {
                                                        string CashierUserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == CashierId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                        if (!string.IsNullOrEmpty(CashierUserNotificationUrl))
                                                        {
                                                            if (HostEnvironment == HostEnvironmentType.Live)
                                                            {
                                                                HCoreHelper.SendPushToDeviceMerchant(CashierUserNotificationUrl, "dashboard", "N " + _Request.RedeemAmount + " amount received.", "Your have received N" + _Request.RedeemAmount + " from " + AccountDetails.DisplayName + " with TUC Pay", "dashboard", 0, null, "View details");
                                                            }
                                                            else
                                                            {
                                                                HCoreHelper.SendPushToDeviceMerchant(CashierUserNotificationUrl, "dashboard", "N " + _Request.RedeemAmount + " amount received.", "Your have received N" + _Request.RedeemAmount + " from " + AccountDetails.DisplayName + " with TUC Pay", "dashboard", 0, null, "View details");
                                                            }
                                                        }
                                                    }
                                                }
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    HCoreHelper.SendPushToTopicMerchant("tucmerchant" + MerchantDetails.ReferenceId, "dashboard", "N " + _Request.RedeemAmount + " payment received.", "Your have received N" + _Request.RedeemAmount + " from " + AccountDetails.DisplayName + " from TUC Pay", "dashboard", 0, null, "View details", null);
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendPushToTopicMerchant("tucmerchant_test" + MerchantDetails.ReferenceId, "dashboard", "N " + _Request.RedeemAmount + " payment received.", "Your have received N" + _Request.RedeemAmount + " from " + AccountDetails.DisplayName + " from TUC Pay", "dashboard", 0, null, "View details", null);
                                                }
                                                ProcessReward(MerchantDetails.ReferenceId, MerchantDetails.DisplayName, _Request.RedeemAmount, _Request);
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, MerchantDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1165, TUCCoreResource.CA1165M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1164, TUCCoreResource.CA1164M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Processes the reward.
        /// </summary>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <param name="MerchantDisplayName">Display name of the merchant.</param>
        /// <param name="InvoiceAmount">The invoice amount.</param>
        /// <param name="_Request">The request.</param>
        private void ProcessReward(long MerchantId, string? MerchantDisplayName, double InvoiceAmount, ORedeem.Confirm.Request _Request)
        {
            try
            {
                double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                if (RewardPercentage > 0)
                {
                    double RewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, RewardPercentage, _AppConfig.SystemEntryRoundDouble);
                    if (RewardAmount > 0)
                    {
                        var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", MerchantId);
                        double UserRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                        double UserRewardAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage);
                        double CommissionAmount = RewardAmount - UserRewardAmount;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        double Balance = _ManageCoreTransaction.GetAccountBalance(MerchantId, TransactionSource.Merchant);
                        bool IsThankUCashEnabled = false;
                        long ThankUCashPlusIsEnable = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", MerchantId));
                        if (ThankUCashPlusIsEnable != 0)
                        {
                            IsThankUCashEnabled = true;
                        }

                        bool AllowReward = false;
                        if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                        {
                            if (RewardAmount <= Balance)
                            {
                                AllowReward = true;
                            }
                            else if (RewardAmount <= (Balance + 5000))
                            {
                                AllowReward = false;
                                try
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        int TransactionTypeId = TransactionType.CashReward;
                                        _TUCLoyaltyPending = new TUCLoyaltyPending();
                                        _TUCLoyaltyPending.Guid = HCoreHelper.GenerateGuid();
                                        _TUCLoyaltyPending.TransactionDate = HCoreHelper.GetGMTDateTime();
                                        //_TUCLoyaltyPending.TransactionId = 1;
                                        _TUCLoyaltyPending.LoyaltyTypeId = Loyalty.Reward;
                                        _TUCLoyaltyPending.TypeId = TransactionTypeId;
                                        _TUCLoyaltyPending.ModeId = TransactionMode.Credit;
                                        _TUCLoyaltyPending.SourceId = TransactionSource.TUC;
                                        _TUCLoyaltyPending.FromAccountId = MerchantId;
                                        _TUCLoyaltyPending.ToAccountId = _Request.UserReference.AccountId;
                                        _TUCLoyaltyPending.Amount = UserRewardAmount;
                                        _TUCLoyaltyPending.CommissionAmount = CommissionAmount;
                                        _TUCLoyaltyPending.TotalAmount = RewardAmount;
                                        _TUCLoyaltyPending.InvoiceAmount = InvoiceAmount;
                                        _TUCLoyaltyPending.LoyaltyInvoiceAmount = InvoiceAmount;
                                        _TUCLoyaltyPending.Balance = 0;
                                        _TUCLoyaltyPending.CustomerId = _Request.UserReference.AccountId;
                                        _TUCLoyaltyPending.MerchantId = MerchantId;
                                        //_TUCLoyaltyPending.StoreId = _RequestItem.GatewayInfo.StoreId;
                                        //_TUCLoyaltyPending.CashierId = _RequestItem.GatewayInfo.CashierId;
                                        //_TUCLoyaltyPending.TerminalId = _RequestItem.GatewayInfo.TerminalId;
                                        //_TUCLoyaltyPending.ProviderId = _RequestItem.GatewayInfo.PtspId;
                                        //_TUCLoyaltyPending.AcquirerId = _RequestItem.GatewayInfo.AcquirerId;
                                        //_TUCLoyaltyPending.InvoiceNumber = 1;
                                        //_TUCLoyaltyPending.AccountNumber = _RequestItem.UserRequest.ReferenceNumber;
                                        //_TUCLoyaltyPending.ReferenceNumber = _RequestItem.UserRequest.ReferenceNumber;
                                        _TUCLoyaltyPending.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _TUCLoyaltyPending.CreatedById = _Request.UserReference.AccountId;
                                        _TUCLoyaltyPending.StatusId = HelperStatus.Transaction.Pending;
                                        _HCoreContext.TUCLoyaltyPending.Add(_TUCLoyaltyPending);
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                catch (Exception _Exception)
                                {

                                    HCoreHelper.LogException("ProcessReward-SAVEPENDINGREWARD", _Exception);
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                    _HCoreContext.Dispose();
                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                    {
                                        if (HostEnvironment == HostEnvironmentType.Live)
                                        {
                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + RewardAmount + " pending rewards received.", "Your have received N" + RewardAmount + " from " + MerchantDisplayName + " for purchase of N" + InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                        }
                                        else
                                        {
                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + RewardAmount + " pending rewards received.", "Your have received N" + RewardAmount + " from " + MerchantDisplayName + " for purchase of N" + InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                AllowReward = false;
                            }
                        }
                        else
                        {
                            AllowReward = true;
                        }
                        if (IsThankUCashEnabled == false && AllowReward == true)
                        {
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.GroupKey = _Request.AccountId + "O" + HCoreHelper.GenerateDateString() + "O" + _Request.ReferenceId;
                            _CoreTransactionRequest.CustomerId = _Request.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.ParentId = MerchantId;
                            _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                            _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = MerchantId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.TUCPayReward,
                                SourceId = TransactionSource.Merchant,
                                Amount = UserRewardAmount,
                                Charge = 0,
                                Comission = CommissionAmount,
                                TotalAmount = RewardAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.UserReference.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.TUCPayReward,
                                SourceId = TransactionSource.TUC,
                                Amount = UserRewardAmount,
                                TotalAmount = UserRewardAmount,
                            });
                            if (CommissionAmount > 0)
                            {
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.TUCPayReward,
                                    SourceId = TransactionSource.Settlement,
                                    Amount = CommissionAmount,
                                    TotalAmount = CommissionAmount,
                                });
                            }


                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                //if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                //{
                                //    AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                //}
                                //else
                                //{
                                //    AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                //}
                                //AccountDetails.RewardAmount = RewardAmount;
                                //AccountDetails.InvoiceAmount = _Request.InvoiceAmount;
                                //AccountDetails.Comment = _Request.Comment;
                                //AccountDetails.TransactionReferenceId = TransactionResponse.ReferenceId;
                                //AccountDetails.TransactionDate = TransactionResponse.TransactionDate;
                                //AccountDetails.StatusName = "success";
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                    {
                                        if (HostEnvironment == HostEnvironmentType.Live)
                                        {
                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + RewardAmount + " rewards received.", "Your have received N" + RewardAmount + " from " + MerchantDisplayName + " for purchase of N" + InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                        }
                                        else
                                        {
                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + RewardAmount + " rewards received.", "Your have received N" + RewardAmount + " from " + MerchantDisplayName + " for purchase of N" + InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TUCPAY-REWARD", _Exception);
            }
        }
    }
}
