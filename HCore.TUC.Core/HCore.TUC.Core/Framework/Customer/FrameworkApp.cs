//==================================================================================
// FileName: FrameworkApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics to get app slider and configuration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Resource.Customer;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Customer
{
    public class FrameworkApp
    {
        OApp.Configuration.Response _AppConfiguration;
        OApp.Configuration.Referral _Referral;
        List<OApp.Country> _Countries;
        List<OApp.Slider> _Sliders;
        HCoreContext _HCoreContext;
        List<OApp.ContactItem> _Contacts;

        List<OApp.Feature> _Features;
        List<OApp.Configuration.TucMileStone> _TucMileStones;
        /// <summary>
        /// Description: Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppConfiguration(OApp.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AppConfiguration = new OApp.Configuration.Response();
                _AppConfiguration.IsAppAvailable = true;
                _AppConfiguration.IsReceiverAvailable = true;


                if (_Request.OsName.ToLower() == "android")
                {
                    _AppConfiguration.ServerAppVersion = "1.0.7";
                }
                else if (_Request.OsName.ToLower() == "ios")
                {
                    _AppConfiguration.ServerAppVersion = "1.0.7";
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    #endregion
                }
                _AppConfiguration.IsReceiverAvailable = false;
                _AppConfiguration.IsDonationAvailable = false;

                _Referral = new OApp.Configuration.Referral();
                _Referral.Title = "Refer Your Friends & Earn";
                _Referral.Description = "Earn upto 5% when your friend earns each time with ThankUCash";
                _Referral.ShareMessageTitle = "ThankUCash : Earn Money On Every Purchase";
                _Referral.ShareMessageDescription = "Find great deals up to 90% off and earn money On Every Purchase. Just Download ThankUCash App.";
                _AppConfiguration.Referral = _Referral;
                _AppConfiguration.AboutApp = "Welcome! ThankUCash is a rewards program operated by Connected Analytics Limited (referred to as ThankUCash or we or us). ThankUCash lets you earn ThankUCash rewards points (Points) by making qualifying purchases at certain retail and service companies and through other promotions and offers that may be provided by merchants (merchants whom we may also refer to as partners) or other merchants.";

                #region Milesstones
                _TucMileStones = new List<OApp.Configuration.TucMileStone>
                {
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Purple",
                        SystemName = "purple",
                        Description = "You get registered to receive rewards from all ThankUCash merchant partners and you also get access to information on Deals and Utilities. <br> <br> Purple class is our entry level and once you reach N50,000 in accumulated spend you will graduate to the Bronze Class.",
                        Limit = 50000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Bronze",
                        SystemName = "bronze",
                        Description = "You get every benefit of Purple class and also:  <br> <br> Get instant Rewards for attaining the bronze class.   <br> <br> Get access to bronze class type rewards and also early notifications to deals.   <br> <br> Once you have reached a total of N200,000 accumulated spend you will graduate to the Silver Class.",
                        Limit = 200000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Silver",
                        SystemName = "silver",
                        Description = "You get every benefit of Bronze class and also:   <br> <br> Get instant Rewards for attaining the Silver class, which you can choose from a list on your phone screen.   <br> <br> Get access to Silver class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Silver Class programs.   <br> <br> Once you have reached a total of N750,000 accumulated spend you will graduate to the Gold Class.",
                        Limit = 750000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Gold",
                        SystemName = "gold",
                        Description = "You get every benefit of Silver class and also:  <br> <br>Get instant Rewards for attaining the Gold class, which you can choose from a list on your phone screen. <br> <br>Get access to Gold class type rewards and also early notifications to deals with rights higher than previous classes. <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Gold member. <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Gold Class programs. <br> <br> Once you have reached a total of N3,000,000 accumulated spend you will graduate to the Diamond Class.",
                        Limit = 3000000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Diamond",
                        SystemName = "diamond",
                        Description = "You get every benefit of Gold class and also: <br> <br> Get instant Rewards for attaining the Diamond class, which you can choose from a list on your phone screen.  <br> <br> Get access to Diamond class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Diamond member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruse liners. <br> <br> You get Hotel get away and more. You earn more on rewards percentage and have access to Airport partner lounges and early access to Diamond Class programs with special lunch at particular partner lounges.  <br> <br> Once you have reached a total of N5,000,000 accumulated spend you will graduate to the Platinum Class.",
                        Limit = 5000000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Platinum",
                        SystemName = "platinum",
                        Description = "You get every benefit of Diamond class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Get access to Platinum class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas, free car checkup, home delivery and also Monthly rewards for being a Platinum member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruise liners.  <br> <br> You get Hotel get away and more.  You earn more on rewards percentage and have access to Airport partner lounges, car rentals and early access to Platinum Class programs with special lunch at particular partner lounges. <br> <br> Once you have reached a total of N15,000,000 accumulated spend you will graduate to the Platinum Plus Class. ",
                        Limit = 15000000
                    },
                    new OApp.Configuration.TucMileStone
                    {
                        Title = "Platinum Plus",
                        SystemName = "platinumplus",
                        Description = "You get every benefit of Platinum class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Personalized gifts from our partners and also get Free Tickets to global networking events.  <br> <br> Also get access to our partner Platinum Airport Network and free upgrades on our Airline partners.",
                        Limit = 30000000
                    }
                };
                _AppConfiguration.TucMileStones = _TucMileStones;
                #endregion


                #region Contact Information
                _Contacts = new List<OApp.ContactItem>
                {
                    new OApp.ContactItem
                    {
                        type = "call",
                        icon = "las la-phone-volume",
                        title = "Call us on",
                        value = "0700 0433433",
                        systemvalue = "+2347000433433",
                    },
                    new OApp.ContactItem
                    {
                        type = "call",
                        icon = "las la-phone-volume",
                        title = "Call us on",
                        value = "01 888 8177",
                        systemvalue = "+23418888177",
                    },
                    new OApp.ContactItem
                    {
                        type = "whatsapp",
                        icon = "lab la-whatsapp",
                        title = "Message us on",
                        value = "0813 3178563",
                        systemvalue = "https://api.whatsapp.com/send?phone=+2348133178563",
                    },
                    new OApp.ContactItem
                    {
                        type = "email",
                        icon = "las la-envelope",
                        title = "Email us on",
                        value = "hello@thankucash.com",
                        systemvalue = "mailto:hello@thankucash.com",
                    }
                };
                _AppConfiguration.Contacts = _Contacts;
                #endregion


                #region Features
                _Features = new List<OApp.Feature>
                {
                    new OApp.Feature { Name = "USSD Verfication", SystemName = "ussdverification" },
                    new OApp.Feature { Name = "Deals", SystemName = "service_deals" },
                    new OApp.Feature { Name = "Stores", SystemName = "service_nearby" },
                    new OApp.Feature { Name = "BNPL", SystemName = "service_bnpl" },
                    new OApp.Feature { Name = "Booking", SystemName = "service_booking" },

                    new OApp.Feature { Name = "VAS Airtime", SystemName = "service_vas_airtime" },
                    new OApp.Feature { Name = "VAS TV Recharge", SystemName = "service_vas_tvrecharge" },
                    new OApp.Feature { Name = "VAS Utilities", SystemName = "service_vas_utilities" },
                    new OApp.Feature { Name = "VAS Electricity", SystemName = "service_vas_electricity" },
                    //new OApp.Feature { Name = "VAS LCC Topup", SystemName = "service_vas_lcctopup" },

                    new OApp.Feature { Name = "General About", SystemName = "general_about" },
                    new OApp.Feature { Name = "General Support", SystemName = "general_support" },
                    new OApp.Feature { Name = "General Faq", SystemName = "general_faq" },

                    new OApp.Feature { Name = "Account Reset Pin", SystemName = "account_resetpin" },
                    new OApp.Feature { Name = "Account Update Pin", SystemName = "account_updatepin" },
                    new OApp.Feature { Name = "Account Stored Cards", SystemName = "account_storedcards" },
                    new OApp.Feature { Name = "Account Bank Accounts", SystemName = "account_bankaccount" },
                    new OApp.Feature { Name = "Account Profile", SystemName = "account_profile" },

                    new OApp.Feature { Name = "Payments Ussd", SystemName = "payments_ussd" },
                    new OApp.Feature { Name = "Payments Paystack", SystemName = "payments_paystack" }
                };


                _Countries = new List<OApp.Country>
                {
                    new OApp.Country
                    {
                        ReferenceId = 1,
                        ReferenceKey = "nigeria",
                        Name = "Nigeria",
                        IconUrl = _AppConfig.StorageUrl + "flag/ng.png",
                        Isd = "234",
                        Iso = "ng",
                        CurrencyName = "Naira",
                        CurrencyNotation = "NGN",
                        CurrencySymbol = "₦",
                        CurrencyHex = "&#8358;",
                        NumberLength = 10,
                        ActiveFeatures = _Features,
                    }
                };
                _Features = new List<OApp.Feature>
                {
                    //_Features.Add(new OApp.Feature { Name = "USSD Verfication", SystemName = "ussdverification" });
                    new OApp.Feature { Name = "Deals", SystemName = "service_deals" },
                    //_Features.Add(new OApp.Feature { Name = "Stores", SystemName = "service_nearby" });
                    //_Features.Add(new OApp.Feature { Name = "BNPL", SystemName = "service_bnpl" });

                    //_Features.Add(new OApp.Feature { Name = "Booking", SystemName = "service_booking" });
                    new OApp.Feature { Name = "VAS Airtime", SystemName = "service_vas_airtime" },
                    new OApp.Feature { Name = "VAS TV Recharge", SystemName = "service_vas_tvrecharge" },
                    new OApp.Feature { Name = "VAS Utilities", SystemName = "service_vas_utilities" },
                    new OApp.Feature { Name = "VAS Electricity", SystemName = "service_vas_electricity" },
                    //
                    new OApp.Feature { Name = "General About", SystemName = "general_about" },
                    new OApp.Feature { Name = "General Support", SystemName = "general_support" },
                    new OApp.Feature { Name = "General Faq", SystemName = "general_faq" },

                    new OApp.Feature { Name = "Account Reset Pin", SystemName = "account_resetpin" },
                    new OApp.Feature { Name = "Account Update Pin", SystemName = "account_updatepin" },
                    new OApp.Feature { Name = "Account Stored Cards", SystemName = "account_storedcards" },
                    new OApp.Feature { Name = "Account Bank Accounts", SystemName = "account_bankaccount" },
                    new OApp.Feature { Name = "Account Profile", SystemName = "account_profile" },

                    new OApp.Feature { Name = "Payments Paystack", SystemName = "payments_paystack" }
                };
                _Countries.Add(new OApp.Country
                {
                    ReferenceId = 87,
                    ReferenceKey = "ghana",
                    Name = "Ghana",
                    IconUrl = _AppConfig.StorageUrl + "flag/gh.png",
                    Isd = "233",
                    Iso = "gh",
                    CurrencyName = "Cedi",
                    CurrencyNotation = "GHS",
                    CurrencySymbol = "₵",
                    CurrencyHex = "&#8373;",
                    NumberLength = 10,
                    ActiveFeatures = _Features,
                });

                _Features = new List<OApp.Feature>();
                _Features.Add(new OApp.Feature { Name = "xx", SystemName = "xxx" });
                _Countries.Add(new OApp.Country
                {
                    ReferenceId = 118,
                    ReferenceKey = "kenya",
                    Name = "Kenya",
                    IconUrl = _AppConfig.StorageUrl + "flag/ke.png",
                    Isd = "254",
                    Iso = "key",
                    CurrencyName = "Shilling",
                    CurrencyNotation = "KHS",
                    CurrencySymbol = "KSh",
                    CurrencyHex = "KSh",
                    NumberLength = 9,
                    ActiveFeatures = _Features,
                });
                _AppConfiguration.Countries = _Countries;
                #endregion


                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                //_Sliders = new List<OApp.Slider>();
                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _Promotions = _HCoreContext.TUCAppPromotion
                //        .Where(x => x.StatusId == HelperStatus.Default.Active
                //                && ActiveTime >= x.StartDate
                //                && ActiveTime <= x.EndDate)
                //        .Select(x => new
                //        {
                //            ImageUrl = _AppConfig.StorageUrl + x.ImageStorage.Path,
                //            NavigationType = x.NavigationType,
                //            NavigateUrl = x.NavigateUrl,
                //            NavigationData = x.NavigationData,
                //        }).Skip(0).Take(8).ToList();
                //    if (_Promotions.Count > 0)
                //    {
                //        foreach (var _Promotion in _Promotions)
                //        {
                //            _Sliders.Add(new OApp.Slider
                //            {
                //                ImageUrl = _Promotion.ImageUrl,
                //                NavigationType = _Promotion.NavigationType,
                //                NavigateUrl = _Promotion.NavigateUrl,
                //            });
                //        }
                //    }
                //}
                //if(_Sliders.Count > 0)
                //{
                //    _AppConfiguration.HomeSlider = _Sliders;
                //}
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppConfiguration, "CA0200", ResponseCode.CA0200);
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetAppConfiguration", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application slider.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppSlider(OReference _Request)
        {
            #region Manage Exception
            try
            {
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                _Sliders = new List<OApp.Slider>();
                using (_HCoreContext = new HCoreContext())
                {

                    var _Promotions = _HCoreContext.TUCAppPromotion
                        .Where(x => x.StatusId == HelperStatus.Default.Active
                        && x.CountryId == _Request.UserReference.CountryId
                                && ActiveTime >= x.StartDate
                                && ActiveTime <= x.EndDate)
                        .Select(x => new
                        {
                            ImageUrl = _AppConfig.StorageUrl + x.ImageStorage.Path,
                            NavigationType = x.NavigationType,
                            NavigateUrl = x.NavigateUrl,
                            NavigationData = x.NavigationData,
                        }).Skip(0).Take(8).ToList();
                    if (_Promotions.Count > 0)
                    {
                        foreach (var _Promotion in _Promotions)
                        {

                            _Sliders.Add(new OApp.Slider
                            {
                                ImageUrl = _Promotion.ImageUrl,
                                NavigationType = _Promotion.NavigationType,
                                NavigateUrl = _Promotion.NavigateUrl,
                            });
                        }
                    }
                    //if (_Sliders.Count < 9)
                    //{
                    //    int Limit = 8 - _Sliders.Count;
                    //    var _Deals = _HCoreContext.MDDeal.Where(x => x.StatusId == HelperStatus.Deals.Published && x.Account.CountryId == _Request.UserReference.CountryId && x.Account.IconStorageId != null && x.PosterStorageId != null && (ActiveTime > x.StartDate && ActiveTime < x.EndDate))
                    //                     .OrderByDescending(x => x.CreateDate)
                    //                     .Select(x => new
                    //                     {
                    //                         ReferenceId = x.Id,
                    //                         ReferenceKey = x.Guid,
                    //                         Title = x.Title,
                    //                         MerchantIconUrl = x.Account.IconStorage.Path,
                    //                         ImageUrl = x.PosterStorage.Path,
                    //                         ActualPrice = x.ActualPrice,
                    //                         SellingPrice = x.SellingPrice,
                    //                     }).Skip(0)
                    //                     .Take(Limit)
                    //                   .ToList();
                    //    if (_Deals.Count > 0)
                    //    {
                    //        foreach (var _Deal in _Deals)
                    //        {
                    //            _Sliders.Add(new OApp.Slider
                    //            {
                    //                ImageUrl = _AppConfig.StorageUrl + _Deal.ImageUrl,
                    //                NavigationType = "app",
                    //                NavigateUrl = "dealdetails",
                    //                NavigationData = new
                    //                {
                    //                    ReferenceId = _Deal.ReferenceId,
                    //                    ReferenceKey = _Deal.ReferenceKey,
                    //                },
                    //                SliderContent = new
                    //                {
                    //                    Title = _Deal.Title,
                    //                    IconUrl = _AppConfig.StorageUrl + _Deal.MerchantIconUrl,
                    //                }
                    //            });
                    //        }
                    //    }
                    //}
                }
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Sliders, "CA0200", ResponseCode.CA0200);
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetAppSlider", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application customization.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppCustomization(OApp.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AppConfiguration = new OApp.Configuration.Response();
                _AppConfiguration.IsAppAvailable = true;
                _AppConfiguration.IsReceiverAvailable = true;
                if (_Request.OsName.ToLower() == "android")
                {
                    _AppConfiguration.ServerAppVersion = "1.0.7";
                }
                else if (_Request.OsName.ToLower() == "ios")
                {
                    _AppConfiguration.ServerAppVersion = "1.0.7";
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    #endregion
                }
                _AppConfiguration.IsReceiverAvailable = false;
                _AppConfiguration.IsDonationAvailable = false;

                _Referral = new OApp.Configuration.Referral();
                _Referral.Title = "Refer Your Friends & Earn";
                _Referral.Description = "Earn upto 5% when your friend earns each time with ThankUCash";
                _Referral.ShareMessageTitle = "ThankUCash : Earn Money On Every Purchase";
                _Referral.ShareMessageDescription = "Find great deals up to 90% off and earn money On Every Purchase. Just Download ThankUCash App.";
                _AppConfiguration.Referral = _Referral;

                _TucMileStones = new List<OApp.Configuration.TucMileStone>();
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Purple",
                    SystemName = "purple",
                    Description = "You get registered to receive rewards from all ThankUCash merchant partners and you also get access to information on Deals and Utilities. <br> <br> Purple class is our entry level and once you reach N50,000 in accumulated spend you will graduate to the Bronze Class.",
                    Limit = 50000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Bronze",
                    SystemName = "bronze",
                    Description = "You get every benefit of Purple class and also:  <br> <br> Get instant Rewards for attaining the bronze class.   <br> <br> Get access to bronze class type rewards and also early notifications to deals.   <br> <br> Once you have reached a total of N200,000 accumulated spend you will graduate to the Silver Class.",
                    Limit = 200000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Silver",
                    SystemName = "silver",
                    Description = "You get every benefit of Bronze class and also:   <br> <br> Get instant Rewards for attaining the Silver class, which you can choose from a list on your phone screen.   <br> <br> Get access to Silver class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Silver Class programs.   <br> <br> Once you have reached a total of N750,000 accumulated spend you will graduate to the Gold Class.",
                    Limit = 750000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Gold",
                    SystemName = "gold",
                    Description = "You get every benefit of Silver class and also:  <br> <br>Get instant Rewards for attaining the Gold class, which you can choose from a list on your phone screen. <br> <br>Get access to Gold class type rewards and also early notifications to deals with rights higher than previous classes. <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Gold member. <br> <br> You also earn more on rewards percentage and have access to Airport partner lounges and early access to Gold Class programs. <br> <br> Once you have reached a total of N3,000,000 accumulated spend you will graduate to the Diamond Class.",
                    Limit = 3000000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Diamond",
                    SystemName = "diamond",
                    Description = "You get every benefit of Gold class and also: <br> <br> Get instant Rewards for attaining the Diamond class, which you can choose from a list on your phone screen.  <br> <br> Get access to Diamond class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas and also Monthly rewards for being a Diamond member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruse liners. <br> <br> You get Hotel get away and more. You earn more on rewards percentage and have access to Airport partner lounges and early access to Diamond Class programs with special lunch at particular partner lounges.  <br> <br> Once you have reached a total of N5,000,000 accumulated spend you will graduate to the Platinum Class.",
                    Limit = 5000000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Platinum",
                    SystemName = "platinum",
                    Description = "You get every benefit of Diamond class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Get access to Platinum class type rewards and also early notifications to deals with rights higher than previous classes.  <br> <br> Get our Gold badge and also exclusive VIP treatment at Fuel stations, Cinemas, free car checkup, home delivery and also Monthly rewards for being a Platinum member.  <br> <br> You also get access to Monthly cruses with our partners at Lagos Boats and other cruise liners.  <br> <br> You get Hotel get away and more.  You earn more on rewards percentage and have access to Airport partner lounges, car rentals and early access to Platinum Class programs with special lunch at particular partner lounges. <br> <br> Once you have reached a total of N15,000,000 accumulated spend you will graduate to the Platinum Plus Class. ",
                    Limit = 15000000
                });
                _TucMileStones.Add(new OApp.Configuration.TucMileStone
                {
                    Title = "Platinum Plus",
                    SystemName = "platinumplus",
                    Description = "You get every benefit of Platinum class and also:  <br> <br> Get instant Rewards for attaining the Platinum class, which you can choose from a list on your phone screen.  <br> <br> Personalized gifts from our partners and also get Free Tickets to global networking events.  <br> <br> Also get access to our partner Platinum Airport Network and free upgrades on our Airline partners.",
                    Limit = 30000000
                });
                _AppConfiguration.TucMileStones = _TucMileStones;
                _Features = new List<OApp.Feature>();
                _Features.Add(new OApp.Feature { Name = "USSD Verfication", SystemName = "ussdverification", });
                _Features.Add(new OApp.Feature { Name = "Deals", SystemName = "deals" });
                _Features.Add(new OApp.Feature { Name = "BNPL", SystemName = "bnpl", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "Stores", SystemName = "stores" });
                _Features.Add(new OApp.Feature { Name = "Airtime", SystemName = "airtime" });
                _Features.Add(new OApp.Feature { Name = "Utility Bills", SystemName = "utilitybills" });
                _Features.Add(new OApp.Feature { Name = "Flight Booking", SystemName = "flightbooking" });
                _Features.Add(new OApp.Feature { Name = "Manage Banks", SystemName = "bankmanager" });



                _Countries = new List<OApp.Country>();
                _Countries.Add(new OApp.Country
                {
                    ReferenceId = 1,
                    ReferenceKey = "nigeria",
                    Name = "Nigeria",
                    IconUrl = _AppConfig.StorageUrl + "flag/ng.png",
                    Isd = "234",
                    Iso = "ng",
                    CurrencyName = "Naira",
                    CurrencyNotation = "NGN",
                    CurrencySymbol = "₦",
                    CurrencyHex = "&#8358;",
                    NumberLength = 10,
                    ActiveFeatures = _Features,
                });
                _Features = new List<OApp.Feature>();
                _Features.Add(new OApp.Feature { Name = "Deals", SystemName = "deals", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "BNPL", SystemName = "bnpl", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "Stores", SystemName = "stores", IsNew = true });
                _Countries.Add(new OApp.Country
                {
                    ReferenceId = 87,
                    ReferenceKey = "ghana",
                    Name = "Ghana",
                    IconUrl = _AppConfig.StorageUrl + "flag/gh.png",
                    Isd = "233",
                    Iso = "gh",
                    CurrencyName = "Cedi",
                    CurrencyNotation = "GHS",
                    CurrencySymbol = "₵",
                    CurrencyHex = "&#8373;",
                    NumberLength = 10,
                    ActiveFeatures = _Features,
                });
                _Features = new List<OApp.Feature>();
                _Features.Add(new OApp.Feature { Name = "Deals", SystemName = "deals", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "BNPL", SystemName = "bnpl", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "Stores", SystemName = "stores", IsNew = true });
                _Features.Add(new OApp.Feature { Name = "Paystack", SystemName = "paystack" });
                _Countries.Add(new OApp.Country
                {
                    ReferenceId = 118,
                    ReferenceKey = "kenya",
                    Name = "Kenya",
                    IconUrl = _AppConfig.StorageUrl + "flag/ke.png",
                    Isd = "254",
                    Iso = "key",
                    CurrencyName = "Shilling",
                    CurrencyNotation = "KHS",
                    CurrencySymbol = "KSh",
                    CurrencyHex = "KSh",
                    NumberLength = 9,
                    ActiveFeatures = _Features,
                });
                _AppConfiguration.Countries = _Countries;
                _AppConfiguration.AboutApp = "Welcome! ThankUCash is a rewards program operated by Connected Analytics Limited (referred to as ThankUCash or we or us). ThankUCash lets you earn ThankUCash rewards points (Points) by making qualifying purchases at certain retail and service companies and through other promotions and offers that may be provided by merchants (merchants whom we may also refer to as partners) or other merchants.";

                _Contacts = new List<OApp.ContactItem>();
                _Contacts.Add(new OApp.ContactItem
                {
                    type = "call",
                    icon = "las la-phone-volume",
                    title = "Call us on",
                    value = "0700 0433433",
                    systemvalue = "+2347000433433",
                });
                _Contacts.Add(new OApp.ContactItem
                {
                    type = "call",
                    icon = "las la-phone-volume",
                    title = "Call us on",
                    value = "01 888 8177",
                    systemvalue = "+23418888177",
                });
                _Contacts.Add(new OApp.ContactItem
                {
                    type = "whatsapp",
                    icon = "lab la-whatsapp",
                    title = "Message us on",
                    value = "0813 3178563",
                    systemvalue = "https://api.whatsapp.com/send?phone=+2348133178563",
                });
                _Contacts.Add(new OApp.ContactItem
                {
                    type = "email",
                    icon = "las la-envelope",
                    title = "Email us on",
                    value = "hello@thankucash.com",
                    systemvalue = "mailto:hello@thankucash.com",
                });
                _AppConfiguration.Contacts = _Contacts;
                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                //_Sliders = new List<OApp.Slider>();
                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _Promotions = _HCoreContext.TUCAppPromotion
                //        .Where(x => x.StatusId == HelperStatus.Default.Active
                //                && ActiveTime >= x.StartDate
                //                && ActiveTime <= x.EndDate)
                //        .Select(x => new
                //        {
                //            ImageUrl = _AppConfig.StorageUrl + x.ImageStorage.Path,
                //            NavigationType = x.NavigationType,
                //            NavigateUrl = x.NavigateUrl,
                //            NavigationData = x.NavigationData,
                //        }).Skip(0).Take(8).ToList();
                //    if (_Promotions.Count > 0)
                //    {
                //        foreach (var _Promotion in _Promotions)
                //        {
                //            _Sliders.Add(new OApp.Slider
                //            {
                //                ImageUrl = _Promotion.ImageUrl,
                //                NavigationType = _Promotion.NavigationType,
                //                NavigateUrl = _Promotion.NavigateUrl,
                //            });
                //        }
                //    }
                //}
                //if(_Sliders.Count > 0)
                //{
                //    _AppConfiguration.HomeSlider = _Sliders;
                //}
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppConfiguration, "CA0200", ResponseCode.CA0200);
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetAppConfiguration", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }

        //internal OResponse GetAppSliderImage(OReference _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        _AppConfiguration = new OApp.Configuration();
        //        _Countries = new List<OApp.Country>();
        //        _Countries.Add(new OApp.Country
        //        {
        //            ReferenceId = 1,
        //            ReferenceKey = "nigeria",
        //            Name = "Nigeria",
        //            IconUrl = "",
        //            Isd = "234",
        //            Iso = "ng",
        //            CurrencyName = "Naira",
        //            CurrencyNotation = "NGN",
        //            CurrencySymbol = "₦",
        //            CurrencyHex = "&#8358;",
        //            NumberLength = 10
        //        });
        //        _Countries.Add(new OApp.Country
        //        {
        //            ReferenceId = 87,
        //            ReferenceKey = "ghana",
        //            Name = "Ghana",
        //            IconUrl = "",
        //            Isd = "233",
        //            Iso = "gh",
        //            CurrencyName = "Cedi",
        //            CurrencyNotation = "GHS",
        //            CurrencySymbol = "₵",
        //            CurrencyHex = "&#8373;",
        //            NumberLength = 10
        //        });
        //        _Countries.Add(new OApp.Country
        //        {
        //            ReferenceId = 118,
        //            ReferenceKey = "kenya",
        //            Name = "Kenya",
        //            IconUrl = "",
        //            Isd = "254",
        //            Iso = "key",
        //            CurrencyName = "Shilling",
        //            CurrencyNotation = "KHS",
        //            CurrencySymbol = "KSh",
        //            CurrencyHex = "KSh",
        //            NumberLength = 9
        //        });
        //        _AppConfiguration.Countries = _Countries;

        //        _Sliders = new List<OApp.Slider>();
        //        _Sliders.Add(new OApp.Slider
        //        {
        //            ImageUrl = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/l/2022/1/f18461ba633341ceaebc7ba0ed98376d.png",
        //            NavigationType = "app",
        //            NavigateUrl = "dealdetails",
        //            NavigationData = new
        //            {
        //                ReferenceId = "",
        //                ReferenceKey = "fe108d5e7215413c9bcf193e6837344f",
        //            }
        //        });
        //        _Sliders.Add(new OApp.Slider
        //        {
        //            ImageUrl = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/l/2022/1/f18461ba633341ceaebc7ba0ed98376d.png",
        //            NavigationType = "app",
        //            NavigateUrl = "dealdetails",
        //            NavigationData = new
        //            {
        //                ReferenceId = "",
        //                ReferenceKey = "fe108d5e7215413c9bcf193e6837344f",
        //            }
        //        });
        //        _Sliders.Add(new OApp.Slider
        //        {
        //            ImageUrl = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/l/2022/1/f18461ba633341ceaebc7ba0ed98376d.png",
        //            NavigationType = "app",
        //            NavigateUrl = "dealdetails",
        //            NavigationData = new
        //            {
        //                ReferenceId = "",
        //                ReferenceKey = "fe108d5e7215413c9bcf193e6837344f",
        //            }
        //        });
        //        _AppConfiguration.HomeSlider = _Sliders;
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppConfiguration, "CA0200", ResponseCode.CA0200);
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "GetCountries", _Exception, _Request.UserReference);
        //        #endregion
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
        //    }
        //    #endregion
        //}

    }
}
