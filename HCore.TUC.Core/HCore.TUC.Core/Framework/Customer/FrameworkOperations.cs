//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to pin and referral bonus
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Delivery.Object.Response.Shipments;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;


namespace HCore.TUC.Core.Framework.Customer
{
    public class FrameworkOperations
    {

        HCoreContext? _HCoreContext;
        OCoreTransaction.Request? _CoreTransactionRequest;
        ManageCoreTransaction? _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;
        HCUAccountSubscription? _HCUAccountSubscription;
        OOperation.Balance.Response? _Balance;
        OOperation.Balance.Details? _BalanceDetails;
        HCUAccountParameter? _HCUAccountParameter;

        /// <summary>
        /// Description: Gets the balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBalance(OOperation.Balance.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Balance = new OOperation.Balance.Response();
                _ManageCoreTransaction = new ManageCoreTransaction();
                var TucBalance = _ManageCoreTransaction.GetAccountBalanceOverview(_Request.AccountId, TransactionSource.TUC);
                _BalanceDetails = new OOperation.Balance.Details();
                _BalanceDetails.Credit = TucBalance.Credit;
                _BalanceDetails.Debit = TucBalance.Debit;
                _BalanceDetails.Balance = TucBalance.Balance;
                _BalanceDetails.LastTransactionDate = TucBalance.LastTransactionDate;
                _Balance.ThankUCash = _BalanceDetails;
                if (_Request.Type != "tuc")
                {
                    var TucPlusBalance = _ManageCoreTransaction.GetAccountBalanceOverview(_Request.AccountId, TransactionSource.ThankUCashPlus);
                    _BalanceDetails = new OOperation.Balance.Details();
                    _BalanceDetails.Credit = TucPlusBalance.Credit;
                    _BalanceDetails.Debit = TucPlusBalance.Debit;
                    _BalanceDetails.Balance = TucPlusBalance.Balance;
                    _BalanceDetails.LastTransactionDate = TucPlusBalance.LastTransactionDate;
                    _Balance.ThankUCashPlus = _BalanceDetails;


                    var GiftCard = _ManageCoreTransaction.GetAccountBalanceOverview(_Request.AccountId, TransactionSource.GiftCards);
                    _BalanceDetails = new OOperation.Balance.Details();
                    _BalanceDetails.Credit = GiftCard.Credit;
                    _BalanceDetails.Debit = GiftCard.Debit;
                    _BalanceDetails.Balance = GiftCard.Balance;
                    _BalanceDetails.LastTransactionDate = GiftCard.LastTransactionDate;
                    _Balance.GiftCard = _BalanceDetails;

                    var GiftPoint = _ManageCoreTransaction.GetAccountBalanceOverview(_Request.AccountId, TransactionSource.GiftPoints);
                    _BalanceDetails = new OOperation.Balance.Details();
                    _BalanceDetails.Credit = GiftPoint.Credit;
                    _BalanceDetails.Debit = GiftPoint.Debit;
                    _BalanceDetails.Balance = GiftPoint.Balance;
                    _BalanceDetails.LastTransactionDate = GiftPoint.LastTransactionDate;
                    _Balance.GiftPoint = _BalanceDetails;
                }

                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Balance, "HC0001");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the referral code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateReferralCode(OOperation.ReferralCode.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.ReferralCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1174, TUCCoreResource.CA1174M);
                }
                else
                {
                    _Request.ReferralCode = _Request.ReferralCode.Trim();
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                        && x.Id == _Request.UserReference.AccountId
                                        && x.Guid == _Request.UserReference.AccountKey)
                           .FirstOrDefault();
                        if (CustomerDetails != null)
                        {
                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                            {
                                bool RefCheck = _HCoreContext.HCUAccount.Any(x => x.ReferralCode == _Request.ReferralCode);
                                if (!RefCheck)
                                {
                                    CustomerDetails.ReferralCode = _Request.ReferralCode;
                                    CustomerDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    CustomerDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1178, TUCCoreResource.CA1178M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1177, TUCCoreResource.CA1177M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1175, TUCCoreResource.CA1175M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        /// <summary>
        /// Description: Validates the referral bonus.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ValidateReferralBonus(OOperation.ReferralBonus.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Transaction = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _Request.ReferenceId
                    && x.AccountId == _Request.UserReference.AccountId
                    && x.TypeId == TransactionType.ReferralBonus
                    && x.SourceId == TransactionSource.TUC
                    && x.ModeId == TransactionMode.Credit)
                    .FirstOrDefault();
                    if (Transaction != null)
                    {
                        if (Transaction.StatusId == HelperStatus.Transaction.Pending)
                        {
                            DateTime ExpiaryTime = Transaction.TransactionDate.AddHours(24);
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            if (ExpiaryTime > CurrentTime)
                            {
                                Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Transaction.ModifyById = _Request.UserReference.AccountId;
                                Transaction.StatusId = HelperStatus.Transaction.Success;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1180, TUCCoreResource.CA1180M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1182, TUCCoreResource.CA1182M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1181, TUCCoreResource.CA1181M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1179, TUCCoreResource.CA1179M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePin(OOperation.PinManager.Update.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    _Request.AccountId = _Request.UserReference.AccountId;
                    _Request.AccountKey = _Request.UserReference.AccountKey;
                }
                if (string.IsNullOrEmpty(_Request.OldPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1255, TUCCoreResource.CA1255M);
                }
                if (string.IsNullOrEmpty(_Request.NewPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1256, TUCCoreResource.CA1256M);
                }
                if (_Request.NewPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1259, TUCCoreResource.CA1259M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.Id == _Request.AccountId
                            && x.Guid == _Request.AccountKey)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                string tOldPin = AccountDetails.AccessPin;
                                string OldPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                                if (OldPin == _Request.OldPin)
                                {
                                    AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewPin);
                                    AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    AccountDetails.ModifyById = _Request.AccountId;

                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountParameter.TypeId = HCoreConstant.UserDetailsChangeLog.ChangeAccessPin;
                                    _HCUAccountParameter.AccountId = _Request.AccountId;
                                    _HCUAccountParameter.Name = "Pin Change";
                                    _HCUAccountParameter.Value = tOldPin;
                                    _HCUAccountParameter.SubValue = AccountDetails.AccessPin;
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.CreatedById = _Request.AccountId;
                                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1260, TUCCoreResource.CA1260M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1258, TUCCoreResource.CA1258M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetPin(OOperation.PinManager.Update.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                        && x.Id == _Request.UserReference.AccountId
                        && x.Guid == _Request.UserReference.AccountKey)
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            DateTime BackHours = HCoreHelper.GetGMTDateTime().AddHours(-24);
                            int ResetCount = _HCoreContext.HCUAccountParameter.Count(x =>
                            x.AccountId == _Request.UserReference.AccountId
                            && x.TypeId == UserDetailsChangeLog.ForgotAccessPin
                            && x.CreateDate > BackHours);
                            if (ResetCount < 4)
                            {
                                string OldPin = AccountDetails.AccessPin;
                                string NewPin = HCoreHelper.GenerateRandomNumber(4);
                                AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                                AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                AccountDetails.ModifyById = _Request.UserReference.AccountId;
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.TypeId = UserDetailsChangeLog.ForgotAccessPin;
                                _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                                _HCUAccountParameter.Name = "Pin Reset";
                                _HCUAccountParameter.Value = OldPin;
                                _HCUAccountParameter.SubValue = AccountDetails.AccessPin;
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                _HCoreContext.SaveChanges();
                                if (HostEnvironment == HostEnvironmentType.Live)
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, AccountDetails.MobileNumber, "Your new transaction PIN is: " + NewPin + ".", AccountDetails.Id, null);
                                }
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14161, TUCCoreResource.CAA14161M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14162, TUCCoreResource.CAA14162M);
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                    }
                }


            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets app Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            try
            {
                return null;
                //#region Manage Operations

                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _Details = _HCoreContext.LogoSettings.Where(x => x.Panel == _Request.Panel && x.Status == 2)
                //        .Select(x => new OAdminUser.AuthUpdate.LogoResponse
                //        {

                //            ImageUrl = x.ImageStorage.Path,

                //        }).FirstOrDefault();

                //    if (_Details != null)
                //    {
                //        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                //        {
                //            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                //        }
                //        else
                //        {
                //            _Details.ImageUrl = _AppConfig.Default_Icon;
                //        }
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1547, TUCCoreResource.CA1547M);
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //}
                //#endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAdminLogo", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }

    }
}
