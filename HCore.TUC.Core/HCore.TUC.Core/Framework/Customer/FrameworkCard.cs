//==================================================================================
// FileName: FrameworkCard.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to cards
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;

namespace HCore.TUC.Core.Framework.Customer
{
    internal class FrameworkCard
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCard(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountParameter
                            .Where(x => x.AccountId == _Request.UserReference.AccountId
                            && x.Account.Guid == _Request.UserReference.AccountKey
                            && x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.TypeId == 465)
                            .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OCard.List
                                                {
                                                    ReferenceId = x.Id,
                                                    //ReferenceKey = x.Guid,
                                                    CardType = x.Name,
                                                    //Bin = x.SystemName,
                                                    CardEnd = x.Value,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCard.List> _List = _HCoreContext.HCUAccountParameter
                            .Where(x => x.AccountId == _Request.UserReference.AccountId
                            && x.Account.Guid == _Request.UserReference.AccountKey
                            && x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.TypeId == 465)
                            .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OCard.List
                                                {
                                                    ReferenceId = x.Id,
                                                    //ReferenceKey = x.Guid,
                                                    CardType = x.Name,
                                                    //Bin = x.SystemName,
                                                    CardEnd = x.Value,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCard", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Deletes the card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCard(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                //else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                //}
                else if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCUAccountParameter
                        .Where(x => x.AccountId == _Request.UserReference.AccountId
                        && x.Id == _Request.ReferenceId
                        && x.Account.Guid == _Request.UserReference.AccountKey
                        && x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.TypeId == 465).FirstOrDefault();
                        if (_Details != null)
                        {
                            _HCoreContext.HCUAccountParameter.Remove(_Details);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1292, TUCCoreResource.CA1292M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteCard", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

    }
}
