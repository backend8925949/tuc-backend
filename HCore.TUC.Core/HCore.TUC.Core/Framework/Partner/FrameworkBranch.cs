//==================================================================================
// FileName: FrameworkBranch.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to branch
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Resource;
namespace HCore.TUC.Core.Framework.Partner
{
    public class FrameworkBranch
    {
        #region Declare
        HCoreContext _HCoreContext;
        TUCBranch _TUCBranch;
        HCUAccountParameter _HCUAccountParameter;
        HCCoreParameter _HCCoreParameter;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        TUCBranchAccount _TUCBranchAccount;
        #endregion
        /// <summary>
        /// Description: Saves the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveBranch(OBranch.Request _Request)
        {
            #region Manage Exception
            try
            {
                return null;

                //if (_Request.AccountId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1011, TUCCoreResource.CA1011M);
                //}
                //if (string.IsNullOrEmpty(_Request.AccountKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1012, TUCCoreResource.CA1012M);
                //}
                //if (_Request.Manager == null)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1013, TUCCoreResource.CA1013M);
                //}
                //if (_Request.Manager.ReferenceId == 0 && string.IsNullOrEmpty(_Request.Manager.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1014, TUCCoreResource.CA1014M);
                //}
                //if (_Request.Manager.ReferenceId == 0 && string.IsNullOrEmpty(_Request.Manager.MobileNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1014, TUCCoreResource.CA1014M);
                //}
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1015, TUCCoreResource.CA1015M);
                //}
                //if (string.IsNullOrEmpty(_Request.DisplayName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1016, TUCCoreResource.CA1016M);
                //}
                //if (string.IsNullOrEmpty(_Request.BranchCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1017, TUCCoreResource.CA1017M);
                //}
                //if (string.IsNullOrEmpty(_Request.PhoneNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1018, TUCCoreResource.CA1018M);
                //}
                //if (string.IsNullOrEmpty(_Request.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1019, TUCCoreResource.CA1019M);
                //}
                //if (string.IsNullOrEmpty(_Request.StateName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1020, TUCCoreResource.CA1020M);
                //}
                //if (string.IsNullOrEmpty(_Request.CityName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1021, TUCCoreResource.CA1021M);
                //}
                //if (string.IsNullOrEmpty(_Request.RegionName) && _Request.RegionId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1022, TUCCoreResource.CA1022M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                //long AccountId = HCoreHelper.GetUserAccountId(_Request.AccountId, _Request.AccountKey, UserAccountType.Acquirer, _Request.UserReference);
                //if (AccountId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                //using (_HCoreContext = new HCoreContext())
                //{
                //    _TUCBranch = new TUCBranch();
                //    _TUCBranch.Guid = HCoreHelper.GenerateGuid();
                //    _TUCBranch.OwnerId = AccountId;
                //    _TUCBranch.Name = _Request.Name;
                //    _TUCBranch.DisplayName = _Request.DisplayName;
                //    _TUCBranch.BranchCode = _Request.BranchCode;
                //    _TUCBranch.PhoneNumber = _Request.PhoneNumber;
                //    _TUCBranch.EmailAddress = _Request.EmailAddress;
                //    _TUCBranch.Address = _Request.Address;
                //    _TUCBranch.Latitude = _Request.Latitude;
                //    _TUCBranch.Longitude = _Request.Longitude;
                //    _TUCBranch.CountryId = 1;
                //    _TUCBranch.CreateDate = HCoreHelper.GetGMTDateTime();
                //    _TUCBranch.CreatedById = _Request.UserReference.AccountId;
                //    _TUCBranch.StatusId = StatusId;
                //    // Manager Information
                //    if (_Request.Manager.ReferenceId != 0)
                //    {
                //        _TUCBranch.ManagerId = _Request.Manager.ReferenceId;
                //    }
                //    //else
                //    //{
                //    //    _HCUAccountAuth = new HCUAccountAuth();
                //    //    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                //    //    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                //    //    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                //    //    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                //    //    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                //    //    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                //    //    if (_Request.UserReference.AccountId != 0)
                //    //    {
                //    //        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                //    //    }
                //    //    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                //    //    #region Save UserAccount
                //    //    _HCUAccount = new HCUAccount();
                //    //    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                //    //    _HCUAccount.AccountTypeId = UserAccountType.Manager;
                //    //    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                //    //    _HCUAccount.OwnerId = _Request.AccountId;
                //    //    _HCUAccount.BankId = _Request.AccountId;
                //    //    _HCUAccount.DisplayName = _Request.Manager.Name;
                //    //    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                //    //    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                //    //    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber);
                //    //    _HCUAccount.RoleId = 1;
                //    //    if (_Request.UserReference.AppVersionId != 0)
                //    //    {
                //    //        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                //    //    }
                //    //    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                //    //    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                //    //    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //    //    if (_Request.UserReference.AccountId != 0)
                //    //    {
                //    //        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                //    //    }
                //    //    _HCUAccount.StatusId = HelperStatus.Default.Active;
                //    //    _HCUAccount.Name = _Request.Manager.Name;
                //    //    _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                //    //    _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                //    //    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                //    //    _HCUAccount.EmailVerificationStatus = 0;
                //    //    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //    //    _HCUAccount.NumberVerificationStatus = 0;
                //    //    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //    //    _HCUAccount.User = _HCUAccountAuth;
                //    //    #endregion
                //    //    //_TUCBranchAccount = new TUCBranchAccount();
                //    //    //_TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //    //    //if (_Request.OwnerId != 0)
                //    //    //{
                //    //    //    _TUCBranchAccount.OwnerId = _Request.OwnerId;
                //    //    //}
                //    //    //_TUCBranchAccount.Account = _HCUAccount;
                //    //    //_TUCBranchAccount.AccountLevelId = 6;
                //    //    //_TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //    //    //_TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //    //    //_TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //    //    //_TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //    //    _TUCBranch.Manager = _HCUAccount;
                //    //}
                //    // State Information
                //    long StateId = _HCoreContext.HCCoreCountryState.Where(x =>  x.Name == _Request.StateName).Select(x => x.Id).FirstOrDefault();
                //    if (StateId != 0)
                //    {
                //        _TUCBranch.StateId = StateId;
                //    }
                //    else
                //    {
                //        _HCCoreParameter = new HCCoreParameter();
                //        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                //        _HCCoreParameter.TypeId = HelperType.CountryRegion;
                //        _HCCoreParameter.Name = _Request.StateName;
                //        _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.StateName);
                //        _HCCoreParameter.AccountId = _Request.AccountId;
                //        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                //        _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                //        _TUCBranch.State = _HCCoreParameter;
                //    }
                //    // City Information
                //    long CityId = _HCoreContext.HCCoreCountryStateCity.Where(x =>  x.StatusId == _TUCBranch.StateId && x.Name == _Request.StateName).Select(x => x.Id).FirstOrDefault();
                //    if (CityId != 0)
                //    {
                //        _TUCBranch.CityId = CityId;
                //    }
                //    else 
                //    {
                //        _HCCoreParameter = new HCCoreParameter();
                //        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                //        _HCCoreParameter.TypeId = HelperType.CountryCity;
                //        _HCCoreParameter.ParentId = _TUCBranch.StateId;
                //        _HCCoreParameter.Name = _Request.CityName;
                //        _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.CityName);
                //        _HCCoreParameter.AccountId = _Request.AccountId;
                //        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                //        _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                //        _TUCBranch.City = _HCCoreParameter;
                //    }
                //    // Region Information
                //    if (_Request.RegionId != 0)
                //    {
                //        _TUCBranch.RegionId = _Request.RegionId;
                //    }
                //    else
                //    {
                //        _HCUAccountParameter = new HCUAccountParameter();
                //        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                //        _HCUAccountParameter.TypeId = HelperType.CountryCityArea;
                //        _HCUAccountParameter.Name = _Request.RegionName;
                //        _HCUAccountParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.RegionName);
                //        _HCUAccountParameter.AccountId = _Request.AccountId;
                //        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                //        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                //        _TUCBranch.Region = _HCUAccountParameter;
                //    }
                //    _HCoreContext.TUCBranch.Add(_TUCBranch);
                //    _HCoreContext.SaveChanges();
                //    long BranchId = _TUCBranch.Id;
                //    if (_Request.Manager.ReferenceId == 0 && !string.IsNullOrEmpty(_Request.Manager.EmailAddress))
                //    {
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            _HCUAccountAuth = new HCUAccountAuth();
                //            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                //            _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                //            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                //            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                //            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                //            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                //            if (_Request.UserReference.AccountId != 0)
                //            {
                //                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                //            }
                //            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                //            #region Save UserAccount
                //            _HCUAccount = new HCUAccount();
                //            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                //            _HCUAccount.AccountTypeId = UserAccountType.Manager;
                //            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                //            _HCUAccount.OwnerId = _Request.AccountId;
                //            _HCUAccount.BankId = _Request.AccountId;
                //            _HCUAccount.DisplayName = _Request.Manager.Name;
                //            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                //            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                //            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber);
                //            _HCUAccount.RoleId = 1;
                //            if (_Request.UserReference.AppVersionId != 0)
                //            {
                //                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                //            }
                //            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                //            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                //            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            if (_Request.UserReference.AccountId != 0)
                //            {
                //                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                //            }
                //            _HCUAccount.StatusId = HelperStatus.Default.Active;
                //            _HCUAccount.Name = _Request.Manager.Name;
                //            _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                //            _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                //            _HCUAccount.CountryId = _Request.UserReference.CountryId;
                //            _HCUAccount.EmailVerificationStatus = 0;
                //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //            _HCUAccount.NumberVerificationStatus = 0;
                //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //            _HCUAccount.User = _HCUAccountAuth;
                //            #endregion
                //            _TUCBranchAccount = new TUCBranchAccount();
                //            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //            _TUCBranchAccount.BranchId = BranchId;
                //            _TUCBranchAccount.Account = _HCUAccount;
                //            if (_Request.Manager.OwnerId != 0)
                //            {
                //                _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                //            }
                //            if (_Request.Manager.RoleId != 0)
                //            {
                //                _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.AccountLevelId = 6;
                //            }
                //            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                //            _HCoreContext.SaveChanges();
                //            long ManagerId = _TUCBranchAccount.Id;
                //            using (_HCoreContext = new HCoreContext())
                //            {
                //                TUCBranch ManagerUpdator = _HCoreContext.TUCBranch.Where(x => x.Id == BranchId).FirstOrDefault();
                //                if (ManagerUpdator != null)
                //                {
                //                    ManagerUpdator.ManagerId = ManagerId;
                //                    _HCoreContext.SaveChanges();
                //                }
                //                _HCoreContext.Dispose();
                //            }
                //        }
                //    }
                //    else
                //    {
                //        using(_HCoreContext = new HCoreContext())
                //        {
                //            var BrancAcc = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.Manager.ReferenceId).FirstOrDefault();
                //            BrancAcc.EndDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.StatusId = HelperStatus.Default.Suspended;
                //            BrancAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.ModifyById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount = new TUCBranchAccount();
                //            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //            _TUCBranchAccount.BranchId = BranchId;
                //            _TUCBranchAccount.AccountId = BrancAcc.AccountId;
                //            if (_Request.Manager.OwnerId != 0)
                //            {
                //                _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.OwnerId = AccountId; 
                //            }
                //            if (_Request.Manager.RoleId != 0)
                //            {
                //                _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.AccountLevelId = 6;
                //            }
                //            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                //            _HCoreContext.SaveChanges();
                //        }

                //    }
                //    var _Response = new
                //    {
                //        ReferenceId = _TUCBranch.Id,
                //        ReferenceKey = _TUCBranch.Guid,
                //    };
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1024, TUCCoreResource.CA1024M);
                //}


            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateBranch(OBranch.Request _Request)
        {
            #region Manage Exception
            try
            {
                return null;
                //if (_Request.ReferenceId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                //}
                //if (string.IsNullOrEmpty(_Request.ReferenceKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                //}
                //long AccountId = 0;
                //long? BranchManagerId = 0;
                //using (_HCoreContext = new HCoreContext())
                //{
                //   var Acc = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.ReferenceId
                //    && x.Guid == _Request.ReferenceKey).Select(x => new { x.OwnerId  , x.ManagerId}).FirstOrDefault();
                //    if (Acc == null)
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //    else
                //    {
                //        BranchManagerId = Acc.ManagerId;
                //        AccountId = Acc.OwnerId;
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            var BrancAcc = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.Manager.ReferenceId).FirstOrDefault();
                //            BrancAcc.EndDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.StatusId = HelperStatus.Default.Suspended;
                //            BrancAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.ModifyById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount = new TUCBranchAccount();
                //            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //            _TUCBranchAccount.BranchId = _Request.ReferenceId;
                //            _TUCBranchAccount.AccountId = BrancAcc.AccountId;
                //            if (_Request.Manager.OwnerId != 0)
                //            {
                //                _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                //            }
                //            if (_Request.Manager.RoleId != 0)
                //            {
                //                _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.AccountLevelId = 6;
                //            }
                //            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                //            _HCoreContext.SaveChanges();
                //        }
                //    }

                //    _HCoreContext.Dispose();
                //}
                //int StatusId = 0;
                //if (!string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //    if (StatusId == 0)
                //    {
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //    }
                //}
                //// State Information
                //long StateId = 0;
                //if (!string.IsNullOrEmpty(_Request.StateName))
                //{
                //    using (_HCoreContext = new HCoreContext())
                //    {

                //        StateId = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == HelperType.CountryRegion && x.Name == _Request.StateName).Select(x => x.Id).FirstOrDefault();
                //        if (StateId == 0)
                //        {
                //            _HCCoreParameter = new HCCoreParameter();
                //            _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                //            _HCCoreParameter.TypeId = HelperType.CountryRegion;
                //            _HCCoreParameter.Name = _Request.StateName;
                //            _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.StateName);
                //            _HCCoreParameter.AccountId = AccountId;
                //            _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                //            _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                //            _HCoreContext.SaveChanges();
                //            StateId = _HCCoreParameter.Id;
                //        }
                //    }
                //}
                //// City Information
                //long CityId = 0;
                //if (StateId > 0)
                //{
                //    if (!string.IsNullOrEmpty(_Request.CityName))
                //    {
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            CityId = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == HelperType.CountryCity && x.ParentId == StateId && x.Name == _Request.StateName).Select(x => x.Id).FirstOrDefault();
                //            if (CityId == 0)
                //            {
                //                _HCCoreParameter = new HCCoreParameter();
                //                _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                //                _HCCoreParameter.TypeId = HelperType.CountryCity;
                //                _HCCoreParameter.ParentId = StateId;
                //                _HCCoreParameter.Name = _Request.CityName;
                //                _HCCoreParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.CityName);
                //                _HCCoreParameter.AccountId = AccountId;
                //                _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //                _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                //                _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                //                _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                //                _HCoreContext.SaveChanges();
                //                CityId = _HCCoreParameter.Id;
                //            }
                //        }
                //    }
                //}
                //long RegionId = _Request.RegionId;
                //if (CityId > 0)
                //{
                //    if (RegionId == 0 && !string.IsNullOrEmpty(_Request.RegionName))
                //    {
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            // Region Information
                //            RegionId = _HCoreContext.HCUAccountParameter.Where(x => x.TypeId == HelperType.CountryCityArea && x.ParentId == CityId && x.Name == _Request.StateName).Select(x => x.Id).FirstOrDefault();
                //            if (RegionId == 0)
                //            {
                //                _HCUAccountParameter = new HCUAccountParameter();
                //                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                //                _HCUAccountParameter.TypeId = HelperType.CountryCityArea;
                //                _HCUAccountParameter.ParentId = CityId;
                //                _HCUAccountParameter.Name = _Request.RegionName;
                //                _HCUAccountParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.RegionName);
                //                _HCUAccountParameter.AccountId = AccountId;
                //                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                //                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                //                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                //                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                //                _HCoreContext.SaveChanges();
                //                RegionId = _HCUAccountParameter.Id;
                //            }
                //        }
                //    }
                //}


                //// Manager Information
                //long ManagerId = 0;
                //if (_Request.Manager != null)
                //{
                //    if (_Request.Manager.ReferenceId != 0)
                //    {
                //        if (_Request.Manager.ReferenceId != BranchManagerId)
                //        {
                //            ManagerId = _Request.Manager.ReferenceId;

                //        }
                //    }
                //    else
                //    {
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            _HCUAccountAuth = new HCUAccountAuth();
                //            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                //            _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                //            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                //            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                //            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                //            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                //            if (_Request.UserReference.AccountId != 0)
                //            {
                //                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                //            }
                //            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                //            #region Save UserAccount
                //            _HCUAccount = new HCUAccount();
                //            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                //            _HCUAccount.AccountTypeId = UserAccountType.Manager;
                //            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                //            _HCUAccount.OwnerId = AccountId;
                //            _HCUAccount.BankId = AccountId;
                //            _HCUAccount.DisplayName = _Request.Manager.Name;
                //            _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                //            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                //            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                //            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber);
                //            _HCUAccount.RoleId = 1;
                //            if (_Request.UserReference.AppVersionId != 0)
                //            {
                //                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                //            }
                //            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                //            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                //            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            if (_Request.UserReference.AccountId != 0)
                //            {
                //                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                //            }
                //            _HCUAccount.StatusId = HelperStatus.Default.Active;
                //            _HCUAccount.Name = _Request.Manager.Name;
                //            _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                //            _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                //            _HCUAccount.CountryId = _Request.UserReference.CountryId;
                //            _HCUAccount.EmailVerificationStatus = 0;
                //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //            _HCUAccount.NumberVerificationStatus = 0;
                //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                //            _HCUAccount.User = _HCUAccountAuth;
                //            #endregion
                //            _HCoreContext.HCUAccount.Add(_HCUAccount);
                //            _HCoreContext.SaveChanges();

                //            _TUCBranchAccount = new TUCBranchAccount();
                //            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //            _TUCBranchAccount.BranchId = _Request.ReferenceId;
                //            _TUCBranchAccount.Account = _HCUAccount;
                //            if (_Request.Manager.OwnerId != 0)
                //            {
                //                _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                //            }
                //            if (_Request.Manager.RoleId != 0)
                //            {
                //                _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.AccountLevelId = 6;
                //            }
                //            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                //            _HCoreContext.SaveChanges();
                //             ManagerId = _TUCBranchAccount.Id;
                //        }
                //    }
                //}
                //using (_HCoreContext = new HCoreContext())
                //{
                //    TUCBranch Details = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                //    if (Details != null)
                //    {
                //        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                //        {
                //            Details.Name = _Request.Name;
                //        }
                //        if (!string.IsNullOrEmpty(_Request.DisplayName) && Details.DisplayName != _Request.DisplayName)
                //        {
                //            Details.DisplayName = _Request.DisplayName;
                //        }
                //        if (!string.IsNullOrEmpty(_Request.BranchCode) && Details.BranchCode != _Request.BranchCode)
                //        {
                //            Details.BranchCode = _Request.BranchCode;
                //        }
                //        if (!string.IsNullOrEmpty(_Request.PhoneNumber) && Details.PhoneNumber != _Request.PhoneNumber)
                //        {
                //            Details.PhoneNumber = _Request.PhoneNumber;
                //        }
                //        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                //        {
                //            Details.EmailAddress = _Request.EmailAddress;
                //        }
                //        if (!string.IsNullOrEmpty(_Request.Address) && Details.Address != _Request.Address)
                //        {
                //            Details.Address = _Request.Address;
                //        }
                //        if (StateId > 0)
                //        {
                //            Details.StateId = StateId;
                //        }
                //        if (CityId > 0)
                //        {
                //            Details.CityId = CityId;
                //        }
                //        if (RegionId > 0)
                //        {
                //            Details.RegionId = RegionId;
                //        }
                //        if (ManagerId > 0 && Details.ManagerId != ManagerId)
                //        {
                //            Details.ManagerId = ManagerId;
                //        }
                //        Details.Latitude = _Request.Latitude;
                //        Details.Longitude = _Request.Longitude;
                //        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                //        Details.ModifyById = _Request.UserReference.AccountId;
                //        if (StatusId != 0)
                //        {
                //            Details.StatusId = StatusId;
                //        }
                //        _HCoreContext.SaveChanges();
                //        var _Response = new
                //        {
                //            ReferenceId = _Request.ReferenceId,
                //            ReferenceKey = _Request.ReferenceKey,
                //        };
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //}
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBranch(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    OBranch.Details Details = _HCoreContext.TUCBranch
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,

                                                    Stores = 0,
                                                    Terminals = 0,
                                                    ActiveTerminals = 0,

                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBranch", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBranch(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCBranch
                                                .Where(x => x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,

                                                    Stores = 0,
                                                    Terminals = 0,
                                                    ActiveTerminals = 0,

                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OBranch.List> Data = _HCoreContext.TUCBranch
                                                .Where(x => x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,
                                                    Stores = 0,
                                                    Terminals = 0,
                                                    ActiveTerminals = 0,
                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetBranch", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveManager(OManager.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.BranchId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1025, TUCCoreResource.CA1025M);
                }
                if (string.IsNullOrEmpty(_Request.BranchKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1026, TUCCoreResource.CA1026M);
                }
                if (_Request.RoleId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1027, TUCCoreResource.CA1027M);
                }
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1027, TUCCoreResource.CA1027M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1028, TUCCoreResource.CA1028M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1029, TUCCoreResource.CA1029M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1030, TUCCoreResource.CA1030M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var BranchDetails = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.BranchId)
                        .Select(x => new
                        {
                            OwnerId = x.OwnerId,
                        }).FirstOrDefault();
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.Manager;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = BranchDetails.OwnerId;
                    _HCUAccount.BankId = BranchDetails.OwnerId;
                    _HCUAccount.DisplayName = _Request.Name;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    _HCUAccount.RoleId = _Request.RoleId;
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _TUCBranchAccount = new TUCBranchAccount();
                    _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                    _TUCBranchAccount.BranchId = _Request.BranchId;
                    if (_Request.OwnerId != 0)
                    {
                        if (_Request.RoleId == 8)
                        {
                            _TUCBranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.AccountId).FirstOrDefault();
                        }
                        else
                        {
                            _TUCBranchAccount.OwnerId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.Id).FirstOrDefault();
                        }
                    }
                    _TUCBranchAccount.Account = _HCUAccount;
                    _TUCBranchAccount.AccountLevelId = _Request.RoleId;
                    _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                    _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                    _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _TUCBranchAccount.Id,
                        ReferenceKey = _TUCBranchAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateManager(OManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var BranchAccount = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (BranchAccount != null)
                    {
                        if (_Request.BranchId != 0 && BranchAccount.BranchId != _Request.BranchId)
                        {
                            BranchAccount.BranchId = _Request.BranchId;
                        }
                        if (_Request.RoleId != 0)
                        {
                            BranchAccount.AccountLevelId = _Request.RoleId;
                        }
                        if (_Request.OwnerId != 0)
                        {
                            if (BranchAccount.AccountLevelId == 8)
                            {
                                BranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.AccountId).FirstOrDefault();
                            }
                            else
                            {
                                BranchAccount.OwnerId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.Id).FirstOrDefault();
                            }
                            BranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.BranchId).Select(x => x.AccountId).FirstOrDefault();
                        }
                        BranchAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                        BranchAccount.ModifyById = _Request.UserReference.AccountId;
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == BranchAccount.AccountId).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.EmailAddress != _Request.Name)
                            {
                                AccountDetails.Name = _Request.Name;
                                AccountDetails.DisplayName = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                            {
                                AccountDetails.EmailAddress = _Request.EmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                AccountDetails.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                            }
                            if (_Request.RoleId != 0)
                            {
                                AccountDetails.RoleId = _Request.RoleId;
                            }
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                        }
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = BranchAccount.Id,
                            ReferenceKey = BranchAccount.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetManager(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    OManager.Details Details = _HCoreContext.TUCBranchAccount
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OManager.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.BranchId,
                                                    BranchKey = x.Branch.Guid,
                                                    BranchName = x.Branch.Name,
                                                    BranchAddress = x.Branch.Address,

                                                    OwnerId = x.Owner.Id,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,


                                                    Name = x.Account.Name,
                                                    MobileNumber = x.Account.MobileNumber,
                                                    EmailAddress = x.Account.EmailAddress,

                                                    CityName = x.Account.City.Name,
                                                    IconUrl = x.Account.IconStorage.Path,

                                                    Stores = 0,
                                                    Terminals = 0,

                                                    RoleId = x.AccountLevelId,
                                                    //RoleKey = x.Role.Guid,
                                                    //RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetManager(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubReferenceId != 0 && !string.IsNullOrEmpty(_Request.SubReferenceKey))
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCBranchAccount
                                                    .Where(x => x.Branch.OwnerId == _Request.ReferenceId
                                                    && x.StatusId != HelperStatus.Default.Suspended
                                                    && x.Branch.Owner.Guid == _Request.ReferenceKey
                                                    && x.BranchId == _Request.SubReferenceId
                                                    && x.Branch.Guid == _Request.SubReferenceKey)
                                                    .Select(x => new OManager.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,



                                                        OwnerId = x.Owner.Id,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,

                                                        Name = x.Account.Name,
                                                        MobileNumber = x.Account.MobileNumber,
                                                        EmailAddress = x.Account.EmailAddress,

                                                        CityName = x.Account.City.Name,
                                                        IconUrl = x.Account.IconStorage.Path,

                                                        Stores = 0,
                                                        Terminals = 0,

                                                        RoleId = x.AccountLevelId,
                                                        RoleKey = x.AccountLevel.Guid,
                                                        RoleName = x.AccountLevel.Name,

                                                        CreateDate = x.CreateDate,
                                                        CreatedById = x.CreatedById,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OManager.List> Data = _HCoreContext.TUCBranchAccount
                                                    .Where(x => x.Branch.OwnerId == _Request.ReferenceId
                                                    && x.StatusId != HelperStatus.Default.Suspended
                                                    && x.Branch.Owner.Guid == _Request.ReferenceKey
                                                    && x.BranchId == _Request.SubReferenceId
                                                    && x.Branch.Guid == _Request.SubReferenceKey)
                                                    .Select(x => new OManager.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,


                                                        OwnerId = x.Owner.Id,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,

                                                        Name = x.Account.Name,
                                                        MobileNumber = x.Account.MobileNumber,
                                                        EmailAddress = x.Account.EmailAddress,

                                                        CityName = x.Account.City.Name,
                                                        IconUrl = x.Account.IconStorage.Path,

                                                        Stores = 0,
                                                        Terminals = 0,

                                                        RoleId = x.AccountLevelId,
                                                        RoleKey = x.AccountLevel.Guid,
                                                        RoleName = x.AccountLevel.Name,

                                                        CreateDate = x.CreateDate,
                                                        CreatedById = x.CreatedById,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in Data)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCBranchAccount
                                                    .Where(x => x.Branch.OwnerId == _Request.ReferenceId && x.Branch.Owner.Guid == _Request.ReferenceKey && x.StatusId != HelperStatus.Default.Suspended && x.AccountLevelId != 9)
                                                    .Select(x => new OManager.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        BranchId = x.BranchId,
                                                        BranchKey = x.Branch.Guid,
                                                        BranchName = x.Branch.Name,
                                                        BranchAddress = x.Branch.Address,

                                                        OwnerId = x.Owner.Id,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,

                                                        Name = x.Account.Name,
                                                        MobileNumber = x.Account.MobileNumber,
                                                        EmailAddress = x.Account.EmailAddress,

                                                        CityName = x.Account.City.Name,
                                                        IconUrl = x.Account.IconStorage.Path,

                                                        Stores = 0,
                                                        Terminals = 0,

                                                        RoleId = x.AccountLevelId,
                                                        RoleKey = x.AccountLevel.Guid,
                                                        RoleName = x.AccountLevel.Name,

                                                        CreateDate = x.CreateDate,
                                                        CreatedById = x.CreatedById,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OManager.List> Data = _HCoreContext.TUCBranchAccount
                                                    .Where(x => x.Branch.OwnerId == _Request.ReferenceId && x.Branch.Owner.Guid == _Request.ReferenceKey && x.StatusId != HelperStatus.Default.Suspended && x.AccountLevelId != 9)
                                                    .Select(x => new OManager.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        BranchId = x.BranchId,
                                                        BranchKey = x.Branch.Guid,
                                                        BranchName = x.Branch.Name,
                                                        BranchAddress = x.Branch.Address,

                                                        OwnerId = x.Owner.Id,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,

                                                        Name = x.Account.Name,
                                                        MobileNumber = x.Account.MobileNumber,
                                                        EmailAddress = x.Account.EmailAddress,

                                                        CityName = x.Account.City.Name,
                                                        IconUrl = x.Account.IconStorage.Path,

                                                        Stores = 0,
                                                        Terminals = 0,

                                                        RoleId = x.AccountLevelId,
                                                        RoleKey = x.AccountLevel.Guid,
                                                        RoleName = x.AccountLevel.Name,

                                                        CreateDate = x.CreateDate,
                                                        CreatedById = x.CreatedById,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in Data)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetManager", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
