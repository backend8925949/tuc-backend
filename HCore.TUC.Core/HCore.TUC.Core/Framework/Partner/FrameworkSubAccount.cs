//==================================================================================
// FileName: FrameworkSubAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to subaccounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Resource;
namespace HCore.TUC.Core.Framework.Partner
{
    public class FrameworkSubAccount
    {

        #region Declare
        HCoreContext _HCoreContext;
        #endregion
        #region Declare
        //HCUAccountParameter _HCUAccountParameter;
        //HCCoreParameter _HCCoreParameter;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        //TUCBranchAccount _TUCBranchAccount;
        #endregion
        /// <summary>
        /// Description: Saves the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveSubAccount(OSubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0 )
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1031, TUCCoreResource.CA1031M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1032, TUCCoreResource.CA1032M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1033, TUCCoreResource.CA1033M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1034, TUCCoreResource.CA1034M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1035, TUCCoreResource.CA1035M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.AcquirerSubAccount;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.AccountId;
                    _HCUAccount.BankId = _Request.AccountId;
                    _HCUAccount.DisplayName = _Request.Name;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    if (_Request.RoleId != 0)
                    {
                        _HCUAccount.RoleId = _Request.RoleId;
                    }
                    else
                    {
                        _HCUAccount.RoleId = 7;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    long? ImageStorageId = null;
                    if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                    {
                        ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                    }
                    if (ImageStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var SaveDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _HCUAccount.Id && x.Guid == _HCUAccount.Guid).FirstOrDefault();
                            if (SaveDetails != null)
                            {
                                if (ImageStorageId != null)
                                {
                                    SaveDetails.IconStorageId = (long)ImageStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1036, TUCCoreResource.CA1036M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveSubAccount", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateSubAccount(OSubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.AcquirerSubAccount).FirstOrDefault();
                    if (AccountDetails != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.EmailAddress != _Request.Name)
                        {
                            AccountDetails.Name = _Request.Name;
                            AccountDetails.DisplayName = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                        {
                            AccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            AccountDetails.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        }
                        if (_Request.RoleId != 0)
                        {
                            AccountDetails.RoleId = _Request.RoleId;
                        }
                        if ( StatusId != 0)
                        {
                            AccountDetails.StatusId = StatusId;
                        }
                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        AccountDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        long? ImageStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, AccountDetails.IconStorageId, _Request.UserReference);
                        }
                        if (ImageStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var _AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == AccountDetails.Guid).FirstOrDefault();
                                if (_AccountDetails != null)
                                {
                                    if (ImageStorageId != null)
                                    {
                                        _AccountDetails.IconStorageId = ImageStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);

                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubAccount(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    OSubAccount.Details Details = _HCoreContext.HCUAccount
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey
                                                && x.AccountTypeId == UserAccountType.AcquirerSubAccount)
                                                .Select(x => new OSubAccount.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.IconStorage.Path,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the list of  sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.AcquirerSubAccount && x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OSubAccount.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.IconStorage.Path,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSubAccount.List> Data = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.AcquirerSubAccount && x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OSubAccount.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.IconStorage.Path,
                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
