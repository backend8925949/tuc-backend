//==================================================================================
// FileName: ActorOnboardMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Akka.Actor;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Partner;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Text;
using HCore.Data;
using HCore.Data.Models;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Operations.Operations;
using HCore.TUC.Core.Framework.Operations;

namespace HCore.TUC.Core.Framework.Partner.Actor
{

    internal class ActorPartnerOnboardMerchant : ReceiveActor
    {
        HCOMerchantUpload _HCOMerchantUpload;
        HCoreContextOperations _HCoreContextOperations;
        public ActorPartnerOnboardMerchant()
        {
            Receive<OOnboarding.Merchant.Request>(_Request =>
            {
                try
                {
                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        foreach (var DataItem in _Request.Data)
                        {
                            int StatusId = 1;
                            string StatusMessage = "Pending";
                            if (string.IsNullOrEmpty(DataItem.DisplayName))
                            {
                                StatusId = 4;
                                StatusMessage = "Display name missing";
                            }
                            if (string.IsNullOrEmpty(DataItem.Name))
                            {
                                StatusId = 4;
                                StatusMessage = "Name missing";
                            }
                            if (string.IsNullOrEmpty(DataItem.ContactNumber))
                            {
                                StatusId = 4;
                                StatusMessage = "Contact number missing";
                            }
                            if (string.IsNullOrEmpty(DataItem.EmailAddress))
                            {
                                StatusId = 4;
                                StatusMessage = "Email address missing";
                            }
                            if (string.IsNullOrEmpty(DataItem.Address))
                            {
                                StatusId = 4;
                                StatusMessage = "Address missing";
                            }
                            //if (!string.IsNullOrEmpty(DataItem.Address))
                            //{
                            //    StatusId = 3;
                            //    StatusMessage = "Email address missing";
                            //}
                            if (string.IsNullOrEmpty(DataItem.CountryName))
                            {
                                StatusId = 4;
                                StatusMessage = "Country name missing";
                            }
                            if (!string.IsNullOrEmpty(DataItem.ReferenceNumber))
                            {
                                bool Item = _HCoreContextOperations.HCOMerchantUpload.Any(x => x.OwnerId == _Request.AccountId && x.ReferenceNumber == DataItem.ReferenceNumber && (x.StatusId == 1 || x.StatusId == 2));
                                if (Item)
                                {
                                    StatusId = 4;
                                    StatusMessage = "Duplicate Reference";
                                }
                            }

                            _HCOMerchantUpload = new HCOMerchantUpload();
                            _HCOMerchantUpload.Guid = HCoreHelper.GenerateGuid();
                            _HCOMerchantUpload.OwnerId = _Request.AccountId;
                            _HCOMerchantUpload.DisplayName = DataItem.DisplayName;
                            _HCOMerchantUpload.RewardPercentage = DataItem.RewardPercentage;
                            _HCOMerchantUpload.Name = DataItem.Name;
                            _HCOMerchantUpload.ContactNumber = DataItem.ContactNumber;
                            _HCOMerchantUpload.EmailAddress = DataItem.EmailAddress;
                            _HCOMerchantUpload.Address = DataItem.Address;
                            _HCOMerchantUpload.CityAreaName = DataItem.CityAreaName;
                            _HCOMerchantUpload.CityName = DataItem.CityName;
                            _HCOMerchantUpload.StateName = DataItem.StateName;
                            if (string.IsNullOrEmpty(DataItem.CountryName))
                            {
                                _HCOMerchantUpload.CountryName = _Request.UserReference.CountryName;
                            }
                            else
                            {
                                _HCOMerchantUpload.CountryName = DataItem.CountryName;
                            }
                            _HCOMerchantUpload.Latitude = DataItem.Latitude;
                            _HCOMerchantUpload.Longitude = DataItem.Longitude;
                            _HCOMerchantUpload.AccountId = DataItem.AccountId;
                            _HCOMerchantUpload.CategoryName = DataItem.CategoryName;
                            _HCOMerchantUpload.ContactPersonName = DataItem.ContactPersonName;
                            _HCOMerchantUpload.ContactPersonMobileNumber = DataItem.ContactPersonMobileNumber;
                            _HCOMerchantUpload.ContactPersonEmailAddress = DataItem.ContactPersonEmailAddress;
                            _HCOMerchantUpload.ReferenceNumber = DataItem.ReferenceNumber;
                            _HCOMerchantUpload.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCOMerchantUpload.CreatedById = _Request.UserReference.AccountId;
                            _HCOMerchantUpload.StatusId = StatusId;
                            _HCOMerchantUpload.StatusMessage = StatusMessage;
                            _HCoreContextOperations.HCOMerchantUpload.Add(_HCOMerchantUpload);
                        }
                        _HCoreContextOperations.SaveChanges();
                    }

                    var system = ActorSystem.Create("ActorPartnerProcessOnboardMerchant");
                    var greeter = system.ActorOf<ActorPartnerProcessOnboardMerchant>("ActorPartnerProcessOnboardMerchant");
                    greeter.Tell(_Request.AccountId);
                }
                catch (Exception ex)
                {
                    HCoreHelper.LogException("PARTNER-SAVETEMPMERCHANT-ActorProcessOnboardMerchant", ex);
                }

            });
        }
    }
    internal class ActorPartnerProcessOnboardMerchant : ReceiveActor
    {
        HCoreContext _HCoreContext;
        OAddress _Address;
        //HCOMerchantUpload _HCOMerchantUpload;
        HCoreContextOperations _HCoreContextOperations;
        Random _Random;
        List<HCUAccountParameter> _HCUAccountParameters;
        HCUAccountParameter _HCUAccountParameter;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        FrameworkSubscription _FrameworkSubscription;
        public ActorPartnerProcessOnboardMerchant()
        {
            Receive<long>(_Request =>
            {

                try
                {
                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var Merchants = _HCoreContextOperations.HCOMerchantUpload.Where(x => x.StatusId == 1 && x.AccountId == null).Skip(0).Take(1000).ToList();
                        foreach (var DataItem in Merchants)
                        {
                            _Address = new OAddress();
                            _Address.Address = DataItem.Address;
                            _Address.CityAreaName = DataItem.CityAreaName;
                            _Address.StateName = DataItem.StateName;
                            _Address.CountryName = DataItem.CountryName;
                            if (DataItem.Latitude != null)
                            {
                                _Address.Latitude = (double)DataItem.Latitude;
                            }
                            if (DataItem.Longitude != null)
                            {
                                _Address.Longitude = (double)DataItem.Longitude;
                            }
                            OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Address, null);
                            using (_HCoreContext = new HCoreContext())
                            {
                                bool ValidateName = _HCoreContext.HCUAccount.Any(x => x.User.Username == DataItem.EmailAddress);
                                if (!ValidateName)
                                {
                                    _Random = new Random();
                                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                                    if (DataItem.RewardPercentage != null && DataItem.RewardPercentage > 0)
                                    {
                                        _HCUAccountParameters = new List<HCUAccountParameter>();
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = DataItem.RewardPercentage.ToString();
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = DataItem.CreatedById;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameters.Add(_HCUAccountParameter);
                                    }

                                    _HCUAccountAuth = new HCUAccountAuth();
                                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountAuth.Username = DataItem.EmailAddress;
                                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash("Rewards@4321");
                                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountAuth.CreatedById = DataItem.CreatedById;
                                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                    #region Save UserAccount
                                    _HCUAccount = new HCUAccount();
                                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                                    _HCUAccount.OwnerId = DataItem.OwnerId;
                                    _HCUAccount.BankId = DataItem.OwnerId;
                                    _HCUAccount.DisplayName = DataItem.DisplayName;
                                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(DataItem.DisplayName);
                                    _HCUAccount.Name = DataItem.Name;
                                    _HCUAccount.EmailAddress = DataItem.EmailAddress;
                                    _HCUAccount.ContactNumber = DataItem.ContactNumber;
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                                    _HCUAccount.AccountPercentage = DataItem.RewardPercentage;
                                    _HCUAccount.FirstName = DataItem.ContactPersonName;
                                    _HCUAccount.LastName = DataItem.ContactPersonEmailAddress;
                                    _HCUAccount.SecondaryEmailAddress = DataItem.ContactPersonEmailAddress;
                                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber("234", DataItem.ContactPersonMobileNumber);
                                    _HCUAccount.Address = _AddressResponse.Address;
                                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                                    if (_AddressResponse.StateId != 0)
                                    {
                                        _HCUAccount.StateId = _AddressResponse.StateId;
                                    }
                                    if (_AddressResponse.CityId != 0)
                                    {
                                        _HCUAccount.CityId = _AddressResponse.CityId;
                                    }
                                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccount.CreatedById = DataItem.AccountId;
                                    _HCUAccount.StatusId = 2;
                                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                                    _HCUAccount.EmailVerificationStatus = 0;
                                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccount.NumberVerificationStatus = 0;
                                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccount.AccountPercentage = 0;
                                    _HCUAccount.ReferenceNumber = DataItem.ReferenceNumber;
                                    _HCUAccount.User = _HCUAccountAuth;
                                    if (_HCUAccountParameters != null && _HCUAccountParameters.Count > 0)
                                    {
                                        _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                                    }
                                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                                    #endregion
                                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                                    _HCoreContext.SaveChanges();
                                    long MerchantId = _HCUAccount.Id;
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _HCUAccountAuth = new HCUAccountAuth();
                                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash("Rewards@4321");
                                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountAuth.CreatedById = DataItem.AccountId;
                                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                        #region Save UserAccount
                                        _HCUAccount = new HCUAccount();
                                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                        _HCUAccount.OwnerId = MerchantId;
                                        _HCUAccount.DisplayName = DataItem.DisplayName;
                                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(DataItem.DisplayName);
                                        _HCUAccount.Name = DataItem.Name;
                                        _HCUAccount.EmailAddress = DataItem.EmailAddress;
                                        _HCUAccount.ContactNumber = DataItem.ContactNumber;
                                        _HCUAccount.FirstName = DataItem.ContactPersonName;
                                        _HCUAccount.SecondaryEmailAddress = DataItem.ContactPersonEmailAddress;
                                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber("234", DataItem.ContactPersonMobileNumber);
                                        _HCUAccount.Address = _AddressResponse.Address;
                                        _HCUAccount.Latitude = _AddressResponse.Latitude;
                                        _HCUAccount.Longitude = _AddressResponse.Longitude;
                                        _HCUAccount.CountryId = _AddressResponse.CountryId;
                                        if (_AddressResponse.StateId != 0)
                                        {
                                            _HCUAccount.StateId = _AddressResponse.StateId;
                                        }
                                        if (_AddressResponse.CityId != 0)
                                        {
                                            _HCUAccount.CityId = _AddressResponse.CityId;
                                        }
                                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccount.CreatedById = DataItem.AccountId;
                                        _HCUAccount.StatusId = 2;
                                        _HCUAccount.EmailVerificationStatus = 0;
                                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccount.NumberVerificationStatus = 0;
                                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccount.User = _HCUAccountAuth;
                                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                                        #endregion
                                        _HCoreContext.SaveChanges();
                                    }
                                    _FrameworkSubscription = new FrameworkSubscription();
                                    _FrameworkSubscription.SetPartnerReferralMerchantSubscription(MerchantId);
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var tITem = _HCoreContextOperations.HCOMerchantUpload.Where(x => x.Id == DataItem.Id).FirstOrDefault();
                                        if (tITem != null)
                                        {
                                            tITem.AccountId = MerchantId;
                                            tITem.StatusId = 3;
                                            tITem.StatusMessage = "Merchant Created";
                                            tITem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var tITem = _HCoreContextOperations.HCOMerchantUpload.Where(x => x.Id == DataItem.Id).FirstOrDefault();
                                        if (tITem != null)
                                        {
                                            tITem.StatusId = 4;
                                            tITem.StatusMessage = "Account already exists";
                                            tITem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                }
                                //Operations.FrameworkSubscription _FrameworkSubscription = new Operations.FrameworkSubscription();
                                //_FrameworkSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    HCoreHelper.LogException("PART-PROCESSMERCHANT-ActorProcessOnboardMerchant", ex);
                }

            });
        }
    }

    //internal class ActorPartnerOnboardMerchant : ReceiveActor
    //{
    //    HCOMerchantUpload _HCOMerchantUpload;
    //    HCoreContextOperations _HCoreContextOperations;
    //    public ActorPartnerOnboardMerchant()
    //    {
    //        Receive<OOnboarding.Merchant.Request>(_Request =>
    //        {
    //            using (_HCoreContextOperations = new HCoreContextOperations())
    //            {
    //                foreach (var DataItem in _Request.Data)
    //                {
    //                    if (!string.IsNullOrEmpty(DataItem.ReferenceNumber))
    //                    {
    //                        bool Item = _HCoreContextOperations.HCOMerchantUpload.Any(x => x.ReferenceNumber == DataItem.ReferenceNumber);
    //                        if (!Item)
    //                        {
    //                            _HCOMerchantUpload = new HCOMerchantUpload();
    //                            _HCOMerchantUpload.Guid = HCoreHelper.GenerateGuid();
    //                            _HCOMerchantUpload.OwnerId = _Request.AccountId;
    //                            _HCOMerchantUpload.DisplayName = DataItem.DisplayName;
    //                            _HCOMerchantUpload.Name = DataItem.Name;
    //                            _HCOMerchantUpload.ContactNumber = DataItem.ContactNumber;
    //                            _HCOMerchantUpload.EmailAddress = DataItem.EmailAddress;
    //                            _HCOMerchantUpload.Address = DataItem.Address;
    //                            _HCOMerchantUpload.CityAreaName = DataItem.CityAreaName;
    //                            _HCOMerchantUpload.CityName = DataItem.CityName;
    //                            _HCOMerchantUpload.StateName = DataItem.StateName;
    //                            _HCOMerchantUpload.CountryName = DataItem.CountryName;
    //                            _HCOMerchantUpload.Latitude = DataItem.Latitude;
    //                            _HCOMerchantUpload.Longitude = DataItem.Longitude;
    //                            _HCOMerchantUpload.AccountId = DataItem.AccountId;
    //                            _HCOMerchantUpload.CategoryName = DataItem.CategoryName;
    //                            _HCOMerchantUpload.ContactPersonName = DataItem.ContactPersonName;
    //                            _HCOMerchantUpload.ContactPersonMobileNumber = DataItem.ContactPersonMobileNumber;
    //                            _HCOMerchantUpload.ContactPersonEmailAddress = DataItem.ContactPersonEmailAddress;
    //                            _HCOMerchantUpload.ReferenceNumber = DataItem.ReferenceNumber;
    //                            _HCOMerchantUpload.CreateDate = HCoreHelper.GetGMTDateTime();
    //                            _HCOMerchantUpload.CreatedById = _Request.UserReference.AccountId;
    //                            _HCOMerchantUpload.StatusId = 1;
    //                            _HCOMerchantUpload.StatusMessage = "Pending";
    //                            _HCoreContextOperations.HCOMerchantUpload.Add(_HCOMerchantUpload);
    //                        }
    //                    }
    //                }
    //                _HCoreContextOperations.SaveChanges();
    //            }

    //            var system = ActorSystem.Create("ActorPartnerProcessOnboardMerchant");
    //            var greeter = system.ActorOf<ActorPartnerProcessOnboardMerchant>("ActorPartnerProcessOnboardMerchant");
    //            greeter.Tell(_Request.AccountId);
    //        });
    //    }
    //}

    //internal class ActorPartnerProcessOnboardMerchant : ReceiveActor
    //{
    //    HCoreContext _HCoreContext;
    //    OAddress _Address;
    //    //HCOMerchantUpload _HCOMerchantUpload;
    //    HCoreContextOperations _HCoreContextOperations;
    //    Random _Random;
    //    List<HCUAccountParameter> _HCUAccountParameters;
    //    HCUAccountParameter _HCUAccountParameter;
    //    HCUAccountAuth _HCUAccountAuth;
    //    HCUAccount _HCUAccount;
    //    public ActorPartnerProcessOnboardMerchant()
    //    {
    //        Receive<long>(_Request =>
    //        {
    //            using (_HCoreContextOperations = new HCoreContextOperations())
    //            {
    //                var Merchants = _HCoreContextOperations.HCOMerchantUpload.Where(x => x.StatusId == 1).Skip(0).Take(1000).ToList();
    //                foreach (var DataItem in Merchants)
    //                {
    //                    _Address = new OAddress();
    //                    _Address.Address = DataItem.Address;
    //                    _Address.CityAreaName = DataItem.CityAreaName;
    //                    _Address.StateName = DataItem.StateName;
    //                    _Address.CountryName = DataItem.CountryName;
    //                    if (DataItem.Latitude != null)
    //                    {
    //                        _Address.Latitude = (double)DataItem.Latitude;
    //                    }
    //                    if (DataItem.Longitude != null)
    //                    {
    //                        _Address.Longitude = (double)DataItem.Longitude;
    //                    }
    //                    OAddressResponse _AddressResponse = HCoreHelper.GetAddressIds(_Address, null);
    //                    using (_HCoreContext = new HCoreContext())
    //                    {
    //                        _Random = new Random();
    //                        string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
    //                        _HCUAccountParameters = new List<HCUAccountParameter>();
    //                        _HCUAccountParameter = new HCUAccountParameter();
    //                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
    //                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
    //                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
    //                        _HCUAccountParameter.Value = DataItem.RewardPercentage.ToString();
    //                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccountParameter.CreatedById = DataItem.CreatedById;
    //                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
    //                        _HCUAccountParameters.Add(_HCUAccountParameter);
    //                        _HCUAccountAuth = new HCUAccountAuth();
    //                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
    //                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
    //                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
    //                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
    //                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
    //                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccountAuth.CreatedById = DataItem.CreatedById;
    //                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
    //                        #region Save UserAccount
    //                        _HCUAccount = new HCUAccount();
    //                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
    //                        _HCUAccount.AccountTypeId = UserAccountType.Merchant;
    //                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
    //                        _HCUAccount.OwnerId = DataItem.OwnerId;
    //                        _HCUAccount.BankId = DataItem.OwnerId;
    //                        _HCUAccount.DisplayName = DataItem.DisplayName;
    //                        _HCUAccount.Name = DataItem.Name;
    //                        _HCUAccount.EmailAddress = DataItem.EmailAddress;
    //                        _HCUAccount.ContactNumber = DataItem.ContactNumber;
    //                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
    //                        _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
    //                        _HCUAccount.AccountPercentage = DataItem.RewardPercentage;
    //                        _HCUAccount.FirstName = DataItem.ContactPersonName;
    //                        _HCUAccount.LastName = DataItem.ContactPersonEmailAddress;
    //                        _HCUAccount.SecondaryEmailAddress = DataItem.ContactPersonEmailAddress;
    //                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber("234", DataItem.ContactPersonMobileNumber);
    //                        _HCUAccount.Address = _AddressResponse.Address;
    //                        _HCUAccount.Latitude = _AddressResponse.Latitude;
    //                        _HCUAccount.Longitude = _AddressResponse.Longitude;
    //                        _HCUAccount.CountryId = _AddressResponse.CountryId;
    //                        if (_AddressResponse.StateId != 0)
    //                        {
    //                            _HCUAccount.RegionId = _AddressResponse.StateId;
    //                        }
    //                        if (_AddressResponse.CityId != 0)
    //                        {
    //                            _HCUAccount.CityId = _AddressResponse.CityId;
    //                        }
    //                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
    //                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccount.CreatedById = DataItem.AccountId;
    //                        _HCUAccount.StatusId = 1;
    //                        _HCUAccount.CountryId = 1;
    //                        _HCUAccount.EmailVerificationStatus = 0;
    //                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccount.NumberVerificationStatus = 0;
    //                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
    //                        _HCUAccount.AccountPercentage = 0;
    //                        _HCUAccount.ReferenceNumber = DataItem.ReferenceNumber;
    //                        _HCUAccount.User = _HCUAccountAuth;
    //                        _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
    //                        _HCUAccount.StatusId = HelperStatus.Default.Active;
    //                        #endregion
    //                        _HCoreContext.HCUAccount.Add(_HCUAccount);
    //                        _HCoreContext.SaveChanges();
    //                        long MerchantId = _HCUAccount.Id;
    //                        using (_HCoreContext = new HCoreContext())
    //                        {
    //                            _HCUAccountAuth = new HCUAccountAuth();
    //                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
    //                            _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
    //                            _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
    //                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
    //                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
    //                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
    //                            _HCUAccountAuth.CreatedById = DataItem.AccountId;
    //                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
    //                            #region Save UserAccount
    //                            _HCUAccount = new HCUAccount();
    //                            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
    //                            _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
    //                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
    //                            _HCUAccount.OwnerId = MerchantId;
    //                            _HCUAccount.DisplayName = DataItem.DisplayName;
    //                            _HCUAccount.Name = DataItem.Name;
    //                            _HCUAccount.EmailAddress = DataItem.EmailAddress;
    //                            _HCUAccount.ContactNumber = DataItem.ContactNumber;
    //                            _HCUAccount.FirstName = DataItem.ContactPersonName;
    //                            _HCUAccount.SecondaryEmailAddress = DataItem.ContactPersonEmailAddress;
    //                            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber("234", DataItem.ContactPersonMobileNumber);
    //                            _HCUAccount.Address = _AddressResponse.Address;
    //                            _HCUAccount.Latitude = _AddressResponse.Latitude;
    //                            _HCUAccount.Longitude = _AddressResponse.Longitude;
    //                            _HCUAccount.CountryId = _AddressResponse.CountryId;
    //                            if (_AddressResponse.StateId != 0)
    //                            {
    //                                _HCUAccount.RegionId = _AddressResponse.StateId;
    //                            }
    //                            if (_AddressResponse.CityId != 0)
    //                            {
    //                                _HCUAccount.CityId = _AddressResponse.CityId;
    //                            }
    //                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
    //                            _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
    //                            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
    //                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
    //                            _HCUAccount.CreatedById = DataItem.AccountId;
    //                            _HCUAccount.StatusId = 1;
    //                            _HCUAccount.EmailVerificationStatus = 0;
    //                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
    //                            _HCUAccount.NumberVerificationStatus = 0;
    //                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
    //                            _HCUAccount.User = _HCUAccountAuth;
    //                            _HCUAccount.StatusId = HelperStatus.Default.Active;
    //                            _HCoreContext.HCUAccount.Add(_HCUAccount);
    //                            #endregion
    //                            _HCoreContext.SaveChanges();
    //                        }
    //                        using (_HCoreContextOperations = new HCoreContextOperations())
    //                        {
    //                            var tITem = _HCoreContextOperations.HCOMerchantUpload.Where(x => x.Id == DataItem.Id).FirstOrDefault();
    //                            if (tITem != null)
    //                            {
    //                                tITem.AccountId = MerchantId;
    //                                tITem.StatusId = 2;
    //                                tITem.StatusMessage = "Merchant Created";
    //                                tITem.ModifyDate = HCoreHelper.GetGMTDateTime();
    //                                _HCoreContextOperations.SaveChanges();
    //                            }
    //                        }
    //                        //ManageSubscription _ManageSubscription = new ManageSubscription();
    //                        //_ManageSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);
    //                    }
    //                }
    //            }
    //        });
    //    }
    //}


}
