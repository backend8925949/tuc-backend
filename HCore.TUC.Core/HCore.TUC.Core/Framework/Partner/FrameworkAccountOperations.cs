//==================================================================================
// FileName: FrameworkAccountOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Resource;
using System.Collections.Generic;

namespace HCore.TUC.Core.Framework.Partner
{
    public class FrameworkAccountOperations
    {
        #region Declare
        HCoreContext _HCoreContext;
        TUCTerminal _TUCTerminal;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        TUCBranchAccount _TUCBranchAccount;
        Random _Random;
        HCUAccountParameter _HCUAccountParameter;
        List<HCUAccountParameter> _HCUAccountParameters;
        #endregion
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveMerchant(OAccounts.Merchant.Onboarding _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    bool ValidateName = _HCoreContext.HCUAccount.Any(x => x.User.Username == _Request.EmailAddress);
                    if (ValidateName)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1432, TUCCoreResource.CA1432M);
                    }
                    _Random = new Random();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.EmailAddress;
                    _HCUAccountAuth.Password =HCoreEncrypt.EncryptHash( HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = _Request.ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.AccountId;
                    _HCUAccount.BankId = _Request.AccountId;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    if (_Request.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = _Request.OwnerId;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                            }
                        }
                    }
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.FirstName = _Request.BusinessOwnerName;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    _HCUAccount.MobileNumber = _Request.MobileNumber;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    if (_Request.ContactPerson != null)
                    {
                        _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                        _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                    }
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = StatusId;
                    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = _Request.RewardPercentage;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    if (_Request.Stores != null)
                    {
                        foreach (var _Store in _Request.Stores)
                        {
                            OAddressResponse _StoreAddress = HCoreHelper.GetAddressComponent(_Store.AddressComponent, _Request.UserReference);
                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #region Save UserAccount
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.BankId = _Request.AccountId;
                                _HCUAccount.DisplayName = _Store.DisplayName;
                                _HCUAccount.Name = _Store.Name;
                                _HCUAccount.EmailAddress = _Store.EmailAddress;
                                _HCUAccount.ContactNumber = _Store.ContactNumber;
                                if (_Store.ContactPerson != null)
                                {
                                    _HCUAccount.FirstName = _Store.ContactPerson.FirstName;
                                    _HCUAccount.LastName = _Store.ContactPerson.LastName;
                                    _HCUAccount.SecondaryEmailAddress = _Store.ContactPerson.EmailAddress;
                                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber, _Store.ContactPerson.MobileNumber.Length);
                                }
                                _HCUAccount.Address = _StoreAddress.Address;
                                _HCUAccount.Latitude = _StoreAddress.Latitude;
                                _HCUAccount.Longitude = _StoreAddress.Longitude;
                                _HCUAccount.CountryId = _StoreAddress.CountryId;
                                if (_StoreAddress.StateId != 0)
                                {
                                    _HCUAccount.StateId = _StoreAddress.StateId;
                                }
                                if (_StoreAddress.CityId != 0)
                                {
                                    _HCUAccount.CityId = _StoreAddress.CityId;
                                }
                                if (_StoreAddress.CityAreaId != 0)
                                {
                                    _HCUAccount.CityAreaId = _StoreAddress.CityAreaId;
                                }
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.StatusId = StatusId;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.User = _HCUAccountAuth;
                                #endregion
                                _TUCBranchAccount = new TUCBranchAccount();
                                _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                                _TUCBranchAccount.BranchId = _Request.BranchId;
                                _TUCBranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.RmId).Select(x => x.AccountId).FirstOrDefault();
                                _TUCBranchAccount.Account = _HCUAccount;
                                _TUCBranchAccount.AccountLevelId = 9;
                                _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                                _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                                _HCoreContext.SaveChanges();
                                _Store.ReferenceId = _HCUAccount.Id;
                                _Store.ReferenceKey = _HCUAccount.Guid;
                                if (_Store.Terminals != null)
                                {
                                    foreach (var _Terminal in _Store.Terminals)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Terminal.TerminalId);
                                            if (!TerminalIdCheck)
                                            {
                                                //#endregion
                                                _TUCTerminal = new TUCTerminal();
                                                _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                                                _TUCTerminal.IdentificationNumber = _Terminal.TerminalId;
                                                _TUCTerminal.SerialNumber = _Terminal.SerialNumber;
                                                _TUCTerminal.Account = _HCUAccount;
                                                _TUCTerminal.MerchantId = MerchantId;
                                                _TUCTerminal.StoreId = _Store.ReferenceId;
                                                _TUCTerminal.ProviderId = _Terminal.ProviderId;
                                                _TUCTerminal.AcquirerId = _Request.AccountId;
                                                _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                                                _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                                                _HCoreContext.SaveChanges();
                                                _Terminal.ReferenceKey = _TUCTerminal.Guid;
                                                _Terminal.ReferenceId = _TUCTerminal.Id;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1062, TUCCoreResource.CA1062M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse  SaveStore(OAccounts.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.MerchantId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1053, TUCCoreResource.CA1053M);
                }
                if (string.IsNullOrEmpty(_Request.MerchantKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1054, TUCCoreResource.CA1054M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    string TGuid = HCoreHelper.GenerateGuid();
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = TGuid;
                    _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                    _HCUAccount.OwnerId = _Request.MerchantId;
                    _HCUAccount.BankId = _Request.AccountId;
                    //_HCUAccount.SubOwnerId = _Request.StoreId;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    _HCUAccount.ReferralCode = _Request.ReferralCode;
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    if (_Request.ContactPerson != null)
                    {
                        _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.LastName = _Request.ContactPerson.LastName;
                        _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber,  _Request.ContactPerson.MobileNumber==null?0: _Request.ContactPerson.MobileNumber.Length);
                    }
                    _HCUAccount.Address = _Request.Address;
                
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = StatusId;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_AddressResponse.CityAreaId != 0)
                    {
                        _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    #endregion
                    var _Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1062, TUCCoreResource.CA1062M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveStore", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateStore(OAccounts.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.DisplayName) && AccountDetails.DisplayName != _Request.DisplayName)
                        {
                            AccountDetails.DisplayName = _Request.DisplayName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.Name != _Request.Name)
                        {
                            AccountDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                        {
                            AccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address) && AccountDetails.Address != _Request.Address)
                        {
                            AccountDetails.Address = _Request.Address;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address) && AccountDetails.Address != _Request.Address)
                        {
                            AccountDetails.Address = _Request.Address;
                        }
                        if (_AddressResponse.Latitude != 0 && AccountDetails.Latitude != _AddressResponse.Latitude)
                        {
                            AccountDetails.Latitude = _AddressResponse.Latitude;
                        }
                        if (_AddressResponse.Longitude != 0 && AccountDetails.Longitude != _AddressResponse.Longitude)
                        {
                            AccountDetails.Longitude = _AddressResponse.Longitude;
                        }
                        if (_AddressResponse.CountryId != 0 && AccountDetails.CountryId != _AddressResponse.CountryId)
                        {
                            AccountDetails.CountryId = _AddressResponse.CountryId;
                        }
                        if (_AddressResponse.StateId != 0 && AccountDetails.StateId != _AddressResponse.StateId)
                        {
                            AccountDetails.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CityId != 0 && AccountDetails.CityId != _AddressResponse.CityId)
                        {
                            AccountDetails.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.CityAreaId != 0 && AccountDetails.CityAreaId != _AddressResponse.CityAreaId)
                        {
                            AccountDetails.CityAreaId = _AddressResponse.CityAreaId;
                        }
                        if (_Request.ContactPerson != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName) && AccountDetails.Address != _Request.ContactPerson.FirstName)
                            {
                                AccountDetails.FirstName = _Request.ContactPerson.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName) && AccountDetails.LastName != _Request.ContactPerson.LastName)
                            {
                                AccountDetails.LastName = _Request.ContactPerson.LastName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber) && AccountDetails.MobileNumber != _Request.ContactPerson.MobileNumber)
                            {
                                AccountDetails.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress) && AccountDetails.SecondaryEmailAddress != _Request.ContactPerson.EmailAddress)
                            {
                                AccountDetails.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            }
                        }
                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        AccountDetails.ModifyById = _Request.UserReference.AccountId;
                        AccountDetails.StatusId = StatusId;
                        if (_Request.RmId != 0 && _Request.BranchId != 0)
                        {
                            var RMDetails = _HCoreContext.TUCBranchAccount.Where(x => x.AccountId == AccountDetails.Id && x.Branch.OwnerId == _Request.AccountId).FirstOrDefault();
                            if (RMDetails != null)
                            {
                                if (_Request.BranchId != 0)
                                {
                                    RMDetails.BranchId = _Request.BranchId;
                                }
                                if (_Request.RmId != 0)
                                {
                                    RMDetails.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.RmId).Select(x => x.AccountId).FirstOrDefault();
                                }
                                RMDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                RMDetails.ModifyById = _Request.UserReference.AccountId;
                            }
                            else
                            {
                                _TUCBranchAccount = new TUCBranchAccount();
                                _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                                _TUCBranchAccount.BranchId = _Request.BranchId;
                                _TUCBranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.RmId).Select(x => x.AccountId).FirstOrDefault();
                                _TUCBranchAccount.AccountId = _Request.ReferenceId;
                                _TUCBranchAccount.AccountLevelId = 9;
                                _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                                _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                            }
                        }
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1037, TUCCoreResource.CA1037M);

                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateStore", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveTerminal(OAccounts.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1038, TUCCoreResource.CA1038M);
                }
                if (_Request.MerchantId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1039, TUCCoreResource.CA1039M);
                }
                if (string.IsNullOrEmpty(_Request.MerchantKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1040, TUCCoreResource.CA1040M);
                }
                if (_Request.StoreId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1041, TUCCoreResource.CA1041M);
                }
                if (string.IsNullOrEmpty(_Request.StoreKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1042, TUCCoreResource.CA1042);
                }
                if (_Request.AcquirerId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1043, TUCCoreResource.CA1043M);
                }
                if (string.IsNullOrEmpty(_Request.AcquirerKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1044, TUCCoreResource.CA1044M);
                }
                if (_Request.ProviderId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1045, TUCCoreResource.CA1045M);
                }
                if (string.IsNullOrEmpty(_Request.ProviderKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1046, TUCCoreResource.CA1046M);
                }

                //int? TypeId = 0;
                //if(!string.IsNullOrEmpty(_Request.TypeCode))
                //{
                int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                //    if(TypeId <= 0)
                //    {
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.POS0001, TUCCoreResource.POS0001M);
                //    }
                //}

                using (_HCoreContext = new HCoreContext())
                {
                    bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Request.TerminalId);
                    if (TerminalIdCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1051, TUCCoreResource.CA1051M);
                    }
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.TerminalId;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.TerminalId);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _TUCTerminal = new TUCTerminal();
                    _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                    _TUCTerminal.DisplayName = _Request.TerminalId;
                    _TUCTerminal.SerialNumber = _Request.SerialNumber;
                    _TUCTerminal.IdentificationNumber = _Request.TerminalId;
                    _TUCTerminal.Account = _HCUAccount;
                    if (_Request.MerchantId != 0)
                    {
                        _TUCTerminal.MerchantId = _Request.MerchantId;
                    }
                    if (_Request.StoreId != 0)
                    {
                        _TUCTerminal.StoreId = _Request.StoreId;
                    }
                    if (_Request.CashierId != 0)
                    {
                        _TUCTerminal.CashierId = _Request.CashierId;
                    }
                    if (_Request.ProviderId != 0)
                    {
                        _TUCTerminal.ProviderId = _Request.ProviderId;
                    }
                    if (_Request.AcquirerId != 0)
                    {
                        _TUCTerminal.AcquirerId = _Request.AcquirerId;
                    }

                    if (TypeId != null)
                    {
                        _TUCTerminal.TypeId = (int)TypeId;
                    }
                    else
                    {
                        _TUCTerminal.TypeId = UserAccountType.PosAccount;
                    }

                    _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                    _TUCTerminal.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _TUCTerminal.Id,
                        ReferenceKey = _TUCTerminal.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1052, TUCCoreResource.CA1052M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveTerminal(OAccounts.Terminal.BulkRequest _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Terminals == null || _Request.Terminals.Count == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1107, TUCCoreResource.CA1107M);
                }
                foreach (var Terminal in _Request.Terminals)
                {
                    if (string.IsNullOrEmpty(Terminal.TerminalId))
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1038, TUCCoreResource.CA1038M);
                    }
                    if (Terminal.MerchantId == 0)
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1039, TUCCoreResource.CA1039M);
                    }
                    if (string.IsNullOrEmpty(Terminal.MerchantKey))
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1040, TUCCoreResource.CA1040M);
                    }
                    if (Terminal.StoreId == 0)
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1041, TUCCoreResource.CA1041M);
                    }
                    if (string.IsNullOrEmpty(Terminal.StoreKey))
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1042, TUCCoreResource.CA1042);
                    }
                    if (Terminal.AcquirerId == 0)
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1043, TUCCoreResource.CA1043M);
                    }
                    if (string.IsNullOrEmpty(Terminal.AcquirerKey))
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1044, TUCCoreResource.CA1044M);
                    }
                    if (Terminal.ProviderId == 0)
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1045, TUCCoreResource.CA1045M);
                    }
                    if (string.IsNullOrEmpty(Terminal.ProviderKey))
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1046, TUCCoreResource.CA1046M);
                    }

                    //int? TypeId = 0;
                    //if (!string.IsNullOrEmpty(Terminal.TypeCode))
                    //{
                    int? TypeId = HCoreHelper.GetSystemHelperId(Terminal.TypeCode, _Request.UserReference);
                    //    if (TypeId <= 0)
                    //    {
                    //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.POS0001, TUCCoreResource.POS0001M);
                    //    }
                    //}

                    using (_HCoreContext = new HCoreContext())
                    {
                        bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == Terminal.TerminalId);
                        if (TerminalIdCheck)
                        {
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1051, TUCCoreResource.CA1051M);
                        }
                        //_HCUAccountAuth = new HCUAccountAuth();
                        //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        //_HCUAccountAuth.Username = Terminal.TerminalId;
                        //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(Terminal.TerminalId);
                        //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        //if (_Request.UserReference.AccountId != 0)
                        //{
                        //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        //}
                        //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        //#region Save UserAccount
                        //_HCUAccount = new HCUAccount();
                        //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
                        //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                        //_HCUAccount.OwnerId = Terminal.ProviderId;
                        //_HCUAccount.BankId = Terminal.AcquirerId;
                        //_HCUAccount.SubOwnerId = Terminal.StoreId;
                        //_HCUAccount.DisplayName = Terminal.TerminalId;
                        //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        //_HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                        //if (_Request.UserReference.AppVersionId != 0)
                        //{
                        //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        //}
                        //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        //if (_Request.UserReference.AccountId != 0)
                        //{
                        //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        //}
                        //_HCUAccount.StatusId = HelperStatus.Default.Active;
                        //_HCUAccount.Name = Terminal.TerminalId;
                        //_HCUAccount.CountryId = _Request.UserReference.CountryId;
                        //_HCUAccount.EmailVerificationStatus = 0;
                        //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        //_HCUAccount.NumberVerificationStatus = 0;
                        //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        //_HCUAccount.User = _HCUAccountAuth;
                        //#endregion
                        _TUCTerminal = new TUCTerminal();
                        _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                        _TUCTerminal.DisplayName = Terminal.TerminalId;
                        _TUCTerminal.SerialNumber = Terminal.SerialNumber;
                        _TUCTerminal.IdentificationNumber = HCoreHelper.GenerateRandomNumber(15);
                        _TUCTerminal.Account = _HCUAccount;
                        if (Terminal.MerchantId != 0)
                        {
                            _TUCTerminal.MerchantId = Terminal.MerchantId;
                        }
                        if (Terminal.StoreId != 0)
                        {
                            _TUCTerminal.StoreId = Terminal.StoreId;
                        }
                        if (Terminal.CashierId != 0)
                        {
                            _TUCTerminal.CashierId = Terminal.CashierId;
                        }
                        if (Terminal.ProviderId != 0)
                        {
                            _TUCTerminal.ProviderId = Terminal.ProviderId;
                        }
                        if (Terminal.AcquirerId != 0)
                        {
                            _TUCTerminal.AcquirerId = Terminal.AcquirerId;
                        }

                        if (TypeId != null)
                        {
                            _TUCTerminal.TypeId = (int)TypeId;
                        }
                        else
                        {
                            _TUCTerminal.TypeId = UserAccountType.PosAccount;
                        }

                        _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                        _TUCTerminal.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCTerminal.Id,
                            ReferenceKey = _TUCTerminal.Guid,
                        };
                    }
                }
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1108, TUCCoreResource.CA1108M);
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminal-BULK", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the terminal status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateTerminalStatus(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {

                    var AccountDetails = _HCoreContext.TUCTerminal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey ).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        AccountDetails.ModifyById = _Request.UserReference.AccountId;
                        AccountDetails.StatusId = StatusId;
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1037, TUCCoreResource.CA1037M);

                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateTerminalStatus", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
