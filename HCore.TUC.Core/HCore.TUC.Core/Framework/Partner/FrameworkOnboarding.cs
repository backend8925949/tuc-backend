//==================================================================================
// FileName: FrameworkOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to onboarding
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Resource;
using System.Collections.Generic;
using HCore.Data.Operations;
using Akka.Actor;
using HCore.TUC.Core.Framework.Partner.Actor;

namespace HCore.TUC.Core.Framework.Partner
{
    internal class FrameworkOnboarding
    {
        HCoreContextOperations _HCoreContextOperations;
        //HCOMerchantUpload _HCOMerchantUpload;
        /// <summary>
        /// Description: Method to onboard merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchants(OOnboarding.Merchant.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId  < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1070, TUCCoreResource.CA1070M);
                }
                if (_Request.Data == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1068, TUCCoreResource.CA1068M);
                }
                if (_Request.Data.Count() < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1069, TUCCoreResource.CA1069M);
                }
                var system = ActorSystem.Create("ActorPartnerOnboardMerchant");
                var greeter = system.ActorOf<ActorPartnerOnboardMerchant>("ActorPartnerOnboardMerchant");
                greeter.Tell(_Request);
                //using (_HCoreContextOperations = new HCoreContextOperations())
                //{
                //    foreach (var DataItem in _Request.Data)
                //    {
                //        _HCOMerchantUpload = new HCOMerchantUpload();
                //        _HCOMerchantUpload.Guid = HCoreHelper.GenerateGuid();
                //        _HCOMerchantUpload.OwnerId = _Request.AccountId;
                //        _HCOMerchantUpload.DisplayName = DataItem.DisplayName;
                //        _HCOMerchantUpload.Name = DataItem.Name;
                //        _HCOMerchantUpload.ContactNumber = DataItem.ContactNumber;
                //        _HCOMerchantUpload.EmailAddress = DataItem.EmailAddress;
                //        _HCOMerchantUpload.Address = DataItem.Address;
                //        _HCOMerchantUpload.CityAreaName = DataItem.CityAreaName;
                //        _HCOMerchantUpload.CityName = DataItem.CityName;
                //        _HCOMerchantUpload.StateName = DataItem.StateName;
                //        _HCOMerchantUpload.CountryName = DataItem.CountryName;
                //        _HCOMerchantUpload.Latitude = DataItem.Latitude;
                //        _HCOMerchantUpload.Longitude = DataItem.Longitude;
                //        _HCOMerchantUpload.AccountId = DataItem.AccountId;
                //        _HCOMerchantUpload.CategoryName = DataItem.CategoryName;
                //        _HCOMerchantUpload.ContactPersonName = DataItem.ContactPersonName;
                //        _HCOMerchantUpload.ContactPersonMobileNumber = DataItem.ContactPersonMobileNumber;
                //        _HCOMerchantUpload.ContactPersonEmailAddress = DataItem.ContactPersonEmailAddress;
                //        _HCOMerchantUpload.ReferenceNumber = DataItem.ReferenceNumber;
                //        _HCOMerchantUpload.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCOMerchantUpload.CreatedById = _Request.UserReference.AccountId;
                //        _HCOMerchantUpload.StatusId = 2;
                //        _HCoreContextOperations.HCOMerchantUpload.Add(_HCOMerchantUpload);
                //    }
                //    _HCoreContextOperations.SaveChanges();
                //}


                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1071, TUCCoreResource.CA1071M);
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchantInformation", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the onboarded merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOnboardedMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContextOperations.HCOMerchantUpload
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName  = x.DisplayName,
                                                    Name  = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    CityAreaName  = x.CityAreaName,
                                                    CityName = x.CityName,
                                                    StateName = x.StateName,
                                                    CountryName = x.CountryName,
                                                    AccountId = x.AccountId,
                                                    RewardPercentage = x.RewardPercentage,
                                                    CategoryName = x.CategoryName,
                                                    ContactPersonName = x.ContactPersonName,
                                                    ContactPersonMobileNumber = x.ContactPersonMobileNumber,
                                                    ContactPersonEmailAddress = x.ContactPersonEmailAddress,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate  = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                    StatusMessage  = x.StatusMessage,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OOnboarding.Merchant.Details> Data = _HCoreContextOperations.HCOMerchantUpload
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    CityAreaName = x.CityAreaName,
                                                    CityName = x.CityName,
                                                    StateName = x.StateName,
                                                    CountryName = x.CountryName,
                                                    AccountId = x.AccountId,
                                                    RewardPercentage = x.RewardPercentage,
                                                    CategoryName = x.CategoryName,
                                                    ContactPersonName = x.ContactPersonName,
                                                    ContactPersonMobileNumber = x.ContactPersonMobileNumber,
                                                    ContactPersonEmailAddress = x.ContactPersonEmailAddress,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                    StatusMessage = x.StatusMessage,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in Data)
                    {

                    }
                    _HCoreContextOperations.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetOnboardedMerchants", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
