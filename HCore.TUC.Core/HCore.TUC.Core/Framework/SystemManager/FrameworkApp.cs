//==================================================================================
// FileName: FrameworkApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for system administration functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.SystemManager
{
    internal class FrameworkApp
    {
        HCoreContext _HCoreContext;
        HCCoreApp _HCCoreApp;
        HCCoreAppVersion _HCCoreAppVersion;
        /// <summary>
        /// Description: Saves the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveApp(OSystemAdministration.App.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1198, TUCCoreResource.CA1198M);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1201, TUCCoreResource.CA1201M);
                }
                else
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1149, TUCCoreResource.CA1149M);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        bool _DetailCheck = _HCoreContext.HCCoreApp.Any(x => x.Name == _Request.Name && x.AccountId == _Request.AccountId);
                        if (_DetailCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1247, TUCCoreResource.CA1247M);
                        }
                        string AppKey = HCoreHelper.GenerateGuid();
                        string AppVersionKey = HCoreHelper.GenerateGuid();
                        _HCCoreApp = new HCCoreApp();
                        _HCCoreApp.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreApp.AccountId = _Request.AccountId;
                        _HCCoreApp.Name = _Request.Name;
                        _HCCoreApp.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        _HCCoreApp.Description = _Request.Description;
                        _HCCoreApp.AppKey = AppKey;
                        _HCCoreApp.IpAddress = _Request.IpAddress;
                        _HCCoreApp.AllowLogging = _Request.LogRequest;
                        _HCCoreApp.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreApp.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreApp.StatusId = StatusId;
                        _HCCoreApp.HCCoreAppVersion.Add(new HCCoreAppVersion
                        {
                            Guid = HCoreHelper.GenerateGuid(),
                            VersionId = "v1",
                            ApiKey = AppVersionKey,
                            PublicKey = AppVersionKey,
                            PrivateKey = AppVersionKey,
                            SystemPublicKey = AppVersionKey,
                            SystemPrivateKey = AppVersionKey,
                            RequestCount = 0,
                            CreateDate = HCoreHelper.GetGMTDateTime(),
                            CreatedById = _Request.UserReference.AccountId,
                            StatusId = HelperStatus.Default.Active
                        });
                        _HCoreContext.HCCoreApp.Add(_HCCoreApp);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _HCCoreApp.Id,
                            ReferenceKey = _HCCoreApp.Guid,
                            Name = _Request.Name,
                            AppKey = AppKey,
                            AppVersionName = "v1",
                            AppVersionKey = AppVersionKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1248, TUCCoreResource.CA1248M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveApp", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Gets the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetApp(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId < 1 && _Request.UserReference.AccountTypeId == UserAccountType.Admin)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCCoreApp
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OSystemAdministration.App.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        IconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                        ActiveVersion = "",
                                                        TotalRequest = 0,
                                                        LastRequestDateTime = x.ModifyDate,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OSystemAdministration.App.List> _List = _HCoreContext.HCCoreApp
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OSystemAdministration.App.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        IconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                        ActiveVersion = "",
                                                        TotalRequest = 0,
                                                        LastRequestDateTime = x.ModifyDate,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCCoreApp
                                                    .Where(x =>
                                                    x.AccountId == _Request.ReferenceId &&
                                                    x.Account.Guid == _Request.ReferenceKey
                                                    && x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OSystemAdministration.App.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,

                                                        ActiveVersion = "",
                                                        TotalRequest = 0,
                                                        LastRequestDateTime = x.ModifyDate,

                                                        CreateDate = x.CreateDate,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OSystemAdministration.App.List> _List = _HCoreContext.HCCoreApp
                                                    .Where(x =>
                                                    x.AccountId == _Request.ReferenceId &&
                                                    x.Account.Guid == _Request.ReferenceKey
                                                    && x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OSystemAdministration.App.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,

                                                        ActiveVersion = "",
                                                        TotalRequest = 0,
                                                        LastRequestDateTime = x.ModifyDate,

                                                        CreateDate = x.CreateDate,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetApp", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetApp(OSystemAdministration.App.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OSystemAdministration.App.Details? Data = _HCoreContext.HCCoreApp
                                                .Where(x => x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.Guid == _Request.ReferenceKey
                                                && x.Id == _Request.ReferenceId)
                                                .Select(x => new OSystemAdministration.App.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LogRequest = x.AllowLogging,
                                                    Name = x.Name,
                                                    //ActiveAppKey = x.Value,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    IconUrl = x.Account.IconStorage.Path,
                                                    ActiveVersion = "",
                                                    TotalRequest = 0,
                                                    LastRequestDateTime = x.ModifyDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.ActiveAppVersionKey = _HCoreContext.HCCoreAppVersion.Where(x => x.AppId == Data.ReferenceId && x.StatusId == HelperStatus.Default.Active).Select(x => x.ApiKey).FirstOrDefault();
                        if (!string.IsNullOrEmpty(Data.IconUrl))
                        {
                            Data.IconUrl = _AppConfig.StorageUrl + Data.IconUrl;
                        }
                        else
                        {
                            Data.IconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetApp", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteApp(OSystemAdministration.App.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    HCCoreApp? Data = _HCoreContext.HCCoreApp
                                                .Where(x => x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.Guid == _Request.ReferenceKey
                                                && x.Id == _Request.ReferenceId)
                                               .FirstOrDefault();
                    if (Data != null)
                    {
                        var AppVersions = _HCoreContext.HCCoreAppVersion.Where(x => x.AppId == Data.Id).ToList();
                        if (AppVersions.Count > 0)
                        {
                            _HCoreContext.HCCoreAppVersion.RemoveRange(AppVersions);
                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                HCCoreApp? DeleteItem = _HCoreContext.HCCoreApp
                                              .Where(x => x.AccountId == _Request.AccountId
                                              && x.Account.Guid == _Request.AccountKey
                                              && x.Guid == _Request.ReferenceKey
                                              && x.Id == _Request.ReferenceId)
                                             .FirstOrDefault();
                                _HCoreContext.HCCoreApp.Remove(DeleteItem);
                                _HCoreContext.SaveChanges();
                            }
                        }
                        else
                        {
                            _HCoreContext.HCCoreApp.Remove(Data);
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1250, TUCCoreResource.CA1250M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteApp", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateApp(OSystemAdministration.App.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1149, TUCCoreResource.CA1149M);
                    }
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    HCCoreApp? Data = _HCoreContext.HCCoreApp
                                                .Where(x => x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.Guid == _Request.ReferenceKey
                                                && x.Id == _Request.ReferenceId)
                                               .FirstOrDefault();
                    if (Data != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Request.Name != Data.Name)
                        {
                            Data.Name = _Request.Name;
                        }
                        if (StatusId != 0)
                        {
                            Data.StatusId = StatusId;
                        }
                        if (!string.IsNullOrEmpty(_Request.IpAddress) && _Request.IpAddress != Data.IpAddress)
                        {
                            Data.IpAddress = _Request.IpAddress;
                        }
                        Data.AllowLogging = _Request.LogRequest;
                        Data.ModifyById = _Request.UserReference.AccountId;
                        Data.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1246, TUCCoreResource.CA1246M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResetAppKey", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the application key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetAppKey(OSystemAdministration.App.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    HCCoreApp? Data = _HCoreContext.HCCoreApp
                                                .Where(x => x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.Guid == _Request.ReferenceKey
                                                && x.Id == _Request.ReferenceId)
                                               .FirstOrDefault();
                    if (Data != null)
                    {
                        var AppVersions = _HCoreContext.HCCoreAppVersion.Where(x => x.AppId == Data.Id).ToList();
                        foreach (var AppVersion in AppVersions)
                        {
                            AppVersion.ModifyById = _Request.UserReference.AccountId;
                            AppVersion.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AppVersion.StatusId = HelperStatus.Default.Blocked;
                        }
                        Data.AppKey = HCoreHelper.GenerateGuid();
                        string NewVersionId = "v" + AppVersions.Count;
                        Data.ModifyById = _Request.UserReference.AccountId;
                        Data.ModifyDate = HCoreHelper.GetGMTDateTime();
                        string AppVersionKey = HCoreHelper.GenerateGuid();
                        _HCCoreAppVersion = new HCCoreAppVersion
                        {
                            Guid = HCoreHelper.GenerateGuid(),
                            AppId = Data.Id,
                            VersionId = NewVersionId,
                            ApiKey = AppVersionKey,
                            PublicKey = AppVersionKey,
                            PrivateKey = AppVersionKey,
                            SystemPublicKey = AppVersionKey,
                            SystemPrivateKey = AppVersionKey,
                            RequestCount = 0,
                            CreateDate = HCoreHelper.GetGMTDateTime(),
                            CreatedById = _Request.UserReference.AccountId,
                            StatusId = HelperStatus.Default.Active
                        };
                        _HCoreContext.HCCoreAppVersion.Add(_HCCoreAppVersion);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            AppKey = Data.AppKey,
                            AppVersionName = NewVersionId,
                            AppVersionKey = AppVersionKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1249, TUCCoreResource.CA1249M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResetAppKey", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        HCoreContextLogging _HCoreContextLogging;
        /// <summary>
        /// Description: Gets the application usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUsage(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "RequestTime", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.ReferenceId < 1 && _Request.UserReference.AccountTypeId == UserAccountType.Admin)
                    {
                        return null;
                        //if (_Request.RefreshCount)
                        //{
                        //    #region Total Records
                        //    _Request.TotalRecords = _HCoreContextLogging.HCLVApiUsage
                        //                            .Where(x => x.AppId == _Request.ReferenceId
                        //                            && x.AppKey == _Request.ReferenceKey)
                        //                           .Where(_Request.SearchCondition)
                        //                   .Count();
                        //    #endregion
                        //}
                        //#region Get Data
                        //List<HCore.Data.Logging.HCLVApiUsage> _List = _HCoreContextLogging.HCLVApiUsage
                        //                            .Where(x => x.AppId == _Request.ReferenceId
                        //                            && x.AppKey == _Request.ReferenceKey)
                        //                           .Where(_Request.SearchCondition)
                        //                         .OrderBy(_Request.SortExpression)
                        //                         .Skip(_Request.Offset)
                        //                         .Take(_Request.Limit)
                        //                         .ToList();
                        //#endregion
                        //#region Create  Response Object
                        //_HCoreContext.Dispose();
                        //OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        //#endregion
                        //#region Send Response
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        //#endregion
                    }
                    else
                    {
                        return null;
                        //if (_Request.RefreshCount)
                        //{
                        //    _Request.TotalRecords = _HCoreContextLogging.HCLVApiUsage
                        //                           .Where(_Request.SearchCondition)
                        //                   .Count();
                        //}
                        //List<HCore.Data.Logging.HCLVApiUsage> _List = _HCoreContextLogging.HCLVApiUsage
                        //                           .Where(_Request.SearchCondition)
                        //                             .OrderBy(_Request.SortExpression)
                        //                             .Skip(_Request.Offset)
                        //                             .Take(_Request.Limit)
                        //                             .ToList();
                        //_HCoreContext.Dispose();
                        //OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAppUsage", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppUsage(OSystemAdministration.App.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    #region Get Data
                    var Data = _HCoreContextLogging.HCLCoreUsage
                                                 .Where(x => x.Id == _Request.ReferenceId
                                                 && x.Guid == _Request.ReferenceKey
                                               )
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Request = x.Request,
                                                     Response = x.Response,
                                                 }).FirstOrDefault();
                    if (Data != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetApp", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


    }
}
