﻿//using System;
//using System.Linq.Dynamic.Core;
//using Akka.Actor;
//using HCore.Data;
//using HCore.Data.Models;
//using HCore.Data.Operations;
//using HCore.Helper;
//using HCore.Operations;
//using HCore.Operations.Object;
//using HCore.TUC.Core.Framework.Merchant.App;
//using HCore.TUC.Core.Object.Merchant;
//using HCore.TUC.Core.Resource;
//using SendGrid;
//using SendGrid.Helpers.Mail;
//using static HCore.Helper.HCoreConstant;
//using static HCore.Helper.HCoreConstant.Helpers;

//namespace HCore.TUC.Core.Framework.Merchant.Upload
//{

//    internal class FrameworkUploadCustomerRewardV2
//    {
//        HCoreContext _HCoreContext;
//        HCoreContextOperations _HCoreContextOperations;
//        OAppProfile.Request _AppProfileRequest;
//        ManageCoreUserAccess _ManageCoreUserAccess;
//        ManageCoreTransaction _ManageCoreTransaction;
//        OCoreTransaction.Request _CoreTransactionRequest;
//        List<OCoreTransaction.TransactionItem> _TransactionItems;
//        internal void ActorUploadCustomerRewardProcess()
//        {
//            #region Manage Exception
//            try
//            {
//                using (_HCoreContextOperations = new HCoreContextOperations())
//                {
//                    var PendingItems = _HCoreContextOperations.HCOUploadCustomerReward.Where(x => x.StatusId == 1).Skip(0).Take(10).ToList();
//                    if (PendingItems.Count > 0)
//                    {
//                        foreach (var PendingItem in PendingItems)
//                        {
//                            PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
//                            PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
//                            PendingItem.StatusId = 2;
//                            PendingItem.StatusMessage = "Processing";
//                        }
//                        _HCoreContextOperations.SaveChanges();
//                        foreach (var Item in PendingItems)
//                        {
//                            bool IsTransactionDone = false;
//                            if (!string.IsNullOrEmpty(Item.TransactionReference))
//                            {
//                                using (_HCoreContext = new HCoreContext())
//                                {
//                                    bool TransactionCheck = _HCoreContext.HCUAccountTransaction.Any(x => x.ParentId == Item.MerchantId && x.GroupKey == Item.TransactionReference);
//                                    if (TransactionCheck)
//                                    {
//                                        IsTransactionDone = true;
//                                    }
//                                }
//                            }
//                            if (IsTransactionDone == false)
//                            {

//                                bool AllowProcess = true;
//                                long ProcessStatusId = 1;
//                                string ProcessMessage = "";
//                                string GroupKey = Item.TransactionReference;
//                                if (string.IsNullOrEmpty(Item.MobileNumber))
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Mobile number missing";
//                                    AllowProcess = false;
//                                }
//                                //if (string.IsNullOrEmpty(_Request.Name))
//                                //{
//                                //    ProcessStatusId = 4;
//                                //    ProcessMessage = "Name missing";
//                                //    AllowProcess = false;
//                                //}
//                                else if (Item.MobileNumber.Length < 10)
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                else if (Item.MobileNumber.Length > 13)
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                else if (!System.Text.RegularExpressions.Regex.IsMatch(Item.MobileNumber, "^[0-9]*$"))
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                long AccountId = 0;
//                                if (AllowProcess)
//                                {
//                                    #region Get Customer 
//                                    using (_HCoreContext = new HCoreContext())
//                                    {
//                                        var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == Item.MerchantId)
//                                            .Select(x => new
//                                            {
//                                                CategoryId = x.PrimaryCategoryId,
//                                                DisplayName = x.DisplayName,
//                                                CountryId = x.CountryId,
//                                                CountryIsd = x.Country.Isd,
//                                                CountryMobileNumberLength = x.Country.MobileNumberLength,
//                                            }).FirstOrDefault();
//                                        OUserReference _OUserReference = new OUserReference();
//                                        _OUserReference.CountryId = MerchantDetails.CountryId;
//                                        if (Item.CreatedById > 0)
//                                        {
//                                            _OUserReference.AccountId = (long)Item.CreatedById;
//                                        }
//                                        string CustomerDisplayName = "";
//                                        string CustomerEmailAddress = "";
//                                        string CustomerAccountNumber = "";
//                                        int? CustomerCountryId = null;
//                                        var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(Item.MobileNumber);
//                                        if (ValidateMobileNumber.IsNumberValid == true)
//                                        {
//                                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == ValidateMobileNumber.Isd)
//                                                .Select(x => new
//                                                {
//                                                    x.Id,
//                                                    x.MobileNumberLength,
//                                                })
//                                                .FirstOrDefault();
//                                            string CustomerMobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, CountryDetails.MobileNumberLength);
//                                            string TUserName = _AppConfig.AppUserPrefix + CustomerMobileNumber;
//                                            string TAccountNumber = Item.MobileNumber;

//                                            var AccountDetails = _HCoreContext.HCUAccount
//                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
//                                                && (x.AccountCode == TAccountNumber || x.User.Username == TUserName))
//                                                .Select(x => new
//                                                {
//                                                    x.Id,
//                                                    x.StatusId,
//                                                    x.DisplayName,
//                                                    x.EmailAddress,
//                                                    x.AccountCode,
//                                                    x.CountryId,
//                                                }).FirstOrDefault();
//                                            if (AccountDetails != null)
//                                            {

//                                                AccountId = AccountDetails.Id;
//                                                _HCoreContext.Dispose();
//                                                CustomerCountryId = AccountDetails.CountryId;
//                                                CustomerEmailAddress = AccountDetails.EmailAddress;
//                                                CustomerDisplayName = AccountDetails.DisplayName;
//                                                CustomerAccountNumber = AccountDetails.AccountCode;
//                                            }
//                                            else
//                                            {
//                                                _AppProfileRequest = new OAppProfile.Request();
//                                                _AppProfileRequest.OwnerId = Item.MerchantId;
//                                                if (Item.CreatedById > 0)
//                                                {
//                                                    _AppProfileRequest.CreatedById = (long)Item.CreatedById;
//                                                }
//                                                _AppProfileRequest.CountryId = CountryDetails.Id;
//                                                _AppProfileRequest.MobileNumber = CustomerMobileNumber;
//                                                _AppProfileRequest.EmailAddress = Item.EmailAddress;
//                                                _AppProfileRequest.DateOfBirth = Item.DateOfBirth;
//                                                _AppProfileRequest.Name = Item.Name;
//                                                _AppProfileRequest.FirstName = Item.FirstName;
//                                                _AppProfileRequest.MiddleName = Item.MiddleName;
//                                                _AppProfileRequest.LastName = Item.LastName;
//                                                if (!string.IsNullOrEmpty(Item.FirstName))
//                                                {
//                                                    _AppProfileRequest.DisplayName = Item.FirstName;
//                                                }
//                                                else
//                                                {
//                                                    _AppProfileRequest.DisplayName = Item.MobileNumber;
//                                                }

//                                                if (!string.IsNullOrEmpty(Item.Gender))
//                                                {
//                                                    _AppProfileRequest.GenderCode = Item.Gender.ToLower();
//                                                }

//                                                _AppProfileRequest.UserReference = _OUserReference;
//                                                _ManageCoreUserAccess = new ManageCoreUserAccess();
//                                                OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
//                                                if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
//                                                {
//                                                    CustomerCountryId = CountryDetails.Id;
//                                                    AccountId = _AppUserCreateResponse.AccountId;
//                                                    CustomerEmailAddress = _AppUserCreateResponse.EmailAddress;
//                                                    CustomerDisplayName = _AppUserCreateResponse.DisplayName;
//                                                    CustomerAccountNumber = _AppUserCreateResponse.AccountCode;
//                                                }
//                                                else
//                                                {
//                                                    ProcessStatusId = 4;
//                                                    ProcessMessage = "Account creation failed";
//                                                    AllowProcess = false;
//                                                }
//                                            }
//                                        }
//                                        else
//                                        {
//                                            ProcessStatusId = 4;
//                                            ProcessMessage = "Invalid mobile number";
//                                            AllowProcess = false;
//                                        }

//                                        if (AllowProcess == true)
//                                        {
//                                            if (!string.IsNullOrEmpty(GroupKey))
//                                            {
//                                                GroupKey = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(Item.MerchantId) + HCoreHelper.GetAccountIdString(AccountId);
//                                            }

//                                            var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", Item.MerchantId);
//                                            double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                            if (TUCRewardPercentage > 0)
//                                            {
//                                                double TUCRewardAmount = HCoreHelper.GetPercentage((double)Item.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
//                                                if (TUCRewardAmount <= 0)
//                                                {
//                                                    ProcessStatusId = 4;
//                                                    ProcessMessage = "Invalid reward amount";
//                                                    AllowProcess = false;
//                                                }
//                                                if (TUCRewardAmount > Item.InvoiceAmount)
//                                                {
//                                                    ProcessStatusId = 4;
//                                                    ProcessMessage = "Invalid invoice amount";
//                                                    AllowProcess = false;
//                                                }
//                                                else
//                                                {
//                                                    if (AllowProcess == true)
//                                                    {
//                                                        int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", Item.MerchantId));
//                                                        int TransactionStatusId = HelperStatus.Transaction.Success;
//                                                        double TUCRewardUserAmount = 0;
//                                                        double TUCRewardUserAmountPercentage = 0;
//                                                        double TUCRewardCommissionAmount = 0;
//                                                        double TUCRewardCommissionAmountPercentage = 0;
//                                                        if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
//                                                        {
//                                                            var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", Item.MerchantId);
//                                                            TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                                            TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
//                                                            TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
//                                                            TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
//                                                            if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
//                                                            {
//                                                                TransactionStatusId = HelperStatus.Transaction.Success;
//                                                            }
//                                                            else
//                                                            {
//                                                                _ManageCoreTransaction = new ManageCoreTransaction();
//                                                                double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
//                                                                if (MerchantBalance >= TUCRewardAmount)
//                                                                {
//                                                                    TransactionStatusId = HelperStatus.Transaction.Success;
//                                                                }
//                                                                else
//                                                                {
//                                                                    TransactionStatusId = HelperStatus.Transaction.Pending;
//                                                                }
//                                                            }
//                                                        }
//                                                        else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
//                                                        {
//                                                            int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", Item.MerchantId));
//                                                            if (TAllowCustomReward == 1)
//                                                            {
//                                                                if (Item.RewardAmount > 0)
//                                                                {
//                                                                    TUCRewardAmount = (double)Item.RewardAmount;
//                                                                }
//                                                            }
//                                                            double MaximumCommissionAmount = 2500;
//                                                            double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
//                                                            double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                                            double SystemCategoryCommissionPercentage = 0;
//                                                            if (MerchantDetails.CategoryId > 0)
//                                                            {
//                                                                using (_HCoreContext = new HCoreContext())
//                                                                {
//                                                                    var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == MerchantDetails.CategoryId)
//                                                                        .Select(x => new
//                                                                        {
//                                                                            Commission = x.Commission,
//                                                                        }).FirstOrDefault();
//                                                                    _HCoreContext.Dispose();
//                                                                    if (CategoryCommission != null && CategoryCommission.Commission > 0)
//                                                                    {
//                                                                        SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
//                                                                    }
//                                                                }
//                                                            }
//                                                            if (SystemMerchantCommissionPercentage > 0)
//                                                            {
//                                                                TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
//                                                            }
//                                                            else if (SystemCategoryCommissionPercentage > 0)
//                                                            {
//                                                                TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
//                                                            }
//                                                            else
//                                                            {
//                                                                TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
//                                                            }

//                                                            TUCRewardCommissionAmount = HCoreHelper.GetPercentage((double)Item.InvoiceAmount, TUCRewardCommissionAmountPercentage);
//                                                            if (TUCGold != null && TUCGold.Value == "1")
//                                                            {
//                                                                TUCRewardCommissionAmount = 0;
//                                                            }
//                                                            if (TUCRewardCommissionAmount > MaximumCommissionAmount)
//                                                            {
//                                                                TUCRewardCommissionAmount = MaximumCommissionAmount;
//                                                            }
//                                                            TUCRewardUserAmount = TUCRewardAmount;
//                                                            TUCRewardUserAmountPercentage = TUCRewardPercentage;
//                                                            TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
//                                                            if (TUCRewardAmount > Item.InvoiceAmount)
//                                                            {
//                                                                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
//                                                            }
//                                                            if (TUCGold != null && TUCGold.Value == "1")
//                                                            {
//                                                                TransactionStatusId = HelperStatus.Transaction.Success;
//                                                            }
//                                                            else
//                                                            {
//                                                                _ManageCoreTransaction = new ManageCoreTransaction();
//                                                                double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
//                                                                if (MerchantBalance >= TUCRewardAmount)
//                                                                {
//                                                                    TransactionStatusId = HelperStatus.Transaction.Success;
//                                                                }
//                                                                else
//                                                                {
//                                                                    if (MerchantBalance > -5000)
//                                                                    {
//                                                                        if ((MerchantBalance - TUCRewardAmount) > -5000)
//                                                                        {
//                                                                            TransactionStatusId = HelperStatus.Transaction.Pending;
//                                                                        }
//                                                                        else
//                                                                        {
//                                                                            TransactionStatusId = HelperStatus.Transaction.Success;
//                                                                        }
//                                                                    }
//                                                                }
//                                                            }
//                                                        }
//                                                        if (TUCRewardAmount > Item.InvoiceAmount)
//                                                        {
//                                                            #region Online Account
//                                                            ProcessStatusId = 4;
//                                                            ProcessMessage = "Invalid reward amount";
//                                                            #endregion
//                                                        }
//                                                        else
//                                                        {
//                                                            _CoreTransactionRequest = new OCoreTransaction.Request();
//                                                            _CoreTransactionRequest.GroupKey = GroupKey;
//                                                            _CoreTransactionRequest.CustomerId = AccountId;
//                                                            _CoreTransactionRequest.UserReference = _OUserReference;
//                                                            _CoreTransactionRequest.StatusId = TransactionStatusId;
//                                                            _CoreTransactionRequest.ParentId = Item.MerchantId;
//                                                            if (Item.StoreId > 0)
//                                                            {
//                                                                _CoreTransactionRequest.SubParentId = (long)Item.StoreId;
//                                                            }
//                                                            _CoreTransactionRequest.InvoiceAmount = (double)Item.InvoiceAmount;
//                                                            _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                            _CoreTransactionRequest.ReferenceAmount = TUCRewardAmount;
//                                                            _CoreTransactionRequest.ReferenceNumber = Item.ReferenceNumber;
//                                                            if (Item.CashierId > 0)
//                                                            {
//                                                                _CoreTransactionRequest.CashierId = (long)Item.CashierId;
//                                                            }
//                                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
//                                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                            {
//                                                                UserAccountId = Item.MerchantId,
//                                                                ModeId = TransactionMode.Debit,
//                                                                TypeId = TransactionType.CashReward,
//                                                                SourceId = TransactionSource.Merchant,
//                                                                Amount = TUCRewardAmount,
//                                                                Charge = 0,
//                                                                Comission = TUCRewardCommissionAmount,
//                                                                TotalAmount = TUCRewardAmount,
//                                                            });
//                                                            if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
//                                                            {
//                                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                                {
//                                                                    UserAccountId = AccountId,
//                                                                    ModeId = TransactionMode.Credit,
//                                                                    TypeId = TransactionType.CashReward,
//                                                                    SourceId = TransactionSource.TUCBlack,
//                                                                    Amount = TUCRewardUserAmount,
//                                                                    TotalAmount = TUCRewardUserAmount,
//                                                                });
//                                                            }
//                                                            else
//                                                            {
//                                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                                {
//                                                                    UserAccountId = AccountId,
//                                                                    ModeId = TransactionMode.Credit,
//                                                                    TypeId = TransactionType.CashReward,
//                                                                    SourceId = TransactionSource.TUC,
//                                                                    Amount = TUCRewardUserAmount,
//                                                                    TotalAmount = TUCRewardUserAmount,
//                                                                });
//                                                            }
//                                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                            {
//                                                                UserAccountId = SystemAccounts.ThankUCashMerchant,
//                                                                ModeId = TransactionMode.Credit,
//                                                                TypeId = TransactionType.CashReward,
//                                                                SourceId = TransactionSource.Settlement,
//                                                                Amount = TUCRewardCommissionAmount,
//                                                                TotalAmount = TUCRewardCommissionAmount,
//                                                            });
//                                                            _CoreTransactionRequest.Transactions = _TransactionItems;
//                                                            _ManageCoreTransaction = new ManageCoreTransaction();
//                                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
//                                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
//                                                            {

//                                                                if (TransactionStatusId == HelperStatus.Transaction.Success)
//                                                                {
//                                                                    if (Item.MerchantId == 583505 || Item.MerchantId == 13)
//                                                                    {
//                                                                        if (HostEnvironment == HostEnvironmentType.Live)
//                                                                        {
//                                                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(AccountId, Item.MerchantId, TransactionSource.TUCBlack);
//                                                                            if (!string.IsNullOrEmpty(ValidateMobileNumber.MobileNumber))
//                                                                            {
//                                                                                string Message = "Credit Alert: You have earned " + TUCRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
//                                                                                HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, Message, AccountId, null);
//                                                                            }
//                                                                            if (!string.IsNullOrEmpty(CustomerEmailAddress)) // Wakanow email
//                                                                            {
//                                                                                string Template = "";
//                                                                                string currentDirectory = Directory.GetCurrentDirectory();
//                                                                                string path = "templates";
//                                                                                string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
//                                                                                using (StreamReader reader = File.OpenText(fullPath))
//                                                                                {
//                                                                                    Template = reader.ReadToEnd();
//                                                                                }
//                                                                                var _EmailParameters = new
//                                                                                {
//                                                                                    MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
//                                                                                    MerchantDisplayName = "WakaPoints",
//                                                                                    UserDisplayName = CustomerDisplayName,
//                                                                                    AccountNumber = CustomerAccountNumber,
//                                                                                    Amount = TUCRewardAmount.ToString(),
//                                                                                    Balance = CustomerBalance.ToString(),
//                                                                                };
//                                                                                Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
//                                                                                Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
//                                                                                Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
//                                                                                Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
//                                                                                Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
//                                                                                Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
//                                                                                var client = new SendGridClient("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");
//                                                                                var msg = new SendGridMessage()
//                                                                                {
//                                                                                    From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
//                                                                                    Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
//                                                                                    HtmlContent = Template
//                                                                                };
//                                                                                msg.AddTo(new EmailAddress(CustomerEmailAddress, CustomerDisplayName));
//                                                                                var response = client.SendEmailAsync(msg);
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                    else
//                                                                    {
//                                                                        if (HostEnvironment == HostEnvironmentType.Live)
//                                                                        {
//                                                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(AccountId, true);
//                                                                            using (_HCoreContext = new HCoreContext())
//                                                                            {
//                                                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
//                                                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
//                                                                                {
//                                                                                    if (HostEnvironment == HostEnvironmentType.Live)
//                                                                                    {
//                                                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDetails.DisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
//                                                                                    }
//                                                                                    else
//                                                                                    {
//                                                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDetails.DisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
//                                                                                    }
//                                                                                }
//                                                                                else
//                                                                                {
//                                                                                    #region Send SMS
//                                                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + MerchantDetails.DisplayName + "  Bal: N" + HCoreHelper.RoundNumber((CustomerBalance + TUCRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
//                                                                                    HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, Message, AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
//                                                                                    #endregion
//                                                                                }
//                                                                            }
//                                                                            #region Send Email 
//                                                                            if (!string.IsNullOrEmpty(CustomerEmailAddress))
//                                                                            {
//                                                                                var _EmailParameters = new
//                                                                                {
//                                                                                    UserDisplayName = CustomerDisplayName,
//                                                                                    MerchantName = MerchantDetails.DisplayName,
//                                                                                    InvoiceAmount = Item.InvoiceAmount.ToString(),
//                                                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
//                                                                                    Balance = CustomerBalance.ToString(),
//                                                                                };
//                                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, CustomerDisplayName, CustomerEmailAddress, _EmailParameters, _OUserReference);
//                                                                            }
//                                                                            #endregion
//                                                                        }
//                                                                    }
//                                                                }
//                                                                else
//                                                                {
//                                                                    if (HostEnvironment == HostEnvironmentType.Live)
//                                                                    {
//                                                                        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(AccountId, true);
//                                                                        using (_HCoreContext = new HCoreContext())
//                                                                        {
//                                                                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
//                                                                            if (!string.IsNullOrEmpty(UserNotificationUrl))
//                                                                            {
//                                                                                if (HostEnvironment == HostEnvironmentType.Live)
//                                                                                {
//                                                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDetails.DisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
//                                                                                }
//                                                                                else
//                                                                                {
//                                                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDetails.DisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
//                                                                                }
//                                                                            }
//                                                                            else
//                                                                            {
//                                                                                string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", Item.MerchantId);
//                                                                                if (!string.IsNullOrEmpty(RewardSms))
//                                                                                {
//                                                                                    #region Send SMS
//                                                                                    string Message = RewardSms
//                                                                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
//                                                                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString())
//                                                                                    .Replace("[MERCHANT]", MerchantDetails.DisplayName)
//                                                                                    .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
//                                                                                    HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, Message, AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
//                                                                                    #endregion
//                                                                                }
//                                                                                else
//                                                                                {
//                                                                                    #region Send SMS
//                                                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + MerchantDetails.DisplayName + "  Bal: N" + HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
//                                                                                    HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, Message, AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
//                                                                                    #endregion
//                                                                                }
//                                                                            }
//                                                                            if (!string.IsNullOrEmpty(CustomerEmailAddress))
//                                                                            {
//                                                                                var _EmailParameters = new
//                                                                                {
//                                                                                    UserDisplayName = CustomerDisplayName,
//                                                                                    MerchantName = MerchantDetails.DisplayName,
//                                                                                    InvoiceAmount = Item.InvoiceAmount.ToString(),
//                                                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
//                                                                                    Balance = CustomerBalance.ToString(),
//                                                                                };
//                                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, CustomerDisplayName, CustomerEmailAddress, _EmailParameters, _OUserReference);
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                }

//                                                                #region Online Account
//                                                                ProcessStatusId = 3;
//                                                                ProcessMessage = "Customer rewarded successfully";
//                                                                #endregion
//                                                            }
//                                                            else
//                                                            {
//                                                                #region Online Account
//                                                                ProcessStatusId = 4;
//                                                                ProcessMessage = "Transaction failed";
//                                                                #endregion
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                            else
//                                            {
//                                                _CoreTransactionRequest = new OCoreTransaction.Request();
//                                                _CoreTransactionRequest.GroupKey = GroupKey;
//                                                _CoreTransactionRequest.CustomerId = AccountId;
//                                                _CoreTransactionRequest.UserReference = _OUserReference;
//                                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
//                                                _CoreTransactionRequest.ParentId = Item.MerchantId;
//                                                _CoreTransactionRequest.InvoiceAmount = (double)Item.InvoiceAmount;
//                                                _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                _CoreTransactionRequest.ReferenceAmount = 0;
//                                                _CoreTransactionRequest.ReferenceNumber = Item.ReferenceNumber;
//                                                if (Item.CashierId > 0)
//                                                {
//                                                    _CoreTransactionRequest.CashierId = (long)Item.CashierId;
//                                                }
//                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
//                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                {
//                                                    UserAccountId = Item.MerchantId,
//                                                    ModeId = TransactionMode.Debit,
//                                                    TypeId = TransactionType.CashReward,
//                                                    SourceId = TransactionSource.Merchant,
//                                                    Amount = 0,
//                                                    Charge = 0,
//                                                    Comission = 0,
//                                                    TotalAmount = 0,
//                                                });
//                                                if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
//                                                {
//                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                    {
//                                                        UserAccountId = AccountId,
//                                                        ModeId = TransactionMode.Credit,
//                                                        TypeId = TransactionType.CashReward,
//                                                        SourceId = TransactionSource.TUCBlack,
//                                                        Amount = 0,
//                                                        TotalAmount = 0,
//                                                    });
//                                                }
//                                                else
//                                                {
//                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
//                                                    {
//                                                        UserAccountId = AccountId,
//                                                        ModeId = TransactionMode.Credit,
//                                                        TypeId = TransactionType.CashReward,
//                                                        SourceId = TransactionSource.TUC,
//                                                        Amount = 0,
//                                                        TotalAmount = 0,
//                                                    });
//                                                }
//                                                _CoreTransactionRequest.Transactions = _TransactionItems;
//                                                _ManageCoreTransaction = new ManageCoreTransaction();
//                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
//                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
//                                                {
//                                                    #region Online Account
//                                                    ProcessStatusId = 3;
//                                                    ProcessMessage = "Customer rewarded successfully";
//                                                    #endregion
//                                                }
//                                                else
//                                                {
//                                                    #region Online Account
//                                                    ProcessStatusId = 4;
//                                                    ProcessMessage = "Reward failed";
//                                                    #endregion
//                                                }
//                                            }
//                                        }
//                                    }
//                                    #endregion
//                                }
//                                using (_HCoreContextOperations = new HCoreContextOperations())
//                                {
//                                    long FileId = 0;
//                                    var UpItem = _HCoreContextOperations.HCOUploadCustomerReward.Where(x => x.Id == Item.Id).FirstOrDefault();
//                                    if (UpItem != null)
//                                    {
//                                        if (AccountId > 0)
//                                        {
//                                            UpItem.AccountId = AccountId;
//                                        }
//                                        if (!string.IsNullOrEmpty(UpItem.TransactionReference))
//                                        {
//                                            UpItem.TransactionReference = GroupKey;
//                                        }
//                                        UpItem.StatusId = ProcessStatusId;
//                                        UpItem.StatusMessage = ProcessMessage;
//                                        UpItem.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                        _HCoreContextOperations.SaveChanges();
//                                        if (UpItem.FileId != null)
//                                        {
//                                            FileId = (long)UpItem.FileId;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        _HCoreContextOperations.SaveChanges();
//                                    }
//                                    if (FileId > 0)
//                                    {
//                                        using (_HCoreContextOperations = new HCoreContextOperations())
//                                        {
//                                            var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
//                                            if (FileDetails != null)
//                                            {
//                                                FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 1);
//                                                FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 2);
//                                                FileDetails.Success = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 3);
//                                                FileDetails.Error = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 4);
//                                                FileDetails.Completed = FileDetails.Success + FileDetails.Error;
//                                                if (FileDetails.TotalRecord == FileDetails.Completed)
//                                                {
//                                                    FileDetails.StatusId = 3;
//                                                }
//                                                // 1 => Pending 
//                                                // 2 => Processing
//                                                // 3 => Success 
//                                                //FileDetails.StatusMessage = ProcessMessage;
//                                                FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                                _HCoreContextOperations.SaveChanges();
//                                            }
//                                            else
//                                            {
//                                                _HCoreContextOperations.SaveChanges();
//                                            }
//                                        }
//                                    }
//                                }

//                            }

//                        }
//                    }
//                    else
//                    {
//                        var PendingItemsU = _HCoreContextOperations.HCOUploadCustomerReward.Where(x => x.StatusId == 2 && x.TransactionReference == null).Skip(0).Take(1000).ToList();
//                        if (PendingItemsU.Count > 0)
//                        {
//                            foreach (var PendingItem in PendingItemsU)
//                            {
//                                PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
//                                PendingItem.StatusId = 1;
//                                PendingItem.StatusMessage = "Pending";
//                            }
//                            _HCoreContextOperations.SaveChanges();
//                        }
//                    }
//                }
//            }
//            catch (Exception _Exception)
//            {
//                HCoreHelper.LogException("ActorUploadCustomerRewardProcess", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
//            }
//            #endregion
//        }
//    }

//    internal class ActorUploadCustomerRewardProcessV2 : ReceiveActor
//    {
//        FrameworkUploadCustomerRewardV2 _FrameworkUploadCustomerReward;
//        public ActorUploadCustomerRewardProcessV2()
//        {
//            Receive<long>(_Action =>
//            {
//                _FrameworkUploadCustomerReward = new FrameworkUploadCustomerRewardV2();
//                _FrameworkUploadCustomerReward.ActorUploadCustomerRewardProcess();
//            });
//        }
//    }

//}


