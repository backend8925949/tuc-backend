////==================================================================================
//// FileName: FrameworkUploadCustomerReward.cs
//// Author : Harshal Gandole
//// Created On : 
//// Description : Class for defining logics related to upload customer rewards
////
//// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
////
//// Revision History:
//// Date             : Changed By        : Comments
//// ---------------------------------------------------------------------------------
////                 | Harshal Gandole   : Created New File
//// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
////
////==================================================================================

//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Linq.Dynamic.Core;
//using Akka.Actor;
//using HCore.Data;
//using HCore.Data.Models;
//using HCore.Data.Operations;
//using HCore.Helper;
//using HCore.Operations;
//using HCore.Operations.Object;
//using HCore.TUC.Core.Framework.Merchant.App;
//using HCore.TUC.Core.Object.Merchant;
//using HCore.TUC.Core.Resource;
//using Microsoft.IdentityModel.Tokens;
//using SendGrid;
//using SendGrid.Helpers.Mail;
//using static HCore.Helper.HCoreConstant;
//using static HCore.Helper.HCoreConstant.Helpers;

//namespace HCore.TUC.Core.Framework.Merchant.Upload
//{
//    public class ManageUploadCustomerReward
//    {
//        FrameworkUploadCustomerReward _FrameworkUploadCustomerReward;

//        //public OResponse UploadBulkCustomerRewards(OUpload.RewardUpload.Request _Request)
//        //{
//        //    _FrameworkUploadCustomerReward = new FrameworkUploadCustomerReward();
//        //    return _FrameworkUploadCustomerReward.UploadBulkCustomerRewards(_Request);
//        //}

//        public void ProcessBulkRewardCustomerReward()
//        {
//            var system = ActorSystem.Create("ActorUploadCustomerRewardProcess");
//            var greeter = system.ActorOf<ActorUploadCustomerRewardProcess>("ActorUploadCustomerRewardProcess");
//            greeter.Tell("call");
//        }

//        public void ProcessBulkRewardCustomerAccountId()
//        {
//            var system = ActorSystem.Create("ProcessBulkRewardCustomerAccountId");
//            var greeter = system.ActorOf<ActorUploadCustomerIdProcess>("ProcessBulkRewardCustomerAccountId");
//            greeter.Tell("call");
//        }
//    }

//    public class FrameworkUploadCustomerReward
//    {
//        HCoreContext _HCoreContext;
//        HCoreContextOperations _HCoreContextOperations;
//        OAppProfileLite.Request _AppProfileRequest;
//        ManageCoreUserAccess _ManageCoreUserAccess;
//        ManageCoreTransaction _ManageCoreTransaction;
//        OCoreTransaction.Request _CoreTransactionRequest;
//        List<OCoreTransaction.TransactionItem> _TransactionItems;
//        public void ProcessAccountId()
//        {
//            #region Manage Exception
//            try
//            {
//                using (_HCoreContext = new HCoreContext())
//                {
//                    var Countries = _HCoreContext.HCCoreCountry
//                        .Select(x => new
//                        {
//                            x.Id,
//                            x.MobileNumberLength,
//                            x.Isd
//                        })
//                        .ToList();
//                    using (_HCoreContextOperations = new HCoreContextOperations())
//                    {
//                        var PendingItems = _HCoreContextOperations.HCOUploadCustomerReward
//                             .Where(x => x.StatusId == 1 && x.AccountId == null)
//                             .Skip(0)
//                             .Take(2000)
//                             .ToList();

//                        if (PendingItems.Count > 0)
//                        {
//                            foreach (var Item in PendingItems)
//                            {
//                                bool AllowProcess = true;
//                                long ProcessStatusId = 1;
//                                string ProcessMessage = "";

//                                int AccountCountryId = 0;
//                                string AccountCountryIsd = "";
//                                string FormattedMobileNumber = "";
//                                string AccountNumber = "";
//                                if (string.IsNullOrEmpty(Item.MobileNumber))
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Mobile number missing";
//                                    AllowProcess = false;
//                                }
//                                else if (Item.MobileNumber.Length < 10)
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                else if (Item.MobileNumber.Length > 13)
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                else if (!System.Text.RegularExpressions.Regex.IsMatch(Item.MobileNumber, "^[0-9]*$"))
//                                {
//                                    ProcessStatusId = 4;
//                                    ProcessMessage = "Invalid mobile number";
//                                    AllowProcess = false;
//                                }
//                                long AccountId = 0;
//                                if (AllowProcess)
//                                {
//                                    #region Get Customer 
//                                    var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(Item.MobileNumber);
//                                    if (ValidateMobileNumber.IsNumberValid == true)
//                                    {
//                                        var CountryInformation = Countries.Where(x => x.Isd == ValidateMobileNumber.Isd)
//                                            .Select(x => new
//                                            {
//                                                x.Id,
//                                                x.MobileNumberLength,
//                                                x.Isd
//                                            })
//                                            .FirstOrDefault();
//                                        string TUserName = _AppConfig.AppUserPrefix + Item.MobileNumber;
//                                        string TAccountNumber = Item.MobileNumber;
//                                        using (_HCoreContext = new HCoreContext())
//                                        {

//                                            var AccountDetails = _HCoreContext.HCUAccount
//                                               .Where(x => (x.AccountTypeId == UserAccountType.Appuser && (x.AccountCode == TAccountNumber || x.MobileNumber == TAccountNumber)))
//                                               .Select(x => new
//                                               {
//                                                   x.Id,
//                                                   x.CountryId,
//                                                   x.AccountCode,
//                                               }).FirstOrDefault();
//                                            if (AccountDetails != null)
//                                            {
//                                                AccountId = AccountDetails.Id;
//                                                AccountCountryId = (int)AccountDetails.CountryId;
//                                                AccountCountryIsd = CountryInformation.Isd;
//                                                AccountNumber = AccountDetails.AccountCode;
//                                            }
//                                            else
//                                            {
//                                                _AppProfileRequest = new OAppProfileLite.Request();
//                                                _AppProfileRequest.OwnerId = Item.MerchantId;
//                                                if (Item.CreatedById > 0)
//                                                {
//                                                    _AppProfileRequest.CreatedById = (long)Item.CreatedById;
//                                                }
//                                                _AppProfileRequest.CountryId = CountryInformation.Id;
//                                                _AppProfileRequest.MobileNumber = TAccountNumber;
//                                                _AppProfileRequest.EmailAddress = Item.EmailAddress;
//                                                _AppProfileRequest.DateOfBirth = Item.DateOfBirth;
//                                                _AppProfileRequest.Name = Item.Name;
//                                                _AppProfileRequest.FirstName = Item.FirstName;
//                                                _AppProfileRequest.MiddleName = Item.MiddleName;
//                                                _AppProfileRequest.LastName = Item.LastName;
//                                                if (!string.IsNullOrEmpty(Item.FirstName))
//                                                {
//                                                    _AppProfileRequest.DisplayName = Item.FirstName;
//                                                }
//                                                else
//                                                {
//                                                    _AppProfileRequest.DisplayName = Item.MobileNumber;
//                                                }
//                                                if (!string.IsNullOrEmpty(Item.Gender))
//                                                {
//                                                    _AppProfileRequest.GenderCode = Item.Gender.ToLower();
//                                                }
//                                                OUserReference _OUserReference = new OUserReference();
//                                                _OUserReference.AccountId = Item.MerchantId;
//                                                _OUserReference.AccountTypeId = UserAccountType.Merchant;
//                                                _AppProfileRequest.UserReference = _OUserReference;
//                                                _ManageCoreUserAccess = new ManageCoreUserAccess();
//                                                OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccountLite(_AppProfileRequest);
//                                                if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
//                                                {
//                                                    AccountCountryIsd = CountryInformation.Isd;
//                                                    AccountCountryId = CountryInformation.Id;
//                                                    AccountNumber = _AppUserCreateResponse.AccountCode;
//                                                    AccountId = _AppUserCreateResponse.AccountId;
//                                                }
//                                                else
//                                                {
//                                                    ProcessStatusId = 4;
//                                                    ProcessMessage = "Account creation failed";
//                                                    AllowProcess = false;
//                                                }
//                                            }
//                                        }
//                                    }
//                                    else
//                                    {
//                                        ProcessStatusId = 4;
//                                        ProcessMessage = "Invalid mobile number";
//                                        AllowProcess = false;
//                                    }
//                                    //}
//                                    #endregion
//                                }
//                                if (AccountId > 0)
//                                {
//                                    Item.AccountId = AccountId;
//                                }
//                                Item.AccountCountryId = AccountCountryId;
//                                Item.AccountCountryIsd = AccountCountryIsd;
//                                Item.FormattedMobileNumber = FormattedMobileNumber;
//                                Item.AccountNumber = AccountNumber;
//                                Item.StatusId = ProcessStatusId;
//                                Item.StatusMessage = ProcessMessage;
//                                Item.ModifyDate = HCoreHelper.GetGMTDateTime();
//                            }
//                            _HCoreContextOperations.SaveChanges();

//                            var FileIds = PendingItems.Where(x => x.FileId != null).Select(x => x.FileId).Distinct().ToList();
//                            if (FileIds.Count > 0)
//                            {
//                                using (_HCoreContextOperations = new HCoreContextOperations())
//                                {
//                                    foreach (var FileId in FileIds)
//                                    {
//                                        var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
//                                        if (FileDetails != null)
//                                        {
//                                            FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 1);
//                                            FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 2);
//                                            FileDetails.Success = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 3);
//                                            FileDetails.Error = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 4);
//                                            FileDetails.Completed = FileDetails.Success + FileDetails.Error;
//                                            if (FileDetails.TotalRecord == FileDetails.Completed)
//                                            {
//                                                FileDetails.StatusId = 3;
//                                            }
//                                            // 1 => Pending 
//                                            // 2 => Processing
//                                            // 3 => Success 
//                                            //FileDetails.StatusMessage = ProcessMessage;
//                                            FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                        }
//                                    }
//                                    _HCoreContextOperations.SaveChanges();
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception _Exception)
//            {
//                HCoreHelper.LogException("ActorUploadCustomerRewardProcess", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
//            }
//            #endregion
//        }
//        HCUAccountTransaction _HCUAccountTransactionChild;
//        public void ProcessCustomerRewardsV2()
//        {
//            #region Manage Exception
//            try
//            {
//                using (_HCoreContextOperations = new HCoreContextOperations())
//                {
//                    var PendingItems = _HCoreContextOperations.HCOUploadCustomerReward.Where(x => x.StatusId == 1 && x.AccountId != null && x.MerchantCountryId != null).Skip(0).Take(10000).ToList();
//                    if (PendingItems.Count > 0)
//                    {
//                        foreach (var PendingItem in PendingItems)
//                        {
//                            PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
//                            PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
//                            PendingItem.StatusId = 2;
//                            PendingItem.StatusMessage = "Processing";
//                        }
//                        _HCoreContextOperations.SaveChanges();
//                    }
//                }
//                using (_HCoreContextOperations = new HCoreContextOperations())
//                {
//                    int offset = 0;
//                    for (int i = 0; i < 6; i++)
//                    {
//                        using (_HCoreContextOperations = new HCoreContextOperations())
//                        {
//                            var PendingItems = _HCoreContextOperations.HCOUploadCustomerReward.Where(x => x.StatusId == 2 && x.AccountId != null && x.MerchantCountryId != null).Skip(offset).Take(2000).ToList();
//                            if (PendingItems.Count > 0)
//                            {
//                                using (_HCoreContext = new HCoreContext())
//                                {
//                                    foreach (var Item in PendingItems)
//                                    {
//                                        bool AllowProcess = true;
//                                        long ProcessStatusId = 1;
//                                        string ProcessMessage = "";
//                                        long AccountId = 0;
//                                        #region Get Customer 
//                                        string GroupKey = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(Item.MerchantId) + HCoreHelper.GetAccountIdString(AccountId) + HCoreHelper.GenerateRandomNumber(4);
//                                        var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", Item.MerchantId);
//                                        double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                        if (TUCRewardPercentage > 0)
//                                        {
//                                            double TUCRewardAmount = HCoreHelper.GetPercentage((double)Item.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
//                                            if (TUCRewardAmount <= 0)
//                                            {
//                                                ProcessStatusId = 4;
//                                                ProcessMessage = "Invalid reward amount";
//                                                AllowProcess = false;
//                                            }
//                                            if (TUCRewardAmount > Item.InvoiceAmount)
//                                            {
//                                                ProcessStatusId = 4;
//                                                ProcessMessage = "Invalid invoice amount";
//                                                AllowProcess = false;
//                                            }
//                                            else
//                                            {
//                                                if (AllowProcess == true)
//                                                {
//                                                    #region Reward Calculation
//                                                    int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", Item.MerchantId));
//                                                    int TransactionStatusId = HelperStatus.Transaction.Success;
//                                                    double TUCRewardUserAmount = 0;
//                                                    double TUCRewardUserAmountPercentage = 0;
//                                                    double TUCRewardCommissionAmount = 0;
//                                                    double TUCRewardCommissionAmountPercentage = 0;
//                                                    if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
//                                                    {
//                                                        var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", Item.MerchantId);
//                                                        TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                                        TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
//                                                        TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
//                                                        TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
//                                                        if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
//                                                        {
//                                                            TransactionStatusId = HelperStatus.Transaction.Success;
//                                                        }
//                                                        else
//                                                        {
//                                                            _ManageCoreTransaction = new ManageCoreTransaction();
//                                                            double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
//                                                            if (MerchantBalance >= TUCRewardAmount)
//                                                            {
//                                                                TransactionStatusId = HelperStatus.Transaction.Success;
//                                                            }
//                                                            else
//                                                            {
//                                                                TransactionStatusId = HelperStatus.Transaction.Pending;
//                                                            }
//                                                        }
//                                                    }
//                                                    else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
//                                                    {

//                                                        int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", Item.MerchantId));
//                                                        if (TAllowCustomReward == 1)
//                                                        {
//                                                            if (Item.RewardAmount > 0)
//                                                            {
//                                                                TUCRewardAmount = (double)Item.RewardAmount;
//                                                            }
//                                                        }
//                                                        double MaximumCommissionAmount = 2500;
//                                                        double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
//                                                        double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", Item.MerchantId)), _AppConfig.SystemRoundPercentage);
//                                                        double SystemCategoryCommissionPercentage = 0;
//                                                        if (Item.MerchantCategoryId > 0)
//                                                        {
//                                                            SystemCategoryCommissionPercentage = (double)Item.MerchantCategoryComission;
//                                                        }
//                                                        if (SystemMerchantCommissionPercentage > 0)
//                                                        {
//                                                            TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
//                                                        }
//                                                        else if (SystemCategoryCommissionPercentage > 0)
//                                                        {
//                                                            TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
//                                                        }
//                                                        else
//                                                        {
//                                                            TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
//                                                        }

//                                                        TUCRewardCommissionAmount = HCoreHelper.GetPercentage((double)Item.InvoiceAmount, TUCRewardCommissionAmountPercentage);
//                                                        if (TUCGold != null && TUCGold.Value == "1")
//                                                        {
//                                                            TUCRewardCommissionAmount = 0;
//                                                        }
//                                                        if (TUCRewardCommissionAmount > MaximumCommissionAmount)
//                                                        {
//                                                            TUCRewardCommissionAmount = MaximumCommissionAmount;
//                                                        }
//                                                        TUCRewardUserAmount = TUCRewardAmount;
//                                                        TUCRewardUserAmountPercentage = TUCRewardPercentage;
//                                                        TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
//                                                        if (TUCRewardAmount > Item.InvoiceAmount)
//                                                        {
//                                                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
//                                                        }
//                                                        if (TUCGold != null && TUCGold.Value == "1")
//                                                        {
//                                                            TransactionStatusId = HelperStatus.Transaction.Success;
//                                                        }
//                                                        else
//                                                        {
//                                                            TransactionStatusId = HelperStatus.Transaction.Pending;
//                                                            //_ManageCoreTransaction = new ManageCoreTransaction();
//                                                            //double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
//                                                            //if (MerchantBalance >= TUCRewardAmount)
//                                                            //{
//                                                            //    TransactionStatusId = HelperStatus.Transaction.Success;
//                                                            //}
//                                                            //else
//                                                            //{
//                                                            //    if (MerchantBalance > -5000)
//                                                            //    {
//                                                            //        if ((MerchantBalance - TUCRewardAmount) > -5000)
//                                                            //        {
//                                                            //            TransactionStatusId = HelperStatus.Transaction.Pending;
//                                                            //        }
//                                                            //        else
//                                                            //        {
//                                                            //            TransactionStatusId = HelperStatus.Transaction.Success;
//                                                            //        }
//                                                            //    }
//                                                            //}
//                                                        }
//                                                    }
//                                                    #endregion

//                                                    if (TUCRewardAmount > Item.InvoiceAmount)
//                                                    {
//                                                        #region Online Account
//                                                        ProcessStatusId = 4;
//                                                        ProcessMessage = "Invalid reward amount";
//                                                        #endregion
//                                                    }
//                                                    else
//                                                    {
//                                                        #region Parent Transaction
//                                                        HCUAccountTransaction _HCUAccountTransaction = new HCUAccountTransaction();
//                                                        _HCUAccountTransaction.Guid = GroupKey;
//                                                        _HCUAccountTransaction.ParentId = Item.MerchantId;
//                                                        _HCUAccountTransaction.AccountId = Item.MerchantId;
//                                                        _HCUAccountTransaction.ModeId = TransactionMode.Debit;
//                                                        _HCUAccountTransaction.TypeId = TransactionType.CashReward;
//                                                        _HCUAccountTransaction.SourceId = TransactionSource.Merchant;
//                                                        _HCUAccountTransaction.Amount = TUCRewardAmount;
//                                                        _HCUAccountTransaction.Charge = 0;
//                                                        _HCUAccountTransaction.ComissionAmount = TUCRewardCommissionAmount;
//                                                        _HCUAccountTransaction.TotalAmount = TUCRewardAmount;
//                                                        _HCUAccountTransaction.Balance = 0;
//                                                        _HCUAccountTransaction.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransaction.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransaction.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransaction.CustomerId = AccountId;
//                                                        _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                        _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                        _HCUAccountTransaction.CreatedById = Item.MerchantId;
//                                                        _HCUAccountTransaction.GroupKey = GroupKey;
//                                                        _HCUAccountTransaction.StatusId = TransactionStatusId;
//                                                        _HCUAccountTransaction.TCode = "TWSQ";
//                                                        #endregion
//                                                        if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
//                                                        {
//                                                            #region Parent Transaction
//                                                            _HCUAccountTransactionChild = new HCUAccountTransaction();
//                                                            _HCUAccountTransactionChild.Guid = GroupKey;
//                                                            _HCUAccountTransactionChild.ParentId = Item.MerchantId;
//                                                            _HCUAccountTransactionChild.AccountId = AccountId;
//                                                            _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
//                                                            _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
//                                                            _HCUAccountTransactionChild.SourceId = TransactionSource.TUCBlack;
//                                                            _HCUAccountTransactionChild.Amount = TUCRewardUserAmount;
//                                                            _HCUAccountTransactionChild.Charge = 0;
//                                                            _HCUAccountTransactionChild.ComissionAmount = 0;
//                                                            _HCUAccountTransactionChild.TotalAmount = TUCRewardUserAmount;
//                                                            _HCUAccountTransactionChild.Balance = 0;
//                                                            _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.CustomerId = AccountId;
//                                                            _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                            _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                            _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
//                                                            _HCUAccountTransactionChild.GroupKey = GroupKey;
//                                                            _HCUAccountTransactionChild.StatusId = TransactionStatusId;
//                                                            _HCUAccountTransactionChild.TCode = "TWSQ";
//                                                            _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
//                                                            #endregion
//                                                        }
//                                                        else
//                                                        {
//                                                            #region Parent Transaction
//                                                            _HCUAccountTransactionChild = new HCUAccountTransaction();
//                                                            _HCUAccountTransactionChild.Guid = GroupKey;
//                                                            _HCUAccountTransactionChild.ParentId = Item.MerchantId;
//                                                            _HCUAccountTransactionChild.AccountId = AccountId;
//                                                            _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
//                                                            _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
//                                                            _HCUAccountTransactionChild.SourceId = TransactionSource.TUC;
//                                                            _HCUAccountTransactionChild.Amount = TUCRewardUserAmount;
//                                                            _HCUAccountTransactionChild.Charge = 0;
//                                                            _HCUAccountTransactionChild.ComissionAmount = 0;
//                                                            _HCUAccountTransactionChild.TotalAmount = TUCRewardUserAmount;
//                                                            _HCUAccountTransactionChild.Balance = 0;
//                                                            _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                            _HCUAccountTransactionChild.CustomerId = AccountId;
//                                                            _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                            _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                            _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
//                                                            _HCUAccountTransactionChild.GroupKey = GroupKey;
//                                                            _HCUAccountTransactionChild.StatusId = TransactionStatusId;
//                                                            _HCUAccountTransactionChild.TCode = "TWSQ";
//                                                            _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
//                                                            #endregion
//                                                        }
//                                                        #region Parent Transaction
//                                                        _HCUAccountTransactionChild = new HCUAccountTransaction();
//                                                        _HCUAccountTransactionChild.Guid = GroupKey;
//                                                        _HCUAccountTransactionChild.ParentId = Item.MerchantId;
//                                                        _HCUAccountTransactionChild.AccountId = AccountId;
//                                                        _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
//                                                        _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
//                                                        _HCUAccountTransactionChild.SourceId = TransactionSource.Settlement;
//                                                        _HCUAccountTransactionChild.Amount = TUCRewardCommissionAmount;
//                                                        _HCUAccountTransactionChild.Charge = 0;
//                                                        _HCUAccountTransactionChild.ComissionAmount = 0;
//                                                        _HCUAccountTransactionChild.TotalAmount = TUCRewardCommissionAmount;
//                                                        _HCUAccountTransactionChild.Balance = 0;
//                                                        _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                        _HCUAccountTransactionChild.CustomerId = AccountId;
//                                                        _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                        _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                        _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
//                                                        _HCUAccountTransactionChild.GroupKey = GroupKey;
//                                                        _HCUAccountTransactionChild.StatusId = TransactionStatusId;
//                                                        _HCUAccountTransactionChild.TCode = "TWSQ";
//                                                        _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
//                                                        #endregion
//                                                        #region Online Account
//                                                        ProcessStatusId = 3;
//                                                        ProcessMessage = "Customer rewarded successfully";
//                                                        #endregion
//                                                    }
//                                                }
//                                            }
//                                        }
//                                        else
//                                        {
//                                            #region Parent Transaction
//                                            HCUAccountTransaction _HCUAccountTransaction = new HCUAccountTransaction();
//                                            _HCUAccountTransaction.Guid = GroupKey;
//                                            _HCUAccountTransaction.ParentId = Item.MerchantId;
//                                            _HCUAccountTransaction.AccountId = Item.MerchantId;
//                                            _HCUAccountTransaction.ModeId = TransactionMode.Debit;
//                                            _HCUAccountTransaction.TypeId = TransactionType.CashReward;
//                                            _HCUAccountTransaction.SourceId = TransactionSource.Merchant;
//                                            _HCUAccountTransaction.Amount = 0;
//                                            _HCUAccountTransaction.Charge = 0;
//                                            _HCUAccountTransaction.ComissionAmount = 0;
//                                            _HCUAccountTransaction.TotalAmount = 0;
//                                            _HCUAccountTransaction.Balance = 0;
//                                            _HCUAccountTransaction.PurchaseAmount = (double)Item.InvoiceAmount;
//                                            _HCUAccountTransaction.ReferenceAmount = (double)Item.InvoiceAmount;
//                                            _HCUAccountTransaction.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                            _HCUAccountTransaction.CustomerId = AccountId;
//                                            _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                            _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
//                                            _HCUAccountTransaction.CreatedById = Item.MerchantId;
//                                            _HCUAccountTransaction.GroupKey = GroupKey;
//                                            _HCUAccountTransaction.TCode = "TWSQ";
//                                            _HCUAccountTransaction.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
//                                            #endregion
//                                            if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
//                                            {
//                                                #region Parent Transaction
//                                                _HCUAccountTransactionChild = new HCUAccountTransaction();
//                                                _HCUAccountTransactionChild.Guid = GroupKey;
//                                                _HCUAccountTransactionChild.ParentId = Item.MerchantId;
//                                                _HCUAccountTransactionChild.AccountId = AccountId;
//                                                _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
//                                                _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
//                                                _HCUAccountTransactionChild.SourceId = TransactionSource.TUCBlack;
//                                                _HCUAccountTransactionChild.Amount = 0;
//                                                _HCUAccountTransactionChild.Charge = 0;
//                                                _HCUAccountTransactionChild.ComissionAmount = 0;
//                                                _HCUAccountTransactionChild.TotalAmount = 0;
//                                                _HCUAccountTransactionChild.Balance = 0;
//                                                _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.CustomerId = AccountId;
//                                                _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
//                                                _HCUAccountTransactionChild.GroupKey = GroupKey;
//                                                _HCUAccountTransactionChild.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
//                                                _HCUAccountTransactionChild.TCode = "TWSQ";
//                                                _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
//                                                #endregion
//                                            }
//                                            else
//                                            {
//                                                #region Parent Transaction
//                                                _HCUAccountTransactionChild = new HCUAccountTransaction();
//                                                _HCUAccountTransactionChild.Guid = GroupKey;
//                                                _HCUAccountTransactionChild.ParentId = Item.MerchantId;
//                                                _HCUAccountTransactionChild.AccountId = AccountId;
//                                                _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
//                                                _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
//                                                _HCUAccountTransactionChild.SourceId = TransactionSource.TUC;
//                                                _HCUAccountTransactionChild.Amount = 0;
//                                                _HCUAccountTransactionChild.Charge = 0;
//                                                _HCUAccountTransactionChild.ComissionAmount = 0;
//                                                _HCUAccountTransactionChild.TotalAmount = 0;
//                                                _HCUAccountTransactionChild.Balance = 0;
//                                                _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
//                                                _HCUAccountTransactionChild.CustomerId = AccountId;
//                                                _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
//                                                _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
//                                                _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
//                                                _HCUAccountTransactionChild.GroupKey = GroupKey;
//                                                _HCUAccountTransactionChild.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
//                                                _HCUAccountTransactionChild.TCode = "TWSQ";
//                                                _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
//                                                #endregion
//                                            }
//                                            #region Online Account
//                                            ProcessStatusId = 3;
//                                            ProcessMessage = "Customer rewarded successfully";
//                                            #endregion
//                                        }
//                                        #endregion
//                                        if (AccountId > 0)
//                                        {
//                                            Item.AccountId = AccountId;
//                                        }
//                                        Item.TransactionReference = GroupKey;
//                                        Item.StatusId = ProcessStatusId;
//                                        Item.StatusMessage = ProcessMessage;
//                                        Item.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                    }
//                                    _HCoreContext.SaveChanges();
//                                    var FileIds = PendingItems.Where(x => x.FileId != null).Select(x => x.FileId).Distinct().ToList();
//                                    if (FileIds.Count > 0)
//                                    {
//                                        foreach (var FileId in FileIds)
//                                        {
//                                            var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
//                                            if (FileDetails != null)
//                                            {
//                                                FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 1);
//                                                FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 2);
//                                                FileDetails.Success = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 3);
//                                                FileDetails.Error = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 4);
//                                                FileDetails.Completed = FileDetails.Success + FileDetails.Error;
//                                                if (FileDetails.TotalRecord == FileDetails.Completed)
//                                                {
//                                                    FileDetails.StatusId = 3;
//                                                }
//                                                // 1 => Pending 
//                                                // 2 => Processing
//                                                // 3 => Success 
//                                                //FileDetails.StatusMessage = ProcessMessage;
//                                                FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                            }
//                                        }
//                                        _HCoreContextOperations.SaveChanges();
//                                    }
//                                }
//                            }
//                            offset = offset + 2000;
//                        }
//                    }
//                }
//            }
//            catch (Exception _Exception)
//            {
//                HCoreHelper.LogException("ActorUploadCustomerRewardProcess", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
//            }
//            #endregion
//        }
//    }

//    public class ActorUploadCustomerRewardProcess : ReceiveActor
//    {
//        FrameworkUploadCustomerReward _FrameworkUploadCustomerReward;
//        public ActorUploadCustomerRewardProcess()
//        {
//            Receive<string>(_Action =>
//            {
//                _FrameworkUploadCustomerReward = new FrameworkUploadCustomerReward();
//                _FrameworkUploadCustomerReward.ProcessCustomerRewardsV2();
//            });
//        }
//    }

//    public class ActorUploadCustomerIdProcess : ReceiveActor
//    {
//        public ActorUploadCustomerIdProcess()
//        {
//            Receive<string>(_Action =>
//            {
//                FrameworkUploadCustomerReward _FrameworkUploadCustomerReward = new FrameworkUploadCustomerReward();
//                _FrameworkUploadCustomerReward.ProcessAccountId();
//            });
//        }
//    }
//}