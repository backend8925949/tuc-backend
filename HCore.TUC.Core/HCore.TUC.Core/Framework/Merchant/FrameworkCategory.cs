//==================================================================================
// FileName: FrameworkCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to category
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Core.Merchant.Object;
using HCore.TUC.Core.Resource;
using System.Collections.Generic;

namespace HCore.TUC.Core.Merchant.Framework
{
    public class FrameworkCategory
    {
        HCoreContext _HCoreContext;
        TUCMerchantCategory _TUCMerchantCategory;

        /// <summary>
        /// Description: Saves the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCategory(OCategory.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Commission < 0 && _Request.Commission > 100)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14119, TUCCoreResource.CAA14119M);
                }
                if (_Request.RootCategoryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14120, TUCCoreResource.CAA14120M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    bool CheckDetails = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.RootCategoryId && x.Guid == _Request.RootCategoryKey);
                    if (CheckDetails)
                    {
                        bool categoryDetails = _HCoreContext.TUCMerchantCategory.Any(x => x.RootCategoryId == _Request.RootCategoryId);
                        if (categoryDetails)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14118, TUCCoreResource.CAA14118M);
                        }
                        else
                        {
                            _TUCMerchantCategory = new TUCMerchantCategory();
                            _TUCMerchantCategory.Guid = HCoreHelper.GenerateGuid();
                            _TUCMerchantCategory.RootCategoryId = _Request.RootCategoryId;
                            _TUCMerchantCategory.Commission = _Request.Commission;
                            _TUCMerchantCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCMerchantCategory.CreatedById = _Request.UserReference.AccountId;
                            _TUCMerchantCategory.StatusId = StatusId;
                            _HCoreContext.TUCMerchantCategory.Add(_TUCMerchantCategory);
                            _HCoreContext.SaveChanges();
                        }
                        var _Response = new
                        {
                            ReferenceId = _TUCMerchantCategory.Id,
                            ReferenceKey = _TUCMerchantCategory.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14121, TUCCoreResource.CAA14121M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14122, TUCCoreResource.CAA14122M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCategory(OCategory.Update _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _CategoryDetails = _HCoreContext.TUCMerchantCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_CategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.RootCategoryKey))
                        {
                            bool categoryDetails = _HCoreContext.TUCMerchantCategory.Any(x => x.Commission == _Request.Commission && x.Id != _CategoryDetails.Id && _CategoryDetails.RootCategoryId == _Request.RootCategoryId);
                            if (categoryDetails)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14118, TUCCoreResource.CAA14118M);
                            }
                            _CategoryDetails.Commission = _Request.Commission;
                        }
                        if (StatusId > 0)
                        {
                            _CategoryDetails.StatusId = StatusId;
                        }
                        _CategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _CategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CategoryDetails, TUCCoreResource.CAA14117, TUCCoreResource.CAA14117M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Deletes the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCategory(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = _HCoreContext.TUCMerchantCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (CategoryDetails != null)
                    {
                        _HCoreContext.TUCMerchantCategory.Remove(CategoryDetails);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14116, TUCCoreResource.CAA14116M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the category details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _CategoryDetails = (from x in _HCoreContext.TUCMerchantCategory
                                                          where x.Guid == _Request.ReferenceKey
                                                          && x.Id == _Request.ReferenceId
                                                          select new OCategory.Details
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              CategoryId = x.RootCategory.Id,
                                                              CategoryKey = x.RootCategory.Guid,
                                                              CategoryName = x.RootCategory.Name,
                                                              CategoryStatusId = x.RootCategory.Status.Id,
                                                              CategoryStatusCode = x.RootCategory.Status.SystemName,
                                                              CategoryStatusName = x.RootCategory.Status.Name,
                                                              CategoryCreateDate = x.RootCategory.CreateDate,
                                                              IconStorageUrl = x.RootCategory.IconStorage.Path,
                                                              PosterStorageUrl = x.RootCategory.PosterStorage.Path,

                                                              Commission = x.Commission,

                                                              CreateDate = x.CreateDate,
                                                              CreatedById = x.CreatedById,
                                                              CreatedByKey = x.CreatedBy.Guid,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                              ModifyDate = x.ModifyDate,
                                                              ModifyById = x.ModifyById,
                                                              ModifyByKey = x.ModifyBy.Guid,
                                                              ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                          })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_CategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_CategoryDetails.IconStorageUrl))
                        {
                            _CategoryDetails.IconStorageUrl = _AppConfig.StorageUrl + _CategoryDetails.IconStorageUrl;
                        }
                        else
                        {
                            _CategoryDetails.IconStorageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_CategoryDetails.PosterStorageUrl))
                        {
                            _CategoryDetails.PosterStorageUrl = _AppConfig.StorageUrl + _CategoryDetails.PosterStorageUrl;
                        }
                        else
                        {
                            _CategoryDetails.PosterStorageUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CategoryDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCMerchantCategory
                                               .Where(x => x.Id == _Request.ReferenceId
                                                   && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCategory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CategoryId = x.RootCategory.Id,
                                                    CategoryKey = x.RootCategory.Guid,
                                                    CategoryName = x.RootCategory.Name,
                                                    CategoryStatusId = x.RootCategory.StatusId,
                                                    IconStorageUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,

                                                    Commission = x.Commission,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                .OrderBy(x => x.CategoryId)
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    var _List = _HCoreContext.TUCMerchantCategory
                                               .Where(x => x.Id == _Request.ReferenceId
                                                   && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCategory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CategoryId = x.RootCategory.Id,
                                                    CategoryKey = x.RootCategory.Guid,
                                                    CategoryName = x.RootCategory.Name,
                                                    CategoryStatusId = x.RootCategory.StatusId,
                                                    IconStorageUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,

                                                    Commission = x.Commission,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                .OrderBy(_Request.SortExpression)
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        List<OCategory.AllCats> _AllCats;
        /// <summary>
        /// Description: Gets all category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAllCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var TCategories = _HCoreContext.TUCMerchantCategory
                           .Select(x => new OCategory.Details
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               ParentCategoryId = x.RootCategoryId,

                               Name = x.RootCategory.Name,
                               Description = x.RootCategory.Description,

                               IconUrl = x.RootCategory.IconStorage.Path,
                               PosterUrl = x.RootCategory.PosterStorage.Path,
                               Fee = x.Commission,
                               StatusId = x.StatusId,
                               StatusCode = x.Status.SystemName,
                               StatusName = x.Status.Name
                           }).OrderBy(x => x.Name).ToList();
                    foreach (var TCategoryItem in TCategories)
                    {
                        if (!string.IsNullOrEmpty(TCategoryItem.IconUrl))
                        {
                            TCategoryItem.IconUrl = _AppConfig.StorageUrl + TCategoryItem.IconUrl;
                        }
                        else
                        {
                            TCategoryItem.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(TCategoryItem.PosterUrl))
                        {
                            TCategoryItem.PosterUrl = _AppConfig.StorageUrl + TCategoryItem.PosterUrl;
                        }
                        else
                        {
                            TCategoryItem.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _AllCats = new List<OCategory.AllCats>();
                    var RootCategories = TCategories.Where(x => x.ParentCategoryId == null).ToList();
                    var SubCategories = TCategories.Where(x => x.ParentCategoryId != null).ToList();
                    foreach (var RootCategory in RootCategories)
                    {
                        var SubCats = SubCategories.Where(x => x.ParentCategoryId == RootCategory.ReferenceId)
                            .Select(x => new OCategory.Item
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,
                                Name = x.Name,
                                IconUrl = x.IconUrl,
                                Fee = x.Fee,
                            }).ToList();
                        _AllCats.Add(new OCategory.AllCats
                        {
                            ReferenceId = RootCategory.ReferenceId,
                            ReferenceKey = RootCategory.ReferenceKey,
                            Name = RootCategory.Name,
                            IconUrl = RootCategory.IconUrl,
                            Fee = RootCategory.Fee,
                            Items = SubCats,
                        });
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AllCats, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAllCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


    }
}
