//==================================================================================
// FileName: FrameworkProcessPendingReward.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Merchant
{
    public class FrameworkProcessPendingReward
    {

        //HCoreContext _HCoreContext;
        //ManageCoreTransaction _ManageCoreTransaction;
        //OCoreTransaction.Request _CoreTransactionRequest;
        //List<OCoreTransaction.TransactionItem> _TransactionItems;
        //OUserReference _OUserReference;
        //public void ProcessPendingReward()
        //{
        //    using (_HCoreContext = new HCoreContext())
        //    {
                
        //        var MerchantPendingTransactions = _HCoreContext.TUCLoyaltyPending.
        //            Where(x =>  x.StatusId == HelperStatus.Transaction.Pending && x.TotalAmount > 0)
        //            .GroupBy(x => x.MerchantId)
        //            .Select(x => x.Key).ToList();
        //        _HCoreContext.Dispose();
        //        foreach (var item in MerchantPendingTransactions)
        //        {
        //            _ManageCoreTransaction = new ManageCoreTransaction();
        //            double TBalance = _ManageCoreTransaction.GetAccountBalance((long)item, TransactionSource.Merchant);
        //            if (TBalance > 0)
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    var PendingRewards = _HCoreContext.TUCLoyaltyPending
        //                                        .Where(x => x.StatusId == HelperStatus.Transaction.Pending
        //                                        && x.Merchant.StatusId == HelperStatus.Default.Active
        //                                        && x.TotalAmount > 0
        //                                        && x.CreatedById != null
        //                                        && x.FromAccountId == item
        //                                        && x.InvoiceAmount > 0)
        //                                        .OrderByDescending(x => x.CreateDate)
        //                                        .Select(x => new
        //                                        {
        //                                            Id = x.Id,
        //                                            Guid = x.Guid,
        //                                            SourceId = x.SourceId,
        //                                            TypeId = x.TypeId,
        //                                            FromAccountId = x.FromAccountId,
        //                                            ToAccountId = x.ToAccountId,
        //                                            StoreId = x.StoreId,
        //                                            Amount = x.Amount,
        //                                            CommissionAmount = x.CommissionAmount,
        //                                            TotalAmount = x.TotalAmount,
        //                                            ReferenceNumber = x.ReferenceNumber,
        //                                            CreatedById = x.CreatedById,
        //                                            InvoiceAmount = x.InvoiceAmount,
        //                                            CashierId = x.CashierId,
        //                                            TerminalId = x.TerminalId,
        //                                            TerminalProviderId = x.Terminal.ProviderId,
        //                                            TerminalAcquirerId = x.Terminal.AcquirerId,
        //                                            LoyaltyInvoiceAmount = x.LoyaltyInvoiceAmount,
        //                                            CreatedByAccounttypeId = x.CreatedBy.AccountTypeId,
        //                                            TransactionId = x.TransactionId,
        //                                            TransactionGroupKey = x.Transaction.Guid,
        //                                        })
        //                                        .Skip(0)
        //                                        .Take(100)
        //                                        .ToList();
        //                    _HCoreContext.Dispose();
        //                    if (PendingRewards.Count > 0)
        //                    {
        //                        foreach (var PendingReward in PendingRewards)
        //                        {
        //                            //double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage",(long) PendingReward.FromAccountId)), _AppConfig.SystemRoundPercentage);
        //                            //double RewardAmount = HCoreHelper.GetPercentage(PendingReward.InvoiceAmount, RewardPercentage, _AppConfig.SystemEntryRoundDouble);
        //                            double AmountPercentage = HCoreHelper.GetAmountPercentage(PendingReward.TotalAmount, PendingReward.InvoiceAmount);
        //                            if (AmountPercentage > 0)
        //                            {
        //                                if (PendingReward.TotalAmount > 0)
        //                                {
        //                                    _ManageCoreTransaction = new ManageCoreTransaction();
        //                                    double Balance = _ManageCoreTransaction.GetAccountBalance((long)PendingReward.FromAccountId, TransactionSource.Merchant);
        //                                    if (Balance >= PendingReward.TotalAmount)
        //                                    {
        //                                        if (PendingReward.TransactionId > 0)
        //                                        {
        //                                            using (HCoreContext _HCoreContext = new HCoreContext())
        //                                            {
        //                                                var GroupTr = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == PendingReward.TransactionGroupKey).ToList();
        //                                                foreach (var GroupTrI in GroupTr)
        //                                                {
        //                                                    GroupTrI.StatusId = HelperStatus.Transaction.Success;
        //                                                    GroupTrI.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                    GroupTrI.ModifyById = 1;
        //                                                }
        //                                                var pReward = _HCoreContext.TUCLoyaltyPending.Where(x => x.Id == PendingReward.Id).FirstOrDefault();
        //                                                if (pReward != null)
        //                                                {
        //                                                    pReward.StatusId = HelperStatus.Transaction.Success;
        //                                                    pReward.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                    pReward.ModifyById = 1;
        //                                                    _HCoreContext.SaveChanges();
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            _OUserReference = new OUserReference();
        //                                            _OUserReference.AccountId = (long)PendingReward.CreatedById;
        //                                            _OUserReference.AccountTypeId = PendingReward.CreatedByAccounttypeId;
        //                                            _CoreTransactionRequest = new OCoreTransaction.Request();
        //                                            _CoreTransactionRequest.GroupKey = PendingReward.Guid;
        //                                            _CoreTransactionRequest.CustomerId = PendingReward.ToAccountId;
        //                                            _CoreTransactionRequest.UserReference = _OUserReference;
        //                                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
        //                                            _CoreTransactionRequest.ParentId = PendingReward.FromAccountId;
        //                                            if (PendingReward.StoreId != null)
        //                                            {
        //                                                _CoreTransactionRequest.SubParentId =(long) PendingReward.StoreId;
        //                                            }
        //                                            if (PendingReward.TerminalId != null)
        //                                            {
        //                                                _CoreTransactionRequest.TerminalId = (long)PendingReward.TerminalId;
        //                                                _CoreTransactionRequest.BankId = (long)PendingReward.TerminalAcquirerId;
        //                                                _CoreTransactionRequest.ProviderId = (long)PendingReward.TerminalProviderId;
        //                                            }
        //                                            _CoreTransactionRequest.InvoiceAmount = PendingReward.InvoiceAmount;
        //                                            _CoreTransactionRequest.ReferenceInvoiceAmount = PendingReward.LoyaltyInvoiceAmount;
        //                                            _CoreTransactionRequest.ReferenceAmount = PendingReward.TotalAmount;
        //                                            if (PendingReward.CashierId > 0)
        //                                            {
        //                                                _CoreTransactionRequest.CashierId = (long)PendingReward.CashierId;
        //                                            }
        //                                            _CoreTransactionRequest.ReferenceNumber = PendingReward.ReferenceNumber;
        //                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                            {
        //                                                UserAccountId = PendingReward.FromAccountId,
        //                                                ModeId = TransactionMode.Debit,
        //                                                TypeId = PendingReward.TypeId,
        //                                                SourceId = TransactionSource.Merchant,
        //                                                Amount = PendingReward.Amount,
        //                                                Charge = 0,
        //                                                Comission = PendingReward.CommissionAmount,
        //                                                TotalAmount = PendingReward.TotalAmount,
        //                                                Comment = "Pending reward credit",
        //                                            });
        //                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                            {
        //                                                UserAccountId = PendingReward.ToAccountId,
        //                                                ModeId = TransactionMode.Credit,
        //                                                TypeId = PendingReward.TypeId,
        //                                                SourceId =  PendingReward.SourceId,
        //                                                Amount = PendingReward.Amount,
        //                                                TotalAmount = PendingReward.Amount,
        //                                                Comment = "Pending reward credit",
        //                                            });
        //                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                            {
        //                                                UserAccountId = SystemAccounts.ThankUCashMerchant,
        //                                                ModeId = TransactionMode.Credit,
        //                                                TypeId = PendingReward.TypeId,
        //                                                SourceId = TransactionSource.Settlement,
        //                                                Amount = PendingReward.CommissionAmount,
        //                                                TotalAmount = PendingReward.CommissionAmount,
        //                                                Comment = "Pending reward credit",
        //                                            });
        //                                            _CoreTransactionRequest.Transactions = _TransactionItems;
        //                                            _ManageCoreTransaction = new ManageCoreTransaction();
        //                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                                            {
        //                                                using (_HCoreContext = new HCoreContext())
        //                                                {
        //                                                    var pReward = _HCoreContext.TUCLoyaltyPending.Where(x => x.Id == PendingReward.Id).FirstOrDefault();
        //                                                    if (pReward != null)
        //                                                    {
        //                                                        pReward.StatusId = HelperStatus.Transaction.Success;
        //                                                        pReward.TransactionId = TransactionResponse.ReferenceId;
        //                                                        pReward.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                                        pReward.ModifyById = 1;
        //                                                        _HCoreContext.SaveChanges();
        //                                                    }
        //                                                }
        //                                            }

        //                                        }
        //                                    }
        //                                }

        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        _HCoreContext.Dispose();
        //                    }

        //                }
        //            }
        //        }
        //    }
        //}
    }
}
